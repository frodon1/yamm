# Welcome to YAMM !

You can read the documentation from [this file](doc/build/html/index.html)

Some projects can be used through a GUI:

    bin/yamm_gui.py salome
    bin/yamm_gui.py nacre
    
A call to `bin/yamm_gui.py` with no argument will provide a list of available projects.

You can also install into your menu application a shortcut to the GUI with an installer:

    bin/yamm_gui.py -i salome

will add shorcuts for Yamm for SALOME in the desktop menu and on the desktop.    

These shortcuts can be removed:

    bin/yamm_gui.py -u salome

There is also some examples scripts provided in the directory examples.

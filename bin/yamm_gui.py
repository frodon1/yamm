#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)
from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                os.pardir)
yamm_src_dir = os.path.join(yamm_default_dir, 'src')
yamm_lib_dir = os.path.join(yamm_default_dir, 'lib')
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str
from PyQt4 import QtGui, QtCore  # @UnresolvedImport
import shutil
import stat
import string
import subprocess
import tempfile

import yamm.core.base.misc  # To test Python version @UnusedImport @IgnorePep8
from yamm.projects import projects_with_gui  # @IgnorePep8

try:
    import argparse
except ImportError:
    from yamm.core.framework.bin import yamm_argparse as argparse


def select_project(install_desktop, uninstall_desktop):
    app = QtGui.QApplication.instance() or QtGui.QApplication(sys.argv)
    app.setApplicationName('Yamm')
    app.setStyleSheet('QComboBox {combobox-popup: 0 ;}')
    # # Translation process
    locale = QtCore.QLocale.system().name()
    qtTranslator = QtCore.QTranslator()
    if qtTranslator.load('qt_' + locale,
                         QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.TranslationsPath)):
        app.installTranslator(qtTranslator)
    icon = os.path.join(yamm_default_dir, 'src', 'yamm', 'core', 'framework',
                        'gui', 'icons', 'yamm-icon.svg')
    app.setWindowIcon(QtGui.QIcon(icon))
    text = 'Please choose a project'
    if install_desktop:
        text += ' Yamm shorcut to install'
    elif uninstall_desktop:
        text += ' Yamm shorcut to uninstall'
    (project_name, ok) = QtGui.QInputDialog.getItem(QtGui.QWidget(), 'Yamm', text,
                                                    sorted(projects_with_gui),
                                                    current=0, editable=False)
    if ok:
        return str(project_name), projects_with_gui[str(project_name)]
    else:
        sys.exit(1)


def install_desktop(project_name):
    desktop(project_name, True, False)


def uninstall_desktop(project_name):
    desktop(project_name, False, True)


def desktop(project_name, install_desktop, uninstall_desktop):
    # Copy desktop file
    copy_file_src = os.path.join(yamm_src_dir, 'yamm', 'core', 'framework',
                                 "gui", "yamm.desktop")
    try:
        with open(copy_file_src) as file_src:
            file_src_content = file_src.read()
    except IOError as error:
        print(error)
        sys.exit(1)

    file_dest_template = string.Template(file_src_content)
    file_dest_content = file_dest_template.substitute(install_path=yamm_default_dir,
                                                      project_title=project_name.capitalize(),
                                                      project_name=project_name)
    copy_file_dest = os.path.join(tempfile.gettempdir(), "yamm-{0}.desktop".format(project_name))
    try:
        with open(copy_file_dest, 'w') as file_dest:
            file_dest.write(file_dest_content)
    except IOError as error:
        print(error)
        sys.exit(1)

    # xdg-desktop-menu
    try:
        command = ['xdg-desktop-menu']
        if install_desktop:
            command.append('install')
        elif uninstall_desktop:
            command.append('uninstall')
        command.append(copy_file_dest)
        retcode = subprocess.call(command)
        if retcode < 0:
            print("xdg-desktop-menu command failed !")
    except OSError as e:
        print("xdg-desktop-menu command failed, exception was: %s" % e)

    # Get the desktop directory
    # Can be $HOME/Desktop or $HOME/Bureau
    command = ["xdg-user-dir", "DESKTOP"]
    desktop_dir = subprocess.Popen(command, stdout=subprocess.PIPE).communicate()[0].strip('\n')
    desktop_file = os.path.join(desktop_dir, os.path.basename(copy_file_dest))
    # Copy / remove file from Desktop
    if install_desktop:
        shutil.copyfile(copy_file_dest, desktop_file)
        st = os.stat(desktop_file)
        os.chmod(desktop_file, st.st_mode | stat.S_IEXEC)
    elif uninstall_desktop:
        if os.path.exists(desktop_file):
            os.remove(desktop_file)


def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-i", "--install-desktop", dest='install_desktop',
                       action="store_true", help="Install menu and desktop entries.")
    group.add_argument("-u", "--uninstall-desktop", dest='uninstall_desktop',
                       action="store_true", help="Uninstall menu and desktop entries.")
    parser.add_argument("project_name", nargs="?", metavar="PROJECT NAME",
                        help="Project to launch or install")

    args = parser.parse_args()
    project_name = args.project_name

    if not project_name:
        project_name, project_gui_file = select_project(args.install_desktop,
                                                        args.uninstall_desktop)
    else:
        yamm_project_directory = os.path.join(yamm_src_dir, 'yamm',
                                              'projects', project_name)
        project_gui_file = os.path.join(yamm_project_directory, 'bin',
                                        project_name + '_gui.py')
        if not os.path.exists(yamm_project_directory):
            print('Project {0} not found'.format(project_name))
            print('Projects available: {0}'.format(' '.join(projects_with_gui)))
            project_name, project_gui_file = select_project(args.install_desktop,
                                                            args.uninstall_desktop)

        if not os.path.exists(project_gui_file):
            print('Project {0} has no GUI'.format(project_name))
            print('Projects available: {0}'.format(' '.join(projects_with_gui)))
            project_name, project_gui_file = select_project(args.install_desktop,
                                                            args.uninstall_desktop)

    if args.install_desktop:
        install_desktop(project_name)
    elif args.uninstall_desktop:
        uninstall_desktop(project_name)
    else:
        globalDict = {}
        globalDict['__file__'] = project_gui_file
        globalDict['__name__'] = '__main__'
        exec(compile(open(project_gui_file).read(), project_gui_file, 'exec'), globalDict)


if __name__ == "__main__":
    # Do not remove this line to keep main QApplication instance in the global
    # scope
    app = QtGui.QApplication.instance() or QtGui.QApplication(sys.argv)
    main()

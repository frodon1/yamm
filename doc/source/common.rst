.. |source_type| replace:: :ref:`source_type <ref_arch_src_options>`

Common features of all the projects
===================================

This section describes all the common features that all YAMM's projects have. Firstly,
common options are explained, then common commands. Finally, executor modes are described.

.. contents:: Table of contents
   :depth: 2
   :local:

.. _options_tables:

Common options
++++++++++++++

These tables explain common options available in all YAMM projects. Each option is describe with
its **Python** type, the application scope and a description.

There are three scopes like option's levels: global, category and software. An option that could be used
for the three levels has a scope defined like this: **[global, category, software]**. An option that could only be
applied to the software level has a scope defined like this: **[software]**.

.. _ref_repertory_options:

Repertory options
-----------------

====================================== ========= ============================ ======================================================= ====================================================
Option name                            Type      Scope                        Description                                             Default value
====================================== ========= ============================ ======================================================= ====================================================
top_directory                          string    [global]                     Top directory of project files                          Depends on the project type
archives_directory                     string    [global]                     Defines the directory where software's archives are     *top_directory*/archives
                                                                              downloaded. YAMM will not download an archive that is
                                                                              already in the archives directory.
version_directory                      string    [global]                     Top directory for software included in a project        *top_directory*/*version*
                                                                              version.
main_software_directory                string    [global, category, software] Top directory for a software. Source, build and         Depends on the project type
                                                                              installation directory are by default defined from this
                                                                              option.
====================================== ========= ============================ ======================================================= ====================================================

.. _ref_arch_src_options:

Archives and sources options
----------------------------

====================================== ========= ============================ ======================================================= ====================================================
Option name                            Type      Scope                        Description                                             Default value
====================================== ========= ============================ ======================================================= ====================================================
archives_download_tool                 string    [global, category, software] Defines the software used to download software's        Default is urlib.
                                                                              archives.
                                                                              Accepted values are: ``urlib`` (Python module), 
                                                                              ``wget`` or ``curl``.
archives_download_mode                 string    [global, category, software] Defines the update mode to download software's          Default is update.
                                                                              archives.
                                                                              Accepted values are: ``force_update``, ``update`` or
                                                                              ``nothing``.
archive_remote_address                 string    [global, category, software] Defines an url where to find software's archives.       Depends on the project
source_type                            string    [global, category, software] Defines the source type of a software. It could be      Depends on the project
                                                                              an archive or a remote repository. Accepted values are:
                                                                              ``archive``, ``remote``.
binary_archive_topdir                  string    [global]                     Defines the directory where software binaries archives  *top_directory*/binary_archives
                                                                              are located (and created)
binary_archive_url                     string    [global, category, software] Defines the url containing the software binary archive. Depends on the project
                                                                              It's a template where *%(name)* is replaced by the
                                                                              installation software directory name.
====================================== ========= ============================ ======================================================= ====================================================

Compilation and main command options
------------------------------------

====================================== ========= ============================ ======================================================= ====================================================
Option name                            Type      Scope                        Description                                             Default value
====================================== ========= ============================ ======================================================= ====================================================
parallel_make                          string    [global]                     Defines the parallel compilation flag sended to make.   1
continue_on_failed                     bool      [global]                     Defines the behavior of the executor when a software    False
                                                                              execution failed. If True, executor continue its
                                                                              execution even if one or more software execution
                                                                              ended an error.
check_dependency_compilation           bool      [global]                     Some executor modes will process a software if some of  True
                                                                              its dependencies had been processed before. Eg: if
                                                                              HDF5 is re-installed, MEDFICHIER will be installed even
                                                                              if MEDFICHIER sources did not change. If False,
                                                                              MEDFICHIER is no re-installed.
clean_src_if_success                   bool      [global, category, software] Deletes sofware's source directory if the               False
                                                                              execution is successfull.
clean_build_if_success                 bool      [global, category, software] Delete software's build directory if the                False
                                                                              execution is correct.
====================================== ========= ============================ ======================================================= ====================================================

Software list management options
--------------------------------

====================================== ========= ============================ ======================================================= ====================================================
Option name                            Type      Scope                        Description                                             Default value
====================================== ========= ============================ ======================================================= ====================================================
software_add_dict                      dict      [global]                     Adds some software to the software version list. Each   Empty by default
                                                                              couple refers to a software name with a
                                                                              version.
software_remove_list                   list      [global]                     Removes some software from the software version list.   Empty by default
software_minimal_list                  list      [global]                     YAMM will try to remove the software that are not in    Empty by default
                                                                              the dependencies of the software defined by this
                                                                              option.
softwares_user_version                 dict      [global]                     Changes for each software defined the version by the    Empty by default
                                                                              new version defined by this option.
software_only_list                     list      [global]                     Removes all software that are not in this list.         Empty by default
====================================== ========= ============================ ======================================================= ====================================================

.. _ref_software_specific_options:

Software specific options
-------------------------

====================================== ========= ============================ ======================================================= ====================================================
Option name                            Type      Scope                        Description                                             Default value
====================================== ========= ============================ ======================================================= ====================================================
software_src_directory                 string    [software]                   Defines software source directory.                      Not defined by default
software_build_directory               string    [software]                   Defines software build directory.                       Not defined by default
software_install_directory             string    [software]                   Defines software install directory.                     Not defined by default
software_parallel_make                 string    [software]                   Defines software parallel compilation flag.             Not defined by default
software_repository_name               string    [software]                   Defines software remote repository for remote           Not defined by default
                                                                              software (see |source_type| option).
software_additional_src_files          dict      [software]                   Adds or replaces sources files                          Not defined by default
                                                                              in the source directory of a
                                                                              software. Each couple defines a source file and
                                                                              a destination file.
software_additional_config_options     string    [software]                   Adds configure options to a software. It is permitted   Not defined by default
                                                                              for software that used autoconf, cmake or alike.
software_additional_env                string    [software]                   Adds environment variables in the software              Not defined by default
                                                                              compilation environment. Full command has to be
                                                                              provided like: `` export TEST_ALL=1 ; ``
software_additional_env_file           string    [software]                   Adds an additional environment file that will be        Not defined by default
                                                                              processed in the
                                                                              software compilation environment.
software_reset_config_options          bool      [software]                   If True, resets config options provided by YAMM.        Not defined by default
====================================== ========= ============================ ======================================================= ====================================================

Misc options
------------

====================================== ========= ============================ ======================================================= ====================================================
Option name                            Type      Scope                        Description                                             Default value
====================================== ========= ============================ ======================================================= ====================================================
platform                               string    [global]                     Chooses the platform of compilation. Correct values     LINUX
                                                                              are: ``LINUX``, ``BSD``.
command_verbose_level                  int       [global]                     Defines the verbose level of commands                   Project object verbose level
log_directory                          string    [global]                     Defines the directory of the log files                  *top_directory*/logs
command_print_console                  bool      [global]                     Writes the logs of the command in the terminal          True
command_write_in_file                  bool      [global]                     Writes the logs of the command in a log file            True
command_files_basename                 string    [global]                     Defines the base name of the log files                  Not defined by default
default_executor_mode                  string    [global, category, software] Defines the executor mode when the project's command    Not defined by default
                                                                              executor_mode parameter is "default".
xterm_make                             bool      [global, category, software] Launches the compilation into an xterm terminal         False
                                                                              (experimental).
====================================== ========= ============================ ======================================================= ====================================================

.. _ref_start_command:

Start command
+++++++++++++

The start command is used to launch the build process of the whole project. This section describes how to use it.

.. automethod:: yamm.core.framework.project.FrameworkProject.start
   :noindex:

Other commands
++++++++++++++

The commands described in the next table are available for each type of projects using YAMM framework architecture.
Most of them are just aliases for the start command with an other executor mode.
Exact signatures of these commands are described in :ref:`ref_framework_project_class`.

=========================== ========================================================= ============================
Command name                Description                                               Options
=========================== ========================================================= ============================
nothing                     Identical to start with executor_mode = "nothing"         None
download                    Identical to start with executor_mode = "download"        None
make                        Identical to start with executor_mode =                   software_list, executor_mode
                            "download_and_build"
update                      Identical to start (kept only for compatibility)          software_list, update_mode
create_binary_archive       Identical to start with executor_mode =                   software_list
                            "create_binary_archive"
install_from_binary_archive Identical to start with executor_mode =                   None
                            "install_from_binary_archive"
print_configuration         Prints information from the current configuration         None
                            of the project.
=========================== ========================================================= ============================

.. YAMM documentation master file, created by
   sphinx-quickstart on Wed Jul  6 16:43:28 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

YAMM documentation contents
===========================

.. toctree::
   :maxdepth: 2

   yamm_principles
   common
   salome_project
   develop
   modules
   examples_tutorials

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`


.. _ref_project_contributor:

Project Contributor
===================

This section is dedicated to contributors. A contributor is a user who needs to add a version, a
software or a test suite to an existing project.

.. contents:: Table of contents
   :depth: 2
   :local:

.. _ref_project_contributor_version:

Adding a new version
++++++++++++++++++++

Definition
----------

A YAMM version defines a **self-consistent** set of software. In a version, each software
has to be added with a **specific** software's version number.

.. _ref_tutorial_creating_a_version:

Creating a version
------------------

To create a version, a contributor has to create a class that inherits from the framework class :py:class:`~yamm.core.framework.version.FrameworkVersion`.
Follow the next example to see the steps to create a YAMM version.

1. Import framework version class::

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    from yamm.core.framework.version import FramworkVersion

  .. note::

    Some projects could provide a specific version class.

2. You **must** define a module variable named ̀̀``version_name``. It defines version's class. It is used
   by the dynamic load system to add the version to the project catalog::

    version_name = "GNU_TOOLS"

3. You define your class and call the subclass **__init__** method. It is in this step that you defines
   the version name (second __init__ argument) that users will have to use::

    class GNU_TOOLS(FrameworkVersion):

      def __init__(self, name=version_name, verbose=0, flavour=""):
        FrameworkVersion.__init__(self, name, verbose, flavour)

4. You define version's software list into the :py:meth:`~yamm.core.framework.version.FrameworkVersion.configure_softwares` method.
   This method is automatically called during the object initialization. To add a software, you have to use the
   :py:meth:`~yamm.core.framework.version.FrameworkVersion.add_software`. This method has four arguments: 
   ``software_name``, ``version_number``, ``ordered_version_number``, ``group``. The two last arguments are optional.
   In this example, four softwares are added to the version::

    def configure_softwares(self):
      self.add_software("M4",       "1.4.9")
      self.add_software("AUTOCONF", "2.68")
      self.add_software("AUTOMAKE", "1.11.1")
      self.add_software("LIBTOOL",  "2.4")

The version is now defined. This is the full file::

  #!/usr/bin/env python
  # -*- coding: utf-8 -*-

  from yamm.core.framework.version import FrameworkVersion

  version_name = "GNU_TOOLS"

  class GNU_TOOLS(FrameworkVersion):

    def __init__(self, verbose=0):
      FrameworkVersion.__init__(self, "GNU_TOOLS", verbose)

    def configure_softwares(self):
      self.add_software("M4",       "1.4.9")
      self.add_software("AUTOCONF", "2.68")
      self.add_software("AUTOMAKE", "1.11.1")
      self.add_software("LIBTOOL",  "2.4")

Adding a version in a project
-----------------------------

There is two ways to add a version to a project: a static and a dynamic one.

- **Static**: You add new version's file into the project version directory. You also need
  to modify the file ̀̀``__init__.py`` in the project's version directory. The project uses this file
  to know all the version that it needs to load by default.

- **Dynamic**: Project's catalog content is dynamic. A YAMM project provides a method named :py:meth:`~yamm.core.framework.project.FrameworkProject.add_version_in_catalog`
  that permits to add in the YAMM user's file a new version. This functionality is useful to test new versions without modifying YAMM::

    # Adding a new version in a SALOME project
    # You can look at the full example in your YAMM source directory: src/yamm/tests/test_add_version_in_catalog.py
    salome_project.add_version_in_catalog("new_salome_version", "tests_files")

  .. note::

    Adding version's file into a directory is correct only if there is a ``__init__.py`` file in the version directory. Indeed,
    the directory has to be Python package directory.

Specific information for SALOME project
---------------------------------------

The SALOME project adds a new class :py:class:`~yamm.projects.salome.version.SalomeVersion`. This class adds two methods to the framework
version class. You **must** define in your SALOME version the method :py:meth:`~yamm.projects.salome.version.SalomeVersion.get_salome_tag`.

If you want to add your version file in the **static** way, you have to:

1. Add your file in the directory:``src/yamm.projects.salome/versions``.
2. Modify the file **__init__.py** in the directory:``src/yamm.projects.salome/versions`` in adding your new version Python module name.

.. _ref_project_contributor_software:

Adding a new software
+++++++++++++++++++++

Definition
----------

A YAMM software defines how to install a software from its sources. It defines all the steps from
where to get the software, how to compile and finally how to install it. It also provides information
about what has to be added in the project's environment(s) file(s).

Creating a software
-------------------

Like for a version, a software is defined in a class included in a Python module. You can only have one software per module (so per file).

The class goal is to define a set of attributes that will be used as parameters for executor tasks. These attributes matches to the
options of the different tasks. Note that some of theses attributes can be redefined by the user using project's options.

To be able to define these attributes, YAMM will call a set of methods in a predefined order.

This section is divided in three parts. In the first part, attributes are presented with their associated task (if the association exists). In
the second part, the methods that you need to implement are presented in the call order. Finally, two examples are given: a basic and a
more advanced one.

Part 1: Attributes
##################

This is a list of the attributes that can be used to defined the software. For each attribute, a related task/engine object
is given. If an option defines the attribute, it is also indicated. It also explains if the user or the contributor
as the priority for the attribute.

.. |compile_task| replace:: :ref:`CompileTask <ref_compile_task_description>`
.. |cr_bina_task| replace:: :ref:`CreateBinaryArchiveTask <ref_create_binary_archive_task_description>`
.. |in_bina_task| replace:: :ref:`InstallBinaryArchiveTask <ref_install_binary_archive_task_description>`
.. |decomre_task| replace:: :ref:`DecompressTask <ref_decompress_task_description>`
.. |get_rem_task| replace:: :ref:`GetRemoteTask <ref_get_remote_task_description>`
.. |remote_task|  replace:: :ref:`RemoteTask <ref_remote_task_description>`
.. |software_cfg| replace:: :ref:`SoftwareConfig <ref_software_configuration_object>`
.. |software_eng| replace:: :ref:`software engine <ref_sofware_engine_class_description>`

.. |src_dir_opt| replace:: :ref:`software_src_directory <ref_software_specific_options>`
.. |bui_dir_opt| replace:: :ref:`software_build_directory <ref_software_specific_options>`
.. |ins_dir_opt| replace:: :ref:`software_install_directory <ref_software_specific_options>`
.. |add_sof_opt| replace:: :ref:`software_additional_src_files <ref_software_specific_options>`
.. |mai_dir_opt| replace:: :ref:`main_software_diretory <ref_repertory_options>`
.. |bin_arc_opt| replace:: :ref:`binary_archive_url <ref_arch_src_options>`
.. |arc_rem_opt| replace:: :ref:`archive_remote_address <ref_arch_src_options>`
.. |par_mak_opt| replace:: :ref:`software_parallel_make <ref_software_specific_options>`
.. |src_typ_opt| replace:: :ref:`source_type <ref_arch_src_options>`
.. |sof_rep_opt| replace:: :ref:`software_repository_name <ref_software_specific_options>`
.. |add_enf_opt| replace:: :ref:`software_additional_env_file <ref_software_specific_options>`
.. |add_env_opt| replace:: :ref:`software_additional_env <ref_software_specific_options>`
.. |add_cfg_opt| replace:: :ref:`software_additional_config_options <ref_software_specific_options>`
.. |res_cfg_opt| replace:: :ref:`software_reset_config_options <ref_software_specific_options>`

=============================== ========= =================== ================== ============== ============= ===================================
Name                            Mandatory Default             Task/object        Project option User priority Description
=============================== ========= =================== ================== ============== ============= ===================================
compil_type                     yes       ""                  |compile_task|     none           none          See description in |compile_task|.
archive_file_name               yes       ""                  |software_cfg|     none           none          See description in |software_cfg|.
archive_type                    yes       ""                  |decomre_task|     none           none          See description in |decomre_task|.
depend_list                     no        []                  |compile_task|     none           none          See description in |compile_task|.
src_directory                   no        See in description  |software_cfg|     |src_dir_opt|  yes           See description in |software_cfg|.
                                                                                                              Default source directory is set to
                                                                                                              |mai_dir_opt|/src/``executor_software_name``.
build_directory                 no        See in description  |software_cfg|     |bui_dir_opt|  yes           See description in |software_cfg|.
                                                                                                              Default build directory is set to
                                                                                                              |mai_dir_opt|/build/``executor_software_name``.
install_directory               no        See in description  |software_cfg|     |ins_dir_opt|  yes           See description in |software_cfg|.
                                                                                                              Default install directory is set to
                                                                                                              |mai_dir_opt|/install/``executor_software_name``.
additional_src_files            no        []                  |compile_task|     |add_sof_opt|  yes           See description in |compile_task|.
                                                                                                              It appends to the attribute the files
                                                                                                              provided by the user. But if a same 
                                                                                                              filename is provided, it is the user
                                                                                                              file which is kept.
binary_archive_url              no        option or ""        |software_cfg|     |bin_arc_opt|  yes           See description in |software_cfg|.
archive_address                 no        option or ""        |get_rem_task|     |arc_rem_opt|  yes           See description in |arc_rem_opt|.
parallel_make                   no        option or ""        |software_cfg|     |par_mak_opt|  no            See description in |par_mak_opt|.
software_source_type            no        option or ""        |software_cfg|     |src_typ_opt|  no            Choose what software config class
                                                                                                              is created. See more information
                                                                                                              in |software_eng|.
executor_software_name          no        See in description  none               none           none          Defines the software name
                                                                                                              according to a YAMM policy. It's
                                                                                                              the software name with the first
                                                                                                              letter in capital with the version
                                                                                                              number. Eg: HDF5 1.8.5 -> Hdf5-185.
software_dependency_list        no        []                  none               none           none          Contains the list of dependency
                                                                                                              object. It is created using
                                                                                                              the method :py:meth:`~yamm.core.framework.software.FrameworkSoftware.get_dependency_object_for`.
archive_download_mode           no        "update"            |get_rem_task|     none           none          See description of option ``mode`` in |get_rem_task|.
remote_type                     no        ""                  |remote_task|      none           none          See description in |remote_task|.
repository_name                 no        ""                  |remote_task|      |sof_rep_opt|  yes           See description in |remote_task|.
root_repository                 no        ""                  |remote_task|      none           none          See description in |remote_task|.
tag                             no        ""                  |remote_task|      none           none          See description in |remote_task|.
env_files                       no        []                  |compile_task|     |add_enf_opt|  yes           See description in |compile_task|.
                                                                                                              Users can only add new files and not
                                                                                                              remove files from the list.
user_dependency_command         no        ""                  |compile_task|     |add_env_opt|  yes           See description in |compile_task|.
                                                                                                              Users can only add new commands and not
                                                                                                              remove commands from the string.
egg_file                        no        ""                  |compile_task|     none           none          See description in |compile_task|.
pro_file                        no        ""                  |compile_task|     none           none          See description in |compile_task|.
gen_file                        no        ""                  |compile_task|     none           none          See description in |compile_task|.
config_options                  no        ""                  |compile_task|     |add_cfg_opt|  yes           See description in |compile_task|. An
                                                                                                              user adds new options with |add_cfg_opt|. It
                                                                                                              resets software's options with |res_cfg_opt|.
make_repeat                     no        1                   |compile_task|     none           none          See description in |compile_task|.
pre_configure_commands          no        []                  |compile_task|     none           none          See description in |compile_task|.
gen_commands                    no        []                  |compile_task|     none           none          See description in |compile_task|.
post_configure_commands         no        []                  |compile_task|     none           none          See description in |compile_task|.
pre_build_commands              no        []                  |compile_task|     none           none          See description in |compile_task|.
post_build_commands             no        []                  |compile_task|     none           none          See description in |compile_task|.
pre_install_commands            no        []                  |compile_task|     none           none          See description in |compile_task|.
post_install_commands           no        []                  |compile_task|     none           none          See description in |compile_task|.
specific_configure_command      no        ""                  |compile_task|     none           none          See description in |compile_task|.
specific_build_command          no        ""                  |compile_task|     none           none          See description in |compile_task|.
specific_install_command        no        ""                  |compile_task|     none           none          See description in |compile_task|.
src_dir                         no        "keep"              |compile_task|     none           none          See description in |compile_task|.
build_dir                       no        "delete_and_create" |compile_task|     none           none          See description in |compile_task|.
install_dir                     no        "delete"            |compile_task|     none           none          See description in |compile_task|.
disable_configure_generation    no        "no"                |compile_task|     none           none          See description in |compile_task|.
disable_configure               no        "no"                |compile_task|     none           none          See description in |compile_task|.
make_movable_archive_commands   no        []                  |cr_bina_task|     none           none          See description in |cr_bina_task|.
unmake_movable_archive_commands no        []                  |in_bina_task|     none           none          See description in |in_bina_task|.
remote_task_src_dir_policy      no        "keep"              |remote_task|      none           none          See description of option ``src_dir`` in |remote_task|.
tips                            no        ""                  none               none           none          Adds a string in the software information.
can_delete_src                  no        True                |software_cfg|     none           none          See description in |software_cfg|.
=============================== ========= =================== ================== ============== ============= ===================================

Part 2: Methods
###############

The software class calls a set of methods that the user can/need to implement. Each method is explained within the call order.
Except for the first method :py:meth:`~yamm.core.framework.software.FrameworkSoftware.init_dependency_list`, all other
methods are called during the execution of method :py:meth:`~yamm.core.framework.software.FrameworkSoftware.set_command_env`.

1. :py:meth:`~yamm.core.framework.software.FrameworkSoftware.init_dependency_list`: Called during the object
   :py:meth:`__init__() <yamm.core.framework.software.FrameworkSoftware>`. In
   this method, you have to define the attribute **software_dependency_list**.

2. :py:meth:`~yamm.core.framework.software.FrameworkSoftware.get_dependency_object_for`: For each software contained in
   the attribute software_dependency_list, YAMM checks if the software is executed by the current command. If
   the check is true, it uses this method to get the dependency object (see section
   :ref:`dependency object <ref_software_dependency_object>`) for the software.

3. :py:meth:`~yamm.core.framework.software.FrameworkSoftware.update_configuration_with_dependency`: For each dependency
   found, this method is called to enable the software to add or remove variables depending of the dependency.

4. The software calculates some default variables names like **executor_software_name**.

5. :py:meth:`~yamm.core.framework.software.FrameworkSoftware.init_variables`: This is the main method of the class.
   It's the method where variables have to be set if they have not been set before.

6. The software adds user's values for variables with user priority.

Part 3: Examples
################

These two examples are extracted and adapted from the example project **compilers** available in the YAMM sources.

.. _ref_basic_add_software:

Adding a simple software
________________________

To create a software, a contributor has to create a class that inherits from the framework class :py:class:`~yamm.core.framework.software.FrameworkSoftware`.
In this example, a software class is created to install GNU **M4** script language.

1. Import framework version class::

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    from yamm.core.framework.software import FrameworkSoftware

  .. note::

    Some projects could provide a specific software class.

2. You **must** define a module variable named ̀̀``software_name``. It defines the software class name. It is used
   by the dynamic load system to add the software to the project catalog::

    software_name = "M4"

3. You define your class and call the subclass **__init__** method (optionnal if nothing special is done inside)::

    class M4(FrameworkSoftware):

      def __init__(self, name, version, verbose):
        FrameworkSoftware.__init__(self, name, version, verbose)

4. Since M4 does not depends on other software, you only have to define the method :py:meth:`~yamm.core.framework.software.FrameworkSoftware.init_variables`.
   In this method, the three mandatory variables are defined::

    def init_variables(self):
      self.archive_file_name    = "m4-" + self.version + ".tar.gz"
      self.archive_type         = "tar.gz"
      self.compil_type          = "autoconf"

  .. note::

    Other variables should be defined, depending of what the project defines. In this
    case, **archive_address** is defined by the project using the option |arc_rem_opt|.

.. _ref_advanced_add_software:

Adding a software with dependencies
___________________________________

Before following this example, please read the example :ref:`ref_basic_add_software`. In this example we add
a new software named automake that depends on the software autoconf.

1. Create class like in the :ref:`previous <ref_basic_add_software>` example::

    from yamm.core.base import misc
    from yamm.core.framework.software import FrameworkSoftware
    from yamm.core.engine.dependency import Dependency

    software_name = "AUTOMAKE"

    class AUTOMAKE(FrameworkSoftware):

      def __init__(self, name, version, verbose):
        FrameworkSoftware.__init__(self, name, version, verbose)

      def init_variables(self):
        self.compil_type          = "autoconf"
        self.archive_file_name    = "automake-" + self.version + ".tar.gz"
        self.archive_type         = "tar.gz"

        self.software_source_type = "archive" # Force archive mode

  .. note::

    We import dependency module since we have to create dependencies objects. We
    also fix the source_type of the software in setting variable
    **software_source_type** to **archive**.

2. Adds :py:meth:`~yamm.core.framework.software.FrameworkSoftware.init_dependency_list` method to declare the software dependency list::

    def init_dependency_list(self):
      self.software_dependency_list = ["AUTOCONF"]

3. Adds :py:meth:`~yamm.core.framework.software.FrameworkSoftware.get_dependency_object_for` method to create dependency object::

    def get_dependency_object_for(self, dependency_name):
      dependency_object = None
      if dependency_name == "AUTOCONF":
        dependency_object = Dependency(name="AUTOCONF",
                                       depend_of=["path"])
      return dependency_object


  .. note::

    YAMM will add autoconf to the **path** of the compilation execution of automake.

4. If autoconf is in the current command, we change the name of automake directories to assure co-coexistence of an automake
   using the autoconf from the system and an automake using the autoconf provided by YAMM. To do this,
   we use the method :py:meth:`~yamm.core.framework.software.FrameworkSoftware.update_configuration_with_dependency`::

    def update_configuration_with_dependency(self, dependency_name, version_name):
      if dependency_name == "AUTOCONF":
        self.executor_software_name += "-au" + misc.transform(version_name)

.. _ref_create_movable_software:

Create a movable software
-------------------------

To create a movable archive, YAMM uses the task :py:class:`~yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask`. This task creates an archive
from the installed software, ready to be deployed on any other machine in any repository.

Usually, when a software is compiled on a machine, it is not prepared to be deployed "as-is" on another machine:

- additional files are created by YAMM
- some binary python files are generated
- some files contain hard-coded paths

A specific work must therefore be done to make the software archive movable:

1. Some additional and unwanted files are all automatically removed within the task :py:class:`~yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask`:

   - .pyc files
   - CVS ans svn directories if they exist
   - .la files
   - .install_ok files (created by YAMM)
   - .binary_archive_ok files (created by YAMM)

2. Some other files need to be treated by the software itself.
   To get rid of the hard-coded paths into the files, there are two solutions:

   a. use environment variables (known at runtime) in the files
   b. replace the hard-coded values by specific keywords which will be treated  during the installation process (in archive mode).

In both ways, the files have to be treated when the binary archive is created. To achieve this, some generic methods are available:

- :py:meth:`~yamm.core.framework.software.FrameworkSoftware.replaceStringByTemplateInFile` : to replace a given string by a given keyword in a file (relatively to the install directory)
- :py:meth:`~yamm.core.framework.software.FrameworkSoftware.replaceStringInFile` : this is an alias of the previous method
- :py:meth:`~yamm.core.framework.software.FrameworkSoftware.replaceTemplateBySoftInstallPathInFile` : to replace a template by the new install path of a software in a file (relatively to the install directory)

This method fill the lists **make_movable_archive_commands** and **unmake_movable_archive_commands** of the software 
which are then treated by the tasks :py:class:`~yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask` 
and :py:class:`~yamm.core.engine.tasks.install_binary_archive.InstallBinaryArchiveTask`
The difficulty relies in the identification of the files which need to be treated.

Adding a software in a project
------------------------------

There is two ways to add a software in a project: a static and a dynamic.

- **Static**: You add the new software's file in the project software directory. You also need
  to modify the file ̀̀``__init__.py`` in the project's software directory. The project uses this file
  to know all the software that it needs to load by default.

- **Dynamic**: Project's catalog content is dynamic. A YAMM project provides a method named :py:meth:`~yamm.core.framework.project.FrameworkProject.add_software_in_catalog`
  that permits to add in the YAMM user's file a new software. This functionality is useful to test new software without modifying YAMM::

    # Adding a new software in a SALOME project
    # You can look at the full example in your YAMM source directory: src/yamm/tests/test_add_software_in_catalog.py
    salome_project.add_software_in_catalog("new_SalomeSoftware", "tests_files")

  .. note::

    Adding software's file into a directory is correct only if you add an ``__init__.py`` in the software directory. Indeed,
    the directory has to be Python package directory.

Specific information for SALOME project
---------------------------------------

The SALOME project adds a new class :py:class:`~yamm.projects.salome.software.SalomeSoftware`. 

This class adds new variables and new methods to the framework
software class.

If you want to add your software file in the **static** way, you have to:

1. Determine if your software is a ``prerequisite``, a ``tool`` or a ``module`` (see section :ref:`classification <ref_doc_project_user>`).
2. Add your file in the correct directory:``src/yamm.projects.salome/softwares/x``.
3. Modify the file **__init__.py** in the directory:``src/yamm.projects.salome/softwares/x`` in adding your new software Python module name.

SALOME new variables behaviors
##############################

SALOME project defines new :ref:`options <salome_options>` that defines new behaviors for a subset of the software variables.

- **cmake_compilation_modules** option enables the build of a subset of SALOME modules with cmake. In this case,
  **compil_type** is set to **cmake** and **gen_file** to **build_cmake** for these modules.

- **modules_debug_mode** option enables to compile SALOME modules in the debug mode. In this case,
  **--enable-debug** is added to **config_options** or **-DCMAKE_BUILD_TYPE=Debug** for **cmake** modules.
  If this option is not set, **autoconf** modules **config_options** is enriched with **--disable-debug --enable-production**.

Finally, it also adds a new optional variable named **module_gui_sort**. It defines an integer that defines
the place of the module in the GUI of SALOME. It is only useful if the software is a SALOME module and if
it provides a SALOME GUI interface.

SALOME specific methods
#######################

SALOME software class adds new methods that a software in the SALOME should define if necessary.

.. |get_typ_meth| replace:: :py:meth:`~yamm.projects.salome.software.SalomeSoftware.get_type`
.. |get_pre_meth| replace:: :py:meth:`~yamm.projects.salome.software.SalomeSoftware.get_prerequisite_str`
.. |get_mod_meth| replace:: :py:meth:`~yamm.projects.salome.software.SalomeSoftware.get_module_str`
.. |is_arte_meth| replace:: :py:meth:`~yamm.projects.salome.software.SalomeSoftware.isArtefact`
.. |has_gui_meth| replace:: :py:meth:`~yamm.projects.salome.software.SalomeSoftware.has_salome_module_gui`
.. |get_nam_meth| replace:: :py:meth:`~yamm.projects.salome.software.SalomeSoftware.get_module_name_for_appli`
.. |conf_occ_meth| replace:: :py:meth:`~yamm.projects.salome.software.SalomeSoftware.configure_occ_software`

================================ ========= ==================== ==========================================
Method name                      Mandatory Default return value Description
================================ ========= ==================== ==========================================
|get_typ_meth|                   yes       error                Returns the SALOME software type. Possible
                                                                choices: ``prerequisite``, ``module``,
                                                                ``tool``.
|get_pre_meth|                   no        ""                   If needed, returns a string to be added to
                                                                the SALOME prerequisite environment file.
|get_mod_meth|                   no        ""                   If needed, returns a string to be added to
                                                                the SALOME module environment file.
|is_arte_meth|                   no        False                Returns True, if the software is not real
                                                                software.
|has_gui_meth|                   no        False                Returns True if the software provides a
                                                                SALOME GUI interface.
|get_nam_meth|                   no        self.name            Returns the software module for the SALOME
                                                                **config_appli.xml** file.
|conf_occ_meth|                  no        ""                   Configure the root_repository and
                                                                repository_name attributes according to
                                                                the options of the project
================================ ========= ==================== ==========================================

Adding a software in the SALOME installer
#########################################

To add a software in the Salome installer, one must ensure that the binaries are portable, i.e. no hard-coded path
should appear in a file, no compiled Python file should remain, ... This is described in the section :ref:`ref_create_movable_software`.

In the SALOME project, we identified 4 different cases where a file should be modified. Each of them is given a method:

   - :py:meth:`~yamm.projects.salome.software.SalomeSoftware.replaceSelfInstallPath` : to automatically treat the install path of the current software
   - :py:meth:`~yamm.projects.salome.software.SalomeSoftware.replaceSoftwareInstallPath` : to automatically treat the install path of another software
   - :py:meth:`~yamm.projects.salome.software.SalomeSoftware.replacePythonHeaderInstallPath` : to automatically treat the python install path in the header of a Python file
   - :py:meth:`~yamm.projects.salome.software.SalomeSoftware.replaceModuleInstallPath` : to automatically treat the install path of a SALOME module

For example the compilation install path of the software OMNIORB is written into the file "bin/omniidl" and must be replaced during the installation
of the movable installer, the following line should be written in the **init_variables** method of the class:

.. code-block:: python

 def init_variables(self):
   # ...
   self.replaceSelfInstallPath("bin/omniidl")
   # ...

If in the same file the install path of another software like QT should be treated too, here is how to do:

.. code-block:: python

 def init_variables(self):
   # ...
   self.replaceSoftwareInstallPath("QT", "bin/omniidl")
   # ...

If in the same file, the install path of a Salome module like KERNEL should be treated too, here is how to do:

.. code-block:: python

 def init_variables(self):
   # ...
   self.replaceModuleInstallPath("KERNEL", "bin/omniidl")
   # ...

Finally if in the same file, the install path of python is written in the header, here is a method to write ``/usr/bin/env python`` instead:

.. code-block:: python

 def init_variables(self):
   # ...
   self.replacePythonHeaderInstallPath("bin/omniidl")
   # ...


When the SALOME installer is created, a post installation file is created (``salome_post_install.py``).
For each software, a variable __SOFTWARE_INSTALL_PATH__ is defined with the new install path of the software.
Then the list of commands ``make_movable_archive_commands`` is analyzed. If it contains a sed command, then
the following lines are written :

- The .template file is created if it does not exist (from the file concerned by the sed command)
- The .template file is copied to a .template.current file
- A sed command is created to replace :

  - the ``__INSTALL_PATH__`` keyword by the software new install path
  - the ``__SOFTWARE_INSTALL_PATH__`` keywords by the value of corresponding variable created above.
  - the ``MODULE_ROOT_DIR`` keywords by ``os.getenv("MODULE_ROOT_DIR")``.

- The .template.current is copied to the real file

The ``salome_post_install.py`` file is then used when a new virtual application is created with the script 
``create_appli_[salome_version].sh``.

Examples of adding software in SALOME
#####################################

These examples explains two software provided in the SALOME project.

.. _ref_example_add_pre_to_salome:

Adding a prerequisite to SALOME
_______________________________

This example explains how the software OMNIORB has been added to the SALOME project.

1. `OmniORB <http://omniorb.sourceforge.net/>`_ is a software developed outside the SALOME project. So it's a **prerequisite**.

   - A file omniorb.py is added in the SALOME prerequisite directory: ``src/yamm.projects.salome/softwares/prerequisites``.
   - **omniorb** is added to the file ``src/yamm.projects.salome/softwares/prerequisites/__init__.py`` to declare the module.

2. Import some YAMM modules::

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    #  Copyright (C) 2011 EDF R&D
    #
    #  This library is free software; you can redistribute it and/or
    #  modify it under the terms of the GNU General Public
    #  License as published by the Free Software Foundation; either
    #  version 2.1 of the License.
    #
    #  This library is distributed in the hope that it will be useful,
    #  but WITHOUT ANY WARRANTY; without even the implied warranty of
    #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    #  Lesser General Public License for more details.
    #
    #  You should have received a copy of the GNU Lesser General Public
    #  License along with this library; if not, write to the Free Software
    #  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
    #
    #  Author : André RIBES (EDF R&D)

    from yamm.projects.salome.software import SalomeSoftware
    from yamm.core.base import misc
    import os

 .. note::

    We import the software class of SALOME project **SalomeSoftware**. We also import the YAMM misc module
    that provides some useful methods.

3. Add software/class name::

    software_name = "OMNIORB"

4. Add a template string that will be used to add some lines in the SALOME prerequisite file::

    omniorb_template = """
    #------ omniORB ------
    export OMNIORBDIR=%install_dir
    export PATH=${OMNIORBDIR}/bin:$PATH
    export %ld_library_path=$OMNIORBDIR/lib:$%ld_library_path
    """
    omniorb_template = misc.PercentTemplate(omniorb_template)

 .. note::

    SALOME project provides a set of variables that can be easily added to these lines. There
    are explained after.

5. Add the class with the four main methods::

    class OMNIORB(SalomeSoftware):

      def __init__(self, name, version, verbose):
        SalomeSoftware.__init__(self, "OMNIORB", version, verbose)

      def init_variables(self):
        self.archive_file_name = "omniORB-" + self.version + ".tar.gz"
        self.archive_type      = "tar.gz"
        self.compil_type       = "autoconf"
        self.config_options    = "--disable-ipv6"

        self.replaceSelfInstallPath("bin/omniidl")
        self.replacePythonHeaderInstallPath("bin/omniidl")

      def init_dependency_list(self):
        self.software_dependency_list = ["PYTHON"]

      def get_dependency_object_for(self, dependency_name):
        dependency_object = SalomeSoftware.get_dependency_object_for(dependency_name)
        if dependency_name == "PYTHON":
          dependency_object.depend_of=["path", "ld_lib_path"]
        return dependency_object

      def update_configuration_with_dependency(self, dependency_name, version_name):
        if dependency_name == "PYTHON":
          self.executor_software_name += "-py" + misc.transform(version_name)
          self.replacePythonHeaderInstallPath("bin/omniidl")

 .. note::

    Notice to usage of methods related to the management of hard-coded path.

6. Add the mandatory software method :py:meth:`~yamm.projects.salome.software.SalomeSoftware.get_type`::

      def get_type(self):
        return "prerequisite"

7. Add a string to the SALOME prerequisite environment file::

      def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
          install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return omniorb_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

 .. note::

    In this example, there are two variables to find: ``install_dir`` and ``ld_library_path``. For ``install_dir``,
    if the argument ``specific_install_dir`` is not empty you have to use it. In the other case, you have
    to use your **install_directory** attribute. For ld_library_path, you must use the method **get_ld_library_path()**
    of the misc module to known the system related environment variable name. If you need the Python version with
    which the software is being compiled you could use the attribute **python_version**.

.. _ref_example_add_mod_to_salome:

Adding a module to SALOME
_________________________

Please read the previous example before reading this one. This example shows how
to add a module in SALOME. It focuses on some specific points that you should know. In this example,
JOBMANAGER module file is described.

1. Like the previous example, a file is added to YAMM. In this case, since the JOBMANAGER is a SALOME module,
   a file is added in the directory ``src/yamm.projects.salome/softwares/modules`` and the ``__init__.py`` is also modified.

2. Add the class::

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    #  Copyright (C) 2011 EDF R&D
    #
    #  This library is free software; you can redistribute it and/or
    #  modify it under the terms of the GNU General Public
    #  License as published by the Free Software Foundation; either
    #  version 2.1 of the License.
    #
    #  This library is distributed in the hope that it will be useful,
    #  but WITHOUT ANY WARRANTY; without even the implied warranty of
    #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    #  Lesser General Public License for more details.
    #
    #  You should have received a copy of the GNU Lesser General Public
    #  License along with this library; if not, write to the Free Software
    #  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
    #
    #  Author : André RIBES (EDF R&D)

    from yamm.projects.salome.software import SalomeSoftware
    from yamm.core.base import misc
    import string
    import os

    software_name = "JOBMANAGER"
    class JOBMANAGER(SalomeSoftware):

      def __init__(self, name, version, verbose):
        SalomeSoftware.__init__(self, "JOBMANAGER", version, verbose)
        self.root_repository_template = string.Template(':pserver:$user@$server:2401/home/server/cvs/KERNEL')
        self.module_gui_sort = 7

 .. note::

    Since it is remote module, we add an attribute ``root_repository_template`` that will be used to calculate
    the value of attribute ``root_repository``. Since JOBMANAGER provides a SALOME GUI interface, attribute
    ``module_gui_sort`` is used.

3. Define :py:meth:`~yamm.core.framework.software.FrameworkSoftware.init_variables`::

    def init_variables(self):

      self.executor_software_name = self.name + "_" + self.version
      self.src_directory     = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "src",     self.executor_software_name)
      self.build_directory   = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "build",   self.executor_software_name)
      self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "install", self.executor_software_name)

      self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
      self.archive_type      = "tar.gz"

      self.remote_type     = "cvs"
      self.repository_name = "JOBMANAGER_SRC"
      self.root_repository = self.root_repository_template.substitute(user=self.project_options.get_option(self.name, "occ_username"), server=self.project_options.get_option(self.name, "occ_server"))
      self.tag             = self.version

      self.compil_type   = "autoconf"
      self.gen_commands += ["./clean_configure", "./build_configure "]

      self.replaceSelfInstallPath("bin/start_jobmanager.sh")

 .. note::

    You can notice that the name of the software is changed. We don't use default framework software class behavior for software name
    in the SALOME modules. Similarly, src, build and install directories are also redefined.

4. Add the other methods::

    def init_dependency_list(self):
      self.software_dependency_list = ["GUI"]

    def get_type(self):
      return "module"

    def has_salome_module_gui(self):
      return True

    def get_module_str(self, specific_install_dir=""):
      install_dir = self.install_directory
      if specific_install_dir != "":
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
      return "export " + self.name + "_ROOT_DIR=" + install_dir + "\n"

 .. note::

    You can notice the usage of the method :py:meth:`~yamm.projects.salome.software.SalomeSoftware.has_salome_module_gui` to
    enables the JOBMANAGER GUI in the main SALOME GUI.


Adding a new test suite
+++++++++++++++++++++++

Definition
----------

A YAMM test suite defines a set of tests that can be run automatically to check if a software or
a version works properly.

Types of test suites
--------------------

YAMM defines different types of test suites that can be easily used for a new project. Those types are:

- command_test_suite: The test suite is launched by the execution of a single command or script.
- MakeCheckTestSuite: The test suite is launched with the command "make check".
- MakeInstallcheckTestSuite: The test suite is launched with the command "make installcheck".
- make_test_suite: The test suite is launched with the command "make test".
- ScriptListTestSuite: The test suite is defined as a list of test scripts. The test scripts are
  run successively and each one produces a test log.

It is also possible to overload those test suite types to implement some specific functions. 

Creating a test suite
---------------------

A test suite is created by instanciating the class corresponding to the chosen type of test suite and
by defining some parameters. For instance, if your software can be tested with the "make check"
command, you can create the corresponding test suite with these two lines of code::

    from yamm.core.framework import test_suite
    make_check = test_suite.MakeCheckTestSuite(name="my software make check",
                                                  soft_name = "MY_SOFTWARE",
                                                  soft_version = "1.0.0")

The tests themselves can be either contained in the source of the software or be accessed through a
remote repository. In the latter case, it is mandatory to define where the repository is and how it is
accessed.

The parameters that can be defined for the test suite depend on its type. The following tables detail
those parameters.

Common parameters
#################

================== ======= ================== =====================================================
Name               Type    Mandatory          Description
================== ======= ================== =====================================================
name               string  yes                The name of the test suite, specified through the
                                              constructor of the class
version            string  no                 The version of the test suite, specified through the
                                              constructor of the class. If the test suite has
                                              versions, it is highly recommended to use the same
                                              version number as for the software.
soft_name          string  yes                The name of the software or the version associated
                                              with this test suite, specified through the
                                              constructor of the class
soft_version       string  no                 The version of the software associated with this test
                                              suite, specified through the constructor of the class
work_directory     string  only if associated The working directory that will be used to run the
                           with a version     test suite (default: build_directory of the software
                                              if associated with a software, None otherwise)
post_build         boolean no                 Indicate if the test suite must be run after the
                                              software is built (default: False). This parameter is
                                              only used if the test suite is associated with a
                                              software.
post_install       boolean no                 Indicate if the test suite must be run after the
                                              software or the version is installed (default: False)
post_move          boolean no                 Indicate if the test suite must be run after the
                                              software or the version is moved (not implemented
                                              yet)
insource           boolean no                 Indicate if the tests are contained in the source of
                                              the software (default: True)
src_directory      string  only if            Source directory where YAMM will copy the content of
                           insource is False  the remote test suite
remote_system_type string  only if            Type of the remote repository (cvs, svn, git or hg)
                           insource is False
repository_name    string  only if            Name of the remote repository
                           insource is False
root_repository    string  only if            Root of the remote repository
                           insource is False
================== ======= ================== =====================================================

Parameter for command test suites
#################################

================== ======= ================== =====================================================
Name               Type    Mandatory          Description
================== ======= ================== =====================================================
command            string  yes                The command to execute in order to run the test
                                              suite, specified through the constructor of the class
================== ======= ================== =====================================================

Parameters for script list test suites
######################################

================== ======= ================== =====================================================
Name               Type    Mandatory          Description
================== ======= ================== =====================================================
test_list          list    only if            This list of TestScript instances detail the tests
                           test_list_file is  that must be executed in this test suite. 
                           None               
test_list_file     string  only if test_list  This file must be a Python script and it must create
                           is None            a list named "test_list". This list replaces the
                                              parameter test_list.
================== ======= ================== =====================================================

Creating test scripts
#####################

The test scripts that are executed in script list test suites are instances of the class
TestScript. A test script has two parameters:

================== ======= ================== =====================================================
Name               Type    Mandatory          Description
================== ======= ================== =====================================================
script_file        string  yes                The file containing the script to execute
interpreter        string  no                 The interpreter to use to run the script. If not
                                              specified, the script must be executable.
================== ======= ================== =====================================================

Example::

    from yamm.core.framework.test_suite import TestScript
    my_TestScript = TestScript("my_test.py", "python")

YAMM can use any interpreter installed on the system to run the test scripts. If you reuse some
existing tests, you will probably have no choice for this interpreter. For new test suites, it is
highly recommended to use Python.

Associating a test suite with a software or a version
-----------------------------------------------------

Each software or version can have zero, one or several associated test suites. To associate test
suites with a software, you have to define the software attribute test_suite_list in the method
init_variables::

    from yamm.core.framework.software import FrameworkSoftware
    
    # Import necessary for the automated tests
    from yamm.core.framework import test_suite

    software_name = "MY_SOFTWARE"
    class MY_SOFTWARE(FrameworkSoftware):
    
      def init_variables(self):
    
        ...
    
        # Creation of the test suite
        make_check = test_suite.MakeCheckTestSuite(name="make check MY_SOFTWARE",
                                                      soft_name = self.name,
                                                      soft_version = self.version)
        self.test_suite_list = [make_check]

To associate test suites with a version, you have to define the version attribute test_suite_list
in the method configure_tests::

    from yamm.core.framework.version import FrameworkVersion

    # Import necessary for the automated tests
    from yamm.core.framework import test_suite
    
    version_name = "MY_VERSION"
    class MY_VERSION(FrameworkVersion):
      
      ...
      
      def configure_tests(self, project_options):
        autotests = test_suite.ScriptListTestSuite(name = "my autotests",
                                                      soft_name = self.name,
                                                      version = version_name)
        
        ...
        
        self.test_suite_list = [autotests]

Specific information for SALOME project
---------------------------------------

The SALOME project defines a new test suite type :py:class:`~yamm.projects.salome.test_suite.SalomeTestSuite`.
The test suite is defined as a list of test scripts. The test scripts are run in SALOME environment,
each in its own SALOME session.

The class SalomeTestSuite inherits ScriptListTestSuite. Therefore, the parameters test_list
and test_list_file are also used for SALOME test suites. Instead of a list of scripts, those
parameters define a list of SALOME test groups. The other specific parameter for SALOME test suites is:

================== ======= ================== =====================================================
Name               Type    Mandatory          Description
================== ======= ================== =====================================================
require_appli      boolean no                 Indicate if the test suite requires a running SALOME
                                              application (default: True)
================== ======= ================== =====================================================

SALOME test groups
##################

Each SALOME test group defines a list of test scripts. Each test group is run with its own SALOME
application. Thus this notion of group allows to restart SALOME between different tests (e.g. if
some tests require SALOME GUI and others can be run in terminal mode). A SALOME test group is
described with the following parameters:

================== ======= ================== =====================================================
Name               Type    Mandatory          Description
================== ======= ================== =====================================================
module_list        string  no                 List of modules included in the application used to
                                              run this group (comma-separated list)
use_gui            boolean no                 Indicate if the application used for this group must
                                              be run with a GUI (default: False)
tests              list    yes                This list of TestScript instances detail the tests
                                              that must be executed in this group. 
================== ======= ================== =====================================================

For example, a SALOME test group can be created with those lines::

    from yamm.core.framework.test_suite import TestScript
    from yamm.projects.salome.test_suite import SalomeTestGroup
    
    my_group = SalomeTestGroup(tests = [TestScript("my_test.py")], use_gui = True)

Examples
--------

This section contains a few examples that should cover most of test suites usages in YAMM.

Example 1: Software with "make check" and "make installcheck"
#############################################################

::

    from yamm.projects.salome.software import SalomeSoftware
    from yamm.core.framework import test_suite
    
    software_name = "OPENTURNS_TOOL"
    class OPENTURNS_TOOL(SalomeSoftware):
    
      def __init__(self, name, version, verbose):
        SalomeSoftware.__init__(self, name, version, verbose)
    
      def init_variables(self):
      
        ...

        make_check = test_suite.MakeCheckTestSuite(name="make check OPENTURNS_TOOL",
                                                      soft_name = self.name,
                                                      soft_version = self.version)
        make_installcheck = test_suite.MakeInstallcheckTestSuite(name="make install check OPENTURNS_TOOL",
                                                                    soft_name = self.name,
                                                                    soft_version = self.version)
        
        self.test_suite_list = [make_check, make_installcheck]

Example 2: SALOME software with a SALOME test suite
###################################################

::

    from yamm.projects.salome.software import SalomeSoftware
    
    # Imports necessary for the automated tests
    from yamm.core.framework.test_suite import TestScript
    from yamm.projects.salome.test_suite import SalomeTestSuite, SalomeTestGroup
    
    import os
    
    software_name = "HOMARD"
    class HOMARD(SalomeSoftware):
    
      def __init__(self, name, version, verbose):
        SalomeSoftware.__init__(self, name, version, verbose)
    
      def init_variables(self):
        self.executor_software_name = self.name + "_" + self.version
        self.src_directory     = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                              "src", self.executor_software_name)
        self.build_directory   = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                              "build", self.executor_software_name)
        self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                              "install", self.executor_software_name)
    
        ...
    
        # Creation of the test suite
        autotests = SalomeTestSuite(name = "module-homard",
                                      soft_name = self.name,
                                      soft_version = self.version,)
        autotests.post_install = True
        autotests.post_move = True
        test_directory = os.path.join(self.install_directory, "share/salome/resources/homard")
        test_group = SalomeTestGroup()
        test_group.tests.append(TestScript(os.path.join(test_directory, "test_1.py"), "python"))
        test_group.tests.append(TestScript(os.path.join(test_directory, "test_2.py"), "python"))
        test_group.tests.append(TestScript(os.path.join(test_directory, "test_3.py"), "python"))
        autotests.test_list = [test_group]
        autotests.require_appli = True
        self.test_suite_list = [autotests]

Example 3: SALOME version with a remote test suite
##################################################

::

    import os
    from yamm.projects.salome.version import SalomeVersion
    from yamm.projects.salome.test_suite import SalomeTestSuite
    
    version_name = "V6_6_0"
    class V6_6_0(SalomeVersion):
    
      def __init__(self, name=version_name, verbose=0, flavour=""):
        SalomeVersion.__init__(self, name, verbose, flavour)
    
      def get_salome_tag(self):
        return "V6_6_0"
    
      def get_salome_version(self):
        return "V6_6_0"
    
      def configure_softwares(self):
        ...
    
      def configure_tests(self, project_options):
        overhead = SalomeTestSuite(name = "salome-autotests-overhead",
                                     soft_name = self.name,
                                     version = version_name)
        overhead.insource = False
        overhead.src_directory = os.path.join(project_options.get_global_option("version_directory"),
                                              "test_src", self.name + "_" + overhead.name + "_" + overhead.version)
        overhead.work_directory = os.path.join(project_options.get_global_option("version_directory"),
                                               "test_workdir", self.name + "_" + overhead.name + "_" + overhead.version)
        overhead.post_install = True
        overhead.post_move = True
        overhead.remote_system_type = "git"
        overhead.repository_name = "salome-autotests-overhead.git"
        overhead.root_repository = "git://cli70rw.der.edf.fr/salome-autotests"
        overhead.test_list_file = os.path.join(overhead.src_directory, "test_list.py")
        overhead.require_appli = True
        self.test_suite_list = [overhead]

Base classes
============

This section provides information about the classes provided by YAMM in the base package.

.. _ref_core_base_options_class:

Base Options class
++++++++++++++++++

.. autoclass:: yamm.core.base.options.Options

User part
---------

.. automethod:: yamm.core.base.options.Options.set_global_option
.. automethod:: yamm.core.base.options.Options.set_category_option
.. automethod:: yamm.core.base.options.Options.set_software_option

Developer part
--------------

Options management
##################

.. automethod:: yamm.core.base.options.Options.add_option
.. automethod:: yamm.core.base.options.Options.add_category
.. automethod:: yamm.core.base.options.Options.add_software
.. automethod:: yamm.core.base.options.Options.is_category_option
.. automethod:: yamm.core.base.options.Options.is_software_option
.. automethod:: yamm.core.base.options.Options.has_option

Options value management
########################

.. automethod:: yamm.core.base.options.Options.get_option
.. automethod:: yamm.core.base.options.Options.get_global_option
.. automethod:: yamm.core.base.options.Options.get_category_option

Miscellaneous
#############

.. automethod:: yamm.core.base.options.Options.print_options

.. _ref_gui_framework_edit_project_window_class:

GUI Framework Edit Project Window class
=======================================

This is the framework for the edition window of a project. This windows allows the user
to define the options of the project. Projects can tweak this window to enable the user
to change some project specific parameters.

.. autoclass:: yamm.core.framework.gui.edit_project_window.FrameworkEditProjectWindow
   :members:
   :undoc-members:

.. _ref_gui_framework_string_option_class:

GUI Framework String Option class
+++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.StringOption
   :members:
   :undoc-members:

.. _ref_gui_framework_int_option_class:

GUI Framework Int Option class
++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.IntOption
   :members:
   :undoc-members:

.. _ref_gui_framework_string_range_option_class:

GUI Framework String Range Option class
+++++++++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.StringRangeOption
   :members:
   :undoc-members:

.. _ref_gui_framework_combo_option_class:

GUI Framework Combo Option class
++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.ComboOption
   :members:
   :undoc-members:

.. _ref_gui_framework_combo_range_option_class:

GUI Framework Combo Range Option class
++++++++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.ComboRangeOption
   :members:
   :undoc-members:

.. _ref_gui_framework_add_option_class:

GUI Framework Add Option class
++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.AddOption
   :members:
   :undoc-members:

.. _ref_gui_framework_bool_option_class:

GUI Framework Bool Option class
+++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.BoolOption
   :members:
   :undoc-members:

.. _ref_gui_framework_bool_range_option_class:

GUI Framework Bool Range Option class
+++++++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.BoolRangeOption
   :members:
   :undoc-members:

.. _ref_gui_framework_list_option_class:

GUI Framework List Option class
+++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.ListOption
   :members:
   :undoc-members:

.. _ref_gui_framework_list_range_option_class:

GUI Framework List Range Option class
+++++++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.ListRangeOption
   :members:
   :undoc-members:

.. _ref_gui_framework_dict_option_class:

GUI Framework Dict Option class
+++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.edit_project_window.DictOption
   :members:
   :undoc-members:
   
   

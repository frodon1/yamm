Engine classes
==============

This section provides information about the classes provided
by YAMM in the engine.

.. _ref_engine_executor_class:

Executor
++++++++

.. autoclass:: yamm.core.engine.executor.Executor

Configure part
--------------

.. automethod:: yamm.core.engine.executor.Executor.set_global_verbose_level
.. automethod:: yamm.core.engine.executor.Executor.add_software
.. automethod:: yamm.core.engine.executor.Executor.add_software_already_done
.. automethod:: yamm.core.engine.executor.Executor.add_hooks

User part
---------

.. automethod:: yamm.core.engine.executor.Executor.execute
.. automethod:: yamm.core.engine.executor.Executor.clean_executor

Internal part
-------------

Print part
##########

.. automethod:: yamm.core.engine.executor.Executor.print_start
.. automethod:: yamm.core.engine.executor.Executor.print_start_software
.. automethod:: yamm.core.engine.executor.Executor.print_end_software

Software part
#############

.. automethod:: yamm.core.engine.executor.Executor.get_software_install_directory
.. automethod:: yamm.core.engine.executor.Executor.get_software_python_version
.. automethod:: yamm.core.engine.executor.Executor.execution_python_version
.. automethod:: yamm.core.engine.executor.Executor.get_software_dependency_type

Miscellaneous
#############

.. automethod:: yamm.core.engine.executor.Executor.sort_handled_softwares
.. automethod:: yamm.core.engine.executor.Executor.insert
.. automethod:: yamm.core.engine.executor.Executor.clean_software_directories

.. _ref_engine_config_class:

Executor configuration
++++++++++++++++++++++

.. autoclass:: yamm.core.engine.config.CompilEnv

Methods
-------

.. automethod:: yamm.core.engine.config.CompilEnv.set_main_topdir
.. automethod:: yamm.core.engine.config.CompilEnv.check_abs
.. automethod:: yamm.core.engine.config.CompilEnv.copy_env
.. automethod:: yamm.core.engine.config.CompilEnv.print_env

.. _ref_engine_software_config_class:

Software configuration
++++++++++++++++++++++

.. autoclass:: yamm.core.engine.config.SoftwareConfig

Methods
-------

.. automethod:: yamm.core.engine.config.SoftwareConfig.merge_with_config
.. automethod:: yamm.core.engine.config.SoftwareConfig.print_execution_config

.. _ref_engine_archive_software_config_class:

Archive Software configuration
++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.engine.config.ArchiveSoftwareConfig

.. _ref_engine_software_class:

Software
++++++++

.. autoclass:: yamm.core.engine.softwares.ExecutorSoftware

Methods
-------

.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.init_software_with_executor
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.init_tasks
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.check_args
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.set_verbose_level
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.get_dependency_command
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.get_cmake_dependency_command
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.prepare_work
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.execute
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.end_work
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.check_installation
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.check_installation_and_diff
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.check_binary_archive_installation
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.check_any_installation
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.install_from_binary_archive
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.post_install_config
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.download
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.create_binary_archive
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.compile
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.download_remote_test
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.run_post_build_tests
.. automethod:: yamm.core.engine.softwares.ExecutorSoftware.run_post_install_tests

.. _ref_engine_archive_software_class:

Archive Software
++++++++++++++++

.. autoclass:: yamm.core.engine.softwares.ArchiveSoftware

Methods
-------

.. automethod:: yamm.core.engine.softwares.ArchiveSoftware.check_args
.. automethod:: yamm.core.engine.softwares.ArchiveSoftware.download
.. automethod:: yamm.core.engine.softwares.ArchiveSoftware.compile
.. automethod:: yamm.core.engine.softwares.ArchiveSoftware.decompress

.. _ref_engine_remote_software_class:

Remote Software
+++++++++++++++

.. autoclass:: yamm.core.engine.softwares.RemoteSoftware

Methods
-------

.. automethod:: yamm.core.engine.softwares.RemoteSoftware.check_args
.. automethod:: yamm.core.engine.softwares.RemoteSoftware.download
.. automethod:: yamm.core.engine.softwares.RemoteSoftware.check_installation_and_diff

.. _ref_engine_dependency_class:

Dependency class
++++++++++++++++

.. autoclass:: yamm.core.engine.dependency.Dependency

Methods
-------

.. automethod:: yamm.core.engine.dependency.Dependency.check_args
.. automethod:: yamm.core.engine.dependency.Dependency.get_dependency_types
.. automethod:: yamm.core.engine.dependency.Dependency.merge
.. automethod:: yamm.core.engine.dependency.Dependency.get_dependency_env
.. automethod:: yamm.core.engine.dependency.Dependency.get_cmake_dependency_env

.. _ref_engine_get_remote_task_class:

Get Remote Task
+++++++++++++++

.. autoclass:: yamm.core.engine.tasks.get_remote.GetRemoteTask

Methods
-------

.. automethod:: yamm.core.engine.tasks.get_remote.GetRemoteTask.check_args
.. automethod:: yamm.core.engine.tasks.get_remote.GetRemoteTask.prepare_work
.. automethod:: yamm.core.engine.tasks.get_remote.GetRemoteTask.execute
.. automethod:: yamm.core.engine.tasks.get_remote.GetRemoteTask.download

.. _ref_engine_remote_task_class:

Remote Task
+++++++++++

.. autoclass:: yamm.core.engine.tasks.remote.RemoteTask

Methods
-------

.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.check_args
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.prepare_work
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.set_software_name
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.delete_install_check_file
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.execute
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.execute_cvs
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.execute_svn
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.execute_git
.. automethod:: yamm.core.engine.tasks.remote.RemoteTask.execute_hg

.. _ref_engine_decompress_task_class:

Decompress Task
+++++++++++++++

.. autoclass:: yamm.core.engine.tasks.decompress.DecompressTask

Methods
-------

.. automethod:: yamm.core.engine.tasks.decompress.DecompressTask.check_args
.. automethod:: yamm.core.engine.tasks.decompress.DecompressTask.prepare_work
.. automethod:: yamm.core.engine.tasks.decompress.DecompressTask.execute

.. _ref_engine_compile_task_class:

Compile Task
++++++++++++

.. autoclass:: yamm.core.engine.tasks.compilation.CompileTask

Methods
-------

.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.check_args
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.set_software_name
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.prepare_work
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.find_python_version
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_autoconf
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_qmake
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_cmake
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_python
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_egg
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_specific
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_configure_with_space
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_make
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_rsync
.. automethod:: yamm.core.engine.tasks.compilation.CompileTask.execute_fake

.. _ref_engine_create_binary_archive_task_class:

Create Binary Archive Task
++++++++++++++++++++++++++

.. autoclass:: yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask

Methods
-------

.. automethod:: yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask.set_software_name
.. automethod:: yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask.prepare_work
.. automethod:: yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask.execute
.. automethod:: yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask.check_binary_archive_already_done

.. _ref_engine_install_binary_archive_task_class:

Install Binary Archive Task
+++++++++++++++++++++++++++

.. autoclass:: yamm.core.engine.tasks.install_binary_archive.InstallBinaryArchiveTask

Methods
-------

.. automethod:: yamm.core.engine.tasks.install_binary_archive.InstallBinaryArchiveTask.setRunPostInstallCommands
.. automethod:: yamm.core.engine.tasks.install_binary_archive.InstallBinaryArchiveTask.prepare_work
.. automethod:: yamm.core.engine.tasks.install_binary_archive.InstallBinaryArchiveTask.execute

.. _ref_engine_post_install_config_task_class:

Post Install Config Task
++++++++++++++++++++++++

.. autoclass:: yamm.core.engine.tasks.post_install_config.PostInstallConfigTask

Methods
-------

.. automethod:: yamm.core.engine.tasks.post_install_config.PostInstallConfigTask.prepare_work
.. automethod:: yamm.core.engine.tasks.post_install_config.PostInstallConfigTask.execute

.. _ref_engine_run_test_suite_class:

Run Test Suite
++++++++++++++

.. autoclass:: yamm.core.engine.tasks.run_test_suite.RunTestSuite

Methods
-------

.. automethod:: yamm.core.engine.tasks.run_test_suite.RunTestSuite.prepare_work
.. automethod:: yamm.core.engine.tasks.run_test_suite.RunTestSuite.execute

.. _ref_engine_additional_file_class:

Additional File
+++++++++++++++

.. autoclass:: yamm.core.engine.tasks.compilation.AdditionalFile


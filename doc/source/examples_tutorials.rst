Examples and Tutorials
======================

Examples
++++++++

- :ref:`build_a_salome_version_in_archive_mode`.
- :ref:`ref_basic_add_software`
- :ref:`ref_advanced_add_software`
- :ref:`ref_example_add_pre_to_salome`
- :ref:`ref_example_add_mod_to_salome`
- :ref:`Creating a project version <ref_tutorial_creating_a_version>`.

Tutorials
+++++++++

- :ref:`ref_salome_project_gui_tutorial`.

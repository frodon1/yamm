Framework classes
=================

This section provides information about the classes provided by YAMM in the framework.
These classes are used to create projects.

.. _ref_framework_yamm_command_decorator:

Yamm_command decorator
++++++++++++++++++++++

.. autofunction:: yamm.core.framework.project.yamm_command

.. _ref_framework_project_class:

Framework Project class
+++++++++++++++++++++++

.. autoclass:: yamm.core.framework.project.FrameworkProject

User part
---------

Commands
########

.. automethod:: yamm.core.framework.project.FrameworkProject.nothing
.. automethod:: yamm.core.framework.project.FrameworkProject.download
.. automethod:: yamm.core.framework.project.FrameworkProject.make
.. automethod:: yamm.core.framework.project.FrameworkProject.start
.. automethod:: yamm.core.framework.project.FrameworkProject.update
.. automethod:: yamm.core.framework.project.FrameworkProject.create_binary_archive
.. automethod:: yamm.core.framework.project.FrameworkProject.install_from_binary_archive
.. automethod:: yamm.core.framework.project.FrameworkProject.print_configuration
.. automethod:: yamm.core.framework.project.FrameworkProject.run_tests
.. automethod:: yamm.core.framework.project.FrameworkProject.get_log_files
.. automethod:: yamm.core.framework.project.FrameworkProject.recover_install_directories
.. automethod:: yamm.core.framework.project.FrameworkProject.remove_backup_directories
.. automethod:: yamm.core.framework.project.FrameworkProject.delete_directories
.. automethod:: yamm.core.framework.project.FrameworkProject.create_software_source_tgz

Catalog management
##################

.. automethod:: yamm.core.framework.project.FrameworkProject.add_software_in_catalog
.. automethod:: yamm.core.framework.project.FrameworkProject.add_version_in_catalog
.. automethod:: yamm.core.framework.project.FrameworkProject.compare_with_version

Tasks management
################

.. automethod:: yamm.core.framework.project.FrameworkProject.get_version_custom_tasks
.. automethod:: yamm.core.framework.project.FrameworkProject.add_version_tasks

Options management
##################

.. automethod:: yamm.core.framework.project.FrameworkProject.set_global_option
.. automethod:: yamm.core.framework.project.FrameworkProject.set_category_option
.. automethod:: yamm.core.framework.project.FrameworkProject.set_software_option
.. automethod:: yamm.core.framework.project.FrameworkProject.get_option
.. automethod:: yamm.core.framework.project.FrameworkProject.get_global_option
.. automethod:: yamm.core.framework.project.FrameworkProject.get_category_option
.. automethod:: yamm.core.framework.project.FrameworkProject.print_options

Miscellaneous
#############

.. automethod:: yamm.core.framework.project.FrameworkProject.print_catalog
.. automethod:: yamm.core.framework.project.FrameworkProject.print_directories
.. automethod:: yamm.core.framework.project.FrameworkProject.print_general_configuration
.. automethod:: yamm.core.framework.project.FrameworkProject.print_compilation_configuration
.. automethod:: yamm.core.framework.project.FrameworkProject.print_version

Developer part
--------------

.. automethod:: yamm.core.framework.project.FrameworkProject.init_catalog
.. automethod:: yamm.core.framework.project.FrameworkProject.check_specific_configuration
.. automethod:: yamm.core.framework.project.FrameworkProject.update_configuration
.. automethod:: yamm.core.framework.project.FrameworkProject.software_only_list_hook
.. automethod:: yamm.core.framework.project.FrameworkProject.add_software_to_env_files_end_hook
.. automethod:: yamm.core.framework.project.FrameworkProject.create_env_files
.. automethod:: yamm.core.framework.project.FrameworkProject.get_env_files
.. automethod:: yamm.core.framework.project.FrameworkProject.update_env_files_hook
.. automethod:: yamm.core.framework.project.FrameworkProject.begin_command
.. automethod:: yamm.core.framework.project.FrameworkProject.end_command
.. automethod:: yamm.core.framework.project.FrameworkProject.execute_command
.. automethod:: yamm.core.framework.project.FrameworkProject.reset_command
.. automethod:: yamm.core.framework.project.FrameworkProject.get_installer_class

Internal part
-------------

.. automethod:: yamm.core.framework.project.FrameworkProject.init
.. automethod:: yamm.core.framework.project.FrameworkProject.define_options
.. automethod:: yamm.core.framework.project.FrameworkProject.set_version
.. automethod:: yamm.core.framework.project.FrameworkProject.check_configuration
.. automethod:: yamm.core.framework.project.FrameworkProject.check_version
.. automethod:: yamm.core.framework.project.FrameworkProject.set_default_options_values
.. automethod:: yamm.core.framework.project.FrameworkProject.remove_global_option_value
.. automethod:: yamm.core.framework.project.FrameworkProject.get_current_python_version
.. automethod:: yamm.core.framework.project.FrameworkProject.get_version_object
.. automethod:: yamm.core.framework.project.FrameworkProject.insert_sorted
.. automethod:: yamm.core.framework.project.FrameworkProject.check_generated_env_files

.. _ref_framework_catalog_class:

Framework Catalog class
+++++++++++++++++++++++

.. autoclass:: yamm.core.framework.catalog.FrameworkCatalog
   :members:
   :undoc-members:

.. _ref_framework_version_class:

Framework Version class
+++++++++++++++++++++++

.. autoclass:: yamm.core.framework.version.FrameworkVersion
   :members:
   :undoc-members:

.. _ref_framework_software_class:

Framework Software class
++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.software.FrameworkSoftware
   :members:
   :undoc-members:

.. _ref_framework_project_gui_class:

Framework ProjectGui class
++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.project_gui.FrameworkProjectGui
   :members:
   :undoc-members:
   

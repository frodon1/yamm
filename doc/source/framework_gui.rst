GUI Framework classes
=====================

This section provides information about the classes provided by YAMM in the GUI framework.
These classes are used to create the GUI of projects.

Contents:

.. toctree::
   :maxdepth: 2

   framework_gui_basic_wizard
   edit_project_window
   gui_project
   main_project_window

.. _ref_gui_framework_basic_wizard_class:

GUI Framework Basic Wizard class
================================

This is the framework for the basic wizard which helps creating a YAMM project.
This wizard is composed of 3 parts:

- An intro
- Some options
- The remotes 

.. autoclass:: yamm.core.framework.gui.basic_wizard.FrameworkBasicWizard
   :members:
   :undoc-members:

.. _ref_gui_framework_basic_intro_class:

GUI Framework Basic Intro class
+++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.basic_wizard.FrameworkBasicIntro
   :members:
   :undoc-members:

.. _ref_gui_framework_basic_options_class:

GUI Framework Basic Options class
+++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.basic_wizard.FrameworkBasicOptions
   :members:
   :undoc-members:

.. _ref_gui_framework_basic_remotes_class:

GUI Framework Basic Remotes class
+++++++++++++++++++++++++++++++++

.. autoclass:: yamm.core.framework.gui.basic_wizard.FrameworkBasicRemotes
   :members:
   :undoc-members:
   

.. _ref_framework_gui_gui_project_class:

GUI Framework Gui Project class
===============================

This is the framework for the persistence of the GUI project files. The class allows 
to load and save parameters of the Yamm GUI projects. 

.. autoclass:: yamm.core.framework.gui.gui_project.FrameworkYammGuiProject
   :members:
   :undoc-members:


.. _ref_framework_project_window_class:

GUI Framework Project Window class
==================================

This is the framework for the main window of YAMM GUI which allows the user to 
launch YAMM commands on the project.

The list of softwares and their version is displayed in a table. Each software can 
be selected to perform an update action on it. 

The common actions provided by the framework and available by all projects are:

- start (classic, offline mode or from scratch)
- download
- clean (src, build, install)

The actions commands can be only printed in the "Used Commands Logs" if the button
"Launch" is pressed to display "Print".

The terminal used to launch the commands can be chosen among a list, allowing the 
user to choose the terminal according to its desktop environment.

.. autoclass:: yamm.core.framework.gui.main_project_window.FrameworkProjectWindow
   :members:
   :undoc-members:


SALOME classes
==============

.. _ref_salome_project_class:

Project class
+++++++++++++

.. autoclass:: yamm.projects.salome.project.Project

User part
---------

.. automethod:: yamm.projects.salome.project.Project.create_env_files
.. automethod:: yamm.projects.salome.project.Project.create_appli
.. automethod:: yamm.projects.salome.project.Project.run_appli
.. automethod:: yamm.projects.salome.project.Project.create_movable_installer
.. automethod:: yamm.projects.salome.project.Project.create_software_source_tgz
.. automethod:: yamm.projects.salome.project.Project.create_modules_source_tgz
.. automethod:: yamm.projects.salome.project.Project.create_tools_source_tgz
.. automethod:: yamm.projects.salome.project.Project.delete_prerequisite_directories
.. automethod:: yamm.projects.salome.project.Project.delete_module_directories
.. automethod:: yamm.projects.salome.project.Project.delete_tool_directories

Internal part
-------------

.. automethod:: yamm.projects.salome.project.Project.print_directories
.. automethod:: yamm.projects.salome.project.Project.print_general_configuration
.. automethod:: yamm.projects.salome.project.Project.print_version

Version class
+++++++++++++

.. autoclass:: yamm.projects.salome.version.SalomeVersion
.. automethod:: yamm.projects.salome.version.SalomeVersion.get_salome_tag
.. automethod:: yamm.projects.salome.version.SalomeVersion.get_salome_version
.. automethod:: yamm.projects.salome.version.SalomeVersion.is_universal

Software class
++++++++++++++

.. autoclass:: yamm.projects.salome.software.SalomeSoftware
.. automethod:: yamm.projects.salome.software.SalomeSoftware.get_type
.. automethod:: yamm.projects.salome.software.SalomeSoftware.get_prerequisite_str
.. automethod:: yamm.projects.salome.software.SalomeSoftware.get_module_str
.. automethod:: yamm.projects.salome.software.SalomeSoftware.isArtefact
.. automethod:: yamm.projects.salome.software.SalomeSoftware.has_salome_module_gui
.. automethod:: yamm.projects.salome.software.SalomeSoftware.get_module_name_for_appli
.. automethod:: yamm.projects.salome.software.SalomeSoftware.configure_occ_software

YAMM architecture
=================

.. contents:: Table of contents
   :depth: 3
   :local:

.. _ref_engine_architecture:

Engine Architecture
+++++++++++++++++++

The engine is part of the core of YAMM. It provides a system to execute a project command. The main object
of the engine is the executor. It is created by a project command if it needs to execute specific tasks on
a list of software.

.. figure:: images/engine_graph.png

Executor
--------

The executor executes or processes each software according to the dependencies definitions of the software.
It also provides some default values defined in a configuration object that has to be provided
during the executor creation. These values are used by the software if they don't have an another
configuration object that overrides the executor configuration object values.

Executor configuration object
#############################

The configuration is an object of type :ref:`CompilEnv <ref_engine_config_class>`.

It provides these options defined in the object constructor:

============================ ================ ======= ====================================================================
Option name                  Type             Default Description
============================ ================ ======= ====================================================================
main_topdir                  string           ""      It is the main top directory. It must be defined with an absolute
                                                      path. It is defined by a YAMM project with the value of
                                                      option **top_directory**.
src_topdir                   string           ""      Defines the source top directory. If it is not defined, the value
                                                      will be **main_topdir**/**src**.
build_topdir                 string           ""      Defines the build top directory. If it is not defined, the value
                                                      will be **main_topdir**/**build**.
install_topdir               string           ""      Defines the install top directory. If it is not defined, the value
                                                      will be **main_topdir**/**install**.
binary_archive_topdir        string           none    Defines where binaries archives are stored. If it is not defined,
                                                      the value will be **main_topdir**/**binary_archives**.
python_version               string           ""      Defines the Python Version used during software executions.
parallel_make                string           ""      Defines the default parallel flag for make. This value is used
                                                      by the :ref:`CompileTask <ref_compile_task_description>`.
continue_on_failed           bool             False   If True, continues execution even if one or more software executions
                                                      failed.
check_dependency_compilation bool             True    If True, executes a new compilation if a dependency of the software
                                                      has been executed, even if the sources of the software has not
                                                      been updated.
============================ ================ ======= ====================================================================

.. _ref_sofware_engine_class_description:

Software
--------

For each **framework** (project) software to process an object **executor software** is associated to this software.
This object contains the **execution mode** and the specific parameters for each task that
will be executed. It also contains information about dependencies that could be required to
execute the tasks for the software. It's the executor's role to provide the required dependencies values for a software.

In the engine, when a software is added, it has to choose how the software's source are get. There is
three kinds of software: **basic**, **archive** and **remote**. A basic software is a software whose sources
are already provided in the source directory defined by its configuration. An archive software
is a software whose sources are provided by an archive file. Finally, a remote software is
a software whose sources are provided by a remote repository. We can resume with this list:

- Basic:   object of class :ref:`ExecutorSoftware <ref_engine_software_class>` and with a configuration object of type :ref:`SoftwareConfig <ref_engine_software_config_class>`.
- Remote:  object of class :ref:`RemoteSoftware <ref_engine_remote_software_class>` and with a configuration object of type :ref:`SoftwareConfig <ref_engine_software_config_class>`.
- Archive: object of class :ref:`ArchiveSoftware <ref_engine_archive_software_class>` and with a configuration object of type :ref:`ArchiveSoftwareConfig <ref_engine_archive_software_config_class>`.

An execution mode is composed by a list of pairs: a method provided by the engine software class and a
condition to continue. The description of the execution modes are described in this :ref:`section <ref_executor_modes>`.

The next table describes software class' methods used by the modes.

Methods
#######

=========================== ========================================================= ============================================================ ====================================
Method name                 Description                                               Result                                                       Related task
=========================== ========================================================= ============================================================ ====================================
nothing                     Does nothing.                                             Always successful.                                           None
download                    Downloads the software using the software configuration.  For an archive, success if it could get a software and       **GetRemoteTask** for archives
                            Depending of software's type, it downloads an archive     if the archive sha1sum success (it's optional). For a        and **RemoteTask** for repositories
                            file or it checkouts/updates a remote repository.         remote repository, success if the checkout is ok. If it's
                                                                                      an update, the method is always successful.
compile                     Compiles and installs the software.                       Success if the compilation and installation are ok.          **DecompressTask** for archives
                                                                                                                                                   software and **CompileTask**
check_installation          Checks if the software has a correct installation.        Success if the software is installed and if the dependencies None
                                                                                      has not been re-installed in the current command. the
                                                                                      dependency check could be avoided if the option
                                                                                      check_dependency_compilation is set to False for the
                                                                                      software.
check_installation_and_diff Like check_installation but checks if there is a diff     Failed if there is a diff between the local an the remote    None or **RemoteTask**
                            between the remote repository and the local               repository. Same behavior than check_installation in
                            repository.                                               the other cases.
create_binary_archive       Creates a binary archive from software installation       Success if binary archive creation succeed.                  **CreateBinaryArchiveTask**
install_from_binary_archive Installs a software from a binary archive.                Success if installation is ok.                               **InstallFromBinaryArchiveTask**
=========================== ========================================================= ============================================================ ====================================

.. _ref_software_configuration_object:

Software configuration object
#############################

The software configuration object configures some variables used by the software methods and the tasks.
Since many options are the same than options of the executor configuration object, the next table
shows only the new options:

============================= ================ ======= ====================================================================
Option name                   Type             Default Description
============================= ================ ======= ====================================================================
archive_file_name             string           ""      Defines the archive filename. It is used by DecompressTask and
                                                       GetRemoteTask. It's **mandatory** for archive configuration.
archive_dir                   string           ""      Defines where the archive is located. It is used by DecompressTask
                                                       and by GetRemoteTask. It's **mandatory** for archive
                                                       configuration.
src_directory                 string           ""      Defines the software source directory.
build_directory               string           ""      Defines the software build directory.
install_directory             string           ""      Defines the software install directory.
binary_archive_url            string           ""      Defines where the binary archive is. It is used by the
                                                       InstallBinaryArchiveTask.
clean_src_if_success          bool             False   If a software execution if successful, clean software source
                                                       directory.
clean_build_if_success        bool             False   If a software execution if successful, clean software build
                                                       directory.
can_delete_src                bool             True    If False, executor cannot delete the source directory.
============================= ================ ======= ====================================================================

.. _ref_software_dependency_object:

Software dependency object
##########################

Each dependency of the software is described with an object of class :ref:`dependency <ref_engine_dependency_class>`.
The next table shows the object class init variables:

============================= ================ ======= ====================================================================
Variable name                 Type             Default Description
============================= ================ ======= ====================================================================
name                          string           none    It's a **mandatory** variable. It defines the YAMM's dependency
                                                       software name.
depend_of                     list of strings  []      Defines what kind of compilation dependency the software has to the
                                                       targeted software. Possible choices are: ``pkg``, ``path``,
                                                       ``ld_lib_path``, ``install_path``, ``python_path``,
                                                       ``python_version``. According to this list, YAMM will add some
                                                       environment variables to the commands executed by the
                                                       :ref:`CompileTask <ref_compile_task_description>`.
install_path                  string           ""      Overloads the install path provided by the executor.
specific_pkg_path             string           ""      Overloads the pkg path provided by the executor.
specific_bin_path             string           ""      Overloads the bin path provided by the executor.
specific_lib_path             string           ""      Overloads the lib path provided by the executor.
specific_install_var_name     string           ""      Overloads the environment variable for dependency type
                                                       ``install_path``.
specific_install_var_end      string           ""      Adds at the end of environment variable ``install_path`` and
                                                       extra path.
python_version                string           ""      Overloads the python version provided by the executor.
============================= ================ ======= ====================================================================

Depending of the content of depend_of list, these variables will be added to the compilation environment:

- **pkg**: ``export PKG_CONFIG_PATH=...:$PKG_CONFIG_PATH``
- **path**: ``export PATH=...:$PATH``
- **ld_lib_path**: ``export LD_LIBRARY_PATH=...:$LD_LIBRARY_PATH``
- **install_path**: ``export software_name_INSTALL_DIR=...`` (could be changed with init variable specific_install_var_name).
- **python_path**: ``export PYTHONPATH=...:$PYTHONPATH``
- **python_version**: ``export PYTHON_VERSION=...``

Tasks
-----

============================ ======================================================================================================================
Task class name                  Description
============================ ======================================================================================================================
GetRemoteTask                Get an archive from an URL (local or remote). Three tools are supported: Python ``urlib`` (default), ``wget`` and ``curl``.
RemoteTask                   Checkout or update a remote repository. Currently ̀``CVS̀``, ̀``SVǸ``, ̀``GIT̀`` and ``HG`` are supported.
DecompressTask               Decompress an archive. Currently, ̀``tar.gz̀`` and ̀``tar.bz2̀`` are supported.
CompileTask                  Compile an install a software. This task supports a lot of type of compilation system: ``autoconf``, ``cmake``, ``qmake``, ``python``,
                             ``python_egg``, ``egg``, ``configure_with_space``, ``specific`` and ``fake``.
CreateBinaryArchiveTask      Create a binary archive from a software installation. It is possible to add specific commands to the task for
                             creating a movable binary archive.
InstallFromBinaryArchiveTask Install a software from a binary archive.
============================ ======================================================================================================================

This section provides information on the engine tasks. It lists all the arguments of each task that a
contributor main defines when it adds a new software description in YAMM.

.. _ref_get_remote_task_description:

GetRemote
#########

This task is implemented by the class :ref:`GetRemoteTask <ref_engine_get_remote_task_class>`

============= ================ ========= ======= ====================================================================
Option name   Type             Mandatory Default Description
============= ================ ========= ======= ====================================================================
remote_adress string           yes       none    Defines the software archive address. The format
                                                 depends on the option ``tool`` used. See the documentations
                                                 of these tools for the format.
tool          string in a list no        urllib  Defines the tool used to get the archive. Possible choices are:
                                                 ``urllib`` (module provided by Python), ``wget`` and ``curl``.
mode          string in a list no        update  Defines what the task has to do. Possible choices are:
                                                 ``update`` or ``nothing``. 

                                                 - Update mode downloads the software
                                                   archives and sha1 sum file if there are not already in the project
                                                   archives directory. It always check the archive if a sha1 sum file
                                                   is provided. If it is not present, a warning is printed.

                                                 - Like the meaning of word nothing, the command does nothing
                                                   in the mode nothing.
sha1tool      string in a list no        hashlib Defines the tool used to check the archive. Possible choices are:
                                                 ̀``hashlib`` (module provided by Python) and ̀``sha1sum``.
============= ================ ========= ======= ====================================================================

.. _ref_remote_task_description:

Remote
######

This task is implemented by the class :ref:`RemoteTask <ref_engine_remote_task_class>`

=============== ================ ========= ======= ====================================================================
Option name     Type             Mandatory Default Description
=============== ================ ========= ======= ====================================================================
remote_type     string in a list yes       none    Defines the type of the remote repository. Possible choices are:
                                                   ``cvs``, ``svn``, ``git`` and ``hg``.
repository_name string           yes       none    Defines the name of the repository to get in the root_repository.
root_repository string           yes       none    Defines the repository address. The format depends on the
                                                   chosen remote_type.
tag             string           no        ""      Defines a tag or a version to get. For git, you have to use
                                                   it to define a branch name or a tag name. For a tag name, you have
                                                   to start the name with the substring **git_tag/**. If not, tag
                                                   value will be interpreted as a git branch name.
src_dir         string in a list no        keep    Defines what to do with the current source directory. Possible
                                                   choices are: ``keep`` or ``delete``. if src_dir is set to delete,
                                                   the source directory will be deleted and a checkout will be done.
                                                   In the opposite, an update will be done.
=============== ================ ========= ======= ====================================================================

.. _ref_decompress_task_description:

Decompress
##########

This task is implemented by the class :ref:`DecompressTask <ref_engine_decompress_task_class>`

================ ================ ========= ======= ====================================================================
Option name      Type             Mandatory Default Description
================ ================ ========= ======= ====================================================================
archive_type     string in a list yes       none    Defines the archive type. Possible choices are: ``tar.gz``,
                                                    ``tar.bz2`` or ``tar.xz``
archive_dir_name string           no        ""      Defines the source directory name to delete.
src_dir          string in a list no        delete  Defines what to do with the current source directory. Possible
                                                    choices are: ``keep`` or ``delete``. if src_dir is set to delete,
                                                    the source directory will be deleted and a checkout will be done. It
                                                    uses the option archive_dir_name to compute the full directory
                                                    name.
                                                    In the opposite, an update will be done.
sha1_tool        string in a list no        hashlib Defines the tool used to check the archive. Possible choices are:
                                                 ̀   ``hashlib`` (module provided by Python) and ̀``sha1sum``.
================ ================ ========= ======= ====================================================================

.. _ref_compile_task_description:

Compile
#######

This task is implemented by the class :ref:`CompileTask <ref_engine_compile_task_class>`

============================ ================ ========= ================= ====================================================================
Option name                  Type             Mandatory Default           Description
============================ ================ ========= ================= ====================================================================
compil_type                  string in a list yes       none              Defines the compilation system used by the software. According to
                                                                          the type, not all the options are used. Possible choices are:
                                                                          ``autoconf``, ``cmake``, ``qmake``, ``python``, ``python_egg``,
                                                                          ``egg``, ``configure_with_space``, ``specific`` and ``fake``.
env_files                    list of strings  no        []                Adds environment files to the software compilation environment.
                                                                          Each file is sourced before dependencies variables and before
                                                                          **user_dependency_command**.
user_dependency_command      string           no        ""                Defines a shell command that is added at the end of all YAMM
                                                                          environment variable.
depend_list                  list of object   no        []                Contains all software's dependencies. Each member of the list is
                                                                          an object of type :ref:`dependency <ref_engine_dependency_class>`.
egg_file                     string           no        ""                Defines the egg filename for a software with a compil_type ``egg``.
pro_file                     string           no        ""                Defines the pro filename for a software with a compil_type
                                                                          ``qmake``.
gen_file                     string           no        ""                Defines a filename used to generate of the software
                                                                          configure file. Available for: ``autoconf``, ``cmake``,
                                                                          ``configure_with_space``.
config_options               string           no        ""                Adds new configure options. Available for: ``autoconf``, ``cmake``,
                                                                          ``python``, ``configure_with_space``.
make_repeat                  int              no        0                 Defines if YAMM must repeat the **make** command or not. It could be
                                                                          useful for software which have makefiles incorrect for
                                                                          parallel compilation.
additional_src_files         list of object   no        []                Add new files into the software source directory. Each file is
                                                                          described with an object of type
                                                                          :ref:`additional_file <ref_engine_additional_file_class>`.
pre_configure_commands       list of strings  no        []                Adds a list of commands before executing the configure. Available
                                                                          for: ``autoconf``, ``cmake``, ``qmake``, ``specific``,
                                                                          ``configure_with_space``.

gen_commands                 list of strings  no        []                Adds a list of commands whose purpose is to create the software
                                                                          configure script. Available for: ``autoconf``, ``configure_with_space``.

post_configure_commands      list of strings  no        []                Adds a list of commands executed after the configure. Currently
                                                                          not available.
pre_build_commands           list of strings  no        []                Adds a list of commands executed before the build. Available for: ``python``.
post_build_commands          list of strings  no        []                Adds a list of commands executed after the build. Currently no
                                                                          available.
pre_install_commands         list of strings  no        []                Adds a list of commands executed before the installation. Available for all.
post_install_commands        list of strings  no        []                Adds a list of commands executed after the installation. Available of all.
specific_configure_command   string           no        ""                Sets the configure command for a ``specific`` type software.
specific_build_command       string           no        ""                Sets the build command for a ``specific`` type software.
specific_install_command     string           no        ""                Sets the install command for a ``specific`` type software.
src_dir                      string in a list no        keep              Defines what to do with the software source directory. Possible
                                                                          choices are: ``delete_and_create``, ``keep_or_create``, 
                                                                          ``keep``, ``delete``.
build_dir                    string in a list no        delete_and_create Defines what to do with the software build directory. Possible
                                                                          choices are: ``delete_and_create``, ``keep_or_create``.
install_dir                  string in a list no        delete            Defines what to do with the software install directory. Possible
                                                                          choices are: ``delete``, ``keep``.
disable_configure_generation string in a list no        no                If set to **yes**, disables the generation of the configure.
disable_configure            string in a list no        no                If set to **yes**, disables the execution of the configure.
============================ ================ ========= ================= ====================================================================

The compil_type ``fake`` permits to add **fake** software into a project. This type of software only creates an empty install directory.

.. _ref_create_binary_archive_task_description:

Create binary archive
#####################

This task is implemented by the class :ref:`CreateBinaryArchiveTask <ref_engine_create_binary_archive_task_class>`.
There is only one option for this task named **make_movable_archive_commands**. It defines additional commands
that remove installation path in the software installation files. It is done before the task creates the binary archive
file. YAMM provides helpers methods to ease the use of this option. See section :ref:`ref_create_movable_software`
for more explanation.

.. make_movable_archive_commands string           ""      Defines additional commands for creating a binary archive of the
..                                                        software. It is used by the CreateBinaryArchiveTask.

.. _ref_install_binary_archive_task_description:

Install from binary archive
###########################

This task is implemented by the class :ref:`InstallBinaryArchiveTask <ref_engine_install_binary_archive_task_class>`.
It installs an archive file created by the CreateBinaryArchiveTask into the software installation directory.

Framework Architecture
++++++++++++++++++++++

The framework is the part of YAMM that offers a set of classes to create YAMM projects. It provides four classes:
``FrameworkProject``, ``FrameworkCatalog``, ``FrameworkVersion`` and ``FrameworkSoftware``.

.. figure:: images/framework_graph.png

FrameworkProject
----------------

A project is an object that provides for an user a set of commands to execute. It also provides
a catalog that contains the list of the software handled by the project and a list of project's versions.

A project is an object whose class inherits from this :ref:`FrameworkProject class <ref_framework_project_class>`.
The framework commands are described in the section :ref:`common commands <ref_framework_project_commands>`.

A project provides some options that are described in the section :ref:`common options <options_tables>`.

It also provides methods to add dynamically new versions and new software. These features are described
in the section :ref:`project contributor <ref_project_contributor>`.

FrameworkCatalog
----------------

The catalog contains all the versions and the software available for a project. It is an object of
class :ref:`FrameworkCatalog <ref_framework_catalog_class>`. Each project has an object catalog named **catalog**.

The class provides methods to add new version, software and some introspection methods too.

FrameworkVersion
----------------

A version describes a coherent set of software versions that defines a version of the platform. It's an
object of the class :ref:`FrameworkVersion <ref_framework_version_class>`.

The class provides methods to manage the list of softwares in the version and some introspection methods too.

The section :ref:`adding a new version <ref_project_contributor_version>` explains
how to create a version for a YAMM project.

FrameworkSoftware
-----------------

A software describes how to install a specific software. It provides information on how to get the software,
how to build it and finally how to install it. It also provides information on how to create a movable
binary archive. It's an object of the class :ref:`FrameworkSoftware <ref_framework_software_class>`.

The section :ref:`adding a new software <ref_project_contributor_software>` explains how to add
 new software. It contains also all the documentation on how the software class work.

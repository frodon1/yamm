YAMM Principles
===============

.. contents:: Table of contents
   :depth: 2
   :local:

Introduction to YAMM
++++++++++++++++++++

YAMM is a framework that permits to create a project whose goal is to install some software
that have dependencies between each other.

As example, it's possible to create a **project** that permits to build KDE.
This project will contain a description of each **software**
contained in KDE and will define some **versions** that will set for each software,
a program version for the targeted version of KDE. This work is done by the user *Project developer*.
To actually build KDE a *Project user* will write a **configuration file** that will contain
the KDE version number that have to be built and some other options for its specific needs like
a different installation path or just an installation of a subset of the software provided by the chosen KDE version.
After this configuration, the *Project user* will add into his configuration file some commands
that will launch the compilation or installation of the software. These **commands** are executed
by the **executor** of YAMM that will process each software with some **tasks** depending of the
command requested.

This example permits to show the main entities/concepts of YAMM:

- Project
- Software
- Version
- Configuration File
- Command
- Executor
- Executor mode
- Task
- Option

Using YAMM
++++++++++

To use YAMM, a user must write a python command file defining his goal. After that, he
launches YAMM by invoking the **python** command on the file.


Basically, there is four parts on a YAMM command file:

1. An object **project** from the project class chosen (salome, e17, ...):

.. code-block:: python

  # -*- coding: utf-8 -*-
  from yamm.projects.salome import project

  salome_project = project.Project()

2. A chosen version of the project:

.. code-block:: python

  salome_project.set_version("V6_3_1")

3. A configuration of the project object using options:

.. code-block:: python

  # Configuration
  salome_project.options.set_global_option("parallel_make", "8")
  # Use archive mode instead of remote mode
  salome_project.options.set_category_option("module", "source_type", "archive")
  salome_project.options.set_category_option("tool", "source_type", "archive")

4. A set of commands on the project object to execute actions:

.. code-block:: python

  # Commands
  salome_project.print_configuration()
  salome_project.start()
  salome_project.create_appli()

To launch YAMM, **python** program is used:

.. code-block:: bash

  python yamm_file.py

.. note::

  YAMM requires a Python version >= 2.5.x and <= 2.7.x

At the end of the execution, a SALOME version is installed in the $HOME/salome user directory.
YAMM also generates a couple of environment files to be able to set a correct environment before
using/starting SALOME.

Main YAMM Concepts
++++++++++++++++++

Project
-------

A project targets a platform or a set of software that are related to create a "platform". A platform could
be GNOME or SALOME. A set of software could be all the software needed to install a specific version of GNU autotools.

To add a project in YAMM, a class who derives from the framework class called *FrameworkProject* has to be written.
(See Developer part in :ref:`ref_framework_project_class` for more information).

An user will: create an instance of the project, select a **version**, configure it and use **commands**.

- A **version** is a set of software predefined with a coherent set of software programs versions.
- A **command** executes some tasks on the software selected by the user project's configuration.

By default, to be able to do this, an user writes a ``Python`` file containing the code for using the project.
This is an example using the SALOME project to start the building of SALOME **V6_3_1**.

.. code-block:: python

  # -*- coding: utf-8 -*-
  from yamm.projects.salome import project

  salome_project = project.Project()
  salome_project.set_version("V6_3_1")

  # Configuration
  salome_project.options.set_global_option("parallel_make", "8")
  # Use archive mode instead of remote mode
  salome_project.options.set_category_option("module", "source_type", "archive")
  salome_project.options.set_category_option("tool", "source_type", "archive")

  # Commands
  salome_project.print_configuration()
  salome_project.start()
  salome_project.create_appli()

With this example, we can see how to set a version, and how to use the options object provided by each project.
You can learn more about how to use a YAMM project in :ref:`ref_doc_project_user`.

Version
-------

A version of a project is a list of pair ``software name``/``software program version``.

This is an example of a version for GNU tools:

.. code-block:: python

  from yamm.core.framework.version import FrameworkVersion

  version_name = "GNU_TOOLS"

  class GNU_TOOLS(FrameworkVersion):

    def __init__(self, name=version_name, verbose=0, flavour=""):
      FrameworkVersion.__init__(self, name, verbose, flavour)

    def configure_softwares(self):
      self.add_software("M4",       "1.4.9")
      self.add_software("AUTOCONF", "2.68")
      self.add_software("AUTOMAKE", "1.11.1")
      self.add_software("LIBTOOL",  "2.4")

Software
--------

In YAMM, a software object describes of how to get, compile and install the program.
It could contain specific compilation information depending on the software version. 

This example defines how to install the GNU software AUTOMAKE. It defines its dependency *AUTOCONF*,
how to compile it with the *autoconf* provided by YAMM and what to add in the project environment file.

.. code-block:: python

  from yamm.projects.compilers.software import CompilersSoftware
  from yamm.core.base import misc
  from yamm.core.engine.dependency import Dependency
  import os

  software_name = "AUTOMAKE"

  automake_template = """
  #------ automake ------
  export AUTOMAKEHOME=%install_dir
  export PATH=${AUTOMAKEHOME}/bin:${PATH}
  """
  automake_template = misc.PercentTemplate(automake_template)

  class AUTOMAKE(CompilersSoftware):

    def __init__(self, name, version, verbose):
      CompilersSoftware.__init__(self, name, version, verbose)

    def init_variables(self):
      self.software_source_type = "archive"
      self.archive_file_name    = "automake-" + self.version + ".tar.gz"
      self.archive_type         = "tar.gz"

      self.compil_type          = "autoconf"

    def init_dependency_list(self):
      self.software_dependency_list = ["AUTOCONF"]

    def get_dependency_object_for(self, dependency_name):
      dependency_object = CompilersSoftware.get_dependency_object_for(self, dependency_name)
      if dependency_name == "AUTOCONF":
        dependency_object.depend_of = ["path"]
      return dependency_object

    def get_env_str(self, specific_install_dir=""):
      install_dir = self.install_directory
      if specific_install_dir != "":
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
      return automake_template.substitute(install_dir=install_dir)

Option
------

Each project has an attribute named **options** containing all the current defined options of the project.
Each option has a name, a type, a level and a default value.
There is three types of level YAMM's options: **global**, **category** or **software**; **software** :math:`>` **category**
:math:`>` **global**.

As example, you should want to define parallel configuration differently for some software:

.. code-block:: python

  # Configuration
  ## All software will be compiled with -j8
  salome_project.options.set_global_option("parallel_make", "8")
  ## But SALOME Modules should be compiled with -j4
  salome_project.options.set_category_option("module", "parallel_make", "4")
  ## But SALOME KERNEL should be compiled with -j16
  salome_project.options.set_software_option("KERNEL", "parallel_make", "16")

To set a value for each option level, an user calls three different methods:

- **set_global_option** for global level.
- **set_category_option** for category level.
- **set_software_option** for software level.

More information about option API could be found in :ref:`ref_core_base_options_class`.

Executor
--------

The executor is an object that executes tasks on software selected by a project command.
It's configured with two mandatory things:

- A list of executor software objects. In fact, to be able to process the software,
  the executor uses an executor software object provided by each software definition. This object provides
  specifics parameters for the tasks that the executor has to perform.
- An **executor mode** which defines what the executor has to do. Each mode defines a set of elementary **tasks**
  that the executor will launch. Eg of tasks: download a software archive file or compile the software.

Usually a project's command adds each software in the executor with the same mode.

More information about the executor and the YAMM engine part could be found in :ref:`ref_engine_architecture`.

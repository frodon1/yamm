# -*- coding: utf-8 -*-
__author__ = "André RIBES - 2011"

import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../src"
sys.path.append(yamm_directory)

from yamm.projects.e17.project import Project

e17_project = Project()
e17_project.set_version("E17_SVN")

# Configuration
e17_project.options.set_global_option("parallel_make", "8")
e17_project.options.set_global_option("source_type", "remote")

# Execution
e17_project.print_configuration()
e17_project.download()
#e17_project.start()

# -*- coding: utf-8 -*-
__author__ = "André RIBES - 2011"

import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../src"
sys.path.append(yamm_directory)

from yamm.projects.kde.project import Project

kde_project = Project()
kde_project.set_version("KDE_4_8_1")

# Configuration
kde_project.options.set_global_option("parallel_make", "8")

# Execution
kde_project.print_configuration()
kde_project.start()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
    os.pardir, os.pardir, 'src')
sys.path.append(yamm_directory)

from yamm.projects.brtv.project import Project
from yamm.core.base.misc import VerboseLevels

#top_directory = os.path.expanduser('~/MCO/BRTV/LIV')

project = Project(verbose_level=VerboseLevels.WARNING)
project.set_version("V3_2")
#project.options.set_global_option("top_directory",top_directory)
project.set_category_option('prerequisite', "source_type", 'archive')

# Execution
res = True
project.print_configuration()
res = project.download()
if res:
  res = project.start()
###res = project.make("only_compile")

if res:
  project.config_local_install()
if res:
  project.create_pack(runnable=True)

# -*- coding: utf-8 -*-
__author__ = "Julien DEREUX - 2014"

import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../src"
sys.path.append(yamm_directory)

from yamm.projects.bthm.project import Project
from yamm.core.base.misc import VerboseLevels

project = Project(verbose_level=VerboseLevels.WARNING)
project.set_version("V2_3_0")
project.options.set_global_option("top_directory", os.path.join(os.path.expanduser("~"), "bthm"))
project.options.set_global_option("parallel_make", "8")

# Execution
res = True
#project.print_configuration()
#project.download()
res = project.start()
#res = project.make("only_compile")
#
if res:
  project.config_local_install()
if res:
  project.create_pack()

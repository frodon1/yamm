#!/usr/bin/env python
# -*- coding: utf-8 *-

import os, sys

yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
    os.pardir, os.pardir, 'src')
sys.path.append(yamm_directory)

from yamm.projects.cocagne.project import Project
from yamm.core.base.misc import VerboseLevels

#Pour la creation de repertoire tempraire dans YAMM : le /tmp Calibre 7 n'a pas assez de place
#Pour la creation d'une archive deplacable complete de Cocagne avec ses prerequis...
# YAMM utilise tempfile.mkstemp()
os.environ["TMP"] = "/scratch/A4XXX"

##################"
home=os.getenv("HOME")
#Répertoire d'installation
top_install_directory    = os.path.join(home, "TRAVAIL/YAMM_COCAGNE_PROJECT/TEST")

#Repertoire et fichier archive des sources cocagne a compiler
# (Par defaut, les données sont récupérées sur /netdata/cocagne)
#cocagne_sources_path     = os.path.join(home, "TRAVAIL/COCAGNE_PORTABLE-V2/SOURCES_COCAGNE")
#cocagne_sources_archives = "sources-Cocagne-1.2.0.tgz"

#Donnees cocagne pour les tests (dklib, saphyb, tables thermo,...)
# (Par defaut, les données sont récupérées sur /netdata/cocagne)
#cocagne_origin_data_dir  = os.path.join(home, "TRAVAIL/COCAGNE_PORTABLE-V2/SOURCES_COCAGNE/DATA")

project = Project()

# Configuration
#project.options.set_global_option("verbose_level", VerboseLevels.WARNING)
project.set_version("Cocagne120_Salome64_calibre7")

# ------------ Options YAMM -----------------
project.options.set_global_option("top_directory", top_install_directory)
project.options.set_global_option("version_directory", os.path.join(top_install_directory, project.version))

project.options.set_global_option("continue_on_failed", False)
project.options.set_global_option("parallel_make", '2')
project.options.set_global_option("archives_directory", os.path.join(top_install_directory, 'archives_install'))
project.options.set_global_option("clean_src_if_success", False)
project.options.set_global_option("clean_build_if_success", False)

# ------------ Options COCAGNE ---------------
#Par defaut, les données sont récupérées sur /netdata/cocagne

#project.options.set_software_option("COCAGNE", "cocagne_archive_filename", cocagne_sources_archives)
#project.options.set_software_option("COCAGNE", "cocagne_archive_address",  "file:%s" % cocagne_sources_path)
project.options.set_software_option("COCAGNE", "cocagne_optim", "opt3")
project.options.set_software_option("COCAGNE", "cocagne_precision", "double")
#project.options.set_software_option("COCAGNE", "cocagne_zipped_dklibs",  os.path.join(cocagne_origin_data_dir, "dklibZipped") )
#project.options.set_software_option("COCAGNE", "cocagne_zipped_saphybs", os.path.join(cocagne_origin_data_dir, "saphybZipped") )
#project.options.set_software_option("COCAGNE", "cocagne_thm_libs",       os.path.join(cocagne_origin_data_dir, "thermo") )


# ---- Informations sur les prérequis si nécessaire ----
# Les archives des prerequis sont récupérées sur le serveur nepal
# Il est possible de les copier ailleurs et de spécifier leur chemin d'acces ici
##localPrerequisites = "XXXX"

#project.options.set_software_option("BOOST", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("HDF5", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("PYTHON", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("NUMPY", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("SWIG", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("DOCUTILS", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("TBB", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("MEDFICHIER", "archive_remote_address", "file:"+localPrerequisites)
#project.options.set_software_option("XDATA", "archive_remote_address", "file:"+localPrerequisites)

# On ne s'interesse qu'a la bibliotheque date_time de boost : on ne compile que ca
project.options.set_software_option("BOOST", "software_additional_config_options", "--with-libraries=date_time")

# Execution
project.print_configuration()

#project.nothing()
project.download()
project.start()

###Ajouté a la fin
#project.create_movable_installer()


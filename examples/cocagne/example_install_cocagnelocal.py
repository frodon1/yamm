#!/usr/bin/env python
# -*- coding: utf-8 *-

import os, sys

yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
    os.pardir, os.pardir, 'src')
sys.path.append(yamm_directory)

from yamm.projects.cocagne.project import Project

#Pour la creation de repertoire tempraire dans YAMM : le /tmp n'a pas assez de place...
# YAMM utilise tempfile.mkstemp()
os.environ["TMP"] = "/scratch/AXXXXX"

##################"
#home=os.getenv("HOME")
home="/scratch/AXXXXX"
#Répertoire d'installation
top_install_directory    = os.path.join(home, "TEST_COCAGNE_COMPLET-jt_yp_CinetiqueAvecThermo_apr12")

#Repertoire et fichier archive des sources cocagne a compiler
# (Par defaut, les données sont récupérées sur /netdata/cocagne)
#cocagne_sources_path     = os.path.join(home, "EXTRACT_COCAGNE_LIB")
cocagne_sources_path     = home
cocagne_sources_archives = "jt_yp_CinetiqueAvecThermo_apr12.tgz"

project = Project()

# Configuration
project.set_version("Cocagne120_Salome64_calibre7")

# ------------ Options YAMM -----------------
project.options.set_global_option("top_directory", top_install_directory)
project.options.set_global_option("version_directory", os.path.join(top_install_directory, project.version))

project.options.set_global_option("continue_on_failed", False)
project.options.set_global_option("parallel_make", '2')
project.options.set_global_option("archives_directory", os.path.join(top_install_directory, 'archives_install'))
project.options.set_global_option("clean_src_if_success", False)
project.options.set_global_option("clean_build_if_success", False)

# ------------ Options COCAGNE ---------------
#Par defaut, les données sont récupérées sur /netdata/cocagne

project.options.set_software_option("COCAGNE", "cocagne_archive_filename", cocagne_sources_archives)
project.options.set_software_option("COCAGNE", "cocagne_archive_address",  "file:%s" % cocagne_sources_path)
project.options.set_software_option("COCAGNE", "cocagne_optim", "opt3")
project.options.set_software_option("COCAGNE", "cocagne_precision", "double")
#project.options.set_software_option("COCAGNE", "cocagne_zipped_dklibs",  os.path.join(cocagne_origin_data_dir, "dklibZipped") )
#project.options.set_software_option("COCAGNE", "cocagne_zipped_saphybs", os.path.join(cocagne_origin_data_dir, "saphybZipped") )
#project.options.set_software_option("COCAGNE", "cocagne_thm_libs",       os.path.join(cocagne_origin_data_dir, "thermo") )

# On ne s'interesse qu'a la bibliotheque date_time de boost : on ne compile que ca
project.options.set_software_option("BOOST", "software_additional_config_options", "--with-libraries=date_time")

# Execution
project.print_configuration()

#project.nothing()
project.download()
project.start()

###Ajouté a la fin
project.create_movable_installer()


#!/usr/bin/env python
# -*- coding: utf-8 *-

import os, sys

yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
    os.pardir, os.pardir, 'src')
sys.path.append(yamm_directory)

from yamm.projects.cocagne.project import Project
from yamm.core.base.misc import VerboseLevels


##################"
home="/tmp/TEST_YAMM"
#Répertoire d'installation
top_install_directory    = os.path.join(home, "TEST_H5PY")

project = Project()

# Configuration
#project.options.set_global_option("verbose_level", VerboseLevels.WARNING)
project.set_version("Prerequis_H5PY")

# ------------ Options YAMM -----------------
project.options.set_global_option("top_directory", top_install_directory)
project.options.set_global_option("version_directory", os.path.join(top_install_directory, project.version))

project.options.set_global_option("continue_on_failed", False)
project.options.set_global_option("parallel_make", '2')
project.options.set_global_option("archives_directory", os.path.join(top_install_directory, 'archives_install'))
project.options.set_global_option("clean_src_if_success", False)
project.options.set_global_option("clean_build_if_success", False)

# Execution
project.print_configuration()

#project.nothing()
project.download()
project.start()

###Ajouté a la fin
project.create_movable_installer()


#!/usr/bin/env python
# encoding: utf8
from __future__ import absolute_import, print_function, unicode_literals

import codecs
import os


__author__ = "Gilles DAVID - 2017"


def run(yamm_project, use_system_version=True, softwares=None, check=False):
    """
    Liste les paquets Debian nécessaires à une installation SALOME.
    Peut aussi vérifier si tous les paquets sont installés (check=True)
    """
    yamm_project.set_version('V1_0_1', 'EEM PRODUCTION')
    yamm_project.set_global_option('version_directory', os.path.join(os.path.expanduser('~'), 'eem_andromede/1.0.1'))
    yamm_project.set_global_option('use_system_version', use_system_version)
    yamm_project.set_global_option('write_soft_infos', False)

#     used_packages = yamm_project.get_debian_packages()
#     depends_packages = yamm_project.get_debian_depends()
#     build_depends_packages = yamm_project.get_debian_build_depends()
#     print('\nUsed packages:\n%s' % ' '.join(used_packages))
#     print('\nDepends:\n%s' % ' '.join(depends_packages))
#     print('\nBuild depends:\n%s' % ' '.join(build_depends_packages))
#
#     print('Vérification des paquets installés pour la construction')
#     if yamm_project.check_debian_packages_build():
#         print('OK: all packages needed for build process are installed')
#
#     print("Vérification des paquets installés pour l'exécution")
#     if yamm_project.check_debian_packages_exec():
#         print('OK: all packages needed for execution of SALOME %s are installed' % yamm_project.version)

    print("Vérification des paquets installés pour l'exécution")
    ret = yamm_project.compute_debian_runtime_packages_from_softwares(softwares)
    print("Packages:", ret)
    if ret is not None:
        basedir = os.path.join(os.path.curdir, 'depends_eem')
        try:
            os.mkdir(basedir)
        except OSError:
            pass
        for softname, packages in list(ret.items()):
            with codecs.open(os.path.join(basedir, '%s_depends.txt' % softname.lower()), 'w') as f:
                f.writelines('\n'.join(packages))
                f.write('\n')
    """
            if check:
                res = yamm_project.check_debian_packages(packages)
                if res[0]:
                    print('OK: all packages needed for execution of %s in SALOME %s are installed' % (softname, yamm_project.version))
                else:
                    print('KO: Some packages are missing for %s' % softname)
    """


if __name__ == "__main__":
    import sys
    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  os.pardir, os.pardir, 'src')
    sys.path.append(yamm_directory)
    from yamm.projects.eem_andromede.project import Project
    softwares = []
    use_system_version = False
    if len(sys.argv) > 1:
        use_system_version = eval(sys.argv[1])
    if len(sys.argv) > 2:
        softwares = sys.argv[2:]
    run(Project(), use_system_version, softwares=softwares)

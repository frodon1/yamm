#!/usr/bin/env python

from __future__ import print_function
from builtins import str
import os
import sys


def usage():
    return """
This script builds emc2 then creates a virtual application, using Yamm scripting.
This script is customized to be called from Jenkins; the following
variables are used:
yamm_workspace:        the installation folder of Yamm tool
emc2_workspace:      the emc2 base directory, containing the application folder.
emc2_version:        the version of emc2 to build, e.g. "DEV"
emc2_version_flavour:the flavour of emc2 to build, e.g. "EMC2 PRODUCTION"
emc2_version_folder: the version directory to write in.
source_type:           the source type ('remote' or 'archive') for tools and modules
clean_workspace:       clean source, build and install folders before new build.
"""


# Build
def build_emc2(yamm_workspace, emc2_workspace,
                        emc2_version, emc2_version_flavour,
                        emc2_version_folder, source_type,
                        clean_workspace):
    import multiprocessing
    cpu_count = multiprocessing.cpu_count()

    yamm_directory = os.path.join(yamm_workspace, 'src')
    sys.path.append(yamm_directory)

    from yamm.core.base.misc import VerboseLevels
    from yamm.projects.emc2.project import Project

    yamm_project = Project()

    # Configuration
    yamm_project.set_version(emc2_version, emc2_version_flavour)
    yamm_project.set_global_option('top_directory', emc2_workspace)
    yamm_project.set_global_option('continue_on_failed', False)
    # yamm_project.set_global_option('continue_on_failed', True)
    yamm_project.set_global_option('parallel_make', str(cpu_count))
    yamm_project.set_global_option('archives_directory',
                                   os.path.join(emc2_workspace, 'archives'))
    yamm_project.set_global_option('clean_src_if_success', False)
    yamm_project.set_global_option('clean_build_if_success', False)
    yamm_project.set_global_option('command_verbose_level', VerboseLevels.DEBUG)

    yamm_project.set_global_option('version_directory',
                                   os.path.join(emc2_workspace, emc2_version_folder))
    yamm_project.set_category_option('tool', 'source_type', source_type)
    yamm_project.set_category_option('module', 'source_type', source_type)
    yamm_project.set_category_option('prerequisite', 'source_type', 'archive')

    yamm_project.set_global_option('command_verbose_level', VerboseLevels.DEBUG)
    yamm_project.set_global_option('use_pleiade_mirrors', True)
    

    if str(clean_workspace).lower() in ["true", '1']:
        yamm_project.delete_directories(delete_src=True, delete_build=True, delete_install=True)

    successful = yamm_project.start()
    if successful:
        successful = yamm_project.create_appli()
    return successful
#

if __name__ == "__main__":
    if len(sys.argv) != 8:
        print("Usage: %s <yamm_workspace> <emc2_workspace> "
              "<emc2_version> <emc2_version_folder> "
              "<source_type> <clean_workspace>\n" % (sys.argv[0]))
        print(usage())
        sys.exit(1)

    successful = build_emc2(*sys.argv[1:])
    if not successful:
        print("EMC2 ANDROME build is not successful!")
        sys.exit(1)
    sys.exit(0)
#

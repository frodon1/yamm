#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

# A renseigner!
yamm_path=""

import sys
sys.path.append(yamm_path)
from yamm.core.base.misc import VerboseLevels
from yamm.projects.eem_andromede.project import Project

yamm_project = Project()

# Configuration
yamm_project.set_version('V1_0_1', 'EEM PRODUCTION')
yamm_project.set_global_option('software_remove_list', [])
yamm_project.set_global_option('command_verbose_level', VerboseLevels.INFO)
yamm_project.set_global_option('parallel_make', '8')

# Execution
yamm_project.print_configuration()
ret = yamm_project.start()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
# pylint: disable=star-args, line-too-long, import-error

"""
Script d'exemple pour la compilation de l'EEM sur le cluster EOLE
"""

from __future__ import print_function

import os
import sys
import tempfile
import pprint

module_load = ['ifort/2017', 'icc/2017']
f = tempfile.NamedTemporaryFile()
for module in module_load:
    f.write("module load %s\n"%module)
f.flush()

def build_eem_andromede(eem_andromede_workspace='%s/eem_andromede' % os.getenv('SCRATCHDIR'),
                        eem_andromede_version='DEV',
                        eem_andromede_dkzip_path='/projets/donnees_rex/DKLibs',
                        eem_andromede_baseexploit_path='/projets/donnees_rex/persee-base-exploitation',
                        eem_andromede_studies_path='$SCRATCHDIR/Etudes'):
    """ Fonction pour compiler l'EEM
    """
    import multiprocessing
    cpu_count = multiprocessing.cpucount()  # @UndefinedVariable

    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir, "src")
    sys.path.append(yamm_directory)

    from yamm.core.base.misc import VerboseLevels
    from yamm.projects.eem_andromede.project import Project

    yamm_project = Project(verbose_level=VerboseLevels.INFO)

    # Configuration
    yamm_project.set_version(eem_andromede_version, 'EEM PRODUCTION EOLE')
    yamm_project.set_global_option('top_directory', eem_andromede_workspace)

    yamm_project.set_global_option('Dkzip_path', eem_andromede_dkzip_path)
    yamm_project.set_global_option('BaseExploitation_path', eem_andromede_baseexploit_path)
    yamm_project.set_global_option('Studies_path', eem_andromede_studies_path)

    yamm_project.set_global_option('software_remove_list', ['INTEL'])
    yamm_project.set_software_option('THYC', "module_load", "%s %s"%(module_load[0],module_load[1]))
    yamm_project.set_global_option('user_extra_prerequisites_env_file', f.name)

    # Execution
    pprint.pprint(dict(yamm_project.get_packages()))
#

if __name__ == "__main__":
    if len(sys.argv) > 1:
        build_eem_andromede(*sys.argv[1:])
    else:
        build_eem_andromede()

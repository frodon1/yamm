#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

import getpass
import os
import sys
import tempfile


home = os.path.expanduser("~")
scratchdir = '/scratch/%s' % getpass.getuser()
module_load = ['icc/2013_sp1.5.212', 'ifort/2013_sp1.5.212', "modan/1.1"]
f = tempfile.NamedTemporaryFile()
for module in module_load:
    f.write("module load %s\n"%module)
f.flush()

def build_emc2(emc2_workspace='%s/emc2' % scratchdir,
                        emc2_version='DEV'):
    import multiprocessing
    cpu_count = multiprocessing.cpu_count()

    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir, "src")
    sys.path.append(yamm_directory)

    from yamm.core.base.misc import VerboseLevels
    from yamm.projects.emc2.project import Project

    yamm_project = Project(verbose_level=VerboseLevels.DEBUG)

    # Configuration
    yamm_project.set_version(emc2_version, 'EMC2 PRODUCTION PORTHOS')
    yamm_project.set_global_option('top_directory', emc2_workspace)
    yamm_project.set_global_option('archives_directory', '%s/emc2/archives' % home)
    yamm_project.set_global_option('parallel_make', str(cpu_count))

    yamm_project.set_global_option('clean_build_if_success', True)
    yamm_project.set_global_option("use_pleiade_mirrors", True)
    yamm_project.set_global_option('software_remove_list', ['INTEL'])
    yamm_project.set_software_option('THYC', "module_load", "%s %s"%(module_load[0],module_load[1]))
    yamm_project.set_global_option('user_extra_prerequisites_env_file', f.name)

    # Execution
    ret = yamm_project.start()
    if ret:
        ret = yamm_project.create_appli()
    return ret
#

if __name__ == "__main__":
    if len(sys.argv) > 1:
        successful = build_emc2(*sys.argv[1:])
    else:
        successful = build_emc2()
    if not successful:
        print("EMC2 build is not successful!")
        sys.exit(1)
    sys.exit(0)

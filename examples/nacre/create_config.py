# -*- coding: utf-8 -*-

from __future__ import print_function

from future import standard_library
standard_library.install_aliases()
from builtins import input
from builtins import range
import configparser
import os
import sys


__booleans__ = ['1', 'yes', 'true', 'on',
                '0', 'no', 'false', 'off']
__sdr__ = ["P04", "P14"]
__profils__ = ["exploitation", "developpement", "ingenierie", "espace"]
__ssl_mode__ = ["NONE", "KEY", "CA"]


def contains_sublist(lst, sublst):
    n = len(sublst)
    return any((sublst == lst[i:i + n]) for i in range(len(lst) - n + 1))


def add_config(config, config_dict):
    """Ajoute des sections et options à un objet ConfigParser.
    config: l'objet ConfigParser
    config_dict: dictionnaire dont les clés sont composées des éléments suivants séparés par un point:
    - un indice (pour ordonner les options)
    - une section
    - une option
    et les valeurs sont composées de 5 éléments:
    - une valeur initiale à None
    - un booleen indiquant si la valeur est obligatoire
    - une liste de valeurs autorisees
    - le nombre max de valeurs
    - un booleen indiquant si la valeur est un nombre
    """
    try:
        for option in sorted(config_dict):
            _, section, option_name = option.split('.')
            valeur, obligatoire, valeurs_autorisees, nb_max, is_digit = config_dict[option]
            while valeur is None:
                valeur = input("Donner une valeur pour l'option '%s': " % option_name)
                if not valeur and obligatoire:
                    print("La valeur ne peut pas être vide")
                    valeur = None
                else:
                    test_valeur = valeur.replace(',', ' ')
                    liste_valeurs = [v.strip() for v in test_valeur.split()]
                    if len(liste_valeurs) > nb_max:
                        print("Nombre max de valeur%s: %d" % ("s" if nb_max > 1 else "", nb_max))
                        valeur = None
                    elif is_digit and not valeur.isdigit():
                        print("Un nombre est attendu")
                        valeur = None
                    elif valeurs_autorisees and not contains_sublist(valeurs_autorisees, liste_valeurs):
                        print("Valeurs autorisées: %s" % ", ".join(valeurs_autorisees))
                        valeur = None
            if not config.has_section(section):
                config.add_section(section)
            config.set(section, option_name, valeur)
    except KeyboardInterrupt:
        print("\nConfiguration abandonnée")
        sys.exit(1)


def add_sde(config):
    nbsde = ""
    while not nbsde.isdigit():
        nbsde = input("Nombre de SDE à ajouter: ")
        if not nbsde.isdigit():
            print("un nombre est attendu")
    nbsde = int(nbsde)
    if nbsde > 0:
        for indice_sde in range(nbsde):
            # Valeurs du dict: (valeur initiale, obligatoire, valeurs autorisees, nombre max de valeurs, valeur est un nombre)
            config_dict = {'0.sde.sde%d' % (indice_sde + 1): (None, True, "", 1, False),
                           '1.sde.path_sde%d' % (indice_sde + 1): (None, False, "", 1, False),
                           '2.sde.host_sde%d' % (indice_sde + 1): (None, False, "", 1, False),
                           '3.sde.port_sde%d' % (indice_sde + 1): (None, True, "", 1, True),
                           '4.sde.polling_sde%d' % (indice_sde + 1): (None, False, "", 1, True),
                           '5.sde.profils_sde%d' % (indice_sde + 1): (None, False, __profils__, 4, False),
                           '6.sde.start_sde%d' % (indice_sde + 1): (None, True, __booleans__, 1, False),
                           '7.sde.auth_sde%d' % (indice_sde + 1): (None, True, __booleans__, 1, False)}
            config = add_config(config, config_dict)


def add_sdr(config):
    nbsdr = ""
    while not nbsdr.isdigit():
        nbsdr = input("Nombre de SDR à ajouter: ")
        if not nbsdr.isdigit():
            print("un nombre est attendu")
    nbsdr = int(nbsdr)
    if nbsdr > 0:
        for indice_sdr in range(nbsdr):
            # Valeurs du dict: (valeur initiale, obligatoire, valeurs autorisees, nombre max de valeurs, valeur est un nombre)
            config_dict = {'0.sdr.sdr%d' % (indice_sdr + 1): (None, True, "", 1, False),
                           '1.sdr.order_sdr%d' % (indice_sdr + 1): (None, False, "", 1, True),
                           '2.sdr.version_sdr%d' % (indice_sdr + 1): (None, False, __sdr__, 1, False),
                           '3.sdr.profils_sdr%d' % (indice_sdr + 1): (None, False, __profils__, 4, False),
                           '4.sdr.host_sdr%d' % (indice_sdr + 1): (None, False, "", 1, False),
                           '5.sdr.port_sdr%d' % (indice_sdr + 1): (None, True, "", 1, True),
                           '6.sdr.origin_sdr%d' % (indice_sdr + 1): (None, False, "", 1, False),
                           '7.sdr.start_sdr%d' % (indice_sdr + 1): (None, False, __booleans__, 1, False)}
            config = add_config(config, config_dict)


def main(config_filename=None):

    if config_filename is None:
        print("Aucun fichier de config donné")
        sys.exit(1)

    config = configparser.ConfigParser()
    config_dict = {'0.musicale.adresse_musicale': (None, True, "", 1, False),
                   '1.musicale.login_musicale': (None, True, "", 1, False),
                   '2.musicale.mdp_musicale': (None, True, "", 1, False),
                   '3.ssl.nacre_ssl_mode': (None, True, __ssl_mode__, 1, False),
                   '4.ssl.nacre_ssl_path': (None, True, "", 1, False),
                   '5.oracle.oracle_sid': (None, True, "", 1, False),
                   '6.oracle.chaine_connection_oracle': (None, True, "", 1, False)}
    print("Configuration de l'application")
    add_config(config, config_dict)
    print("Configuration des serveurs de donnée et de publication")
    add_sde(config)
    add_config(config, {'0.sdp.port': (None, True, "", 1, True)})
    add_sdr(config)

    if not os.path.exists(os.path.dirname(config_filename)):
        os.makedirs(os.path.dirname(config_filename))
    with open(config_filename, 'wb') as configfile:
        config.write(configfile)

if __name__ == "__main__":

    import optparse

    parser = optparse.OptionParser()
    parser.add_option('-c', dest="config", metavar="CONFIG",
                      default=os.path.join(os.path.expanduser('~'), '.config', 'nacre', 'config_example.cfg'),
                      help=u'Chemin complet du fichier de config à générer (%default par défaut)')
    options, _ = parser.parse_args()
    main(options.config)

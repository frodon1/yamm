# -*- coding: utf-8 -*-

from future import standard_library
standard_library.install_aliases()
from builtins import range
import configparser
import os
import sys

yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir, "src")
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels  # @IgnorePep8
from yamm.projects.nacre.project import Project  # @IgnorePep8


__author__ = "Gilles DAVID - 2016"


def main(version, config_file=None, debug=False):

    config = configparser.ConfigParser()
    filenames = [os.path.join(os.path.expanduser('~'), '.config', 'nacre', 'config_%s.cfg' % version)]
    if config_file:
        filenames.append(config_file)
    read_ok = config.read(filenames)
    if not read_ok:
        parser.error("Aucun fichier de configuration de Nacre n'a pu être lu: {0}".format(filenames))
        sys.exit(2)

    verbose_level = VerboseLevels.INFO
    if debug:
        verbose_level = VerboseLevels.DEBUG
    nacre_project = Project(verbose_level=verbose_level)
    nacre_project.set_version(version)
    nacre_project.options.set_global_option("parallel_make", "8")
    nacre_project.options.set_global_option("nb_calcs", 8)
    nacre_project.options.set_global_option("nb_thread_cox", 1)
    nacre_project.options.set_global_option("source_type", "remote")
    nacre_project.options.set_software_option("ICAREX", "software_additional_env_file", "/home/J99972/projets/nacre/intel.sh")

    nacre_project.set_global_option('login_musicale', config.get('musicale', 'login_musicale'))
    nacre_project.set_global_option('adresse_musicale', config.get('musicale', 'adresse_musicale'))
    nacre_project.set_global_option('mdp_musicale', config.get('musicale', 'mdp_musicale'))
    nacre_project.set_global_option('nacre_ssl_mode', config.get('ssl', 'nacre_ssl_mode'))
    nacre_project.set_global_option('nacre_ssl_path', config.get('ssl', 'nacre_ssl_path'))
    nacre_project.set_global_option('oracle_sid', config.get('oracle', 'oracle_sid'))
    nacre_project.set_global_option('chaine_connection_oracle', config.get('oracle', 'chaine_connection_oracle'))

    # Configurations spécifiques au projet: les SDE
    nbsde = config.getint('sde', 'nbsde')
    for indice_sde in range(nbsde):
        sde = 'sde%d' % (indice_sde + 1)
        name = config.get('sde', sde)
        try:
            profils = config.get('sde', 'profils_%s' % sde)
            profils = profils.split(',')
        except configparser.NoOptionError:
            profils = []
        try:
            host = config.get('sde', 'host_%s' % sde)
        except configparser.NoOptionError:
            host = ''
        try:
            path = config.get('sde', 'path_%s' % sde)
        except configparser.NoOptionError:
            path = ''
        port = config.getint('sde', 'port_%s' % sde)  # 24780
        port_polling = config.getint('sde', 'polling_%s' % sde)  # 24760
        start = config.getboolean('sde', 'start_%s' % sde)  # True
        try:
            invites = config.get('sde', 'invites_%s' % sde).split(',')
        except configparser.NoOptionError:
            invites = []
        try:
            sde_auth = config.getboolean('sde', 'auth_%s' % sde)
        except configparser.NoOptionError:
            sde_auth = False

        nacre_project.add_sde(name, profils, invites, host, path, port, port_polling, start, sde_auth)

    # Configurations spécifiques au projet: les SDP
    nacre_project.set_sdp(config.getint('sdp', 'port'))

    # Configurations spécifiques au projet: les SDR additionnels
    nbsdr = config.getint('sdr', 'nbsdr')
    for indice_sdr in range(nbsdr):
        sdr = 'sdr%d' % (indice_sdr + 1)
        name = config.get('sdr', sdr)
        try:
            order = config.getint('sdr', 'order_%s' % sdr)
        except configparser.NoOptionError:
            order = 0
        try:
            version = config.get('sdr', 'version_%s' % sdr)
        except configparser.NoOptionError:
            version = 'P04'
        try:
            profils = config.get('sdr', 'profils_%s' % sdr)
            profils = profils.split(',')
        except configparser.NoOptionError:
            profils = []
        try:
            host = config.get('sdr', 'host_%s' % sdr)
        except configparser.NoOptionError:
            host = ""
        port = config.getint('sdr', 'port_%s' % sdr)
        try:
            origin = config.get('sdr', 'origin_%s' % sdr)
        except configparser.NoOptionError:
            origin = ''
        try:
            start = config.getboolean('sdr', 'start_%s' % sdr)
        except configparser.NoOptionError:
            start = False

        nacre_project.add_sdr(name, order, version, profils, host, port, origin, start)

    # Execution
    # res = True
    # nacre_project.print_configuration()
    # nacre_project.download()
    res = nacre_project.start()
    if res:
        res = nacre_project.config_local_install()
        # if res:
        #     nacre_project.create_pack(unie=False, runnable=True)
    return res

if __name__ == "__main__":

    import optparse

    parser = optparse.OptionParser(usage="%prog nacre.cfg")
    parser.add_option('-d', '--debug', action='store_true', help='Executer en mode DEBUG')
    options, args = parser.parse_args()
    config_file = None
    if len(args) != 0:
        config_file = args[0]
        if not os.path.exists(config_file):
            parser.error("Le fichier de configuration de Nacre n'existe pas: %s" % config_file)
            sys.exit(1)

    main('DEV', config_file, options.debug)

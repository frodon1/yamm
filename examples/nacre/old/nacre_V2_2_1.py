# -*- coding: utf-8 -*-

import os
import sys

yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir, os.pardir, "src")
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels  # @IgnorePep8
from yamm.projects.nacre.project import Project  # @IgnorePep8

__author__ = "Gilles DAVID - 2013"

nacre_project = Project(verbose_level=VerboseLevels.WARNING)
nacre_project.set_version("V2_2_1")
nacre_project.options.set_global_option("top_directory", os.path.join(os.path.expanduser("~"), "nacre"))
nacre_project.options.set_global_option("parallel_make", "8")
nacre_project.options.set_global_option("nb_calcs", 8)
nacre_project.options.set_global_option("nb_thread_cox", 1)
nacre_project.options.set_global_option("write_soft_infos", False)
nacre_project.options.set_global_option("source_type", "remote")
# nacre_project.options.set_software_option("ICAREX", "software_additional_env_file", "/home/J99972/projets/nacre/intel.sh")
nacre_project.options.set_software_option("ORACLE_CLIENT", "source_type", "archive")
nacre_project.options.set_software_option("POCO", "source_type", "archive")

# Configurations spécifiques au projet: les SDE
nacre_project.add_sde("SDE",
                      profils=["exploitation", "developpement", "ingenierie", "espace"],
                      host="localhost",
                      path=os.path.join(os.path.expanduser("~"), "projets", "nacre", "sde"),
                      port=24780, port_polling=24760, start=True,
                      invites=[])
nacre_project.add_sde("SDE_PCY",
                      host="pcy00309.pcy.edfgdf.fr",
                      port=24780, port_polling=24760, start=False)
# Configurations spécifiques au projet: les SDP
nacre_project.set_sdp(90)
# Configurations spécifiques au projet: les SDR additionnels
nacre_project.add_sdr("P04_NEW",
                      order=0,
                      version="P04",
                      profils=["exploitation", "developpement", "ingenierie", "espace"],
                      host="",
                      port=24771,
                      origin="",
                      start=True)

nacre_project.add_sdr("P14_NEW",
                      order=1,
                      version="P14",
                      profils=["exploitation", "developpement", "ingenierie", "espace"],
                      host="",
                      port=24771,  # ce port sera incrémenté car déjà utilisé
                      origin="P14",
                      start=True)

# Execution
res = True
# nacre_project.print_configuration()
# nacre_project.download()
res = nacre_project.start()
if res:
    nacre_project.config_local_install()
    # nacre_project.create_pack()

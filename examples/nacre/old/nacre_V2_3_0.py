#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

import os
import sys

yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir, os.pardir, "src")
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels  # @IgnorePep8
from yamm.projects.nacre.project import Project  # @IgnorePep8


yamm_project = Project(verbose_level=VerboseLevels.WARNING)
# Configuration
yamm_project.set_version("V2_3_0")
yamm_project.options.set_global_option("top_directory", os.path.join(os.path.expanduser("~"), "nacre"))
yamm_project.options.set_global_option("parallel_make", '8')
# yamm_project.options.set_global_option("adresse_musicale", 'http://noev02u7.noe.edf.fr:4201')
yamm_project.options.set_global_option("mdp_musicale", 'I21z282C')
yamm_project.options.set_global_option("software_remove_list", ['NOAH'])
yamm_project.options.set_global_option("oracle_sid", 'NRD')
yamm_project.options.set_software_option("ICAREX", "software_additional_env_file", '/home/J99972/projets/nacre/intel.sh')

yamm_project.add_sde("SDE",
                     profils=['exploitation', 'developpement', 'ingenierie', 'espace'],
                     host="localhost",
                     path="/home/J99972/projets/nacre/sde",
                     port=24780,
                     port_polling=24760,
                     start=True,
                     invites=[''])

# Execution
yamm_project.print_configuration()
ret = yamm_project.start()
if ret:
    yamm_project.config_local_install()

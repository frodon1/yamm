#!/usr/bin/env python
__author__ = "Renaud BARATE - 2015"

import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../src"
sys.path.append(yamm_directory)

from yamm.projects.openturns import project

version = "V1_5_1"
top_directory = os.path.join(os.getenv("HOME"), "openturns")
installer_topdirectory = os.path.join(top_directory, version, "movable_installer", "OpenTURNS-1.5.1-Calibre-7")

openturns_project = project.Project()
openturns_project.set_version(version)

# Configuration
openturns_project.options.set_global_option("top_directory", top_directory)
openturns_project.options.set_global_option("parallel_make", "8")
openturns_project.options.set_global_option("version_flavour", "calibre_7")

# Execution
openturns_project.print_configuration()
openturns_project.start()
openturns_project.create_movable_installer(installer_topdirectory = installer_topdirectory)

#!/usr/bin/env python
# encoding: utf8
from __future__ import absolute_import, print_function, unicode_literals


__author__ = "Gilles DAVID - 2017"


def run(yamm_project):
    yamm_project.set_version('DEV')
    yamm_project.set_global_option('use_system_version', True)
    yamm_project.set_global_option('write_soft_infos', False)

    used_packages = yamm_project.get_debian_packages()
    depends_packages = yamm_project.get_debian_depends()
    build_depends_packages = yamm_project.get_debian_build_depends()
    print('\nUsed packages: %s' % ' '.join(used_packages))
    print('\nDepends: %s' % ' '.join(depends_packages))
    print('\nBuild depends %s' % ' '.join(build_depends_packages))

    print('Vérification des paquets installés pour la construction')
    if yamm_project.check_debian_packages_build():
        print('OK: all packages needed for build process are installed')

    print("Vérification des paquets installés pour l'exécution")
    if yamm_project.check_debian_packages_exec():
        print('OK: all packages needed for execution of SALOME %s are installed' % yamm_project.version)

if __name__ == "__main__":
    import os
    import sys
    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  os.pardir, os.pardir, 'src')
    sys.path.append(yamm_directory)
    from yamm.projects.salome.project import Project
    run(Project())

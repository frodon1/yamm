#!/usr/bin/env python

from __future__ import print_function
import os

__author__ = "Florian BRUNET - 2015"

"""
  This script aims at build a Salome with MPI with a minimal effort.
  At least, you have the MPI options and the Yamm function to set the option for a list of softwares needed to build Salome.

  IMPORTANT : when building Salome, you HAVE TO load the MPI library suited for Salome
  via 'module avail' command to know the list of available libraries and giving the mpi version to the script.
  It has been tested only with OpenMPI 1.8.2 mlnx.

  If you build Salome with src and build directories in the /tmp you have to execute
  "newgrp cl-pj-salome-admin" before executing this script.

  TO DO: OPENMPI environment is currently given to each software needing MPI link.
  A better integration would be to have a Yamm variable to add MPI to the construction environment.
  So far, no object in Yamm seems to be suited to the job.
"""


# ########## ATTENTION #################
# ########## C'est un script pour developpeur en dev  ######
# ########## A checker avant de lancer comme une brute :
# ########## 1 - TRES IMPORTANT tool/modules en mode remote ou archive
# ########## 2 - le repertoire /tmp (cf line ~121 )
# ########## 3 - ATTENTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ########## IL FAUT ABSOLUMENT METTRE  du "keep_if_installed" a KERNEL, GUI, GEOM, SMESH sinon petage de cervelle garanti
# ########## Exemple Usage : python DEV_withMPI.py /logiciels/calibre9/binaries/OpenMPI/1.8.2rc6mlnx /scratch/NNI/salome 1.8.2rc6mlnx

def run(yamm_project, salome_dir, mpi_dir_bin, mpi_dir_lib, openmpi_version, is_cluster):
    version ="DEV"
    yamm_project.set_version(version, "mpi")
    yamm_project.set_global_option('top_directory', salome_dir)
    yamm_project.set_global_option('version_directory', salome_dir + "/" + version)
    yamm_project.set_global_option('continue_on_failed', False)
    yamm_project.set_global_option('parallel_make', "20")  # str(multiprocessing.cpu_count()/2))
    yamm_project.set_global_option('use_pleiade_mirrors', True)
    yamm_project.set_global_option("command_verbose_level", 2)

    yamm_project.set_category_option('tool', "source_type", 'remote')
    yamm_project.set_category_option('module', "source_type", 'remote')
    yamm_project.set_category_option('prerequisite', "source_type", 'archive')

    #yamm_project.set_global_option("software_minimal_list", ["PARAVIS", "KERNEL", "ENVHPCOPENMPI", "PARAVISADDONS"])

    yamm_project.set_global_option("modules_debug_mode", True)
    yamm_project.set_global_option("tools_debug_mode", True)
#    yamm_project.set_global_option("check_debian_packages", False)

    yamm_project.set_global_option("module_load", 'openmpi/%s' % openmpi_version)

    # MPI options for Salome environment
    yamm_project.set_software_option('ENVHPCOPENMPI', 'software_additional_env', 'export ENVHPCOPENMPIHOME=' + mpi_dir_lib)

    # MPI options for HDF5
    yamm_project.set_software_option('HDF5', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('HDF5', "software_additional_config_options",
                                     '-DHDF5_ENABLE_PARALLEL=ON -DSALOME_USE_MPI=ON '
                                     '-DMPI_CXX_LIBRARIES:FILEPATH=' + mpi_dir_lib + 'libmpi_cxx.so '
                                     '-DMPI_C_LIBRARIES:FILEPATH=' + mpi_dir_lib + 'libmpi.so '
                                     '-DMPI_LIBRARY:PATH=' + mpi_dir_lib +
                                     ' -DCMAKE_SHARED_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi" ')

    # MPI options for CGNSLIB
    yamm_project.set_software_option('CGNSLIB', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('CGNSLIB', "software_additional_config_options", '-DHDF5_NEED_MPI=ON ')

    # MPI options for KERNEL
    yamm_project.set_software_option('KERNEL', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('KERNEL', "software_additional_config_options", '-DSALOME_USE_MPI=ON ')

    # MPI options for Paraview
    yamm_project.set_software_option('PARAVIEW', 'software_additional_env',
                                     'export OPENMPIHOME=' + mpi_dir_lib + '; '
                                     'export LD_LIBRARY_PATH=' + mpi_dir_lib + ':$LD_LIBRARY_PATH')
    yamm_project.set_software_option('PARAVIEW', "software_additional_config_options",
                                     '-DPYVERSIONS_EXE=/fake_python_exe '
                                     '-DPYTHON_INCLUDE_PATH=${PYTHON_INSTALL_DIR}/include/python${PYTHON_VERSION} '
                                     '-DPARAVIEW_USE_MPI=ON -DHDF5_IS_PARALLEL:BOOL=ON '
                                     '-DPARAVIEW_ENABLE_CATALYST:BOOL=ON '
                                     '-DPARAVIEW_BUILD_PLUGIN_CatalystScriptGeneratorPlugin:BOOL=ON '
                                     '-DMPI_CXX_LIBRARIES:FILEPATH=' + mpi_dir_lib + 'libmpi_cxx.so '
                                     '-DMPI_C_LIBRARIES:FILEPATH=' + mpi_dir_lib + 'libmpi.so '
                                     '-DMPI_LIBRARY:PATH=' + mpi_dir_lib +
                                     ' -DCMAKE_SHARED_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi " '
                                     '-DCMAKE_EXE_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi" ')

    # MPI options for GUI
    yamm_project.set_software_option('GUI', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('GUI', "software_additional_config_options", "-DWITH_MPI=" + mpi_dir_lib)

    # MPI options for MEDFichier
    yamm_project.set_software_option('MEDFICHIER', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('MEDFICHIER', "software_additional_config_options", "--enable-parallel --with-swig=yes ")

    # Global option for PADDER: so far there is no simple solution to have a uniform PADDER build config compatible with and without MPI.
    yamm_project.set_global_option('software_remove_list', ['PADDER', "H5PY"])
#     # So far, ala H5PY is a pain in the a** to build, it is just skipped.
#     # If you are not happy with it, I invite you to my office so we install together H5PY and solve the problem.
#     yamm_project.set_global_option('software_remove_list', ['PADDER', 'H5PY', ])

    # MPI options for SMESH
    yamm_project.set_software_option('SMESH', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('SMESH', "software_additional_config_options",
                                     '-DSALOME_USE_MPI=ON -DCMAKE_SHARED_LINKER_FLAGS="-L' + mpi_dir_lib +
                                     '-lmpi_cxx -lmpi" -DCMAKE_EXE_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi" ')

    # MPI options for MED
    yamm_project.set_software_option('MED', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('MED', "software_additional_config_options",
                                     '-DSALOME_USE_MPI=ON -DSALOME_MED_ENABLE_PARTITIONER=OFF '
                                     '-DSALOME_MED_ENABLE_RENUMBER=OFF -DSALOME_BUILD_TESTS=OFF '
                                     '-DSALOME_BUILD_DOC=OFF -DSALOME_MED_STANDALONE=OFF '
                                     '-DSALOME_MED_MICROMED=OFF -DMPI_CXX_LIBRARIES:FILEPATH=' + mpi_dir_lib +
                                     'libmpi_cxx.so -DMPI_C_LIBRARIES:FILEPATH=' + mpi_dir_lib + 'libmpi.so '
                                     '-DMPI_LIBRARY:PATH=' + mpi_dir_lib +
                                     ' -DCMAKE_SHARED_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi " '
                                     '-DCMAKE_EXE_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi" ')

    # MPI options for MEDCoupling
    yamm_project.set_software_option('MEDCOUPLING', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option("MEDCOUPLING", "software_additional_config_options",
                                     ' -DMEDCOUPLING_USE_MPI=ON -DSALOME_USE_MPI=ON '
                                     '-DMEDCOUPLING_PARTITIONER_PARMETIS=OFF '
                                     '-DMPI_CXX_LIBRARIES:FILEPATH=' + mpi_dir_lib + 'libmpi_cxx.so '
                                     '-DMPI_C_LIBRARIES:FILEPATH=' + mpi_dir_lib + 'libmpi.so '
                                     '-DMPI_LIBRARY:PATH=' + mpi_dir_lib +
                                     ' -DCMAKE_SHARED_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi " '
                                     '-DCMAKE_EXE_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi" ')

    # MPI options for PARAVIS
    yamm_project.set_software_option('PARAVIS', 'software_additional_env',
                                     'export OPENMPIHOME=' + mpi_dir_lib + '; '
                                     'export LD_LIBRARY_PATH=' + mpi_dir_lib + ':$LD_LIBRARY_PATH')
    yamm_project.set_software_option('PARAVIS', "software_additional_config_options",
                                     " -DHDF5_IS_PARALLEL:BOOL=ON -DSALOME_PARAVIS_BUILD_PLUGINS=ON ")

    # MPI options for GHS3DPRLPLUGIN
    yamm_project.set_software_option('GHS3DPRLPLUGIN', 'software_additional_env', 'export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('GHS3DPRLPLUGIN', "software_additional_config_options",
                                     ' -DCMAKE_SHARED_LINKER_FLAGS="-L' + mpi_dir_lib + 
                                     '-lmpi_cxx -lmpi" -DCMAKE_EXE_LINKER_FLAGS="-L' + mpi_dir_lib + ' -lmpi_cxx -lmpi" ')

    # MPI options for CYTHON
    yamm_project.set_software_option('CYTHON', 'software_additional_env', 'export CFLAGS=-l' + mpi_dir)

    # MPI options for H5PY
    yamm_project.set_software_option('H5PY', 'software_additional_env',
                                     'export CC=' + mpi_dir_bin + 'mpicc ; export OPENMPIHOME=' + mpi_dir_lib)
    yamm_project.set_software_option('H5PY', "software_additional_config_options", ' --hdf5-version=1.8.14 --mpi ')

    # MPI options for PARAVISADDONS
    yamm_project.set_software_option('PARAVISADDONS', 'software_additional_env',
                                     'export OPENMPIHOME=' + mpi_dir_lib + '; '
                                     'export LD_LIBRARY_PATH=' + mpi_dir_lib + ':$LD_LIBRARY_PATH')
    yamm_project.set_software_option('PARAVISADDONS', "software_additional_config_options", ' -DSALOME_USE_MPI=ON ')

    # Trick to download and build in /tmp mounted in RAM to speed up construction process.
    if is_cluster:
        yamm_project.set_global_option('clean_build_if_success', True)
        yamm_project.set_global_option('clean_src_if_success', True)

        not_in_tmp = ["DOXYGEN"]
        # not_in_tmp = ["DOXYGEN", "PARAVIEW"]
        for soft in yamm_project.catalog.softwares:
            # si le soft peut etre construit dans un dossier temporaire :
            if soft not in not_in_tmp:
                yamm_project.set_software_option(soft, "software_build_directory", "/tmp/yamm_build_{0}_{1}".format(os.getenv("USER"), soft))
                yamm_project.set_software_option(soft, "software_src_directory", "/tmp/yamm_src_{0}_{1}".format(os.getenv("USER"), soft))

            # Trick to download and build in /tmp mounted in RAM to speed up construction process.
            yamm_project.set_software_option(soft, 'clean_build_if_success', True)
            yamm_project.set_software_option(soft, 'clean_src_if_success', True)


    # Pour eviter de tout casser !
    # yamm_project.set_software_option("CONFIGURATION","keep_if_installed","build")
    # yamm_project.set_software_option("LIBBATCH","keep_if_installed","build")
    # yamm_project.set_software_option("KERNEL","keep_if_installed","build")
    # yamm_project.set_software_option("GUI","keep_if_installed","build")
    # yamm_project.set_software_option("GEOM","keep_if_installed","build")
    # yamm_project.set_software_option("SMESH","keep_if_installed","build")
    # yamm_project.set_software_option("MED","keep_if_installed","build")
    # yamm_project.set_software_option("PARAVIS","keep_if_installed","build")

    # Execution
    yamm_project.print_configuration()

    # si besoin de download
    # - decommenter la ligne yamm_project.download()
    # - commenter toutes les lignes suivantes
    yamm_project.download()
    ret = yamm_project.start()
    if ret:
        ret = yamm_project.create_appli()
    return ret

if __name__ == "__main__":
    import sys
    import socket  # for gethostname method
    if len(sys.argv) < 3:
        sys.exit('Usage : python %s <mpi_dir> <salome_dir> <openmpi_version>' % sys.argv[0])

    mpi_dir = sys.argv[1]
    salome_dir = sys.argv[2]
    openmpi_version = sys.argv[3]

    if "eofront" in socket.gethostname():
        is_cluster  = True
        mpi_dir_lib = os.path.join(mpi_dir, 'lib', 'x86_64-linux-gnu')
    elif "porthos" in socket.gethostname():
        is_cluster  = True
        mpi_dir_lib = os.path.join(mpi_dir, 'lib')
    else:
        is_cluster  = False
        mpi_dir_lib = os.path.join(mpi_dir, 'lib', 'x86_64-linux-gnu') # au moins pour Calibre 9
    print(mpi_dir_lib)

    mpi_dir_bin = os.path.join(mpi_dir, 'bin')
    if not os.path.isdir(mpi_dir):
        sys.exit('There is no valid MPI installation in that directory: %s' % mpi_dir)

    if not os.path.isdir(salome_dir):
        sys.exit('The salome installation dir is not valid: %s' % salome_dir)

    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  os.pardir, os.pardir, 'src')
    sys.path.append(yamm_directory)
    from yamm.projects.salome.project import Project
    ret = run(Project(), salome_dir, mpi_dir_bin, mpi_dir_lib, openmpi_version, is_cluster)
    sys.exit(ret is False)

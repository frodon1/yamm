#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2013 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 
  os.pardir, os.pardir, 'src'))
from yamm.projects.salome.project import Project

version="DEV"
oscard_version="V4.4"

salome_project = Project()
#top_directory = os.path.expanduser('/netdata/oscard/COUPLAGE/DEV/V4.3.8/outils/')
top_directory = os.path.expanduser('~')
installer_path = os.path.join(top_directory,"Salome_{0}_Oscard_{1}_calibre7".format(version, oscard_version))

# Configuration
salome_project.set_version("{0}_OscardYacsOnlyNoGui".format(version))
salome_project.options.set_global_option("top_directory", os.path.join(top_directory,'salome'))
salome_project.options.set_global_option("version_directory", os.path.join(top_directory,'salome/{0}_oscard'.format(version)))
salome_project.options.set_global_option("parallel_make", '8')
salome_project.options.set_category_option("tool", "source_type", 'remote')
salome_project.options.set_category_option("module", "source_type", 'remote')
salome_project.options.set_software_option("OSCARD", "source_type", 'archive')
salome_project.options.set_software_option("YACS", "software_additional_config_options", '-DSALOME_BUILD_GUI=OFF -DSALOME_BUILD_DOC=OFF -DSALOME_BUILD_TESTS=OFF -DSALOME_YACS_USE_QSCINTILLA=OFF ')
salome_project.options.set_software_option("KERNEL", "software_additional_config_options", '-DSALOME_BUILD_DOC=OFF -DSALOME_BUILD_TESTS=OFF -DSALOME_USE_PORTMANAGER=ON')

# Execution
salome_project.print_configuration()
#salome_project.nothing()
ret = salome_project.start()
if ret:
  ret = salome_project.create_appli()
  #salome_project.create_movable_installer(installer_topdirectory=installer_path)

#!/usr/bin/env python
from __future__ import print_function
from builtins import str
__author__ = "Florian BRUNET - 2015"

"""
  This script aims at build a Salome with MPI with a minimal effort. At least, you have the MPI options and the Yamm function to set the option for a list of softwares needed to build Salome. 
  This works on the development version (2015-09-07) and will be ported to a V7_7_0 when available.

  IMPORTANT : before building Salome, you HAVE TO load the MPI library suited for Salome via 'module avail' command to know the list of available libraries and 'module load <MPI_library>' to load it. It has been tested only with OpenMPI 1.8.2 mlnx.

  TO DO: OPENMPI environment is currently given to each software needing MPI link. A better integration would be to have a Yamm variable to add MPI to the construction environment. So far, no object in Yamm seems to be suited to the job.
"""

import os
import sys
import multiprocessing
yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
    os.pardir, os.pardir, 'src')
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels
from yamm.projects.salome.project import Project

mpi_dir = ''
salome_dir = ''
if len(sys.argv) > 2:
   mpi_dir+=sys.argv[1]
   salome_dir+=sys.argv[2]
   mpi_dir_lib=mpi_dir+'/lib/'
   mpi_dir_bin=mpi_dir+'/bin/'
   if(os.path.isdir(mpi_dir)) :
       if(os.path.isdir(salome_dir)):

          yamm_project = Project()

          # Configuration
          yamm_project.set_version('V7_7_1',"mpi")
          yamm_project.set_global_option('top_directory', salome_dir)
          yamm_project.set_global_option('continue_on_failed', False)
          yamm_project.set_global_option('parallel_make', str(multiprocessing.cpu_count()))
          yamm_project.set_global_option('archives_directory', salome_dir+'/archives')
          yamm_project.set_global_option('clean_src_if_success', False)
          yamm_project.set_global_option('clean_build_if_success', False)
          yamm_project.set_global_option('version_directory', salome_dir+'V7_7_1')
          yamm_project.set_global_option('occ_server', 'git.salome-platform.org')
          yamm_project.set_global_option('occ_username', 'brunet')
          yamm_project.set_category_option('tool', "source_type", 'archive')
          yamm_project.set_category_option('module', "source_type", 'archive')
          yamm_project.set_category_option('prerequisite', "source_type", 'archive')
          
          yamm_project.set_global_option("command_verbose_level", 2)

          # MPI options for Salome environment 
          yamm_project.set_software_option('OPENMPI', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)

          # MPI options for HDF5
          yamm_project.set_software_option('HDF5', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('HDF5', "software_additional_config_options",'-DHDF5_ENABLE_PARALLEL=ON -DMPI_CXX_LIBRARIES:FILEPATH='+mpi_dir_lib+'libmpi_cxx.so -DMPI_C_LIBRARIES:FILEPATH='+mpi_dir_lib+'libmpi.so -DMPI_LIBRARY:PATH='+mpi_dir_lib+' -DCMAKE_SHARED_LINKER_FLAGS="-L${LD_LIBRARY_PATH} -lmpi_cxx -lmpi" ')

          #MPI options for CGNSLIB
          yamm_project.set_software_option('CGNSLIB', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('CGNSLIB', "software_additional_config_options", '-DHDF5_NEED_MPI=ON ')

          #MPI options for KERNEL
          yamm_project.set_software_option('KERNEL', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('KERNEL', "software_additional_config_options", '-DSALOME_USE_MPI=ON ')

          #MPI options for Paraview
          yamm_project.set_software_option('PARAVIEW', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('PARAVIEW', "software_additional_config_options", '-DPYVERSIONS_EXE=/fake_python_exe -DPYTHON_INCLUDE_PATH=${PYTHON_INSTALL_DIR}/include/python${PYTHON_VERSION} -DPARAVIEW_USE_MPI=ON -DHDF5_IS_PARALLEL:BOOL=ON -DPARAVIEW_ENABLE_CATALYST:BOOL=ON -DPARAVIEW_BUILD_PLUGIN_CatalystScriptGeneratorPlugin:BOOL=ON -DMPI_CXX_LIBRARIES:FILEPATH='+mpi_dir_lib+'libmpi_cxx.so -DMPI_C_LIBRARIES:FILEPATH='+mpi_dir_lib+'libmpi.so -DMPI_LIBRARY:PATH='+mpi_dir_lib+' -DCMAKE_SHARED_LINKER_FLAGS="-L${LD_LIBRARY_PATH} -lmpi_cxx -lmpi " -DCMAKE_EXE_LINKER_FLAGS="-L${LD_LIBRARY_PATH} -lmpi_cxx -lmpi" ')

          #MPI options for GUI
          yamm_project.set_software_option('GUI', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('GUI', "software_additional_config_options", "-DWITH_MPI=${LD_LIBRARY_PATH} ")

          #MPI options for MEDFichier
          yamm_project.set_software_option('MEDFICHIER', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('MEDFICHIER', "software_additional_config_options", "--enable-parallel --with-swig=yes ")

          #Global option for PADDER: so far there is no simple solution to have a uniform PADDER build config compatible with and without MPI.
          yamm_project.set_global_option('software_remove_list', ['PADDER'])

          #MPI options for SMESH
          yamm_project.set_software_option('SMESH', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('SMESH', "software_additional_config_options", '-DSALOME_USE_MPI=ON -DCMAKE_SHARED_LINKER_FLAGS="-L${LD_LIBRARY_PATH} -lmpi_cxx -lmpi" -DCMAKE_EXE_LINKER_FLAGS="-L${LD_LIBRARY_PATH} -lmpi_cxx -lmpi" ')

          #MPI options for MED
          yamm_project.set_software_option('MED', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('MED', "software_additional_config_options", "-DSALOME_USE_MPI=ON -DSALOME_MED_ENABLE_PARTITIONER=OFF -DSALOME_MED_ENABLE_RENUMBER=OFF -DSALOME_BUILD_TESTS=OFF -DSALOME_BUILD_DOC=OFF -DSALOME_MED_STANDALONE=OFF -DSALOME_MED_MICROMED=OFF ")

          #MPI options for PARAVIS
          yamm_project.set_software_option('PARAVIS', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('PARAVIS', "software_additional_config_options"," -DHDF5_IS_PARALLEL:BOOL=ON -DSALOME_PARAVIS_BUILD_PLUGINS=ON ")

          #MPI options for GHS3DPRLPLUGIN
          yamm_project.set_software_option('GHS3DPRLPLUGIN', 'software_additional_env', 'export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('GHS3DPRLPLUGIN', "software_additional_config_options",' -DCMAKE_SHARED_LINKER_FLAGS="-L${LD_LIBRARY_PATH} -lmpi_cxx -lmpi" -DCMAKE_EXE_LINKER_FLAGS="-L${LD_LIBRARY_PATH} -lmpi_cxx -lmpi" ')

          #MPI options for CYTHON 
          yamm_project.set_software_option('CYTHON', 'software_additional_env', 'export CFLAGS=-l'+mpi_dir)

          #MPI options for H5PY 
          yamm_project.set_software_option('H5PY', 'software_additional_env', 'export CC='+mpi_dir_bin+'mpicc ; export OPENMPIHOME='+mpi_dir_lib)
          yamm_project.set_software_option('H5PY', "software_additional_config_options",' --hdf5-version=1.8.14 --mpi ')
          

          # Execution
          yamm_project.print_configuration()
          ret = yamm_project.start()
          if ret:
            yamm_project.create_appli()
       else:
            print('The salome installation dir is not valid.')
   else:
        print('There is no valid MPI installation in that directory.')
else:
     print('Usage : python V7_7_1-WithMPI.py <mpi_dir> <salome_dir>')
     sys.exit(2)

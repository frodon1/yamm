#!/usr/bin/env python
# encoding: utf8
from __future__ import absolute_import, print_function, unicode_literals

import codecs
import os


__author__ = "Gilles DAVID - 2017"


def run(yamm_project, version, version_directory, softwares_user_version=None,
        use_system_version=True, softwares=None,
        package_name='', package_version='', package_maintainer='', package_description='',
        build_depends='', depends=''):
    """
    Liste les paquets Debian nécessaires à une installation SALOME.
    Peut aussi vérifier si tous les paquets sont installés (check=True)
    Créé un méta-paquet prêt à être installé sur Debian.

    Script à exécuter depuis un envorinnement SALOME chargé (salome shell).
    """
    basedir = os.path.join(os.path.curdir, 'depends_salome')
    if not os.path.exists(basedir):
        os.makedirs(basedir)
    yamm_project.set_version(version)
    yamm_project.set_global_option('version_directory', version_directory)
    yamm_project.set_global_option('use_system_version', use_system_version)
    if softwares_user_version:
        yamm_project.set_global_option('softwares_user_version', softwares_user_version)
    yamm_project.set_global_option('write_soft_infos', False)

    if depends:
        if not isinstance(depends, list):
            depends = depends.split()
        depends = [d for d in depends if d.strip()]

    if not depends:
        print("Récupération des paquets utilisés pour la compilation")
        used_packages = yamm_project.get_debian_packages()
        with codecs.open(os.path.join(basedir, 'used_packages.txt'), 'w') as f:
            f.writelines(', '.join(used_packages))

        print("Récupération des paquets installés pour l'exécution")
        depends_packages = []
        ret = yamm_project.compute_debian_runtime_packages_from_softwares(softwares)
        if ret is not None:
            for softname, packages in list(ret.items()):
                depends_packages += [p for p in packages if p not in used_packages + depends_packages]
                with codecs.open(os.path.join(basedir, '%s_depends.txt' % softname.lower()), 'w') as f:
                    f.writelines('\n'.join(packages))
                    f.write('\n')
        print('\nUsed packages:\n%s' % ' '.join(used_packages))
        print('\nDepends packages:\n%s' % ' '.join(depends_packages))
        with codecs.open(os.path.join(basedir, 'depends_packages.txt'), 'w') as f:
            f.writelines(', '.join(depends_packages))
        depends = used_packages + depends_packages

    print('Creation of meta-package')
    if not package_description.endswith('\n'):
        package_description += '\n'

    yamm_project.create_meta_package(name=package_name,
                                     version=package_version,
                                     maintainer=package_maintainer,
                                     description=package_description,
                                     depends=depends)


if __name__ == "__main__":
    import sys
    import argparse
    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  os.pardir, os.pardir, 'src')
    sys.path.append(yamm_directory)
    from yamm.projects.salome.project import Project

    version = 'V8_2_0'
    version_directory = os.path.join(os.path.expanduser('~'), 'salome', '8.2_system')
    # OTGUI V8_2_0 links to python3 => use dedicated branch
    customversion = {'OTGUI': 'gdd/detect_python_27 V8_2_0'}

    use_system_version = False
    package_name = 'salome-prerequisites-8.2.0'
    package_version = '8.2.0'
    package_maintainer = 'Gilles DAVID <gilles-g.david@edf.fr>'
    package_description = """Pré-requis pour SALOME 8.2.0
 Ce méta-paquet regroupe tous les paquets nécessaires pour exécuter SALOME 8.2.0."""

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', '--version', help='SALOME version', default=version)
    parser.add_argument("--customversion", action='append',
                        help='Softwares custom versions, format: key=value',
                        type=lambda kv: kv.split("=", 1),
                        default=list(customversion.items()))
    parser.add_argument('--version-directory', dest='version_directory',
                        help='SALOME version directory',
                        default=version_directory)
    parser.add_argument('-s', dest='use_system_version', action="store_true",
                        help='YAMM should use system packages')
    parser.add_argument('--package-name', dest='package_name',
                        help='Package name', default=package_name)
    parser.add_argument('--package-version', dest='package_version',
                        help='Package version', default=package_version)
    parser.add_argument('--package-maintainer', dest='package_maintainer',
                        help='Package maintainer', default=package_maintainer)
    parser.add_argument('--package-description', dest='package_description',
                        help='Package description', default=package_description)
    parser.add_argument("--softwares", nargs="+", metavar="SOFTWARE",
                        help="List of softwares (all if empty)", default='')
    parser.add_argument("--depends", nargs="+", metavar="PACKAGE",
                        help="List of debian packages dependencies. If empty, the list is computed by YAMM. It is time consuming",
                        default='')
    args = parser.parse_args()

    if not os.path.exists(args.version_directory):
        sys.exit("Le répertoire d'installation de SALOME n'existe pas: %s" % args.install_directory)

    run(Project(), args.version, version_directory=args.version_directory,
        softwares_user_version=dict(list(args.customversion)),
        use_system_version=args.use_system_version,
        softwares=args.softwares, package_name=args.package_name,
        package_version=args.package_version, package_maintainer=args.package_maintainer,
        package_description=args.package_description, depends=args.depends)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#


def main(yamm_project, version, use_system_version, softwares, basename):
    # Configuration
    yamm_project.set_version(version)
    yamm_project.set_global_option('separate_dependencies', True)
    yamm_project.set_global_option('write_soft_infos', False)
    yamm_project.set_global_option('use_system_version', use_system_version)
    yamm_project.set_global_option('software_minimal_list', softwares or [])

    for deptype in ['all', 'exec', 'build']:
        # Execution
        if softwares:
            for soft in softwares:
                yamm_project.set_global_option('software_minimal_list', [soft])
                dotfile = '%s_%s_%s.dot' % (basename, deptype, soft)
                pdffile = '%s_%s_%s.pdf' % (basename, deptype, soft)
                yamm_project.logger.info('Write %s deps graph for %s in %s' % (deptype, soft, ' and '.join((dotfile, pdffile))))
                yamm_project.plot_dot(deptype=deptype, dot=dotfile, pdf=pdffile,
                                      overwrite=True, only_compiled=True, label=soft)
        else:
            dotfile = '%s_%s.dot' % (basename, deptype)
            pdffile = '%s_%s.pdf' % (basename, deptype)
            yamm_project.logger.info('Write %s deps graph for all softwares in %s' % (deptype, ' and '.join((dotfile, pdffile))))
            yamm_project.plot_dot(deptype=deptype, dot=dotfile, pdf=pdffile,
                                  overwrite=True, only_compiled=True)

if __name__ == "__main__":
    import argparse
    import os
    import sys
    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  os.pardir, os.pardir, 'src')
    sys.path.append(yamm_directory)
    from yamm.projects.salome.project import Project

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', '--version', help='SALOME version', default='V8_3_0')
    parser.add_argument('-b', '--basename', help='Dot and pdf files base name',
                        default='/tmp/salome')
    parser.add_argument('-s', '--system-package', dest='use_system_version',
                        action="store_true",
                        help='YAMM should use system packages')
    parser.add_argument("--softwares", nargs="+", metavar="SOFTWARE",
                        help="Softwares minimal list (all if empty)", default='')
    args = parser.parse_args()

    main(Project(), args.version, args.use_system_version, args.softwares, args.basename)

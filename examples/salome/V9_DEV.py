#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

import multiprocessing
import sys
yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                              os.pardir, os.pardir, 'src')
sys.path.append(yamm_directory)

from yamm.projects.salome.project import Project

yamm_project = Project()

# Configuration
yamm_project.set_version('V9_DEV', 'python3')
yamm_project.set_global_option('top_directory', '/home/J99972/salome_py3')
yamm_project.set_global_option('version_directory', '/home/J99972/salome_py3/V9_DEV')
yamm_project.set_global_option('parallel_make', str(multiprocessing.cpu_count())))
yamm_project.set_global_option('use_pleiade_mirrors', False)
yamm_project.set_global_option('modules_debug_mode', True)
yamm_project.set_global_option('separate_dependencies', True)
yamm_project.set_global_option('tools_debug_mode', True)
yamm_project.set_global_option('write_soft_infos', False)
# yamm_project.set_global_option('use_system_version', True)
yamm_project.set_category_option('tool', "clean_build_if_success", False)
yamm_project.set_category_option('prerequisite', "clean_build_if_success", True)
#yamm_project.set_software_option('PARAVIEW', "use_pleiade_mirrors", True)
#yamm_project.set_software_option('PARAVIEW', "source_type", 'remote')

# Execution
yamm_project.print_configuration()
ret = yamm_project.start()
if ret:
    ret = yamm_project.create_appli()
sys.exit(ret is False)

#!/usr/bin/env python
__author__ = "Renaud BARATE - 2012"


def run(salome_project):
    salome_project.set_version("DEV")

    # Configuration
    salome_project.set_global_option('use_pleiade_mirrors', True)
    salome_project.options.set_global_option("parallel_make", "8")
    salome_project.options.set_global_option("run_tests", True)
    salome_project.print_configuration()

    # Execution
    return salome_project.start()


if __name__ == "__main__":
    import os
    import sys
    yamm_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  os.pardir, os.pardir, 'src')
    sys.path.append(yamm_directory)

    from yamm.projects.salome.project import Project

    ret = run(Project())
    sys.exit(ret is False)

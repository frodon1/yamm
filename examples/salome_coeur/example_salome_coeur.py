#!/usr/bin/env python

import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../src"
sys.path.append(yamm_directory)

from yamm.projects.salome_coeur.project import Project
#from yamm.core.base.misc import VerboseLevels

salome_coeur_project = Project()
salome_coeur_project.set_version("DEV")

# Configuration
#salome_coeur_project.options.set_global_option("top_directory", os.path.join(os.getenv("HOME"), "salome_coeur"))
#salome_coeur_project.options.set_global_option("parallel_make", "4")
#salome_coeur_project.options.set_global_option("run_tests", True)
#salome_coeur_project.options.set_global_option("run_version_tests", False)
#salome_coeur_project.options.set_global_option("command_verbose_level", VerboseLevels.DEBUG)

salome_coeur_project.print_configuration()
salome_coeur_project.start()
salome_coeur_project.create_movable_installer()

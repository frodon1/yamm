#!/usr/bin/env python

import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../src"
sys.path.append(yamm_directory)

from yamm.projects.salome_hydro.project import Project

yamm_project = Project()

# Configuration
yamm_project.set_version('DEV')
yamm_project.set_global_option('parallel_make', '8')

# Build platform
yamm_project.start()

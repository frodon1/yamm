#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import input
from builtins import str
from builtins import object
__author__ = "Mathieu Courtois - 2015"

USAGE = """
    %prog [options] version

Build an installer for Salome-Meca

Example:
    %prog --build_directory=/tmp/build --package=V2015_2 -j 6 V7_DEV
    %prog --build_directory=/tmp/build --integr --norun --user_soft MT:tip DEV
"""

import os
import os.path as osp
import sys
from optparse import OptionParser
import tempfile
import platform
import datetime

HERE = osp.dirname(osp.abspath(__file__))
YAMM_DIRECTORY = osp.join(HERE, os.pardir, "src")
sys.path.append(YAMM_DIRECTORY)

from yamm.core.base.misc import VerboseLevels
from yamm.projects.salome_meca.project import Project
from yamm.projects.salome_meca import templates


# flavours
# - keys are only aliases,
# - only the values are really used
FLAVOURS = {
    "calibre": "calibre_9", # default calibre distribution
    "calibre_7" : "calibre_7",
    "public": "public",
    # shortcuts
    "Pre_C7":     "codeaster_prerequisites_calibre_7",
    "Pre_C7_mpi": "codeaster_prerequisites_calibre_7_mpi",
    "Pre_C9":     "codeaster_prerequisites_calibre_9",
    "Pre_C9_mpi": "codeaster_prerequisites_calibre_9_mpi",
    "Pre_Centos": "codeaster_prerequisites_centos_32",
}

# default splash screen
SPLASH = osp.join(HERE, "salome_meca", "icone-smeca.png")

# si integration : Environnement utilise
# INTEGR_DIR : Build directory
# HG_DIR : Repertoire des dépots Mercurial en local
INTEGR_DIR = os.getenv("INTEGR_DIR")
HG_DIR = os.getenv("HG_DIR")

# integration dict_depot={"name": (port,repertoire,isModule)}

DICT_DEPOT={"OMA":("10000","salome-oma",True),"MT":("10001","salome-rotor",True),"EDYOS":("10002","salome-edyos",True),\
           "EUROPLEXUS":("10003","salome-europlexus",True),"GVB":("10004","salome-gevibus",True),"SALOMEMECA_AC":("10005","salome-meca-ac",False),\
           "SALOMEMECA_DHRC":("10006","salome-meca-dhrc",False),"SALOMEMECA_ORT":("10007","salome-meca-ort",False),\
           "SALOMEMECA_PLUGINS":("10008","salome-meca-plugins",False),"SALOMEMECA_MOFEM":("10012","salome-meca-mofem",False),\
           "YACS_EDYOS":("10009","salome-yacs-edyos",True),"SALOMEMECA_RSTE":("10010","salome-meca-rste",False),\
           "SALOMEMECA_ARCADE":("10011","salome-meca-arcade",False),"SALOMEMECA_MAP":("10014","salome-map",False),"SALOMEMECA_TRANSLATOR":("10013","salome-codeaster-translator",False)}

class SalomeMecaBuild(object):
    """Salome-Meca builder"""

    def __init__(self):
        """initialisation"""
        # define the arguments parser
        # short options names are kept for compatibility
        parser = OptionParser(usage=USAGE, prog=osp.basename(sys.argv[0]))
        parser.add_option('-g', '--debug', action='store_true',
                          help="turn on debug informations")
        parser.add_option('-n', '--package', metavar='PKG',
            help="define the package name (by default use the `version` argument)")
        parser.add_option('-b', '--build_directory', metavar="DIR",
            help="define the build directory")
        parser.add_option('-i', '--install_directory', metavar="DIR",
            help="define the installation directory")
        parser.add_option('-v', '--flavour', default="calibre", metavar="NAME",
            help="use a flavour, a variation of a version (default: calibre)")
        parser.add_option('-e', '--environment', metavar="FILE",
            help="use an environment file, sourced before compiling softwares. "
                 "Example: /projets/projets.002/simumeca.033/etc/codeaster/profile_impi.sh")
        parser.add_option('-p', '--splash', default=SPLASH, metavar="FILE",
            help="path to the image file used as splash screen "
                 "(default: %s)" % SPLASH)
        parser.add_option('--nodownload', dest="download",
            default=True, action="store_false",
            help="do not download the softwares (use them as they were "
                 "already downloaded).")
        parser.add_option('--noprompt', dest="prompt",
            default=True, action="store_false",
            help="automatic yes to prompts; assume 'yes' as answer to all "
                 "prompts and run non-interactively.")
        parser.add_option('--universal', action="store_true",
            help="create an universal distribution.")
        parser.add_option('--continue_on_failed', action="store_true", default=False,
            help="continue compilation even if some software compilation failed.")
        parser.add_option('-j', '--jobs',
            help="number of jobs run simultaneously, passed to make "
                 "(default: number of cores)")
        parser.add_option('--remove',
            help="list of softwares to remove from the installation list"
                 "(comma separated)")
        parser.add_option('--minimal',
            help="only this list of softwares will be installed"
                 "(comma separated)")
        parser.add_option('--integr', action="store_true",
            help="construction with mode integration")
        parser.add_option('--user_soft',
            help="list of user revision software"
                 "(soft:revision with comma separated)")
        parser.add_option('--delete', dest="delete",
            default=False, action="store_true",
            help="delete the modules, prerequisites and tools directories")
        parser.add_option('--norun',action="store_true",
            help="suppression de la construction du movable installer")
        parser.add_option('--publish',action="store_true", default=False,
            help="publish the movable installer to Pleiade FTP server")

        self.project = None
        self.parser = parser
        self.opts = None
        self.version = None
        self.package = None
        self.universal = False
        self.distrib = ""
        self.public = False
        # build only the prerequisites (no appli)
        self.only_prereq = False

    def parse_args(self):
        """Parse command line arguments and set default values"""
        parser = self.parser
        opts, args = parser.parse_args()
        self.opts = opts
        if len(args) != 1:
            parser.error("installer takes the version name as unique argument")
        self.version = args[0]
        self.package = opts.package or self.version
        opts.flavour = FLAVOURS.get(opts.flavour) or opts.flavour
        if not opts.flavour:
            flavours = list(set(FLAVOURS.values()))
            flavours.sort()
            parser.error("unknown flavour '%s', must be one of: %s" \
                % (opts.flavour, ', '.join(flavours)))
        if "codeaster_prerequisites" in opts.flavour:
            self.only_prereq = True
        if "clap0f0q" in platform.node() and not self.only_prereq:
            parser.error("only codeaster_prerequisites is supported on this server")
        if opts.integr :
            if not opts.user_soft : parser.error("--user_soft is required if --integr")
            if not HG_DIR : parser.error("Environnement variable HG_DIR required if --integr")
        if not opts.build_directory:
            if opts.integr :
               if not INTEGR_DIR : parser.error("Environnement variable INTEGR_DIR required if no --build_directory")
               opts.build_directory=INTEGR_DIR
            else :
               parser.error("--build_directory is required!")
        self._makedirIfNotExist(opts.build_directory)
        if not opts.install_directory:
            opts.install_directory = osp.join(opts.build_directory, "installer")
        if not opts.jobs:
            opts.jobs = _get_number_of_threads()
        self.universal = opts.universal or False
        if "public" in opts.flavour:
            self.universal = True
            self.public = True
        if self.universal:
            self.distrib = "jessie"
        if not osp.isfile(self.opts.splash):
            parser.error("no such file: %s" % self.opts.splash)

    def manage_port(self,action,list_pid=None):
        """Manage mercurial port omening or closing"""
        # use the parser object to report errors


        import subprocess

        opts = self.opts
        list_integr = list(comma_dict(opts.user_soft).keys())

        # ouverture port
        if action =='start':
            list_pid = []

            for d in list_integr :
                if d not in DICT_DEPOT :
                    self.parser.error("software non integrable : %s" % d)
                else :
                    os.chdir(os.path.join(HG_DIR,DICT_DEPOT[d][1]))
                    p=DICT_DEPOT[d][0]
                    cmd=["hg","serve","--port",str(p)]
                    p1 = subprocess.Popen(cmd, shell=False)
                    list_pid.append(p1.pid)
                    print("ouverture ",d, "port ",p)

            return list_pid

        #fermeture port
        else :
            for p in list_pid :
                cmd="kill -9 "+str(p)
                subprocess.Popen(cmd, shell=True)
            return

    def add_prefix_integr(self,dico):
        """Add prefix integr for user software revision"""
        opts = self.opts
        if opts.integr :
            dico_integr={}
            for k,v in list(dico.items()) : dico_integr[k]="integr_"+v
            return dico_integr
        else :
           return dico



    def configure(self):
        """Configure the project"""
        opts = self.opts
        proj = self.project = Project()
        proj.set_version(self.version, opts.flavour, self.package)
        # On rajoute la version du projet Salome pour install_directory (ex. V7_main)
        opts.install_directory = osp.join(opts.install_directory, self.package)
        self._makedirIfNotExist(opts.install_directory)
        # add options to the project
        proj.set_global_option("top_directory", opts.build_directory)
        proj.set_global_option("smeca_installer_directory", opts.install_directory)
        proj.set_global_option("parallel_make", str(opts.jobs))

        proj.options.set_global_option("continue_on_failed", opts.continue_on_failed)

        verbosity = VerboseLevels.WARNING
        if opts.debug:
            verbosity = VerboseLevels.DEBUG
        proj.set_global_option("command_verbose_level", verbosity)
        proj.set_global_option("smeca_public_build", self.public)
        proj.set_global_option("smeca_universal", self.universal)
        proj.set_global_option("smeca_distrib", self.distrib)
        self.set_software_minimal_list(comma_list(opts.minimal))
        self.remove_softwares(comma_list(opts.remove))

        if opts.integr :
            user_soft = self.add_prefix_integr(comma_dict(opts.user_soft))
        else :
            user_soft = comma_dict(opts.user_soft)
        self.change_softwares_version(user_soft)

        if opts.environment:
            proj.set_global_option("software_additional_env_file", opts.environment)

        if opts.integr :
            list_integr = list(comma_dict(opts.user_soft).keys())
            proj.delete_directories(software_list=list_integr,delete_src=True, delete_build=True, delete_install=True)


        if opts.delete:
          proj.delete_prerequisite_directories()
          proj.delete_directories(delete_src=True, delete_build=True, delete_install=True)


        #Use of pleiade mirrrors to avoid proxy issues
        proj.set_global_option("use_pleiade_mirrors", True)

    def run(self):
        """Build the archive"""
        self.parse_args()
        self.configure()
        proj = self.project
        self.project.print_configuration()
        if self.opts.prompt:
            cont=input("\nDo you want to continue ([y]/n) ? ")
            if 'n' in cont.lower():
                return

        # open ports Mercurial if integr
        if self.opts.integr:
            list_pid=self.manage_port('start')
            if self.opts.prompt:
                cont=input("\nDo you want to continue ([y]/n) ? ")
                if 'n' in cont.lower():
                    self.manage_port('close',list_pid)
                    return

        # dowload & compile
        try :
            if self.opts.download:
                assert proj.download(), "download failed!"
            assert proj.start(), "make failed!"
        except AssertionError:
            if self.opts.integr:
                self.manage_port('close',list_pid)
            raise

        # close ports Mercurial if integr
        if self.opts.integr :
            self.manage_port('close',list_pid)

        # generate the appli or just the environment files
        if not self.only_prereq:
            proj.create_appli()

        # create the installer
        if not self.opts.norun:
            self.installer = proj.create_movable_installer( **self._installer_options() )

        # publish the installer to ftp pleiade
        if self.opts.publish:
          platform="Calibre-9"
          if ("calibre_7") in self.opts.flavour:
              platform="Calibre-7"
          if ("public") in self.opts.flavour:
              platform="Calibre-9_univ"
          proj.publish_installer(installer_name=self.installer, platform=platform, remote_repository="Projets/SMECA")

        # rename .run for code-aster prerequisites installer
        if  self.only_prereq:
            now = datetime.datetime.now()
            if "mpi" in self.opts.flavour:
                suffix='_mpi'
            else:
                suffix=''
            os.system("mv "+self.opts.install_directory+".run "+self.opts.install_directory+'-'+now.strftime('%Y%m%d')+suffix+".run")
            os.system("mv "+self.opts.install_directory+".tar.gz "+self.opts.install_directory+'-'+now.strftime('%Y%m%d')+suffix+".tar.gz")
            os.system("mv "+self.opts.install_directory+".run.sha1 "+self.opts.install_directory+'-'+now.strftime('%Y%m%d')+suffix+".run.sha1")

        return True

    def _installer_options(self):
        """Return optional arguments for the installer"""
        instdir = self.project.get_global_option("smeca_installer_directory")
        args = {
            'universal': self.universal,
            'distrib': self.distrib,
            'public': self.public,
            'installer_topdirectory': instdir,
            'additional_commands': self._additional_commands(),
            'final_custom_command': self._post_install_command(),
        }
        if self.only_prereq:
            if "mpi" in self.opts.flavour:
                suffix='_mpi'
            else:
                suffix=''
            args['delete'] = False
            args['default_install_dir'] = "${HOME}/dev/codeaster-prerequisites"
            now             = datetime.datetime.now()
            postinstall_cmd = args['final_custom_command']
            postinstall_cmd += " cd ${SALOME_HOME}"
            postinstall_cmd += "; rm -r plugins salome_hpc_visu_porthos.py README  salome_context.cfg  salome_modules.sh "
            postinstall_cmd += "; rm -r -f create_appli_"+self.package+".sh prerequisites/Cmake* salome_post_install.py "
            postinstall_cmd += "; grep -v Cmake salome_prerequisites.sh > temp ; mv temp salome_prerequisites"+suffix+"_"+now.strftime('%Y%m%d')+".sh"
            postinstall_cmd += "; rm salome_prerequisites.sh ; ln -s salome_prerequisites"+suffix+"_"+now.strftime('%Y%m%d')+".sh salome_prerequisites.sh"
            if "mpi" in self.opts.flavour:
                postinstall_cmd += "; cd tools/Code_aster_frontend-salomemeca/etc/codeaster ; echo localhost > mpi_hostfile "
                postinstall_cmd += "; sed 's/mpi_get_procid_cmd : echo $PMI_RANK/mpi_get_procid_cmd : echo $OMPI_COMM_WORLD_RANK/g' asrun > asrun_tmp && mv asrun_tmp asrun"
            args['final_custom_command'] =postinstall_cmd
            args['default_name'] = "Codeaster"+suffix+"-prerequisites"
        else:
            args['default_install_dir'] = "${HOME}/salome_meca"
            args['default_exec'] = "salome"
            args['default_name'] = "salome_meca"
            args['default_icon_path'] = self.opts.splash
        return args

    def _additional_commands(self):
        """Return the additional commands to be run before creating the archive"""
        commands = []
        proj = self.project
        if self.only_prereq:
            commands.append( self._substitute(templates.empty))
        else:
            commands.append( self._substitute(templates.custom_appli) )
        return commands

    def _post_install_command(self):
        """Return the postinstall script to be run by the installer after
        the installation"""
        proj = self.project
        if self.only_prereq:
            command = self._substitute(templates.post_install_prerequisites)
        else:
            command = self._substitute(templates.post_install_appli)
        return command

    def _substitute(self, template):
        """Performs the template substitution"""
        # this will force to always use the same variables in templates
        proj = self.project
        instdir = proj.get_global_option("smeca_installer_directory")
        values = {
            'version': proj.version,
            'package': self.package,
            'installer_directory': instdir,
            'base_installer_directory': osp.basename(instdir),
            # do not call _substitute() recursively
            'custom_appli_py' : templates.custom_appli_py.substitute(),
        }
        return template.substitute(**values)

    def set_software_minimal_list(self, minimal):
        """Restrict the list of softwares"""
        if minimal:
            self.project.options.set_global_option("software_minimal_list", minimal)

    def remove_softwares(self, removed):
        """Remove some softwares"""
        if removed:
            self.project.options.set_global_option("software_remove_list", removed)

    def change_softwares_version(self, changed_version):
        """Use different version for some softwares"""
        if changed_version:
            self.project.options.set_global_option("softwares_user_version", changed_version)


    def _makedirIfNotExist(self, directory):
        """Create a directory if it does not exist"""
        # use the parser object to report errors
        if not osp.isdir(directory):
            try:
                os.makedirs(directory)
            except OSError:
                self.parser.error("can not create directory: %s" % directory)
        if not os.access(directory, os.W_OK):
            self.parser.error("write access is needed in %s" % directory)


def _get_number_of_threads():
    """Return the number of threads, passed to make"""
    nbthread = 1
    try:
        nbthread = open('/proc/cpuinfo').read().count('processor\t:')
    except IOError:
        try:
            import psutil
            nbthread = psutil.NUM_CPUS
        except ImportError:
            pass
    return nbthread

def comma_list(comma_separated):
    """Return the list of values by splitting by a comma"""
    if comma_separated:
        return comma_separated.split(',')
    return ''

def comma_dict(comma_separated):
    """Return the dict of values by splitting by a comma"""
    if comma_separated:
        dico={}
        for j in comma_list(comma_separated) :
            dico[j.split(':')[0]]=j.split(':')[1]
        return dico
    return ''


if __name__ == '__main__':
    build = SalomeMecaBuild()
    build.run()

#
# set environment to build the 'integration' version of salome-meca and salome-meca_integr build
#
# Ask for intergartion configuration
echo -n "Entrer la liste d'intégration (exemple:MT:tip,SALOMEMECA_AC:tip): "
read list_integr
echo "On intègre :  $list_integr"
echo -n "Entrer la liste des softwares a ne pas construire (exemple:EDFDOC,...): "
read list_nobuild
if [ -z "$list_nobuild" ] ; then
    opts1=''
else
    opts1='--remove'
fi

echo "On ne construit pas :  $list_nobuild"
echo -n "On reconstruit code_aster (o/n) (defaut=non)? "
read aster
echo -n "On construit le .run (o/n) (defaut=non)? "
read run
if [ "$run" = "o" ]; then
    opts=''
else
    opts='--norun'
fi

echo -n "On lance les tests (defaut=oui)? "
read Stest
# Delete code_aster last construction if code_aster rebuild is asked
if [ "$aster" = "o" ]; then
    echo -n "On reconstruit code_aster"
    rm -rf /home/$USER/smeca/yamm_build/smeca_V8_integr_build/V8_INTEGR_calibre_9/tools/*/Code_aster*
fi
# Construction of Salome-Meca with yamm procedure and options previously set
if [ -z "$list_integr" ] ; then
    python salome_meca_installer.py --build_directory /home/$USER/smeca/yamm_build/smeca_V8_integr_build/ --install_directory /home/$USER/smeca/yamm_build/smeca_V8_integr_install/ $opts $opts1 $list_nobuild V8_INTEGR -j8
else
    export HG_DIR=/home/$USER/smeca/dev/integration/
    python salome_meca_installer.py --build_directory /home/$USER/smeca/yamm_build/smeca_V8_integr_build/ --install_directory /home/$USER/smeca/yamm_build/smeca_V8_integr_install/ $opts $opts1 $list_nobuild  --integr --user_soft=$list_integr  V8_INTEGR -j8
fi
# Integration tests
if [ "$Stest" != "n" ]; then
    if [ "$run" = "o" ]; then
        echo -n "On installe Salome-Meca"
        rm -rf    /home/$USER/smeca_integr
        /home/$USER/smeca/yamm_build/smeca_V8_integr_install/V8_INTEGR.run -t /home/$USER/smeca_integr -l English
        echo "On lance les tests d'integration de Salome-Meca"
        /home/$USER/smeca_integr/appli_V8_INTEGR/salome test -L "SMECA_INTEGR"
        echo -n "Validation graphique (o/n)?"
        read rgraph
        if [ "$rgraph" = "o" ] ; then
            /home/$USER/smeca_integr/appli_V8_INTEGR/salome
        fi 
    else 
        echo "On lance les tests d'integration de Salome-Meca"
        export ASTER_TMPDIR=/local00/tmp
        /home/$USER/smeca/yamm_build/smeca_V8_integr_build/V8_INTEGR_calibre_9/appli_V8_INTEGR/salome test -L "SMECA_INTEGR"
        echo -n "Validation graphique (o/n)?"
        read rgraph
        if [ "$rgraph" = "o" ] ; then
            /home/$USER/smeca/yamm_build/smeca_V8_integr_build/V8_INTEGR_calibre_9/appli_V8_INTEGR/salome
        fi 
    fi
else 
    if [ "$run" = "o" ]; then
        echo -n "Validation graphique (o/n)?"
        read rgraph
        if [ "$rgraph" = "o" ] ; then
             echo -n "On installe Salome-Meca"
             rm -rf    /home/$USER/smeca_integr
             /home/$USER/smeca/yamm_build/smeca_V8_integr_install/V8_INTEGR.run -t /home/$USER/smeca_integr -l English
             /home/$USER/smeca_integr/appli_V8_INTEGR/salome
        fi 
    else 
        echo -n "Validation graphique (o/n)?"
        read rgraph
        if [ "$rgraph" = "o" ] ; then
            /home/$USER/smeca/yamm_build/smeca_V8_integr_build/V8_INTEGR_calibre_9/appli_V8_INTEGR/salome
        fi 
    fi
fi

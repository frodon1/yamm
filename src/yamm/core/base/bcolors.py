#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

try:
    from builtins import object
except ImportError:
    pass
import sys


class Bcolors(object):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    DEBUG = '\033[94m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    @staticmethod
    def init():
        if not Bcolors.can_display_color(sys.stdout):
            Bcolors.disable()

    @staticmethod
    def can_display_color(stream):
        return hasattr(stream, "isatty") and stream.isatty()

    @staticmethod
    def disable():
        Bcolors.HEADER = ''
        Bcolors.OKBLUE = ''
        Bcolors.OKGREEN = ''
        Bcolors.DEBUG = ''
        Bcolors.WARNING = ''
        Bcolors.FAIL = ''
        Bcolors.ENDC = ''

    @staticmethod
    def set_light_background():
      """
      Adapt the color of log messages to a light background
      """
      Bcolors.HEADER = '\033[35m'
      Bcolors.OKBLUE = '\033[34m'
      Bcolors.OKGREEN = '\033[32m'
      Bcolors.DEBUG = '\033[34m'
      Bcolors.WARNING = '\033[33m'
      Bcolors.FAIL = '\033[31m'

Bcolors.init()


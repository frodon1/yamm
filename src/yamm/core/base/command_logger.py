#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import str


import datetime
import fileinput
import sys

from yamm.core.base.logger import BaseLog
from yamm.core.base.misc import Log, relpath


class CommandLog(BaseLog):

  def __init__(self, command_name,
               command_verbose_level,
               log_directory,
               print_console=False,
               write_in_file=False,
               command_files_basename="",
               version_name="",
               light_background=False):
    BaseLog.__init__(self, command_name,
                     command_verbose_level,
                     log_directory,
                     print_console,
                     write_in_file,
                     command_files_basename,
                     "",
                     light_background)

    self.logger = Log("[COMMAND LOG]")
    self.version_name = version_name

#######
#
# Framework part
#
#######

  def start_command(self, command_options):
    BaseLog.start_command(self)

    self.command_options = command_options

    if self.print_console:

      # Print command name
      self.print_info()
      self.print_info("Version %s - Starting command: %s" % (self.version_name,self.name))
      self.print_info()

      # Print command options
      self.print_info("Command options values:")
      self.print_info("-----------------------")
      for option in self.command_options._options:
        if self.command_options.has_global_option(option):
          self.print_info("%s [global]: %s" % (option, self.command_options.get_global_option(option)))
        for category in self.command_options._categories:
          if self.command_options.has_category_option(option, category):
            self.print_info("%s [category: %s]: %s" % (option, category,
              self.command_options.get_category_option(category, option)))
        for software in self.command_options._software_opt:
          if self.command_options.is_software_option(software, option):
            self.print_info("%s [software: %s]: %s" % (option, software,
              self.command_options.get_option(software, option)))

    if self.write_in_file:
      try:
        with open(self.report_filename, 'a') as command_file:
          intro_str = "Version %s - Log for command: %s" % (self.version_name,self.name)
          intro_sub = "=" * len(intro_str)
          self.write_in_reportfile(command_file, "\n", intro_str, intro_sub, "")
          self.write_in_reportfile(command_file, "\n", "ADD SUMMARY")
          self.write_in_reportfile(command_file, "\n", "")

          #opt_intro = "`Command options values`_"
          #opt_sub = "-" * len(opt_intro)
          #self.write_in_reportfile(command_file, "\n", opt_intro, opt_sub, "")
          #self.write_in_reportfile(command_file, "\n", "")
          #self.write_in_reportfile(command_file, "\n",".. _Command options values: javascript:hideshow(document.getElementById('hideAndDisplayOptions'))")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n",".. raw:: html")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n","  <div id='option-arrow' class='basarrowdivoption'>")
          self.write_in_reportfile(command_file, "\n","  <h2><a href=\"javascript:hideshow(document.getElementById('hideAndDisplayOptions'))\">Command options values</a></h2>")
          self.write_in_reportfile(command_file, "\n","  </div>")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n","  <div id='hideAndDisplayOptions'>")
          self.write_in_reportfile(command_file, "\n","")
      except IOError as e:
        self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

      # Get option values
      tab_list = []
      for option in self.command_options._options:
        if self.command_options.has_global_option(option):
          scope = "global"
          value = str(self.command_options.get_global_option(option))
          tab_list.append((option,scope,value))
        for category in self.command_options._categories:
          if self.command_options.has_category_option(option, category):
            scope = "category: %s" % category
            value = str(self.command_options.get_category_option(category, option))
            tab_list.append((option,scope,value))
        for software in self.command_options._software_opt:
          if self.command_options.is_software_option(software, option):
            scope = "software: %s" % software
            value = str(self.command_options.get_option(software, option))
            tab_list.append((option,scope,value))

      self.write_rst_tab_from_dict(tab_list, "Option Name", "Scope", "Value")

      try:
        with open(self.report_filename, 'a') as command_file:
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n",".. raw:: html")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n","  </div>")
          self.write_in_reportfile(command_file, "\n","")
      except IOError as e:
        self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

#######
#
# Executor part
#
#######

  def executor_begin(self, sorted_softwares, max_name_len, version_object):

    if self.print_console:
      self.print_info("")
      self.print_info("Starting execution:")
      self.print_info("-------------------")
      self.print_info("Ordered list of softwares to be executed:")
      for soft in sorted_softwares:
        self.print_info("  %s" % version_object.get_software_info(soft.name,
                                                                  max_name_len))
      self.print_info("")

    if self.write_in_file:
      try:
        with open(self.report_filename, 'a') as command_file:
          #res_intro = "`Software results`_"
          #res_sub = "-" * len(res_intro)
          #self.write_in_reportfile(command_file, "\n", res_intro, res_sub, "")
          #self.write_in_reportfile(command_file, "\n", "")
          #self.write_in_reportfile(command_file, "\n",".. _Software results: javascript:hideshow(document.getElementById('hideAndDisplayExecution'))")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n",".. raw:: html")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n","  <div id='soft-arrow' class='basarrowdivsoft'>")
          self.write_in_reportfile(command_file, "\n","  <h2><a href=\"javascript:hideshow(document.getElementById('hideAndDisplaySoft'))\">Software results</a></h2>")
          self.write_in_reportfile(command_file, "\n","  </div>")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n","  <div id='hideAndDisplaySoft'>")
          self.write_in_reportfile(command_file, "\n","")
      except IOError as e:
        self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

  def executor_end(self, executor, t_ini, soft_exe_tab, version_exe_tab,
                  number_ok_software, number_skipped_software, number_error_software,
                  software_list_in_error, errors, test_log_files = None, info_log_files = None,
                  version_test_log_files = None):

    if self.print_console:
      self.print_info("")
      self.print_info("Execution Summary:")
      self.print_info("------------------")
      self.print_info("Number of softwares: %s" % str(len(executor.sorted_executor_softwares)))
      self.print_info("Successfull: %s" % str(number_ok_software))
      self.print_info("Skipped: %s" % str(number_skipped_software))
      self.print_info("Error: %s" % str(number_error_software))
      self.print_info("Not Executed: %s" % str(len(executor.sorted_executor_softwares) - (number_ok_software+number_skipped_software+number_error_software)))
      self.print_info("Error list: %s" % str(software_list_in_error))
      self.print_info("Total execution is done in %s :-) !" % str(datetime.timedelta(seconds=t_ini)))

    if self.write_in_file:


      # Adding summary:
      summary  = "\n"
      summary += "Execution Summary:\n"
      summary += "------------------\n"
      summary += "- Number of softwares: %s\n" % str(len(executor.sorted_executor_softwares))
      summary += "- Successfull: %s\n" % str(number_ok_software)
      summary += "- Skipped: %s\n" % str(number_skipped_software)
      summary += "- Error: %s\n" % str(number_error_software)
      summary += "- Not Executed: %s\n" % str(len(executor.sorted_executor_softwares) - (number_ok_software+number_skipped_software+number_error_software))
      summary += "- Error list: %s\n" % str(software_list_in_error)
      summary += "- Total execution is done in %s :-) !\n" % str(datetime.timedelta(seconds=t_ini))

      for line in fileinput.input(self.report_filename, inplace = 1):
          sys.stdout.write(line.replace('ADD SUMMARY', summary))

      # Write Execution tab
      self.write_rst_tab_from_dict(soft_exe_tab, "Software", "State", "Nb. tests", "Tests results", "Time", "Software infos", "Comment")

      try:
        with open(self.report_filename, 'a') as command_file:
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n",".. raw:: html")
          self.write_in_reportfile(command_file, "\n","")
          self.write_in_reportfile(command_file, "\n","  </div>")
          self.write_in_reportfile(command_file, "\n","")
      except IOError as e:
        self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

      if test_log_files is not None:
        try:
          with open(self.report_filename, 'a') as command_file:
            for softname,test_log_file in list(test_log_files.items()):
              test_log_file_rel_path= relpath(test_log_file, self.log_directory)
              self.write_in_reportfile(command_file, "", ".. _%s tests log: %s"%(softname,test_log_file_rel_path), "\n")
        except IOError as e:
          self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

      if info_log_files is not None:
        try:
          with open(self.report_filename, 'a') as command_file:
            for softname,info_log_file in list(info_log_files.items()):
              info_log_file_rel_path= relpath(info_log_file, self.log_directory)
              self.write_in_reportfile(command_file, "", ".. _%s options: %s"%(softname,info_log_file_rel_path), "\n")
        except IOError as e:
          self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))


      if version_test_log_files is not None:
        with open(self.report_filename, 'a') as command_file:
          title = "Results for tests of version %s"%self.version_name
          self.write_in_reportfile(command_file, "\n", title)
          self.write_in_reportfile(command_file, "\n", "-"*len(title))

        self.write_rst_tab_from_dict(version_exe_tab, "Test suite", "Nb. tests", "Tests results", "Time", "Tests log")

        for version_test_suite_name,version_test_log_file in list(version_test_log_files.items()):
          try:
            with open(self.report_filename, 'a') as command_file:
              version_test_log_file_rel_path= relpath(version_test_log_file, self.log_directory)
              self.write_in_reportfile(command_file, "", ".. _%s tests log: %s"%(version_test_suite_name,version_test_log_file_rel_path), "\n")
          except IOError as e:
            self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

      if errors != "":
        try:
          with open(self.report_filename, 'a') as command_file:
            self.write_in_reportfile(command_file, "\n", "")
            self.write_in_reportfile(command_file, "\n", "Errors:")
            self.write_in_reportfile(command_file, "",   "-------", "\n")
            self.write_in_reportfile(command_file, "", errors, "\n")
        except IOError as e:
          self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))


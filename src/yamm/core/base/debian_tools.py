#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)
from __future__ import absolute_import

import os
import sys
import platform

CURDIR = os.path.dirname(os.path.abspath(__file__))

py_version = sys.version[:3]
yamm_default_dir = (CURDIR,) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
sys.path.append(yamm_src_dir)

from yamm.core.base import misc

DEPENDENCY_TYPES = [
    "Depends",
    "Recommends",
    "Suggests",
    "Replaces",
    "Enhances",
]

try:
    import apt
    import apt_pkg
    cache = apt.cache.Cache()
except ImportError:
    pyversion = platform.python_version_tuple()
    cache = None
    if pyversion[:2] == ('2', '6'):
        sys.path.append(os.path.join(CURDIR, 'yamm_apt_py26'))
        sys.path.append(CURDIR)
        try:
            from . import yamm_apt_py26 as apt
            from yamm_apt_py26 import apt_pkg
            cache = apt.cache.Cache()  # @UndefinedVariable
        except:
            pass
    elif pyversion[:2] == ('2', '7'):
        sys.path.append(os.path.join(CURDIR, 'yamm_apt'))
        sys.path.append(CURDIR)
        try:
            from . import yamm_apt as apt
            from yamm_apt import apt_pkg
            cache = apt.cache.Cache()  # @UndefinedVariable
        except:
            pass
    elif pyversion[0] == '3':
        sys.path.append(os.path.join(CURDIR, 'yamm_apt_py3'))
        sys.path.append(CURDIR)
        try:
            from . import yamm_apt_py3 as apt
            from yamm_apt_py3 import apt_pkg
            cache = apt.cache.Cache()  # @UndefinedVariable
        except:
            pass


def version_compare(a, b=''):
    # If version starts with [d]:, consider what's after the :
    a = a.split(':', 1)[-1]
    a = misc.simplify(a)
    # Replace '~' by '-' so that x.y~z > x.y
    return apt_pkg.version_compare(a.replace('~', '-'), b)


def exists(package):
    if cache is not None:
        return package in cache
    return None


def is_installed(package):
    if not exists(package):
        return

    return cache[package].is_installed


def soft_version_compare(package, version_min):
    version = get_system_version(package)
    if version is None:
        return -1
    return version_compare(version, version_min)


def get_system_version(package):
    if not is_installed(package):
        return
    return cache[package].installed.version


def extract_dependencies(package, dependency_type):
    """
    Generator that produce all the dependencies of a particular type
    """
    for dependency_list in package.candidate.get_dependencies(dependency_type):
        for dependency in dependency_list.or_dependencies:
            yield dependency.name


def get_package(pkg):
    if cache is None:
        return None
    try:
        return cache[pkg]
    except KeyError as error:
        pkg_split = pkg.split('-')
        if len(pkg_split) > 1:
            architectures = ('i386', 'i686', 'pic', 'x32', 'xen')
            if pkg_split[-1] in architectures:
                pkg = '-'.join(pkg_split[:-1])
                return get_package(pkg)
        sys.exit(error.args[0])


def minimize_dependencies(packages):
    # print("Original list:", packages)
    packages_to_remove = []

    for pkgname in packages:
        pkg = get_package(pkgname)
        for other_package in packages:
            if other_package == pkgname:
                continue
            other_package = get_package(other_package)
            for dependency_type in DEPENDENCY_TYPES:
                if pkg.shortname in extract_dependencies(other_package, dependency_type):
                    # print(pkgname, "is a dependency(", dependency_type, ") of", other_package.shortname)
                    packages_to_remove.append(pkgname)
                    break
            if pkgname in packages_to_remove:
                break
    # print(packages_to_remove)
    packages_minimal = list(set(packages).difference(set(packages_to_remove)))
    return packages_minimal

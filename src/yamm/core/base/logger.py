#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import range
from builtins import object
import datetime
import locale
import platform
import shutil
import subprocess
from time import localtime
from time import strftime

from yamm.core.base import misc
from yamm.core.base.bcolors import Bcolors
from yamm.core.base.misc import VerboseLevels


locale.setlocale(locale.LC_ALL, '')



class BaseLog(object):

  def __init__(self, name,
               verbose_level,
               log_directory,
               print_console=False,
               write_in_file=False,
               log_files_basename="",
               logo_directory="",
               light_background=False):

    self.name = name
    self.logger = misc.Log("[BASE LOG]")
    self.print_console = print_console
    self.write_in_file = write_in_file
    self.verbose_level = verbose_level
    self.log_directory = log_directory
    if logo_directory == "":
      self.logo_directory = os.path.join(self.log_directory, "_static")
    else:
      self.logo_directory = logo_directory
    self.log_files_basename = log_files_basename or strftime("%Y_%m_%d_%H_%M_%S_",
                                                             localtime())
    self.report_filename = self.get_report_rst_filename()
    self.report_html_filename = self.get_report_html_filename()
    self.log_filename = self.get_report_filename() + ".log"
    if light_background:
      Bcolors.set_light_background()

    # Init report log directory
    report_dirname = os.path.dirname(self.report_filename)
    if os.path.exists(report_dirname):
      # If is a directory -> ok
      if not os.path.isdir(report_dirname):
        self.logger.error("Log directory is not a directory !")
    else:
      try:
        os.makedirs(report_dirname)
      except:
        self.logger.exception("Cannot create the log directory !")

  def start_header(self, *args):
    if self.write_in_file: 
      logo_src = os.path.join(*(yamm_default_dir + ("doc", "source", "_static",
                              "yamm-inskcape.png")))
      logo = os.path.join(os.path.join(self.logo_directory, 
                                       "yamm-inskcape.png"))
      add_logo = ""
      if os.path.exists(logo_src):
        if not os.path.exists(self.logo_directory):
          os.makedirs(self.logo_directory)
        shutil.copy(logo_src, logo)
        add_logo = '<img src="{0}" alt="YAMM logo" />'.format(misc.relpath(logo, self.log_directory))
      try:
        with open(self.report_filename, 'w') as report_file:
          try:
            locale.setlocale(locale.LC_ALL, 'en_GB.utf8')
          except:
            pass
          thedate = datetime.datetime.today().strftime('%a, %x %X')
          locale.setlocale(locale.LC_ALL, '')
          report_file.write(""".. raw:: html

  <div style="background-color: white; text-align: left; padding: 10px 10px 15px 15px">
  {0}
  <script type="text/javascript">
    function hideshow(which){{
      if (!document.getElementById)
        return
      if (which.style.display!="none") {{
        which.style.display="none"
        if (which.id == "hideAndDisplayOptions")
          document.getElementById("option-arrow").setAttribute("class","gauchearrowdivoption")
        else if (which.id == "hideAndDisplaySoft")
          document.getElementById("soft-arrow").setAttribute("class","gauchearrowdivsoft")
      }}
      else {{
        which.style.display="block"
        if (which.id == "hideAndDisplayOptions")
          document.getElementById("option-arrow").setAttribute("class","basarrowdivoption")
        else if (which.id == "hideAndDisplaySoft")
          document.getElementById("soft-arrow").setAttribute("class","basarrowdivsoft")
      }}
    }}
  </script>

Log file generated on {1} on machine {2}

""".format(add_logo, thedate, platform.node()))
      except IOError as e:
        self.logger.exception('Impossible to write into the file {0},\
                          exception was: {1}'.format(self.report_filename, e))

  def start_command(self, *args):
    self.start_header()
    if self.write_in_file:
      try:
        with open(self.report_filename, 'a') as report_file:
          # For colors in html...
          report_file.write("""
.. role:: ok
.. |OK| replace:: :ok:`OK`
.. role:: error
.. |ERROR| replace:: :error:`ERROR`
.. role:: test-init-error
.. |TEST_INIT_ERROR| replace:: :test-init-error:`ERROR AT TEST INIT`
.. role:: known-error
.. |KNOWN_ERROR| replace:: :known-error:`KNOWN ERROR`
.. role:: skipped
.. |SKIPPED| replace:: :skipped:`SKIPPED`

""")
      except IOError as e:
        self.logger.exception("Impossible to write into the file %s, exception was: %s" % (self.report_filename, e))

  def end_command(self):

    if self.write_in_file and os.path.exists(self.report_filename):
      # Generates html file
      cssfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), "log.css")
      command = "rst2html --stylesheet={0} {1} {2}".format(cssfile,
                                                           self.report_filename,
                                                           self.report_html_filename)
      try:
        retcode = subprocess.call(command, shell=True)
        if retcode < 0:
          self.logger.error("rst2html command failed !")
      except OSError as e:
        self.logger.exception("rst2html command failed, exception was: %s" % e)

#######
#
# Usefull methods
#
#######

  def get_report_filename(self):
    return os.path.join(self.log_directory, 
                        self.log_files_basename + "_" + self.name.replace(" ", "_"))

  def get_report_rst_filename(self):
    return self.get_report_filename() + '.rst'

  def get_report_html_filename(self):
    return self.get_report_filename() + '.html'

  def write_in_reportfile(self, file_object, separator, *args):
    file_object.write(separator.join(args) + '\n')

  def write_in_consolefile(self, text="", space=0, middle_of_a_line=False):
    try:
      with open(self.log_filename, 'a') as file_object:
        if text != "":
          begin_text = " " * space
          text = begin_text + text
          if middle_of_a_line:
            file_object.write(text)
          else:
            file_object.write(text + '\n')
        else:
          file_object.write('\n')
    except IOError as e:
      self.logger.exception("Impossible to write into the file %s, exception was: %s" % (self.log_filename, e))

  def print_in_console(self, text="", space=0, middle_of_a_line=False):
    if text != "":
      begin_text = " " * space
      text = begin_text + text
      if middle_of_a_line:
        print(text, end=' ')
      else:
        print(text)
    else:
      print()
    sys.stdout.flush()

  """ The tab list is a like this
   [(line1_col1, line1_col2), (line2_col1, line2_col2)]
  """
  def write_rst_tab_from_dict(self, tab_list, *col_names):
    try:
      with open(self.report_filename, 'a') as report_file:
        col_number = len(col_names)
        cols_lgth = [0] * col_number
        for i, col in enumerate(col_names):
          cols_lgth[i] = len(col)

        for col in range(col_number):
          for line in tab_list:
            if len(line[col]) > cols_lgth[col]:
              cols_lgth[col] = len(line[col])

        # Writing start tab line
        tab_line = ""
        for lgth in cols_lgth:
          tab_line += "=" * lgth + " "
        self.write_in_reportfile(report_file, "", tab_line[:-1])
        names_line = ""
        for i, word in enumerate(col_names):
          if len(word) < cols_lgth[i]:
            word = word + " " * (cols_lgth[i] - len(word))
          names_line += word + " "
        self.write_in_reportfile(report_file, "", names_line[:-1])
        self.write_in_reportfile(report_file, "", tab_line[:-1])

        # Writing each line
        for line in tab_list:
          final_line = ""
          for i, word in enumerate(line):
            if len(word) < cols_lgth[i]:
              word = word + " " * (cols_lgth[i] - len(word))
            final_line += word + " "
          self.write_in_reportfile(report_file, "", final_line[:-1])

        # Writing end of tab
        self.write_in_reportfile(report_file, "", tab_line[:-1])
        self.write_in_reportfile(report_file, "\n", "")
    except IOError as e:
      self.logger.exception("Impossible to write into the file %s, exception was: %s" % (self.report_filename, e))

#######
#
# Méthodes permettant de gérer les sorties des traitements
#
#######

  def print_info(self, msg="", space=0, middle_of_a_line=False):
    if self.verbose_level >= VerboseLevels.INFO:
      self.print_in_console(msg, space, middle_of_a_line)
    self.write_in_consolefile(msg, space, middle_of_a_line)

  def print_debug(self, msg):
    if self.verbose_level >= VerboseLevels.DEBUG:
      begin_text = "- DEBUG "
      if self.name != "":
        begin_text += '[' + self.name + '] '
      text = begin_text + msg
      text = Bcolors.DEBUG + text + Bcolors.ENDC
      self.print_in_console()
      self.print_in_console(text)
      self.write_in_consolefile()
      self.write_in_consolefile(text)

  def print_warning(self, msg):
    if self.verbose_level >= VerboseLevels.WARNING:
      begin_text = "- WARNING "
      if self.name != "":
        begin_text += '[' + self.name + '] '
      text = begin_text + msg
      text = Bcolors.WARNING + text + Bcolors.ENDC
      self.print_in_console()
      self.print_in_console(text)
      self.write_in_consolefile()
      self.write_in_consolefile(text)

  def print_begin_work(self, text, space=0):
    begin_text = " " * space + "- "
    text = begin_text + text
    text = text + "..." + (" " * (50 - (len(text) + 3)))
    self.write_in_consolefile(text=text, middle_of_a_line=True)
    if self.verbose_level >= VerboseLevels.INFO:
      self.print_in_console(text=text, middle_of_a_line=True)
      pass

  def print_end_work(self, msg=""):
    text = Bcolors.OKGREEN + "[OK]"
    if msg == "":
      text += Bcolors.ENDC
    else:
      text += msg + Bcolors.ENDC
    self.write_in_consolefile(text)
    if self.verbose_level >= VerboseLevels.INFO:
      self.print_in_console(text)
      pass

  def print_end_work_with_warning(self, text):
    text = Bcolors.WARNING + "[WARNING] " + text + Bcolors.ENDC
    self.write_in_consolefile(text)
    if self.verbose_level >= VerboseLevels.WARNING:
      self.print_in_console(text)
      pass


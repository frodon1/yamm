#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import os
import sys
import platform

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_bin_dir = os.path.join(*(yamm_default_dir + ('bin',)))
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

try:
    from future import standard_library
    standard_library.install_aliases()
    # from builtins import str
    from builtins import bytes
    from builtins import zip
    from builtins import map
    from builtins import range
    from builtins import object
    from past.utils import old_div
    from past.builtins import str
    import urllib.request
    import urllib.error
    import urllib.parse
    import urllib.parse
except ImportError:
    pass

from netrc import netrc, NetrcParseError
from functools import partial, wraps
import hashlib
import posixpath
import re
import shlex
import shutil
import string
import subprocess
import tempfile
import traceback

from yamm.core.base.bcolors import Bcolors


if sys.version_info < (2, 6):
    sys.stderr.write("Error: Python version must be at least 2.6 to run yamm\n")
    sys.exit(1)

# Attribut du module utilisé
# par les fonctions du module
misc_debug = 0

CLUSTERS = {'porthos': '9',
            'eofront': '9'}


class PercentTemplate(string.Template):
    delimiter = '%'
    # Set idpattern to change what constitutes an 'identifier'


class PoundTemplate(string.Template):
    delimiter = '£'
    # Set idpattern to change what constitutes an 'identifier'


# Function to return a new type similar to the behavior of an enum
def enum(*sequential, **named):
    """
    >>> Numbers = enum(ONE=1, TWO=2, THREE='three')
    >>> Numbers.ONE
    1
    >>> Numbers.TWO
    2
    >>> Numbers.THREE
    'three'
    >>> Numbers = enum('ZERO', 'ONE', 'TWO')
    >>> Numbers.ZERO
    0
    >>> Numbers.ONE
    1
    """
    enums = dict(list(zip(sequential, list(range(len(sequential))))), **named)
    reverse = dict((value, key) for key, value in enums.items())
    enums['reverse_mapping'] = reverse
    return type('Enum', (), enums)

VerboseLevels = enum(QUIET=-1, WARNING=0, INFO=1, DEBUG=2, DRY_RUN=3)

#######
#
# Gestion multi-plateforme
#
#######

misc_platform = "LINUX"


def get_ld_library_path():
    if misc_platform == "BSD":
        return "DYLD_LIBRARY_PATH"
    else:
        return "LD_LIBRARY_PATH"


def relpath(path, start=os.path.curdir):
    """Return a relative version of a path"""
    if not path:
        raise ValueError("no path specified")
    start_list = posixpath.abspath(start).split(os.path.sep)
    path_list = posixpath.abspath(path).split(os.path.sep)
    # Work out how much of the filepath is shared by start and path.
    i = len(posixpath.commonprefix([start_list, path_list]))
    rel_list = [os.path.pardir] * (len(start_list) - i) + path_list[i:]
    if not rel_list:
        return os.path.curdir
    return os.path.join(*rel_list)


def ping(nameOfHost):
    p = subprocess.Popen(["ping", "-c1", "-w1", nameOfHost],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
    return p.returncode


def istext(filename):
    """ returns True if filename is a text file
    """
    s = open(filename, 'rb').read(512)
    text_characters = "".join(list(map(chr, list(range(32, 127)))) + list("\n\r\t\b")).encode()
    _null_trans = bytes.maketrans(b"", b"")
    if not s:
        # Empty files are considered text
        return True
    if "\0" in s:
        # Files with null bytes are likely binary
        return False
    # Get the non-text characters (maps a character to itself then
    # use the 'remove' option to get rid of the text characters.)
    t = s.translate(_null_trans, text_characters)
    # If more than 30% non-text characters, then
    # this is considered a binary file
    if old_div(float(len(t)), float(len(s))) > 0.30:
        return False
    return True


class LoggerException(Exception):
  def __init__(self, msg):
    self.msg = msg
  def __str__(self):
    return repr(self.msg)


class Log(object):
  def __init__(self, name="", verbose=VerboseLevels.INFO):
    self.name = name
    # verbose  < 0 - erreur
    # verbose == 0 - info, erreur
    # verbose == 1 - info, warning, erreur
    # verbose  > 1 - debug, info, warning, erreur
    self.verbose = verbose

  def set_light_background(self):
    Bcolors.set_light_background()

  def error(self, msg=''):
    begin_text = "- ERROR "
    if self.name != "":
      begin_text += self.name + " "
    text = begin_text + msg
    print()
    print(Bcolors.FAIL + text + Bcolors.ENDC)
    print()
    raise LoggerException(text)

  def debug(self, msg=''):
    global middle_of_a_line
    if self.verbose >= VerboseLevels.DEBUG:
      begin_text = "- DEBUG "
      if self.name != "":
        begin_text += self.name + " "
      text = begin_text + msg
      if middle_of_a_line:
        print()
        middle_of_a_line = False
      print(Bcolors.DEBUG + text + Bcolors.ENDC)
      sys.stdout.flush()

  def warning(self, msg=''):
    global middle_of_a_line
    if self.verbose >= VerboseLevels.WARNING:
      begin_text = "- WARNING "
      if self.name != "":
        begin_text += self.name + " "
      text = begin_text + msg
      if middle_of_a_line:
        print()
        middle_of_a_line = False
      print(text)
      sys.stdout.flush()

  def info(self, msg=''):
    global middle_of_a_line
    if self.verbose >= VerboseLevels.INFO:
      if msg != "":
        begin_text = "- INFO "
        if self.name != "":
          begin_text += self.name + " "
        text = begin_text + msg
        if middle_of_a_line:
          print()
          middle_of_a_line = False
        print(text)
        sys.stdout.flush()
      else:
        print()
        sys.stdout.flush()

  def exception(self, msg):
    global middle_of_a_line
    if middle_of_a_line:
      print()
      middle_of_a_line = False
    print()
    traceback.print_exc()
    self.error(msg)

  def check_return(self, rtn, ignore=False):
    if rtn != "":
      print()
      print()
      print(Bcolors.FAIL + rtn + Bcolors.ENDC)
      print()
    if not ignore and rtn != "":
      print(Bcolors.FAIL + "Error detected - stop project" + Bcolors.ENDC)
      print()
      sys.exit(1)

#######
#
# Méthodes permettant de gérer les sorties des traitements
#
#######

middle_of_a_line = False


def simple_print(text="", space=0, debug=0):
  if text != "":
    begin_text = " " * space + "- "
    text = begin_text + text
    if debug == 1:
      if misc_debug != 0:
        print(text)
    else:
      print(text)
  else:
    print()
  sys.stdout.flush()


def print_begin_work(text, space=0, debug=0):
  global middle_of_a_line
  begin_text = " " * space + "- "
  text = begin_text + text
  text = text + "..." + (" " * (30 - (len(text) + 3)))
  if debug == 1:
    if misc_debug != 0:
      print(text, end=' ')
      middle_of_a_line = True
  else:
    print(text, end=' ')
    middle_of_a_line = True
  sys.stdout.flush()


def print_end_work(text=""):
  global middle_of_a_line
  if text == "":
    print(Bcolors.OKGREEN + "[OK]" + Bcolors.ENDC)
  else:
    print(Bcolors.OKGREEN + "[OK] " + text + Bcolors.ENDC)
  middle_of_a_line = False
  sys.stdout.flush()


def print_end_work_with_warning(text):
  global middle_of_a_line
  print(Bcolors.WARNING + "[WARNING] " + text + Bcolors.ENDC)
  middle_of_a_line = False
  sys.stdout.flush()


def check_return(rtn, cont=False):
  if rtn != "":
    print()
    print()
    print(Bcolors.FAIL + rtn + Bcolors.ENDC)
    print()
    if not cont:
      print(Bcolors.FAIL + "Error detected - stop project" + Bcolors.ENDC)
      print()
      sys.exit(1)


#######
#
# Méthodes auxiliaires
#
#######

def transform(version, dot=True, slash=True, dash=True):
  # version can begin with tags/ or git_tag/
  if version.startswith("tags/"):
    version = version[5:]
  if version.startswith("git_tag/"):
    version = version[8:]
  if version.startswith("hg_tag/"):
    version = version[7:]
  if dot:
    version = version.replace(".", "")
  if slash:
    version = version.replace("/", "_")
  if dash:
    version = version.replace("-", "")
  return version


def simplify(version, dot=False, slash=False, dash=False, underscore=True):
  if version.startswith("tags/"):
    version = version[5:]
  if version.startswith("git_tag/"):
    version = version[8:]
  if version.startswith("V"):
    version = version[1:]
  if version.startswith("v"):
    version = version[1:]
  if underscore:
    version = version.replace("_", ".")
  return transform(version, dot, slash, dash)


def compareVersionsElements(elem, ref):
  if elem > ref:
    return 1
  if elem == ref:
    return 0
  if elem < ref:
    return -1


def compareVersions(version, ref):
  '''
  Compare 2 versions. Returns 0 is both versions are identical,
  -1 if version is< ref, +1 if version is > ref.
  @param version: string, the version to test
  @param ref: string, the reference version
  @return: an integer indicating the position of version according to ref
  '''
  major_version = major(version)
  major_ref = major(ref)
  minor_version = minor(version)
  minor_ref = minor(ref)
  maintenance_version = maintenance(version)
  maintenance_ref = maintenance(ref)

  compMajor = compareVersionsElements(major_version, major_ref)
  if compMajor != 0:
    return compMajor

  compMinor = compareVersionsElements(minor_version, minor_ref)
  if compMinor != 0:
    return compMinor

  compMaintenance = compareVersionsElements(maintenance_version, maintenance_ref)
  if compMaintenance != 0:
    return compMaintenance

  if version == ref:
    return 0

  if version < ref:
    return -1

  if version > ref:
    return 1


# Methods for dictionaries comparison
def added_in_dict(d1, d2):
  """
  Returns the keys in d2 not present in d1
  """
  return set(d2).difference(set(d1))


def removed_in_dict(d1, d2):
  """
  Returns the keys in d1 not present in d2"
  """
  return set(d1).difference(set(d2))


def changed_in_dict(d1, d2):
  """
  Returns the keys present in both d1 and d2 for which the corresponding value
  is different between d1 and d2
  """
  return set(key for key in set(d1).intersection(set(d2)) if d1[key] != d2[key])


def major(version):
  v_list = split_version(version)
  return v_list[0] if v_list else -1


def minor(version):
  v_list = split_version(version)
  return v_list[1] if len(v_list) > 1 else -1


def maintenance(version):
  v_list = split_version(version)
  return v_list[2] if len(v_list) > 2 else -1


def split_version(version_string):
  """
  Split a version string in a list of integers representing the version numbers.
  This is useful for comparison between versions.
  """
  version_string = simplify(version_string, underscore=False)
  version_tokens = re.split("\.|_|sp", version_string)
  version_list = []
  for numstr in version_tokens:
    try:
      num = int(numstr)
    except ValueError:
      # We consider that values that are not integers ("main", "BR", "trunk", etc.)
      # correspond to development versions, that are more recent than any other
      # version. Other specific cases (development versions based on older versions
      # for instance) should be handled explicitly with the version parameter
      # "ordered_version".
      num = sys.maxsize
    version_list.append(num)
  return version_list


def print_error(text):
  print()
  print()
  print(Bcolors.FAIL + text + Bcolors.ENDC)
  print()
  print()
  print(Bcolors.FAIL + "Project stopped" + Bcolors.ENDC)
  print()
  print()
  sys.exit(1)


def print_exception(text):
  print()
  print()
  print(Bcolors.FAIL + text + Bcolors.ENDC)
  traceback.print_exc()
  print()
  print()
  print(Bcolors.FAIL + "Project stopped" + Bcolors.ENDC)
  print()
  print()
  sys.exit(1)


def print_warning(text, space=0):
  begin_text = " " * space + "- WARNING "
  text = begin_text + text
  print(Bcolors.WARNING + text + Bcolors.ENDC)
  sys.stdout.flush()


def print_debug(text, space=0):
  if misc_debug == 1:
    begin_text = " " * space + "- DEBUG "
    text = begin_text + text
    print(Bcolors.OKBLUE + text + Bcolors.ENDC)
    sys.stdout.flush()


def compute_sha1(compute_type, filepath, sha1_tool):
  msg = ""
  if compute_type == "archive_file":
    return compute_sha1_archive("", filepath, sha1_tool)
  elif compute_type == "textfile":
    return compute_sha1_textfile(filepath, sha1_tool)
  elif compute_type == "filelist":
    sha1_list = []
    for patch in filepath:
      sha1_list.append(compute_sha1_textfile(patch, sha1_tool))
    return msg, sha1_list
  else:
    msg = 'sha1 compute type unknown: {0}'.format(compute_type)
    return (msg, None)


def compute_sha1_textfile(filepath, sha1_tool):
  with open(filepath, 'rb') as patch_file:
      return hashlib.sha1(patch_file.read()).hexdigest()


def compute_sha1_archive(archive_directory, archive_file_name, sha1_tool):
  msg = ""
  sha1_value = None
  archive_file_path = os.path.join(archive_directory, archive_file_name)
  if not os.path.exists(archive_file_path):
    return (msg, sha1_value)

  # Compute sha1 of local archive
  if sha1_tool == 'hashlib':
    sha1 = hashlib.sha1()
    print('Check sha1 from', archive_file_path)
    with open(archive_file_path, 'rb') as archive_file:
      for buf in iter(partial(archive_file.read, 65536), b''):
        sha1.update(buf)
    sha1_value = sha1.hexdigest()

  elif sha1_tool == "sha1sum":
    # Check sha1sum utility
    ier = subprocess.call(['sha1sum', '--version'], stdout=subprocess.PIPE)
    if ier != 0:
      msg = 'sha1sum program not found'
    else:
      p = subprocess.Popen(['sha1sum', archive_file_path], stdout=subprocess.PIPE)
      (stdoutdata, stderrdata) = p.communicate()
      try:
        sha1_value = stdoutdata.decode('utf-8').strip().split()[0]
      except IndexError:
        msg = "sha1sum execution failed"
  else:
    msg = 'sha1 tool unknown: {0}'.format(sha1_tool)

  return (msg, sha1_value)


def get_sha1_from_sha1file(archive_directory, sum_file_name):
  msg = ''
  sha1 = None
  sum_file_path = os.path.join(archive_directory, sum_file_name)
  if not os.path.exists(sum_file_path):
    return (msg, sha1)
  with open(sum_file_path, 'r') as f:
    sum_file_content = f.read().strip()
  tokens = sum_file_content.split()
  if len(tokens) != 2:
    msg = "Invalid sha1 file content: {0}".format(sum_file_content)
  else:
    sha1, file_name = tokens
    if file_name != sum_file_name[:-5]:
      msg = 'Invalid sha1 file: archive file name is {0} instead of {1}'\
              .format(file_name, sum_file_name)
  return (msg, sha1)


def check_sha1_sum(archive_directory, archive_file_name, sum_file_name,
                   sha1_tool, sha1_value):
  msg = ""

  archive_file_path = os.path.join(archive_directory,
                                   archive_file_name)

  if sha1_tool == "hashlib":
    try:
      sum_file_path = os.path.join(archive_directory, sum_file_name)
      msg, ref_sha1 = get_sha1_from_sha1file(archive_directory, sum_file_name)
      if not msg and sha1_value != ref_sha1:
        msg = 'sha1 file check failed. Delete archive file in the archive '\
          'directory and try again. Command: rm {0}'.format(archive_file_path)
    except Exception as exc:
      msg = str(exc)
  elif sha1_tool == "sha1sum":
    command = ["sha1sum", "-c", sum_file_name]
    with open(os.path.join(archive_directory, "sha1sum.log"), "wb") as log_file:
      ier = subprocess.call(command, stdout=log_file, stderr=log_file, cwd=archive_directory)
    if ier != 0:
      msg = 'Error, sha1 file check failed !!! Delete archive file {0}' \
            ' in the  archive directory and try again, command was: {1}'.format(archive_file_path, " ".join(command))

  # Final step: delete sha1 file
  if os.path.exists(sum_file_path):
    os.remove(sum_file_path)

  return msg


class TaskException(Exception):
  def __init__(self, msg, include_traceback_info=False):
    self.msg = msg
    self.traceback_info = None
    if include_traceback_info:
      self.traceback_info = traceback.format_exc()

  def __str__(self):
    complete_msg = self.msg
    if self.traceback_info is not None:
      complete_msg += "\n\nOriginal exception is:\n\n" + self.traceback_info
    return complete_msg

  def format_rst(self):
    complete_msg = self.msg
    if self.traceback_info is not None:
      complete_msg += ". Original exception is:\n\n::\n\n    " + self.traceback_info.replace("\n", "\n    ")
    return complete_msg


def create_sha1(target_file_path, sha1_file_path):
  try:
    sha1 = hashlib.sha1()
    with open(target_file_path, "rb") as target_file:
      bufsize = 65536
      data = target_file.read(bufsize)
      while len(data) > 0:
        sha1.update(data)
        data = target_file.read(bufsize)
    with open(sha1_file_path, "wb") as sha1_file:
      sha1_file.write(sha1.hexdigest())
      sha1_file.write("  ")
      sha1_file.write(os.path.basename(target_file_path))
  except:
    raise TaskException("Error: Cannot create SHA1 file {0} for file {1}" \
                        .format(sha1_file_path, target_file_path), True)


def touch(fname, times=None):
  try:
    with open(fname, 'ab'):
      os.utime(fname, times)
  except:
    raise TaskException("Error: Cannot touch file {0}".format(fname), True)


def copy(src, dst):
  try:
    shutil.copy(src, dst)
  except:
    raise TaskException("Cannot copy file {0} to {1}".format(src, dst), True)


def is64():
    return os.uname()[-1].endswith("64")


def sedSep():
    return "#"


def get_ftp_server():
    return 'http://ftp.pleiade.edf.fr/projets'


def get_pypi_server():
    return 'http://nexus3.pleiade.edf.fr/repository/pypi.python.org'


def get_git_version():
    command = ['git', '--version']
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdoutdata, _) = p.communicate()
    git_version = stdoutdata.decode('utf-8').strip().split()[-1] or None
    return git_version


def get_gcc_version():
    command = ['gcc', '-dumpversion']
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdoutdata, _) = p.communicate()
    gcc_version = stdoutdata.decode('utf-8').strip() or None
    return gcc_version


def get_calibre_package_name(basename, ordered_version):
    version_l = [str(s) for s in split_version(ordered_version)]
    return '%s-%s' % (basename, '.'.join(version_l))


def get_calibre_version():
    calibre_version = None
    # Detect Calibre version with /etc/issue
    p = subprocess.Popen(['cat', '/etc/issue'], stdout=subprocess.PIPE)
    stdoutdata = p.communicate()[0].decode().strip()
    result = [w for w in stdoutdata.split()]
    if result[0] == "Calibre":
        calibre_version = str(int(result[1][0]))
    elif result[0] == "Scibian":
        # Scibian 9 => Calibre 10
        calibre_version = str(int(result[1][0]) + 1)
    else:
        # Calibre non détecté, nous sommes peut etre sur un cluster
        # On regarde le hostname et on vérifie avec le tuple CLUSTERS
        pHost = subprocess.Popen(['cat', '/etc/hostname'], stdout=subprocess.PIPE)
        host = pHost.communicate()[0].decode().strip()
        for cluster, version in CLUSTERS.items():
            if host.startswith(cluster):
                calibre_version = version
                break

    return calibre_version


def can_use_calibre7_compat_env():
    return can_use_compat_env("7")


def can_use_calibre8_compat_env():
    return can_use_compat_env("8")


def can_use_calibre9_compat_env():
    return can_use_compat_env("9")


def can_use_compat_env(palier):
    compat_paliers = ["7", "8", '9']
    if palier not in compat_paliers:
        print_error("Mauvais environnement de compatibilité: %s. Choisir parmi %s" % (palier, compat_paliers))

    version_calibre = get_calibre_version()
    # pas sur calibre ou le palier en cours est le même que celui demandé
    if not version_calibre or version_calibre == palier:
        return False
    # Check that compat-calibreX is installed
    cmd = ["which", "compat-calibre" + palier]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    compat_calibre_bin = p.communicate()[0].decode().strip()
    # Return True if compat-calibreX is installed
    if not compat_calibre_bin:
        print_error("L'environnement de compatibilité Calibre %s n'est pas installé" % palier)

    return True


def get_current_yamm_sha1():
    yamm_path = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
    yamm_path = os.path.realpath(os.path.join(*yamm_path))
    if not os.path.exists(os.path.join(yamm_path, '.git')):
        return ''
    cmd = ["git", "rev-parse", "HEAD"]
    p = subprocess.Popen(cmd, cwd=yamm_path, stdout=subprocess.PIPE)
    p.wait()
    yamm_sha1 = p.stdout.read().decode('utf-8').strip('\n')
    cmd = ['git', 'status', '--untracked-files=no', '--porcelain']
    p = subprocess.Popen(cmd,
                         cwd=yamm_path,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    p.wait()
    local_diffs = p.stdout.read().decode('utf-8').strip()
    if local_diffs:
        yamm_sha1 += '*'
    return yamm_sha1


def get_current_yamm_sha1_short():
    yamm_sha1 = get_current_yamm_sha1()
    if yamm_sha1:
        yamm_sha1 = yamm_sha1.split("*")[0][:8]
    return yamm_sha1


def fast_delete(path):
    """ Fast deletes a directory using the rsync command with --delete option
    If path is a file, just delete it.
    """
    if os.path.isfile(path):
        os.remove(path)
        return

    # tmpdir must ends with a '/', hence the os.path.join
    emptydir = os.path.join(tempfile.mkdtemp(), '')
    command = 'rsync -d --delete %s %s' % (emptydir, path)
    try:
        ier = subprocess.call(shlex.split(command))
        if ier != 0:
            print_exception('Fast delete command failed: %s' % command)
        # Now path is an empty directory
        os.rmdir(path)
    except:
        shutil.rmtree(path)
    finally:
        # Remove temporary empty directory
        os.rmdir(emptydir)
    return


def config_urllib2_netrc(address, command_log=None):
    hostname = urllib.parse.urlsplit(address).hostname
    if not hostname:
        return
    auth_netrc = None
    try:
        auth_netrc = netrc().authenticators(hostname)  # read from ~/.netrc
    except NetrcParseError as err:
        if command_log:
            command_log.print_debug("Error while searching authenticators in netrc file: {0}".format(err))
    except IOError as ioerror:
        if command_log:
            command_log.print_debug("Skipping authenticators research: {0}".format(ioerror))

    if auth_netrc is not None:
        if command_log:
            command_log.print_debug("Authentification for server %s found" % hostname)
        username, _, password = auth_netrc
        password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
        # créer un password manager
        password_mgr.add_password(None, hostname, username, password)
        # ajoute le username et password
        # si on connaissait le domaine, on pourrait l'utiliser au lieu de ``None``
        handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
        # créer le handler
        opener = urllib.request.build_opener(handler)
        # du handler vers  l'opener
        try:
            opener.open(address)
        except urllib.error.HTTPError as e:
            raise TaskException('Cannot download %s: %s' % (address, e.msg))
        # utilise l'opener pour ramener un URL
        urllib.request.install_opener(opener)
        # installe l'opener
        # maintenant tous les appels à urllib2.urlopen vont utiliser notre opener
    return hostname


def insert_string(source_str, insert_str, pos):
    return source_str[:pos] + insert_str + source_str[pos:]


def compute_python_version(py3=False, command=''):
    """ Returns the python version as 2.7.9, 3.4.2
    """
    command = command.strip()
    if command and not command.endswith(';'):
        command += '; '
    command += 'python'
    if py3:
        command += '3'
    command += " -c \"import sys;print(sys.version[:5])\""

    try:
        p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        p.wait()
    except OSError as e:
        raise TaskException("in getting python version, command was: %s, exception was: %s" % (command, e))

    # Strip newline
    return p.stdout.read()[:-1].decode('utf8')


def is_on_debian():
    return platform.linux_distribution()[0] == 'debian'


def is_on_ubuntu():
    return platform.linux_distribution()[0] == 'Ubuntu'


def get_linux_version():
    return platform.linux_distribution()[1]


def debian_compliant(command):
    """
    This decorator must be used to ensure that the system
    is Debian compliant
    """
    @wraps(command)
    def wrapper(*args, **kw):
        ret = None
        if (is_on_debian() or is_on_ubuntu()):
            ret = command(*args, **kw)
        return ret
    return wrapper

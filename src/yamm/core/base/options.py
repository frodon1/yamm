#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from past.builtins import basestring
from builtins import object
from builtins import str
from yamm.core.base import misc
import os
import sys
if sys.version_info < (3,):
    integer_types = (int, long,)  # @UndefinedVariable
    string_type = (basestring, str, type(''))  # @UndefinedVariable
else:
    integer_types = (int,)
    string_type = (str, type(''))



class Options(object):
    """
      .. py:currentmodule:: yamm.core.base.Options

      This class aims to manage the options of a project.
      It works like a catalog where it's possible to add options,
      get and set theirs values.

    """

    def __init__(self):
        self._global_opt = {}
        self._category_opt = {}
        self._software_opt = {}
        self._software_category = {}
        self._categories = []
        self._options = []
        self._options_types  = {}
        self._options_ranges = {}
        self._options_values = {}
        self._compatibility_func = {}

    def add_option(self, option, option_type, option_ranges, option_values=None, compatibility_func=None):
        """
          Add a new option in the catalog.

          :param string option: Option name.
          :param object option_type: Object instance representing option requested type.
          :param list option_ranges: Ranges of the option: ["global", "category", "software"]
          :param list option_values: List of possible values for the option
          :param function compatibility_func: use this function to test if option can be used

        """
        if option in self._options:
            misc.print_warning("Option %s is already defined in project" % option)
        else:
            self._options.append(option)
            if isinstance(option_type, string_type):
              self._options_types[option] = string_type
            else:
              self._options_types[option] = type(option_type)
            self._options_ranges[option] = option_ranges
            self._options_values[option] = None
            if self._options_types[option] in string_type + integer_types:
              self._options_values[option] = option_values
            self._compatibility_func[option] = compatibility_func

    def get_categories(self):
      return self._categories

    def get_option_list_without_range(self, range_to_select):
      option_list = []
      for opt in self._options:
        if self._options_ranges[opt] != range_to_select:
          option_list.append(opt)
      option_list.sort()
      return option_list

    def get_option_list_with_range(self, range_to_select):
      option_list = []
      for opt in self._options:
        if self._options_ranges[opt] == range_to_select:
          option_list.append(opt)
      option_list.sort()
      return option_list

    def get_option_ranges(self, option):
      if option not in self._options:
        misc.print_warning("Option %s is not defined in project (method test_option_type)" % option)
      return self._options_ranges.get(option, ["globals"])

    def get_option_possible_values(self, option):
      if option not in self._options:
        misc.print_warning("Option %s is not defined in project (method test_option_type)" % option)
      return self._options_values.get(option, None)

    def get_option_type(self, option):
      """
        Return option type.

        :param string option: Option name.
        :return: Return option type.
        :rtype: type

      """
      if option not in self._options:
          misc.print_warning("Option %s is not defined in project" % option)
      return self._options_types.get(option, None)

    def add_category(self, category):
        """
          Add a new category in the catalog.

          :param string category: Category name.

        """
        if category in self._categories:
            misc.print_warning("Category %s is already defined in project" % category)
        else:
            self._categories.append(category)

    def add_software(self, software, category=None):
        """
          Add a new software in the catalog. It's possible
          to attach the software to an already added category.

          :param string software: Software name.
          :param string category: Category name to attach to.

        """
        if software in self._software_category:
            misc.print_warning("Software %s is already defined in project" % software)
        elif category is not None and category not in self._categories:
            misc.print_warning("Category %s is not defined in project" % category)
        self._software_category[software] = category

    def is_category_option(self, category, option):
        """
          Returns True if the option has a specific value for the category.

          :param string category: Category name.
          :param string option: Option name.
          :return: Return True if value found.
          :rtype: boolean

        """
        return category in self._category_opt and option in self._category_opt[category]

    def is_software_option(self, software, option):
        """
          Returns True if the option has a specific value for the software.

          :param string software: Software name.
          :param string option: Option name.
          :return: Return True if value found.
          :rtype: boolean

        """
        return software in self._software_opt and option in self._software_opt[software]

    def has_option(self, option):
        """
          Returns True if the option has been added to the catalog.

          :param string option: Option name.
          :return: Return True if option found.
          :rtype: boolean

        """
        return option in self._options

    def has_global_option(self, option):
        return self.get_global_option(option) is not None

    def has_category_option(self, option, category):
        return option in self._options and category in self._categories \
          and category in self._category_opt and option in self._category_opt[category]

    def set_global_option(self, option, value):
        """
          Set a value for option for global level.

          :param string option: Option name.
          :param value: Value for option.

        """
        if option not in self._options:
            misc.print_warning("Global option {0} is not defined in project, "
                               "option cannot be set".format(option))
            return

        if not self.check_compatibility(option, value):
            misc.print_warning("Global option '{0}' cannot be set "
                               "(not compatible)".format(option))
            return

        self.check_type(option, value)

        possible_values = self._options_values[option]
        if possible_values is None or value in possible_values:
            self._global_opt[option] = value
        else:
            misc.print_error("Global option '{0}' (value {1} of type {2}) is "
                             "not in the possible values: {3}"
                             .format(option, value, type(value),
                                     possible_values))

    def set_category_option(self, category, option, value):
      """
        Set a value for option for category level.

        :param string category: Category name.
        :param string option: Option name.
        :param value: Value for option.

      """
      if category not in self._categories:
          misc.print_warning("Category {0} is not defined in project".format(category))
          return
      if option not in self._options:
          misc.print_warning("Category option {0} is not defined in project, option cannot be set".format(option))
          return
      
      if not self.check_compatibility(option, value):
          misc.print_warning("Category option '{0}' cannot be set for category {1} (not compatible)".format(option, category))
          return

      self.check_type(option, value)

      possible_values = self._options_values[option]
      if possible_values is None or value in possible_values:
        if category not in self._category_opt:
            self._category_opt[category] = {}
        self._category_opt[category][option] = value
      else:
          misc.print_error("Category option '{0}' value '{1}' is not in the possible values: {2}".format(option, value, possible_values))


    def set_software_option(self, software, option, value):
      """
        Set a value for option for software level.

        :param string software: Software name.
        :param string option: Option name.
        :param value: Value for option.

      """
      if option not in self._options:
          misc.print_warning("Software option {0} is not defined in project, option cannot be set".format(option))
          return
      
      if not self.check_compatibility(option, value):
          misc.print_warning("Software option '{0}' cannot be set for {1} (not compatible)".format(option, software))
          return

      self.check_type(option, value)

      possible_values = self._options_values[option]
      if possible_values is None or value in possible_values:
        if software not in self._software_opt:
            self._software_opt[software] = {}
        self._software_opt[software][option] = value
      else:
          misc.print_error("Software option '{0}' value '{1}' is not in the possible values: {2}".format(option, value, possible_values))

    def get_option(self, software, option):
        """
           Get option value for a software. It firsly check if has value in the
           software level, then in the catalog level and finally in the global level.

           :param string software: Software name.
           :param string option: Option name.

           :return: Return the value of the option for the software.

        """
        if option not in self._options:
            misc.print_warning("Option %s is not defined in project" % option)
        try:
            return self._software_opt[software][option]
        except KeyError:
            try:
                return self._category_opt[self._software_category[software]][option]
            except (KeyError, TypeError):
                return self._global_opt.get(option, None)

    def get_global_option(self, option):
        """
          Get option value for global level

          :param string option: Option name.

          :return: Return option global value.

        """
        return self._global_opt.get(option, None)

    def remove_global_option_value(self, option):
        """
          Remove option value for global level
          :param string option: Option name.
        """
        if option in self._global_opt:
            del self._global_opt[option]

    def get_category_option(self, category, option):
        """
           Get option value for a category. It firsly check if has value in the
           catalog level then in the global level.

           :param string category: Category name.
           :param string option: Option name.

           :return: Return the value of the option for the category.

        """
        if option not in self._options:
            misc.print_warning("Option %s is not defined in project" % option)
        if category not in self._categories:
            misc.print_warning("Category %s is not defined in project" % category)
        try:
            return self._category_opt[category][option]
        except KeyError:
            return self._global_opt.get(option, None)

    # Print methods

    def print_options(self, software):
        """
          Print all options values for a software.

          :param string software: Software name.

        """
        print()
        print("- Options for software %s" % software)
        for option in self._options:
            print("  - Option %s: %s"
                  % (option, self.get_option(software, option)))

    def print_option_value(self, logger, value, label,
                           width=0, fillchar=' '):
        logger.info('{0} : {1}'.format(label.ljust(width, fillchar), value))

    def print_global_option(self, logger, option, label='',
                            width=0, fillchar=' '):
        value = self.get_global_option(option)
        if not label:
            label = option
        self.print_option_value(logger, value, label, width, fillchar)

    def print_category_option(self, logger, option, category, label='',
                              width=0, fillchar=' '):
        value = self.get_category_option(category, option)
        if not label:
            label = option
        self.print_option_value(logger, value, label, width, fillchar)

    def print_software_option(self, logger, option, software, label='',
                              width=0, fillchar=' '):
        value = self.get_option(software, option)
        if not label:
            label = option
        self.print_option_value(logger, value, label, width, fillchar)

    # Check methods

    def check_compatibility(self, option, value):
        option_is_compatible = True
        compatibility_func = self._compatibility_func[option]
        if compatibility_func:
            option_is_compatible = compatibility_func(value)
        return option_is_compatible

    def check_type(self, option, value):
        if not isinstance(value, self._options_types[option]):
            misc.print_error("Option {0} is not correctly defined, its type "
                             "should be {1}, not {2}"
                             .format(option, self._options_types[option],
                                     type(value)))

    def check_abs_path(self, name, category=None, software=None, logger=None):
        if software:
            value = self.get_option(software, name)
        elif category:
            value = self.get_category_option(category, name)
        else:
            value = self.get_global_option(name)
        if not os.path.isabs(value):
            msg = "{0} has to be an absolute path, "\
                  "current value is: {1}".format(name, value)
            if logger:
                logger.error(msg)
            else:
                print(msg)

    def check_allowed_value(self, name, allowed_values,
                            category=None, software=None,
                            logger=None):
        if software:
            value = self.get_option(software, name)
        elif category:
            value = self.get_category_option(category, name)
        else:
            value = self.get_global_option(name)
        if value not in allowed_values:
            msg = "{0} is not correctly defined, value is {1}, "\
                  "allowed values are {2}".format(name, value, allowed_values)
            if logger:
                logger.error(msg)
            else:
                print(msg)

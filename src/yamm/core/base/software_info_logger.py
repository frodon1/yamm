#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)



import os
import shutil

from yamm.core.base.logger import BaseLog
from yamm.core.base.misc   import Log
from yamm.core.base.misc   import relpath
from yamm.core.base.misc   import transform


class SoftwareInfoLog(BaseLog):

  def __init__(self, software, command_verbose_level, log_directory, print_console=False, write_in_file=False, command_files_basename="", logo_directory=""):
    self.software = software
    logo_directory = os.path.join(log_directory,"_static")
    log_directory = os.path.join(log_directory, self.software.name, transform(self.software.version, dot=False, dash=False))
    BaseLog.__init__(self, software.name, command_verbose_level, log_directory, False, write_in_file, command_files_basename, logo_directory)
    self.logger = Log("[SOFTWARE INFO LOG]")

  def get_report_filename(self):
    return os.path.join(self.log_directory, self.log_files_basename+"_infos_"+self.name)

  def write_infos(self):
    BaseLog.start_header(self)

    if self.write_in_file:
      try:
        with open(self.report_filename, 'a') as log_file:
          intro_str = "Informations for software %s" % self.name
          intro_sub = "=" * len(intro_str)
          self.write_in_reportfile(log_file, "\n", intro_str, intro_sub, "")
          # For raw input of info values
          self.write_in_reportfile(log_file, "", ".. role:: info_raw(raw)")
          self.write_in_reportfile(log_file, "", "   :format: html")
          self.write_in_reportfile(log_file, "\n", "")
      except IOError as e:
        self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

      infos = self.software.get_infos(light=True)
      tab = []
      logtab = []
      for key in sorted(infos.keys()):
        if key.endswith(".log"):
          logfile = os.path.basename(infos[key])
          shutil.copy(infos[key],self.log_directory)
          newfile = os.path.join(self.log_directory,logfile)
          logtab.append([key,"`%s <%s>`_"%(logfile, relpath(newfile, self.log_directory))])
        else:
          val = infos[key].split("\n")
          newval = []
          for v in val:
            lstrip_v = v.lstrip()
            nbspaces = len(v) - len(lstrip_v)
            newval.append("&nbsp;"*nbspaces+lstrip_v.strip())
          val = "<br/>".join(newval)
          val = val.strip()
          if val != "":
            val = ":info_raw:`%s`"%val
          tab.append([key,val])

      tab += logtab

      self.write_rst_tab_from_dict(tab, "Info name", "Info value")

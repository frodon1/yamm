#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import str


import os
import shutil

from yamm.core.base.logger import BaseLog
from yamm.core.base.misc   import Log
from yamm.core.base.misc   import relpath


class TestLog(BaseLog):

  def __init__(self, test_name, soft_name, soft_version, command_verbose_level, log_directory, print_console=False, write_in_file=False, command_files_basename="", logo_directory=""):
    self.soft_name = soft_name
    self.soft_version = soft_version
    logo_directory = os.path.join(log_directory,"_static")
    log_directory = os.path.join(log_directory, self.soft_name, self.soft_version)
    BaseLog.__init__(self, test_name, command_verbose_level, log_directory, print_console, write_in_file, command_files_basename, logo_directory)
    self.logger = Log("[TEST LOG]")

  def get_report_filename(self):
    return os.path.join(self.log_directory, self.log_files_basename+"_tests_"+self.name.replace(" ","_"))
#######
#
# Framework part
#
#######

  def start_test(self, test_suite_list):
    #BaseLog.start_command(self)
    self.test_suite_list = test_suite_list

    if self.print_console:

      # Print command name
      self.print_info()
      self.print_info("Starting test: %s" % self.name)
      self.print_info()

    if self.write_in_file:
      try:
        with open(self.report_filename, 'w') as log_file:
          intro_str = "Log for test %s" % self.name
          intro_sub = "=" * len(intro_str)
          self.write_in_reportfile(log_file, "\n", intro_str, intro_sub, "")
      except IOError as e:
        self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

#######
#
# Executor part
#
#######

  def test_suite_end(self, exe_tab, errors, test_log_files = None):

    if self.print_console:
      self.print_info("")
      self.print_info("Execution Summary:")
      self.print_info("-----------------:")
      self.print_info("Number of tests: %s" % str(self.test_suite_list.nbTests()))
      self.print_info("Successfull: %s" % str(self.test_suite_list.nbSucceededTests()))
      self.print_info("Error: %s" % str(self.test_suite_list.nbFailedTests()))
      #self.print_info("Not Executed: %s" % str(len(executor.sorted_executor_softwares) - (number_ok_software+number_skipped_software+number_error_software)))
      if type(self.test_suite_list.failed_tests) == {}:
        self.print_info("Error list: %s" % str(list(self.test_suite_list.failed_tests.values())))
      if type(self.test_suite_list.failed_tests) == []:
        self.print_info("Error list: %s" % str(self.test_suite_list.failed_tests))
      #self.print_info("Total execution is done in %s :-) !" % str(datetime.timedelta(seconds=t_ini)))

    if self.write_in_file:

      # Write Execution tab
      self.write_rst_tab_from_dict(exe_tab, "Test", "State", "Time", "Comment")

      if test_log_files is not None:
        try:
          with open(self.report_filename, 'a') as log_file:
            for test_num,test_log_file in list(test_log_files.items()):
              logfile = os.path.basename(test_log_file)
              shutil.copy(test_log_file,self.log_directory)
              newfile = os.path.join(self.log_directory,logfile)
              self.write_in_reportfile(log_file, "", ".. _Test %s log file: %s"%(test_num,relpath(newfile, self.log_directory)), "\n")
        except IOError as e:
          self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))

      if errors != "":
        try:
          with open(self.report_filename, 'a') as log_file:
            self.write_in_reportfile(log_file, "\n", "")
            self.write_in_reportfile(log_file, "\n", "Errors:")
            self.write_in_reportfile(log_file, "",   "-------", "\n")
            self.write_in_reportfile(log_file, "", errors, "\n")
        except IOError as e:
          self.logger.exception("Impossible to write into the file %s, exception was: %s"%(self.report_filename, e))
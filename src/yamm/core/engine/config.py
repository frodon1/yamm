#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import str
from builtins import object
import os

from yamm.core.base.misc import Log


class CompilEnv(object):

  def __init__(self,
               main_topdir="",
               src_topdir="",
               build_topdir="",
               install_topdir="",

               python_version="",
               parallel_make="",

               continue_on_failed=False,
               check_dependency_compilation=True,
               backup_software=False,

               logger_name="[EXECUTOR CONFIG]",
               ):

    self.main_topdir = main_topdir
    self.src_topdir = src_topdir
    self.build_topdir = build_topdir
    self.install_topdir = install_topdir
    self.continue_on_failed = continue_on_failed
    self.backup_software = backup_software
    self.check_dependency_compilation = check_dependency_compilation
    self.python_version = python_version
    self.py3 = python_version.startswith('3')
    self.parallel_make = parallel_make

    # Initialisation
    self.logger = Log(logger_name)
    if parallel_make == "":
      self.parallel_make = "1"
    if self.main_topdir != "":
      self.set_main_topdir(self.main_topdir)

  def set_main_topdir(self, main_topdir):

    self.main_topdir = main_topdir
    self.src_topdir = os.path.join(self.main_topdir, "src")
    self.build_topdir = os.path.join(self.main_topdir, "build")
    self.install_topdir = os.path.join(self.main_topdir, "install")

  def check_abs(self):

    if not os.path.isabs(self.src_topdir):
      self.logger.error("src_topdir has to be an absolute path")
    if not os.path.isabs(self.build_topdir):
      self.logger.error("build_topdir has to be an absolute path")
    if not os.path.isabs(self.install_topdir):
      self.logger.error("install_topdir has to be an absolute path")

  def copy_env(self, src_env):

    self.main_topdir = src_env.main_topdir
    self.src_topdir = src_env.src_topdir
    self.build_topdir = src_env.build_topdir
    self.install_topdir = src_env.install_topdir
    self.parallel_make = src_env.parallel_make
    self.python_version = src_env.python_version
    self.continue_on_failed = src_env.continue_on_failed
    self.backup_software = src_env.backup_software
    self.check_dependency_compilation = src_env.check_dependency_compilation

  def print_env(self, command_log, msg="Compilation environnement:", ignore_topdirs=False, space=0):

    command_log.print_info(msg, space)
    command_log.print_info("Main topdir                 : " + self.main_topdir, space + 2)
    if not ignore_topdirs:
      command_log.print_info("Src topdir                  : " + self.src_topdir, space + 2)
      command_log.print_info("Build topdir                : " + self.build_topdir, space + 2)
      command_log.print_info("Install topdir              : " + self.install_topdir, space + 2)
    command_log.print_info("Make flag                   : -j" + self.parallel_make, space + 2)
    command_log.print_info("Python version              : " + self.python_version, space + 2)
    command_log.print_info("Continue on failed          : " + str(self.continue_on_failed), space + 2)
    command_log.print_info("Backup install directory    : " + str(self.backup_software), space + 2)
    command_log.print_info("Check dependency compilation: " + str(self.check_dependency_compilation), space + 2)

class SoftwareConfig(CompilEnv):

  def __init__(self,
               main_topdir="",
               src_topdir="",
               build_topdir="",
               install_topdir="",
               parallel_make="",
               make_target="",
               python_version="",
               src_directory="",
               cmake_src_directory="",
               build_directory="",
               install_directory="",
               backup_directory="",
               clean_src_if_success=False,
               clean_build_if_success=False,
               can_delete_src=True,
               logger_name="[SOFTWARE CONFIG]",
               ):
    CompilEnv.__init__(self,
                        main_topdir=main_topdir,
                        src_topdir=src_topdir,
                        build_topdir=build_topdir,
                        install_topdir=install_topdir,
                        parallel_make=parallel_make,
                        python_version=python_version,
                        logger_name=logger_name,
                        )

    # Specific attributes for software
    self.src_directory = src_directory
    self.build_directory = build_directory
    self.install_directory = install_directory
    self.backup_directory = backup_directory
    self.make_target = make_target
    self.cmake_src_directory = cmake_src_directory
    self.clean_src_if_success = clean_src_if_success
    self.clean_build_if_success = clean_build_if_success
    self.can_delete_src = can_delete_src

    self.parallel_make_forced = 0
    if parallel_make != "":
      self.parallel_make_forced = 1

    # Check directories
    if self.src_directory and not os.path.isabs(self.src_directory):
      self.logger.error('software is not correctly initialised, src directory "%s" is not '
                              'a valid path' % self.src_directory)
    if self.build_directory and not os.path.isabs(self.build_directory):
      self.logger.error('software is not correctly initialised, build directory "%s" is not '
                              'a valid path' % self.build_directory)
    if self.install_directory and not os.path.isabs(self.install_directory):
      self.logger.error('software is not correctly initialised, install directory "%s" is not '
                              'a valid path' % self.install_directory)
    if self.backup_directory and not os.path.isabs(self.backup_directory):
      self.logger.error('software is not correctly initialised, backup directory "%s" is not '
                              'a valid path' % self.backup_directory)

    self.update_log_path()

  def merge_with_config(self, config):

    """ merge_with_project_env documentation
    It merges the env with config argument.
    Values of config are taken when the current env
    has default values.
    """
    if self.src_topdir == "":
      self.src_topdir = config.src_topdir
    if self.build_topdir == "":
      self.build_topdir = config.build_topdir
    if self.install_topdir == "":
      self.install_topdir = config.install_topdir
    if self.parallel_make == "1" and self.parallel_make_forced == 0:
      self.parallel_make = config.parallel_make
    if self.check_dependency_compilation:
      self.check_dependency_compilation = config.check_dependency_compilation
    self.update_log_path()

  def update_log_path(self):
    self.log_dirs = {}

    self.yamm_src_log_dir = os.path.join(self.src_directory, '.yamm')
    self.yamm_build_log_dir = os.path.join(self.build_directory, '.yamm')
    self.yamm_install_log_dir = os.path.join(self.install_directory, '.yamm')

    self.env_build = os.path.join(self.yamm_build_log_dir, 'env_build.sh')

    self.log_dirs['checkout'] = os.path.join(self.yamm_src_log_dir, "checkout.log")
    self.log_dirs['diff'] = os.path.join(self.yamm_src_log_dir, "diff.log")
    self.log_dirs['update'] = os.path.join(self.yamm_src_log_dir, "update.log")
    self.log_dirs['fetch'] = os.path.join(self.yamm_src_log_dir, "fetch.log")
    self.log_dirs['switch'] = os.path.join(self.yamm_src_log_dir, "switch.log")
    self.log_dirs['status'] = os.path.join(self.yamm_src_log_dir, "status.log")
    self.log_dirs['upstream'] = os.path.join(self.yamm_src_log_dir, "upstream.log")

    self.log_dirs['generation'] = os.path.join(self.yamm_build_log_dir, 'generation.log')
    self.log_dirs['pre_configure'] = os.path.join(self.yamm_build_log_dir, 'pre_configure.log')
    self.log_dirs['configure'] = os.path.join(self.yamm_build_log_dir, 'configure.log')
    self.log_dirs['post_configure'] = os.path.join(self.yamm_build_log_dir, 'post_configure.log')
    self.log_dirs['pre_build'] = os.path.join(self.yamm_build_log_dir, 'pre_build.log')
    self.log_dirs['build'] = os.path.join(self.yamm_build_log_dir, 'build.log')
    self.log_dirs['build_egg'] = os.path.join(self.yamm_build_log_dir, 'build_egg.log')
    self.log_dirs['pre_install'] = os.path.join(self.yamm_build_log_dir, 'pre_install.log')
    self.log_dirs['install'] = os.path.join(self.yamm_build_log_dir, 'install.log')
    self.log_dirs['post_install'] = os.path.join(self.yamm_build_log_dir, 'post_install.log')
    self.log_dirs['make'] = os.path.join(self.yamm_build_log_dir, 'make.log')
    self.log_dirs['rsync'] = os.path.join(self.yamm_build_log_dir, 'rsync.log')

    # Log files for commands
    for f, d in list(self.log_dirs.copy().items()):
        if f.endswith('_cmd'):
            continue
        dirname = os.path.dirname(d)
        basename = os.path.basename(d)
        value = basename.split('.')
        if value[-1] != 'log':
            continue
        value[0] += '_cmd'
        self.log_dirs[f + '_cmd'] = os.path.join(dirname, '.'.join(value))

  def print_execution_config(self, command_log, space=0):

    command_log.print_info()
    command_log.print_info("Execution configuration:", space)
    command_log.print_info("Make    flag     : -j" + self.parallel_make, space + 2)
    if self.make_target:
      command_log.print_info("Make    target   : " + self.make_target, space + 2)
    command_log.print_info("Src     directory: " + self.src_directory, space + 2)
    command_log.print_info("Build   directory: " + self.build_directory, space + 2)
    command_log.print_info("Install directory: " + self.install_directory, space + 2)
#     command_log.print_info()

class ArchiveSoftwareConfig(SoftwareConfig):

  def __init__(self,
               archive_file_name,
               archive_dir,
               patches,
               main_topdir="",
               src_topdir="",
               build_topdir="",
               install_topdir="",
               parallel_make="",
               make_target="",
               python_version="",
               src_directory="",
               cmake_src_directory="",
               build_directory="",
               install_directory="",
               backup_directory="",
               clean_src_if_success=False,
               clean_build_if_success=False,
               can_delete_src=True):
    SoftwareConfig.__init__(self,
                             main_topdir=main_topdir,
                             src_topdir=src_topdir,
                             build_topdir=build_topdir,
                             install_topdir=install_topdir,
                             parallel_make=parallel_make,
                             make_target=make_target,
                             python_version=python_version,
                             src_directory=src_directory,
                             cmake_src_directory=cmake_src_directory,
                             build_directory=build_directory,
                             install_directory=install_directory,
                             backup_directory=backup_directory,
                             clean_src_if_success=clean_src_if_success,
                             clean_build_if_success=clean_build_if_success,
                             can_delete_src=can_delete_src)

    # Specific attributes for archive_software
    self.archive_file_name = archive_file_name
    self.archive_dir = archive_dir
    self.patches = patches
    self.user_full_archive_file_name = os.path.join(archive_dir, archive_file_name)

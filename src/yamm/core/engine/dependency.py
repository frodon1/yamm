#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import object
from yamm.core.base import misc


class Dependency(object):

  dependency_types = ["pkg", "path", "ld_lib_path", "install_path", "python_path", "python_version"]

  def __init__(self,
               name,
               depend_of=None,
               install_path="",
               specific_pkg_path="",
               specific_bin_path="",
               specific_lib_path="",
               specific_install_var_name="",
               specific_install_var_end="",
               python_version="",
              ):
    self.name = name
    self._depend_of = depend_of
    if self._depend_of is None:
      self._depend_of = []

    self.install_path = install_path
    self.specific_pkg_path = "/" + specific_pkg_path
    self.specific_bin_path = "/" + specific_bin_path
    self.specific_lib_path = "/" + specific_lib_path
    self.specific_install_var_name = specific_install_var_name
    self.specific_install_var_end = specific_install_var_end
    self.python_version = python_version

    self.logger = misc.Log("[DEPENDENCY %s]" % self.name)

    self.check_args()

  @property
  def depend_of(self):
    return self._depend_of

  @depend_of.setter
  def depend_of(self, value): #@DuplicatedSignature
    if type(value) is not type([]):
      self.logger.error("- ERROR [DEPENDENCY] type of attribute 'depend_of' must be a list")
    self._depend_of = value
    self.check_args()

  def check_args(self):
    for dependency in self._depend_of:
      if dependency not in self.dependency_types:
        self.logger.error(" ".join(["- ERROR [DEPENDENCY] depend type", dependency, "of software", self.name, "is not supported"]))

  def get_dependency_types(self):
    return self.dependency_types

  def merge(self, depend_father):

    if self.install_path == "":
      self.install_path = depend_father.install_path
    if self.specific_pkg_path == "/":
      self.specific_pkg_path = depend_father.specific_pkg_path
    if self.specific_bin_path == "/":
      self.specific_bin_path = depend_father.specific_bin_path
    if self.specific_lib_path == "/":
      self.specific_lib_path = depend_father.specific_lib_path
    if self.specific_install_var_name == "":
      self.specific_install_var_name = depend_father.specific_install_var_name
    if self.specific_install_var_end == "":
      self.specific_install_var_end = depend_father.specific_install_var_end
    if self.python_version == "":
      self.python_version = depend_father.python_version

  def get_dependency_env(self, executor):

    depend_soft_install_directory = self.install_path
    if self.install_path == "":
      depend_soft_install_directory = executor.get_software_install_directory(self.name)
    if self.python_version == "":
      self.python_version = executor.get_software_python_version(self.name)

    dependency_command = ""
    for dependency in self._depend_of:
      if dependency == "pkg":
        end = "/lib/pkgconfig"
        if self.specific_pkg_path != "/":
          end = self.specific_pkg_path
        dependency_command += "export PKG_CONFIG_PATH=\"" + depend_soft_install_directory + end + ":$PKG_CONFIG_PATH\" ; "
      if dependency == "path":
        end = "/bin"
        if self.specific_bin_path != "/":
          end = self.specific_bin_path
        dependency_command += "export PATH=\"" + depend_soft_install_directory + end + ":$PATH\" ; "
      if dependency == "ld_lib_path":
        end = "/lib"
        if self.specific_lib_path != "/":
          end = self.specific_lib_path
        dependency_command += "export " + misc.get_ld_library_path() + "=\"" + depend_soft_install_directory + end + ":$" + misc.get_ld_library_path() + "\" ; "
      if dependency == "install_path":
        var_name = self.name + "_INSTALL_DIR"
        if self.specific_install_var_name != "":
          var_name = self.specific_install_var_name
        dependency_command += "export " + var_name + "=\"" + depend_soft_install_directory + self.specific_install_var_end + "\" ; "
      if dependency == "python_path":
        dependency_command += "export PYTHONPATH=\"" + depend_soft_install_directory
        dependency_command += "/lib/python" + self.python_version + "/site-packages:$PYTHONPATH\" ; "
      if dependency == "python_version":
        dependency_command += "export PYTHON_VERSION=" + self.python_version + " ; "

    return dependency_command


  def get_cmake_dependency_env(self, executor):

    depend_soft_install_directory = self.install_path
    if self.install_path == "":
      depend_soft_install_directory = executor.get_software_install_directory(self.name)
    if self.python_version == "":
      self.python_version = executor.get_software_python_version(self.name)

    dependency_command = ""
    for dependency in self._depend_of:
      if dependency == "install_path":
        var_name = self.name.upper() + "_ROOT_DIR"
        if self.specific_install_var_name != "":
          var_name = self.specific_install_var_name
        dependency_command += " -D{0}=\"{1}\" ".format(var_name, depend_soft_install_directory)

    return dependency_command

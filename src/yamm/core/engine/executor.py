#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from __future__ import print_function

from builtins import str
from builtins import object
import datetime
import os
import shutil
import subprocess
import time

from yamm.core.base.bcolors import Bcolors
from yamm.core.base.command_logger import CommandLog
from yamm.core.base.misc import fast_delete
from yamm.core.base.misc import Log
from yamm.core.base.software_info_logger import SoftwareInfoLog
from yamm.core.base.test_logger import TestLog
from yamm.core.engine import config
from yamm.core.engine.softwares import ExecutorSoftware, ArchiveSoftware, SoftwareStatus
from yamm.core.engine.tasks.generic_task import TaskException
from yamm.core.base import misc


class Executor(object):

  def __init__(self,
               version_object,
               options,
               executor_config,
               ignore_topdirs=False,
               verbose=0,
               command_log=None
              ):
    self.logger = Log(name="[EXECUTOR]")
    self.verbose_level = verbose
    self.version_object = version_object
    self.options = options
    # Contient les informations générales sur la compilation
    self.executor_config = executor_config
    if not isinstance(self.executor_config, config.CompilEnv):
      self.logger.error("Project config env's type is wrong")
      self.executor_config.check_abs()
    # Contient la liste des logiciels à exécuter
    self.executor_softwares = []
    # Contient la liste triée des logiciels à exécuter
    self.sorted_executor_softwares = []
    # Contient un dictionnaire des logiciels gérés
    self.handled_executor_softwares = {}
    # Contient des informations sur des logiciels déjà réalisés
    # qui permet ensuite de calculer le répertoire d'installation
    self.softwares_already_done = {}
    # Contient une liste des logiciels compilés
    self.compiled_list = []
    # Permet de savoir si on doit utiliser seulement main_topdir
    # Ou si on a besoin de src_topdir, build_topdir, install_todir
    self.ignore_topdirs = ignore_topdirs
    # Begin function hooks called before each software
    self.begin_hooks = []
    # End function hooks called after each software
    self.end_hooks = []
    # Command Log
    self.command_log = command_log
    if not command_log:
      self.command_log = CommandLog("Executor",
                                    command_verbose_level=self.verbose_level,
                                    log_directory=self.options.get_global_option("log_directory"),
                                    print_console=True,
                                    write_in_file=False,
                                    version_name=version_object.name)

#######
#
# Méthodes de configuration de l'exécuteur
#
#######

  def set_global_verbose_level(self, verbose):
    self.verbose_level = verbose

  def add_software(self, executor_software):
    """
    Ajout d'un logiciel à exécuter
    """
    if not isinstance(executor_software, ExecutorSoftware):
      self.logger.error(" ".join(["Software's type is wrong:", str(executor_software)]))
    executor_software.init_software_with_executor(self.executor_config)
    # Si le software est déjà dans la liste, on le supprime avant de le remettre
    # On s'assure ainsi d'avoir le mode d'exécution à jour
    if executor_software.name in self.handled_executor_softwares:
      self.executor_softwares.remove(self.handled_executor_softwares[executor_software.name])
    self.executor_softwares.append(executor_software)
    self.handled_executor_softwares[executor_software.name] = executor_software

  def add_software_already_done(self, software_name, software_value):
    """
    On ajoute un logiciel qui ne doit pas être exécuté mais qui est une dépendance
    d'un logiciel à exécuter
    software_value est une liste contenant le répertoire d'installation et optionnellement
    la version python
    """
    if not isinstance(software_value, type([])):
      self.logger.error(" ".join(["software_value type for software", software_name, "has to be a list"]))
    self.softwares_already_done[software_name] = software_value

  def add_hooks(self, begin_hook, end_hook):
    self.begin_hooks.append(begin_hook)
    self.end_hooks.append(end_hook)

#######
#
# Méthodes d'exécution
#
#######

  def execute(self):
    # Starting Log
    self.command_log.start_command(self.options)

    # Init
    self.sort_handled_softwares()
    self.execution_python_version()
    self.prepare_execution()

    # Affichage des options générales
    self.print_start()

    t_ini = int(time.time())
    version_exe_tab = []
    soft_exe_tab = []
    software_list_in_error = []
    number_ok_software = 0
    number_skipped_software = 0
    number_error_software = 0
    errors = ""
    version_test_log_files = {}
    test_log_files = {}
    info_log_files = {}

    success = True

    for index, executor_software in enumerate(self.sorted_executor_softwares):
      # Begin
      t_soft = int(time.time())
      self.print_start_software(index + 1, len(self.sorted_executor_softwares),
                                executor_software.name, executor_software.mode)

      # Soft Begin hook
      self.hooks_begin_software(executor_software)

      # Execution step
      # Result is a list of task name /result of task
      results = executor_software.execute(self)
      # self.command_log.print_info("results : %s"%(str(results)))
      t_soft = int(time.time()) - t_soft
      total_time = str(datetime.timedelta(seconds=t_soft))

      test_msg = "NOT AVAILABLE"
      comment = ""

      if results.test_error is not None:
        test_msg = "|TEST_INIT_ERROR|"
        comment = "See error [#]_."
        errors += "\n" + ".. [#] " + results.test_error.format_rst()

      current_software_info_log = SoftwareInfoLog(executor_software.framework_software,
                          self.command_log.verbose_level,
                          self.command_log.log_directory,
                          self.command_log.print_console,
                          self.command_log.write_in_file,
                          self.command_log.log_files_basename)

      if self.options.get_option(executor_software.name, "write_soft_infos"):
        current_software_info_log.write_infos()
        current_software_info_log.end_command()

      soft_option_str = ""
      if os.path.exists(current_software_info_log.report_html_filename):
        soft_option_str = "`%s options`_" % executor_software.name
        info_log_files[executor_software.name] = current_software_info_log.report_html_filename

      runned_tests = 0
      succeeded_tests = 0
      failed_tests = 0
      known_failed_tests = 0

      if executor_software.framework_software:
        run_tests = executor_software.framework_software.run_tests
        test_suite_list = executor_software.framework_software.test_suite_list

        if test_msg != "|TEST_INIT_ERROR|" and test_suite_list != [] and run_tests:
          my_test_log = TestLog(executor_software.name,
                            executor_software.name,
                            executor_software.framework_software.version,
                            self.command_log.verbose_level,
                            self.command_log.log_directory,
                            self.command_log.print_console,
                            self.command_log.write_in_file,
                            self.command_log.log_files_basename)
          my_test_log.start_command()

          for test_suite in test_suite_list:
            try:
              runned_tests += test_suite.nbTests()
            except Exception as exc:
              test_msg = "|TEST_INIT_ERROR|"
              comment = "See error [#]_."
              errors += "\n" + ".. [#] " + str(exc)
              break
            failed_tests += test_suite.nbFailedTests()
            succeeded_tests += test_suite.nbSucceededTests()
            known_failed_tests += test_suite.nbKnownFailedTests()
            if test_suite.test_log:
              report_lines = ""
              try:
                with open(test_suite.test_log.report_filename) as log_file:
                  report_lines = log_file.readlines()
              except IOError as e:
                success = False
                self.logger.exception("Impossible to read the file %s, exception was: %s" % (test_suite.test_log.report_filename, e))
              try:
                with open(my_test_log.report_filename, "a") as log_file:
                  log_file.write("\n")
                  log_file.writelines(report_lines)
              except IOError as e:
                success = False
                self.logger.exception("Impossible to write into the file %s, exception was: %s" % (my_test_log.report_filename, e))

              test_log_files[executor_software.name] = my_test_log.report_html_filename
              comment = "`%s tests log`_" % executor_software.name
          my_test_log.end_command()

        if test_msg != "|TEST_INIT_ERROR|":
          if test_suite_list != []:
            test_msg = "TO RUN"
            if not run_tests:
              test_msg = ":skipped:`%d IGNORED`" % runned_tests
              pass
            pass

          if failed_tests > 0:
            if failed_tests == known_failed_tests:
              test_msg = ":known-error:`%d KNOWN FAILED`" % (known_failed_tests)
            else:
              test_msg = ":error:`%d FAILED`" % (failed_tests)
              if known_failed_tests > 0:
                test_msg += ", :known-error:`%d KNOWN FAILED`" % (known_failed_tests)
          else:
            if runned_tests > 0:
              if succeeded_tests == runned_tests:
                test_msg = ":ok:`%d SUCCESSFUL`" % (succeeded_tests)
              else:
                test_msg = ":skipped:`%d NOT EXECUTED`" % runned_tests
                comment = ""
            else:
              comment = ""

      status = "|OK|"
      if results.status == SoftwareStatus.OK:
        # Software correctly executed
        number_ok_software += 1
      elif results.status == SoftwareStatus.SKIPPED:
        # # Software skipped
        status = "|SKIPPED|"
        number_skipped_software += 1
      else:
        # Software in error
        status = "|ERROR|"
        number_error_software += 1
        comment = "See error [#]_."
        errors += "\n" + ".. [#] " + results.error.format_rst()

      soft_exe_tab.append((executor_software.name, status, str(runned_tests), test_msg, str(total_time), soft_option_str, comment))

      if results.status == SoftwareStatus.ERROR:
        software_list_in_error.append(executor_software.name)
        # Always print the message
        print()
        print(Bcolors.FAIL + str(results.error) + Bcolors.ENDC)
        print()
        if not self.executor_config.continue_on_failed:
          # Finish log file
          t_ini = int(time.time()) - t_ini
          self.command_log.executor_end(self, t_ini, soft_exe_tab, version_exe_tab,
                                    number_ok_software, number_skipped_software,
                                    number_error_software, software_list_in_error, errors,
                                    test_log_files, info_log_files, version_test_log_files)
          self.command_log.end_command()  # To generates html file
          success = False
          self.logger.error("Error detected in %s - stop project" % results.error_task)

      # Soft End hook
      self.hooks_end_software(executor_software)

      # Clean directories (for any mode but download)
      if executor_software.mode != 'download' and results.status == SoftwareStatus.OK:
        if executor_software.software_config.clean_src_if_success:
          if executor_software.software_config.can_delete_src:
            self.clean_software_directories(executor_software.name, clean_src_dir=True)
          else:
            self.command_log.print_warning("Cannot delete now source directory for %s, it's forbidden because of source dependency" % executor_software.name)
            self.command_log.print_warning("You can delete it (if you know what you are doing!!!) with this command: rm -rf %s" % executor_software.software_config.src_directory)
        if executor_software.software_config.clean_build_if_success:
          self.clean_software_directories(executor_software.name, clean_build_dir=True)

      # Final end...
      self.print_end_software(t_soft)

    self.command_log.print_info("Success status of Executor: %s" % success)

    # Version final tasks
    if success and len(self.version_object.tasks) > 0:
      self.command_log.print_info()
      self.command_log.print_info("##### Version %s final tasks" % self.version_object.name)
      t_version = int(time.time())
      test_suite_list = []
      for task in self.version_object.tasks:
        t_version_task = int(time.time())
        self.command_log.print_info("Run version final task:", space=2)
        try:
          rtn_exe = task.prepare_work(None, 4, self.command_log)
          if not rtn_exe:
            rtn_exe = task.execute(None, self, None)
        except TaskException as e:
          rtn_exe = str(e)
        t_version_task = int(time.time()) - t_version_task
        total_time = str(datetime.timedelta(seconds=t_version_task))

        version_run_tests = self.options.get_option("global", "run_version_tests")
        version_test_suite_list = self.version_object.test_suite_list

        if version_test_suite_list != [] and version_run_tests:
          my_version_test_log = TestLog(self.version_object.name,
                            "",
                            self.version_object.name,
                            self.command_log.verbose_level,
                            self.command_log.log_directory,
                            self.command_log.print_console,
                            self.command_log.write_in_file,
                            self.command_log.log_files_basename)
          my_version_test_log.start_command()
          for test_suite in version_test_suite_list:
            # self.command_log.print_info("test_suite name: "+test_suite.name)
            test_msg = "NO TEST"
            comment = ""
            runned_tests = 0
            succeeded_tests = 0
            failed_tests = 0
            known_failed_tests = 0
            if test_suite.test_log:
              runned_tests = test_suite.nbTests()
              failed_tests = test_suite.nbFailedTests()
              succeeded_tests = test_suite.nbSucceededTests()
              known_failed_tests = test_suite.nbKnownFailedTests()
              report_lines = ""
              try:
                with open(test_suite.test_log.report_filename) as log_file:
                  report_lines = log_file.readlines()
              except IOError as e:
                success = False
                self.logger.exception("Impossible to read the file %s, exception was: %s" % (test_suite.test_log.report_filename, e))
              try:
                with open(my_version_test_log.report_filename, "a") as log_file:
                  log_file.write("\n")
                  log_file.writelines(report_lines)
              except IOError as e:
                success = False
                self.logger.exception("Impossible to write into the file %s, exception was: %s" % (my_version_test_log.report_filename, e))

              version_test_log_files[test_suite.name] = my_version_test_log.report_html_filename
              comment = "`%s tests log`_" % test_suite.name

              if version_test_suite_list != []:
                test_msg = "TO RUN"
                if not version_run_tests:
                  test_msg = ":skipped:`%d IGNORED`" % runned_tests
                  pass
                pass

              if failed_tests > 0:
                if failed_tests == known_failed_tests:
                  test_msg = ":known-error:`%d KNOWN FAILED`" % (known_failed_tests)
                else:
                  test_msg = ":error:`%d FAILED`" % (failed_tests)
                  if known_failed_tests > 0:
                    test_msg += ", :known-error:`%d KNOWN FAILED`" % (known_failed_tests)
              else:
                if runned_tests > 0:
                  if succeeded_tests == runned_tests:
                    test_msg = ":ok:`%d SUCCESSFUL`" % (succeeded_tests)
                  else:
                    test_msg = ":skipped:`%d NOT EXECUTED`" % runned_tests
                    comment = ""
                else:
                  comment = ""

              version_exe_tab.append((test_suite.name, str(runned_tests), test_msg, str(total_time), comment))

          my_version_test_log.end_command()

        if rtn_exe:
          print()
          print(Bcolors.FAIL + rtn_exe + Bcolors.ENDC)
          print()
          if not self.executor_config.continue_on_failed:
            # Finish log file
            t_ini = int(time.time()) - t_ini
            self.command_log.executor_end(self, t_ini, soft_exe_tab, version_exe_tab,
                                        number_ok_software, number_skipped_software, number_error_software,
                                        software_list_in_error, errors, test_log_files, info_log_files,
                                        version_test_log_files)
            self.command_log.end_command()  # To generates html file
            success = False
            self.logger.error("Error detected - stop project")

      t_version = int(time.time()) - t_version
      self.command_log.print_info("Version final tasks are done in %s" % datetime.timedelta(seconds=t_version))

    # Executor end
    t_ini = int(time.time()) - t_ini
    self.command_log.executor_end(self, t_ini, soft_exe_tab, version_exe_tab,
                                  number_ok_software, number_skipped_software, number_error_software,
                                  software_list_in_error, errors, test_log_files, info_log_files,
                                  version_test_log_files)
    return success

  def prepare_execution(self):
    self.command_log.print_debug("Preparing execution")

    # Check if main directory exists
    if not os.path.exists(self.executor_config.main_topdir):
      self.command_log.print_debug(" ".join(["Creating main path:", self.executor_config.main_topdir]))
      os.makedirs(self.executor_config.main_topdir, 0o755)

    if not self.ignore_topdirs:
      # Check if source directory exists
      if not os.path.exists(self.executor_config.src_topdir):
        self.command_log.print_debug(" ".join(["Creating src path:", self.executor_config.src_topdir]))
        os.makedirs(self.executor_config.src_topdir, 0o755)

      # Check if build directory exists
      if not os.path.exists(self.executor_config.build_topdir):
        self.command_log.print_debug(" ".join(["Creating build path:", self.executor_config.build_topdir]))
        os.makedirs(self.executor_config.build_topdir, 0o755)

      # Check if install directory exists
      if not os.path.exists(self.executor_config.install_topdir):
        self.command_log.print_debug(" ".join(["Creating install path:", self.executor_config.install_topdir]))
        os.makedirs(self.executor_config.install_topdir, 0o755)

  def hooks_begin_software(self, software):
    """
    Méthode appelée avant l'exécution du logiciel
    """
    if self.begin_hooks:
      for begin_hook in self.begin_hooks:
        if begin_hook:
          begin_hook(software)

  def hooks_end_software(self, software):
    """
    Méthode appelée après l'exécution du logiciel
    """
    if self.end_hooks:
      for end_hook in self.end_hooks:
        if end_hook:
          end_hook(software)

#######
#
# Méthodes d'affichage lors de l'exécution
#
#######

  def print_start(self):
    max_name_len = max([0] + [len(s.name) for s in self.sorted_executor_softwares])
    self.command_log.executor_begin(self.sorted_executor_softwares,
                                    max_name_len,
                                    self.version_object)
    self.executor_config.print_env(self.command_log, msg="General Executor environnement:", ignore_topdirs=self.ignore_topdirs)

  def print_start_software(self, index, total, name, mode):
    self.command_log.print_info()
    self.command_log.print_info("##### Software %s [%d/%d]" % (name, index, total))
    self.command_log.print_info("Execution mode: %s" % mode, space=2)

  def print_end_software(self, t_soft):
    self.command_log.print_info("Software is done in %s" % str(datetime.timedelta(seconds=t_soft)))

#######
#
# Méthodes utilisés par les logiciels exécutés pour obtenir des informations
# sur leur dépendances afin de définir correctement leur variables.
#
#######

  def get_software_install_directory(self, name):
    if name in self.handled_executor_softwares:
      return self.handled_executor_softwares[name].software_config.install_directory
    elif name in self.softwares_already_done:
      return self.softwares_already_done[name][0]
    else:
      self.logger.error(" ".join(["Requested a software installation directory that is not handled in the project:", name]))

  def get_software_python_version(self, name):
    if name in self.softwares_already_done:
      depend_infos = self.softwares_already_done[name]
      if len(depend_infos) == 2:
        return depend_infos[1]
    return self.executor_config.python_version

  def execution_python_version(self):
    # Step 1: si déjà calculé
    if self.executor_config.python_version != "":
      return

    # Step 2: si dans les softwares
    for executor_software in self.executor_softwares:
      if executor_software.is_python:
        self.executor_config.python_version = executor_software.python_version
        return

    # Step 3: Get Python version from system
    self.executor_config.python_version = misc.compute_python_version(self.version_object._py3)[:-2]

  def get_software_dependency_type(self, software_name):
    if software_name in self.handled_executor_softwares:
      return "project"
    # Check if informations are provided
    elif software_name in self.softwares_already_done:
      return "depend_only"
    else:
      return "system"

#######
#
# Méthodes pour le tri des logiciels
#
#######

  def sort_handled_softwares(self):
    self.command_log.print_debug("Sort list of softwares")
    self.sorted_executor_softwares = []
    self.check_cyclic = []
    for executor_software in self.executor_softwares:
      self.insert(executor_software)
    self.command_log.print_debug("End of sort")

  def insert(self, executor_software):
    if executor_software not in self.sorted_executor_softwares:
      if executor_software in self.check_cyclic :
        self.logger.error("Cyclic dependencies detected for " + executor_software.name)
      self.check_cyclic.append(executor_software)
      for soft_depend in executor_software.depend_list:
        if soft_depend.name in self.handled_executor_softwares:
          self.insert(self.handled_executor_softwares[soft_depend.name])
      self.check_cyclic.pop()
      if executor_software in self.executor_softwares:
        self.sorted_executor_softwares.append(executor_software)

#######
#
# Méthodes auxiliaires
#
#######

  def clean_software_directories(self, software_name, clean_src_dir=False, clean_build_dir=False, clean_install_dir=False, clean_archive=False):

    if software_name in self.handled_executor_softwares:

      software = self.handled_executor_softwares[software_name]

      # Get directories's names
      soft_src_directory = software.software_config.src_directory
      soft_build_directory = software.software_config.build_directory
      soft_install_directory = software.software_config.install_directory
      soft_archive_name = ""
      if isinstance(software, ArchiveSoftware):
        soft_archive_name = software.software_config.user_full_archive_file_name

      # We can now delete known directories
      if clean_src_dir:
        if soft_src_directory != "":
          if os.path.exists(soft_src_directory):
            try:
              fast_delete(soft_src_directory)
            except:
              self.logger.exception(" ".join(["In deleting directory", soft_src_directory]))
        else:
          self.command_log.print_warning(" ".join(["Source directory is not known at this time for software", software_name]))
      if clean_build_dir:
        if soft_build_directory != "":
          if os.path.exists(soft_build_directory):
            try:
              fast_delete(soft_build_directory)
            except:
              self.logger.exception(" ".join(["In deleting directory", soft_build_directory]))
        else:
          self.command_log.print_warning(" ".join(["Build directory is not known at this time for software", software_name]))
      if clean_install_dir:
        if soft_install_directory != "":
          if os.path.exists(soft_install_directory):
            try:
              fast_delete(soft_install_directory)
            except:
              self.logger.exception(" ".join(["In deleting directory", soft_install_directory]))
        else:
          self.command_log.print_warning(" ".join(["Install directory is not known at this time for software", software_name]))
      if clean_archive:
        if soft_archive_name != "":
          if os.path.exists(soft_archive_name):
            try:
              os.remove(soft_archive_name)
            except:
              self.logger.exception(" ".join(["In deleting archive", soft_archive_name]))
        else:
          self.command_log.print_warning(" ".join(["Cannot delete archive file, software is not an archive software !", software_name]))

    else:
      self.logger.error(" ".join(["Requested a software that is not handled in the project:", software_name]))

  def clean_executor(self):
    """
    Cette méthode permet de nettoyer l'executeur pour pouvoir réutiliser
    l'instance
    """
    self.executor_softwares = []
    self.sorted_executor_softwares = []
    self.handled_executor_softwares = {}
    self.softwares_already_done = {}
    self.compiled_list = []

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010, 2015 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import absolute_import
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import object
import contextlib
import os
import urllib.request, urllib.error, urllib.parse

from .tasks.compilation import CompileTask
from .tasks.generic_task import TaskException
from yamm.core.base import misc
from yamm.core.engine import config
from yamm.core.engine import dependency
from yamm.core.base.misc import VerboseLevels


tasks_categories = {"tests": ["download_remote_test",
                              "run_post_build_tests",
                              "run_post_install_tests"],
                    "notests": ["check_installation",
                              "check_installation_and_diff",
                              "check_binary_archive_installation",
                              "install_from_binary_archive",
                              "post_install_config",
                              "collect_sha1",
                              "download",
                              "create_binary_archive",
                              "check_binary_archive",
                              "compile"]}

# This map describes the tasks executed for each executor mode
mode_task_map = {}
mode_task_map["nothing"] = []
mode_task_map["install_from_binary_archive"] = [("check_binary_archive_installation", "skip_software_on_success"),
                                                ("install_from_binary_archive", "continue_on_success")]
mode_task_map["post_install_config"] = [("post_install_config", "continue_on_success")]
mode_task_map["check_installation"] = [("check_installation", "continue_on_success")]
mode_task_map["create_binary_archive"] = [("check_any_installation", "continue_on_success"),
                                                ("create_binary_archive", "continue_on_success")]
mode_task_map["download"] = [("download", "continue_on_success")]
mode_task_map["collect_sha1"] = [("check_any_installation", "continue_on_success"),
                                 ("collect_sha1", "continue_on_success")]
mode_task_map["download_tests"] = [("download_remote_test", "continue_on_success")]
mode_task_map["run_tests"] = mode_task_map["download_tests"] + [("run_post_build_tests", "continue_on_success"),
                                                                                ("run_post_install_tests", "continue_on_success")]
mode_task_map["incremental_compile"] = [("incremental_compile", "continue_on_success")] + mode_task_map["run_tests"]
mode_task_map["build"] = [("compile", "continue_on_success")] + mode_task_map["run_tests"]
mode_task_map["download_and_build"] = mode_task_map["download"] + mode_task_map["build"]
mode_task_map["keep_if_installed"] = [("check_installation", "skip_software_on_success")] + mode_task_map["build"]
mode_task_map["keep_if_no_diff"] = mode_task_map["download_tests"] + [("check_installation_and_diff", "skip_software_on_success")]
mode_task_map["keep_if_no_diff"] += [current_mode for current_mode in mode_task_map["download_and_build"] if current_mode not in mode_task_map["keep_if_no_diff"]]

SoftwareStatus = misc.enum("OK", "SKIPPED", "ERROR")

class SoftwareResults(object):
  def __init__(self):
    self.status = SoftwareStatus.OK
    self.error_task = None
    self.error = None
    self.test_error = None

class ExecutorSoftware(object):

  def __init__(self,
               name,
               software_config,
               framework_software=None,
               mode="download_and_build",
               compile_task=None,
               collect_sha1_task=None,
               create_binary_archive_task=None,
               install_binary_archive_task=None,
               post_install_config_task=None,
               download_test_task_list=None,
               post_build_test_task_list=None,
               post_install_test_task_list=None,
               post_move_test_task_list=None,
               module_load="",
               env_files=None,
               user_dependency_command="",
               depend_list=None,
               is_python=False,
               python_version="",
               logger_name="[SOFTWARE]"
               ):

    # Methods available and tasks needed for each method
    # (These lists could be changed by inherited classes)
    self.tasks_needed_by_method = {}
    self.tasks_needed_by_method["nothing"] = []
    self.tasks_needed_by_method["check_installation"] = []
    self.tasks_needed_by_method["check_installation_and_diff"] = []
    self.tasks_needed_by_method["check_binary_archive_installation"] = []
    self.tasks_needed_by_method["check_any_installation"] = []
    self.tasks_needed_by_method["install_from_binary_archive"] = []
    self.tasks_needed_by_method["post_install_config"] = []
    self.tasks_needed_by_method["collect_sha1"] = []
    self.tasks_needed_by_method["download"] = []
    self.tasks_needed_by_method["download_remote_test"] = []
    self.tasks_needed_by_method["create_binary_archive"] = []
    self.tasks_needed_by_method["compile"] = ["compile_task"]
    self.tasks_needed_by_method["incremental_compile"] = []
    self.tasks_needed_by_method["run_post_build_tests"] = []
    self.tasks_needed_by_method["run_post_install_tests"] = []

    self.name = name
    self.mode = mode
    self.is_python = is_python
    self.python_version = python_version
    self.software_config = software_config
    self.framework_software = framework_software
    self.space = 2
    self.compile_task = compile_task
    self.collect_sha1_task = collect_sha1_task
    self.create_binary_archive_task = create_binary_archive_task
    self.install_binary_archive_task = install_binary_archive_task
    self.post_install_config_task = post_install_config_task
    self.download_test_task_list = download_test_task_list if download_test_task_list is not None else []
    self.post_build_test_task_list = post_build_test_task_list if post_build_test_task_list is not None else []
    self.post_install_test_task_list = post_install_test_task_list if post_install_test_task_list is not None else []
    self.post_move_test_task_list = post_move_test_task_list if post_move_test_task_list is not None else []
    self.module_load = module_load
    self.env_files = env_files if env_files is not None else []
    self.user_dependency_command = user_dependency_command
    self.depend_list = depend_list if depend_list is not None else []
    self.logger = misc.Log("[CORE SOFTWARE %s]" % self.name)
    self.software_name_file_compliant = self.name.replace(' ', '_')
    self.install_ok_filename = ".install_ok_" + self.software_name_file_compliant
    self.install_ok_path = None
    self.binary_archive_sha1 = None
    self.install_binarch_ok_path = None
    ExecutorSoftware.check_args(self)
    self.init_tasks()
    self.patches_filename = ".patches_" + self.software_name_file_compliant
    self.patches_path = None

#######
#
# Méthodes d'initialisation
#
#######

  def init_software_with_executor(self, executor_config):
    """
    Cette méthode est appellée lorsque le logiciel est ajouté
    dans l'exécuteur
    """
    self.software_config.merge_with_config(executor_config)

  def init_tasks(self):
    self.compile_task.set_software_name(self.name)

  def check_args(self):
    if self.name == "":
      self.logger.error("name is mandatory for a software")
    if self.compile_task:
      if not isinstance(self.compile_task, CompileTask):
        self.logger.error("compil task is mandatory for a software")
    if not isinstance(self.software_config, config.SoftwareConfig):
      self.logger.error("software config's type is wrong")
    if self.mode not in mode_task_map:
      self.logger.error("mode is not correct: %s" % str(self.mode))
    for method in mode_task_map[self.mode]:
      for task_needed in self.tasks_needed_by_method[method[0]]:
        if not getattr(self, task_needed):
          self.logger.error(" ".join(["task", task_needed, "is mandatory for mode", self.mode]))

    if not isinstance(self.env_files, type([])):
      self.logger.error("Error env_files type has to be a list")
    for env_file in self.env_files:
      if env_file and not os.path.isabs(env_file):
        self.logger.error("Error, env file path is not an absolute path: %s" % env_file)

    if not isinstance(self.depend_list, type([])):
      self.logger.error("Error depend_list type has to be a list")

    for depend_soft in self.depend_list:
      if not isinstance(depend_soft, dependency.Dependency):
        self.logger.error("Error, a dependency has not a correct type " + str(depend_soft))

  def set_verbose_level(self, verbose):
    self.verbose_level = verbose
    self.logger.verbose = verbose

  def get_dependency_command(self, executor):
    # Dependency environnement
    dependency_command = ""

    # Helpful for complex softwares
    dependency_command += "export CURRENT_SOFTWARE_SRC_DIR=" + self.software_config.src_directory + " ; "
    dependency_command += "export CURRENT_SOFTWARE_BUILD_DIR=" + self.software_config.build_directory + " ; "
    dependency_command += "export CURRENT_SOFTWARE_INSTALL_DIR=" + self.software_config.install_directory + " ; "
    dependency_command += "export PYTHON_VERSION=\"" + executor.executor_config.python_version + "\" ; "

    if self.module_load:
        dependency_command += "module load %s ; " % self.module_load

    for env_file in set(self.env_files):
      if env_file and os.path.exists(env_file):
        dependency_command += ". " + env_file + " >/dev/null 2>&1 ; "

    for depend_soft in self.depend_list:
      dependency_command += depend_soft.get_dependency_env(executor)

    if self.user_dependency_command != "":
      dependency_command += self.user_dependency_command
      if self.user_dependency_command.rstrip()[-1] != ";":
        dependency_command += " ;"

    return dependency_command

  def get_cmake_dependency_command(self, executor):
    # Dependency environnement
    cmake_dependency_command = ""

    for depend_soft in self.depend_list:
      cmake_dependency_command += depend_soft.get_cmake_dependency_env(executor)

    return cmake_dependency_command

#######
#
# Méthodes d'exécution
#
#######

  def print_additional_infos(self):
    if self.framework_software:
      self.command_log.print_info("Version          : " + self.framework_software.version, self.space + 2)
      if self.framework_software.ordered_version != self.framework_software.version:
        self.command_log.print_info("Ordered version  : " + self.framework_software.ordered_version, self.space + 2)

  def prepare_work(self, executor_config):
    self.command_log.print_begin_work("Prepare step", space=self.space)
    self.command_log.print_end_work()
    return ""

  def execute(self, executor):
    self.command_log = executor.command_log
    self.software_config.print_execution_config(self.command_log, self.space)
    self.print_additional_infos()
    self.command_log.print_info()
    software_results = SoftwareResults()

    tasks = mode_task_map[self.mode]
    if len(tasks) == 0:
      software_results.status = SoftwareStatus.SKIPPED

    for task_to_do in tasks:
      # Execution de la tâche
      task_name = task_to_do[0]
      # self.command_log.print_info("Executor software is starting task: %s"%task_name)
      error = None
      try:
        result = getattr(self, task_name)(executor)
        if result:
          error = TaskException(result)
      except TaskException as e:
        error = e
      # self.command_log.print_info("Result: \"%s\""%result)

      if task_name in tasks_categories["tests"]:
        if error is not None and software_results.test_error is None:
          software_results.test_error = error
          self.command_log.print_warning(str(error))
      else:
        if task_to_do[1] == "continue_on_success" and error is not None:
          software_results.status = SoftwareStatus.ERROR
          software_results.error_task = task_name
          software_results.error = error
          break
        elif task_to_do[1] == "skip_software_on_success":
          if error is None:
            software_results.status = SoftwareStatus.SKIPPED
            break
          else:
            # Just print why the software is not skipped
            self.command_log.print_info(str(error), space=self.space)
    # End
    return software_results

  def end_work(self):
    self.command_log.print_begin_work("End", space=self.space)
    self.command_log.print_end_work()
    return ""

  def check_installation_sha1(self, source_sha1):
    self.command_log.print_begin_work("Check installation version", space=self.space)
    self.command_log.print_debug("Source sha1: {0}".format(source_sha1))
    if self.install_ok_path:
      sha1_line = None
      with open(self.install_ok_path, 'rb') as f:
        sha1_line = f.read().strip()
      if sha1_line:
        if sha1_line != source_sha1:
          return "Current installation doesn't match current sources"
        else:
          return ""
      else:
        self.command_log.print_debug("No sha1 information in {0}".format(self.install_ok_path))
        return ""
    self.command_log.print_end_work()

  def check_patches_sha1(self):
    # Method to check whether there is change in the list of patches.
    self.command_log.print_begin_work("Check patches applied", space=self.space)
    if self.patches_path:
      with open(self.patches_path, 'rb') as patch_file:
        sha1_patch_file = sorted(patch_file.read().split())
        self.command_log.print_debug("Patches to apply to the current installation {0}".format(self.software_config.patches))
        msg, sha1_patcheslist_variable = misc.compute_sha1("filelist", self.software_config.patches, "hashlib")
        self.command_log.print_debug("Sha1s of patches planned for install {0}".format(sorted(sha1_patcheslist_variable)))
        self.command_log.print_debug("Sha1s of patches already installed   {0}".format(sha1_patch_file))
        if list(set(sha1_patch_file).symmetric_difference(set(sorted(sha1_patcheslist_variable)))) != []:
        # Symmetric_difference omit values found in both lists
          return "Current set of patches does not match original set."
    self.command_log.print_end_work()

#######
#
# Méthodes d'exécution des tâches
#
#######

  def check_installation(self, executor):
    """
    Options de check_installation

     - check_dependency_compilation

    Stop execution si l'installation est trouvée
    """
    self.command_log.print_begin_work("Check installation", space=self.space)
    # Step 1: check_dependency_compilation
    if self.software_config.check_dependency_compilation:
      for dependency in self.depend_list:
        if dependency.name in executor.compiled_list:
          return "A dependency has changed ({0})".format(dependency.name)

    # Step 2: check install
    old_install_check_filename = os.path.join(self.software_config.install_directory,
                                           self.install_ok_filename)
    logdir = os.path.join(self.software_config.install_directory, '.yamm')
    self.patches_path = os.path.join(logdir, self.patches_filename)
    self.install_ok_path = os.path.join(logdir, self.install_ok_filename)
    self.command_log.print_debug(" ".join(["Checking installation in directory:",
                                 self.software_config.install_directory]))
    if os.path.exists(self.install_ok_path):
      self.command_log.print_debug("Check Installation: a previous installation exists")
    else:
      if os.path.exists(old_install_check_filename):
        if not os.path.exists(logdir):
          os.makedirs(logdir)
        os.rename(old_install_check_filename, self.install_ok_path)
        self.command_log.print_debug("Check Installation: installation already done")
      else:
        return "Check Installation: no installation found in {0}"\
                      .format(self.software_config.install_directory)
    self.command_log.print_end_work()
    return ''

  def check_installation_and_diff(self, executor):
    """
    Cette méthode peut être redéfinie par les classes dérivées
    """
    return self.check_installation(executor)

  def check_binary_archive_installation(self, executor, check_sha1=True):
    """
    Check if the binary archive has already been installed
    """
    logdir = os.path.join(self.software_config.install_directory, '.yamm')
    self.install_binarch_ok_path = os.path.join(logdir, ".install_binarch_ok_" + self.software_name_file_compliant)
    old_install_binarch_ok_path = os.path.join(self.software_config.install_directory,
                                               ".install_binarch_ok_" + self.software_name_file_compliant)
    self.command_log.print_debug("Checking binary archive installation in directory {0}".\
                                 format(self.software_config.install_directory))

    if check_sha1:
      # Read binary archive SHA1
      sha1_url = self.install_binary_archive_task.binary_archive_url + ".sha1"
      if self.logger.verbose < VerboseLevels.DRY_RUN:
        hostname = misc.config_urllib2_netrc(sha1_url, self.command_log)
        if not hostname:
            sha1_url = 'file://%s' % sha1_url
        try:
          with contextlib.closing(urllib.request.urlopen(sha1_url)) as remote_sha1_file:
            sha1_line = remote_sha1_file.read()
          self.binary_archive_sha1 = sha1_line.split(None, 1)[0]
        except:
          raise TaskException("Error: Cannot read SHA1 file at URL {0}".format(sha1_url), True)

        self.command_log.print_debug("Binary archive SHA1 is {0}".format(self.binary_archive_sha1))
        if len(self.binary_archive_sha1) != 40:
          raise TaskException("Error: Invalid SHA1 at URL {0}: {1}".format(sha1_url, self.binary_archive_sha1))
      else:
        if self.command_log:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Command is: urllib2.urlopen({0})".format(sha1_url))

    # Check existence of install_binarch_ok file
    if not os.path.exists(self.install_binarch_ok_path):
      if os.path.exists(old_install_binarch_ok_path):
        if not os.path.exists(logdir):
          os.makedirs(logdir)
        os.rename(old_install_binarch_ok_path, self.install_binarch_ok_path)
      else:
        raise TaskException("Binary archive installation not found, file {0} does not exist".\
                            format(self.install_binarch_ok_path))

    if check_sha1:
      # Check SHA1 in install_binarch_ok file
      try:
        with open(self.install_binarch_ok_path, "r") as binarch_ok_file:
          binarch_ok_sha1 = binarch_ok_file.read()
      except:
        raise TaskException("Error: Cannot read file {0}".format(self.install_binarch_ok_path), True)

      if binarch_ok_sha1 != self.binary_archive_sha1:
        raise TaskException("Current binary archive installation is invalid, SHA1 doesn't match")

    self.command_log.print_info("Check binary archive installation: installation already done",
                                space=self.space)

  def check_any_installation(self, executor):
    """
    Check if the software has already been installed, either by compilation or
    by binary archive extraction
    """
    inst_from_compil_error = None
    inst_from_binarch_error = None
    try:
      result = self.check_installation(executor)
      if result:
        inst_from_compil_error = TaskException(result)
    except TaskException as e:
      inst_from_compil_error = e

    if inst_from_compil_error is not None:
      try:
        result = self.check_binary_archive_installation(executor, False)
        if result:
          inst_from_binarch_error = TaskException(result)
      except TaskException as e:
        inst_from_binarch_error = e

    if inst_from_binarch_error is not None:
      # No installation has been found by either method,
      # we arbitrarily return the error from the compiled installation check
      raise inst_from_compil_error

  def install_from_binary_archive(self, executor):
    """
    Installation du logiciel avec une archive binaire
    """
    self.command_log.print_info("Install from binary archive task:", space=self.space)
    self.install_binary_archive_task.prepare_work(self.software_config, self.space + 2, self.command_log)
    self.install_binary_archive_task.execute(self.install_binarch_ok_path, self.binary_archive_sha1)
    executor.compiled_list.append(self.name)

  def post_install_config(self, executor):
    """
    Configuration du logiciel en exécutant les commandes post-installation
    """
    self.command_log.print_info("Post installation config task:", space=self.space)
    rtn = self.post_install_config_task.prepare_work(self.software_config, self.space + 2, self.command_log)
    if rtn != "":
      return rtn
    try:
      dependancy_command = self.get_dependency_command(executor)
    except misc.LoggerException as e:
      return str(e)
    rtn = self.post_install_config_task.execute(dependancy_command)
    if rtn != "":
      return rtn
    executor.compiled_list.append(self.name)
    return ""

  def collect_sha1(self, executor):
    """
    Collection des sha1 des softwares
    """
    self.command_log.print_info("Collect sha1 task:", space=self.space)
    self.collect_sha1_task.set_software_name(self.name)
    rtn = self.collect_sha1_task.prepare_work(self.software_config, self.name, self.space + 2, self.command_log)
    if rtn != "":
      return rtn
    rtn = self.collect_sha1_task.execute()
    if rtn != "":
      return rtn
    return ""

  def download(self, executor):
    """
    Cette méthode peut être redéfinie par les classes dérivées
    """
    return ""

  def create_binary_archive(self, executor):
    self.command_log.print_info("Create binary archive task:", space=self.space)
    self.create_binary_archive_task.set_software_name(self.name)
    self.create_binary_archive_task.prepare_work(self.software_config, self.space + 2, self.command_log)
    if not self.create_binary_archive_task.check_binary_archive_already_done():
        self.command_log.print_debug('Creating substitution commands')
        for rel_f in self.framework_software.get_substitute_files():
            self.framework_software.replaceSelfInstallPath(rel_f)
            if os.path.splitext(rel_f)[1] == '.py':
                self.framework_software.replacePythonHeaderInstallPath(rel_f)
            for dep in self.framework_software.get_dependencies(recursive=True):
                self.framework_software.replaceSoftwareInstallPath(dep, rel_f)
        self.create_binary_archive_task.set_commands(self.framework_software.make_movable_archive_commands)
    self.create_binary_archive_task.execute()
    return ""

  def compile(self, executor):
    # Print compilation configuration
    self.command_log.print_info("Compilation task:", space=self.space)
    rtn = self.compile_task.prepare_work(self.software_config, self.space + 2, self.command_log)
    if rtn != "":
      return rtn
    try:
      dependancy_command = self.get_dependency_command(executor)
      cmake_dependancy_command = self.get_cmake_dependency_command(executor)
    except misc.LoggerException as e:
      return str(e)
    rtn = self.compile_task.execute(dependancy_command, cmake_dependancy_command,)
    if rtn != "":
      return rtn
    # Add software into the compiled list
    executor.compiled_list.append(self.name)
    return ""

  def incremental_compile(self, executor):
    self.compile_task.build_dir = "keep_or_create"
    self.compile_task.install_dir = "keep"
    self.compile_task.disable_configure_generation = "yes"
    self.compile_task.disable_configure = "yes"
    return self.compile(executor)

  def download_remote_test(self, executor):
    for test_task in self.download_test_task_list:
      self.command_log.print_info("Run download test task:", space=self.space)
      rtn = test_task.prepare_work(self.software_config, self.space + 2, self.command_log)
      if rtn != "":
        return rtn
      rtn = test_task.execute(self.get_dependency_command(executor), executor, self)
      if rtn != "":
        self.command_log.print_end_work_with_warning(rtn)
        return rtn
    return ""

  def run_post_build_tests(self, executor):
    for test_task in self.post_build_test_task_list:
      self.command_log.print_info("Run post-build test task:", space=self.space)
      rtn = test_task.prepare_work(self.software_config, self.space + 2, self.command_log)
      if rtn != "":
        return rtn
      rtn = test_task.execute(self.get_dependency_command(executor), executor, self)
      # if rtn != "":
        # return rtn
    return ""

  def run_post_install_tests(self, executor):
    for test_task in self.post_install_test_task_list:
      self.command_log.print_info("Run post-install test task:", space=self.space)
      rtn = test_task.prepare_work(self.software_config, self.space + 2, self.command_log)
      if rtn != "":
        return rtn
      rtn = test_task.execute(self.get_dependency_command(executor), executor, self)
      # if rtn != "":
        # return rtn
    return ""

class ArchiveSoftware(ExecutorSoftware):

  def __init__(self,
               name,
               software_config,
               framework_software=None,
               mode="download_and_build",
               get_remote_task=None,
               decompress_task=None,
               compile_task=None,
               create_binary_archive_task=None,
               install_binary_archive_task=None,
               post_install_config_task=None,
               collect_sha1_task=None,
               download_test_task_list=None,
               post_build_test_task_list=None,
               post_install_test_task_list=None,
               post_move_test_task_list=None,
               module_load="",
               env_files=None,
               user_dependency_command="",
               depend_list=None,
               is_python=False,
               python_version=""
               ):

    ExecutorSoftware.__init__(self,
                      name=name,
                      mode=mode,
                      framework_software=framework_software,
                      compile_task=compile_task,
                      create_binary_archive_task=create_binary_archive_task,
                      install_binary_archive_task=install_binary_archive_task,
                      post_install_config_task=post_install_config_task,
                      collect_sha1_task=collect_sha1_task,
                      download_test_task_list=download_test_task_list,
                      post_build_test_task_list=post_build_test_task_list,
                      post_install_test_task_list=post_install_test_task_list,
                      post_move_test_task_list=post_move_test_task_list,
                      module_load=module_load,
                      env_files=env_files,
                      user_dependency_command=user_dependency_command,
                      depend_list=depend_list,
                      software_config=software_config,
                      is_python=is_python,
                      python_version=python_version,
                      logger_name="[ARCHIVE SOFTWARE]",
                      )

    # Tâches pour les modes
    self.tasks_needed_by_method["download"] += ["get_remote_task"]
    self.tasks_needed_by_method["compile"] += ["decompress_task"]
    self.get_remote_task = get_remote_task
    self.decompress_task = decompress_task
    self.check_args()

#######
#
# Méthodes d'initialisation
#
#######

  def check_args(self):
    if not isinstance(self.software_config, config.ArchiveSoftwareConfig):
      self.logger.error("software_config type is wrong, should be a ArchiveSoftwareConfig")
    for method in mode_task_map[self.mode]:
      for task_needed in self.tasks_needed_by_method[method[0]]:
        if not getattr(self, task_needed):
          self.logger.error(" ".join(["task", task_needed, "is mandatory for mode", self.mode]))

#######
#
# Méthodes d'exécution des tâches
#
#######

  def download(self, executor):
    """
    Cette méthode permet de récupérer une archive
    """
    self.command_log.print_info("Get Remote task:", space=self.space)
    rtn = self.get_remote_task.prepare_work(self.software_config, self.space + 2, self.command_log)
    if rtn != "":
      return rtn
    rtn = self.get_remote_task.execute()
    if not rtn:
      self.compile_task.source_sha1 = self.get_remote_task.sha1_value
    return rtn

  def compile(self, executor):
    rtn = self.decompress(executor)
    if rtn != "":
      return rtn
    return ExecutorSoftware.compile(self, executor)

  def incremental_compile(self, executor):
    self.decompress_task.mode = "nothing"
    self.decompress_task.src_dir = "keep"
    return ExecutorSoftware.incremental_compile(self, executor)

  def decompress(self, executor):
    """
    Cette méthode permet de décompresser une archive
    """
    self.command_log.print_info("Decompress task:", space=self.space)
    self.decompress_task.archive_dir_name = os.path.basename(self.software_config.src_directory)
    rtn = self.decompress_task.prepare_work(self.software_config, self.space + 2, self.command_log)
    if rtn != "":
      return rtn
    rtn = self.decompress_task.execute()
    return rtn

  def check_installation_and_diff(self, executor, check_sha1=True):
    """
    Cette méthode fait un download puis interroge la tâche remote
    pour savoir s'il y a des différences
    """
    # 1: On check l'installation
    rtn = self.check_installation(executor)
    if rtn != "":
      return rtn

    # 2: On vérifié le sha1 de l'installation et celui de l'archive
    if check_sha1:
      source_sha1 = None
      self.get_remote_task.prepare_work(self.software_config, 0, None)

      # Step 2a: Try to get sha1 sum from server
      archive_file_name_sum = self.get_remote_task.archive_file_name + ".sha1"
      archive_file_name_sum_path = os.path.join(
                                        self.get_remote_task.archive_directory,
                                        archive_file_name_sum)

      # Get sha1 file from server
      sha1_to_remove = not os.path.exists(archive_file_name_sum_path)
      self.command_log.print_debug("Downloading {0}".format(archive_file_name_sum))
      msg = self.get_remote_task.download(archive_file_name_sum)
      if msg:
        self.command_log.print_debug(msg)
#         return ""

      if os.path.exists(archive_file_name_sum_path):
        self.command_log.print_debug("Get sha1 from {0}".format(archive_file_name_sum))
        msg, source_sha1 = misc.get_sha1_from_sha1file(
                                      self.get_remote_task.archive_directory,
                                      archive_file_name_sum)
        if msg:
          self.command_log.print_debug(msg)
#           return ''

        if sha1_to_remove:
          os.remove(archive_file_name_sum_path)

      if source_sha1:
        rtn = self.check_installation_sha1(source_sha1)
        if rtn != "":
          return rtn
      else:
        # Step 2b: Try to compute sha1 sum from archive
        self.command_log.print_debug("Compute sha1 from archive {0}".format(self.get_remote_task.archive_file_name))
        archive_file_name_path = os.path.join(
                                        self.get_remote_task.archive_directory,
                                        self.get_remote_task.archive_file_name)
        if os.path.exists(archive_file_name_path):
          msg, source_sha1 = misc.compute_sha1_archive(self.get_remote_task.archive_directory,
                                        self.get_remote_task.archive_file_name,
                                        self.get_remote_task.sha1_tool)

          if msg:
            self.command_log.print_debug(msg)
            return msg
        else:
          self.command_log.print_debug("Archive not found".format(self.get_remote_task.archive_file_name))

    # 3: Check if there is change in patches with sha1 lists
    self.command_log.print_debug("Compare sha1s of patches planned for install to the one already installed")
    if os.path.isfile(self.patches_path):
    # If [INSTALLDIR]/.yamm/patches_SOFTWARE exists
      rtn = self.check_patches_sha1()
      # If yes and set of patches different from patches_SOFTWARE, we build
    elif self.software_config.patches != []:
      # Else if self.patches not empty, we build
      rtn = "There are patches to apply for the first time. OK Yamm, patch!"

    return rtn

class RemoteSoftware(ExecutorSoftware):

  def __init__(self,
               name,
               software_config,
               framework_software,
               mode="download_and_build",
               remote_task=None,
               compile_task=None,
               create_binary_archive_task=None,
               install_binary_archive_task=None,
               post_install_config_task=None,
               collect_sha1_task=None,
               download_test_task_list=None,
               post_build_test_task_list=None,
               post_install_test_task_list=None,
               post_move_test_task_list=None,
               module_load="",
               env_files=None,
               user_dependency_command="",
               depend_list=None,
               is_python=False,
               python_version="",
               ):

    ExecutorSoftware.__init__(self,
                      name=name,
                      mode=mode,
                      framework_software=framework_software,
                      compile_task=compile_task,
                      create_binary_archive_task=create_binary_archive_task,
                      install_binary_archive_task=install_binary_archive_task,
                      post_install_config_task=post_install_config_task,
                      collect_sha1_task=collect_sha1_task,
                      download_test_task_list=download_test_task_list,
                      post_build_test_task_list=post_build_test_task_list,
                      post_install_test_task_list=post_install_test_task_list,
                      post_move_test_task_list=post_move_test_task_list,
                      module_load=module_load,
                      env_files=env_files,
                      user_dependency_command=user_dependency_command,
                      depend_list=depend_list,
                      software_config=software_config,
                      is_python=is_python,
                      python_version=python_version,
                      logger_name="[REMOTE SOFTWARE]"
                      )

    # Tâches pour les modes
    self.tasks_needed_by_method["download"] += ["remote_task"]
    self.remote_task = remote_task
    self.remote_task.set_software_name(self.name)
    self.check_args()
    self.check_installation_and_diff_download_done = False

  def check_args(self):
    for method in mode_task_map[self.mode]:
      for task_needed in self.tasks_needed_by_method[method[0]]:
        if not getattr(self, task_needed):
          self.logger.error(" ".join(["task", task_needed, "is mandatory for mode", self.mode]))

#######
#
# Méthodes d'exécution des tâches
#
#######

  def print_additional_infos(self):
    url = os.path.join(self.remote_task.root_repository, self.remote_task.repository_name)
    self.command_log.print_info("Repository       : " + url, self.space + 2)
    ExecutorSoftware.print_additional_infos(self)

  def download(self, executor):
    """
    Cette méthode met à jour le répertoire source
    """
    if not self.check_installation_and_diff_download_done:
      self.command_log.print_info("Update software sources:", space=self.space)
      rtn = self.remote_task.prepare_work(self.software_config, self.space + 2, self.command_log)
      if rtn != "":
        return rtn
      rtn = self.remote_task.execute()
      if not rtn and self.remote_task.last_commit:
        self.compile_task.source_sha1 = self.remote_task.last_commit
      return rtn
    else:
      return ""

  def compile(self, executor):
    rtn = self.remote_task.prepare_work(self.software_config, self.space + 2, self.command_log)
    if rtn != "":
      return rtn
    last_commit = self.remote_task.get_last_commit_id()
    if last_commit:
      self.compile_task.source_sha1 = last_commit
    return ExecutorSoftware.compile(self, executor)

  def check_installation_and_diff(self, executor, check_sha1=True):
    """
    Cette méthode fait un download puis interroge la tâche remote
    pour savoir s'il y a des différences
    """
    # 1: On check l'installation
    rtn = self.check_installation(executor)
    if rtn != "":
      return rtn
    # 2: On met à jour le répertoire source
    rtn = self.download(executor)
    if rtn != "":
      return rtn

    self.check_installation_and_diff_download_done = True
    # 3: On regarde s'il y a des différences
    if self.remote_task.diff == 0:
      rtn = ""
      if check_sha1:
        # self.remote_task.last_commit was set by the download task
        rtn = self.check_installation_sha1(self.remote_task.last_commit)
      return rtn
    else:
      return "diff found !"

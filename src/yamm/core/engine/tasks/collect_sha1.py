# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.engine.tasks.generic_task import GenericTask


class CollectSha1Task(GenericTask):
    """
      This task is called to collect the sha1 of the software source
      into a sha1 collection file.
    """

    def __init__(self,
                 sha1_collection_file,
                 name_max_length=0,
                 version_max_length=0,
                 verbose_level=VerboseLevels.INFO,
                 software_version=""
                 ):
        GenericTask.__init__(self, '[COLLECT SHA1 TASK]', verbose_level)
        self.sha1_collection_file = sha1_collection_file
        self.name_max_length = name_max_length
        self.version_max_length = version_max_length
        self.install_ok_path = ""
        self.src_topdir = ""
        self.software_version=software_version

    def set_software_name(self, software_name):
        GenericTask.set_software_name(self, software_name)
        self.install_ok = ".install_ok_" + self.software_name_replaced
        self.install_binarch_ok = ".install_binarch_ok_" + self.software_name_replaced

    def prepare_work(self, software_config, software_name, space, command_log):

        self.command_log = command_log
        self.space = space
        self.command_log.print_begin_work("Prepare step", space=self.space)

        # Get values from config
        self.software_name = software_name

        # Paths for check files
        self.install_ok_path = os.path.join(software_config.install_directory,
                                            '.yamm', self.install_ok)
        self.install_binarch_ok_path = os.path.join(software_config.install_directory,
                                                    '.yamm', self.install_binarch_ok)

        self.command_log.print_end_work()
        return ""

    def execute(self):
        self.command_log.print_begin_work("Collect sha1", space=self.space)
        sha1sum = "no_sha1"
        namecolumnlength = self.name_max_length or len(self.software_name)
        versioncolumnlength = self.version_max_length or len(self.software_version)

        install_ok_path = self.install_ok_path
        if not os.path.exists(install_ok_path):
            install_ok_path = self.install_binarch_ok_path
        if os.path.exists(install_ok_path):
            with open(install_ok_path, 'rb') as install_ok_file:
                sha1sum = install_ok_file.read().strip() or sha1sum
        if self.logger.verbose < VerboseLevels.DRY_RUN:
            software_version = misc.transform(self.software_version, dot=False,
                                              dash=False, slash=False)
            with open(self.sha1_collection_file, 'a') as sha1_file:
                sha1_file.write("{0} {1} {2}\n".format(self.software_name.ljust(namecolumnlength),
                                                       software_version.ljust(versioncolumnlength),
                                                       sha1sum))
        else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug(sha1sum)
        self.command_log.print_end_work()
        return ""

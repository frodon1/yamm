#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)



from io import open
import os
import shutil
import traceback

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.base.misc import compute_sha1
from yamm.core.base.misc import fast_delete
from yamm.core.engine.tasks.generic_task import GenericTask, TaskException, touch


class AdditionalFile(GenericTask):

  def __init__(self, name, filepath, insertpath, verbose_level=VerboseLevels.INFO):
    GenericTask.__init__(self, '[ADDITIONAL FILE]', verbose_level)
    self.name = name
    self.filepath = filepath
    self.insertpath = insertpath

    if not os.path.exists(self.filepath):
      self.logger.error("Cannot find file at: %s" % self.filepath)


# TODO
# post_build_commands
class CompileTask(GenericTask):

  # compilation supported
  compil_supported_type = ["autoconf", "qmake", "cmake", "configure_with_space",
                           "python", "python_egg", "egg", "specific", "rsync", "fake"]
  src_dir_options = ["delete_and_create", "keep_or_create", "keep", "delete"]
  build_dir_options = ["delete_and_create", "keep_or_create"]
  install_dir_options = ["delete", "keep", "backup", "backup_copy"]

  def __init__(self,
               compil_type,
               software_source_type,

               source_sha1=None,

               egg_file="",
               pro_file="",
               gen_file="",

               config_options="",
               build_options="",
               make_repeat=0,

               additional_src_files=[],

               pre_configure_commands=[],
               gen_commands=[],
               post_configure_commands=[],

               pre_build_commands=[],
               post_build_commands=[],

               pre_install_commands=[],
               post_install_commands=[],

               specific_configure_command="",
               specific_build_command="",
               specific_install_command="",

               src_dir="keep",
               build_dir="delete_and_create",
               install_dir="delete",

               disable_configure_generation="no",
               disable_configure="no",

               xterm_make=False,
               shell=None,
               patches=[],
               verbose_level=VerboseLevels.INFO,

               build_depends=[]
               ):

    GenericTask.__init__(self, '[COMPILE TASK]', verbose_level)
    self.source_sha1 = source_sha1
    self.xterm_make = xterm_make
    self.shell = shell
    self.compil_type = compil_type
    self.software_source_type = software_source_type
    self.egg_file = egg_file
    self.pro_file = pro_file
    self.gen_file = gen_file
    self.gen_commands = gen_commands
    self.src_dir = src_dir
    self.build_dir = build_dir
    self.install_dir = install_dir
    self.config_options = config_options
    self.build_options = build_options
    self.make_repeat = make_repeat
    self.specific_configure_command = specific_configure_command
    self.specific_build_command = specific_build_command
    self.specific_install_command = specific_install_command
    self.pre_configure_commands = pre_configure_commands
    self.post_configure_commands = post_configure_commands
    self.pre_build_commands = pre_build_commands
    self.pre_install_commands = pre_install_commands
    self.post_install_commands = post_install_commands
    self.additional_src_files = additional_src_files

    self.disable_configure_generation = disable_configure_generation
    self.disable_configure = disable_configure

    self.patches = patches

    self.build_depends = build_depends or []
    if not isinstance(self.build_depends, list):
      self.build_depends = [self.build_depends]

    self.check_args()

  def check_args(self):

    if self.gen_commands != [] and self.gen_file != "":
      self.logger.error("Error, you cannot have gen_command and gen_file option")

    if self.compil_type not in self.compil_supported_type:
      self.logger.error("Error, compil_type option " + self.compil_type + " not supported")

    if self.compil_type == "egg" and self.egg_file == "":
      self.logger.error("Error, you have to provide an egg_file name for egg softwares")

    if self.compil_type == "qmake" and self.pro_file == "":
      self.logger.error("Error, you have to provide a pro_file name for qmake softwares")

    if self.compil_type == "specific" and self.specific_install_command == "":
      self.logger.error("Error, you have to provide a specific_install_command name for specific softwares")

    if self.src_dir not in self.src_dir_options:
      self.logger.error("Error, src_dir option " + self.src_dir + " not supported")

    if self.build_dir not in self.build_dir_options:
      self.logger.error("Error, build_dir option " + self.build_dir + " not supported")

    if self.install_dir not in self.install_dir_options:
      self.logger.error("Error, install_dir option " + self.install_dir + " not supported")

    if not isinstance(self.additional_src_files, type([])):
      self.logger.error("Error additional_src_files type has to be a list")
    for add_file in self.additional_src_files:
      if not isinstance(add_file, AdditionalFile):
        self.logger.error("additional file type has to be a compilation.AdditionalFile")

  def check_build_depends(self):
    from yamm.core.base import debian_tools
    for build_depend in self.build_depends:
      is_installed = debian_tools.is_installed(build_depend)
      if is_installed is None:
        self.logger.warning("%s is a build dependency but this package does not exist" % build_depend)
      if not is_installed:
        self.logger.warning("%s package is missing. Compilation may fail or not be complete" % build_depend)

#######
#
# Méthodes d'initialisation
#
#######

  def set_software_name(self, software_name):
    self.software_name_replaced = software_name.replace(" ", "_")

#######
#
# Méthodes d'exécution
#
#######

  def prepare_work(self, software_config, space, command_log):

    self.command_log = command_log
    self.space = space
    self.command_log.print_begin_work("Prepare step", space=self.space)

    self.src_directory = software_config.src_directory
    self.cmake_src_directory = software_config.cmake_src_directory
    self.build_directory = software_config.build_directory
    self.install_directory = software_config.install_directory
    self.yamm_src_log_dir = software_config.yamm_src_log_dir
    self.yamm_build_log_dir = software_config.yamm_build_log_dir
    self.yamm_install_log_dir = software_config.yamm_install_log_dir

    self.log_dirs = software_config.log_dirs.copy()

    self.parallel_make = software_config.parallel_make
    self.make_target = software_config.make_target
    self.backup_directory = software_config.backup_directory
    self.install_ok_path = os.path.join(self.yamm_install_log_dir,
                                  ".install_ok_" + self.software_name_replaced)
    self.patches_path = os.path.join(self.yamm_install_log_dir,
                                  ".patches_" + self.software_name_replaced)

    if self.logger.verbose < VerboseLevels.DRY_RUN:
      if self.src_dir == "delete" or self.src_dir == "delete_and_create":
        # Check if directory exists
        if os.path.exists(self.src_directory):
          try:
            fast_delete(self.src_directory)
          except:
            self.logger.exception("Error in deleting directory" + self.src_directory)

      if self.src_dir == "keep_or_create" or self.src_dir == "delete_and_create":
        if not os.path.exists(self.src_directory):
          try:
            os.makedirs(self.src_directory, 0o755)
          except:
            self.logger.exception("Error in creating directory" + self.src_directory)

      # Clean up old execution and backup env_build.sh file is present
      env_build = software_config.env_build
      env_build_backup = os.path.join(self.build_directory, os.pardir, 'env_build.sh')
      if self.build_dir == "delete_and_create":
        # Check if directory exists
        if os.path.exists(self.build_directory):
          if os.path.exists(env_build):
            try:
              shutil.move(env_build, env_build_backup)
            except:
              self.logger.exception("Error in saving env file %s to %s" % (env_build, env_build_backup))
          try:
            fast_delete(self.build_directory)
          except:
            self.logger.exception("Error in deleting directory %s" % self.build_directory)

      if self.install_dir == "delete":
        # Check if directory exists
        if os.path.exists(self.install_directory):
          try:
            fast_delete(self.install_directory)
          except:
            self.logger.exception("Error in deleting directory" + self.install_directory)

      if self.install_dir in ("backup", "backup_copy"):
        if os.path.exists(self.install_directory):
          # self.command_log.print_info("Compile task install_dir option is %s"%self.install_dir)
          # self.command_log.print_info("Backup directory is %s"%self.backup_directory)
          # Check if backup directory already exists
          if os.path.exists(self.backup_directory):
            try:
              # self.command_log.print_info("Removing existing backup directory: %s"%self.backup_directory)
              fast_delete(self.backup_directory)
            except:
              self.logger.exception("Error in deleting directory" + self.backup_directory)
          if self.install_dir == "backup":
            try:
              # self.command_log.print_info("Moving install directory from %s to %s"%(self.install_directory,self.backup_directory))
              os.rename(self.install_directory, self.backup_directory)
            except:
              self.logger.exception("Error in renaming install directory from %s to %s" % (self.install_directory, self.backup_directory))
          if self.install_dir == "backup_copy":
            try:
              # self.command_log.print_info("Copying install directory from %s to %s"%(self.install_directory,self.backup_directory))
              shutil.copytree(self.install_directory, self.backup_directory)
            except:
              self.logger.exception("Error in copying install directory from %s to %s" % (self.install_directory, self.backup_directory))

      # Create source directory if it does not exist (needed for some artefact softwares like PYTHONSTARTUP)
      if not os.path.exists(self.yamm_src_log_dir):
        try:
          os.makedirs(self.yamm_src_log_dir, 0o755)
        except:
          self.logger.exception("Error in creating directory" + self.yamm_src_log_dir)

      # Create build directory if it does not exist
      if not os.path.exists(self.yamm_build_log_dir):
        try:
          os.makedirs(self.yamm_build_log_dir, 0o755)
        except:
          self.logger.exception("Error in creating directory" + self.yamm_build_log_dir)
        # Recover env_build.sh file
        if os.path.exists(env_build_backup) and not os.path.exists(env_build):
          shutil.move(env_build_backup, env_build)

      # Additional files
      for add_file in self.additional_src_files:
        try:
          shutil.copyfile(add_file.filepath, os.path.join(self.src_directory, add_file.insertpath))
        except:
          self.logger.exception("Cannot copy additional source file %s, exeception is %s" % (add_file.name, traceback.print_exc()))
    else:
      self.command_log.print_debug("=== SIMULATION MODE ===")
    self.command_log.print_end_work()
    return ""

  def find_python_version(self, dependency_command):
    return misc.compute_python_version(command=dependency_command)[:-2]

  def install_ok(self):
    # Install ok
    if os.path.exists(self.yamm_install_log_dir) and \
        not os.path.isdir(self.yamm_install_log_dir):
      os.remove(self.yamm_install_log_dir)
    if not os.path.exists(self.yamm_install_log_dir):
      os.makedirs(self.yamm_install_log_dir)
    if self.source_sha1:
      with open(self.install_ok_path, 'wb') as f:
        f.write(self.source_sha1)
        f.write(b'\n')
    else:
      touch(self.install_ok_path)

  def patches_sha1(self):
    # Create a sha1 with patch files
    if os.path.exists(self.yamm_install_log_dir) and \
        not os.path.isdir(self.yamm_install_log_dir):
      os.remove(self.yamm_install_log_dir)
    if not os.path.exists(self.yamm_install_log_dir):
      os.makedirs(self.yamm_install_log_dir)
    if self.patches != []:
      msg, sha1_list = compute_sha1("filelist", self.patches, "hashlib")
      with open(self.patches_path, 'wb') as patches_file:
        for sha1 in sha1_list:
          patches_file.write(sha1)
          patches_file.write('\n')

  def execute(self, dependency_command, cmake_dependency_command=""):

    self.check_build_depends()
    if self.compil_type == "autoconf":
      rtn = self.execute_autoconf(dependency_command)
      if rtn != "":
        return rtn
    if self.compil_type == "qmake":
      rtn = self.execute_qmake(dependency_command)
      if rtn != "":
        return rtn
    if self.compil_type == "cmake":
      rtn = self.execute_cmake(dependency_command, cmake_dependency_command)
      if rtn != "":
        return rtn
    elif self.compil_type == "configure_with_space":
      rtn = self.execute_configure_with_space(dependency_command)
      if rtn != "":
        return rtn

    # Since we are going to compile we need to delete install file if it exists
    if os.path.exists(self.install_ok_path):
      if os.path.isfile(self.install_ok_path):
        # Try to delete the file
        try:
          if self.logger.verbose < VerboseLevels.DRY_RUN:
            os.remove(self.install_ok_path)
          else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug("Command is: os.remove{0})".format(self.install_ok_path))
        except:
          self.logger.exception("Error in deleting file %s", self.install_ok_path)
      else:
        # This should not happen !
        self.logger.error("Install test file is not a file !: %s", self.install_ok_path)

    # These three types do configure, build and install in the src directory
    if self.compil_type == "python" or self.compil_type == "python_egg":
      return self.execute_python(dependency_command)
    elif self.compil_type == "egg":
      return self.execute_egg(dependency_command)
    elif self.compil_type == "specific":
      return self.execute_specific(dependency_command)
    elif self.compil_type == "rsync":
      return self.execute_rsync(dependency_command)
    elif self.compil_type == "fake":
      return self.execute_fake(dependency_command)
    # Compile
    return self.execute_make(dependency_command)

  def execute_autoconf(self, dependency_command):

    if self.disable_configure == "no":

      if self.disable_configure_generation == "no":
        # Generates configure
        if self.gen_file != "":
          self.command_log.print_begin_work("Configure generation", space=self.space)
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command
          command += "./" + self.gen_file
          command += ") > {0} 2>&1 ;".format(self.log_dirs['generation'])
          self.execute_command("configure generation", command,
                               logfilename=self.log_dirs['generation_cmd'])
          self.command_log.print_end_work()

        elif self.gen_commands != []:
          self.command_log.print_begin_work("Configure generation", space=self.space)
          for gen_command in self.gen_commands:
            command = "(cd {0} ;".format(self.src_directory)
            command += dependency_command + ' '
            command += gen_command + ' '
            command += ") >> {0} 2>&1 ;".format(self.log_dirs['generation'])
            self.execute_command("configure generation", command,
                               logfilename=self.log_dirs['generation_cmd'])
          self.command_log.print_end_work()

      # Pre-configure for patches if archive mode
      if self.software_source_type == "archive":
        if self.patches != []:
            for patch in self.patches:
              self.pre_configure_commands += ["patch -p1 < " + patch]

      # Configure
      if self.pre_configure_commands != []:
        self.command_log.print_begin_work("Pre-configure generation", space=self.space)
        for pre_configure_command in self.pre_configure_commands:
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command + ' '
          command += pre_configure_command + ' '
          command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_configure'])
          self.execute_command("pre_configure_commands", command,
                               logfilename=self.log_dirs['pre_configure_cmd'])
        self.command_log.print_end_work()

      self.command_log.print_begin_work("Configure", space=self.space)
      command = "(cd {0} ;".format(self.build_directory)
      command += dependency_command + ' '
      command += "{0}/configure ".format(self.src_directory)
      command += "--prefix={0} ".format(self.install_directory)
      # Config options
      if self.config_options != "":
        command += self.config_options + ' '
      command += ") > {0} 2>&1 ;".format(self.log_dirs['configure'])
      self.execute_command("configure", command, logfilename=self.log_dirs['configure_cmd'])
      self.command_log.print_end_work()

      if self.post_configure_commands != []:
        open(self.log_dirs['post_configure_cmd'], 'wb')
        self.command_log.print_begin_work("Post-configure generation", space=self.space)
        for post_configure_command in self.post_configure_commands:
          command = "(cd {0} ;".format(self.build_directory)
          command += dependency_command + ' '
          command += post_configure_command + ' '
          command += ") >> {0} 2>&1 ;".format(self.log_dirs['post_configure'])
          self.execute_command("post_configure_commands", command + '\n',
                               logfilename=self.log_dirs['post_configure_cmd'],
                               logmode='a')
        self.command_log.print_end_work()
    # Create file which contains sha1 of patches if archive mode
    if self.software_source_type == "archive":
      self.patches_sha1()
    return ""

  def execute_qmake(self, dependency_command):

    if self.disable_configure == "no":
      # Pre-configure for patches if archive mode
      if self.software_source_type == "archive":
        if self.patches != []:
            for patch in self.patches:
              self.pre_configure_commands += ["patch -p1 < " + patch]
      if self.pre_configure_commands != []:
        open(self.log_dirs['pre_configure_cmd'], 'wb')
        for pre_configure_command in self.pre_configure_commands:
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command
          command += pre_configure_command
          command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_configure'])
          self.execute_command("pre_configure_commands", command + '\n',
                               logfilename=self.log_dirs['pre_configure_cmd'],
                               logmode='a')
      # Qmake
      self.command_log.print_begin_work("Configure", space=self.space)
      command = "( "
      command += dependency_command
      if self.specific_configure_command != "":
        command += "cd {0} ;".format(self.src_directory)
        command += self.specific_configure_command
      if not command.strip().endswith(';'):
        command += ' ; '
      command += "cd {0} ;".format(self.build_directory)
      command += "qmake -d {0} ".format(os.path.join(self.src_directory,
                                                  self.pro_file))
      command += ") >> {0} 2>&1 ;".format(self.log_dirs['configure'])
      self.execute_command("configure", command, logfilename=self.log_dirs['configure_cmd'])
      self.command_log.print_end_work()
    # Create file which contains sha1 of patches if archive mode
    if self.software_source_type == "archive":
      self.patches_sha1()
    return ""

  def execute_cmake(self, dependency_command, cmake_dependency_command):

    if self.disable_configure == "no":
      if self.disable_configure_generation == "no":
        # Generates configure
        if self.gen_file != "":
          self.command_log.print_begin_work("Configure generation", space=self.space)
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command + ' '
          command += "./{0} ".format(self.gen_file)
          command += ") > {0} 2>&1 ;".format(self.log_dirs['generation'])
          self.execute_command("configure generation", command,
                               logfilename=self.log_dirs['generation_cmd'])
          self.command_log.print_end_work()

      # cmake
      # Patches if archive mode
      if self.software_source_type == "archive":
        if self.patches != []:
          for patch in self.patches:
            self.pre_configure_commands += ["patch -p1 < " + patch + " ; "]

      if self.pre_configure_commands != []:
        open(self.log_dirs['pre_configure_cmd'], 'wb')
        for pre_configure_command in self.pre_configure_commands:
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command + ' '
          command += pre_configure_command
          command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_configure'])
          self.execute_command("pre_configure_commands", command + '\n',
                               logfilename=self.log_dirs['pre_configure_cmd'],
                               logmode='a')

      self.command_log.print_begin_work("Configure", space=self.space)
      command = "(cd {0} ; ".format(self.build_directory)
      command += dependency_command
      command += " cmake -DCMAKE_INSTALL_PREFIX={0} ".format(self.install_directory)
      command += cmake_dependency_command + ' '
      # Config options
      if self.config_options != "":
        command += self.config_options + ' '
      if self.cmake_src_directory:
        command += self.cmake_src_directory
      else:
        command += self.src_directory
      command += ") > {0} 2>&1 ;".format(self.log_dirs['configure'])
      self.execute_command("configure", command, logfilename=self.log_dirs['configure_cmd'])
      self.command_log.print_end_work()
      if self.pre_build_commands != []:
        command = "(cd {0} ;".format(self.src_directory)
        command += dependency_command + ' '
        for pre_build_command in self.pre_build_commands:
          command += pre_build_command
          if not command.strip().endswith(';'):
            command += ' ;'
        command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_build'])
        self.execute_command("pre_build_commands", command,
                             logfilename=self.log_dirs['pre_build_cmd'])
    # Create file which contains sha1 of patches if archive mode
    if self.software_source_type == "archive":
      self.patches_sha1()
    return ""

  def execute_python(self, dependency_command):

    self.log_dirs['install'] = os.path.join(self.yamm_src_log_dir, 'install.log')

    user_python_version = self.find_python_version(dependency_command)

    python_command = "export PYTHONPATH={0}:{0}/lib/python{1}/site-packages:"\
          "$PYTHONPATH ; ".format(self.install_directory, user_python_version)
    python_command += "mkdir -p {0}/lib/python{1}/site-packages ; "\
                          .format(self.install_directory, user_python_version)

    # Pre-configure for patches if archive mode
    if self.software_source_type == "archive":
      if self.patches != []:
        for patch in self.patches:
          self.pre_configure_commands += ["patch -p1 < " + patch]

    if self.pre_configure_commands != []:
        open(self.log_dirs['pre_configure_cmd'], 'wb')
        for pre_configure_command in self.pre_configure_commands:
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command + ' '
          command += pre_configure_command
          command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_configure'])
          self.execute_command("pre_configure_commands", command + '\n',
                               logfilename=self.log_dirs['pre_configure_cmd'],
                               logmode='a')

    # Optional configure
    if self.config_options != "":

      self.command_log.print_begin_work("Configure", space=self.space)
      command = "(cd {0} ;".format(self.src_directory)
      command += dependency_command + ' '
      command += "python setup.py configure " + self.config_options
      if not command.strip().endswith(';'):
        command += ' ; '
      command += ") >> {0} 2>&1 ;".format(self.log_dirs['configure'])
      self.execute_command("configure", command, logfilename=self.log_dirs['configure_cmd'])
      self.command_log.print_end_work()

    # Build
    self.command_log.print_begin_work("Build", space=self.space)

    # User Pre Build
    if self.pre_build_commands != []:
      command = "(cd {0} ;".format(self.src_directory)
      command += dependency_command + ' '
      for pre_build_command in self.pre_build_commands:
        command += pre_build_command
        if not command.strip().endswith(';'):
          command += ' ;'
      command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_build'])
      self.execute_command("python pre_build_commands", command,
                           logfilename=self.log_dirs['pre_build_cmd'])

    # Remove --index-url option for the build command and add info into setup.cfg file
    if '--index-url' in self.build_options:
      build_options_split = self.build_options.split()
      index_to_remove = build_options_split.index('--index-url')
      # Pop 2 times index of '--index-url' because the option has a value:
      # --index-url my.server.com
      build_options_split.pop(index_to_remove)
      index_url = build_options_split.pop(index_to_remove)
      self.build_options = ' '.join(build_options_split)
      try:
        import ConfigParser
      except ImportError:
        import configparser as ConfigParser
      cfg = ConfigParser.ConfigParser()
      setup_cfg = os.path.join(self.src_directory, 'setup.cfg')
      cfg.read(setup_cfg)
      if 'easy_install' not in cfg.sections():
        cfg.add_section('easy_install')
      cfg.set('easy_install', 'index-url', index_url)
      cfg.write(open(setup_cfg, 'w'))
      del cfg

    command = "(cd {0} ; ".format(self.src_directory)
    command += dependency_command + ' ' + python_command
    # Trick to force installation of dependencies in software directory, not in
    # Python lib directory
    # See https://pythonhosted.org/setuptools/setuptools.html#development-mode
    command += "python -c \"import setuptools; g=globals(); " \
        "g['__file__']='setup.py'; exec(open('setup.py').read(), g)\" develop "\
        "--install-dir {0} {1}".format(self.install_directory, self.build_options)
    command += ") > {0} 2>&1 ;".format(self.log_dirs['build'])
    try:
      self.execute_command("python build", command,
                           logfilename=self.log_dirs['build_cmd'])
    except TaskException as e:
      if self.make_repeat == 0:
        raise e
      self.command_log.print_end_work(" NOK (Warning: Python build failed, try to repeat)")
      if not self.xterm_make:
        error_file_name = os.path.join(self.build_directory,
                                       os.pardir,
                                       'build_error_{0}.log'.format(self.software_name_replaced))
        mv_command = "mv {0} {1}".format(self.log_dirs['build'], error_file_name)
        self.logger.info("Log file is in file %s" % error_file_name)
        self.execute_command("Move log build file", mv_command)

      command = "(cd {0} ; ".format(self.src_directory)
      command += dependency_command + ' ' + python_command
      command += "python setup.py build {0}".format(self.build_options)
      command += ") > {0} 2>&1 ;".format(self.log_dirs['build'])
      self.execute_command("python build", command, logfilename=self.log_dirs['build_cmd'])
    self.command_log.print_end_work()

    # Optional Build Egg
    if self.compil_type == "python_egg":
      self.command_log.print_begin_work("Build Egg", space=self.space)
      command = "(cd {0} ; ".format(self.src_directory)
      command += dependency_command
      command += " python setup.py bdist_egg"
      command += ") > {0} 2>&1 ;".format(self.log_dirs['build_egg'])
      self.execute_command("python build_egg", command,
                           logfilename=self.log_dirs['build_egg_cmd'])
      self.command_log.print_end_work()

    # Install
    self.command_log.print_begin_work("Installation", space=self.space)

    # User Pre Install
    if self.pre_install_commands != []:
      open(self.log_dirs['pre_install_cmd'], 'wb')
      for pre_install_command in self.pre_install_commands:
        command = "(cd {0} ; ".format(self.build_directory)
        command += dependency_command + ' '
        command += pre_install_command + ' '
        command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_install'])
        self.execute_command("python pre_install_commands", command + '\n',
                             logfilename=self.log_dirs['pre_install_cmd'],
                             logmode='a')

    command = "(cd {0} ; ".format(self.src_directory)
    command += dependency_command + ' ' + python_command
    command += " python setup.py install --prefix {0}".format(self.install_directory)
    command += ") > {0} 2>&1 ;".format(self.log_dirs['install'])
    self.execute_command("python install", command, logfilename=self.log_dirs['install_cmd'])

    # User Post Install
    if self.post_install_commands != []:
      open(self.log_dirs['post_install_cmd'], 'wb')
      for post_install_command in self.post_install_commands:
        command = "(cd {0} ; ".format(self.install_directory)
        command += dependency_command + ' '
        command += post_install_command + ' '
        command += ") >> {0} 2>&1 ;".format(self.log_dirs['post_install'])
        self.execute_command("python post_install_commands", command + '\n',
                             logfilename=self.log_dirs['post_install_cmd'],
                             logmode='a')

    self.install_ok()
    # Create file which contains sha1 of patches if archive mode
    if self.software_source_type == "archive":
      self.patches_sha1()
    self.command_log.print_end_work()

    return ""

  def execute_egg(self, dependency_command):

    self.log_dirs['install'] = os.path.join(self.yamm_src_log_dir, 'install.log')

    user_python_version = self.find_python_version(dependency_command)

    python_command = "export PYTHONPATH={0}/lib/python{1}/site-packages:"\
          "$PYTHONPATH ; ".format(self.install_directory, user_python_version)
    python_command += "mkdir -p {0}/lib/python{1}/site-packages ; "\
                          .format(self.install_directory, user_python_version)

    # Install
    self.command_log.print_begin_work("Installation", space=self.space)

    # User Pre Install
    if self.pre_install_commands != []:
      open(self.log_dirs['pre_install_cmd'], 'wb')
      for pre_install_command in self.pre_install_commands:
        command = "(cd {0} ; ".format(self.build_directory)
        command += dependency_command + ' '
        command += pre_install_command + ' '
        command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_install'])
        self.execute_command("egg pre_install_commands", command + '\n',
                             logfilename=self.log_dirs['pre_install_cmd'],
                             logmode='a')

    command = "(cd {0} ; ".format(self.src_directory)
    command += dependency_command + ' ' + python_command
    command += " sh {0} --prefix {1}".format(self.egg_file,
                                            self.install_directory)
    command += ") > {0} 2>&1 ;".format(self.log_dirs['install'])
    self.execute_command("egg install", command, logfilename=self.log_dirs['install_cmd'])

    # User Post Install
    if self.post_install_commands != []:
      open(self.log_dirs['post_install_cmd'], 'wb')
      for post_install_command in self.post_install_commands:
        command = "(cd {0} ; ".format(self.install_directory)
        command += dependency_command + ' '
        command += post_install_command + ' '
        command += ") >> {0} 2>&1 ;".format(self.log_dirs['post_install'])
        self.execute_command("egg post_install_commands", command + '\n',
                             logfilename=self.log_dirs['post_install_cmd'],
                             logmode='a')

    self.install_ok()
    self.command_log.print_end_work()
    return ""

  def execute_specific(self, dependency_command):

    self.log_dirs['install'] = os.path.join(self.yamm_src_log_dir, 'install.log')

    if self.disable_configure == "no":
      # Pre-configure for patches if archive mode
      if self.software_source_type == "archive" and self.patches != []:
        for patch in self.patches:
          self.pre_configure_commands += ["patch -p1 < " + patch]
      if self.pre_configure_commands != []:
        open(self.log_dirs['pre_configure_cmd'], 'wb')
        for pre_configure_command in self.pre_configure_commands:
          command = "( cd {0} ; ".format(self.src_directory)
          command += dependency_command + ' '
          command += pre_configure_command + ' '
          command += " ) >> {0} 2>&1 ;".format(self.log_dirs['pre_configure'])
          self.execute_command("pre_configure_commands", command + '\n',
                               logfilename=self.log_dirs['pre_configure_cmd'],
                               logmode='a')
      # Configure
      if self.specific_configure_command != "":
        self.command_log.print_begin_work("Configure", space=self.space)
        command = "( cd {0} ; ".format(self.src_directory)
        command += dependency_command + ' '
        command += self.specific_configure_command + ' '
        command += " ) > {0} 2>&1 ;".format(self.log_dirs['configure'])
        self.execute_command("configure", command, logfilename=self.log_dirs['configure_cmd'])

        self.command_log.print_end_work()

    # Make
    if self.specific_build_command != "":
      self.command_log.print_begin_work("Compilation", space=self.space)
      command = "( cd {0} ; ".format(self.src_directory)
      command += dependency_command + ' '
      command += self.specific_build_command + ' '
      command += ')'
      if not self.xterm_make :
        command += "> {0} 2>&1 ;".format(self.log_dirs['make'])
      else:
        command = 'xterm -e "{0}"'.format(command)

      try:
        self.execute_command("make", command, logfilename=self.log_dirs['make_cmd'])
      except TaskException as e:
        # Make Repeat
        if self.make_repeat == 0:
          raise e
        else:
          self.command_log.print_end_work("NOK (Warning: make failed, try to repeat)")
          if not self.xterm_make:
            error_file_name = os.path.join(self.build_directory,
                                           os.pardir,
                                           'make_error_{0}.log'.format(self.software_name_replaced))
            mv_command = "mv {0} {1}".format(self.log_dirs['make'], error_file_name)
            self.logger.info("Log file is in file %s" % error_file_name)
            self.execute_command("make when moving make log file", mv_command)
          self.command_log.print_begin_work("ReCompilation", space=self.space)
          self.execute_command("make", command, logfilename=self.log_dirs['make_cmd'])
      self.command_log.print_end_work()

    # Install
    self.command_log.print_begin_work("Installation", space=self.space)

    # Create install directory if not created
    try:
      if not os.path.exists(self.install_directory):
        if self.logger.verbose < VerboseLevels.DRY_RUN:
          os.makedirs(self.install_directory)
        else:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Command is: os.makedirs{0})".format(self.install_directory))
    except:
      self.logger.warning("Install directory cannot be created")

    # User Pre Install
    if self.pre_install_commands != []:
      open(self.log_dirs['pre_install_cmd'], 'wb')
      for pre_install_command in self.pre_install_commands:
        command = "( cd {0} ; ".format(self.build_directory)
        command += dependency_command + ' '
        command += pre_install_command + ' '
        command += " ) >> {0} 2>&1 ;".format(self.log_dirs['pre_install'])
        self.execute_command("pre_install_commands", command + '\n',
                             logfilename=self.log_dirs['pre_install_cmd'],
                             logmode='a')

    command = "( cd {0} ; ".format(self.src_directory)
    command += dependency_command + ' '
    command += self.specific_install_command + ' '
    command += ") > {0} 2>&1 ;".format(self.log_dirs['install'])
    self.execute_command("make install", command, logfilename=self.log_dirs['install_cmd'])

    # User Post Install
    if self.post_install_commands != []:
      open(self.log_dirs['post_install_cmd'], 'wb')
      for post_install_command in self.post_install_commands:
        command = "( cd {0} ; ".format(self.install_directory)
        command += dependency_command + ' '
        command += post_install_command + ' '
        command += " ) >> {0} 2>&1 ;".format(self.log_dirs['post_install'])
        self.execute_command("post_install_commands", command + '\n',
                             logfilename=self.log_dirs['post_install_cmd'],
                             logmode='a')

    self.install_ok()
    # Create file which contains sha1 of patches if archive mode
    if self.software_source_type == "archive":
      self.patches_sha1()
    self.command_log.print_end_work()
    return ""

  def execute_configure_with_space(self, dependency_command):

    if self.disable_configure == "no":

      if self.disable_configure_generation == "no":
        # Generates configure
        if self.gen_file != "":
          self.command_log.print_begin_work("Configure generation", space=self.space)
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command + ' '
          command += "./{0} ".format(self.gen_file)
          command += ") > {0} 2>&1 ;".format(self.log_dirs['generation'])
          self.execute_command("configure generation", command,
                               logfilename=self.log_dirs['generation_cmd'])
          self.command_log.print_end_work()

        elif self.gen_commands != []:
          self.command_log.print_begin_work("Configure generation", space=self.space)
          open(self.log_dirs['generation_cmd'], 'wb')
          for gen_command in self.gen_commands:
            command = "(cd {0} ;".format(self.src_directory)
            command += dependency_command + ' '
            command += gen_command + ' '
            command += ") >> {0} 2>&1 ;".format(self.log_dirs['generation'])
            self.execute_command("configure generation", command + '\n',
                                 logfilename=self.log_dirs['generation_cmd'],
                                 logmode='a')
          self.command_log.print_end_work()

      # Pre-configure for patches if archive mode
      if self.software_source_type == "archive":
        if self.patches != []:
            for patch in self.patches:
              self.pre_configure_commands += ["patch -p1 < " + patch]

      # Configure
      if self.pre_configure_commands != []:
        self.command_log.print_begin_work("Pre-configure generation", space=self.space)
        open(self.log_dirs['pre_configure_cmd'], 'wb')
        for pre_configure_command in self.pre_configure_commands:
          command = "(cd {0} ;".format(self.src_directory)
          command += dependency_command + ' '
          command += pre_configure_command + ' '
          command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_configure'])
          self.execute_command("pre_configure_commands", command + '\n',
                               logfilename=self.log_dirs['pre_configure_cmd'],
                               logmode='a')
        self.command_log.print_end_work()

      self.command_log.print_begin_work("Configure", space=self.space)
      command = "(cd {0} ;".format(self.build_directory)
      command += dependency_command + ' '
      command += "{0}/configure ".format(self.src_directory)
      command += "--prefix {0} ".format(self.install_directory)
      # Config options
      if self.config_options != "":
        command += self.config_options + ' '
      command += ") > {0} 2>&1 ;".format(self.log_dirs['configure'])
      self.execute_command("configure", command, logfilename=self.log_dirs['configure_cmd'])
      self.command_log.print_end_work()
    # Create file which contains sha1 of patches if archive mode
    if self.software_source_type == "archive":
      self.patches_sha1()
    return ""

  def execute_make(self, dependency_command):

    # Make
    self.command_log.print_begin_work("Compilation", space=self.space)
    command = "(cd {0} ; ".format(self.build_directory)
    command += dependency_command + ' '
    command += "make -j{0} {1}".format(self.parallel_make, self.make_target)
    command += ')'
    if not self.xterm_make :
      command += "> {0} 2>&1 ;".format(self.log_dirs['make'])
    else:
      command = 'xterm -e "{0}"'.format(command)

    try:
      self.execute_command("make", command, logfilename=self.log_dirs['make_cmd'])
    except TaskException as e:
      # Make Repeat
      if self.make_repeat == 0:
        raise e
      else:
        self.command_log.print_end_work(" NOK (Warning: make failed, try to repeat)")
        if not self.xterm_make:
          error_file_name = os.path.join(self.build_directory,
                                         os.pardir,
                                         'make_error_{0}.log'.format(self.software_name_replaced))
          mv_command = "mv {0} {1}".format(self.log_dirs['make'], error_file_name)
          self.logger.info("Log file is in file %s" % error_file_name)
          self.execute_command("make when moving make log file", mv_command)
        self.command_log.print_begin_work("ReCompilation", space=self.space)
        self.execute_command("make", command, logfilename=self.log_dirs['make_cmd'])
    self.command_log.print_end_work()

    # Install
    self.command_log.print_begin_work("Installation", space=self.space)

    # Create install directory if not created
    try:
      if not os.path.exists(self.install_directory):
        if self.logger.verbose < VerboseLevels.DRY_RUN:
          os.makedirs(self.install_directory)
        else:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Command is: os.makedirs{0})".format(self.install_directory))
    except:
      self.logger.warning("Install directory cannot be created")

    # User Pre Install
    if self.pre_install_commands != []:
      open(self.log_dirs['pre_install_cmd'], 'wb')
      for pre_install_command in self.pre_install_commands:
        command = "(cd {0} ; ".format(self.build_directory)
        command += dependency_command + ' '
        command += pre_install_command + ' '
        command += ") >> {0} 2>&1 ;".format(self.log_dirs['pre_install'])
        self.execute_command("pre_install_commands", command + '\n',
                             logfilename=self.log_dirs['pre_install_cmd'],
                             logmode='a')

    command = "(cd {0} ; ".format(self.build_directory)
    command += dependency_command + ' '
    if self.specific_install_command == "":
      if self.make_target == "":
        command += "make install "
    else:
      command += self.specific_install_command + ' '
    command += ")> {0} 2>&1 ;".format(self.log_dirs['install'])
    self.execute_command("make install", command, logfilename=self.log_dirs['install_cmd'])

    # User Post Install
    if self.post_install_commands != []:
      open(self.log_dirs['post_install_cmd'], 'wb')
      for post_install_command in self.post_install_commands:
        command = "(cd {0} ; ".format(self.install_directory)
        command += dependency_command + ' '
        command += post_install_command + ' '
        command += ")>> {0} 2>&1 ;".format(self.log_dirs['post_install'])
        self.execute_command("post_install_commands", command + '\n',
                             logfilename=self.log_dirs['post_install_cmd'],
                             logmode='a')

    self.install_ok()
    self.command_log.print_end_work()
    return ""

  def execute_rsync(self, dependency_command):
    # Create install directory if not created
    try:
      if not os.path.exists(self.install_directory):
        if self.logger.verbose < VerboseLevels.DRY_RUN:
          os.makedirs(self.install_directory, 0o755)
        else:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Command is: os.makedirs{0})".format(self.install_directory))
    except:
      self.logger.warning("Install directory cannot be created")

    self.command_log.print_begin_work("Rsync", space=self.space)
    command = dependency_command + ' '
    command += "(cd {0} ;".format(self.src_directory)
    command += "rsync -avz --exclude '.svn' --exclude '.git' --exclude 'CVS' --exclude '.hg' --exclude '.yamm' --exclude .gitignore "
    # Config options
    if self.config_options != "":
      command += self.config_options + ' '
    # The following line ensures that rsync will synchronize the good directory
    rsync_src = os.path.join(self.src_directory, '')
    install_directory = self.install_directory
    if self.make_target != "":
      # Every target in make_target is prefixed by src_directory and added to rsync
      targets = self.make_target.split()
      target_prefixed = [os.path.join(self.src_directory, target) for target in targets]
      rsync_src = ' '.join(target_prefixed)
      install_directory = os.path.join(self.install_directory, '')
    command += "{0} {1} ".format(rsync_src, install_directory)
    command += ') > {0} 2>&1 ;'.format(self.log_dirs['rsync'])
    self.execute_command("rsync", command, logfilename=self.log_dirs['rsync_cmd'])
    self.command_log.print_end_work()

    # User Post Install
    if self.post_install_commands != []:
      self.command_log.print_begin_work("Rsync post install", space=self.space)
      open(self.log_dirs['post_install_cmd'], 'wb')
      for post_install_command in self.post_install_commands:
        command = "(cd {0} ; ".format(self.install_directory)
        command += dependency_command + ' '
        command += post_install_command + ' '
        command += ") >> {0} 2>&1 ;".format(self.log_dirs['post_install'])
        self.execute_command("rsync post_install_commands", command + '\n',
                             logfilename=self.log_dirs['post_install_cmd'],
                             logmode='a')
      self.command_log.print_end_work()

    self.install_ok()
    self.command_log.print_end_work()
    return ""

  def execute_fake(self, dependency_command):
    self.command_log.print_begin_work("Installation", space=self.space)

    # Install ok
    if not os.path.exists(self.install_directory):
      try:
        os.makedirs(self.install_directory)
      except:
        raise TaskException("Error: Cannot create directory {0}".format(self.install_directory), True)

    self.install_ok()
    self.command_log.print_end_work()
    return ""

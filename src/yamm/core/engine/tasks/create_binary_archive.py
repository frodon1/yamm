# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import shutil
import tarfile
import tempfile

from yamm.core.base.misc import VerboseLevels
from yamm.core.base.misc import fast_delete
from yamm.core.engine.tasks.generic_task import GenericTask, TaskException, touch, create_sha1


class CreateBinaryArchiveTask(GenericTask):
    """
      This task is called when a binary archive of a software is built.

      :param list make_movable_archive_commands:  Defines the list of commands to execute
                                                  to ensure that the software install binaries
                                                  are portable.
      :param string verbose: Defines the verbose level in YAMM
    """

    rm_files_command = ''
    for expr in ['CVS', '*.svn', '.yamm']:
      rm_files_command += 'find . -name "' + expr + '" -exec rm -rf "{}" + ;'

    def __init__(self,
                 binary_archive_topdir,
                 binary_archive_name,
                 make_movable_archive_commands=None,
                 verbose_level=VerboseLevels.INFO
                 ):
      GenericTask.__init__(self, '[CREATE BINARY ARCHIVE TASK]', verbose_level)
      self.binary_archive_topdir = binary_archive_topdir
      binary_archive_base = os.path.join(self.binary_archive_topdir, binary_archive_name)
      self.binary_archive_path = binary_archive_base
      if (not binary_archive_name.endswith(".tgz")) and (not binary_archive_name.endswith(".tar.gz")):
        self.binary_archive_path += ".tgz"
      self.binary_archive_sha1_path = self.binary_archive_path + ".sha1"
      self.set_commands(make_movable_archive_commands)

    def set_commands(self, commands=None):
      self.make_movable_archive_commands = commands or []
      self.make_movable_archive_commands.append(CreateBinaryArchiveTask.rm_files_command)

    def prepare_work(self, software_config, space, command_log):

      self.command_log = command_log
      self.space = space
      self.command_log.print_begin_work("Prepare step", space=self.space)

      # Get values from config
      self.install_directory = software_config.install_directory
      self.src_directory = software_config.src_directory

      # Check if binary archive top directory exists
      if self.binary_archive_topdir is None:
        raise TaskException('Cannot create binary archive because the option '
                            '"binary_archive_topdir" is not defined')

      if not os.path.exists(self.binary_archive_topdir):
        if self.logger.verbose < VerboseLevels.DRY_RUN:
          os.makedirs(self.binary_archive_topdir, 0o755)
        else:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Command is: os.makedirs{0}, 0755)".format(self.binary_archive_topdir))

      # Paths for check files
      self.binarch_ok_path = os.path.join(self.install_directory, '.yamm', ".binary_archive_ok_" + self.software_name_replaced)
      self.install_ok_path = os.path.join(self.install_directory, '.yamm', ".install_ok_" + self.software_name_replaced)
      if not os.path.exists(self.install_ok_path):
        self.install_ok_path = os.path.join(self.install_directory, '.yamm', ".install_binarch_ok_" + self.software_name_replaced)

      self.command_log.print_end_work()

    def execute(self):
      if self.check_binary_archive_already_done():
        self.command_log.print_info("Software skipped (Binary archive already done)", space=self.space)
        return
      self.command_log.print_begin_work("Create binary archive", space=self.space)
      if self.logger.verbose < VerboseLevels.DRY_RUN:
        tmprootdir = tempfile.mkdtemp()
        tmpdir = os.path.join(tmprootdir, "binarch")
        shutil.copytree(self.install_directory, tmpdir, symlinks=True)
        curdir = os.getcwd()
        os.chdir(tmpdir)
        try:
          for i, command in enumerate(self.make_movable_archive_commands):
            self.execute_command("Make movable archive command #%d" % (i + 1), command)
          os.chdir(curdir)
          self.command_log.print_debug("Creating TGZ...")
          tar = tarfile.open(self.binary_archive_path, "w:gz")
          tar.errorlevel = 1
          tar.add(tmpdir, os.path.basename(self.install_directory))
          tar.close()
        except TaskException as e:
          raise e
        except:
          raise TaskException("Cannot create archive %s." % self.binary_archive_path, True)
        finally:
          fast_delete(tmprootdir)

        # SHA1 file
        create_sha1(self.binary_archive_path, self.binary_archive_sha1_path)

        # Binary archive ok
        touch(self.binarch_ok_path)
      else:
        self.command_log.print_debug("=== SIMULATION MODE ===")
      self.command_log.print_end_work()

    def check_binary_archive_already_done(self):
      self.command_log.print_debug("Check existence of binary archive file: " + self.binary_archive_path)
      if not os.path.isfile(self.binary_archive_path):
        return False
      self.command_log.print_debug("Check existence of binary archive SHA1 file: " + self.binary_archive_sha1_path)
      if not os.path.isfile(self.binary_archive_sha1_path):
        return False
      self.command_log.print_debug("Check existence of binary archive OK file: " + self.binarch_ok_path)
      if not os.path.isfile(self.binarch_ok_path):
        return False
      self.command_log.print_debug("Check if binary archive OK file is newer than install OK file")
      if os.path.getmtime(self.binarch_ok_path) < os.path.getmtime(self.install_ok_path):
        return False
      return True

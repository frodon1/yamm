#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import tarfile
import tempfile

from yamm.core.base.misc import VerboseLevels
from yamm.core.base.misc import fast_delete
from yamm.core.engine.tasks.generic_task import GenericTask


class DecompressTask(GenericTask):

  # Class attributes
  modes_allowed = ["decompress", "nothing"]
  archive_supported_type = ["tar.gz", "tar.bz2", "tar.xz"]
  src_dir_options = ["delete", "keep"]
  sha1_tools_allowed = ["sha1sum", "hashlib"]

  def __init__(self,
               archive_type,
               src_dir="delete",
               archive_dir_name="",
               mode="decompress",
               sha1_tool="hashlib",
               verbose_level=VerboseLevels.INFO):
    """ DecompressTask constructor
    If you want to delete the source directory
    you have to provide archive_dir_name
    """
    GenericTask.__init__(self, '[DECOMPRESS TASK]', verbose_level)
    self.archive_type = archive_type
    self.src_dir = src_dir
    self.archive_dir_name = archive_dir_name
    self.mode = mode
    self.sha1_tool = sha1_tool
    self.check_args()

  def check_args(self):

    if self.mode not in DecompressTask.modes_allowed:
      self.logger.error("mode is unknown")
    if self.archive_type not in self.archive_supported_type:
      self.logger.error("archive_type %s not supported" % self.archive_type)
    if self.src_dir not in self.src_dir_options:
      self.logger.error("src_dir %s not supported" % self.src_dir)
    if self.sha1_tool not in DecompressTask.sha1_tools_allowed:
      self.logger.error("sha1 tool is unknown")

  def prepare_work(self, software_config, space, command_log):

    self.command_log = command_log
    self.space = space
    self.command_log.print_begin_work("Prepare step", space=self.space)

    # Get values from config
    self.archive_file_name = software_config.user_full_archive_file_name
    self.decompress_directory = software_config.src_topdir
    self.archive_directory = software_config.archive_dir
    self.archive_file_name_sum = software_config.archive_file_name + ".sha1"

    if not os.path.exists(self.decompress_directory):
      if self.logger.verbose < VerboseLevels.DRY_RUN:
        os.makedirs(self.decompress_directory, 0o755)
      else:
        self.command_log.print_debug("=== SIMULATION MODE ===")
        self.command_log.print_debug("Command is: os.makedirs({0}, 0755)".format(self.decompress_directory))

    # Delete source directory if needed
    if self.src_dir == "delete" and self.archive_dir_name != "":
      # Check if directory exists
      full_src_directory = os.path.join(self.decompress_directory, self.archive_dir_name)
      self.command_log.print_debug("Deleting source directory: %s" % full_src_directory)
      if os.path.isdir(full_src_directory):
        try:
          if self.logger.verbose < VerboseLevels.DRY_RUN:
            fast_delete(full_src_directory)
          else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug("Command is: misc.fast_delete{0})".format(full_src_directory))
        except:
          self.logger.exception("in deleting directory %s" % full_src_directory)
      elif os.path.isfile(full_src_directory):
        try:
          if self.logger.verbose < VerboseLevels.DRY_RUN:
            os.remove(full_src_directory)
          else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug("Command is: os.remove{0})".format(full_src_directory))
        except:
          self.logger.exception("in deleting file %s" % full_src_directory)

    self.command_log.print_end_work()
    return ""

  def execute(self):

    if self.mode == "nothing":
      return ""

    self.command_log.print_begin_work("Decompression", space=self.space)
    if self.archive_type == "tar.xz":
      command = "cd " + self.archive_directory + " ; "
      command += "xz -d -f " + self.archive_file_name
      self.execute_command("Decompression", command)
      self.archive_file_name = self.archive_file_name[:-3]

    if self.logger.verbose < VerboseLevels.DRY_RUN:
      # Create a temporary directory to extract the archive
      tmpdir = tempfile.mkdtemp(prefix=self.archive_dir_name, dir=self.decompress_directory)

      # Extract the archive in the temporary directory
      mode = 'r|*'
      try:
        tar_file = tarfile.open(name=self.archive_file_name, mode=mode)
        tar_file.extractall(tmpdir)
        tar_file.close()
      except:
        self.logger.exception("Cannot decompress file %s" % self.archive_file_name)
    else:
      self.command_log.print_debug("=== SIMULATION MODE ===")
      self.command_log.print_debug("Decompress file {0})".format(self.archive_file_name))

    if self.archive_type == "tar.xz":
      # Delete tar file
      tarfilename = self.archive_file_name
      self.command_log.print_debug("Deleting tar file %s" % tarfilename)

      # Test if file exist
      if not os.path.isfile(tarfilename):
        self.command_log.print_warning("Cannot find tar file to delete: %s" % tarfilename)
        self.command_log.print_end_work()
        return ""

      # Delete file
      try:
        if self.logger.verbose < VerboseLevels.DRY_RUN:
          os.remove(tarfilename)
        else:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Command is: os.remove{0})".format(tarfilename))
      except:
        self.logger.exception("Cannot delete tar file %s" % tarfilename)

    if self.logger.verbose < VerboseLevels.DRY_RUN:
      full_src_directory = os.path.join(self.decompress_directory, self.archive_dir_name)
      # Check the extracted content
      is_single_dir = False
      extracted_content = os.listdir(tmpdir)
      if len(extracted_content) == 1:
        content_path = os.path.join(tmpdir, extracted_content[0])
        if os.path.isdir(content_path):
          # If the extracted content is a single directory, we consider it's the software root dir
          is_single_dir = True
          os.rename(content_path, full_src_directory)
          os.rmdir(tmpdir)

      if not is_single_dir:
        # If the extracted content is not a single directory,
        # we consider that the temporary extraction directory is the software root dir
        os.rename(tmpdir, full_src_directory)
      # Ensure src directory is readable by all
      os.chmod(full_src_directory, 0o755)
    else:
      self.command_log.print_debug("=== SIMULATION MODE ===")
      self.command_log.print_debug("Check extracted content")

    # Execution ok
    self.command_log.print_end_work()
    return ""

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010, 2016 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import object
import subprocess

from yamm.core.base.misc import Log, VerboseLevels, TaskException
from yamm.core.base.misc import touch, create_sha1, copy  # @UnusedImport


class GenericTask(object):
    def __init__(self, taskname='[GENERIC TASK]', verbose_level=VerboseLevels.INFO):
        self.logger = Log(taskname, verbose_level)
        self.shell = None
        self.command_log = None

    def set_software_name(self, software_name):
        self.software_name = software_name
        self.software_name_replaced = self.software_name.replace(" ", "_")

    def execute_command(self, name, command, no_dry_run=False, logfilename=None, logmode='w'):
        try:
            ier = 0
            if self.shell is None:
                if self.command_log:
                    self.command_log.print_debug("Command is: {0}".format(command))
                if no_dry_run or self.logger.verbose < VerboseLevels.DRY_RUN:
                    if logfilename:
                        with open(logfilename, logmode) as logfile:
                            logfile.write(command)
                    ier = subprocess.call(command, shell=True)
                else:
                    if self.command_log:
                        self.command_log.print_debug("=== SIMULATION MODE ===")
                        self.command_log.print_debug("Command is: {0}".format(command))
                        self.command_log.print_debug("=======================")
            else:
                if self.command_log:
                    self.command_log.print_debug("Command is: {0} -c \"{1}\"".format(self.shell, command))
                if no_dry_run or self.logger.verbose < VerboseLevels.DRY_RUN:
                    if logfilename:
                        with open(logfilename, logmode) as logfile:
                            logfile.write(command)
                    ier = subprocess.call([self.shell, "-l", "-c", command])
                else:
                    if self.command_log:
                        self.command_log.print_debug("=== SIMULATION MODE ===")
                        self.command_log.print_debug("Command is: {0}".format(command))
                        self.command_log.print_debug("=======================")
            if ier != 0:
                raise TaskException("Error in %s, command was: %s" % (name, command))
        except OSError:
            raise TaskException("Error in %s, command was: %s" % (name, command), True)

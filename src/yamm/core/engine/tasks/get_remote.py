#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_bin_dir = os.path.join(*(yamm_default_dir + ('bin',)))
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from future import standard_library
standard_library.install_aliases()
from builtins import str
import urllib

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.engine.tasks.generic_task import GenericTask


class GetRemoteTask(GenericTask):

    modes_allowed = ["force_update", "update", "nothing"]
    tools_allowed = ["wget", "curl", "urllib"]
    sha1_tools_allowed = ["sha1sum", "hashlib"]

    def __init__(self,
                 remote_address,
                 tool="urllib",
                 mode="update",
                 verbose=0,
                 sha1_tool="hashlib",
                 verbose_level=VerboseLevels.INFO):

        GenericTask.__init__(self, '[GET REMOTE TASK]', verbose_level)
        assert (remote_address)
        self.remote_address = remote_address
        if not self.remote_address[-1] == os.sep:
            self.remote_address += os.sep
        self.mode = mode
        self.tool = tool
        self.sha1_tool = sha1_tool
        self.sha1_value = None
        self.check_args()

    def check_args(self):

        if self.mode not in GetRemoteTask.modes_allowed:
            self.logger.error("mode is unknown")
        if self.tool not in GetRemoteTask.tools_allowed:
            self.logger.error("tool is unknown")
        if self.sha1_tool not in GetRemoteTask.sha1_tools_allowed:
            self.logger.error("sha1 tool is unknown")
        if self.remote_address == "":
            self.logger.error("remote_address is mandatory cannot be empty")

    def prepare_work(self, software_config, space, command_log):

        self.command_log = command_log
        self.space = space
        if self.command_log:
            self.command_log.print_begin_work("Prepare step", space=self.space)

        # Get values from config
        self.archive_file_name = software_config.archive_file_name
        self.archive_directory = software_config.archive_dir

        # Check if archive directory exists
        if not os.path.exists(self.archive_directory):
            if self.logger.verbose < VerboseLevels.DRY_RUN:
                os.makedirs(self.archive_directory, 0o755)
            else:
                if self.command_log:
                    self.command_log.print_debug("=== SIMULATION MODE ===")
                    self.command_log.print_debug("Command is: os.makedirs{0}, 0755)".format(self.archive_directory))

        if self.command_log:
            self.command_log.print_end_work()
        return ""

    def execute(self):

        if self.mode == "nothing":
            return ""

        # Step 1: Download archive
        self.command_log.print_begin_work("Downloading", space=self.space)
        # Check if archive already downloaded
        archive_file_path = os.path.join(self.archive_directory,
                                         self.archive_file_name)
        if (self.mode == "update") and os.path.exists(archive_file_path):
            self.command_log.print_end_work("(Already Downloaded)")
        else:
            msg = self.download(self.archive_file_name)
            if msg == "":
                self.command_log.print_end_work()
            else:
                return "Getting software failed: %s" % msg

        self.command_log.print_begin_work("Computing sha1 of local archive",
                                          space=self.space)
        msg, self.sha1_value = misc.compute_sha1_archive(self.archive_directory,
                                                         self.archive_file_name,
                                                         self.sha1_tool)
        # self.command_log.print_debug('Sha1 of local archive {0} is {1}'\
        #                          .format(self.archive_file_name, self.sha1_value))

        if msg:
            return "Computing sha1 of local archive failed"  # %s" % msg
        self.command_log.print_end_work('({0})'.format(self.sha1_value))

        # Step 2: Try to get sha1 sum
        archive_file_name_sum = self.archive_file_name + ".sha1"
        self.command_log.print_begin_work("Checking file integrity",
                                          space=self.space)
        # Check if archive already downloaded
        msg = self.download(archive_file_name_sum)
        if msg != "":
            self.command_log.print_end_work_with_warning("(Check integrity skipped: sha1 cannot be downloaded - %s)" % msg)
            return ""

        # Step 3: Check download
        msg = misc.check_sha1_sum(self.archive_directory, self.archive_file_name,
                                  archive_file_name_sum, self.sha1_tool,
                                  self.sha1_value)
        if msg != "":
            return msg
        self.command_log.print_end_work()
        return ""

    def download(self, file_name):
        if self.tool == "urllib":
            try:
                fullurl = urllib.parse.urljoin(self.remote_address, file_name)
                urlsplit = urllib.parse.urlsplit(fullurl)
                if not urlsplit.scheme:
                    fullurl = 'file://%s' % fullurl
                    urlsplit = urllib.parse.urlsplit(fullurl)
                # RB: For the time being (20/11/2013), it seems that FTP protocol support is
                # bad with urllib and urllib2 with Python 2.7.
                # More precisely, "no_proxy" environment variable is not taken into account,
                # and for an unknown reason calls to close() on objects opened with urllib2.urlopen()
                # on FTP hang for a VERY long time in EDF network.
                if urlsplit.scheme == "ftp":
                    if self.command_log:
                        self.command_log.print_warning("URL %s requires FTP protocol which is badly supported. "
                                                       "Consider using HTTP instead" % fullurl)
                # We use urllib2.urlopen instead of urllib.urlretrieve because urllib.urlretrieve
                # does not raise an error if the HTTP status code is 404 for instance
                if self.command_log:
                    self.command_log.print_debug("Command is: remotefile = urllib.request.urlopen('%s')" % fullurl)
                if self.logger.verbose < VerboseLevels.DRY_RUN:
                    misc.config_urllib2_netrc(self.remote_address, self.command_log)

                    try:
                        remotefile = urllib.request.urlopen(fullurl)
                    except urllib.error.HTTPError as e:
                        return str(e)
                    # remotefile must not be None
                    # We check the content-location because sha1 file can be downloaded
                    # instead of original file
                    contentlocation = remotefile.info().get('content-location',
                                                            os.path.basename(fullurl))
                    if remotefile is None or contentlocation != os.path.basename(fullurl):
                        return 'Impossible to download %s' % fullurl

                    with open(os.path.join(self.archive_directory, file_name), "w") as localfile:
                        buf = remotefile.read(65536)
                        while buf != "":
                            localfile.write(buf)
                            buf = remotefile.read(65536)
                    remotefile.close()
                else:
                    if self.command_log:
                        self.command_log.print_debug("=== SIMULATION MODE ===")
                        self.command_log.print_debug("Command is: urllib2.urlopen({0})".format(fullurl))
            except Exception as exc:
                return str(exc)
        else:
            command = "cd " + self.archive_directory + " ; "
            if self.tool == "wget":
                command += 'wget -e "netrc=on" -nv ' + os.path.join(self.remote_address, file_name)
            if self.tool == "curl":
                command += "curl -n -o " + file_name + " " + os.path.join(self.remote_address, file_name)
            command += " > " + file_name + ".log 2>&1 ;"
            self.execute_command("Get remote file", command)
        return ""

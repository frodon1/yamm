#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from future import standard_library
standard_library.install_aliases()
import os
import tarfile
import urllib.request, urllib.error, urllib.parse

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.engine.tasks.generic_task import GenericTask, TaskException


class InstallBinaryArchiveTask(GenericTask):
    """
      This task is called when a software is installed from a binary archive built by the task
      :py:class:`~yamm.core.engine.tasks.create_binary_archive.CreateBinaryArchiveTask`.

      :param list unmake_movable_archive_commands:  Defines the list of argument which will be used
                                                    to generate the commands to run during the
                                                    installation process. Each argument is a tuple of the form:
                                                    (template_string, software, filename)
      :param boolean runPostInstallCommands:  if True, the post install commands are generated and executed; if
                                              False, no post install command is run.
      :param string verbose: Defines the verbose level in YAMM
    """

    def __init__(self,
                 binary_archive_url,
                 unmake_movable_archive_commands=None,
                 runPostInstallCommands=True,
                 verbose_level=VerboseLevels.INFO
                 ):
      GenericTask.__init__(self, '[INSTALL BINARY ARCHIVE TASK]', verbose_level)
      self.binary_archive_url = binary_archive_url
      self.unmake_movable_archive_commands = unmake_movable_archive_commands or []
      self.runPostInstallCommands = runPostInstallCommands
      self.delete_install_dir = True

    def setRunPostInstallCommands(self, runPostInstallCommands):
      self.runPostInstallCommands = runPostInstallCommands

    def prepare_work(self, software_config, space, command_log):

      self.command_log = command_log
      self.space = space
      self.command_log.print_begin_work("Prepare step", space=self.space)
      # Get values from config
      self.install_directory = software_config.install_directory

      if self.binary_archive_url == "":
        raise TaskException("binary archive URL is not specified")

      self.command_log.print_end_work()

    def execute(self, install_binarch_ok_path, binary_archive_sha1):
        self.command_log.print_info("Binary archive URL: %s" % self.binary_archive_url, space=self.space)
        self.command_log.print_begin_work("Install binary archive", space=self.space)

        if self.logger.verbose < VerboseLevels.DRY_RUN:
          hostname = misc.config_urllib2_netrc(self.binary_archive_url, self.command_log)
          if not hostname:
            self.binary_archive_url = 'file://%s' % self.binary_archive_url
          tar = None
          remoteFile = None
          try:
            remoteFile = urllib.request.urlopen(self.binary_archive_url)
            tar = tarfile.open(mode="r|gz", fileobj=remoteFile)
          except:
            if tar:
                tar.close()
            if remoteFile:
                remoteFile.close()
            raise TaskException("Cannot open tar.gz file at URL {0}.".format(self.binary_archive_url), True)
  
          if self.delete_install_dir and os.path.isdir(self.install_directory):
            misc.fast_delete(self.install_directory)

          tar.errorlevel = 1
          try:
              for tarinfo in tar:
                  pos = tarinfo.name.find("/")
                  if pos != -1:
                    tarinfo.name = tarinfo.name[pos + 1:]
                    tar.extract(tarinfo, self.install_directory)
          except:
            raise TaskException("Cannot extract tar.gz file to directory {0}.".format(self.install_directory), True)
  
          tar.close()
          remoteFile.close()

          if self.runPostInstallCommands and self.unmake_movable_archive_commands:
            for command_args in self.unmake_movable_archive_commands:
              template_string, software, filename = command_args
              dirname = os.path.dirname(filename)
              template = os.path.join(dirname, '.%s.template' % os.path.basename(filename))
              current = os.path.join(dirname, '.%s.current' % os.path.basename(filename))

              command = "cd {0}; ".format(self.install_directory)
              command += "if [ ! -e {0} ]; then cp {1} {0}; chmod +w {0} ; fi ; ".format(template, filename)
              command += "if [ ! -e {0} ]; then cp {1} {0}; chmod +w {0} ; fi ; ".format(current, template)
              if software and template_string:
                sed_pattern = misc.sedSep() + template_string + misc.sedSep() + software.install_directory + misc.sedSep()
                command += "sed -i -e 's{0}g' {1}; ".format(sed_pattern, current)
              command += "cp {0} {1}; ".format(current, filename)
              command += "chmod +x {0}".format(filename)
              self.execute_command("Post Install Command", command)

          # Binary archive OK file
          if binary_archive_sha1 is not None:
            yamm_binarch_ok_dir = os.path.dirname(install_binarch_ok_path)
            if not os.path.isdir(yamm_binarch_ok_dir):
              try:
                os.makedirs(yamm_binarch_ok_dir)
              except:
                raise TaskException("Error: Cannot create directory {0}".format(yamm_binarch_ok_dir), True)
            try:
              with open(install_binarch_ok_path, "w") as f:
                f.write(binary_archive_sha1)
            except:
              raise TaskException("Error: Cannot create binary archive installation check file {0}".\
                                  format(install_binarch_ok_path), True)
        else:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Download and install binary archive")
        self.command_log.print_end_work()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.core.base.misc import VerboseLevels
from yamm.core.engine.tasks.generic_task import GenericTask
import os


class PostInstallConfigTask(GenericTask):
  """
    This task is called when a software post install commands are called by the task
    :py:class:`~yamm.core.engine.tasks.post_install_config.PostInstallConfigTask`.

    :param list post_install_commands:  Defines the list of argument which will be used
                                                  to generate the commands to run during the
                                                  configuration process. Each argument is a tuple of the form:
                                                  (template_string, software, filename)
    :param string shell: Specify the shell to use to run the task command
    :param string verbose_level: Defines the verbose level in YAMM
  """

  def __init__(self,
                post_install_commands=None,
                shell=None,
                verbose_level=VerboseLevels.INFO
                ):
    GenericTask.__init__(self, '[INSTALL BINARY ARCHIVE TASK]', verbose_level)
    if post_install_commands is None:
      post_install_commands = []
    self.post_install_commands = post_install_commands
    if self.post_install_commands is None:
      self.post_install_commands = []
    self.shell = shell

  def prepare_work(self, software_config, space, command_log):

    self.command_log = command_log
    self.space = space
    self.command_log.print_begin_work("Prepare step", space=self.space)
    # Get values from config
    self.install_directory = software_config.install_directory
    self.yamm_log_dir = os.path.join(self.install_directory, '.yamm')
    self.post_install_config_log = os.path.join(self.yamm_log_dir, "post_install_config.log")

    self.command_log.print_end_work()
    return ""

  def execute(self, dependency_command):
      self.command_log.print_begin_work("Post install configuration", space=self.space)
      if self.post_install_commands != []:
        self.post_install_commands.insert(0, "mkdir -p {0} ".format(self.yamm_log_dir))
        for post_install_command in self.post_install_commands:
          command = "( cd {0} ;".format(self.install_directory)
          command += dependency_command + ' '
          command += post_install_command + ' '
          command += " ) >> {0} 2>&1 ;".format(self.post_install_config_log)
          self.execute_command("post_install_config", command)

      self.command_log.print_end_work()
      return ""


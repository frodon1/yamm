#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2010 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import with_statement

from builtins import str


import os
import re
import shutil
import subprocess

from yamm.core.base.misc import TaskException
from yamm.core.base.misc import VerboseLevels
from yamm.core.base.misc import fast_delete
from yamm.core.engine.tasks.generic_task import GenericTask


class RemoteTask(GenericTask):

  # Options
  remote_system_type = ["cvs", "svn", "git", "hg"]
  src_dir_policy_options = ["delete", "keep"]

  def __init__(self,
               use_compat_env,
               remote_type,
               remote_env,
               remote_config,
               remote_options,
               repository_name,
               root_repository,
               tag="",
               sha1_version="",
               src_dir_policy="keep",
               verbose_level=VerboseLevels.INFO
               ):
    GenericTask.__init__(self, '[REMOTE TASK]', verbose_level)
    self.use_compat_env = use_compat_env
    self.remote_type = remote_type
    self.remote_env = remote_env.strip() + " "
    self.remote_config = remote_config
    self.remote_options = remote_options
    self.repository_name = repository_name.strip()
    self.root_repository = root_repository
    self.tag = tag
    self.sha1_version = sha1_version
    self.last_commit = None
    self.src_dir_policy = src_dir_policy
    self.diff = 1
    self.istrunk = self.tag in ("", "trunk", "master", "default")
    self.install_ok_path = ''
    self.install_ok = ""
    self.check_args()

  def check_args(self):

    if self.remote_type not in self.remote_system_type:
      self.logger.error("remote_type %s not supported" % self.remote_type)
    if self.src_dir_policy not in self.src_dir_policy_options:
      self.logger.error("src_dir_policy option %s not supported" % self.src_dir_policy)

  def prepare_work(self, software_config, space, command_log):

    self.command_log = command_log
    self.space = space
    self.command_log.print_begin_work("Prepare step", space=self.space)

    self.src_directory = software_config.src_directory
    self.base_directory = os.path.realpath(os.path.join(self.src_directory,
                                                        os.pardir))
    self.extract_directory_name = os.path.basename(self.src_directory)
    self.yamm_src_log_dir = software_config.yamm_src_log_dir

    self.log_dirs = software_config.log_dirs.copy()

    if software_config is not None:
      self.install_ok_path = os.path.join(software_config.yamm_install_log_dir,
                                          self.install_ok)


    # Check if src_directory has to deleted
    if os.path.exists(self.src_directory) and self.src_dir_policy == "delete":
      try:
        if self.logger.verbose < VerboseLevels.DRY_RUN:
          fast_delete(self.src_directory)
        else:
          self.command_log.print_debug("=== SIMULATION MODE ===")
          self.command_log.print_debug("Command is: misc.fast_delete({0})".format(self.src_directory))
      except:
        self.logger.exception("Error in deleting directory %s" % self.src_directory)

    # Check if src_directory exists
    if not os.path.exists(self.src_directory):
      self.command_log.print_debug("Creating src path: %s" % self.src_directory)
      if self.logger.verbose < VerboseLevels.DRY_RUN:
        os.makedirs(self.src_directory, 0o755)
      else:
        self.command_log.print_debug("=== SIMULATION MODE ===")
        self.command_log.print_debug("Command is: os.makedirs({0}, 0755)".format(self.src_directory))

    self.command_log.print_end_work()
    return ""

  def set_software_name(self, software_name):
    GenericTask.set_software_name(self, software_name)
    self.install_ok = ".install_ok_" + self.software_name_replaced

  def delete_install_check_file(self):
    # Since we are going to compile we need to delete install file if it exists
    if self.install_ok_path and os.path.exists(self.install_ok_path):
      if os.path.isfile(self.install_ok_path):
        # Try to delete the file
        try:
          if self.logger.verbose < VerboseLevels.DRY_RUN:
            os.remove(self.install_ok_path)
          else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug("Command is: os.remove({0})".format(self.install_ok_path))
        except:
          self.logger.exception("Error in deleting file %s", self.install_ok_path)
      else:
        # This should not happen !
        self.logger.error("Install test file is not a file !: %s", self.install_ok_path)

  def get_current_remote_address(self):
    """ Returns the address of the remote
    """
    remote_address = None
    if self.remote_type == "svn":
      command = "cd " + self.src_directory + "; "
      command += self.remote_env
      command += self.get_svn_command("info | grep ^Racine | awk '{print $4}'")
      p = subprocess.Popen(command,
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
      p.wait()
      remote_address = p.stdout.read().strip()
    elif self.remote_type == "git":
      command = "cd " + self.src_directory + "; "
      command += self.remote_env
      command += self.get_git_command("remote -v |grep ^origin|grep fetch| awk '{print $2}'")
      p = subprocess.Popen(command,
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
      p.wait()
      remote_address = p.stdout.read().strip()
    elif self.remote_type == "hg":
      command = "cd " + self.src_directory + "; "
      command += self.remote_env
      #command += self.get_hg_command("config paths.default")
      command += self.get_hg_command("showconfig paths.default")
      p = subprocess.Popen(command,
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
      p.wait()
      remote_address = p.stdout.read().strip()
    return remote_address

  def get_last_commit_id(self, src_directory=None):
    # Record the last commit id
    last_commit = None
    if src_directory is None:
      src_directory = self.src_directory
    if self.remote_type == "svn":
      command = "cd " + src_directory + "; "
      command += self.remote_env
      command += self.get_compat_env_command()
      command += 'svnversion'
      p = subprocess.Popen(command,
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           universal_newlines=True)
      p.wait()
      m = re.match(r'(|\d+M?S?):?(\d+)(M?)S?', p.stdout.read())
      rev = int(m.group(2))
      if m.group(3) == 'M':
        rev += 1
      last_commit = str(rev)
      #
    elif self.remote_type == "git":
      # A '*' is added in case of non commited modifications
      command = "cd " + src_directory + "; "
      command += self.remote_env
      command += self.get_git_command('rev-parse HEAD')
      p = subprocess.Popen(command,
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           universal_newlines=True)
      p.wait()
      last_commit = p.stdout.read().strip()
      # check non commited modifications
      command = 'cd ' + src_directory + '; '
      command += self.remote_env
      command += self.get_git_command('status --untracked-files=no --porcelain')
      p = subprocess.Popen(command,
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           universal_newlines=True)
      p.wait()
      local_diffs = p.stdout.read().strip()
      if local_diffs:
        last_commit += '*'
      #
    return last_commit

  def get_compat_env_command(self, cmd="", compat_prefix=""):
      command = ''
      if self.use_compat_env:
          command += "compat-calibre%s " % self.use_compat_env
          if compat_prefix:
              command += compat_prefix
      if cmd:
          command += cmd
      if not command.endswith(' '):
          command += ' '
      return command

  def get_svn_command(self, command=''):
    svn_command = self.get_compat_env_command("svn", compat_prefix='/usr/bin/')
    if command:
      svn_command += command
    if not svn_command.endswith(' '):
      svn_command += ' '
    return svn_command

  def get_hg_command(self, command=''):
    hg_command = self.get_compat_env_command("hg", compat_prefix='/usr/bin/')
    if command:
      hg_command += command
    if not hg_command.endswith(' '):
      hg_command += ' '
    return hg_command

  def get_git_command(self, command=''):
    git_command = self.get_compat_env_command("git", compat_prefix='/usr/bin/')
    if self.remote_options:
      git_command += "-c "
      for key, val in list(self.remote_options["git"].items()):
        git_command += "%s=%s " % (key, val)
    if command:
      git_command += command
    if not git_command.endswith(' '):
      git_command += ' '
    return git_command

  def remove_existing_empty_src_directory(self):
      backup_log_dir = os.path.join(self.base_directory,
                                    '.yamm_{0}'.format(self.repository_name))
      if os.path.isdir(self.src_directory):
          # File .yamm/env_build may be present is the build dir is the
          # same as the source dir and software has a build dependency.
          if os.path.isdir(self.yamm_src_log_dir):
              if os.path.isdir(backup_log_dir):
                  fast_delete(backup_log_dir)
              shutil.move(self.yamm_src_log_dir, backup_log_dir)
          # Try to remove existing src directory only if empty
          os.rmdir(self.src_directory)

  def move_yamm_log(self, src, dest):
      backup_log_dir = os.path.join(self.base_directory,
                                    '.yamm_{0}'.format(self.repository_name))
      # Try to recover yamm log dir from backup or create it
      if not os.path.exists(self.yamm_src_log_dir):
        if os.path.isdir(backup_log_dir):
            shutil.move(backup_log_dir, self.yamm_src_log_dir)
        else:
            os.makedirs(self.yamm_src_log_dir)
      # Move log file
      shutil.move(src, dest)

  def check_tagsha1_git_consistency(self, tag, sha1):
    if self.git_tag_mode and self.sha1_version:
      command = "(cd {0} && ".format(self.src_directory)
      command += self.remote_env
      command += self.get_git_command('rev-list -1')
      command += tag
      command += " ) "
      p = subprocess.Popen(command,
                           shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
      p.wait()
      if p.returncode == 0:  # tag was found
        sha_of_tag = p.stdout.read().strip()
        if not sha_of_tag.startswith(sha1):
          self.logger.exception("Inconsistency between tag %s(%s) and sha1 %s." %
                                (tag, sha_of_tag, sha1))

  def cvs_checkout_repository(self):
      self.command_log.print_begin_work("Checkout", space=self.space)
      checkout_log = os.path.join(self.base_directory,
                                  "checkout_%s.log" % self.extract_directory_name)
      checkout_cmd_log = os.path.join(self.base_directory,
                                      "checkout_%s_cmd.log" % self.extract_directory_name)
      command = "cd {0} ;".format(os.path.join(self.src_directory, os.pardir))
      command += self.remote_env
      command += "cvs -d {0} ".format(self.root_repository)
      command += "checkout -kk -P "
      if not self.istrunk:
        command += "-r {0} ".format(self.tag)
      command += "-d {0} {1}".format(self.extract_directory_name, self.repository_name)
      command += " > {0} 2>&1".format(checkout_log)
      try:
        self.execute_command("CVS checkout", command, logfilename=checkout_cmd_log)
      except TaskException as e:
        cvsdir = os.path.join(self.src_directory, "CVS")
        if os.path.exists(cvsdir):
          if self.logger.verbose < VerboseLevels.DRY_RUN:
            fast_delete(cvsdir)
          else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug("Command is: misc.fast_delete({0})".format(cvsdir))
        raise e
      self.move_yamm_log(checkout_log, self.log_dirs['checkout'])
      self.move_yamm_log(checkout_cmd_log, self.log_dirs['checkout_cmd'])
      self.command_log.print_end_work()

  def cvs_update_repository(self):
      self.command_log.print_begin_work("Diff", space=self.space)
      command = "cd {0} ;".format(self.src_directory)
      command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
      command += self.remote_env
      command += "cvs diff -kk -u -r {0} ".format(self.tag or 'HEAD')
      command += "> {0} 2>&1 ;".format(self.log_dirs['diff'])
      self.execute_command("CVS diff", command, no_dry_run=True, logfilename=self.log_dirs['diff_cmd'])
      if os.path.exists(self.log_dirs['diff']):
        with open(self.log_dirs['diff']) as diff_file:
          diff = 0
          for line in diff_file.readlines():
            line_split = line.split()
            if line_split[0] == "RCS":
              diff = 1
              break
          if diff == 0:
            self.diff = 0
      # Cannot use ier (look at cvs diff documentation)
      if self.diff == 1:
        self.command_log.print_end_work()
      else:
        self.command_log.print_end_work("(No Diff Found)")

      if self.diff == 1:
        self.command_log.print_begin_work("Update", space=self.space)
        self.delete_install_check_file()
        command = "cd {0} ;".format(self.src_directory)
        command += self.remote_env
        command += "cvs -d {0} ".format(self.root_repository)
        command += "update -d -kk -P -R "
        command += "> {0} 2>&1 ;".format(self.log_dirs['update'])
        self.execute_command("CVS update", command, logfilename=self.log_dirs['update_cmd'])
        self.command_log.print_end_work()

  def svn_checkout_repository(self, url):
      checkout_log = os.path.join(self.base_directory,
                                  "checkout_%s.log" % self.extract_directory_name)
      checkout_cmd_log = os.path.join(self.base_directory,
                                      "checkout_%s_cmd.log" % self.extract_directory_name)
      self.command_log.print_begin_work("Checkout", space=self.space)
      command = "cd {0} ;".format(os.path.join(self.src_directory, os.pardir))
      command += self.remote_env
      command += self.get_svn_command("co " + url)
      command += self.extract_directory_name
      command += " > {0} 2>&1".format(checkout_log)
      try:
        self.execute_command("SVN checkout", command, logfilename=checkout_cmd_log)
      except TaskException as e:
        svndir = os.path.join(self.src_directory, ".svn")
        if os.path.exists(svndir):
          if self.logger.verbose < VerboseLevels.DRY_RUN:
            fast_delete(svndir)
          else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug("Command is: misc.fast_delete({0})".format(svndir))
        raise e
      self.move_yamm_log(checkout_log, self.log_dirs['checkout'])
      self.move_yamm_log(checkout_cmd_log, self.log_dirs['checkout_cmd'])
      self.command_log.print_end_work()

  def svn_update_repository(self, root_url, url):
      # Need to upgrade local copy ?
      """
      if self.use_compat_env:
        self.command_log.print_begin_work("Svn upgrade local copy", space=self.space)
        remote_command = "cd {0} && ".format(self.src_directory)
        remote_command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
        remote_command += self.remote_env
        remote_command += self.get_svn_command("upgrade")
        remote_command += '>> {0} 2>&1'.format(self.log_dirs['update'])
        self.execute_command("Svn update remote", remote_command)
        self.command_log.print_end_work()
      """
      # Check if remote address is correct
      current_remote_address = self.get_current_remote_address()
      if root_url != current_remote_address:
        # Fetch command failed : trying to update address of remote origin
        self.command_log.print_begin_work("Svn update remote", space=self.space)
        remote_command = "cd {0} && ".format(self.src_directory)
        remote_command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
        remote_command += self.remote_env
        remote_command += self.get_svn_command("switch --relocate {0} {1}".format(current_remote_address, root_url))
        remote_command += '>> {0} 2>&1'.format(self.log_dirs['switch'])
        self.execute_command("Svn update remote", remote_command, logfilename=self.log_dirs['switch_cmd'])
        self.command_log.print_end_work()
      # Check if there is a diff
      self.command_log.print_begin_work("Diff", space=self.space)
      command = "cd {0} ;".format(self.src_directory)
      command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
      command += self.remote_env
      command += self.get_svn_command("diff --old=. --new={0}".format(url))
      command += "> {0} 2>&1 ;".format(self.log_dirs['diff'])
      self.execute_command("SVN diff", command, no_dry_run=True, logfilename=self.log_dirs['diff_cmd'])
      if os.path.exists(self.log_dirs['diff']):
        with open(self.log_dirs['diff']) as diff_file:
          diff = 0
          for line in diff_file.readlines():
            line_split = line.split()
            if line_split and line_split[0] == "Index:":
              diff = 1
              break
          if diff == 0:
            self.diff = 0
      if self.diff == 1:
        self.command_log.print_end_work()
      else:
        self.command_log.print_end_work("(No Diff Found)")

      if self.diff == 1:
        # Update
        self.command_log.print_begin_work("Update", space=self.space)
        self.delete_install_check_file()
        command = "cd {0} ;".format(self.src_directory)
        command += self.remote_env
        command += self.get_svn_command("update --non-interactive --trust-server-cert")
        command += "> {0} 2>&1 ;".format(self.log_dirs['update'])

        # TODO - need to check update.log for conflict
        self.execute_command("SVN update", command, logfilename=self.log_dirs['update_cmd'])
        self.command_log.print_end_work()

  def git_clone_repository(self, url, branch_or_tag):
      self.command_log.print_begin_work("Clone", space=self.space)
      self.remove_existing_empty_src_directory()
      checkoutlog = os.path.join(self.base_directory,
                                 "checkout_%s.log" % self.extract_directory_name)
      checkout_cmd_log = os.path.join(self.base_directory,
                                      "checkout_%s_cmd.log" % self.extract_directory_name)
      command = "cd {0} && ".format(self.base_directory)
      command += self.remote_env
      command += self.get_git_command('clone {0} {1}'.format(url, self.extract_directory_name))
      command += " > {0} 2>&1".format(checkoutlog)
      try:
        self.execute_command("Git clone", command, logfilename=checkout_cmd_log)
      except TaskException as e:
        gitdir = os.path.join(self.src_directory, ".git")
        if os.path.exists(gitdir):
          if self.logger.verbose < VerboseLevels.DRY_RUN:
            fast_delete(gitdir)
          else:
            self.command_log.print_debug("=== SIMULATION MODE ===")
            self.command_log.print_debug("Command is: misc.fast_delete({0})".format(gitdir))
        raise e
      self.move_yamm_log(checkoutlog, self.log_dirs['checkout'])
      self.move_yamm_log(checkout_cmd_log, self.log_dirs['checkout_cmd'])
      self.command_log.print_end_work()

      self.check_tagsha1_git_consistency(branch_or_tag, self.sha1_version)

      # Checkout to the required branch/tag
      self.command_log.print_begin_work("Checkout", space=self.space)
      command = "cd {0} && ".format(self.src_directory)
      command += self.remote_env
      command += self.get_git_command('checkout')
      if self.sha1_version:
        command += self.sha1_version
      else:
        command += branch_or_tag
      command += " >> {0} 2>&1".format(self.log_dirs['checkout'])
      try:
        self.execute_command("Git checkout", command, logfilename=self.log_dirs['checkout_cmd'])
      except TaskException as e:
        command = "cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command('checkout')
        if self.sha1_version:
          command += self.sha1_version
        else:
          raise e
        command += " >> {0} 2>&1".format(self.log_dirs['checkout'])
        self.execute_command("Git checkout", command, logfilename=self.log_dirs['checkout_cmd'])
      self.command_log.print_end_work()

      # Update the git config
      git_config_commands = []
      for key, val in list(self.remote_config["git"].items()):
        if val != "":
          local_command = self.get_git_command("config ")
          if key.startswith('submodule.'):
            local_command += "-f .gitmodules "
          local_command += "%s %s" % (key, val)
          git_config_commands.append(local_command)
      if git_config_commands:
        self.command_log.print_begin_work("Update config", space=self.space)
        command = "(cd {0} && ".format(self.src_directory)
        command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
        command += self.remote_env
        command += " ; ".join(git_config_commands)
        command += ') >> {0} 2>&1'.format(self.log_dirs['update'])
        self.execute_command("Git update config", command, logfilename=self.log_dirs['update_cmd'])
        self.command_log.print_end_work()

      if os.path.exists(os.path.join(self.src_directory, ".gitmodules")):
        # Update submodules remote url
        self.command_log.print_begin_work("Update submodules remote", space=self.space)
        command = "(cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command('submodule sync')
        command += ") >> {0} 2>&1".format(self.log_dirs['checkout'])
        self.execute_command("Git update submodules remote", command, logfilename=self.log_dirs['checkout_cmd'])
        self.command_log.print_end_work()

        # Update submodules
        self.command_log.print_begin_work("Init and update submodules", space=self.space)
        command = "( cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command('submodule update --init --recursive')
        # Reset to HEAD otherwise switch will fail due to local change in .gitmodules
        command += "&& "
        command += self.get_git_command('checkout -- .gitmodules')
        command += ") >> {0} 2>&1".format(self.log_dirs['checkout'])
        self.execute_command("Git init and update submodules", command, logfilename=self.log_dirs['checkout_cmd'])
        self.command_log.print_end_work()

  def git_update_repository(self, url, branch_or_tag):
      # Check if .yamm directory exists
      if not os.path.exists(self.yamm_src_log_dir):
        os.makedirs(self.yamm_src_log_dir)
      # Check if remote address is correct
      current_remote_address = self.get_current_remote_address()
      if url != current_remote_address:
        self.command_log.print_begin_work("Git update remote", space=self.space)
        command = "cd {0} && ".format(self.src_directory)
        command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
        command += self.remote_env
        command += self.get_git_command('remote set-url origin {0}'.format(url))
        command += '>> {0} 2>&1'.format(self.log_dirs['update'])
        self.execute_command("Git update remote", command, logfilename=self.log_dirs['update_cmd'])
        self.command_log.print_end_work()

      # Update the git config
      git_remote_config = self.remote_config["git"]
      if git_remote_config:
        git_config_commands = []
        for key, val in list(git_remote_config.items()):
          if val != "":
            local_command = self.get_git_command("config")
            if key.startswith('submodule.'):
              local_command += "-f .gitmodules "
            local_command += "%s %s" % (key, val)
            git_config_commands.append(local_command)
          else:
            git_config_commands.append("if test $({0} --get {1}); then {0} --unset-all {1}; fi"
                                       .format(self.get_git_command("config").strip(), key))
        if git_config_commands:
          self.command_log.print_begin_work("Update config", space=self.space)
          command = "(cd {0} && ".format(self.src_directory)
          command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
          command += " ; ".join(git_config_commands)
          command += ') >> {0} 2>&1'.format(self.log_dirs['update'])
          self.execute_command("Git update config", command, logfilename=self.log_dirs['update_cmd'])
          self.command_log.print_end_work()

      # Fetch the remote branches.
      self.command_log.print_begin_work("Fetch", space=self.space)
      command = "cd {0} && ".format(self.src_directory)
      command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
      command += self.remote_env
      command += self.get_git_command('fetch --prune')
      command += '> {0} 2>&1'.format(self.log_dirs['fetch'])
      self.execute_command("Git fetch", command, logfilename=self.log_dirs['fetch_cmd'])
      self.command_log.print_end_work()

      # Fetch the remote tags
      self.command_log.print_begin_work("Fetch tags", space=self.space)
      command = "cd {0} && ".format(self.src_directory)
      command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
      command += self.remote_env
      command += self.get_git_command('fetch --tags')
      command += '>> {0} 2>&1'.format(self.log_dirs['fetch'])
      self.execute_command("Git fetch tags", command, logfilename=self.log_dirs['fetch_cmd'])
      self.command_log.print_end_work()

      self.check_tagsha1_git_consistency(branch_or_tag, self.sha1_version)

      # Eventually switch branch. It is always done so that the diff
      # is really done between the specified branch and the remote one.
      work_msg = "Checkout tag" if self.git_tag_mode else "Checkout branch"
      self.command_log.print_begin_work(work_msg, space=self.space)
      command = "cd {0} && ".format(self.src_directory)
      command += self.remote_env
      command += self.get_git_command('checkout')
      command += branch_or_tag
      command += "> {0} 2>&1".format(self.log_dirs['switch'])
      try:
        self.execute_command("Git " + work_msg, command, logfilename=self.log_dirs['switch_cmd'])
      except TaskException as e:
        # In case of failure and if not sha1_version is given, exception is raised
        if not self.sha1_version:
          raise e
        command = "cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command('checkout')
        command += self.sha1_version
        command += " >> {0} 2>&1".format(self.log_dirs['switch'])
        self.execute_command("Git " + work_msg, command, logfilename=self.log_dirs['switch_cmd'])
      self.command_log.print_end_work()

      if os.path.exists(os.path.join(self.src_directory, ".gitmodules")):
        # Update submodules remote url
        self.command_log.print_begin_work("Update submodules remote", space=self.space)
        command = "(cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command('submodule sync')
        command += ") >> {0} 2>&1".format(self.log_dirs['checkout'])
        self.execute_command("Git update submodules remote", command, logfilename=self.log_dirs['checkout_cmd'])
        self.command_log.print_end_work()

        # Update potential submodules
        self.command_log.print_begin_work("Init and update submodules", space=self.space)
        command = "(cd {0} && ".format(self.src_directory)
        command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
        command += self.remote_env
        command += self.get_git_command('submodule update --init --recursive')
        # Reset to HEAD otherwise switch will fail due to local change in .gitmodules
        command += "&& "
        command += self.get_git_command('checkout -- .gitmodules')
        command += ') >> {0} 2>&1'.format(self.log_dirs['checkout'])
        self.execute_command("Git init and update submodules", command, logfilename=self.log_dirs['checkout_cmd'])
        self.command_log.print_end_work()

      # Check if there is a diff. The cases of branch and tags are
      # treated seperately:
      # - for a branch, only local commited changes are checked
      # - for a tag, we don't allow local differences
      self.diff = 0
      if self.git_tag_mode:
        self.command_log.print_begin_work("Check status", space=self.space)
        command = "cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command('status --untracked-files=no --porcelain')
        command += " > {0} 2>&1".format(self.log_dirs['status'])
        self.execute_command("Git status", command, no_dry_run=True, logfilename=self.log_dirs['status_cmd'])
        if os.path.exists(self.log_dirs['status']):
          try:
            size = os.path.getsize(self.log_dirs['status'])
            if size > 0:
              self.diff = 1
          except:
            self.logger.exception("Error in reading git status file: %s" % self.log_dirs['status'])
        if self.diff == 1:
          return "Error in Switch tag, the repository has local changes"
        else:
          self.command_log.print_end_work("(Status ok)")

      else:
        # In case of a branch, we must check if it has an upstream branch
        self.command_log.print_begin_work("Diff", space=self.space)
        command = "cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command()
        command += "for-each-ref --format='%(upstream:short)' $({0})".format(self.get_git_command("symbolic-ref -q HEAD"))
        command += " > {0} 2>&1".format(self.log_dirs['upstream'])
        self.execute_command("Git test upstream", command, no_dry_run=True, logfilename=self.log_dirs['upstream_cmd'])
        has_upstream = False
        try:
          with open(self.log_dirs['upstream']) as upstream_file:
            upstream = upstream_file.read().strip()
            has_upstream = len(upstream) > 0
        except:
          self.logger.exception("Error in reading upstream file: %s" % self.log_dirs['upstream'])
        os.remove(self.log_dirs['upstream'])

        command = "cd {0} && ".format(self.src_directory)
        command += self.remote_env
        command += self.get_git_command('diff')
        if has_upstream:
          # @{u} is a shortcut for upstream (remote tracking branch)
          command += "@{u} "
        else:
          command += "HEAD "
        command += "--summary --no-ext-diff --stat "
        command += '> {0} 2>&1'.format(self.log_dirs['diff'])
        self.execute_command("Git diff", command, no_dry_run=True, logfilename=self.log_dirs['diff_cmd'])
        if os.path.exists(self.log_dirs['diff']):
          try:
            size = os.path.getsize(self.log_dirs['diff'])
            if size > 0:
              self.diff = 1
          except:
            self.logger.exception("Error in reading git diff file: %s" % self.log_dirs['diff'])
        if self.diff == 1:
          self.command_log.print_end_work()
        else:
          self.command_log.print_end_work("(No Diff Found)")

        if has_upstream and self.diff == 1:
          # Merge from remote tracking branch
          self.command_log.print_begin_work("Update", space=self.space)
          command = "cd {0} && ".format(self.src_directory)
          command += self.remote_env
          command += self.get_git_command('merge @{u}')
          command += " > {0} 2>&1 ;".format(self.log_dirs['update'])
          self.execute_command("Git merge", command, logfilename=self.log_dirs['update_cmd'])
          if os.path.exists(self.log_dirs['update']):
            try:
              with open(self.log_dirs['update']) as update_file:
                updatelog = update_file.read().strip()
                if updatelog == "Already up-to-date.":
                  self.diff = 0
                else:
                  self.delete_install_check_file()
            except:
              self.logger.exception("Error in reading git update file: %s" % self.log_dirs['update'])
              self.delete_install_check_file()
          self.command_log.print_end_work()
  
  def hg_clone_repository(self, url, branch_or_tag):
      self.command_log.print_begin_work("Clone", space=self.space)
      self.remove_existing_empty_src_directory()
      checkoutlog = os.path.join(self.base_directory,
                                 "checkout_%s.log" % self.extract_directory_name)
      checkout_cmd_log = os.path.join(self.base_directory,
                                      "checkout_%s_cmd.log" % self.extract_directory_name)
      command = "cd {0} && ".format(self.base_directory)
      command += self.remote_env
      command += self.get_hg_command('clone {0} {1}'.format(url, self.extract_directory_name))
      command += " > {0} 2>&1".format(checkoutlog)
      try:
          self.execute_command("Hg clone", command, logfilename=checkout_cmd_log)
      except TaskException as e:
          hgdir = os.path.join(self.src_directory, ".hg")
          if os.path.exists(hgdir):
              if self.logger.verbose < VerboseLevels.DRY_RUN:
                  fast_delete(hgdir)
              else:
                  self.command_log.print_debug("=== SIMULATION MODE ===")
                  self.command_log.print_debug("Command is: misc.fast_delete({0})".format(hgdir))
          raise e
      self.move_yamm_log(checkoutlog, self.log_dirs['checkout'])
      self.move_yamm_log(checkout_cmd_log, self.log_dirs['checkout_cmd'])
      self.command_log.print_end_work()

      # Checkout to the required branch/tag
      self.command_log.print_begin_work("Checkout", space=self.space)
      command = "cd {0} && ".format(self.src_directory)
      command += self.remote_env
      # command += self.get_hg_command('checkout')
      command += self.get_hg_command('update')
      command += branch_or_tag
      command += " >> {0} 2> /dev/null".format(self.log_dirs['checkout'])
      self.execute_command("Hg checkout", command, logfilename=self.log_dirs['checkout_cmd'])
      self.command_log.print_end_work()
  
  def hg_update_repository(self, url, branch_or_tag):
      # Check if remote address is correct
      current_remote_address = self.get_current_remote_address()
      if url != current_remote_address:
          raise TaskException("Current remote address and url of the repo are not the same. Please fix this discrepancy.")
      # Fetch the remote branches.
      self.command_log.print_begin_work("Fetch", space=self.space)
      command = "cd {0} && ".format(self.src_directory)
      command += "mkdir -p {0} && ".format(self.yamm_src_log_dir)
      command += self.remote_env
      command += self.get_hg_command('pull')
      command += '> {0} 2> /dev/null'.format(self.log_dirs['fetch'])
      self.execute_command("Hg pull", command, logfilename=self.log_dirs['fetch_cmd'])
      self.command_log.print_end_work()

      # Eventually switch branch. It is always done so that the diff
      # is really done between the specified branch and the remote one.
      work_msg = "Checkout tag" if self.hg_tag_mode else "Checkout branch"
      self.command_log.print_begin_work(work_msg, space=self.space)
      command = "cd {0} && ".format(self.src_directory)
      command += self.remote_env
      #command += self.get_hg_command('checkout')
      command += self.get_hg_command('update')
      command += branch_or_tag
      command += "> {0} 2> /dev/null".format(self.log_dirs['switch'])
      self.execute_command("Hg " + work_msg, command, logfilename=self.log_dirs['switch_cmd'])
      self.command_log.print_end_work()

      # Check if there is a diff. The cases of branch and tags are
      # treated seperately:
      # - for a branch, only local commited changes are checked
      # - for a tag, we don't allow local differences
      self.diff = 0
      if self.hg_tag_mode:
          self.command_log.print_begin_work("Check status", space=self.space)
          command = "cd {0} && ".format(self.src_directory)
          command += self.remote_env
          command += self.get_hg_command('status --quiet')
          command += " > {0} 2>&1".format(self.log_dirs['status'])
          self.execute_command("Hg status", command, no_dry_run=True, logfilename=self.log_dirs['status_cmd'])
          if os.path.exists(self.log_dirs['status']):
              try:
                  size = os.path.getsize(self.log_dirs['status'])
                  if size > 0:
                      self.diff = 1
              except:
                  self.logger.exception("Error in reading hg status file: %s" % self.log_dirs['status'])
          if self.diff == 1:
              return "Error in Switch tag, the repository has local changes"
          else:
              self.command_log.print_end_work("(Status ok)")

      else:
          command = "cd {0} && ".format(self.src_directory)
          command += self.remote_env
          command += self.get_hg_command('diff')
          command += "--stat "
          command += '> {0} 2> /dev/null'.format(self.log_dirs['diff'])
          self.execute_command("Hg diff", command, no_dry_run=True, logfilename=self.log_dirs['diff_cmd'])
          if os.path.exists(self.log_dirs['diff']):
              try:
                  size = os.path.getsize(self.log_dirs['diff'])
                  if size > 0:
                      self.diff = 1
              except:
                  self.logger.exception("Error in reading hg diff file: %s" % self.log_dirs['diff'])
          if self.diff == 1:
              self.command_log.print_end_work()
          else:
              self.command_log.print_end_work("(No Diff Found)")

          if self.diff == 1:
              # Merge from remote tracking branch
              self.command_log.print_begin_work("Update", space=self.space)
              self.delete_install_check_file()
              command = "cd {0} && ".format(self.src_directory)
              command += self.remote_env
              command += self.get_hg_command('update tip')
              command += " > {0} 2> /dev/null ;".format(self.log_dirs['update'])
              self.execute_command("Hg update", command, logfilename=self.log_dirs['update_cmd'])
              self.command_log.print_end_work()

  def execute(self, *args):

    if self.remote_type == "cvs":
      ret = self.execute_cvs()
    elif self.remote_type == "svn":
      ret = self.execute_svn()
    elif self.remote_type == "git":
      ret = self.execute_git()
    elif self.remote_type == "hg":
      ret = self.execute_hg()

    if not ret:
      self.last_commit = self.get_last_commit_id()
    return ret

  def execute_cvs(self):
    # Check if it is an update or a checkout
    if not os.path.exists(os.path.join(self.src_directory, "CVS")):
        self.cvs_checkout_repository()
    else:
        self.cvs_update_repository()
    return ""

  def execute_svn(self):
    url = os.path.join(self.root_repository, self.repository_name)
    root_url = url
    if self.tag != "":
      url += "/" + self.tag

    # Check if it is an update or a checkout
    if not os.path.exists(os.path.join(self.src_directory, ".svn")):
        self.svn_checkout_repository(url)
    else:
        self.svn_update_repository(root_url, url)
    return ""

  def execute_git(self):
    url = os.path.join(self.root_repository, self.repository_name)
    if self.istrunk:
      self.tag = "master"

    self.git_tag_mode = False
    branch_or_tag = self.tag
    tag_labels = ('git_tag/', 'tags/')
    for tag_label in tag_labels:
      if self.tag.startswith(tag_label):
        self.git_tag_mode = True
        branch_or_tag = self.tag[len(tag_label):]
        break

    if self.sha1_version:
        self.git_tag_mode = True

    # Check if it is an update or a checkout
    if not os.path.exists(os.path.join(self.src_directory, ".git")):
      self.git_clone_repository(url, branch_or_tag)
    else:
      self.git_update_repository(url, branch_or_tag)

    return ""

  def execute_hg(self):
      """Clone or update a Mercurial repository"""
      if not self.repository_name:
          self.repository_name = ''
      url = os.path.join(self.root_repository, self.repository_name)
      if self.istrunk and self.tag != "trunk":
          self.tag = "default"

      self.hg_tag_mode = False
      branch_or_tag = self.tag
      tag_labels = ('hg_tag/', 'tags/')
      for tag_label in tag_labels:
          if self.tag.startswith(tag_label):
              self.hg_tag_mode = True
              branch_or_tag = self.tag[len(tag_label):]
              break

      # Check if it is an update or a checkout
      if not os.path.exists(os.path.join(self.src_directory, ".hg")):
          self.hg_clone_repository(url, branch_or_tag)
      else:
          self.hg_update_repository(url, branch_or_tag)

      return ""

#  Copyright (C) 2012 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

import os

from yamm.core.base.misc import VerboseLevels
from yamm.core.engine.tasks.generic_task import GenericTask


class RunTestSuite(GenericTask):
    """
      This task is called to run test suites on a software or a version.
    """

    def __init__(self, test_suite, verbose_level=VerboseLevels.INFO):
      GenericTask.__init__(self, '[RUN TEST SUITE TASK]', verbose_level)
      self.test_suite = test_suite

    def prepare_work(self, software_config, space, command_log):
      self.command_log = command_log
      self.space = space
      self.work_directory = self.test_suite.work_directory
      if self.work_directory is None:
        self.work_directory = software_config.build_directory

      # Create work directory if it does not exist
      if self.logger.verbose < VerboseLevels.DRY_RUN:
        if not os.path.exists(self.work_directory):
          try:
            os.makedirs(self.work_directory, 0o755)
          except:
            return "Error in creating directory %s" % self.work_directory
      else:
        self.command_log.print_debug("=== SIMULATION MODE ===")
        self.command_log.print_debug("Command is: os.makedirs({0}, 0755)".format(self.work_directory))
      return ""

    def execute(self, dependency_command, executor, current_software):
      self.command_log.print_info("Run test suite %s" % self.test_suite.name, space=self.space)

      logfile = os.path.join(self.work_directory, "test_%s.log" % self.test_suite.name.replace(" ", "_"))
      msg = self.test_suite.execute_test(self.command_log, dependency_command,
                                         self.work_directory, logfile, space=self.space + 2)
      if msg != "":
        self.command_log.print_warning("Test suite failed: %s" % msg)
      #self.test_suite.test_log.end_test()
      return msg

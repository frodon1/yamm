#!/usr/bin/env python
# -*- coding: utf-8 *-

from __future__ import absolute_import

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from past.builtins import execfile
from builtins import object
import subprocess

try:
  import argparse
except ImportError:
  from . import yamm_argparse as argparse

class FrameworkYammBuildSoftware(object):
  def __init__(self):
    self.notify_title = "YAMM"
    self.notify_icon = os.path.realpath(\
                        os.path.join(os.path.dirname(\
                          os.path.abspath(__file__)), os.pardir,
                                     'gui/icons/yamm-icon.svg'))
    self.possible_project_variable_names = ["yamm_project"]
    self.project_variable_name = None
    self.parser = argparse.ArgumentParser()

  def set_parser_args(self):
    # Parse command line
    self.parser.add_argument("-c", "--config", dest="config_file",
                             help="configuration file (default = %(default)s). "
                      "Other command line options will override corresponding "
                      "options in configuration file.",
                      default="config.py")
    group = self.parser.add_mutually_exclusive_group()
    group.add_argument("-d", "--default", action="store_true",
                       help='Use executor mode specified by option '
                       '"default_executor_mode" (default)')
    group.add_argument("--download", action="store_true",
                       help="download the sources of the software")
    group.add_argument("-u", "--keep_if_no_diff", action="store_true",
                       help="build the software if differences found in "
                       "the network or in the sources")
    group.add_argument("-f", "--build", action="store_true",
                       help="rebuild all the softwares (build_configure; "
                       "configure; make; make install)")
    group.add_argument("-i", "--incremental_compile", action="store_true",
                       help="build the software incrementally "
                       "(make; make install)")
    group.add_argument("-b", "--install_from_binary_archive",
                       action="store_true",
                       help="install the software from a binary archive")
    group.add_argument("-t", "--run_tests", action="store_true",
                       help="run tests on the software")
    group.add_argument("--keep_if_installed", action="store_true",
                        help="build the software only if it is not "
                        "already installed")
    group.add_argument("--download_and_build", action="store_true",
                       help="download the sources and build the software")
    self.parser.add_argument("-o", "--ok_message",
                             help="Display a message on success", default="")
    self.parser.add_argument("-k", "--ko_message",
                             help="Display a message on failure", default="")
    self.parser.add_argument("--show-html", dest="show_html",
                             action="store_true",
                             help="Display the html log file in the default browser")
    self.parser.add_argument("software_list", nargs="+", metavar="SOFTWARE",
                             help="List of softwares to build")
    self.parser.set_defaults(update=True)

  def set_project_options(self, args, config):
    pass

  def run(self):
    self.set_parser_args()
    args = self.parser.parse_args()

    config = {}
    execfile(args.config_file, config)

    for name in self.possible_project_variable_names:
      if name in config:
        self.project_variable_name = name
        break

    self.set_project_options(args, config)

    # Run requested tasks
    update_mode = "default"
    update_modes = ["default",
                    "download",
                    "keep_if_no_diff",
                    "build",
                    "incremental_compile",
                    "install_from_binary_archive",
                    "run_tests",
                    "keep_if_installed",
                    "download_and_build"]

    for mode in update_modes:
      if getattr(args, mode):
        update_mode = mode

    res = config[self.project_variable_name]\
          .update(software_list=args.software_list,
                  update_mode=update_mode)

    if args.show_html:
      html_log_file = config[self.project_variable_name]\
              .get_last_command_html_log_file()
      if os.path.isfile(html_log_file):
        subprocess.call(['xdg-open', html_log_file])
#         subprocess.call('xdg-open {0}'.format(html_log_file), shell=True)

    msg = ""
    if res and args.ok_message:
      msg = 'notify-send -i {0} {1} "{2}"'.format(self.notify_icon,
                                                  self.notify_title,
                                                  args.ok_message)

    if not res and args.ko_message:
      msg = 'notify-send -i {0} {1} "{2}"'.format(self.notify_icon,
                                                  self.notify_title,
                                                  args.ko_message)

    if msg:
      subprocess.call(msg, shell=True)

if __name__ == "__main__":
  FrameworkYammBuildSoftware().run()



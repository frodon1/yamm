#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from __future__ import absolute_import

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from past.builtins import execfile
from builtins import object
import subprocess

try:
  import argparse
except ImportError:
  from . import yamm_argparse as argparse

class LauncherException(Exception):
  pass

class FrameworkYammCommand(object):
  def __init__(self, name, cmd):
    self.name = name
    self.cmd = cmd

class FrameworkYammCommandsLauncher(object):
  def __init__(self):
    self.yamm_project = None
    self.notify_title = "YAMM"
    self.notify_icon = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../gui/icons/yamm-icon.svg")
    self.commands = {"start":               FrameworkYammCommand("Start", self.start),
                     "start_offline":       FrameworkYammCommand("Start offline", self.start_offline),
                     "start_from_scratch":  FrameworkYammCommand("Start from sratch", self.start_from_scratch),
                     "clean_sources":       FrameworkYammCommand("Clean sources", self.clean_sources),
                     "clean_builds":        FrameworkYammCommand("Clean builds", self.clean_builds),
                     "clean_installs":      FrameworkYammCommand("Clean installs", self.clean_installs),
                     "download":            FrameworkYammCommand("Download", self.download)
                     }
  def run(self):
    # Parse command line
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", dest="config_file", help="Configuration file (default: %(default)s). "
                      "Other command line options will override corresponding options in configuration file.",
                      default="config.py")
    parser.add_argument("-o", "--ok_message", help="Display a message on success", default="")
    parser.add_argument("-k", "--ko_message", help="Display a message on failure", default="")
    parser.add_argument("--show-html", dest="show_html",
                        action="store_true",
                        help="Display the html log file in the default browser")
    parser.add_argument("yamm_command", choices=list(self.commands.keys()), help="Yamm comand to execute")
    parser.add_argument("yamm_command_args", nargs=argparse.REMAINDER, help="Arguments to the command")
    args = parser.parse_args()
    
    if args.yamm_command not in self.commands:
      raise LauncherException("Error, command {0} unknown".format(args.yamm_command))
    command = self.commands[args.yamm_command]

    config = {}
    execfile(args.config_file, config)
    self.yamm_project = config.get("yamm_project", None)
    if self.yamm_project is None:
      raise LauncherException("No Yamm project instance found")

    self.notify(args, command)

  def notify(self, args, command):
    msg = ""
    command_args = []
    command_kw = {}
    for yamm_command_args in args.yamm_command_args:
      if '=' in yamm_command_args:
        key_val = yamm_command_args.split('=')
        key = key_val[0].strip('-')
        val = '='.join(key_val[1:])
        command_kw[key] = val
      else:
        command_args.append(yamm_command_args)
    res = command.cmd(command_args, command_kw)
      
    if args.show_html:
      html_log_file = self.yamm_project.get_last_command_html_log_file()
      if os.path.isfile(html_log_file):
        subprocess.call(['xdg-open', html_log_file])

    if res is True and args.ok_message:
      ok_message = args.ok_message.replace("__CMD__", command.name)
      msg = 'notify-send -i %s "%s" "%s"' % (self.notify_icon, self.notify_title, ok_message)
    if res is False and args.ko_message:
      ko_message = args.ko_message.replace("__CMD__", command.name)
      msg = 'notify-send -i %s "%s" "%s"' % (self.notify_icon, self.notify_title, ko_message)
    if msg:
      subprocess.call(msg, shell=True)

  def start(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.start()

  def start_offline(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.make(executor_mode = "keep_if_installed")

  def start_from_scratch(self, args, kw):
    self.yamm_project.print_configuration()
    self.yamm_project.delete_directories(delete_install=True)
    return self.yamm_project.start()

  def clean_sources(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.delete_directories(delete_src=True)

  def clean_builds(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.delete_directories(delete_build=True)

  def clean_installs(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.delete_directories(delete_install=True)

  def download(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.download()

if __name__ == "__main__":
  FrameworkYammCommandsLauncher().run()

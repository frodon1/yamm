#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import object

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from PyQt4 import QtGui, QtCore


from yamm.core.framework.project_gui import FrameworkProjectGui  # @IgnorePep8


class FrameworkGuiLauncher(object):
    def __init__(self, argv):
        self.argv = argv
        self.lang = ""

    def getProjectGui(self):
        return FrameworkProjectGui

    def getTranslationFile(self):
        return os.path.join(*(yamm_default_dir +
                            ('src', 'yamm', 'core', 'framework', 'gui',
                            'yamm_' + self.lang)))

    def getSpecificTranslationFile(self):
        return ""

    def launchGui(self):
        app = QtGui.QApplication.instance() or QtGui.QApplication(self.argv)
        utf8codec = QtCore.QTextCodec.codecForName("UTF-8")
        QtCore.QTextCodec.setCodecForCStrings(utf8codec)
        # Translation process
        if self.lang != "":
            locale = QtCore.QLocale.system().name()
            qtTranslator = QtCore.QTranslator()
            if qtTranslator.load("qt_" + locale,
                                 QtCore.QLibraryInfo.location(
                                      QtCore.QLibraryInfo.TranslationsPath)):
                app.installTranslator(qtTranslator)

        appTranslator = QtCore.QTranslator()
        if appTranslator.load(self.getTranslationFile()):
            app.installTranslator(appTranslator)

        specificTranslator = QtCore.QTranslator()
        if self.getSpecificTranslationFile():
            if specificTranslator.load(self.getSpecificTranslationFile()):
                app.installTranslator(specificTranslator)

        # There is a bug on some Linux distributions where the QComboBoxes
        # display all their elements instead of a limited number and
        # a scrollbar.  This line ensure that the scrollbar will be displayed.
        app.setStyleSheet("QComboBox {combobox-popup: 0 ;}")

        gui = self.getProjectGui()(app)
        gui.create_widgets()
        gui.init_history()
        gui.start_gui()

if __name__ == "__main__":
    launcher = FrameworkGuiLauncher(sys.argv)
    launcher.launchGui()

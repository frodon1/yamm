#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

# import os
# import sys

from __future__ import print_function
from __future__ import absolute_import

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import object

try:
  import argparse
except ImportError:
  from . import yamm_argparse as argparse

from yamm.core.framework.project import FrameworkProject

class FrameworkYammInfos(object):
  def __init__(self):
    self.project = self.get_project()
    self.parser = argparse.ArgumentParser()
    self.subparsers = self.parser.add_subparsers()
    self.subparsers_dict = {}
    self.subparsers_groups = {}
    self.subparsers_dict["project"] = self.subparsers.add_parser('project-info')
    self.subparsers_groups["project"] = {}
    self.subparsers_dict["software"] = self.subparsers.add_parser('software-info')
    self.subparsers_groups["software"] = {}
    
  def get_name(self):
    return "YAMM"

  def get_project(self):
    return FrameworkProject()

  def set_parser_args(self):
    # Parse command line
    self.subparsers_groups["project"]["project_infos"] = self.subparsers_dict["project"].add_mutually_exclusive_group(required=True)
    self.subparsers_groups["project"]["project_infos"].add_argument("--get-versions", 
                                       action="store_true", 
                                       dest="versions", 
                                       help="print supported versions of %s project"%self.get_name())
    
    self.subparsers_dict["software"].add_argument("--set-version", 
                                               action="store",
                                               dest = "version",
                                               required=True,
                                               help="set %s version"%self.get_name())
    self.subparsers_dict["software"].add_argument("--set-version-flavor", 
                                               action="store", 
                                               dest="version_flavor", 
                                               help="set %s version flavor"%self.get_name())
    self.subparsers_groups["software"]["soft_infos"] = self.subparsers_dict["software"].add_mutually_exclusive_group(required=True)
    self.subparsers_groups["software"]["soft_infos"].add_argument("--get-software-list", 
                                  action="store_true", 
                                  dest="software_list",
                                   help="print the list of softwares")
    self.subparsers_groups["software"]["soft_infos"].add_argument("--get-software-infos", 
                                  action="store", 
                                  dest="software_infos",
                                   help="print compilation informations for a software")
    self.subparsers_groups["software"]["soft_infos"].add_argument("--get-softwares-infos", 
                                  action="store_true", 
                                  dest="softwares_infos", 
                                  help="print compilation informations for all softwares")

  def parse_args(self, args):
    if vars(args).get('versions'):
      self.project.catalog.print_versions()
    if vars(args).get('version'):
      self.project.set_version(args.version)
    if vars(args).get('version_flavor'):
      self.project.set_global_option("version_flavour",args.version_flavor)
    if vars(args).get('software_list'):
      self.project.begin_command()
      software_mode = "nothing"
      print("List of softwares for {0} version {1} {2}:".format(self.get_name(), 
                                                               args.version, 
                                                               "(%s)"%args.version_flavor if vars(args).get('version_flavor') else ""))
      print([soft.name for soft in self.project.current_command_softwares])
      self.project.end_command()
    if vars(args).get('software_infos'):
      self.project.begin_command()
      if not self.project.current_command_version_object.has_software(args.software_infos):
        self.project.end_command()
        self.parser.error("Software unknown in the selected version: %s"%args.software_infos)
      software_mode = "nothing"
      for software in self.project.current_command_softwares:
        if software.name == args.software_infos:
          software.set_command_env(self.project.current_command_softwares,
                                   self.project.current_command_options,
                                   software_mode,
                                   self.project.current_command_version_object,
                                   self.project.current_command_python_version)
          print(software.get_infos_as_string())
          break
      self.project.end_command()
    if vars(args).get('softwares_infos'):
      self.project.begin_command()
      software_mode = "nothing"
      for software in self.project.current_command_softwares:
        software.set_command_env(self.project.current_command_softwares,
                                 self.project.current_command_options,
                                 software_mode,
                                 self.project.current_command_version_object,
                                 self.project.current_command_python_version)
        print(software.get_infos_as_string())
      self.project.end_command()
    
  def run(self):
    self.set_parser_args()
    args = self.parser.parse_args()
    if self.project.catalog is None:
      print("No catalog found")
      return
    self.parse_args(args)

if __name__ == "__main__":
  FrameworkYammInfos().run()


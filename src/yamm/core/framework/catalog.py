#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function
from builtins import object
import sys

from yamm.core.base import misc


class FrameworkCatalog(object):

  def __init__(self, name="default", verbose=0):
    self.name = name
    self.softwares = {}
    self.versions = {}
    self.logger = misc.Log("[CATALOG %s]" % self.name, verbose)

  def add_software(self, name, module_name, module_path=""):
    self.logger.debug("Software added: %s %s %s" % (name, module_name,
                                                    module_path))
    self.softwares[name] = [module_name, module_path]

  def has_software(self, software_name):
    if software_name in list(self.softwares.keys()):
      return True
    else:
      return False

  def get_software_class(self, software_name):
    if not self.has_software(software_name):
      self.print_softwares()
      self.logger.error("Software %s does not exists" % software_name)

    module_name, module_path = self.softwares[software_name]
    if module_path != "":
      module_import_name = module_path + "." + module_name
    else:
      module_import_name = module_name
    __import__(module_import_name)
    software_module = sys.modules[module_import_name]
    self.logger.debug("Software module %s for software %s" % (software_module,
                                                              software_name))
    software_class = getattr(software_module, software_name)
    self.logger.debug("Software class %s for software %s" % (software_class,
                                                             software_name))
    return software_class

  def add_version(self, name, module_name, module_path=""):
    self.logger.debug("Version added: %s %s %s" % (name, module_name,
                                                   module_path))
    self.versions[name] = [module_name, module_path]

  def has_version(self, version_name):
    if version_name in list(self.versions.keys()):
      return True
    else:
      return False

  def get_version_class(self, version_name):
    if not self.has_version(version_name):
      self.print_versions()
      self.logger.error("Version %s does not exists" % version_name)

    module_name, module_path = self.versions[version_name]
    if module_path != "":
      module_import_name = module_path + "." + module_name
    else:
      module_import_name = module_name
    __import__(module_import_name)
    version_module = sys.modules[module_import_name]
    self.logger.debug("Version module %s for version %s" % (version_module,
                                                            version_name))
    version_class = getattr(version_module, version_name)
    self.logger.debug("Version class %s for version %s" % (version_class,
                                                           version_name))
    return version_class

  def print_softwares(self):
    self.logger.info("List of softwares:")
    self.logger.info("------------------")
    softwares_keys = list(self.softwares.keys())
    softwares_keys.sort()
    for software_name in softwares_keys:
      self.logger.info("- %s" % software_name)

  def print_versions(self):
    self.logger.info("List of versions:")
    self.logger.info("-----------------")
    versions_keys = list(self.versions.keys())
    versions_keys.sort()
    for version_name in versions_keys:
      self.logger.info("- %s" % version_name)

if __name__ == "__main__":

  print("Testing FrameworkCatalog class")
  cat = FrameworkCatalog(verbose=1)
  cat.add_software("soft1", "toto.titi", "/temp/tutu")
  cat.add_version("V1", "ver.v1", "/temp/ver")
  cat.print_softwares()
  cat.print_versions()

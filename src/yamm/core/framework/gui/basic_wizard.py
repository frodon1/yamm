#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import sys
from builtins import range
from PyQt4 import QtGui, QtCore
import os

from yamm.core.framework.gui.select_software_list import ManageSoftwareList
from yamm.core.framework.project import FrameworkProject


class FrameworkBasicWizard(QtGui.QWizard):

  def __init__(self, parent, title="", project = None):
    QtGui.QWizard.__init__(self, parent)
    self.nbPages = 0
    self.result = 0
    self.yamm_project = project or FrameworkProject()
    self.setWindowTitle(title or self.tr("Create a YAMM project"))
    self.terminal_list = ["xterm","konsole","gnome-terminal"]
    self.version_comboBox = None
    self.flavour_comboBox = None
    self.remove_list = []
    self.category_attributes = {}
    self.add_pages()
    self.create_titles()
    self.finished.connect(self.end)
  
  def addPage(self, page):
    QtGui.QWizard.addPage(self, page)
    self.nbPages += 1

  def get_yamm_project(self):
    return FrameworkProject()

  def add_pages(self):
    self.addPage(FrameworkBasicIntro(self, self.get_yamm_project()))
    self.addPage(FrameworkBasicOptions(self, self.get_yamm_project()))
    self.addPage(FrameworkBasicRemotes(self, self.get_yamm_project()))
    self.addPage(ManageSoftwareList(self, self.get_yamm_project()))
    
  def create_titles(self):
    for page_en_cours in range(self.nbPages):
      self.page(page_en_cours).setTitle(
                              self.tr("Step {0}/{1}".format(page_en_cours+1,
                                                            self.nbPages)))

  def add_remove_list(self, remove_list):
    # Step 4
    self.remove_list  += remove_list

  def end(self, result):
    self.result = result
    if self.result:
      self.record_intro_page_fields()
      self.record_options_page_fields()
      self.record_remotes_page_fields()

  def record_intro_page_fields(self):
    self.project_name            = str(self.field("project_name").toString())
    self.project_files_directory = \
        str(self.field("project_files_directory").toString())

  def record_options_page_fields(self):
    self.version            = str(self.field("version").toString())
    self.flavour            = str(self.field("flavour").toString())
    self.top_directory      = str(self.field("top_directory").toString())
    self.version_directory  = str(self.field("version_directory").toString())
    self.archives_directory = str(self.field("archives_directory").toString())
    self.parallel_make      = str(self.field("parallel_make").toString())
    self.terminal           = \
        self.terminal_list[int(self.field("terminal").toString())]

    self.continue_on_failed     = False
    self.clean_src_if_success   = False
    self.clean_build_if_success = False
    if self.field("continue_on_failed").toBool():
      self.continue_on_failed = True
    if self.field("clean_src_if_success").toBool():
      self.clean_src_if_success = True
    if self.field("clean_build_if_success").toBool():
      self.clean_build_if_success = True

  def record_remotes_page_fields(self):
    for category in self.yamm_project.options.get_categories():
      # Source types
      source_type = "archive"
      if self.field("%s_source_type"%category).toBool():
        source_type = "remote"
      self.category_attributes[category] = ("source_type", source_type)

class FrameworkBasicIntro(QtGui.QWizardPage):

  def __init__(self, parent, project = None):
    QtGui.QWizardPage.__init__(self, parent)
#     self.setTitle("Step 1/4")
    self.yamm_project = project or FrameworkProject()
    # Page layout
    self.setPageLayout()
    # Fields
    self.registerFields()
    # Default values
    self.setDefaultValues()

  def setPageLayout(self):
    # Name
    name_groupBox = QtGui.QGroupBox(self.tr("Project Name"))
    self.name_lineEdit = QtGui.QLineEdit()
    name_layout = QtGui.QVBoxLayout(self)
    name_layout.addWidget(self.name_lineEdit)
    name_layout.addStretch(1)
    name_groupBox.setLayout(name_layout)

    # Project files directory
    directory_groupBox = QtGui.QGroupBox(self.tr("Project Files Directory"))
    directory_chooseButton = QtGui.QPushButton(self.tr("Browse..."))
    self.directory_lineEdit = QtGui.QLineEdit()
    directory_layout = QtGui.QHBoxLayout(self)
    directory_layout.addWidget(self.directory_lineEdit)
    directory_layout.addWidget(directory_chooseButton)
    directory_groupBox.setLayout(directory_layout)

    # SIGNAL and SLOTS
    directory_chooseButton.clicked.connect(self.choose_directory)

    # Page layout
    layout = QtGui.QVBoxLayout(self)
    layout.addWidget(name_groupBox)
    layout.addWidget(directory_groupBox)
    layout.addStretch(1)
    self.setLayout(layout)

  def registerFields(self):
    # Fields
    self.registerField("project_name*", self.name_lineEdit)
    self.registerField("project_files_directory*", self.directory_lineEdit)

  def setDefaultValues(self):
    pass

  def choose_directory(self):
    select_dir = os.path.expanduser("~")
    cur_dir = self.directory_lineEdit.text()
    if cur_dir and os.path.exists(cur_dir):
      select_dir = cur_dir
    directory_name = QtGui.QFileDialog.getExistingDirectory(self,
                                      self.tr('Choose directory'), select_dir)
    if directory_name:
      self.directory_lineEdit.setText(directory_name)

class FrameworkBasicOptions(QtGui.QWizardPage):

  def __init__(self, parent, project = None):
    QtGui.QWizardPage.__init__(self, parent)
#     self.setTitle("Step 2/4")
    self.yamm_project = project or FrameworkProject()
    self.parent = parent
    self.old_version = "my_version"
    self.home_dir = os.path.expanduser("~")
    self.current_topDir = os.path.join(self.home_dir, "yamm")
    self.topDirDependentWidgets = []
    self.widgets_in_directories_layout = []
    self.widgets_in_options_layout = []
    self.widgets_in_layout = []
    # Page widgets
    self.setWidgets()
    # Page layout
    self.setPageLayout()
    self.setDirectoriesLayout()
    self.setOptionsLayout()
    # Fields
    self.registerFields()
    # Default values
    self.setDefaultValues()

  def setWidgets(self):
    # Version
    self.version_groupBox = QtGui.QGroupBox(self.tr("Version"))
    version_layout = QtGui.QGridLayout(self)
    version_label = QtGui.QLabel(self.tr("Version:"))
    self.version_comboBox = QtGui.QComboBox(self)
    self.version_comboBox.addItem("")
    if self.yamm_project.catalog:
      version_keys = list(self.yamm_project.catalog.versions.keys())
      version_keys.sort(reverse = True)
      self.version_comboBox.addItems(version_keys)
    version_layout.addWidget(version_label, 0, 0)
    version_layout.addWidget(self.version_comboBox, 0, 1)
    self.flavour_label = QtGui.QLabel(self.tr("Flavour:"))
    self.flavour_comboBox = QtGui.QComboBox(self)
    self.flavour_label.setEnabled(False)
    self.flavour_comboBox.setEnabled(False)
    version_layout.addWidget(self.flavour_label, 1, 0)
    version_layout.addWidget(self.flavour_comboBox, 1, 1)
    self.version_groupBox.setLayout(version_layout)
    self.widgets_in_layout.append(self.version_groupBox)
    # Parent
    self.parent.version_comboBox = self.version_comboBox
    self.parent.flavour_comboBox = self.flavour_comboBox

    # Directories
    self.dir_groupBox = QtGui.QGroupBox(self.tr("Directories"))
    self.dir_top_lineEdit = QtGui.QLineEdit()
    self.widgets_in_directories_layout.append([self.tr("Top Directory:"),
                                               self.dir_top_lineEdit])
    
    self.dir_ver_lineEdit = QtGui.QLineEdit()
    self.topDirDependentWidgets.append(self.dir_ver_lineEdit)
    self.widgets_in_directories_layout.append([self.tr("Version Directory:"),
                                               self.dir_ver_lineEdit])
    
    self.dir_arch_lineEdit = QtGui.QLineEdit()
    self.topDirDependentWidgets.append(self.dir_arch_lineEdit)
    self.widgets_in_directories_layout.append([self.tr("Archives Directory:"),
                                               self.dir_arch_lineEdit])
    
    self.widgets_in_layout.append(self.dir_groupBox)

    # Options
    self.opt_groupBox = QtGui.QGroupBox(self.tr("Options"))

    self.opt_term_combo = QtGui.QComboBox()
    self.opt_term_combo.addItems(self.parent.terminal_list)
    self.widgets_in_options_layout.append([self.tr("Terminal:"),
                                           self.opt_term_combo])

    self.opt_par_spinBox = QtGui.QSpinBox()
    self.opt_par_spinBox.setRange(1,999)
    self.widgets_in_options_layout.append(\
                                      [self.tr("Parallel Compilation Flag:"),
                                       self.opt_par_spinBox])

    self.opt_cont_button = QtGui.QPushButton("OFF")
    self.opt_cont_button.setCheckable(True)
    self.widgets_in_options_layout.append([self.tr("Continue on failure:"),
                                           self.opt_cont_button])

    self.opt_clean_src_button = QtGui.QPushButton("OFF")
    self.opt_clean_src_button.setCheckable(True)
    self.widgets_in_options_layout.append([self.tr("Clean sources if success:"),
                                           self.opt_clean_src_button])

    self.opt_clean_build_button = QtGui.QPushButton("OFF")
    self.opt_clean_build_button.setCheckable(True)
    self.widgets_in_options_layout.append([self.tr("Clean builds if success:"),
                                           self.opt_clean_build_button])

    self.widgets_in_layout.append(self.opt_groupBox)

    # Signal and Slots
    self.dir_top_lineEdit.editingFinished.connect(self.topDirectory_changed)
    self.opt_cont_button.toggled.connect(self.toggled_button)
    self.opt_clean_src_button.toggled.connect(self.toggled_button)
    self.opt_clean_build_button.toggled.connect(self.toggled_button)
    self.version_comboBox.currentIndexChanged.connect(self.directory_changed)
    self.version_comboBox.currentIndexChanged[str].connect(self.version_changed)
    self.flavour_comboBox.currentIndexChanged[str].connect(self.flavour_changed)

  def setDirectoriesLayout(self):
    dir_layout = QtGui.QGridLayout()
    row = 0
    for label,widget in self.widgets_in_directories_layout:
      widget.textChanged.connect(self.directory_changed)
      dir_layout.addWidget(QtGui.QLabel(label), row, 0)
      dir_layout.addWidget(widget,              row, 1)
      row += 1
    self.dir_groupBox.setLayout(dir_layout)

  def setOptionsLayout(self):
    opt_layout = QtGui.QGridLayout()
    row = 0
    for label,widget in self.widgets_in_options_layout:
      opt_layout.addWidget(QtGui.QLabel(label), row, 0)
      opt_layout.addWidget(widget,              row, 1, QtCore.Qt.AlignRight)
      row += 1
    self.opt_groupBox.setLayout(opt_layout)

  def setPageLayout(self):
    layout = QtGui.QVBoxLayout(self)
    for widget in self.widgets_in_layout:
      layout.addWidget(widget)
    layout.addStretch(1)
    self.setLayout(layout)

  def registerFields(self):
    self.registerField("version",                self.version_comboBox,
                       "currentText")
    self.registerField("flavour",                self.flavour_comboBox,
                       "currentText")
    self.registerField("top_directory",          self.dir_top_lineEdit)
    self.registerField("version_directory",      self.dir_ver_lineEdit)
    self.registerField("archives_directory",     self.dir_arch_lineEdit)
    self.registerField("terminal",               self.opt_term_combo)
    self.registerField("parallel_make",          self.opt_par_spinBox)
    self.registerField("continue_on_failed",     self.opt_cont_button)
    self.registerField("clean_src_if_success",   self.opt_clean_src_button)
    self.registerField("clean_build_if_success", self.opt_clean_build_button)
    
  def isComplete(self):
    directoryEmpty = False
    for _, widget in self.widgets_in_directories_layout:
      if not str(widget.text()):
        directoryEmpty = True
        break
    if directoryEmpty:
      return False
    
    if not str(self.version_comboBox.currentText()):
      return False
    
    return True

  def setDefaultValues(self):
    self.dir_top_lineEdit.setText(self.current_topDir)
    self.dir_arch_lineEdit.setText(os.path.join(self.current_topDir,
                                                "archives"))
    self.dir_ver_lineEdit.setText(os.path.join(self.current_topDir,
                                               self.old_version))
    self.opt_term_combo.setCurrentIndex(2)
    self.opt_par_spinBox.setValue(1)
    self.opt_cont_button.setChecked(False)
    self.opt_clean_src_button.setChecked(False)
    self.opt_clean_build_button.setChecked(False)

  def toggled_button(self, status):
    if status:
      self.sender().setText(self.tr("ON"))
    else:
      self.sender().setText(self.tr("OFF"))

  def initializePage(self):
    pass

  def cleanupPage(self):
    pass
  
  def directory_changed(self):
    self.completeChanged.emit()

  def version_changed(self, version):
    version = str(version)
    self.flavour_label.setEnabled(False)
    self.flavour_comboBox.clear()
    self.flavour_comboBox.setEnabled(False)
    if not version:
      return
    # Update flavours
    if self.yamm_project.catalog:
      flavour_list = self.yamm_project.catalog.\
          get_version_class(version)().accepted_flavours
      if len(flavour_list) > 0:
        self.flavour_label.setEnabled(True)
        self.flavour_comboBox.setEnabled(True)
        self.flavour_comboBox.addItem("")
        flavour_list.sort()
        self.flavour_comboBox.addItems(flavour_list)
    # Update Version directory
    dir_version = str(self.dir_ver_lineEdit.text())
    if not dir_version.endswith(version):
      dir_version = os.path.join(os.path.sep.join(
                                      dir_version.split(os.path.sep)[:-1]),
                                 version)
      self.dir_ver_lineEdit.setText(dir_version)

  def flavour_changed(self, flavour):
    flavour = str(flavour)
    if not flavour:
      return
    flavour = flavour.replace(' ', '_')
    # Update Version directory
    full_current_dir_version = str(self.dir_ver_lineEdit.text())
    version = str(self.version_comboBox.currentText())
    dir_version = "_".join([version,flavour])
    if not full_current_dir_version.endswith(dir_version):
      full_current_dir_version = os.path.join(
            os.path.sep.join(full_current_dir_version.split(os.path.sep)[:-1]),
            dir_version)
      self.dir_ver_lineEdit.setText(full_current_dir_version)

  def topDirectory_changed(self):
    topDir = str(self.dir_top_lineEdit.text())
    for lineEdit in self.topDirDependentWidgets:
      self.updateLineEditWithTopDir(topDir, lineEdit)
    self.current_topDir = topDir
  
  def updateLineEditWithTopDir(self, topDir, linedit):
    versionDir = str(linedit.text())
    if versionDir.startswith(self.current_topDir + os.path.sep):
      versionDir = versionDir.replace(self.current_topDir, topDir, 1)
      linedit.setText(versionDir)


class FrameworkBasicRemotes(QtGui.QWizardPage):

  def __init__(self, parent, project = None):
    QtGui.QWizardPage.__init__(self, parent)
#     self.setTitle("Step 3/4")
    self.yamm_project = project or FrameworkProject()
    self.categoriesButtons = {}
    self.widgets_in_layout = []
    # Page widgets
    self.setWidgets()
    # Page layout
    self.setPageLayout()
    # Fields
    self.registerFields()
    # Default values
    self.setDefaultValues()

  def _toggled_button_text(self, button, status, checked_text, uncheked_text):
    if status:
      button.setText(checked_text)
    else:
      button.setText(uncheked_text)

  def toggled_button_remote_mode(self, status):
    self._toggled_button_text(self.sender(), status, self.tr("remote"), self.tr("archive"))

  def toggled_button_on_off(self, status):
    self._toggled_button_text(self.sender(), status, self.tr("ON"), self.tr("OFF"))

  def setWidgets(self):
    # Softwares modes
    mod_groupBox = QtGui.QGroupBox(self.tr("Softwares remote modes"))
    mod_layout = QtGui.QGridLayout()
    row = 0
    for category in sorted(self.yamm_project.options.get_categories()):
      cat_label = QtGui.QLabel("%s:"%category)
      button = QtGui.QPushButton(self.tr("archive"))
      button.setCheckable(True)
      button.toggled.connect(self.toggled_button_remote_mode)
      mod_layout.addWidget(cat_label, row, 0)
      mod_layout.addWidget(button, row, 1, QtCore.Qt.AlignRight)
      self.categoriesButtons[category] = button
      row += 1
    mod_groupBox.setLayout(mod_layout)
    self.widgets_in_layout.append(mod_groupBox)

  def setPageLayout(self):
    # Page layout
    layout = QtGui.QVBoxLayout(self)
    for widget in self.widgets_in_layout:
      layout.addWidget(widget)
    layout.addStretch(1)
    self.setLayout(layout)

  def registerFields(self):
    for category, button in list(self.categoriesButtons.items()):
      self.registerField("%s_source_type"%category, button)

  def setDefaultValues(self):
    for button in list(self.categoriesButtons.values()):
      button.setChecked(False)

  def initializePage(self):
    pass

  def cleanupPage(self):
    pass


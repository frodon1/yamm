#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

# from builtins import str
from builtins import range
from builtins import object
from PyQt4 import QtGui, QtCore, uic
import copy
import types

from yamm.core.base import options
from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.gui.gui_project import FrameworkYammGuiProject
from yamm.core.framework.gui.select_software_list import ManageListOfSoftwares
from yamm.core.framework.project import FrameworkProject


class FrameworkEditProjectWindow(QtGui.QMainWindow):

  def __init__(self, parent, yamm_gui, mode, yamm_gui_project=None):
    QtGui.QMainWindow.__init__(self, parent)
    self.yamm_gui = yamm_gui
    self.mode = mode
    if mode == 'edit' and yamm_gui_project is None:
      raise Exception('Impossible to edit non existent Yamm project')

    self.yamm_gui_project = yamm_gui_project

    self.init_variables()
    self.init_widgets()

  def init_variables(self):
    self.yamm_project = self.get_empty_yamm_project()
    if self.mode != "edit":
      self.yamm_gui_project = self.get_empty_yamm_gui_project()
    # Internal attributes
    self.yamm_project_set_version = False

    # Options list widgets on main tab
    self.mandatory_options = {}
    self.current_global_options = {}
    self.current_add_list_global_options = \
              self.yamm_project.options.get_option_list_with_range(["global"])
    self.current_remove_list_global_options = []
    # Remove software list option
    self.current_add_list_global_options.remove("software_remove_list")

  def init_widgets(self):
    # Init
    if self.mode == "edit":
      self.setWindowTitle(self.tr('Edit project: {0}'.format(
                                          self.yamm_gui_project.project_name)))
    else:
      self.setWindowTitle(self.tr('Create a new project'))
    self.center_gui()
    
    self.main_widget = QtGui.QWidget()
    self.options_widget = QtGui.QWidget()
    self.softwares_widget = QtGui.QWidget()

    # Main configuration
    self.save_button = QtGui.QPushButton(self.tr("Save"))
    self.save_and_close_button = QtGui.QPushButton(self.tr("Save and Close"))
    self.cancel_button = QtGui.QPushButton(self.tr("Cancel"))

    self.main_button_layout = QtGui.QHBoxLayout()
    self.main_button_layout.addStretch()
    self.main_button_layout.addWidget(self.save_button)
    self.main_button_layout.addWidget(self.save_and_close_button)
    self.main_button_layout.addWidget(self.cancel_button)

    self.central_tab = QtGui.QTabWidget(self)
    self.central_tab.addTab(self.main_widget, self.tr("Main"))
    self.central_tab.addTab(self.options_widget, self.tr("Options"))
    self.central_tab.addTab(self.softwares_widget, self.tr("Softwares"))

    self.central_layout = QtGui.QVBoxLayout()
    self.central_layout.addWidget(self.central_tab)
    self.central_layout.addLayout(self.main_button_layout)

    # Main Signal and Slots
    self.save_button.clicked.connect(self.save)
    self.save_and_close_button.clicked.connect(self.save_and_close)
    self.cancel_button.clicked.connect(self.close)

    # Central Widget
    self.central_widget = QtGui.QWidget(self)
    self.central_widget.setLayout(self.central_layout)
    self.setCentralWidget(self.central_widget)

    self.init_main_tab()
    self.init_options_tab()
    self.init_softwares_tab()

    # Add default values
    if self.mode == "edit":
      self.mandatory_options["Project Name"].set_value(
                                self.yamm_gui_project.project_name)
      self.mandatory_options["Project Dir"].set_value(
                                self.yamm_gui_project.project_files_directory)
      self.mandatory_options["Project Version"].set_value(
                                self.yamm_gui_project.version)

      # Global options
      list_global_options = self.yamm_project.options.\
                                get_option_list_with_range(["global"])
      for opt_name in sorted(self.yamm_gui_project.global_options.keys()):
        opt_value = self.yamm_gui_project.global_options[opt_name]
        if opt_name != "software_remove_list":
          if opt_name in list_global_options:
            self.add_main_tab_global_option(opt_name,opt_value)
          else:
            self.add_options_tab_option_widget(opt_name, "global", opt_value)
      if self.yamm_gui_project.version_flavour:
        self.add_main_tab_global_option("version_flavour",
                                        self.yamm_gui_project.version_flavour)

      # Other options
      for opt_name in sorted(self.yamm_gui_project.category_options.keys()):
        opt_values = self.yamm_gui_project.category_options[opt_name]
        for opt_value in opt_values:
          self.add_options_tab_option_widget(opt_name,
                                             opt_value[0],
                                             opt_value[1])
      for opt_name in sorted(self.yamm_gui_project.software_options.keys()):
        opt_values = self.yamm_gui_project.software_options[opt_name]
        for opt_value in opt_values:
          self.add_options_tab_option_widget(opt_name,
                                             opt_value[0],
                                             opt_value[1])

      # Softwares list
      if "software_remove_list" in self.yamm_gui_project.global_options:
        for category in sorted(self.categories_tab.keys()):
          category_tab = self.categories_tab[category]
          category_list = category_tab.get_list()
          orig_category_list = copy.deepcopy(category_list)
          for category_soft in orig_category_list:
            if category_soft in self.yamm_gui_project.global_options["software_remove_list"]:
              category_list.remove(category_soft)
          category_tab.set_list(category_list)

    # After
    if self.current_remove_list_global_options == []:
      self.remove_main_tab_option_button.setEnabled(False)

  def get_empty_yamm_project(self):
    return FrameworkProject()

  def get_empty_yamm_gui_project(self):
    return FrameworkYammGuiProject()

  def center_gui(self):
    screen = QtGui.QDesktopWidget().screenGeometry()
    size =  self.geometry()
    self.move((screen.width()-size.width())//2,
              (screen.height()-size.height())//2)

  def init_main_tab(self):

    # Main Tab
    self.add_main_tab_option_button    = QtGui.QPushButton(
                                            self.tr("Add Global Option"))
    self.remove_main_tab_option_button = QtGui.QPushButton(
                                            self.tr("Remove Global Option"))
    self.main_tab_button_layout    = QtGui.QHBoxLayout()
    self.main_tab_button_layout.addStretch()
    self.main_tab_button_layout.addWidget(self.add_main_tab_option_button)
    self.main_tab_button_layout.addWidget(self.remove_main_tab_option_button)

    self.main_tab_qframe = QtGui.QFrame()
    self.main_tab_qframe.setFrameStyle(QtGui.QFrame.HLine)
    self.main_tab_qframe_2 = QtGui.QFrame()
    self.main_tab_qframe_2.setFrameStyle(QtGui.QFrame.HLine)
    self.main_tab_qframe_3 = QtGui.QFrame()
    self.main_tab_qframe_3.setFrameStyle(QtGui.QFrame.HLine)

    self.main_tab_option_layout           = QtGui.QGridLayout()
    self.main_tab_option_layout_v         = QtGui.QVBoxLayout()
    self.main_tab_option_layout_v.addLayout(self.main_tab_option_layout)
    self.main_tab_option_layout_v.addStretch(1)
    self.main_tab_list_dict_option_layout = QtGui.QVBoxLayout()
    self.main_tab_list_dict_option_tabWidget = QtGui.QTabWidget()
    self.main_tab_options_layout          = QtGui.QHBoxLayout()
    
    scrollarea = QtGui.QScrollArea()
    widget = QtGui.QWidget()
    widget.setLayout(self.main_tab_option_layout_v)
    scrollarea.setWidget(widget)
    scrollarea.setWidgetResizable(True)
    
    self.main_tab_options_layout.addWidget(scrollarea)    
    self.main_tab_options_layout.addLayout(
                                      self.main_tab_list_dict_option_layout)

    self.main_tab_layout = QtGui.QVBoxLayout()
    self.main_tab_layout.addWidget(self.main_tab_qframe)
    self.main_tab_layout.addLayout(self.main_tab_options_layout)

    self.main_tab_layout.addWidget(self.main_tab_qframe_3)
    self.main_tab_layout.addLayout(self.main_tab_button_layout)
    self.main_widget.setLayout(self.main_tab_layout)

    self.main_tab_list_dict_option_layout.addWidget(
                                      self.main_tab_list_dict_option_tabWidget)
    self.main_tab_list_dict_option_tabWidget.hide()

    # Main Tab Signal and Slots
    self.add_main_tab_option_button.clicked.connect(self.add_main_tab_option)
    self.remove_main_tab_option_button.clicked.connect(
                                      self.remove_main_tab_option)

    # Add default options
    self.current_main_tab_row = 0
    for option in ("Project Name", "Project Dir"):
      self.mandatory_options[option] = StringOption(option)
      self.mandatory_options[option].add_global_option_in_layout(
        self.main_tab_option_layout, 
        self.current_main_tab_row)
      self.current_main_tab_row += 1

    version_list = list(self.yamm_project.catalog.versions.keys()) or []
    version_list.sort(reverse = True)
    self.mandatory_options["Project Version"] = ComboOption(
                                                  "Project Version",
                                                  version_list,
                                                  self.set_project_version)
    self.mandatory_options["Project Version"].add_global_option_in_layout(
      self.main_tab_option_layout, 
      self.current_main_tab_row)

    self.current_main_tab_row += 1

    self.main_tab_option_layout.addWidget(self.main_tab_qframe_2,
                                          self.current_main_tab_row, 0, 1 , -1)

  def init_options_tab(self):
    # Options Tab Var

    self.current_add_list_options = \
          self.yamm_project.options.get_option_list_without_range(["global"])
    self.options_software_list = []
    self.options_tab_dict = {}
    self.options_added_dict = {}
    self.add_option_button_dict = {}
    self.options_to_remove = []

    # Options Tab Widget
    self.add_options_tab_option_button = \
        QtGui.QPushButton(self.tr("Add Option"))
    self.remove_options_tab_option_button = \
        QtGui.QPushButton(self.tr("Remove Selected Options"))

    self.options_tab_button_layout = QtGui.QHBoxLayout()
    self.options_tab_button_layout.addStretch()
    self.options_tab_button_layout.addWidget(self.add_options_tab_option_button)
    self.options_tab_button_layout.addWidget(
        self.remove_options_tab_option_button)

    self.options_tab_qframe = QtGui.QFrame()
    self.options_tab_qframe.setFrameStyle(QtGui.QFrame.HLine)

    self.options_tab_qtab   = QtGui.QTabWidget(self)
    self.options_tab_layout = QtGui.QVBoxLayout()
    self.options_tab_layout.addWidget(self.options_tab_qtab)
    self.options_tab_layout.addWidget(self.options_tab_qframe)
    self.options_tab_layout.addLayout(self.options_tab_button_layout)
    self.options_widget.setLayout(self.options_tab_layout)

    # Options Tab Signal and Slots
    self.add_options_tab_option_button.clicked.connect(
        self.add_options_tab_option)
    self.remove_options_tab_option_button.clicked.connect(
        self.remove_options_tab_option)

    self.central_tab.setTabEnabled(1, False)

  def configure_options_tab(self):
    if not self.yamm_project_set_version:
      self.central_tab.setTabEnabled(1, False)
    else:
      self.central_tab.setTabEnabled(1, True)
      self.yamm_project.begin_command()
      self.options_software_list = [software.name \
                  for software in self.yamm_project.current_command_softwares]
      self.options_software_list += [s for s in self.yamm_project.current_command_version_object.system_softwares
                                     if s not in self.options_software_list]
      self.yamm_project.end_command()
      self.options_software_list.sort()

  def init_softwares_tab(self):
    self.softwares_tab_qtab   = QtGui.QTabWidget(self)
    self.softwares_tab_layout = QtGui.QVBoxLayout()
    self.softwares_tab_layout.addWidget(self.softwares_tab_qtab)
    self.softwares_widget.setLayout(self.softwares_tab_layout)

    # Categories
    self.categories_tab = {}
    for category in sorted(self.yamm_project.options.get_categories()):
      self.categories_tab[category] = ManageListOfSoftwares(
          self.parent(), self.yamm_project, category)
      self.softwares_tab_qtab.addTab(self.categories_tab[category], category)
    self.central_tab.setTabEnabled(2, False)

  def configure_global_options_list(self):
    if self.yamm_project_set_version:
      self.yamm_project.begin_command()
      accepted_flavours = \
          self.yamm_project.get_version_object().accepted_flavours
      self.yamm_project.end_command()
      option = self.current_global_options.get("version_flavour", None)
      if "version_flavour" not in self.current_add_list_global_options:
        if accepted_flavours != [] and option is None:
          self.current_remove_list_global_options.remove("version_flavour")
          self.current_add_list_global_options.append("version_flavour")
          self.current_add_list_global_options.sort()
      else:
        if accepted_flavours == []:
          self.current_remove_list_global_options.append("version_flavour")
          self.current_add_list_global_options.remove("version_flavour")
          self.current_remove_list_global_options.sort()

  def configure_softwares_tab(self):
    if self.yamm_project_set_version:
      for category_tab in list(self.categories_tab.values()):
        category_tab.yamm_project = self.yamm_project
        category_tab.init_list()

    if not self.yamm_project_set_version:
      self.central_tab.setTabEnabled(2, False)
    else:
      self.central_tab.setTabEnabled(2, True)

  def set_project_version(self, version):
    if version == "":
      self.yamm_project.version = None
      self.yamm_project_set_version = False
    else:
      self.yamm_project_set_version = True
      self.yamm_project.set_version(str(version))
      self.yamm_project.begin_command()
      accepted_flavours = \
          self.yamm_project.get_version_object().accepted_flavours
      self.yamm_project.end_command()
      self.update_flavours(accepted_flavours)
      option = self.current_global_options.get("version_flavour", None)
      if option:
        version_flavour = option.get_value()
        if version_flavour: 
          self.yamm_project.set_version(str(version), version_flavour)

    self.configure_global_options_list()
    self.configure_options_tab()
    self.configure_softwares_tab()

  def update_flavours(self, accepted_flavours):
    option = self.current_global_options.get("version_flavour", None)
    if option:
      current_flavour = option.get_value()
      keep_flavour = current_flavour in accepted_flavours
      current_flavour = current_flavour if keep_flavour else ""
      option.set_combo_list(accepted_flavours)
      self.yamm_project.set_global_option("version_flavour", current_flavour)
      option.set_value(current_flavour)

  def set_software_minimal_list(self, software_minimal_list=None):
    if software_minimal_list is None:
      software_minimal_list = []
    self.yamm_project.options.set_global_option("software_minimal_list",
                                                software_minimal_list)
#    self.configure_options_tab()
    self.configure_softwares_tab()

  def set_version_flavour(self, version_flavour):
    self.yamm_project.options.set_global_option("version_flavour",
                                                str(version_flavour))
    self.configure_global_options_list()
    self.configure_options_tab()
    self.configure_softwares_tab()

  def add_main_tab_option(self):
    (option_name, ok) = QtGui.QInputDialog.getItem(self,
                                self.tr("Select a global option"),
                                self.tr("Options:"),
                                self.current_add_list_global_options,
                                editable=False)
    if ok:
      self.add_main_tab_global_option(str(option_name))

  def add_main_tab_global_option(self, option_name, option_value = None):
    try:
      self.current_add_list_global_options.remove(option_name)
      self.current_remove_list_global_options.append(option_name)
      self.current_remove_list_global_options.sort()
      self.current_add_list_global_options.sort()
    except:
      pass

    # Buttons
    self.remove_main_tab_option_button.setEnabled(True)
    if self.current_add_list_global_options == []:
      self.add_main_tab_option_button.setEnabled(False)

    # Get option type
    self.yamm_project.begin_command()
    option_type = \
        self.yamm_project.current_command_options.get_option_type(option_name)
    option_possible_values = \
        self.yamm_project.current_command_options.get_option_possible_values(
            option_name)
#     software_list = self.yamm_project.current_sorted_softwares
    self.yamm_project.end_command()

    # Add Widget
    callback = None
    if option_name == "version_flavour":
      self.yamm_project.begin_command()
      option_possible_values = \
          self.yamm_project.get_version_object().accepted_flavours
      self.yamm_project.end_command()
      callback = self.set_version_flavour
    if option_name in ("software_minimal_list", "software_only_list",
                       "softwares_user_version"):
      option_possible_values = self.options_software_list
      #callback = self.set_software_minimal_list
    if option_name == "command_verbose_level":
      option_possible_values = [VerboseLevels.reverse_mapping[x] \
                                    for x in option_possible_values]
      option_value = VerboseLevels.reverse_mapping.get(option_value, None)
    if option_type == options.string_type:
      self.current_main_tab_row += 1
      if option_possible_values is not None:
        self.current_global_options[option_name] = \
            ComboOption(option_name, option_possible_values, callback)
      else:
        self.current_global_options[option_name] = StringOption(option_name,
                                                                callback)
      self.current_global_options[option_name].add_global_option_in_layout(
          self.main_tab_option_layout, self.current_main_tab_row)
      if option_value is not None:
        self.current_global_options[option_name].set_value(option_value)
    elif option_type == bool:
      self.current_main_tab_row += 1
      self.current_global_options[option_name] = BoolOption(option_name)
      self.current_global_options[option_name].add_global_option_in_layout(
          self.main_tab_option_layout, self.current_main_tab_row)
      if option_value is not None:
        self.current_global_options[option_name].set_value(option_value)
    elif option_type == int:
      self.current_main_tab_row += 1
      if option_possible_values is not None:
        self.current_global_options[option_name] = ComboOption(
                                                        option_name,
                                                        option_possible_values,
                                                        callback)
      else:
        self.current_global_options[option_name] = \
            IntOption(option_name, callback)
      self.current_global_options[option_name].add_global_option_in_layout(
                                                   self.main_tab_option_layout,
                                                   self.current_main_tab_row)
      if option_value is not None:
        self.current_global_options[option_name].set_value(option_value)
    elif option_type == list:
      self.current_global_options[option_name] = ListOption(
                                                        option_name,
                                                        option_possible_values,
                                                        callback)
      self.current_global_options[option_name].add_global_option_in_layout(
          self.main_tab_list_dict_option_tabWidget)
      if option_value is not None:
        self.current_global_options[option_name].set_value(option_value)
      self.main_tab_list_dict_option_tabWidget.show()
    elif option_type == dict:
      self.current_global_options[option_name] = DictOption(
                                                        option_name,
                                                        option_possible_values)
      self.current_global_options[option_name].add_global_option_in_layout(
          self.main_tab_list_dict_option_tabWidget)
      if option_value is not None:
        self.current_global_options[option_name].set_value(option_value)
      self.main_tab_list_dict_option_tabWidget.show()
    else:
      print('OPTION TYPE NOT YET IMPLEMENTED ', option_type)

  def remove_main_tab_option(self):
    (option_name, ok) = QtGui.QInputDialog.getItem(self,
                                self.tr("Select a global option"),
                                self.tr("Options:"),
                                self.current_remove_list_global_options,
                                editable=False)
    if ok:
      option_name = str(option_name)
      self.current_remove_list_global_options.remove(option_name)
      self.current_add_list_global_options.append(option_name)
      self.current_remove_list_global_options.sort()
      self.current_add_list_global_options.sort()

      # Buttons
      if self.current_remove_list_global_options == []:
        self.remove_main_tab_option_button.setEnabled(False)
      self.add_main_tab_option_button.setEnabled(True)

      # Remove widget
      self.current_global_options[option_name].remove_option()
      del self.current_global_options[option_name]

      if self.main_tab_list_dict_option_tabWidget.count() == 0:
        self.main_tab_list_dict_option_tabWidget.hide()

  def add_options_tab_option(self, option_name=""):
    if option_name:
      ok = True
    else:
      (option_name, ok) = QtGui.QInputDialog.getItem(self,
                                self.tr("Select an option"),
                                self.tr("Options:"),
                                self.current_add_list_options,
                                editable=False)
    if ok:
      option_name = str(option_name)
      option_ranges = []
      ranges = self.yamm_project.options.get_option_ranges(option_name)
      if "global" in ranges:
        option_ranges.append("global")
      if "category" in ranges:
        option_ranges = option_ranges + \
            self.yamm_project.options.get_categories()
      if "software" in ranges:
        option_ranges = option_ranges + self.options_software_list
      (range_name, ok) = QtGui.QInputDialog.getItem(self,
                                self.tr("Select a range"),
                                self.tr("Ranges:"),
                                option_ranges,
                                editable=False)
      if ok:
        self.add_options_tab_option_widget(str(option_name), str(range_name))

  def add_options_tab_option_widget(self, option_name, range_name,
                                    option_value = None):
    if not option_name in list(self.options_tab_dict.keys()):
      # Create a new tab for option name if needed
      option_widget = QtGui.QWidget()
      option_widget_layout = QtGui.QGridLayout()
      main_option_tab_layout = QtGui.QVBoxLayout()
      main_option_tab_layout.addLayout(option_widget_layout)
      main_option_tab_layout.addStretch(1)
      option_widget.setLayout(main_option_tab_layout)
      option_current_row = -1  
      scrollarea = QtGui.QScrollArea()
      scrollarea.setWidget(option_widget)
      scrollarea.setWidgetResizable(True)
      self.options_tab_qtab.addTab(scrollarea, option_name)
      self.options_tab_dict[option_name] = [option_widget, option_widget_layout,
                                            option_current_row]
      self.options_added_dict[option_name] = {}
      self.add_option_button_dict[option_name] = None

    # Check if option already added
    if option_name in list(self.options_added_dict.keys()):
      option_range_added_dict = self.options_added_dict[option_name]
      if range_name in list(option_range_added_dict.keys()):
        QtGui.QMessageBox.warning(
                              self, self.tr('Cannot add this option'),
                              self.tr('This option has already been added.'))
        return

    # Get option type
    self.yamm_project.begin_command()
    option_type = self.yamm_project.current_command_options.get_option_type(
                                                              str(option_name))
    option_possible_values = \
      self.yamm_project.current_command_options.get_option_possible_values(
                                                              str(option_name))
    self.yamm_project.end_command()

    # Add Widget
    option_widget        = self.options_tab_dict[option_name][0]
    option_widget_layout = self.options_tab_dict[option_name][1]
    option_current_row   = self.options_tab_dict[option_name][2]
    if self.add_option_button_dict[option_name]:
      self.add_option_button_dict[option_name].remove_button()
      option_current_row -=1
    widget_added = True
    if option_type == options.string_type:
      option_current_row += 1
      if option_possible_values is not None:
        self.options_added_dict[option_name][range_name] = \
            ComboRangeOption(option_name, option_possible_values, range_name,
                             self, None, False)
      else:
        self.options_added_dict[option_name][range_name] = \
            StringRangeOption(option_name, range_name, self)
      self.options_added_dict[option_name][range_name].add_range_option(
                                      option_widget_layout, option_current_row)
      self.options_tab_dict[option_name][2] = option_current_row
      self.options_tab_qtab.setCurrentWidget(option_widget)
      if option_value is not None:
        self.options_added_dict[option_name][range_name].set_value(option_value)
    elif option_type == bool:
      option_current_row += 1
      self.options_added_dict[option_name][range_name] = \
          BoolRangeOption(option_name, range_name, self)
      self.options_added_dict[option_name][range_name].add_range_option(
                                      option_widget_layout, option_current_row)
      self.options_tab_dict[option_name][2] = option_current_row
      self.options_tab_qtab.setCurrentWidget(option_widget)
      if option_value is not None:
        self.options_added_dict[option_name][range_name].set_value(option_value)
    elif option_type == list:
      option_current_row += 1
      self.options_added_dict[option_name][range_name] = \
          ListRangeOption(option_name, range_name, self)
      self.options_added_dict[option_name][range_name].add_range_option(
                                      option_widget_layout, option_current_row)
      self.options_tab_dict[option_name][2] = option_current_row
      self.options_tab_qtab.setCurrentWidget(option_widget)
      if option_value is not None:
        self.options_added_dict[option_name][range_name].set_value(option_value)
    else:
      widget_added = True
      print('OPTION TYPE NOT YET IMPLEMENTED ', option_type)

    if widget_added:
      option_current_row += 1
      self.add_option_button_dict[option_name] = AddOption(self, option_name)
      self.add_option_button_dict[option_name].add_button(option_widget_layout,
                                                          option_current_row)
      self.options_tab_dict[option_name][2] = option_current_row

  def add_checked_options(self, option):
    if option not in self.options_to_remove:
      self.options_to_remove.append(option)

  def remove_checked_options(self, option):
    if option in self.options_to_remove:
      self.options_to_remove.remove(option)

  def remove_options_tab_option(self):
    for option in self.options_to_remove:
      option.remove_range_option()
      del self.options_added_dict[option.name][option.option_range]
    self.options_to_remove = []

  def save_from_gui(self):
    # Check des valeurs obligatoires
    project_name = self.mandatory_options['Project Name'].get_value()
    project_files_directory = self.mandatory_options['Project Dir'].get_value()
    version = self.mandatory_options['Project Version'].get_value()
    version_flavour = self.current_global_options.get('version_flavour', '')
    if version_flavour:
      version_flavour = version_flavour.get_value()
    terminal = ''
    show_html = True
    if self.mode == 'edit':
      terminal = self.parent().terminal_combo_box.currentText()
      show_html = self.parent().checkBox_html.isChecked()

    if project_name == '' or project_files_directory == '' or version == '':
      QtGui.QMessageBox.warning(self, self.tr('Cannot save this project'),
          self.tr('Project cannot be saved because mandatory values '\
                  'are not set: Project name, Project dir and Project'\
                  ' version.'))
      return False

    # Clear project object
    self.yamm_gui_project.clear()

    # Config de l'objet projet
    self.yamm_gui_project.project_name            = project_name
    self.yamm_gui_project.project_files_directory = project_files_directory
    self.yamm_gui_project.version                 = version
    self.yamm_gui_project.version_flavour         = version_flavour
    self.yamm_gui_project.terminal                = terminal
    self.yamm_gui_project.show_html               = show_html
    #self.yamm_gui_project.create_appli            = create_appli

    self.yamm_gui_project.set_env_files(project_files_directory)

    # Options globales
    for (opt_name, opt_obj) in self.current_global_options.items():
      opt_name = str(opt_name)
      if opt_name == "version_flavour":
        continue
      if not opt_obj.isEmpty():
        opt_value = opt_obj.get_value()
        if opt_name == "command_verbose_level":
          opt_value = getattr(VerboseLevels, opt_value)
        self.yamm_gui_project.add_global_option(opt_name, opt_value)

    # Autres options
    project_categories = sorted(self.yamm_project.options.get_categories())
    for (opt_name, opt_dict) in self.options_added_dict.items():
      opt_name = str(opt_name)
      for (opt_range, opt_obj) in opt_dict.items():
        if not opt_obj.isEmpty():
          opt_value = opt_obj.get_value()
          if opt_range == "global":
            self.yamm_gui_project.add_global_option(opt_name, opt_value)
          elif opt_range in project_categories:
            self.yamm_gui_project.add_category_option(opt_range, opt_name,
                                                      opt_value)
          else:
            self.yamm_gui_project.add_software_option(opt_range, opt_name,
                                                      opt_value)

    # List of softwares
    # Get software lists
    remove_list = []
    for category_tab in list(self.categories_tab.values()):
      remove_list += category_tab.get_remove_list()

    self.yamm_gui_project.add_global_option("software_remove_list", remove_list)
    return True

  def save(self):
    ret = self.save_from_gui()
    if ret:
      # Ajout du projet...
      self.yamm_gui.add_project(self.yamm_gui_project)
  
      if self.mode == "edit":
        self.parent().update_after_edit()
  
      # To keep the edit window in front of the main window
      self.raise_()
      return True
    else:
      return False

  def save_and_close(self):
    if self.save():
      # Close
      self.close()

class StringOption(object):

  def __init__(self, name, callback=None):
    self.name = name
    self.row = -1

    self.label = QtGui.QLabel(name)
    self.line_edit = QtGui.QLineEdit()
    if name.startswith("mdp_") or  name.startswith("password_"):
      self.line_edit.setEchoMode(QtGui.QLineEdit.Password)

    if callback is not None:
      if type(callback) is not list:
        callback = [callback]
      for c in callback:
        self.line_edit.editingFinished.connect(c)

    self.main_layout = None

  def add_global_option_in_layout(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.label, row, 0)
    self.main_layout.addWidget(self.line_edit, row, 1)

  def remove_option(self):
    self.main_layout.removeWidget(self.label)
    self.main_layout.removeWidget(self.line_edit)
    self.label.setParent(None)
    self.line_edit.setParent(None)

  def set_value(self, value):
    self.line_edit.setText(value)

  def get_value(self):
    return str(self.line_edit.text())

  def isEmpty(self):
    return not self.get_value()

class IntOption(object):

  def __init__(self, name, callback=None):
    self.name = name
    self.row = -1

    self.label = QtGui.QLabel(name)
    self.spin_box = QtGui.QSpinBox()

    if callback is not None:
      if type(callback) is not list:
        callback = [callback]
      for c in callback:
        self.spin_box.editingFinished.connect(c)

    self.main_layout = None

  def add_global_option_in_layout(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.label, row, 0)
    self.main_layout.addWidget(self.spin_box, row, 1)

  def remove_option(self):
    self.main_layout.removeWidget(self.label)
    self.main_layout.removeWidget(self.spin_box)
    self.label.setParent(None)
    self.spin_box.setParent(None)

  def set_value(self, value):
    self.spin_box.setValue(value)

  def get_value(self):
    return self.spin_box.value()

  def isEmpty(self):
    return False

class StringRangeOption(StringOption):

  def __init__(self, name, option_range, parent, callback=None):
    StringOption.__init__(self, name, callback)
    self.option_range = option_range
    self.label     = QtGui.QLabel(option_range)
    self.check_box = QtGui.QCheckBox()
    self.parent = parent

    self.check_box.stateChanged.connect(self.stateChanged)

  def stateChanged(self, state):
    if self.check_box.isChecked():
      self.parent.add_checked_options(self)
    else:
      self.parent.remove_checked_options(self)

  def add_range_option(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.check_box, row, 0)
    self.main_layout.addWidget(self.label, row, 1)
    self.main_layout.addWidget(self.line_edit, row, 2)

  def remove_range_option(self):
    self.main_layout.removeWidget(self.check_box)
    self.main_layout.removeWidget(self.label)
    self.main_layout.removeWidget(self.line_edit)
    self.check_box.setParent(None)
    self.label.setParent(None)
    self.line_edit.setParent(None)

class ComboOption(object):

  def __init__(self, name, combo_list, callbacks = None, emptyItem=True):
    self.name =name
    self.callbacks = callbacks
    if self.callbacks is not None:
      if not isinstance(self.callbacks, list):
        self.callbacks = [self.callbacks]
    self.emptyItem = emptyItem
    self.row = -1

    self.label = QtGui.QLabel(name)
    self.combo = QtGui.QComboBox()

    self.set_combo_list(combo_list)

    self.main_layout = None

  def get_combo_list(self):
    return self.combo_list
  
  def set_combo_list(self, combo_list):
    self.combo_list = combo_list
    try:
      self.combo.currentIndexChanged.disconnect()
    except TypeError:
      pass
    self.combo.clear()
    if self.emptyItem:
      self.combo.addItem("")
    self.combo.addItems(self.combo_list)
    if self.callbacks is not None:
      for c in self.callbacks:
        self.combo.currentIndexChanged[QtCore.QString].connect(c)

  def add_global_option_in_layout(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.label, row, 0)
    self.main_layout.addWidget(self.combo, row, 1)
    self.main_layout.setColumnStretch(1,2)

  def remove_option(self, layout=None):
    if layout is None:
      layout = self.main_layout
    layout.removeWidget(self.label)
    layout.removeWidget(self.combo)
    self.label.setParent(None)
    self.combo.setParent(None)

  def set_value(self, value):
    self.combo.setCurrentIndex(self.combo.findText(value))

  def get_value(self):
    return str(self.combo.currentText())

  def isEmpty(self):
    return not self.get_value()

class ComboRangeOption(ComboOption):

  def __init__(self, name, combo_list, option_range, parent, callback = None,
               emtpyItem=True):
    ComboOption.__init__(self, name, combo_list, callback, emtpyItem)
    self.option_range = option_range
    self.label     = QtGui.QLabel(option_range)
    self.check_box = QtGui.QCheckBox()
    self.parent = parent

    self.check_box.stateChanged.connect(self.stateChanged)

  def stateChanged(self, state):
    if self.check_box.isChecked():
      self.parent.add_checked_options(self)
    else:
      self.parent.remove_checked_options(self)

  def add_range_option(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.check_box, row, 0)
    self.main_layout.addWidget(self.label, row, 1)
    self.main_layout.addWidget(self.combo, row, 2)
    self.main_layout.setColumnStretch(2,2)

  def remove_range_option(self):
    self.main_layout.removeWidget(self.check_box)
    self.main_layout.removeWidget(self.label)
    self.main_layout.removeWidget(self.combo)
    self.check_box.setParent(None)
    self.label.setParent(None)
    self.combo.setParent(None)

class AddOption(object):

  def __init__(self, parent, name):
    self.edit_project_window = parent
    self.name = name
    self.row = -1

    self.button = QtGui.QPushButton()
    self.button.setText(self.button.tr("Add"))
    self.main_layout = None

    # Signal and Slots
    self.button.clicked.connect(self.on_button_clicked)

  def on_button_clicked(self):
    self.edit_project_window.add_options_tab_option(self.name)

  def add_button(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.button, row, 0, 1, -1)

  def remove_button(self):
    self.main_layout.removeWidget(self.button)
    self.button.setParent(None)


class BoolOption(object):

  def __init__(self, name):
    self.name = name
    self.row = -1

    self.label       = QtGui.QLabel(name)
    self.bool_button = QtGui.QPushButton()
    self.bool_button.setText(self.bool_button.tr("OFF"))
    self.bool_button.setCheckable(True)

    self.main_layout = None

    # Signal and Slots
    self.bool_button.toggled.connect(self.toggled_button)

  def toggled_button(self):
    if self.bool_button.isChecked():
      self.bool_button.setText(self.bool_button.tr("ON"))
    else:
      self.bool_button.setText(self.bool_button.tr("OFF"))

  def add_global_option_in_layout(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.label, row, 0)
    self.main_layout.addWidget(self.bool_button, row, 1, QtCore.Qt.AlignRight)

  def remove_option(self):
    self.main_layout.removeWidget(self.label)
    self.main_layout.removeWidget(self.bool_button)
    self.label.setParent(None)
    self.bool_button.setParent(None)

  def set_value(self, value):
    if value:
      if not self.bool_button.isChecked():
        self.bool_button.click()
    else:
      if self.bool_button.isChecked():
        self.bool_button.click()

  def get_value(self):
    if self.bool_button.isChecked():
      return True
    else:
      return False

  def isEmpty(self):
    return False

class BoolRangeOption(BoolOption):

  def __init__(self, name, option_range, parent):
    BoolOption.__init__(self, name)
    self.option_range = option_range
    self.label     = QtGui.QLabel(option_range)
    self.check_box = QtGui.QCheckBox()
    self.parent = parent

    self.check_box.stateChanged.connect(self.stateChanged)

  def stateChanged(self, state):
    if self.check_box.isChecked():
      self.parent.add_checked_options(self)
    else:
      self.parent.remove_checked_options(self)

  def add_range_option(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.check_box, row, 0)
    self.main_layout.addWidget(self.label, row, 1)
    self.main_layout.addWidget(self.bool_button, row, 2, QtCore.Qt.AlignRight)

  def remove_range_option(self):
    self.main_layout.removeWidget(self.check_box)
    self.main_layout.removeWidget(self.label)
    self.main_layout.removeWidget(self.bool_button)
    self.check_box.setParent(None)
    self.label.setParent(None)
    self.bool_button.setParent(None)

class ListOption(object):

  def __init__(self, name, list_values=None, callback=None):
    self.name = name
    self.list_values = list_values
    if self.list_values is None:
      self.list_values = []
    self.callback = callback
    self.widget = uic.loadUi(os.path.join(
                                    os.path.dirname(os.path.abspath(__file__)),
                                    "option_list.ui"))

    self.tab_widget= None

    # Signals
    self.widget.qlist.activated.connect(self.manage_buttons)
    self.widget.qlist.itemSelectionChanged.connect(self.manage_buttons)
    self.widget.remove_button.clicked.connect(self.remove_software)
    self.widget.add_button.clicked.connect(self.add_software)
    self.widget.qlist.model().rowsInserted.connect(self.run_callback)
    self.widget.qlist.model().rowsRemoved.connect(self.run_callback)

#    # Start
#    self.remove_button.setEnabled(False)
#    self.qlist.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

  def run_callback(self):
    if self.callback is not None:
      self.callback(self.get_value)


  def add_software(self):
    (software, ok) = QtGui.QInputDialog.getItem(self.widget, "Add a software",
                                                "Software to add:",
                                                self.list_values,
                                                editable=False)
    if ok and software != "":
      self.widget.qlist.addItem(str(software))


  def remove_software(self):
    self.widget.qlist.takeItem(self.widget.qlist.currentRow())
    self.manage_buttons()

  def manage_buttons(self):
    self.widget.remove_button.setEnabled(self.widget.qlist.count() != 0)

  def add_global_option_in_layout(self, tab_widget):
    self.tab_widget= tab_widget
    idx = self.tab_widget.addTab(self.widget, self.name)
    self.tab_widget.setCurrentIndex(idx)

  def remove_option(self):
    self.tab_widget.removeTab(self.tab_widget.indexOf(self.widget))
    self.widget.setParent(None)

  def set_value(self, value):
    self.widget.qlist.addItems(value)

  def get_value(self):
    py_string_list = []
    for i in range(self.widget.qlist.count()):
      py_string_list.append(str(self.widget.qlist.item(i).text()))
    return py_string_list

  def isEmpty(self):
    current_val = self.get_value()
    return current_val == []


class ListRangeOption(ListOption):

  def __init__(self, name, option_range, parent):
    ListOption.__init__(self, name)
    self.option_range = option_range
    self.check_box = QtGui.QCheckBox()
    self.range_widget = QtGui.QGroupBox(self.option_range)
    self.range_widget.setLayout(self.widget.layout)
    self.parent = parent

    self.check_box.stateChanged.connect(self.stateChanged)

  def stateChanged(self, state):
    if self.check_box.isChecked():
      self.parent.add_checked_options(self)
    else:
      self.parent.remove_checked_options(self)

  def add_range_option(self, grid_layout, row):
    self.row = row
    self.main_layout = grid_layout
    self.main_layout.addWidget(self.check_box, row, 0)
    self.main_layout.addWidget(self.range_widget, row, 1, 1, 1)

  def remove_range_option(self):
    self.main_layout.removeWidget(self.check_box)
    self.main_layout.removeWidget(self.range_widget)
    self.check_box.setParent(None)
    self.range_widget.setParent(None)

class DictOption(object):

  def __init__(self, name, software_list=None):
    self.name = name
    self.software_list = software_list or []
    self.widget = uic.loadUi(os.path.join(
                                    os.path.dirname(os.path.abspath(__file__)),
                                    "option_dic.ui"))

    self.tab_widget= None

    # Signals
    self.widget.key_list.itemSelectionChanged.connect(self.manage_buttons)
    self.widget.value_list.itemSelectionChanged.connect(\
                                                    self.value_list_selection)
    self.widget.value_list.itemDoubleClicked.connect(self.edit_value)
    self.widget.remove_button.clicked.connect(self.remove_software)
    self.widget.add_button.clicked.connect(self.add_software)

    # Start
#    self.remove_button.setEnabled(False)
#    self.key_list.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
#    self.value_list.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

  def add_software(self):
    ok = True
    key = None
    while (ok and key is None):
      (key, ok) = QtGui.QInputDialog.getItem(self.widget, "Key",
                                             "Key to add:",
                                             self.software_list,
                                             editable=False)
      if ok and key:
        if self.widget.key_list.findItems(key, QtCore.Qt.MatchExactly):
          QtGui.QMessageBox.critical(self.widget, "Error", 
                                     "Key '%s' is already added"%key)
          key = None

    if ok and key:
      (value, ok) = QtGui.QInputDialog.getText(self.widget,
                                               "Value",
                                               "Value to add:")
      if ok and value:
        self.widget.key_list.addItem(key)
        self.widget.value_list.addItem(value)

  def remove_software(self):
    delete_row = self.widget.key_list.currentRow()
    self.widget.key_list.takeItem(delete_row)
    self.widget.value_list.takeItem(delete_row)
    self.manage_buttons()

  def edit_value(self):
    edit_row = self.widget.value_list.currentRow()
    item = self.widget.value_list.item(edit_row)
    (value, ok) = QtGui.QInputDialog.getText(self.widget,
                                             "Value", 
                                             "Edit value:",
                                             QtGui.QLineEdit.Normal,
                                             item.text())
    if ok and value:
      self.widget.value_list.takeItem(edit_row)
      self.widget.value_list.insertItem(edit_row, value)
      self.widget.value_list.clearSelection()
      self.widget.key_list.setCurrentRow(edit_row,
                                       QtGui.QItemSelectionModel.SelectCurrent)
      self.widget.value_list.setCurrentRow(edit_row,
                                       QtGui.QItemSelectionModel.SelectCurrent)

  def value_list_selection(self):
    if self.widget.key_list.count() == 0:
      self.widget.remove_button.setEnabled(False)
    else:
      self.widget.remove_button.setEnabled(True)
    self.widget.key_list.setCurrentRow(self.widget.value_list.currentRow(),
                                       QtGui.QItemSelectionModel.SelectCurrent)

  def manage_buttons(self):
    self.widget.remove_button.setEnabled(self.widget.key_list.count() != 0)
    self.widget.value_list.setCurrentRow(self.widget.key_list.currentRow(),
                                       QtGui.QItemSelectionModel.SelectCurrent)

  def add_global_option_in_layout(self, tab_widget):
    self.tab_widget= tab_widget
    idx = self.tab_widget.addTab(self.widget, self.name)
    self.tab_widget.setCurrentIndex(idx)

  def remove_option(self):
    self.tab_widget.removeTab(self.tab_widget.indexOf(self.widget))
    self.widget.setParent(None)

  def set_value(self, value):
    for (key, val) in value.items():
      self.widget.key_list.addItem(key)
      self.widget.value_list.addItem(val)

  def get_value(self):
    py_string_dict = {}
    for i in range(self.widget.key_list.count()):
      py_string_dict[str(self.widget.key_list.item(i).text())] = \
          str(self.widget.value_list.item(i).text())
    return py_string_dict

  def isEmpty(self):
    current_val = self.get_value()
    return current_val == {}


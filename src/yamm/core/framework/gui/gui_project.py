#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from past.builtins import execfile
from builtins import zip
from builtins import str
from builtins import object
import time

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels

copyright_header = """#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) %year EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
"""
gui_file = copyright_header + """
project_name            = '%project_name'
project_files_directory = '%project_files_directory'
version                 = '%version'
version_flavour         = '%version_flavour'
terminal                = '%terminal'
notify                  = %notify
show_html               = %show_html

global_options = %global_options
category_options = %category_options
software_options = %software_options
"""
gui_file = misc.PercentTemplate(gui_file)

start_file = copyright_header + """
import sys
sys.path.append('%yamm_default_directory')
from yamm.core.base.misc import VerboseLevels
from yamm.projects.%project_class.project import Project

yamm_project = Project()

# Configuration
yamm_project.set_version('%version', '%version_flavour')
"""
start_file = misc.PercentTemplate(start_file)

global_option_str = """yamm_project.set_global_option('%name', %value)
"""
global_option_str = misc.PercentTemplate(global_option_str)

category_option_str = \
    """yamm_project.set_category_option('%category', "%name", %value)
"""
category_option_str = misc.PercentTemplate(category_option_str)

software_option_str = \
    """yamm_project.set_software_option('%software', "%name", %value)
"""
software_option_str = misc.PercentTemplate(software_option_str)

start_part_file = """
# Execution
yamm_project.print_configuration()
ret = yamm_project.start()
"""

classic_run_part_file = """
# Execution
yamm_project.print_configuration()
ret = yamm_project.start()
"""

class FrameworkYammGuiProject(object):

  def __init__(self, project_class=""):

    # YAMM gui env files
    yamm_path = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,)*4
    self.yamm_default_directory = os.path.realpath(os.path.join(*yamm_path))
    self.project_class = project_class
    self.project_name = ""
    self.project_files_directory = ""
    #self.fullfilename            = ""
    self.gui_filename = ""
    self.config_filename = ""
    self.project_filename = ""
    self.run_filename = ""

    # YAMM project configuration
    self.version = ""
    self.version_flavour = ""

    # Options
    self.global_options = {}
    self.category_options = {}
    self.software_options = {}

    # Terminal
    self.terminal = ""

    # Notifications
    self.notify = True
    self.show_html = False

  def set_env_files(self, project_files_directory, project_name=""):
    if project_files_directory:
      if project_name == "":
        project_name = self.project_name
      else:
        self.project_name = project_name
      self.project_files_directory = project_files_directory
      suffixe = ""
      if self.project_class:
        suffixe = "-" + self.project_class
      self.gui_filename = os.path.join(self.project_files_directory,
                                  project_name + suffixe + "-yamm_gui.py")
      self.config_filename = os.path.join(self.project_files_directory,
                                  project_name + suffixe + "-config.py")
      self.project_filename = os.path.join(self.project_files_directory,
                                  project_name + suffixe + "-start.py")
      self.run_filename = os.path.join(self.project_files_directory,
                                  project_name + suffixe + "-classic-run.py")

  def clear(self):
    self.project_name = ""
    self.project_files_directory = ""
    self.gui_filename = ""
    self.config_filename = ""
    self.project_filename = ""
    self.run_filename = ""
    self.version = ""
    self.version_flavour = ""
    self.global_options = {}
    self.category_options = {}
    self.software_options = {}
    self.terminal = ""
    self.notify = True
    self.show_html = False

  def add_global_option(self, name, value):
    self.global_options[name] = value

  def add_category_option(self, category, name, value):
    if not name in list(self.category_options.keys()):
      self.category_options[name] = []
    if [category , value] not in self.category_options[name]:
      self.category_options[name].append([category, value])

  def add_software_option(self, software, name, value):
    if not name in list(self.software_options.keys()):
      self.software_options[name] = []
    if [software, value] not in self.software_options[name]:
      self.software_options[name].append([software, value])

  def create_gui_project_file(self):
    filetext = gui_file.substitute(
        project_name=self.project_name,
        project_files_directory=self.project_files_directory,
        version=self.version,
        version_flavour=self.version_flavour,
        terminal=self.terminal,
        notify=self.notify,
        show_html=self.show_html,
        global_options=str(self.global_options),
        category_options=str(self.category_options),
        software_options=str(self.software_options),
        year=time.strftime("%Y", time.localtime()))

    with open(self.gui_filename, 'w') as gui_project_file:
      gui_project_file.write(filetext)

  def get_start_text_project_files(self):
    filetext = start_file.substitute(
        yamm_default_directory=self.yamm_default_directory,
        version=self.version,
        version_flavour=self.version_flavour,
        project_class=self.project_class,
        year=time.strftime("%Y", time.localtime()))

    # Add Options
    for name, value in self.global_options.items():
      insert_value = value
      if name == "command_verbose_level":
        insert_value = "VerboseLevels.{0}".format(
                                  VerboseLevels.reverse_mapping[insert_value])
      elif isinstance(insert_value, (type(''), str)):
        insert_value = """'""" + insert_value + """'"""
      filetext += global_option_str.substitute(name=name, value=insert_value)
    for name, value_list in self.category_options.items():
      for values in value_list:
        insert_value = values[1]
        if isinstance(insert_value, (type(''), str)):
          insert_value = """'""" + insert_value + """'"""
        filetext += category_option_str.substitute(category=values[0],
                                                   name=name,
                                                   value=insert_value)
    for name, value_list in self.software_options.items():
      for values in value_list:
        insert_value = values[1]
        if isinstance(insert_value, (type(''), str)):
          insert_value = """'""" + insert_value + """'"""
        filetext += software_option_str.substitute(software=values[0],
                                                   name=name,
                                                   value=insert_value)
    return filetext

  def create_yamm_config_project_file(self):
    filetext = self.get_start_text_project_files()
    with open(self.config_filename, 'w') as config_project_file:
      config_project_file.write(filetext)

  def create_yamm_project_files(self):

    # Start file
    filetext = self.get_start_text_project_files()
    filetext += start_part_file
    with open(self.project_filename, 'w') as project_file:
      project_file.write(filetext)

    # classic run file
    filetext = self.get_start_text_project_files()
    filetext += classic_run_part_file
    with open(self.run_filename, 'w') as project_file:
      project_file.write(filetext)

  def load_project(self, filename):
    exec_dict = {}
    execfile(filename, exec_dict)
    
    # Get Data
    self.project_name = exec_dict.get("project_name", "")
    self.set_env_files(exec_dict.get("project_files_directory", ""))

    self.version = exec_dict.get("version", "")
    self.version_flavour = exec_dict.get("version_flavour", "")
    self.terminal = exec_dict.get("terminal", "")
    self.notify = exec_dict.get("notify", True)
    self.show_html = exec_dict.get("show_html", False)
    self.global_options = exec_dict.get("global_options", {})
    self.category_options = exec_dict.get("category_options", {})
    self.software_options = exec_dict.get("software_options", {})
    
    # version_flavour option is removed because it is saved apart from the
    # options dicts.
    try:
      self.version_flavour = self.global_options.pop("version_flavour")
    except KeyError:
      pass
    # Executor mode changes
    # See commit 7fd58d4 (refactor-executor-modes)
    self.fix_option('executor_mode', 'default_executor_mode',
                    {'update':'keep_if_no_diff',
                     'full_compile':'build',
                     'only_compile':'incremental_compile',
                     'install_binary':'install_from_binary_archive'})
    
    #return exec_dict
      
  def fix_option(self, optionname, newoptionname=None, optionmap=None):
    """
    This method updates options when name and/or possible values change.
    If 'optionmap' is None and 'newoptionname' is an empty string, then the 
    option 'optionname' is deleted.
    
    :param optionname: Name of the option.
    :type optionname: string
    :param newoptionname: New name of the option.
    :type newoptionname: string
    :param optionmap: Mapping from old to new values.
    :type optionmap: dict
    """
    if optionmap and newoptionname is None:
      print('Error: new option name missing')
      return
      
    # Update needed is a new option name or a new/old options possible values
    # mapping is given
    if newoptionname or optionmap:
      # Global options
      try:
        value = self.global_options[optionname]
        if not optionmap and newoptionname:
          self.global_options[newoptionname] = value
          print('"{0}/{1}" updated to "{2}/{1}" in global options'\
                .format(optionname, value, newoptionname))

        if optionmap and value in optionmap:
          new_value = optionmap[value]
          # Case where option name change
          if newoptionname:
              self.global_options[newoptionname] = new_value
              print('"{0}/{1}" updated to "{2}/{3}" in global options'\
                    .format(optionname, value, newoptionname, new_value))
          else:
              self.global_options[optionname] = new_value
              print('"{0}/{1}" updated to "{0}/{2}" in global options'\
                    .format(optionname, value, new_value))
      except KeyError:
        pass
      
      # Category and software options
      for dict_name, options_dict in zip(('category', 'software'),
                                         (self.category_options,
                                          self.software_options)):
        try:
          values = options_dict[optionname]
          if newoptionname and newoptionname != optionname:
            if not optionmap:
              options_dict[newoptionname] = list(options_dict[optionname])
              print('"{0}" updated to "{1}" for {2} options'\
                    .format(optionname, newoptionname, dict_name))
            else:
              options_dict[newoptionname] = []
          if optionmap:
            for category_value in values:
              category, value = category_value
              if value in optionmap:
                new_value = optionmap[value]
                # Case where option name change
                if newoptionname:
                  options_dict[newoptionname].append([category, new_value])
                  print('"{0}/{1}" updated to "{2}/{3}" for {4}'\
                        .format(optionname, value, newoptionname, new_value,
                                category))
                else:
                  options_dict[optionname].append([category, new_value])
                  options_dict[optionname].remove(category_value)
                  print('"{0}/{1}" updated to "{0}/{2}" for {3}'\
                        .format(optionname, value, new_value, category))
        except KeyError:
          pass
      
    if newoptionname is not None:
      for options_dict in (self.global_options,
                           self.category_options,
                           self.software_options):
        try:
          options_dict.pop(optionname)
        except KeyError:
          pass
  

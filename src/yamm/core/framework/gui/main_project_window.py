#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str
from builtins import range
from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4 import uic
import subprocess

from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.gui import edit_project_window
from yamm.core.framework.gui import highlighter
if py_version[0] == '3':
    from yamm.core.framework.gui import icons_rc_py3  # @UnusedImport
else:
    from yamm.core.framework.gui import icons_rc  # @UnusedImport
from yamm.core.framework.project import FrameworkProject


# This import is used by the file project_window.ui
update_mode_descriptions = {}
update_mode_descriptions['default'] = \
'Use option "default_executor_mode" to get the executor mode. \
If it is not set, executor_mode is "keep_if_no_diff".'
update_mode_descriptions['keep_if_no_diff'] = \
'Build the software if differences found in the network or in the sources.'
update_mode_descriptions['download'] = \
'Download or update the sources of the software.'
update_mode_descriptions['keep_if_installed'] = \
'Build the software if it is not already installed.'
update_mode_descriptions['download_and_build'] = \
'Download or update the sources, then build the software.'
update_mode_descriptions['build'] = \
'Fully rebuild the software (ex: build_configure; configure; make; \
make install) from local sources.'
update_mode_descriptions['incremental_compile'] = \
'Build the software incrementally (make; make install) from local sources.'
update_mode_descriptions['install_from_binary_archive'] = \
'Install the software from a binary archive.'
update_mode_descriptions['run_tests'] = 'Run tests on the software.'

class FrameworkProjectWindow(QtGui.QMainWindow):

  def __init__(self, parent, gui_project, yamm_gui):
    QtGui.QMainWindow.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            'project_window.ui'), self)
    self.soft_processed_gb_default_text = 'Update softwares'
    highlighter.Highlighter(self.command_log_widget.document(), 'bash')
    self.yamm_gui = yamm_gui
    self.gui_project = gui_project

    self.yamm_bin_directory = os.path.realpath(\
                                os.path.join(os.path.dirname(\
                                  os.path.abspath(__file__)), os.pardir, 'bin'))

    self.init_widgets()
    # Update software to be processed
    self.update_processed_list()

  def init_widgets(self):
    # Init
    title = '{0} Project: {1}'.format(self.gui_project.project_class.upper(),
                                      self.gui_project.project_name)
    self.setWindowTitle(self.tr(title))
    self.center_gui()

    self.specific_groupBox.setVisible(False)
    self.line_specific_actions.setVisible(False)
    self.update_toolButton.setEnabled(False)

    self.softwares_model = QtGui.QStandardItemModel(0, 5, self)
    self.softwares_model.setHorizontalHeaderLabels(
        ['#', 'Software', 'Version', 'Ordered\nversion', 'Category'])
    self.soft_processed_view.setModel(self.softwares_model)
    self.soft_processed_view.clicked.connect(self.activate_update)

    self.update_toolButton.setIcon(QtGui.QIcon(QtGui.qApp.style()\
                             .standardIcon(QtGui.QStyle.SP_DialogApplyButton)))
    self.clear_command_log_button.setIcon(QtGui.QIcon(QtGui.qApp.style()\
                             .standardIcon(QtGui.QStyle.SP_DialogResetButton)))

    self.edit_button = self.buttonBox.button(self.buttonBox.Open)
    self.edit_button.setText(self.tr(u'Edit Project'))
    self.edit_button.clicked.connect(self.edit_project)

    # # Widget configuration
    term_index = self.terminal_combo_box.findText(self.gui_project.terminal,
                                                  QtCore.Qt.MatchFixedString)
    if term_index != -1:
      self.terminal_combo_box.setCurrentIndex(term_index)
    else:
      self.terminal_combo_box.setCurrentIndex(0)
    self.checkBox_notifications.setChecked(self.gui_project.notify)
    self.checkBox_html.setChecked(self.gui_project.show_html)
    self.command_log_widget.setReadOnly(True)
    self.splitter.setSizes([1000, 5])
    self.on_update_mode_change()

  def display_help(self):
    yammdir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
    docdir = list(yammdir) + ['doc']
    docdir = os.path.realpath(os.path.join(*docdir))
    index = os.path.join(docdir, 'build', 'html', 'index.html')
    if os.path.exists(index):
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(index))
    else:
        QtGui.QMessageBox.warning(self, self.tr('YAMM help'),
                                  self.tr('Impossible to find help.\n'
                                          'Run make html in %s' % docdir))

  def activate_update(self,):
    soft_processed_gb_text = self.soft_processed_gb_default_text
    nb_selectedRows = len(set([index.row() for index in self.soft_processed_view.selectedIndexes()]))
    if nb_selectedRows:
      soft_processed_gb_text += ' (%d selected)' % nb_selectedRows
    self.soft_processed_gb.setTitle(soft_processed_gb_text)
    self.update_toolButton.setEnabled(nb_selectedRows > 0)

  def close(self):
    QtGui.QMainWindow.close(self)

  def center_gui(self):
    screen = QtGui.QDesktopWidget().screenGeometry()
    size = self.geometry()
    self.move((screen.width() - size.width()) // 2,
              (screen.height() - size.height()) // 2)

  def get_yamm_project_edit_window(self):
    return edit_project_window.FrameworkEditProjectWindow(self, self.yamm_gui,
                                                      'edit', self.gui_project)

  def edit_project(self):
    ed_window = self.get_yamm_project_edit_window()
    ed_window.show()

  def update_after_edit(self):
    # self.update_software_list()
    sortedColumn = self.soft_processed_view.horizontalHeader()\
                                              .sortIndicatorSection()
    self.update_processed_list()
    self.soft_processed_view.sortByColumn(sortedColumn,
                                          QtCore.Qt.AscendingOrder)
    self.update_toolButton.setEnabled(False)

  def clean_command_log(self):
    self.command_log_widget.clear()

  @QtCore.pyqtSlot()
  def on_command_mode_button_toggled(self, status):
    self.command_mode_button.setText('Print command' if status else 'Launch command')

  @QtCore.pyqtSlot()
  def on_start_button_clicked(self):
    self.start_command()

  @QtCore.pyqtSlot()
  def on_start_offline_button_clicked(self):
    self.start_offline_command()

  @QtCore.pyqtSlot()
  def on_start_from_scratch_button_clicked(self):
    self.start_from_scratch_command()

  @QtCore.pyqtSlot()
  def on_clean_src_button_clicked(self):
    self.clean_src_command()

  @QtCore.pyqtSlot()
  def on_clean_build_button_clicked(self):
    self.clean_build_command()

  @QtCore.pyqtSlot()
  def on_clean_install_button_clicked(self):
    self.clean_install_command()

  @QtCore.pyqtSlot()
  def on_download_button_clicked(self):
    print("on_download_button_clicked called")
    self.download_command()

  @QtCore.pyqtSlot(str)
  def on_terminal_combo_box_currentIndexChanged(self, txt=''):
    self.update_terminal(txt)

  @QtCore.pyqtSlot()
  def on_checkBox_notifications_toggled(self, status):
    self.update_notify(status)

  @QtCore.pyqtSlot()
  def on_checkBox_html_toggled(self, status):
    self.update_show_html(status)

  def start_command(self, command_name='start'):
    command = self.prepare_command(command_name)
    self.exec_command(command)

  def start_offline_command(self, command_name='start_offline'):
    command = self.prepare_command(command_name)
    if QtGui.QMessageBox.warning(
        self,
        self.tr('Offline compilation'),
        self.tr('This action will compile (from local sources) the softwares '
                'of the project that are not already installed. Continue ?'),
        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
      self.exec_command(command)
    else:
      self.print_command(command + '\nCommand ABORTED')

  def start_from_scratch_command(self, command_name='start_from_scratch'):
    command = self.prepare_command(command_name)
    if QtGui.QMessageBox.warning(
        self,
        self.tr('Compilation from scratch'),
        self.tr('This action will delete all install directories and '
                'recompile all softwares of the project ! Continue ?'),
        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
      self.exec_command(command)
    else:
      self.print_command(command + '\nCommand ABORTED')

  def clean_src_command(self):
    command = self.prepare_command('clean_sources')
    if QtGui.QMessageBox.warning(
        self,
        self.tr('Delete source directories'),
        self.tr('This action will delete all source directories ! Continue ?'),
        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
      self.exec_command(command)
    else:
      self.print_command(command + '\nCommand ABORTED')

  def clean_build_command(self):
    command = self.prepare_command('clean_builds')
    if QtGui.QMessageBox.warning(
        self,
        self.tr('Delete build directories'),
        self.tr('This action will delete all build directories ! Continue ?'),
        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
      self.exec_command(command)
    else:
      self.print_command(command + '\nCommand ABORTED')

  def clean_install_command(self):
    command = self.prepare_command('clean_installs')
    if QtGui.QMessageBox.warning(
        self,
        self.tr('Delete install directories'),
        self.tr('This action will delete all install directories ! Continue ?'),
        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
      self.exec_command(command)
    else:
      self.print_command(command + '\nCommand ABORTED')

  def download_command(self):
    command = self.prepare_command('download')
    self.exec_command(command)

  def update_terminal(self, txt=''):
    if not txt:
        txt = str(self.terminal_combo_box.currentText())
    self.gui_project.terminal = txt
    self.gui_project.create_gui_project_file()

  def update_notify(self, status=None):
    if status is None:
        status = self.checkBox_notifications.isChecked()
    self.gui_project.notify = status
    self.gui_project.create_gui_project_file()

  def update_show_html(self, status=None):
    if status is None:
        status = self.checkBox_html.isChecked()
    self.gui_project.show_html = status
    self.gui_project.create_gui_project_file()

  def get_command_script(self):
    return os.path.join(self.yamm_bin_directory, 'yamm_command.py')

  def prepare_command(self, command_name):
    command = 'python '
    command += self.get_command_script() + ' '
    command += '-c ' + self.gui_project.config_filename + ' '
    if self.checkBox_html.isChecked():
      command += ' --show-html'
    if self.checkBox_notifications.isChecked():
      command += ' -o "La commande \'__CMD__\' est terminée avec'
      command += ' <b>succès</b>"'
      command += ' -k "La commande \'__CMD__\' est terminée en <b>échec</b>"'
    command += ' ' + command_name
    return command

  def exec_command(self, command, args=None, kw=None):
    self.print_command(command, args, kw)
    if not self.command_mode_button.isChecked():
      self.launch_command(command, args, kw)

  def print_command(self, command, args=None, kw=None):
    if args is None:
      args = []
    if kw is None:
      kw = {}
    self.command_log_widget.setReadOnly(False)
    self.command_log_widget.moveCursor(QtGui.QTextCursor.Start)
    cursor = self.command_log_widget.textCursor()
    cursor.insertText(command)
    if args:
      cursor.insertText(' ' + ' '.join(str(arg) for arg in args) + '\n')
    if kw:
      cursor.insertText(' ' + ' '.join('--{0}="{1}"'.\
                                    format(k, v) for k, v in list(kw.items())) + '\n')
    cursor.insertText('\n')
    self.command_log_widget.setReadOnly(True)

  def launch_command(self, command, args=None, kw=None):
    def quoted_args(args=None):
      string = ''
      if args:
        for arg in args:
          string += ' "{0}"'.format(arg)
      return string
    def quoted_kw(kw=None):
      string = ''
      if kw:
        for k, v in list(kw.items()):
          string += ' --{0}={1}'.format(k, v)
      return string

    term_command = str(self.terminal_combo_box.currentText())
    exec_command = []
    if term_command.startswith('gnome-t'):
      term_command = 'gnome-terminal'
      exec_command = [term_command, '-e']
      exec_command += ["/bin/bash -c '" + command + quoted_args(args) +
                       quoted_kw(kw) + "; cat'"]
    elif term_command.startswith('xterm'):
      exec_command = [term_command, '-hold', '-e']
      exec_command += [command + quoted_args(args) + quoted_kw(kw)]
    elif term_command.startswith('konsole'):
      exec_command = [term_command, '--hold', '-e']
      exec_command += ['/bin/bash', '-c', command + quoted_args(args) +
                       quoted_kw(kw)]
    # print('exec_command: ',exec_command)
    subprocess.Popen(exec_command)

  def update_processed_list(self):
    self.softwares_model.removeRows(0, self.softwares_model.rowCount())

    yamm_proj = self.get_project_from_gui()
    yamm_proj.set_global_option('command_verbose_level',
                                   VerboseLevels.WARNING)

    # Update list of processed softwares
    yamm_proj.begin_command()
    software_mode = 'nothing'
    rank_in_process = 0
    for soft_item in yamm_proj.current_command_softwares:
      soft_item.set_command_env(yamm_proj.current_command_softwares,
                                yamm_proj.current_command_options,
                                software_mode,
                                yamm_proj.current_command_version_object,
                                yamm_proj.current_command_python_version)
      item_rank = QtGui.QStandardItem('%0*d' % (3, rank_in_process + 1))
      item_name = QtGui.QStandardItem(soft_item.name)
      item_version = QtGui.QStandardItem(soft_item.version)
      item_ordered_version = QtGui.QStandardItem(soft_item.ordered_version)
      item_category = QtGui.QStandardItem(soft_item.get_type())
      toolTipLines = []
      for line in soft_item.get_infos_as_string().split('\n'):
        if line and len(line) <= 150:
          toolTipLines.append(line)
          pass
        pass
      toolTip = '\n'.join(toolTipLines)
      item_rank.setToolTip(toolTip)
      item_name.setToolTip(toolTip)
      item_version.setToolTip(toolTip)
      self.softwares_model.insertRow(rank_in_process,
                                     [item_rank, item_name, item_version, 
                                      item_ordered_version, item_category])
      rank_in_process += 1
    yamm_proj.end_command()

  def get_empty_yamm_project(self):
    return FrameworkProject()

  def get_project_from_gui(self):
    yamm_proj = self.get_empty_yamm_project()
    yamm_proj.verbose_level = VerboseLevels.WARNING
    yamm_proj.set_version(self.gui_project.version,
                          self.gui_project.version_flavour)
    # Update global options
    for option_name, option_value in list(self.gui_project.global_options.items()):
      yamm_proj.set_global_option(option_name, option_value)

    # Update category options
    for option_name, option_processed in list(self.gui_project.category_options.\
                                            items()):
      for i in range(len(option_processed)):
        category_name = option_processed[i][0]
        option_value = option_processed[i][1]
        yamm_proj.options.set_category_option(category_name,
                                              option_name,
                                              option_value)

    # Update software options
    for option_name, option_processed in list(self.gui_project.software_options.\
                                            items()):
      for i in range(len(option_processed)):
        software_name = option_processed[i][0]
        option_value = option_processed[i][1]
        yamm_proj.options.set_software_option(software_name,
                                              option_name,
                                              option_value)
    return yamm_proj

  def show_software(self, currentSoftwareItem):
    yamm_proj = self.get_project_from_gui()
    yamm_proj.begin_command()
    # TO_IMPLEMENT
    print('Showing software')
    yamm_proj.end_command()

  def get_build_software_script(self):
    return os.path.join(self.yamm_bin_directory, 'yamm_build_software.py')

  def run_update(self):
    selected_rows = set([idx.row() for idx in self.soft_processed_view.\
                                                          selectedIndexes()])
    software_name_list = [str(self.softwares_model.item(row, 1).text()) \
                                                    for row in selected_rows]

    command = 'python '
    command += self.get_build_software_script() + ' '
    command += '-c ' + self.gui_project.config_filename + ' '
    command += '--' + str(self.mode_comboBox.currentText())
    software_names = ' '.join(software_name_list)
    if self.checkBox_html.isChecked():
      command += ' --show-html'
    if self.checkBox_notifications.isChecked():
      command += u' -o "La commande Update {0} est terminée avec'.format(software_names)
      command += u' <b>succès</b>"'
      command += u' -k "La commande Update {0} est terminée en'.format(software_names)
      command += u' <b>échec</b>"'
    command += ' ' + software_names

    self.exec_command(command)

  def on_update_mode_change(self, mode='default'):
    mode = str(mode)
    self.update_mode_description.clear()
    if mode in list(update_mode_descriptions.keys()):
      self.update_mode_description.setText(update_mode_descriptions[mode])

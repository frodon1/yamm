#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str
import copy
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4 import uic

from yamm.core.framework.project import FrameworkProject

class ManageSoftwareList(QtGui.QWizardPage):

  def __init__(self, parent, project = None):
    QtGui.QWizardPage.__init__(self, parent)
#     self.setTitle(self.tr("Step 4/4"))
    self.yamm_project = project or FrameworkProject()
    self.parent = parent

    # Main QTabWidget
    self.qtabwidget = QtGui.QTabWidget(self)

    # Page layout
    layout = QtGui.QVBoxLayout(self)
    layout.addWidget(self.qtabwidget)
    self.setLayout(layout)

    # Categories
    self.categories_tab = {}
    for category in sorted(self.yamm_project.options.get_categories()):

      self.categories_tab[category] = ManageListOfSoftwares(self.parent,
                                                            self.yamm_project,
                                                            category)
      self.qtabwidget.addTab(self.categories_tab[category], category)

    self.version = ""
    self.version_flavour = ""

  def initializePage(self):
    current_version = str(self.parent.version_comboBox.currentText())
    current_flavour = str(self.parent.flavour_comboBox.currentText())
    update = False
    if current_version != self.version:
      update = True
    elif current_flavour != self.version_flavour:
      update = True
    if update:  
      self.version = current_version
      self.version_flavour = current_flavour
      self.yamm_project.set_version(self.version, self.version_flavour)
      for category_tab in list(self.categories_tab.values()):
        category_tab.init_list()

  def validatePage(self):
    # Get software lists
    for category_tab in list(self.categories_tab.values()):
      # Add them to the father
      self.parent.add_remove_list(category_tab.get_remove_list())

    return True

class ManageListOfSoftwares(QtGui.QWidget):

  def __init__(self, parent, yamm_project, type_of_software):
    QtGui.QWidget.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            "manage_software_list.ui"), self)
    self.list_model = QtGui.QStringListModel(self)
    self.removed_list_model = QtGui.QStringListModel(self)
    self.qlist.setModel(self.list_model)
    self.qlist_removed.setModel(self.removed_list_model)
    self.yamm_project = yamm_project
    self.type_of_software = type_of_software

  def add_list(self, list_of_items):
    self.full_list = copy.deepcopy(list_of_items)
    self.list_model.setStringList(list_of_items)
    self.removed_list_model.setStringList([])

  def set_list(self, list_of_items):
    self.list_model.setStringList(list_of_items)
    self.removed_list_model.setStringList(self.get_remove_list())

  def add_software(self):
    selected_items = self.qlist_removed.selectedIndexes()
    selected_soft = []
    for item in selected_items:
      data = str(self.removed_list_model.data(item,
                                              QtCore.Qt.EditRole).toString())
      selected_soft.append(data)
    self.set_list(self.get_list() + selected_soft)
    self.removed_list_model.sort(0)
    self.list_model.sort(0)

  def remove_software(self):
    selected_items = self.qlist.selectedIndexes()
    selected_soft = []
    for item in selected_items:
      data = str(self.list_model.data(item,
                                  QtCore.Qt.EditRole).toString())
      selected_soft.append(data)
    self.set_list(list(set(self.get_list()).difference(selected_soft)))
    self.removed_list_model.sort(0)
    self.list_model.sort(0)

  def manage_add_button(self, item_selected):
    self.add_button.setEnabled(self.qlist_removed.selectedIndexes() != [])
    self.remove_button.setEnabled(False)

  def manage_remove_button(self, item_selected):
    self.add_button.setEnabled(False)
    self.remove_button.setEnabled(self.qlist.selectedIndexes() != [])

  def get_remove_list(self):
    qt_string_list = list(self.list_model.stringList())
    py_string_list = []
    for qt_str in qt_string_list:
      py_string_list.append(str(qt_str))
    remove_items = list(set(self.full_list).difference(py_string_list))
    remove_items.sort()
    return remove_items

  def get_list(self):
    qt_string_list = list(self.list_model.stringList())
    py_string_list = []
    for qt_str in qt_string_list:
      py_string_list.append(str(qt_str))
    return py_string_list

  def init_list(self):
    self.category_list = []
    self.get_category_list()
    self.add_list(self.category_list)

  def get_category_list(self):
    self.yamm_project.begin_command()
    for software in self.yamm_project.current_command_softwares:
      if software.get_type() == self.type_of_software:
        self.category_list.append(software.name)
    self.yamm_project.end_command()
    self.category_list.sort()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str
from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4 import uic

from yamm.core.base import misc

from yamm.core.framework.gui import basic_wizard
from yamm.core.framework.gui import gui_project
from yamm.core.framework.gui import main_project_window
from yamm.core.framework.gui import edit_project_window
#from yamm.core.framework.gui import project_window_rc

class FrameworkIntroWidget(QtGui.QMainWindow):

  def __init__(self, yamm_gui):
    QtGui.QMainWindow.__init__(self)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            "intro.ui"), self)
    self.yamm_gui = yamm_gui

    self.load_project_main_windows = {}

    self.style_comboBox.addItems(list(QtGui.QStyleFactory.keys()))

    # Rigth Recent Projects List
    self.recent_load_button = self.buttonBox.button(self.buttonBox.Apply)
    self.recent_load_button.setText(self.tr("Load recent"))
    self.recent_load_button.setEnabled(False)

    # Rigth Recent Projects List
    self.recent_remove_button = self.buttonBox.button(self.buttonBox.Discard)
    self.recent_remove_button.setText(self.tr("Remove recent"))
    self.recent_remove_button.setEnabled(False)

    # Signals and Slots
    self.buttonBox.rejected.connect(self.close)
    self.recent_remove_button.clicked.connect(self.recent_remove)
    self.recent_load_button.clicked.connect(self.recent_load)

    self.buttonBox.helpRequested.connect(self.display_help)

    #self.update_style(self.style_comboBox.itemText(0))

    # TODO: fix the display problem when changing the style
    self.style_comboBox.hide()
  
  def display_help(self):
    docdir = list(yamm_default_dir) + ['doc', ]
    docdir = os.path.realpath(os.path.join(*docdir))
    index = os.path.join(docdir, 'build', 'html', 'index.html')
    if os.path.exists(index):
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(index))
    else:
        QtGui.QMessageBox.warning(self, self.tr('YAMM help'),
                                  self.tr('Impossible to find help.\n'
                                          'Run make html in %s' % docdir))

  def close(self):
    if QtGui.QMessageBox.question(self, self.tr('Quit YAMM'),
                          self.tr('Quit YAMM ?'),
                          QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                          QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
      QtGui.QMainWindow.close(self)

  def update_style(self, style):
    QtGui.QApplication.setStyle(QtGui.QStyleFactory.create(style))
    QtGui.QApplication.setPalette(QtGui.QApplication.style().standardPalette())
    self.style_comboBox.update()

  def add_recent_project(self, filename):
    list_of_items = self.recent_list.findItems(filename,
                                               QtCore.Qt.MatchFixedString)
    if len(list_of_items) == 1:
      item = self.recent_list.takeItem(self.recent_list.row(list_of_items[0]))
      self.recent_list.insertItem(0, item)
    else:
      self.recent_list.insertItem(0, filename)
    self.recent_list.setCurrentItem(self.recent_list.item(0))
    self.recent_list.setMinimumWidth(max(350,
                                         self.recent_list.sizeHintForColumn(0)))
    self.adjustSize()

  def remove_recent_project(self, filename):
    list_of_items = self.recent_list.findItems(filename,
                                               QtCore.Qt.MatchFixedString)
    if len(list_of_items) == 1:
      self.recent_list.takeItem(self.recent_list.row(list_of_items[0]))
    if self.recent_list.count():
      self.recent_list.setCurrentItem(self.recent_list.item(0))
    self.recent_list.setMinimumWidth(max(350,
                                         self.recent_list.sizeHintForColumn(0)))
    self.adjustSize()

  def get_basic_wizard_instance(self):
    return basic_wizard.FrameworkBasicWizard(self)

  def create_basic_wizard(self):
    wizard = self.get_basic_wizard_instance()
    wizard.exec_()
    if wizard.result:
      self.yamm_gui.add_new_project_with_wizard(wizard)

  def get_advanced_wizard_instance(self):
    return edit_project_window.FrameworkEditProjectWindow(self, self.yamm_gui,
                                                          "create")

  def create_project_adv(self):
    ed_window = self.get_advanced_wizard_instance()
    ed_window.show()

  def copy_project(self):
    project_filename = str(self.recent_list.currentItem().text())
    if not os.path.exists(project_filename):
      QtGui.QMessageBox.warning(self, self.tr("Unable to load project"), 
          self.tr("Project file {0} is not found,"\
                  " removing it from recent list".format(project_filename)))
      self.yamm_gui.remove_recent_project(project_filename)
    new_project = self.get_empty_yamm_gui_project()
    new_project.load_project(project_filename)
    project_files_directory = new_project.project_files_directory
    project_name = new_project.project_name
    new_project_name = project_name
    while new_project_name == project_name:
      (new_project_name, ok) = QtGui.QInputDialog.getText(self,
          self.tr("Copy Project {0}".format(project_name)),
          self.tr("Name of the new project:"),
          text=new_project_name)
      new_project_name = str(new_project_name)
      if not ok:
        break
    if ok and new_project_name != "" and new_project_name != project_name:
      new_project.set_env_files(project_files_directory, new_project_name)
      # Add project
      self.yamm_gui.add_project(new_project, create_gui_files=True)
      self.open_yamm_gui_project(new_project)

    pass

  def recent_item_selected(self):
    for button in (self.recent_load_button,
                   self.recent_remove_button,
                   self.copy_button):
      if not button.isEnabled():
        button.setEnabled(True)
      elif self.recent_list.currentRow() == -1:
        button.setEnabled(False)

  def recent_load(self):
    filename = str(self.recent_list.currentItem().text())
    if os.path.exists(filename):
      self.load_project_file(filename)
    else:
      QtGui.QMessageBox.warning(self, self.tr("Unable to load this project"),
          self.tr("Project file is not found, remove it from recent list"))
      self.yamm_gui.remove_recent_project(filename)

  def recent_remove(self):
    filename = str(self.recent_list.currentItem().text())
    self.yamm_gui.remove_recent_project(filename)

  def load_project(self):
    home_dir = os.path.expanduser("~")
    filename = QtGui.QFileDialog.getOpenFileName(self,
        self.tr("Open YAMM GUI config file"),
        home_dir,
        self.tr("Yamm Gui Python files (*{0}-yamm_gui.py)".\
                format(self.yamm_gui.yamm_class_name)))
    if filename:
      self.load_project_file(str(filename))

  def get_main_project_window(self, yamm_gui_project):
    return main_project_window.FrameworkProjectWindow(self,
                                                      yamm_gui_project,
                                                      self.yamm_gui)

  def get_empty_yamm_gui_project(self):
    return gui_project.FrameworkYammGuiProject()

  def load_project_file(self, project_filename):
    new_project = self.get_empty_yamm_gui_project()
    new_project.load_project(project_filename)
    # Add project
    self.yamm_gui.add_project(new_project, False)
    self.open_yamm_gui_project(new_project)

  def open_yamm_gui_project(self, project):
    if project.gui_filename not in list(self.load_project_main_windows.keys()):
      try:
        self.load_project_main_windows[project.gui_filename] = \
            self.get_main_project_window(project)
      except misc.LoggerException as e:
        QtGui.QMessageBox.critical(self, self.tr("Error"), e.msg)
    if project.gui_filename in list(self.load_project_main_windows.keys()):
      self.load_project_main_windows[project.gui_filename].show()
      self.load_project_main_windows[project.gui_filename].raise_()


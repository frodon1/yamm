<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>AddOption</name>
    <message>
        <location filename="edit_project_window.py" line="887"/>
        <source>Add</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BoolOption</name>
    <message>
        <location filename="edit_project_window.py" line="925"/>
        <source>OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="923"/>
        <source>ON</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="getItemDlg.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="getItemDlg.ui" line="23"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DictOption</name>
    <message>
        <location filename="edit_project_window.py" line="1109"/>
        <source>Key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1109"/>
        <source>Key to add:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1129"/>
        <source>Value</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1113"/>
        <source>Value to add:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1129"/>
        <source>Edit value:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="option_list.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="manage_software_list.ui" line="20"/>
        <source>Added softwares</source>
        <translation></translation>
    </message>
    <message>
        <location filename="manage_software_list.ui" line="70"/>
        <source>Removed softwares</source>
        <translation></translation>
    </message>
    <message>
        <location filename="option_list.ui" line="38"/>
        <source>Add</source>
        <translation></translation>
    </message>
    <message>
        <location filename="option_list.ui" line="48"/>
        <source>Remove</source>
        <translation></translation>
    </message>
    <message>
        <location filename="option_dic.ui" line="22"/>
        <source>Keys:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="option_dic.ui" line="29"/>
        <source>Values:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkBasicIntro</name>
    <message>
        <location filename="basic_wizard.py" line="116"/>
        <source>Project Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="124"/>
        <source>Project Files Directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="125"/>
        <source>Browse...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="155"/>
        <source>Choose directory</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkBasicOptions</name>
    <message>
        <location filename="basic_wizard.py" line="188"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="190"/>
        <source>Version:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="200"/>
        <source>Flavour:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="213"/>
        <source>Directories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="215"/>
        <source>Top Directory:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="219"/>
        <source>Version Directory:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="223"/>
        <source>Archives Directory:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="228"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="232"/>
        <source>Terminal:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="236"/>
        <source>Parallel Compilation Flag:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="240"/>
        <source>Continue on failure:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="244"/>
        <source>Clean sources if success:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="248"/>
        <source>Clean builds if success:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="324"/>
        <source>ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="326"/>
        <source>OFF</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkBasicRemotes</name>
    <message>
        <location filename="basic_wizard.py" line="403"/>
        <source>Softwares remote modes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="440"/>
        <source>archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="438"/>
        <source>remote</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkBasicWizard</name>
    <message>
        <location filename="basic_wizard.py" line="36"/>
        <source>Create a YAMM project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="61"/>
        <source>Step {0}/{1}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkEditProjectWindow</name>
    <message>
        <location filename="edit_project_window.py" line="83"/>
        <source>Edit project: {0}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="85"/>
        <source>Create a new project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="93"/>
        <source>Save</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="94"/>
        <source>Save and Close</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="95"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="104"/>
        <source>Main</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="105"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="106"/>
        <source>Softwares</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="183"/>
        <source>Add Global Option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="184"/>
        <source>Remove Global Option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="258"/>
        <source>Add Option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="259"/>
        <source>Remove Selected Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="459"/>
        <source>Select a global option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="485"/>
        <source>Options:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="485"/>
        <source>Select an option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="497"/>
        <source>Select a range</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="497"/>
        <source>Ranges:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="525"/>
        <source>Cannot add this option</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="525"/>
        <source>This option has already been added.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="607"/>
        <source>Cannot save this project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="607"/>
        <source>Project cannot be saved because mandatory values are not set: Project Name, Project Dir and Project Version.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkIntroWidget</name>
    <message>
        <location filename="start_widget.py" line="49"/>
        <source>Load recent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="49"/>
        <source>Remove recent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="68"/>
        <source>Quit YAMM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="68"/>
        <source>Quit YAMM ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="118"/>
        <source>Unable to load project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="118"/>
        <source>Project file {0} is not found, removing it from recent list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="127"/>
        <source>Copy Project {0}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="127"/>
        <source>Name of the new project:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="155"/>
        <source>Unable to load this project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="155"/>
        <source>Project file is not found, remove it from recent list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="165"/>
        <source>Open YAMM GUI config file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="165"/>
        <source>Yamm Gui Python files (*{0}-yamm_gui.py)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="start_widget.py" line="188"/>
        <source>Error</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkProjectGui</name>
    <message>
        <location filename="../project_gui.py" line="78"/>
        <source>YAMM for {0}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="80"/>
        <source>YAMM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="182"/>
        <source>Cannot add project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="175"/>
        <source>Projet specified named directory is not a directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="182"/>
        <source>Projet specified named directory cannot be created</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkProjectWindow</name>
    <message>
        <location filename="main_project_window.py" line="139"/>
        <source>Offline compilation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="139"/>
        <source>This action will compile all softwares (from archives) of the project ! Continue ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="148"/>
        <source>Compilation from scratch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="148"/>
        <source>This action will delete all install directories and recompile all softwares of the project ! Continue ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="157"/>
        <source>Delete source directories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="157"/>
        <source>This action will delete all source directories ! Continue ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="166"/>
        <source>Delete build directories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="166"/>
        <source>This action will delete all build directories ! Continue ?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="175"/>
        <source>Delete install directories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="175"/>
        <source>This action will delete all install directories ! Continue ?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ListOption</name>
    <message>
        <location filename="edit_project_window.py" line="1019"/>
        <source>Add a software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1019"/>
        <source>Software to add:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="intro.ui" line="14"/>
        <source>YAMM_TITLE</source>
        <translation>YAMM</translation>
    </message>
    <message>
        <location filename="intro.ui" line="27"/>
        <source>CREATE_PROJECT_BASIC</source>
        <translation>Create a YAMM Project (Basic)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="34"/>
        <source>CREATE_PROJECT_ADVANCED</source>
        <translation>Create a YAMM Project (Advanced)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="51"/>
        <source>LOAD_PROJECT</source>
        <translation>Load a YAMM Project</translation>
    </message>
    <message>
        <location filename="intro.ui" line="44"/>
        <source>COPY_PROJECT</source>
        <translation>Copy a YAMM Project</translation>
    </message>
    <message>
        <location filename="intro.ui" line="75"/>
        <source>Recent projects</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="34"/>
        <source>YAMM Information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="40"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Softwares to be processed&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="85"/>
        <source>Update Software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="95"/>
        <source>update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="100"/>
        <source>full-compile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="105"/>
        <source>incremental</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="110"/>
        <source>from-scratch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="115"/>
        <source>install-binary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="120"/>
        <source>run-tests</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="128"/>
        <source>Update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="161"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Used Commands Logs&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="257"/>
        <source>Clean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="203"/>
        <source>YAMM Project Commands</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="213"/>
        <source>Actions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="219"/>
        <source>Start</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="226"/>
        <source>Start (offline mode)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="233"/>
        <source>Start From Scratch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="247"/>
        <source>Download</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="263"/>
        <source>Clean Sources</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="270"/>
        <source>Clean Builds</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="277"/>
        <source>Clean Installs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="287"/>
        <source>Specific actions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="294"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="300"/>
        <source>Launch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="317"/>
        <source>xterm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="322"/>
        <source>konsole</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="327"/>
        <source>gnome-terminal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="335"/>
        <source>Show notifications</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="quit_message_box.py" line="32"/>
        <source>Quit YAMM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="quit_message_box.py" line="32"/>
        <source>Are you sure to quit?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>update_widget</name>
    <message>
        <location filename="update_dock.ui" line="20"/>
        <source>Update Dock</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="27"/>
        <source>Select Software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="39"/>
        <source>Select Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="49"/>
        <source>default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="54"/>
        <source>update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="59"/>
        <source>full-compile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="64"/>
        <source>incremental</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="69"/>
        <source>from-scratch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="74"/>
        <source>install-binary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="79"/>
        <source>run-tests</source>
        <translation></translation>
    </message>
</context>
</TS>

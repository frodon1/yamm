<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AddOption</name>
    <message>
        <location filename="edit_project_window.py" line="887"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
</context>
<context>
    <name>BoolOption</name>
    <message>
        <location filename="edit_project_window.py" line="925"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="923"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="getItemDlg.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="getItemDlg.ui" line="23"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DictOption</name>
    <message>
        <location filename="edit_project_window.py" line="1109"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1109"/>
        <source>Key to add:</source>
        <translation>Clé à ajouter :</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1129"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1113"/>
        <source>Value to add:</source>
        <translation>Valeur à ajouter :</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1129"/>
        <source>Edit value:</source>
        <translation>Modifier la valeur :</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="option_list.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="manage_software_list.ui" line="20"/>
        <source>Added softwares</source>
        <translation>Logiciels ajoutés</translation>
    </message>
    <message>
        <location filename="manage_software_list.ui" line="70"/>
        <source>Removed softwares</source>
        <translation>Logiciels supprimés</translation>
    </message>
    <message>
        <location filename="option_list.ui" line="38"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="option_list.ui" line="48"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="option_dic.ui" line="22"/>
        <source>Keys:</source>
        <translation>Clés :</translation>
    </message>
    <message>
        <location filename="option_dic.ui" line="29"/>
        <source>Values:</source>
        <translation>Valeurs :</translation>
    </message>
</context>
<context>
    <name>FrameworkBasicIntro</name>
    <message>
        <location filename="basic_wizard.py" line="116"/>
        <source>Project Name</source>
        <translation>Nom du projet</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="124"/>
        <source>Project Files Directory</source>
        <translation>Répertoire des fichiers du projet</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="125"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="155"/>
        <source>Choose directory</source>
        <translation>Choisir un répertoire</translation>
    </message>
</context>
<context>
    <name>FrameworkBasicOptions</name>
    <message>
        <location filename="basic_wizard.py" line="188"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="190"/>
        <source>Version:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="200"/>
        <source>Flavour:</source>
        <translation>Variante :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="213"/>
        <source>Directories</source>
        <translation>Répertoires</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="215"/>
        <source>Top Directory:</source>
        <translation>Répertoire de base :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="219"/>
        <source>Version Directory:</source>
        <translation>Répertoire des versions :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="223"/>
        <source>Archives Directory:</source>
        <translation>Répertoire des archives :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="228"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="232"/>
        <source>Terminal:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="236"/>
        <source>Parallel Compilation Flag:</source>
        <translation>Compilation parallèle :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="240"/>
        <source>Continue on failure:</source>
        <translation>Continuer après un échec :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="244"/>
        <source>Clean sources if success:</source>
        <translation>Supprimer les sources après une installation réussie :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="248"/>
        <source>Clean builds if success:</source>
        <translation>Supprimer les builds après une installation réussie :</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="324"/>
        <source>ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="326"/>
        <source>OFF</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FrameworkBasicRemotes</name>
    <message>
        <location filename="basic_wizard.py" line="403"/>
        <source>Softwares remote modes</source>
        <translation>Mode de récupération des logiciels</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="440"/>
        <source>archive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="438"/>
        <source>remote</source>
        <translation>distant</translation>
    </message>
</context>
<context>
    <name>FrameworkBasicWizard</name>
    <message>
        <location filename="basic_wizard.py" line="36"/>
        <source>Create a YAMM project</source>
        <translation>Créer un projet YAMM</translation>
    </message>
    <message>
        <location filename="basic_wizard.py" line="61"/>
        <source>Step {0}/{1}</source>
        <translation>Etape {0}/{1}</translation>
    </message>
</context>
<context>
    <name>FrameworkEditProjectWindow</name>
    <message>
        <location filename="edit_project_window.py" line="83"/>
        <source>Edit project: {0}</source>
        <translation>Modifier le projet: {0}</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="85"/>
        <source>Create a new project</source>
        <translation>Créer un nouveau projet</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="93"/>
        <source>Save</source>
        <translation>Sauver</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="94"/>
        <source>Save and Close</source>
        <translation>Sauver et fermer</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="95"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="104"/>
        <source>Main</source>
        <translation>Principal</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="105"/>
        <source>Options</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="106"/>
        <source>Softwares</source>
        <translation>Logiciels</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="183"/>
        <source>Add Global Option</source>
        <translation>Ajouter une options globale</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="184"/>
        <source>Remove Global Option</source>
        <translation>Supprimer une options globale</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="258"/>
        <source>Add Option</source>
        <translation>Ajouter une option</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="259"/>
        <source>Remove Selected Options</source>
        <translation>Supprimer les options sélectionnées</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="459"/>
        <source>Select a global option</source>
        <translation>Sélectionner une option globale</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="485"/>
        <source>Options:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="485"/>
        <source>Select an option</source>
        <translation>Sélectionner une option</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="497"/>
        <source>Select a range</source>
        <translation>Sélectionner la portée</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="497"/>
        <source>Ranges:</source>
        <translation>Portées :</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="525"/>
        <source>Cannot add this option</source>
        <translation>Impossible d&apos;ajouter cette option</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="525"/>
        <source>This option has already been added.</source>
        <translation>Cette option a déjà été ajoutée.</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="607"/>
        <source>Cannot save this project</source>
        <translation>Impossible de sauver le projet</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="607"/>
        <source>Project cannot be saved because mandatory values are not set: Project Name, Project Dir and Project Version.</source>
        <translation>Le projet ne peut pas être sauvé car des options obligatoire ne sont pas définies: Project Name, Project Dir et Project Version.</translation>
    </message>
</context>
<context>
    <name>FrameworkIntroWidget</name>
    <message>
        <location filename="start_widget.py" line="49"/>
        <source>Load recent</source>
        <translation>Charger le projet récent sélectionné</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="49"/>
        <source>Remove recent</source>
        <translation>Retirer le projet récent sélectionné</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="68"/>
        <source>Quit YAMM</source>
        <translation>Quitter YAMM</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="68"/>
        <source>Quit YAMM ?</source>
        <translation>Quitter Yamm ?</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="118"/>
        <source>Unable to load project</source>
        <translation>Impossible de charger le projet</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="118"/>
        <source>Project file {0} is not found, removing it from recent list</source>
        <translation>Le fichier {0} du projet n&apos;a pas été trouvé, suppression de la liste des projets récents</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="127"/>
        <source>Copy Project {0}</source>
        <translation>Copie du projet {0}</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="127"/>
        <source>Name of the new project:</source>
        <translation>Nom du nouveau projet :</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="155"/>
        <source>Unable to load this project</source>
        <translation>Impossible de charger ce projet</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="155"/>
        <source>Project file is not found, remove it from recent list</source>
        <translation>Le fichier du projet n&apos;a pas été trouvé, suppression de la liste des projets récents</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="165"/>
        <source>Open YAMM GUI config file</source>
        <translation>Ouvrir un fichier de config de l&apos;IHM de YAMM</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="165"/>
        <source>Yamm Gui Python files (*{0}-yamm_gui.py)</source>
        <translation>Fichiers IHM YAMM (*{0}-yamm_gui.py)</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="188"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>FrameworkProjectGui</name>
    <message>
        <location filename="../project_gui.py" line="78"/>
        <source>YAMM for {0}</source>
        <translation>YAMM pour {0}</translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="80"/>
        <source>YAMM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="182"/>
        <source>Cannot add project</source>
        <translation>Impossible d&apos;ajouter le projet</translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="175"/>
        <source>Projet specified named directory is not a directory</source>
        <translation>Le nom du répertoire pour le projet n&apos;est pas un répertoire</translation>
    </message>
    <message>
        <location filename="../project_gui.py" line="182"/>
        <source>Projet specified named directory cannot be created</source>
        <translation>Le nom du répertoire pour le projet ne pas être créé</translation>
    </message>
</context>
<context>
    <name>FrameworkProjectWindow</name>
    <message>
        <location filename="main_project_window.py" line="139"/>
        <source>Offline compilation</source>
        <translation>Compilation hors ligne</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="139"/>
        <source>This action will compile all softwares (from archives) of the project ! Continue ?</source>
        <translation>Cette action compilera tous les logiciels (à partir de leurs archives) du projet ! Continuer ?</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="148"/>
        <source>Compilation from scratch</source>
        <translation>Compilation depuis zéro</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="148"/>
        <source>This action will delete all install directories and recompile all softwares of the project ! Continue ?</source>
        <translation>Cette action supprimera tous les répertoires d&apos;installatioh et recompilera tous les logiciels du projet ! Continuer ?</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="157"/>
        <source>Delete source directories</source>
        <translation>Supprimer les répertoires des sources</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="157"/>
        <source>This action will delete all source directories ! Continue ?</source>
        <translation>Cette action supprimera tous les répertoires des sources ! Continuer ?</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="166"/>
        <source>Delete build directories</source>
        <translation>Supprimer les répertoires des builds</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="166"/>
        <source>This action will delete all build directories ! Continue ?</source>
        <translation>Cette action supprimera tous les répertoires des builds ! Continuer ?</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="175"/>
        <source>Delete install directories</source>
        <translation>Supprimer les répertoires des installations</translation>
    </message>
    <message>
        <location filename="main_project_window.py" line="175"/>
        <source>This action will delete all install directories ! Continue ?</source>
        <translation>Cette action supprimera tous les répertoires des installations ! Continuer ?</translation>
    </message>
</context>
<context>
    <name>ListOption</name>
    <message>
        <location filename="edit_project_window.py" line="1019"/>
        <source>Add a software</source>
        <translation>Ajouter un logiciel</translation>
    </message>
    <message>
        <location filename="edit_project_window.py" line="1019"/>
        <source>Software to add:</source>
        <translation>Logiciel à ajouter :</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="intro.ui" line="14"/>
        <source>YAMM_TITLE</source>
        <translation>YAMM</translation>
    </message>
    <message>
        <location filename="intro.ui" line="27"/>
        <source>CREATE_PROJECT_BASIC</source>
        <translation>Créer un projet YAMM (Assistant)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="34"/>
        <source>CREATE_PROJECT_ADVANCED</source>
        <translation>Créer un projet YAMM (Avancé)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="51"/>
        <source>LOAD_PROJECT</source>
        <translation>Ouvrir un projet YAMM</translation>
    </message>
    <message>
        <location filename="intro.ui" line="44"/>
        <source>COPY_PROJECT</source>
        <translation>Copier un projet YAMM</translation>
    </message>
    <message>
        <location filename="intro.ui" line="75"/>
        <source>Recent projects</source>
        <translation>Projets récents</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="34"/>
        <source>YAMM Information</source>
        <translation>Informations YAMM</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="40"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Softwares to be processed&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Logiciels à traiter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="85"/>
        <source>Update Software</source>
        <translation>Mettre à jour un logiciel</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="95"/>
        <source>update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="100"/>
        <source>full-compile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="105"/>
        <source>incremental</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="110"/>
        <source>from-scratch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="115"/>
        <source>install-binary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="120"/>
        <source>run-tests</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="128"/>
        <source>Update</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="161"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Used Commands Logs&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Trace des commandes&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="257"/>
        <source>Clean</source>
        <translation>Nettoyage</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="203"/>
        <source>YAMM Project Commands</source>
        <translation>Commandes YAMM</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="213"/>
        <source>Actions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="219"/>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="226"/>
        <source>Start (offline mode)</source>
        <translation>Démarrer (mode hors ligne)</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="233"/>
        <source>Start From Scratch</source>
        <translation>Démarrer depuis zéro</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="247"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="263"/>
        <source>Clean Sources</source>
        <translation>Supprimer les sources</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="270"/>
        <source>Clean Builds</source>
        <translation>Supprimer les builds</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="277"/>
        <source>Clean Installs</source>
        <translation>Supprimer les installations</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="287"/>
        <source>Specific actions</source>
        <translation>Actions spécifiques</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="294"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="project_window.ui" line="300"/>
        <source>Launch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="317"/>
        <source>xterm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="322"/>
        <source>konsole</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="327"/>
        <source>gnome-terminal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="project_window.ui" line="335"/>
        <source>Show notifications</source>
        <translation>Afficher les notifications</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="quit_message_box.py" line="32"/>
        <source>Quit YAMM</source>
        <translation>Quitter YAMM</translation>
    </message>
    <message>
        <location filename="quit_message_box.py" line="32"/>
        <source>Are you sure to quit?</source>
        <translation>Voulez-vous vraiment quitter ?</translation>
    </message>
</context>
<context>
    <name>update_widget</name>
    <message>
        <location filename="update_dock.ui" line="20"/>
        <source>Update Dock</source>
        <translation>Panneau de mise à jour</translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="27"/>
        <source>Select Software</source>
        <translation>Sélectionner le logiciel</translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="39"/>
        <source>Select Mode</source>
        <translation>Mode par défaut</translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="49"/>
        <source>default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="54"/>
        <source>update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="59"/>
        <source>full-compile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="64"/>
        <source>incremental</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="69"/>
        <source>from-scratch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="74"/>
        <source>install-binary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="update_dock.ui" line="79"/>
        <source>run-tests</source>
        <translation></translation>
    </message>
</context>
</TS>

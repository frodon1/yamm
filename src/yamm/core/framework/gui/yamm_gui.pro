SOURCES += basic_wizard.py \
	  edit_project_window.py \
          gui_project.py \
	  main_project_window.py \
	  quit_message_box.py \
	  select_software_list.py \
	  start_widget.py
FORMS += getItemDlg.ui \
        intro.ui \
	manage_software_list.ui \
	option_dic.ui \
	option_list.ui \
	project_window.ui
RESOURCES += icons.qrc
TRANSLATIONS += yamm.ts yamm_fr.ts

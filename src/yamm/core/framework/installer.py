#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import str
from builtins import object
import copy
import datetime
import os
import shutil
import stat
import subprocess
import tarfile
import time

from .installer_files.strings_template import FrameworkInstallerStringsTemplate


# # fancy installer bar file
# See http://www.theiling.de/projects/bar.html
installer_bar_cat = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                 "installer_files", "bar_cat.sh")


class FrameworkInstaller(object):
    def __init__(self, name, logger, topdirectory,
                 softwares, executor, delete=False, runnable=False,
                 nosubdir=False):
        self.name = name
        self.logger = logger
        self.topdirectory = topdirectory
        self.init_softwares = softwares
        self.init_executor = executor
        self.delete = delete
        self.runnable = runnable
        self.nosubdir = nosubdir

        self.debian_depends = []
        self.module_load = []

        self.stringsTemplate = FrameworkInstallerStringsTemplate()
        self.bar_cat = ""
        self.crcsum = 10 * "0"
        self.md5sum = 32 * "0"

        self.post_install_file_name = os.path.join(self.topdirectory,
                                                   "post_install.py")

        self.reset()
        pass

    def reset(self):
        self.softwares = copy.deepcopy(self.init_softwares)
        self.executor = copy.deepcopy(self.init_executor)
        self.map_name_to_softwares = {}
        self.python_version = self.executor.executor_config.python_version
        self.debian_depends = sorted(self.executor.version_object.debian_depends)
        for software in self.softwares:
            software.set_command_env(self.softwares,
                                     self.executor.options,
                                     "nothing",
                                     self.executor.version_object,
                                     self.python_version)
            software.create_tasks()
            # Step 2: ajout du logiciel dans l'executeur
            executor_software = software.get_executor_software()
            self.executor.add_software(executor_software)
            self.map_name_to_softwares[software.name] = software
            self.sha1_collection_file = self.executor.options.get_global_option("sha1_collection_file")

            if software.module_load:
                self.module_load += [m for m in software.module_load.split() if m not in self.module_load]

    def begin(self):
        return True

    def create(self):
        return True

    def end(self):
        return True

    def execute(self):
        ret = self.begin()
        if ret:
            self.reset()
            ret = self.create()
        if ret:
            self.reset()
            ret = self.end()
        return ret

    def create_pack(self, installer_tgz_path):
        self.logger.info("Creating archive file {0}".
                         format(os.path.basename(installer_tgz_path)))
        os.chdir(os.path.dirname(self.topdirectory))
        t_tgz = int(time.time())
        if os.path.exists(self.sha1_collection_file):
            short_sha1_collection_file = os.path.basename(self.sha1_collection_file)
            shutil.copyfile(self.sha1_collection_file, os.path.join(self.topdirectory, short_sha1_collection_file))
        try:
            tar = tarfile.open(installer_tgz_path, "w:gz")
            tar.errorlevel = 1
            tar.add(os.path.basename(self.topdirectory))
            tar.close()
        except Exception:
            self.logger.exception("Impossible to create the archive file {0}.".
                                  format(installer_tgz_path))
        t_tgz = int(time.time()) - t_tgz
        self.logger.info("Archive file {0} created in {1}s".
                         format(installer_tgz_path,
                                datetime.timedelta(seconds=t_tgz)))
        return True

    def get_bar_cat(self):
        try:
            with open(installer_bar_cat) as installer_bar_cat_file:
                self.logger.info("Reading bar_cat file content")
                self.bar_cat = "".join(installer_bar_cat_file.readlines())
        except:
            self.bar_cat = ""
        if self.bar_cat == "":
            self.logger.error("bar_cat file is empty")

    def get_crc(self, installer_tgz_path):
        """
        This method calculates the CRC of a file
        """
        try:
            self.logger.info("Calculating CRC for the archive " + installer_tgz_path)
            cmd = "cat \"{0}\" | CMD_ENV=xpg4 cksum ".format(installer_tgz_path)
            cmd += "|sed -e 's/ /Z/' -e 's/ /Z/' |cut -dZ -f1"
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            stdoutdata, _ = p.communicate()
            self.crcsum = stdoutdata.strip()
        except Exception as e:
            self.logger.info("Exception !: {0}".format(e))
            self.crcsum = 10 * "0"
        self.logger.info("CRC = " + self.crcsum)
        if self.crcsum == 10 * "0":
            self.logger.error("CRC value must not be " + self.crcsum)

    def get_md5sum(self, installer_tgz_path):
        """
        This method calculates the MD5SUM of a file
        """
        try:
            self.logger.info("Calcul du MD5SUM de l'archive " + installer_tgz_path)
            cmd = "md5sum {0} | cut -b-32".format(installer_tgz_path)
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            stdoutdata, _ = p.communicate()
            self.md5sum = stdoutdata.strip()
        except:
            self.md5sum = 32 * "0"
        self.logger.info("MD5SUM = " + self.md5sum)
        if self.md5sum == 32 * "0":
            self.logger.error("MD5SUM value must not be " + self.md5sum)

    def get_decompress_content(self):
        return ""

    def create_decompress_file(self, decompress_file_name, installer_tgz_name):
        self.logger.info("Creating the decompression script file {0}".
                         format(decompress_file_name))
        installer_tgz_path = os.path.join(os.path.dirname(self.topdirectory),
                                          installer_tgz_name)

        self.get_bar_cat()
        # # Calculate the CRC
        self.get_crc(installer_tgz_path)
        # # Calculate the MD5SUM
        self.get_md5sum(installer_tgz_path)

        # Content of the decompress file
        try:
            content = self.get_decompress_content()
            if not content:
                self.logger.error("The decompression script file is empty")
                return False
        except OSError as e:
            self.logger.exception(str(e))

        script_content = self.stringsTemplate.get_installer_script_template().substitute(
              bar_cat=self.bar_cat,
              crcsum=self.crcsum,
              md5sum=self.md5sum,
              content=content)
        with open(decompress_file_name, 'w') as installer_decompress_file:
            self.logger.info("Writing decompression script content in file {0}".
                             format(decompress_file_name))
            installer_decompress_file.write(script_content)
        os.chmod(decompress_file_name, stat.S_IRWXU)
        return True

    def create_runnable(self, installer_tgz_base_name):
        '''
        This method creates a self extractable archive by concatenating a shell
        script and the archive file.
        It supposes that :py:meth:`~FrameworkInstaller.create_decompress_file`
        successfully creates the shell script and that the archive file exists.
        :param str installer_tgz_name: name of the archive file
        :return: Return the success status.
        :rtype: bool
        '''
        installer_tgz_name = installer_tgz_base_name + ".tar.gz"
        installer_run_name = installer_tgz_base_name + ".run"
        self.logger.info("Creating installer {0}".format(installer_run_name))
        installer_tgz_path = os.path.join(os.path.dirname(self.topdirectory),
                                          installer_tgz_name)
        if not os.path.exists(installer_tgz_path):
            self.logger.error("Installer cannot be created: archive file {0} "
                              "does not exist".format(installer_tgz_path))

        t_tgz = int(time.time())

        installer_decompress_file_name = os.path.join(
                                                os.path.dirname(self.topdirectory),
                                                "decompress")
        if self.create_decompress_file(installer_decompress_file_name,
                                       installer_tgz_name):
            self.logger.info("Creating run file...")
            installer_run_path = os.path.join(os.path.dirname(self.topdirectory),
                                              installer_run_name)
            t_run = int(time.time())
            try:
                with open(installer_run_path, 'wb') as destination:
                    shutil.copyfileobj(open(installer_decompress_file_name, 'rb'),
                                       destination)
                    shutil.copyfileobj(open(installer_tgz_path, 'rb'), destination)
            except OSError as e:
                self.logger.exception("Installer cannot be created: {0}".format(e))
            os.chmod(installer_run_name, stat.S_IRWXU)

            if self.delete:
                os.remove(installer_decompress_file_name)

            sha1sum_command = "sha1sum {0} > {0}.sha1".format(installer_run_name)
            try:
                retcode = subprocess.call(sha1sum_command, shell=True)
                if retcode < 0:
                    self.logger.error("sha1sum file cannot be created")
            except OSError as e:
                self.logger.exception("sha1sum file cannot be created: {0}".format(e))
            t_run = int(time.time()) - t_run
            self.logger.info(" ".join(["RUN file is done in",
                                       str(datetime.timedelta(seconds=t_run))]))
        else:
            self.logger.error("Installer cannot be created: error while creating decompress file")

        t_tgz = int(time.time()) - t_tgz
        self.logger.info("Installeur {0} créé en {1}s"
                         .format(installer_tgz_path, datetime.timedelta(seconds=t_tgz)))
        return True

    def get_post_install_dict_substitute_string(self, software):
        installdir = software.get_executor_software_name()
        substitute_string = "os.path.join(CURDIR, '%s')" % installdir
        return substitute_string, installdir

    def writePostInstallFile(self):
        self.logger.info("Writing %s" % self.post_install_file_name)

        softwares_names = [s.name for s in self.softwares]

        with open(self.post_install_file_name, 'w') as post_install_file:
            # Post install file header
            post_install_file.write(self.stringsTemplate.get_installer_post_install_header())

            template = {}
            # Add specific lines for each software
            for software in self.softwares:
                # Artefact softwares are used by yamm only for compilation purposes.
                # They are not real softwares
                if software.isArtefact():
                    continue

                template[software.name] = {'files_to_substitute': set(),
                                           'installdir': '',
                                           'currentsoft': software.get_name_for_installer()}

                substitute_string, installdir = self.get_post_install_dict_substitute_string(software)
                if installdir:
                    template[software.name]['installdir'] = installdir
                if substitute_string:
                    post_install_file.write("d[\"__%s_INSTALL_PATH__\"] = %s\n"
                                            % (software.get_name_for_installer(), substitute_string))

                # If the software defines some movable archive commands with sed commands,
                # The reverse sed commands are written in the post install script.
                # The new sed commands will use install directories given by the env files.
                software.set_command_env(self.softwares,
                                         self.executor.options, "nothing",
                                         self.executor.version_object,
                                         self.python_version)

                for command in software.make_movable_archive_commands:
                    if command.split()[0].startswith("sed"):
                        # target is the file to modify
                        # it is the last element given by split() (removing any trailing ";"),
                        target = command.split()
                        if ";" in target:
                            target.pop(target.index(";"))
                        target = target[-1]
                        if target.endswith(";"):
                            # if no whitespace before the ";"
                            target = target[:-1]
                        dirname = os.path.dirname(target)
                        filename = os.path.basename(target)
                        if filename.startswith('.') and filename.endswith('.template'):
                            target = os.path.join(dirname, filename[1:-9])
                        template[software.name]['files_to_substitute'].add(target)

                for f in software.get_substitute_files():
                    template[software.name]['files_to_substitute'].add(f)

            for softname in softwares_names:
                soft_template = template[softname]
                if soft_template['files_to_substitute']:
                    post_install_file.write(self.stringsTemplate.get_installer_cp_current_template()
                                            .format(**soft_template))

        os.chmod(self.post_install_file_name, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

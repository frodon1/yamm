#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import object
from yamm.core.base import misc


class FrameworkInstallerStringsTemplate(object):

    def get_installer_post_install_header(self):
        return """#!/usr/bin/env python

import os, re, shutil, stat, tempfile


CURDIR = os.path.dirname(os.path.realpath(__file__))


def sed_inplace(filename, pattern, repl):
    '''
    Perform the pure-Python equivalent of in-place `sed` substitution: e.g.,
    `sed -i -e 's/'${pattern}'/'${repl}' "${filename}"`.
    '''
    # For efficiency, precompile the passed regular expression.
    pattern_compiled = re.compile(pattern)

    # For portability, NamedTemporaryFile() defaults to mode "w+b" (i.e., binary
    # writing with updating). This is usually a good thing. In this case,
    # however, binary writing imposes non-trivial encoding constraints trivially
    # resolved by switching to text writing. Let's do that.
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as tmp_file:
        with open(filename, 'rb') as src_file:
            for line in src_file:
                try:
                    line = line.decode()
                except:
                    line = line.decode('latin1')
                tmp_file.write(pattern_compiled.sub(repl, line))

    # Overwrite the original file with the munged temporary file in a
    # manner preserving file attributes (e.g., permissions).
    shutil.copystat(filename, tmp_file.name)
    shutil.move(tmp_file.name, filename)


def substitute(currentsoft, installdir, files):
    '''
    Perform all needed substitutions in given files
    '''
    key = "__%s_INSTALL_PATH__" % currentsoft
    for f in files:
        target = os.path.join(d[key], f)
        dirname = os.path.dirname(target)
        filename = os.path.basename(target)
        target_template = os.path.join(dirname, '.%s.template' % filename)
        target_current = os.path.join(dirname, '.%s.current' % filename)
        if not os.path.exists(target):
            os.system('echo "[WARNING] No such file : %s"' % os.path.join(installdir, f))
        else:
            target_stat = os.lstat(target)
            os.chmod(target, target_stat.st_mode | stat.S_IWUSR)
            if not os.path.exists(target_template):
                shutil.copyfile(target, target_template)
            if not os.path.exists(target_current):
                shutil.copyfile(target_template, target_current)
            sed_inplace(target_current, "__INSTALL_PATH__", d[key])
            for k, v in d.items():
                sed_inplace(target_current, k, v)
            shutil.copyfile(target_current, target)
            os.chmod(target, target_stat.st_mode)
            os.remove(target_current)

d = {}
d["__ENV_DIRECTORY_PATH__"] = CURDIR
"""

    def get_installer_cp_current_template(self):
        return """
# File substitution for {currentsoft}
files_to_substitute = {files_to_substitute}
substitute("{currentsoft}", "{installdir}", files_to_substitute)
"""

    def get_installer_script_template(self):
        installer_decompress_template = """#!/bin/bash
CRCsum=%{crcsum}
MD5=%{md5sum}

%{bar_cat}

export BAR_CMD="tar xzf -"
export BAR_ETA=0
export BAR_CLEAR=1

die()
{
  echo "FATAL ERROR: $* (status $?)" 1>&2
  exit 1
}

# Inspired from http://www.megastep.org/makeself/
check()
{
    OLD_PATH="$PATH"
    PATH=${GUESS_MD5_PATH:-"$OLD_PATH:/bin:/usr/bin:/sbin:/usr/local/ssl/bin:/usr/local/bin:/opt/openssl/bin"}
    MD5_ARG=""
    MD5_PATH=$(exec <&- 2>&-; which md5sum || type md5sum)
    test -x "$MD5_PATH" || MD5_PATH=$(exec <&- 2>&-; which md5 || type md5)
    test -x "$MD5_PATH" || MD5_PATH=$(exec <&- 2>&-; which digest || type digest)
    PATH="$OLD_PATH"

    if test -z "$QUIET"; then
      echo "Verifying archive integrity..."
    fi
    verb=$2

    crc=$(echo "$CRCsum" | cut -d" " -f 1)
    if test -x "$MD5_PATH"; then
      if test "$(basename "$MD5_PATH")" = digest; then
        MD5_ARG="-a md5"
      fi
      md5=$(echo "$MD5" | cut -d" " -f 1)
      if test "$md5" = "00000000000000000000000000000000"; then
        test "x$verb" = "xy" && echo " $1 does not contain an embedded MD5 checksum." >&2
      else
        md5sum=$(eval "$MD5_PATH $MD5_ARG" "$1" | cut -b-32);
        if test "$md5sum" != "$md5"; then
          echo "Error in MD5 checksums: $md5sum is different from $md5" >&2
          rm "$1"
          exit 2
        else
          test "x$verb" = "xy" && echo "MD5 checksums are OK." >&2
        fi
        crc="0000000000"; verb=n
      fi
    fi
    if test $crc = "0000000000"; then
      test x$verb = xy && echo " $1 does not contain a CRC checksum." >&2
    else
      sum1=$(CMD_ENV=xpg4 cksum "$1" | awk '{print $1}')
      if test "$sum1" = "$crc"; then
        test "x$verb" = "xy" && echo "CRC checksums are OK." >&2
      else
        echo "Error in checksums: $sum1 is different from $crc"
        rm "$1"
        exit 2;
      fi
    fi

    if test -z "$QUIET"; then
      echo "All good."
    fi
}

ARCHIVE=$(awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0)

%{content}

exit 0

__ARCHIVE_BELOW__
"""
        return misc.PercentTemplate(installer_decompress_template)

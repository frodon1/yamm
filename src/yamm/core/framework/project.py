#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from __future__ import print_function

from functools import wraps
import io
import copy
import datetime
import glob
import os
import shlex
import shutil
import subprocess
import sys
import time

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str
from builtins import object

from yamm.core.base import misc
from yamm.core.base.command_logger import CommandLog
from yamm.core.base.misc import LoggerException, VerboseLevels
from yamm.core.base.options import Options
from yamm.core.engine.config import CompilEnv
from yamm.core.engine.executor import Executor
from yamm.core.engine.softwares import mode_task_map
from yamm.core.engine.tasks.get_remote import GetRemoteTask
from yamm.core.framework.installer import FrameworkInstaller


def yamm_command(title=""):
  """
  This decorator must be used to declare a Yamm command.\n
  It will wrap a project method with begin_command and end_command.\n
  The time used in the command will also be logged.
  """
  def decorator(command):
    @wraps(command)
    def wrapper(*args, **kw):
      self = args[0]
      if title:
        self.logger.info("{0} starts...".format(title))
        t_ini = int(time.time())
      self.begin_command(command.__name__)
      ret = command(*args, **kw)
      self.last_command_html_log_file = self.current_command_log\
        .get_report_html_filename()
#       print 'self.last_command_html_log_file: ', \
#           self.last_command_html_log_file
      self.end_command()
      if title:
        self.logger.info("{0} is done in {1} seconds".format(
          title, datetime.timedelta(seconds=int(time.time()) - t_ini)))
      return ret
    return wrapper
  return decorator


class FrameworkProject(object):
  """
    .. py:currentmodule:: yamm.core.framework.project

    Base class for projects. It defines the common parameters
    and common commands of YAMM projects.

    :param log_name: Name of the project.
    :type log_name: string
    :param verbose:  Verbose level of the project.
    :type verbose: int


  """


  def __init__(self, log_name="FRAMEWORK", verbose_level=VerboseLevels.INFO):

    # Init default log
    self.name = log_name
    self.verbose_level = verbose_level
    self.logger = misc.Log("[%s PROJECT]" % log_name, self.verbose_level)

    self.options = Options()
    self.version = None
    self.version_flavour = None
    self.current_command_log = None
    self.last_command_html_log_file = ''
    self.extra_python_env_file_name = None
    self.catalog = None

    self.current_map_name_to_softwares = {}
    self.reset_command_vars()
    self.init()


#######
#
# Methods for projects
#
#######

  def init_catalog(self):
    """
      This method is used to add pre defined softwares and versions
      into the catalog of the project.
      It's called by :py:meth:`~Project.init`.

    """
    self.logger.warning("Default init_catalog called !")

  def check_specific_configuration(self, current_command_options):
    """
    Check a command configuration. It checks specific project options.
    It is called by :py:meth:`~Project.check_configuration`.

    :param options.Options current_command_options: Options object of current
        command.

    """
    pass

  def update_configuration(self):
    """
    This method is called by :py:meth:`~Project.begin_command`. It permits
    to update or finish the command options before applying
    :py:meth:`~Project.begin_command` modifications.

    """
    pass

  def software_only_list_hook(self):
    """
      It's a hook called when software_only_list options is used.

    """
    pass

  def add_software_to_env_files_begin_hook(self, executor_software):
    """
    It's a hook called before the end of each treatment in the executor
    on the **make** command.

    :param yamm.core.engine.executor executor_software: Current executor object.

    """
    if self.current_command_options.get_global_option('separate_dependencies'):
      build_file = executor_software.software_config.env_build

      software = self.current_map_name_to_softwares[executor_software.name]
      build_deps = software.get_build_dependencies()

      if build_deps:
        content = '\n# ======================={0}\n'.format("="*len(software.name))
        content += '# Build dependencies for %s\n' % software.name
        content += '# {0}\n'.format(build_deps)
        for dep_name in build_deps:
          try:
            content += self.current_map_name_to_softwares[dep_name].get_build_str()
          except KeyError:
            # dependency is absent
            pass

        self.create_env_build_file(build_file)
        with io.open(build_file, "ab") as pre_file:
          pre_file.write(content)

        software.env_files.insert(0, build_file)

  def add_software_to_env_files_end_hook(self, executor_software):
    """
    It's a hook called after the end of each treatment in the executor
    on the **make** command.

    :param yamm.core.engine.executor executor_software: Current executor object.

    """
    pass

  def create_sha1_collection_file(self, delete=True):
    sha1_collection_file = self.current_command_options.get_global_option("sha1_collection_file")
    self.logger.info("Creating sha1 collection file : %s" % sha1_collection_file)
    sha1_collection_file_directory = os.path.dirname(sha1_collection_file)

    if os.path.exists(sha1_collection_file):
      if delete:
        try:
          os.remove(sha1_collection_file)
        except:
          self.logger.exception("Error in deleting sha1 collection file %s"
                                % sha1_collection_file)
        misc.touch(sha1_collection_file)

    else:
      if not os.path.isdir(sha1_collection_file_directory):
        try:
          os.makedirs(sha1_collection_file_directory)
        except:
          self.logger.error("Creation of directory for sha1 collection file "
                            "failed ! Directory was: %s"
                            % sha1_collection_file_directory)
        misc.touch(sha1_collection_file)

    # Starting sha1 collection file
    name_max_length = max([len(soft.name) for soft in self.current_command_softwares])
    version_max_length = max([len(soft.version) for soft in self.current_command_softwares])
    with io.open(sha1_collection_file, 'ab') as sha1_file:
        sha1_file.write("{0} {1} {2}\n".format("YAMM".ljust(name_max_length),
                                           "".ljust(version_max_length),
                                           misc.get_current_yamm_sha1()))

    return sha1_collection_file

  def create_env_files(self, delete=True):
    """
      It's a method called before starting **make**'s command
      executor to be able to create environnement files that
      :py:meth:`~Project.add_software_to_env_files_end_hook` could use.

      :param boolean delete: If equals to True, the method should delete old
          environnement files.

    """
    pass

  def get_env_files(self):
    """
    This method returns the list of env files of the project
    """
    return []

  def update_env_files_hook(self, software_list):
    """
      It's a hook called during the execution of update method.
      It permits to change the env files of the softwares during the update
      execution.

      :param list software_list: List of softwares names to be updated.
    """
    pass

#######
#
# Init methods
#
#######

  def init(self):
    """
      This method calls two methods init_catalog and define_options
      to initialize the project.

    """
    self.logger.debug("Init project")
    self.init_catalog()
    self.define_options()
    self.logger.debug("End init project")


  def define_options(self):
    """
      This method defines the generic options for all YAMM projects.
      It's called by :py:meth:`~Project.init`.

    """
    GLOBAL = ['global']
    SOFT = ['software']
    ALL_RANGES = ['global', 'category', 'software']

    # Options globales sur les répertoires
    self.options.add_option("top_directory", "", GLOBAL)
    self.options.add_option("archives_directory", "", GLOBAL)
    self.options.add_option("version_directory", "", GLOBAL)
    self.options.add_option("main_software_directory", "", ALL_RANGES)

    # SHA1
    self.options.add_option("sha1_collection_file", "", GLOBAL)

    # Options pour la tâche get_remote
    self.options.add_option("archives_download_tool", "", ALL_RANGES,
                            ["wget", "curl", "urllib"])
    self.options.add_option("archives_download_mode", "", ALL_RANGES,
                            ["force_update", "update", "nothing"])
    self.options.add_option("archive_remote_address", "", ALL_RANGES)
    self.options.add_option("source_type", "", ALL_RANGES,
                            ["remote", "archive", "software"])
    self.options.add_option("use_compat_env", '', ALL_RANGES, ["7", "8", '9'], misc.can_use_compat_env)

    # Options pour les tâches des archives binaires
    self.options.add_option("binary_archive_topdir", "", ALL_RANGES)
    self.options.add_option("binary_archive_url", "", ALL_RANGES)

    # Option diverses
    self.options.add_option("platform", "", GLOBAL)
    self.options.add_option("default_executor_mode", "", ALL_RANGES,
                            list(mode_task_map.keys()))
    self.options.add_option("run_tests", True, ALL_RANGES)
    self.options.add_option("run_version_tests", True, GLOBAL)
    self.options.add_option("write_soft_infos", True, ALL_RANGES)

    # Proxy related options
    self.options.add_option("git_config_proxy", True, ALL_RANGES)
    self.options.add_option("proxy_server", "", ALL_RANGES)
    self.options.add_option("proxy_username", "", ALL_RANGES)

    # Use local server mirror (Pleiade)
    self.options.add_option("use_pleiade_mirrors", False, ["global", "category", "software"])

    # Options pour les logs
    self.options.add_option("command_verbose_level", 1, GLOBAL,
                            sorted(VerboseLevels.reverse_mapping.keys()))
    self.options.add_option("command_print_console", True, GLOBAL)
    self.options.add_option("command_write_in_file", True, GLOBAL)
    self.options.add_option("command_files_basename", "", GLOBAL)
    self.options.add_option("log_directory", "", GLOBAL)
    self.options.add_option("light_background_colors", False, GLOBAL)

    # Options pour la compilation
    self.options.add_option("parallel_make", "", GLOBAL)
    self.options.add_option("xterm_make", True, ALL_RANGES)
    self.options.add_option("continue_on_failed", True, GLOBAL)
    self.options.add_option("check_dependency_compilation", True, GLOBAL)
    self.options.add_option("clean_src_if_success", True, ALL_RANGES)
    self.options.add_option("clean_build_if_success", True, ALL_RANGES)
    self.options.add_option("backup_software", True, GLOBAL)
    self.options.add_option("separate_dependencies", True, GLOBAL)
    self.options.add_option("module_load", "", ALL_RANGES)

    # Global options for managing the list of softwares
    self.options.add_option("software_add_dict", {}, GLOBAL)
    self.options.add_option("software_remove_list", [], GLOBAL)
    self.options.add_option("software_minimal_list", [], GLOBAL)
    self.options.add_option("softwares_user_version", {}, GLOBAL)
    self.options.add_option("software_only_list", [], GLOBAL)
    self.options.add_option("version_flavour", "", GLOBAL)
    self.options.add_option("use_system_version", True, ALL_RANGES)

    # Global options for sources location of python prerequisites packages
    self.options.add_option("python_prerequisites_server", "", GLOBAL)
    self.options.add_option("python_prerequisites_ftp_server", "", GLOBAL)
    self.options.add_option("python_prereq_force_ftp_server", True, GLOBAL)

    # Options pour les catégories
    # Options avancées les logiciels
    self.options.add_option("software_src_directory", "", SOFT)
    self.options.add_option("software_build_directory", "", SOFT)
    self.options.add_option("software_install_directory", "", SOFT)
    self.options.add_option("software_parallel_make", "", SOFT)
    self.options.add_option("software_make_target", "", SOFT)
    self.options.add_option("software_repository_name", "", SOFT)
    self.options.add_option("software_additional_src_files", [], SOFT)
    self.options.add_option("software_additional_config_options", "", SOFT)
    self.options.add_option("software_additional_build_options", "", SOFT)
    self.options.add_option("software_additional_env", "", SOFT)
    self.options.add_option("software_additional_env_file", "", SOFT)
    self.options.add_option("software_reset_config_options", True, SOFT)
    self.options.add_option("software_reset_build_options", True, SOFT)

    # Options related to Debian packages
    self.options.add_option("check_debian_packages", True, GLOBAL)

  def set_version(self, version, flavour=""):
    """
      Project's user version to select a version for project's commands.

      :param string version: Version name.

    """
    self.version = version
    self.version_flavour = flavour
    self.check_version()

#######
#
# Check methods
#
#######

  def check_configuration(self, current_command_options):
    """
      Check a command configuration. It checks all the generic options.
      It calls :py:meth:`~Project.check_specific_configuration` for specific
      projects.

      :param misc.options current_command_options: options object of current
          command.
    """

    # Test des répertoires
    current_command_options.check_abs_path('top_directory', logger=self.logger)
    current_command_options.check_abs_path('archives_directory',
                                           logger=self.logger)
    current_command_options.check_abs_path('version_directory',
                                           logger=self.logger)
    current_command_options.check_abs_path('log_directory', logger=self.logger)
    current_command_options.check_abs_path('binary_archive_topdir',
                                           logger=self.logger)

    current_command_options.check_allowed_value('archives_download_tool',
                                       GetRemoteTask.tools_allowed,
                                       logger=self.logger)
    current_command_options.check_allowed_value('archives_download_mode',
                                       GetRemoteTask.modes_allowed,
                                       logger=self.logger)
    current_command_options.check_allowed_value('source_type',
                                       ["remote", "archive", "software"],
                                       logger=self.logger)

    self.check_specific_configuration(current_command_options)

  def check_version(self):
    """
      Internal method to check if the version selected is available.

    """
    if not self.version:
      self.logger.error("Version is not defined")
    if not self.catalog.has_version(self.version):
      self.catalog.print_versions()
      self.logger.error("Version %s is unknown" % self.version)
    if self.version_flavour:
      version_class = self.catalog.get_version_class(self.version)
      version_object = version_class(verbose=0, flavour=self.version_flavour)
      version_object.check_flavour(self.version_flavour)

  @misc.debian_compliant
  def _compute_debian_packages_from_softwares(self, software_list=None, directory='install_directory'):
    """
    From a list of YAMM softwares, get all the libraries (*.so) and the debian
    packages and then check if they are installed.
    """
    from yamm.core.base import debian_tools
    if software_list and not isinstance(software_list, list):
        software_list = [software_list.encode('utf8')]

    packages_dict = {}
    known_packages = {}
    # Create and add softwares to the executor
    for software in self.current_command_softwares:
        # Determine the executor mode for this software (see start method for the detailed rules)
        if software_list and software.name not in software_list:
            continue

        libraries = []
        software.set_command_env(self.current_command_softwares,
                                 self.current_command_options,
                                 'nothing',
                                 self.current_command_version_object,
                                 self.current_command_python_version)
        install_directory = getattr(software, directory)
        if not os.path.exists(install_directory):
            self.logger.warning('Installdir missing: %s' % install_directory)
            continue
        self.logger.info('Gathering libraries for %s (%s)' % (software.name, install_directory))
        for root, _, _ in os.walk(install_directory):
            libraries += glob.glob(os.path.join(root, '*.so'))
        if not libraries:
            continue
        pack_from_libs = self.get_debian_packages_from_libraries(libraries, known_packages)
        depends_pack = software.get_debian_depends()
        if depends_pack:
            depends_pack = [d for d in software.get_debian_depends() if debian_tools.exists(d) and d not in pack_from_libs]
        packages_dict[software.name] = sorted(debian_tools.minimize_dependencies(pack_from_libs + depends_pack))
    return packages_dict

  @misc.debian_compliant
  def get_debian_packages_from_libraries_file(self, filename=None):
    """
    From a file containing a list of libraries (*.so files), get the debian
    packages and then check if they are installed.
    """
    if not filename or not os.path.exists(filename):
        return
    with io.open(filename, mode='rb') as f:
      return self.get_debian_packages_from_libraries([w.strip() for w in f.readlines()])

  @misc.debian_compliant
  def get_debian_packages_from_libraries(self, libraries=None,
                                         known_packages={}):
    """
    From a list of libraries (*.so files), get the debian packages and
    then check if they are installed.
    The 'known_packages' dict allows to indicate already know packages for libraries
    """
    from yamm.core.base import debian_tools
    if not libraries:
        return

    def strip_dep_libs(ldd):
        """ Return a list of dependencies from a result of ldd.

            libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f8e09d8e000)
            /lib64/ld-linux-x86-64.so.2 (0x00007f8e0a5b9000)
            linux-vdso.so.1 (0x00007ffea43fa000)
            libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f8e097cd000)
            libstdc++.so.6 => /usr/lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f8e0a08f000)
            libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f8e09b78000)

            will become:

            /lib/x86_64-linux-gnu/libm.so.6
            /lib/x86_64-linux-gnu/libc.so.6
            /usr/lib/x86_64-linux-gnu/libstdc++.so.6
            /lib/x86_64-linux-gnu/libgcc_s.so.1
        """
        for ldd_line in ldd:
            if not ldd_line.strip():
                continue
            libname = ldd_line.split('=>')[-1]
            libname = libname.split()[0]
            self.logger.debug(' => '.join([ldd_line, libname]))
            yield libname

    def get_system_libs(libs):
        """ Yields the system libs from a list of libs
        """
        for libname in libs:
            if not (libname.startswith('/usr') or libname.startswith('/lib')):
                self.logger.debug("Not a system library: skipped")
                continue
            yield libname

    def get_package_from_lib(libname):
        """ Returns the package which provides the given lib
        If not package is found and the library is a symlink, the function
        will try to get the package by following the symlink.
        """
        command = "dpkg -S %s" % libname
        self.logger.info(command)
        p = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        package, err = p.communicate()
        retcode = p.poll()
        if retcode != 0:
            self.logger.info('No package found for %s: %s' % (libname, err.decode().strip('\n')))
            if not os.path.islink(libname):
                return None
            p = subprocess.Popen(['readlink', libname], stdout=subprocess.PIPE)
            newlibname = p.communicate()[0].decode().strip('\n')
            self.logger.info('File %s is a symlink, trying with its source: %s' % (libname, newlibname))
            package = get_package_from_lib(newlibname)
        else:
            package = package.decode().strip()
            package = package.split('\n')[-1].split(':')[0].strip()
        return package

    # Construction de la liste des librairies en dépendance
    nblibs = len(libraries)
    libdeps = set()
    for index, mylib in enumerate(libraries):
        self.logger.info('Get dependencies from library [%d/%d] %s' % (index + 1, nblibs, mylib))
        if not os.path.isabs(mylib):
            self.logger.debug('Not an absolute path: skipped (%s)' % mylib)
        p = subprocess.Popen(['ldd', mylib], stdout=subprocess.PIPE)
        current_deps = p.communicate()[0].decode().split('\n')
        libdeps = libdeps.union(strip_dep_libs(current_deps))
    self.logger.debug('\n'.join(libdeps))

    # Construction de la liste des paquets debian fournissant les librairies
    # système détectées précédemment
    packages = set()
    packages_not_found = set()
    for libname in get_system_libs(libdeps):
        self.logger.info("Check package for %s" % libname)
        libknown = False
        if libname in known_packages:
            package = known_packages[libname]
            libknown = True
        else:
            package = get_package_from_lib(libname)
        if not libknown:
            known_packages[libname] = package
        else:
            self.logger.info('It was a known library !')
        if package is None:
            self.logger.info('No package found for %s' % libname)
            packages_not_found.add(libname)
            continue
        else:
            self.logger.info('Package: %s' % package)
            packages.add(package)

    packages = sorted(debian_tools.minimize_dependencies(packages))
    if packages_not_found:
        self.logger.debug('No packages found for these libraries:')
        self.logger.debug('\n'.join(packages_not_found))
    return packages

  @misc.debian_compliant
  @yamm_command("Create meta package")
  def create_meta_package(self, name, version, maintainer, description,
                          build_depends=None, depends=None):
    """ Create a meta-package base on used and depends packages.
    The 'equivs-control' command is used.
    """
    from yamm.core.base import debian_tools
    if build_depends is None:
        build_depends = self._get_debian_build_depends()
    else:
        if not isinstance(build_depends, list):
            build_depends = build_depends.split()
    build_depends = debian_tools.minimize_dependencies(build_depends)

    if depends is None:
        depends = self._get_debian_packages()
        depends_dict = self._compute_debian_packages_from_softwares()
        if depends_dict:
            for packages in depends_dict.values():
                depends += [p for p in packages if p not in depends]
    else:
        if not isinstance(depends, list):
            depends = depends.split()

    depends = [d for d in depends if d not in build_depends]
    depends = sorted(debian_tools.minimize_dependencies(depends))

    wd = os.path.join(os.path.realpath(os.curdir), name)
    ns_control = os.path.join(wd, 'ns-control')
    if os.path.isdir(wd):
        shutil.rmtree(wd)
    os.makedirs(wd, mode=0o755)
    command = shlex.split("equivs-control ns-control")
    try:
        p = subprocess.Popen(command, cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()
    except Exception as e:
        self.logger.exception('in create_meta_package, command was: {0}, '
                              'exception was: {1}'.format(command, e))
    if p.returncode != 0:
        self.logger.exception('in create_meta_package, command was: {0}, '
                              'return code was: {1}, '
                              'error was: {2}'.format(command, p.returncode, p.stderr))

    ns_control_file = io.open(ns_control, mode='r').read()
    ns_control_file = ns_control_file[:ns_control_file.rfind('Description:')]
    ns_control_file = ns_control_file.replace('<package name; defaults to equivs-dummy>', name)
    ns_control_file = ns_control_file.replace('# Version: <enter version here; defaults to 1.0>',
                                              'Version: %s' % version)
    ns_control_file = ns_control_file.replace('# Maintainer: Your Name <yourname@example.com>',
                                              'Maintainer: %s' % maintainer)
    ns_control_file = ns_control_file.replace('# Depends: <comma-separated list of packages>',
                                              'Depends: %s' % ', '.join(depends))
    ns_control_file += 'Description: %s' % description

    with io.open(ns_control, 'w', encoding='utf8') as ns:
        ns.write(ns_control_file)

    command = shlex.split("equivs-build -f ns-control")
    try:
        p = subprocess.Popen(command, cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()
    except OSError as e:
        self.logger.exception('in create_meta_package, command was: {0}, '
                              'exception was: {1}'.format(command, e))
    if p.returncode not in (0, 25):
        self.logger.exception('in create_meta_package, command was: {0}, '
                              'return code was: {1}'.format(command, p.returncode))
    self.logger.info("Meta package created in %s" % wd)

  @misc.debian_compliant
  def check_debian_packages(self, packages=None):
    """
      Allows the user to check if a list of packages is installed.
      Usefull when some softwares are removed and project relies on the
      system version.
    """
    from yamm.core.base import debian_tools
    packages = packages or []
    if not isinstance(packages, list):
      packages = [packages]
    if not packages:
      return True, [], [], []

    not_a_package = set()
    required_installed = set()
    required_not_installed = set()
    self.logger.debug('=' * 33)
    self.logger.debug('Check presence of Debian packages: {0}'.format(' '.join(packages)))
    for p in packages:
      is_installed = debian_tools.is_installed(p.strip())
      if is_installed is None:
        not_a_package.add(p)
      if is_installed:
        required_installed.add(p)
      else:
        required_not_installed.add(p)

    if not_a_package:
      self.logger.debug('Not a valid package name:')
      for p in not_a_package:
        self.logger.debug('\t{0}'.format(p))

    if required_installed:
      self.logger.debug('Installed packages:')
      for p in required_installed:
        self.logger.debug('\t{0}'.format(p))

    if required_not_installed:
      self.logger.info("Some packages are missing:")
      for p in required_not_installed:
        self.logger.info('\t{0}'.format(p))

      self.logger.debug()
      command = 'sudo apt-get'
      if misc.get_calibre_version() == '9':
        command = 'pkcon'
      self.logger.info('Run the following command to install the required '
                       'packages: {0} -y install {1}'
                       .format(command, ' '.join(required_not_installed)))

    self.logger.debug('=' * 33)
    return (not required_not_installed, not_a_package,
            required_installed, required_not_installed)

  @misc.debian_compliant
  def _check_debian_packages_build(self):
      return self.check_debian_packages(self._get_debian_build_depends())

  @misc.debian_compliant
  def _check_debian_packages_exec(self):
      return self.check_debian_packages(self._get_debian_depends())

  @misc.debian_compliant
  def _get_debian_depends(self):
    packages = set()
    for soft in self.current_command_softwares:
      depends = soft.get_debian_depends()
      if depends:
        if not isinstance(depends, list):
          depends = [depends]
        for d in depends:
          packages.add(d)
      exec_depends = soft.get_exec_dependencies()
      for exec_depend in exec_depends:
        version, p_names, p_versions = self.current_command_version_object.get_packages()[exec_depend]
        if p_names:
          for p_name in p_names:
            packages.add(p_name)
    return list(packages)

  @misc.debian_compliant
  def _get_debian_build_depends(self):
    packages = set()
    all_packages = self.current_command_version_object.get_packages()
    for soft in self.current_command_softwares:
      depends = soft.get_debian_build_depends()
      if depends:
        if not isinstance(depends, list):
          depends = [depends]
        for d in depends:
          packages.add(d)
      build_depends = soft.get_build_dependencies()
      for build_depend in build_depends:
        version, p_names, p_versions = all_packages[build_depend]
        if p_names:
          for p_name in p_names:
            packages.add(p_name)
    return list(packages)

  @misc.debian_compliant
  def _get_debian_packages(self):
    packages = set()
    for softname, package in list(self.current_command_version_object.get_packages().items()):
      version, p_names, p_versions = package
      if p_names:
        for p_name in p_names:
          packages.add(p_name)
    return list(packages)


#######
#
# Methods for default initialisation
#
#######

  def set_default_options_values(self, current_command_options):
    """
      This method is used by :py:meth:`~Project.begin_command` to define
      default values to the generic options.

      :param misc.options current_command_options: Options object of current
          command.

    """

    # Options sur les répertoires
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory",
          os.path.join(os.environ["HOME"], "yamm_default_topdirectory"))
    if not current_command_options.get_global_option("archives_directory"):
      current_command_options.set_global_option("archives_directory",
          os.path.join(current_command_options.get_global_option(
                                                              "top_directory"),
                       "archives"))

    if not current_command_options.get_global_option("version_directory"):
      version_directory = misc.transform(self.version, dot=False, dash=False)
      if self.version_flavour:
        version_directory += "_{0}".format(self.version_flavour.replace(' ', '_'))
      version_directory = os.path.join(current_command_options.get_global_option("top_directory"),
                                       version_directory)
      current_command_options.set_global_option("version_directory", version_directory)

    if not current_command_options.get_global_option("sha1_collection_file"):
      current_command_options.set_global_option("sha1_collection_file",
          os.path.join(current_command_options.get_global_option(
                                                        "version_directory"),
                       "sha1_collections.txt"))

    if not current_command_options.get_global_option("python_prerequisites_server"):
      current_command_options.set_global_option("python_prerequisites_server",
                                                misc.get_pypi_server())

    if not current_command_options.get_global_option("python_prereq_force_ftp_server"):
      current_command_options.set_global_option("python_prereq_force_ftp_server", False)

    if not current_command_options.get_global_option("python_prerequisites_ftp_server"):
      current_command_options.set_global_option("python_prerequisites_ftp_server",
          os.path.join(misc.get_ftp_server(), 'YAMM', 'Public', 'SALOME', 'sources', 'prerequis'))

    if not current_command_options.get_global_option("platform"):
      current_command_options.set_global_option("platform", "LINUX")
    if not current_command_options.get_global_option("command_verbose_level"):
      current_command_options.set_global_option("command_verbose_level",
                                                self.verbose_level)
    if not current_command_options.get_global_option("command_print_console"):
      current_command_options.set_global_option("command_print_console", True)
    if not current_command_options.get_global_option("command_write_in_file"):
      current_command_options.set_global_option("command_write_in_file", True)
    if not current_command_options.get_global_option("log_directory"):
      log_directory = os.path.join(current_command_options.get_global_option("version_directory"),
                                   "logs")
      current_command_options.set_global_option("log_directory", log_directory)
    if not current_command_options.get_global_option("light_background_colors"):
      current_command_options.set_global_option("light_background_colors",
                                                False)
    if not current_command_options.get_global_option("command_files_basename"):
      current_command_options.set_global_option("command_files_basename", "")

    if not current_command_options.get_global_option("archives_download_tool"):
      current_command_options.set_global_option("archives_download_tool",
                                                "urllib")
    if not current_command_options.get_global_option("archives_download_mode"):
      current_command_options.set_global_option("archives_download_mode",
                                                "update")
    if not current_command_options.get_global_option("archive_remote_address"):
      current_command_options.set_global_option("archive_remote_address", "")
    if not current_command_options.get_global_option("binary_archive_topdir"):
      current_command_options.set_global_option("binary_archive_topdir",
          os.path.join(current_command_options.get_global_option(
                                                              "top_directory"),
                       "binary_archives"))

    if not current_command_options.get_global_option("parallel_make"):
      current_command_options.set_global_option("parallel_make", "1")
    if current_command_options.get_global_option("xterm_make") is None:
      current_command_options.set_global_option("xterm_make", False)
    if current_command_options.get_global_option("continue_on_failed") is None:
      current_command_options.set_global_option("continue_on_failed", False)
    if current_command_options.get_global_option(
        "check_dependency_compilation") is None:
      current_command_options.set_global_option("check_dependency_compilation",
                                                True)
    if current_command_options.get_global_option("backup_software") is None:
      current_command_options.set_global_option("backup_software", False)
    if current_command_options.get_global_option("separate_dependencies") is None:
      current_command_options.set_global_option("separate_dependencies", True)

    if not current_command_options.get_global_option("software_add_dict"):
      current_command_options.set_global_option("software_add_dict", {})
    if not current_command_options.get_global_option("software_remove_list"):
      current_command_options.set_global_option("software_remove_list", [])
    if not current_command_options.get_global_option("software_minimal_list"):
      current_command_options.set_global_option("software_minimal_list", [])
    if not current_command_options.get_global_option("softwares_user_version"):
      current_command_options.set_global_option("softwares_user_version", {})
    if not current_command_options.get_global_option("software_only_list"):
      current_command_options.set_global_option("software_only_list", [])
    if current_command_options.get_global_option("version_flavour") is None:
      current_command_options.set_global_option("version_flavour",
                                                self.version_flavour or "")
    if current_command_options.get_global_option("use_system_version") is None:
      current_command_options.set_global_option("use_system_version", False)

    if not current_command_options.get_global_option("source_type"):
      current_command_options.set_global_option("source_type", "archive")
    if not current_command_options.get_global_option("run_tests"):
      current_command_options.set_global_option("run_tests", False)
    if current_command_options.get_global_option("run_version_tests") is None:
      current_command_options.set_global_option("run_version_tests",
          current_command_options.get_global_option("run_tests"))
    if current_command_options.get_global_option("write_soft_infos") is None:
      current_command_options.set_global_option("write_soft_infos", True)
    if current_command_options.get_global_option("git_config_proxy") is None:
      current_command_options.set_global_option("git_config_proxy", True)

    if current_command_options.get_global_option("use_pleiade_mirrors") is None:
      current_command_options.set_global_option("use_pleiade_mirrors", False)

  def remove_global_option_value(self, option):
    self.options.remove_global_option_value(option)

  def set_global_option(self, option, value):
    self.options.set_global_option(option, value)
    if option == "version_flavour":
      self.version_flavour = value

  def set_category_option(self, category, option, value):
    self.options.set_category_option(category, option, value)

  def set_software_option(self, software, option, value):
    self.options.set_software_option(software, option, value)

  def get_option(self, software, option):
    return self.options.get_option(software, option)

  def get_global_option(self, option):
    return self.options.get_global_option(option)

  def get_category_option(self, category, option):
    return self.options.get_category_option(category, option)

  def print_options(self, software):
    return self.options.print_options(software)

  def get_installer_class(self):
    return FrameworkInstaller

#######
#
# Methods used to build project commands
#
#######

  def is_build_only_software(self, software_name):
    return software_name in self.current_command_build_softwares \
        and software_name not in self.current_command_exec_softwares

  def get_current_python_version(self, softwares, current_command_options):
    """
      This method returns the Python version number of the current command.

      :param [yamm.core.framework.software] softwares: List of softwares of
          current command.

      :param misc.options current_command_options: Options object of current
          command.

      :return: Return the python version.
      :rtype: string

    """

    # Step 1: si Python dans les softwares
    for soft in softwares:
      if soft.is_python():
        return soft.get_python_version(self.current_command_version_object._py3)

    # Step 1 bis: Get Python version from system
    command = ""
    if self.extra_python_env_file_name:
      if current_command_options.get_global_option(self.extra_python_env_file_name):
        command += '. '
        command += current_command_options.get_global_option(self.extra_python_env_file_name)
        command += ' ; '
    python_version = misc.compute_python_version(self.current_command_version_object._py3, command)
    return python_version[:-2]

  def get_version_object(self):
    version_class = self.catalog.get_version_class(self.version)
    version_flavour = self.current_command_options.get_global_option(
                                                            'version_flavour')
    version_object = version_class(
        verbose=self.current_command_options.get_global_option(
                                                    'command_verbose_level'),
        flavour=version_flavour)

    version_object.init_configuration(self.catalog)
    self.logger.debug('Softwares: {0}'.format(list(version_object.softwares.keys())))
    soft_use_system_version = None
    for softname in version_object.softwares:
      # Deal with system softwares
      soft_use_system_version = self.current_command_options.get_option(softname,
                                                                        'use_system_version')
      if soft_use_system_version:
        version_object.add_soft_system_version(softname)
    # Configuration de l'objet version
    version_object.update_configuration()
    # Set version python
    return version_object

  def insert_sorted(self, software):
    if software not in self.current_sorted_softwares:
      if software in self.current_check_cyclic :
        self.logger.error("Cyclic dependencies detected for " + software.name)
      self.current_check_cyclic.append(software)

      for soft_depend in software.get_dependencies(recursive=True):
        for current_soft in self.current_command_softwares:
          if current_soft.name == soft_depend:
            self.insert_sorted(current_soft)

      self.current_check_cyclic.pop()
      if software in self.current_command_softwares:
        if software.first():
          if self.first_software is not None:
            self.logger.error('Set %s as first but another software is already marked as first (%s)'
                              % (software.name, self.first_software.name))
          self.first_software = software
        self.current_sorted_softwares.append(software)

  def check_generated_env_files(self, env_files=None):
    if env_files is None or env_files == []:
      env_files = self.get_env_files()

    if env_files:
      cmd1 = ""
      for env_file in env_files:
        cmd1 += ". %s ; " % env_file

      cmd2 = "if test ! -z $TEST ; "
      cmd2 += "  then arr=$(echo $TEST | tr \":\" \"\\n\") ; "
      cmd2 += "  for x in $arr ; "
      cmd2 += "  do if test ! -d $x ; "
      cmd2 += "       then result=${result}\":\"${x} ; "
      cmd2 += "  fi ; "
      cmd2 += "  done ; "
      cmd2 += "fi ; "
      cmd2 += "if test ! -z $result ; "
      cmd2 += "  then echo $result ; "
      cmd2 += "fi"

      rtn_dict = {}
      paths_list = ["LD_LIBRARY_PATH", "PATH", "PYTHONPATH"]
      for path in paths_list:
        cmd = cmd1 + "export TEST=${" + path + "} ; " + cmd2
        try:
          # We force usage of bash here because the installer prerequisite
          # script uses variable BASH_SOURCE which is bash-specific and has no
          # POSIX Shell equivalent.
          p = subprocess.Popen(cmd,
                               executable="/bin/bash",
                               shell=True,
                               stdout=subprocess.PIPE)
        except OSError as e:
          self.logger.exception('in checking missing paths, command was: {0}, '\
                            'exception was: {1}'.format(cmd, e))

        rtn = ''.join([str(line) for line in p.stdout.readlines()])
        rtn_dict[path] = rtn

      for path, rtn in list(rtn_dict.items()):
        if rtn != '':
          self.logger.warning('Missing paths in {0} : {1} '\
                              .format(path, rtn.replace(':', '\n')))

  def begin_command(self, command_name="", createLog=True):
    """
      All the commands calls this method to initialize the different attributes
      needed by a command.

      It checks options, calculate command's software list and configure a new
      executor object.

      :param string command_name: Set the command name for logging purposes

    """

    if command_name == "":
      import inspect
      caller_frame = inspect.stack()[1]
      caller_info = inspect.getframeinfo(caller_frame[0])
      command_name = caller_info[2]

    self.check_version()

    # Step 0: options object
    self.current_command_options = copy.deepcopy(self.options)
    self.set_default_options_values(self.current_command_options)
    self.update_configuration()
    self.check_configuration(self.current_command_options)

    # Step 1: version object
    self.current_command_version_object = self.get_version_object()
    # Check system dependencies
    ret = self.check_debian_packages(self.current_command_version_object.debian_depends)
    if not ret[0]:
        self.logger.warning('Missing system packages: {}'.format(' '.join(ret[-1])))

    # Step 2: specialize version object
    for software_name, software_version in self.current_command_options\
        .get_global_option('software_add_dict').items():
      self.current_command_version_object.add_software(software_name,
                                                       software_version)

    self.current_map_name_to_softwares.clear()
    for software_name, software_version in self.current_command_options\
        .get_global_option('softwares_user_version').items():
      if not self.current_command_version_object.has_software(software_name):
        self.logger.warning("[softwares_user_version] Cannot change version for"
                            " %s, it is not in the software list" % software_name)
        continue
      try:
        _version, ordered_version = software_version.split()
      except ValueError:
        _version = software_version
        ordered_version = None
      try :
        version, sha1_version = _version.split(',')
        if not ordered_version:
          ordered_version = version
      except ValueError:
        version = _version
        if not ordered_version:
          ordered_version = _version
        sha1_version = None
      self.current_command_version_object.set_software_version(software_name,
                                                               version,
                                                               ordered_version,
                                                               sha1_version)

    self.current_command_version_object.configure_tests(
                                                  self.current_command_options)

    # Step 3: Get softwares list and calculate correct order
    self.current_command_softwares = self.current_command_version_object\
        .get_softwares()
    self.current_sorted_softwares = []
    self.current_check_cyclic = []
    self.first_software = None
    for software in self.current_command_softwares:
      self.insert_sorted(software)
    if self.first_software is not None:
      self.current_sorted_softwares.remove(self.first_software)
      self.current_sorted_softwares.insert(0, self.first_software)
    self.current_command_softwares = self.current_sorted_softwares

    # Step 4: calculate list of softwares
    # Order is: software_minimal_list and software_only_list
    # and software_remove_list

    # software_minimal_list
    software_minimal_list = self.current_command_options.get_global_option(
                                                       'software_minimal_list')
    if software_minimal_list:
      list_done = self.current_command_version_object\
          .set_software_minimal_list(software_minimal_list)
      iter_list = copy.copy(self.current_command_softwares)
      for current_software in iter_list:
        if current_software.name not in list_done:
          self.current_command_softwares.remove(current_software)

    # software_only_list
    only_list = self.current_command_options.get_global_option(
                                                         'software_only_list')
    if only_list:
      iter_list = copy.copy(self.current_command_softwares)
      for current_software in iter_list:
        if not current_software.name in only_list:
          self.current_command_version_object.remove_software(
                                                        current_software.name)
          self.current_command_softwares.remove(current_software)
    self.software_only_list_hook()

    # software_remove_list
    software_remove_list = self.current_command_options.get_global_option(
                                                        'software_remove_list')
    if software_remove_list:
      for soft_name in software_remove_list:
        self.current_command_version_object.remove_software(soft_name)
        iter_list = copy.copy(self.current_command_softwares)
        for current_software in iter_list:
          if current_software.name == soft_name:
            self.current_command_softwares.remove(current_software)
    
#     # Update the version_object
#     self.current_command_version_object.update_configuration()

    # Update the build and execution only software name lists
    current_build_softwares = set()
    current_exec_softwares = set()
    for soft in self.current_command_softwares:
      for dep in soft.get_build_dependencies():
        current_build_softwares.add(dep)
      for dep in soft.get_exec_dependencies():
        current_exec_softwares.add(dep)
    self.current_command_build_softwares = list(current_build_softwares)
    self.current_command_exec_softwares = list(current_exec_softwares)

    # Update option object with the list of softwares
    for software in self.current_command_softwares:
      self.current_command_options.add_software(software.name,
                                                software.get_type())

    # Step 5: calcul la version python
    self.current_command_python_version = self.get_current_python_version(
                                              self.current_command_softwares,
                                              self.current_command_options)

    # Step 6: Create Command and Test logger Objects
    if createLog:
      self.current_command_log = CommandLog(
          command_name,
          command_verbose_level=self.current_command_options\
              .get_global_option('command_verbose_level'),
          log_directory=self.current_command_options\
              .get_global_option('log_directory'),
          print_console=self.current_command_options\
              .get_global_option('command_print_console'),
          write_in_file=self.current_command_options\
              .get_global_option('command_write_in_file'),
          command_files_basename=self.current_command_options\
              .get_global_option('command_files_basename'),
          version_name=self.current_command_version_object.name,
          light_background=self.current_command_options\
              .get_global_option('light_background_colors'))

    # Step 7: création de l'exécuteur
    executor_config = CompilEnv(
        main_topdir=self.current_command_options\
            .get_global_option('top_directory'),
        python_version=self.current_command_python_version,
        parallel_make=self.current_command_options\
            .get_global_option('parallel_make'),
        continue_on_failed=self.current_command_options\
            .get_global_option('continue_on_failed'),
        check_dependency_compilation=self.current_command_options\
            .get_global_option('check_dependency_compilation'),
        backup_software=self.current_command_options\
            .get_global_option('backup_software'))

    self.current_command_executor = Executor(
        self.current_command_version_object,
        self.current_command_options,
        executor_config,
        True,
        self.current_command_options.get_global_option('command_verbose_level'),
        command_log=self.current_command_log)

  def execute_command(self):
    # Launch executor
    return self.current_command_executor.execute()

  def get_last_command_html_log_file(self):
    return self.last_command_html_log_file

  def end_command(self, endlog=True):
    """
      All the commands calls this method to clean commands specific attributes

    """
    if self.current_command_log and endlog:
      self.current_command_log.end_command()
      self.current_command_log = None
    self.reset_command_vars()

  def reset_command(self, command_name='', endlog=True):
    self.end_command(endlog)
    self.begin_command(command_name, createLog=endlog)

  def reset_command_vars(self):
    self.current_command_python_version = ""
    self.current_command_version_object = None
    self.current_command_softwares = []
    self.current_command_build_softwares = []
    self.current_command_exec_softwares = []
    self.current_command_executor = None
    self.current_command_options = None

#######
#
# Print methods
#
#######

  def print_catalog(self):
    """
      Print project's current catalog, versions and softwares that has
      been added.

    """

    self.logger.info()
    self.logger.info('FrameworkCatalog:')
    self.logger.info('--------')
    self.catalog.print_versions()
    self.catalog.print_softwares()

  def print_directories(self, current_cmd_opts, max_width=-1):
    """
      Print project's main directories.

      :param misc.options current_cmd_opts: Options object of
             current command.

      :return: Return the max width of the option labels.
      :rtype: int

    """

    self.logger.info()
    self.logger.info("Directories:")
    self.logger.info("------------")
    option_labels = {}
    option_labels['top_directory'] = 'Top directory'
    option_labels['archives_directory'] = 'Archives directory'

    ljust_width = max([len(x) for x in option_labels.values()])
    ljust_width = max(ljust_width, max_width)
    for option, label in option_labels.items():
      current_cmd_opts.print_global_option(self.logger, option,
                                         label=label,
                                         width=ljust_width)
    return ljust_width

  def print_general_configuration(self, current_cmd_opts, max_width=-1):
    """
      Print project's general configuration.

      :param misc.options current_cmd_opts: Options object of current command.

      :return: Return the max width of the option labels.
      :rtype: int

    """

    self.logger.info()
    self.logger.info("General configuration:")
    self.logger.info("----------------------")
    option_labels = {}
    option_labels['platform'] = 'Platform'
    option_labels['command_verbose_level'] = 'Command verbose level'
    option_labels['archives_download_tool'] = 'Archives download tool'
    option_labels['archives_download_mode'] = 'Archives download mode'

    ljust_width = max([len(x) for x in option_labels.values()])
    ljust_width = max(ljust_width, max_width)
    for option, label in option_labels.items():
      current_cmd_opts.print_global_option(self.logger, option,
                                         label=label,
                                         width=ljust_width)
    return ljust_width

  def print_compilation_configuration(self, current_cmd_opts, max_width=-1):
    """
      Print project's general compilation configuration.

      :param misc.options current_cmd_opts: Options object of current command.

      :return: Return the max width of the option labels.
      :rtype: int

    """

    self.logger.info()
    self.logger.info("Compilation configuration:")
    self.logger.info("--------------------------")
    option_labels = {}
    option_labels['parallel_make'] = 'Parallel make flag'
    if current_cmd_opts.get_global_option("software_make_target"):
      option_labels['software_make_target'] = 'Make target'
    option_labels['xterm_make'] = 'Xterm make flag'
    option_labels['continue_on_failed'] = 'Continue on failed'
    option_labels['check_dependency_compilation'] = 'Check dependencies '\
                                                    'compilation'
    option_labels['backup_software'] = 'Backup install directory'

    ljust_width = max([len(x) for x in option_labels.values()])
    ljust_width = max(ljust_width, max_width)
    for option, label in option_labels.items():
      current_cmd_opts.print_global_option(self.logger, option,
                                         label=label,
                                         width=ljust_width)
    return ljust_width

  def print_version(self, version_object, current_command_python_version,
                    max_width=-1):
    """
      Print project's general version configuration.

      :param yamm.core.framework.version.FrameworkVersion version_object:
          Version object of current command.

      :param string current_command_python_version: Python version of current
          command.

      :return: Return the max width of the labels.
      :rtype: int

    """

    version_label = 'Version name'
    python_label = 'Python version'

    ljust_width = max([len(version_label), len(python_label), max_width])

    self.logger.info()
    self.logger.info("Version configuration")
    self.logger.info("---------------------")
    self.logger.info("{0} : {1}".format(version_label.ljust(ljust_width),
                                        version_object.name))
    self.logger.info('{0} : {1}'.format(python_label.ljust(ljust_width),
                                        current_command_python_version))
    version_object.print_softwares()
    return ljust_width

  @yamm_command("Creation of dependency graph (dot format)")
  def plot_dot(self, deptype='all', dot='', pdf='', overwrite=False, only_compiled=True,
               label=None):
    """
      Returns a graphviz representation of the software dependencies
      in the dot format. Write the result in a file and use the 'dot'
      command too generate a pdf file:
      with open('deps.dot', 'w') as f:
        project.plot_dot()
      subprocess.call(['dot', '-Tpdf', 'deps.dot', '-o', 'deps.pdf'])

      :param string deptype: choose with dependencies to plot: all, exec or build
      :param string dot: Path of file to write result in dot format
      :param string pdf: Path of pdf file to generate from dot file
      :param bool overwrite: Overwrite file if it exists
      :param bool only_compiled: If False, do not plot removed softwares
      :param string label: Label to be used in the title.

      :return: Return a string in dot format.
      :rtype: int

    """
    deptypes = ('all', 'exec', 'build')
    if deptype not in deptypes:
        self.logger.warning('Dependency type not in {}: {}'
                            .format(deptypes, deptype))
        deptype = 'all'
    current_command_softwares = self.current_command_softwares

    removed_graph = """\n subgraph cluster_removed {%removed_nodes
    colorscheme=paired12;
    color=12;
    label = "Softwares not compiled";
    }
    """
    removed_graph = misc.PercentTemplate(removed_graph)

    categories_graph = {}
    nbcat = len(self.options.get_categories())
    for index, category in enumerate(self.options.get_categories()):
        c = int(12 * (index + 1) / (nbcat + 1))
        category_graph = "\n subgraph cluster_%category {%category_nodes\n"
        category_graph += '    colorscheme=paired12;\n'
        category_graph += '    color=%s;\n' % c
        category_graph += '    label = "%category";\n}'

        categories_graph[category] = misc.PercentTemplate(category_graph)

    plot = "digraph Project_dependency_diagram {"
    plot += "\n rankdir=LR; ratio = fill;"
    if label is None:
        plot += "\n label = \"Global dependency diagram of %s softwares (version %s)\";" % (self.name, self.version)
    else:
        plot += "\n label = \"Dependency diagram of %s software (%s version %s)\";" % (label, self.name, self.version)

    soft_types = {"other": []}
    txt_nodes = {"other": "",
                 "removed": ""}
    soft_added = {"other": [],
                  "removed": []}
    for category in self.options.get_categories():
        soft_types[category] = []
        txt_nodes[category] = ""
        soft_added[category] = []

    current_command_softwares_names = [soft.name for soft in current_command_softwares]

    for soft in current_command_softwares:
        know_type = False
        soft_type = soft.get_type()
        if deptype == 'all':
            dependencies = soft.get_dependencies(recursive=True)
        elif deptype == 'exec':
            if self.is_build_only_software(soft.name) and self.current_command_options.get_global_option("separate_dependencies"):
                continue
            dependencies = soft.get_exec_dependencies()
        else:
            dependencies = soft.get_build_dependencies()
        if soft_type in soft_types and soft.name not in soft_types[soft_type]:
            soft_types[soft_type].append(soft.name)
            know_type = True
        else:
            soft_types["other"] = soft.name

        if dependencies == []:
            txt = "\n    %s;" % soft.name
            if know_type and soft.name not in soft_types[soft_type]:
                txt_nodes[soft_type] += txt
            else:
                txt_nodes["other"] += txt
        else:
            for dep_name in dependencies:
                if only_compiled and dep_name not in current_command_softwares_names:
                    continue
                if deptype == 'exec' and self.is_build_only_software(dep_name) and self.current_command_options.get_global_option("separate_dependencies"):
                    continue
                txt = "\n    %s -> %s;" % (soft.name, dep_name)
                if know_type and dep_name in soft_types[soft_type] and dep_name not in soft_added[soft_type]:
                    txt_nodes[soft_type] += txt
                    soft_added[soft_type].append(dep_name)
                elif dep_name not in soft_added["other"]:
                    txt_nodes["other"] += txt
                    soft_added["other"].append(dep_name)

                # Cas des softwares retirés de la version
                if dep_name not in current_command_softwares_names and dep_name not in soft_added["removed"]:
                    txt_nodes["removed"] += "\n    %s;" % dep_name
                    soft_added["removed"].append(dep_name)

    for soft in current_command_softwares:
        soft_type = soft.get_type()
        category = soft_type
        if category not in soft_types:
            category = "other"
        if soft.name not in soft_added[soft_type]:
            txt_nodes[category] += "\n    %s;" % soft.name

    plot += "\n"
    for category, category_graph in categories_graph.items():
        plot += category_graph.substitute(category_nodes=txt_nodes[category],
                                          category=category)
    plot += removed_graph.substitute(removed_nodes=txt_nodes["removed"])
    plot += "\n"
    plot += txt_nodes["other"]
    plot += "\n}"

    if dot and isinstance(dot, str):
        if not overwrite and os.path.exists(dot):
            self.logger.info('%s exists: dot file is not written' % dot)
            return plot
        with open(dot, 'w') as f:
            f.write(plot)

        if pdf and isinstance(pdf, str):
            if not overwrite and os.path.exists(pdf):
                self.logger.info('%s exists: pdf file is not written' % pdf)
                return plot
            subprocess.call(['dot', '-Tpdf', dot, '-o', pdf])
    return plot

#######
#
# Methods for managing dynamically the catalog project
#
#######

  def add_software_in_catalog(self, module_name, module_path):
    """
      Add a new software in the catalog of the project. A new software
      is added in *importing* the software's module.

      :param string module_name: New software Python module name
      :param string module_path: New software Python module path

    """

    module_import_name = module_path + "." + module_name
    __import__(module_import_name)
    temp_module = sys.modules[module_import_name]
    # Check de la variable software_name
    if hasattr(temp_module, "software_name"):
      self.catalog.add_software(temp_module.software_name, module_name, module_path)
      pass
    else:
      self.logger.debug("Module %s can not be added, please define software_name attribute" % module_import_name)

  def add_version_in_catalog(self, module_name, module_path):
    """
      Add a new version in the catalog of the project. A new version
      is added in *importing* the version's module.

      :param string module_name: New version Python module name
      :param string module_path: New version Python module path

    """

    module_import_name = module_path + "." + module_name
    __import__(module_import_name)
    temp_module = sys.modules[module_import_name]
    # Check de la variable version_name
    if hasattr(temp_module, "version_name"):
      self.catalog.add_version(temp_module.version_name, module_name, module_path)
      pass
    else:
      self.logger.debug("Version %s can not be added, please define version_name attribute" % module_import_name)


  def compare_with_version(self, other, category):
    v1_class = self.catalog.get_version_class(self.version)
    v2_class = self.catalog.get_version_class(other)
    v1 = v1_class(verbose=self.options.get_global_option("command_verbose_level"))
    v2 = v2_class(verbose=self.options.get_global_option("command_verbose_level"))
    v1.set_catalog(self.catalog)
    v2.set_catalog(self.catalog)
    v1.compare(v2, category)

  def get_version_custom_tasks(self):
    return []

  def add_version_tasks(self):
    self.current_command_version_object.add_tasks(self.get_version_custom_tasks())
    if self.current_command_options.get_global_option("run_version_tests"):
      self.current_command_version_object.add_post_compile_test_tasks()

#######
#
# Generic methods used by the project commands
#
#######

  def create_env_build_file(self, build_file):
    build_file_directory = os.path.dirname(build_file)
    if os.path.exists(build_file):
      try:
        os.remove(build_file)
      except:
        self.logger.exception("Error in deleting env build file: %s" % build_file)

    try:
      if not os.path.isdir(build_file_directory):
        os.makedirs(build_file_directory)
    except:
      self.logger.error("Creation of directory for env build file failed ! "
                        "Directory was: %s" % build_file_directory)

    # Starting env build file
    with open(build_file, 'w') as pre_file:
      pre_file.write("#!/bin/bash\n")
    return build_file

  def _get_log_files(self):
    logfiles = {}  # key = soft name, value = logfiles
    software_mode = "nothing"
    for software in self.current_command_softwares:
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      logfiles[software.name] = []

      # log files de compilation, installation, ...
      for k, v in software.get_infos().items():
        if k.endswith(".log"):
          logfiles[software.name].append(v)

      # log files de tests
      for test_suite in software.test_suite_list:
        logfiles[software.name] += test_suite.log_files

    # log files des tests de version
    if self.current_command_version_object.test_suite_list != []:
      logfiles[self.current_command_version_object.name] = []
    for test_suite in self.current_command_version_object.test_suite_list:
      logfiles[self.current_command_version_object.name] += test_suite.log_files

    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)
    return logfiles

  def _recover_install_directories(self):
    software_mode = "nothing"
    recover_list = []
    for software in self.current_command_softwares:
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      if os.path.exists(software.backup_directory):
        if os.path.exists(software.install_directory):
          misc.fast_delete(software.install_directory)
        shutil.move(software.backup_directory, software.install_directory)
        recover_list.append(software)

    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)
    return recover_list

  def _remove_backup_directories(self, softlist=None):
    software_mode = "nothing"
    if softlist is None or softlist == []:
      softlist = self.current_command_softwares
    for software in softlist:
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      if os.path.exists(software.backup_directory):
        misc.fast_delete(software.backup_directory)

    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)

  def _print_configuration(self):
    """
      This command prints all the informations of a project.

    """
    self.print_directories(self.current_command_options)
    self.print_general_configuration(self.current_command_options)
    self.print_compilation_configuration(self.current_command_options)
    self.print_version(self.current_command_version_object,
                       self.current_command_python_version)

  def _get_software(self, softname):
    # Get a software given its name
    soft = None
    for software in self.current_command_softwares:
        if software.name == softname:
            # Configure software
            software.set_command_env(self.current_command_softwares, self.current_command_options, 'nothing',
                                     self.current_command_version_object, self.current_command_python_version)
            soft = software
            break
    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)
    return soft

  def _get_softwares(self, softnames):
    # Get a software given its name
    softlist = [self._get_software(softname) for softname in softnames if self._get_software(softname) is not None]
    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)
    return softlist

  def _make(self, software_list=None, executor_mode="default"):
    """
      This method is called by methods start, update, make, ... to build a complete version.
      See method :py:meth:`~Project.start` for more details about the parameters.

    """
    if executor_mode in ["default", "keep_if_no_diff", "download_and_build", "build",
                         "incremental_compile", "post_install_config"]:
      check_debian = self.current_command_options.get_global_option('check_debian_packages')
      if not check_debian:
        self.logger.info('Désactivation de la vérification des paquets Debian')
      else:
        self.logger.info('Vérification des paquets Debian nécessaires à la compilation')
        if not self._check_debian_packages_build()[0]:
          return False
    # Create and add softwares to the executor
    for software in self.current_command_softwares:
      # Determine the executor mode for this software (see start method for the detailed rules)
      if software_list and software.name not in software_list:
        software_mode = "nothing"
#       elif software.use_system_version:
#         return True
      elif executor_mode != "default":
        software_mode = executor_mode
      elif self.current_command_options.get_option(software.name, "default_executor_mode"):
        software_mode = self.current_command_options.get_option(software.name, "default_executor_mode")
      else:
        software_mode = "keep_if_no_diff"

      # Configure software
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode,
                               self.current_command_version_object, self.current_command_python_version)
      software.create_tasks()

      # Add software in the executor
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
      self.current_map_name_to_softwares[software.name] = software

    # tasks for the version
    self.add_version_tasks()

    # execution
    self.create_env_files()
    self.current_command_executor.add_hooks(self.add_software_to_env_files_begin_hook,
                                            self.add_software_to_env_files_end_hook)

    if executor_mode == "collect_sha1":
      self.create_sha1_collection_file(delete=True)

    success = False
    try:
      success = self.execute_command()
    except LoggerException:
      pass

    # check generated env files
    self.check_generated_env_files()

    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)
    return success

  def _delete_directories_of(self, software_object, delete_src=False, delete_build=False, delete_install=False):
    if delete_src and software_object.can_delete_src:
      if os.path.exists(software_object.src_directory):
        try:
          misc.fast_delete(software_object.src_directory)
          try:
              os.rmdir(os.path.dirname(software_object.src_directory))
          except OSError:
              pass
          self.logger.debug("Src directory of software %s is deleted" % software_object.name)
        except:
          self.logger.exception("Exception in deleting src directory of software %s" % software_object.name)
    if delete_build:
      if os.path.exists(software_object.build_directory):
        try:
          misc.fast_delete(software_object.build_directory)
          try:
              os.rmdir(os.path.dirname(software_object.build_directory))
          except OSError:
              pass
          self.logger.debug("Build directory of software %s is deleted" % software_object.name)
        except:
          self.logger.exception("Exception in deleting build directory of software %s" % software_object.name)
    if delete_install:
      if os.path.exists(software_object.install_directory):
        try:
          misc.fast_delete(software_object.install_directory)
          try:
              os.rmdir(os.path.dirname(software_object.install_directory))
          except OSError:
              pass
          self.logger.debug("Install directory of software %s is deleted" % software_object.name)
        except:
          self.logger.exception("Exception in deleting install directory of software %s" % software_object.name)

  def _create_software_source_tgz(self, software_name, tgz_topdirectory, delete_src_dir=True):
    """
      This command creates an archive source file of a software.

      :param string software_name: Software name
      :param string tgz_topdirectory: Top directory of archives
      :param bool delete_src_dir: If True, delete the temporary source directory
    """
    # Check software
    command_current_software = None
    for current_software in self.current_command_softwares:
      if current_software.name == software_name:
        command_current_software = current_software
        break
    if not command_current_software:
      self.logger.error("Software %s not found, cannot create a source tgz" % software_name)
    software_mode = "download"
    command_current_software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
    # Do nothing for archive softwares
    if command_current_software.software_source_type == "archive":
      # This command allows to use several commands one after the others in a single yamm command
      self.reset_command(endlog=False)
      return

    # Step 1: Download software
    # Change src directory
    command_current_software.src_directory = os.path.join(tgz_topdirectory, command_current_software.executor_software_name)
    # Delete src directory for remote softwares
    command_current_software.remote_task_src_dir_policy = "delete"
    command_current_software.create_tasks()
    executor_software = command_current_software.get_executor_software()
    self.current_command_executor.add_software(executor_software)
    self.execute_command()
    # check generated env files
    self.check_generated_env_files()

    clean_command = "cd " + command_current_software.src_directory + " ; find . -name \"CVS\" | xargs rm -rf ; find . -name \".svn\" | xargs rm -rf ; find . -name \".git\" | xargs rm -rf ;find . -name \".yamm\" | xargs rm -rf ; "
    clean_command += "find . -name \"checkout.log\" | xargs rm -f"

    src_directory_name = command_current_software.archive_file_name[:command_current_software.archive_file_name.rfind(".")]
    mv_command = ""
    if src_directory_name != command_current_software.executor_software_name:
      mv_command = "cd " + tgz_topdirectory + " ; mv " + command_current_software.executor_software_name + " " + src_directory_name
    create_command = "cd " + tgz_topdirectory + " ; tar czf " + command_current_software.archive_file_name + " " + src_directory_name

    # Step 2: Create tgz
    ier = os.system(clean_command)
    if ier != 0:
      # This command allows to use several commands one after the others in a single yamm command
      self.reset_command(endlog=False)
      self.logger.error("Error number %s during clean command, command was %s" % (str(ier), clean_command))
    if mv_command != "":
      ier = os.system(mv_command)
      if ier != 0:
        # This command allows to use several commands one after the others in a single yamm command
        self.reset_command(endlog=False)
        self.logger.error("Error number %s during mv command, command was %s" % (str(ier), mv_command))
    ier = os.system(create_command)
    if ier != 0:
      # This command allows to use several commands one after the others in a single yamm command
      self.reset_command(endlog=False)
      self.logger.error("Error number %s during create command, command was %s" % (str(ier), create_command))

    # Step 3: Create sha1sum of archives
    sha1sum_command = "cd " + tgz_topdirectory + " ; sha1sum " + command_current_software.archive_file_name + " > " + command_current_software.archive_file_name + ".sha1"
    ier = os.system(sha1sum_command)
    if ier != 0:
      self.logger.warning("(sha1sum failed)")

    # Step 4: Delete source directory
    if delete_src_dir:
      index_first_slash = command_current_software.executor_software_name.find("/")
      if index_first_slash != -1:
        command_current_software.src_directory = os.path.join(tgz_topdirectory, command_current_software.executor_software_name[:index_first_slash])
      if os.path.exists(command_current_software.src_directory):
        try:
          misc.fast_delete(command_current_software.src_directory)
        except:
          # This command allows to use several commands one after the others in a single yamm command
          self.reset_command(endlog=False)
          self.logger.exception(" ".join(["In deleting directory", command_current_software.src_directory]))
      if os.path.exists(os.path.join(tgz_topdirectory, src_directory_name)):
        try:
          misc.fast_delete(os.path.join(tgz_topdirectory, src_directory_name))
        except:
          # This command allows to use several commands one after the others in a single yamm command
          self.reset_command(endlog=False)
          self.logger.exception(" ".join(["In deleting directory", os.path.join(tgz_topdirectory, src_directory_name)]))

    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)


#######
#
# Yamm project commands
#
#######

  @yamm_command("Get software command")
  def get_software(self, softname):
    return self._get_software(softname)

  @yamm_command("Get softwares command")
  def get_softwares(self, softnames):
    return self._get_softwares(softnames)

  @yamm_command("Nothing command")
  def nothing(self):
    return self._make(executor_mode="nothing")

  @yamm_command()
  def get_log_files(self):
    return self._get_log_files()

  @yamm_command("Recover install directories from backup")
  def recover_install_directories(self):
    return self._recover_install_directories()

  @yamm_command("Remove install backup directories")
  def remove_backup_directories(self, softlist=None):
    return self._remove_backup_directories(softlist)

  @yamm_command()
  def print_configuration(self):
    return self._print_configuration()

  @yamm_command("Download sources")
  def download(self, software_list=None):
    return self._make(software_list, executor_mode="download")

  @yamm_command("Make command")
  def make(self, software_list=None, executor_mode="download_and_build"):
    """
      This command is identical to :py:meth:`~Project.start` but use the
      executor mode **download_and_build**.

    """
    return self._make(software_list, executor_mode)

  @yamm_command("Start command")
  def start(self, software_list=None, executor_mode="default"):
    """
    This command builds a complete version by building all the softwares and by running version
    tasks, if any.

    :param [string] software_list: List of softwares to build. If :const:`None`, all softwares in
                                   the version are built. Default value is :const:`None`.
    :param string executor_mode: Executor mode to use for the softwares (see list below). Default
                                 value is "default".

    The possible executor modes are:

    - **default**: This mode uses the value of the option "default_executor_mode". If this
      option is not set, this mode is identical to **keep_if_no_diff**.
    - **keep_if_no_diff**: With this mode, the local sources of the software are compared to the
      remote sources. If there is any difference or if the software was not previously installed,
      the software is completely rebuilt. Otherwise the software is left unchanged.
    - **keep_if_installed**: The software is left unchanged if it has already been installed
      successfully. Otherwise it is fully built from local sources.
    - **download_and_build**: The sources for the software are downloaded or updated, then the
      software is built completely.
    - **build**: The software is built completely using local sources.
    - **download**: The sources for the software are downloaded or updated.
    - **incremental_compile**: The software is built but the build and install directories are not
      deleted before, and the configure step is skipped. For most softwares, it means the software
      is compiled with only the commands "make; make install".
    - **install_from_binary_archive**: The software is installed from a precompiled binary archive.
    - **run_tests**: The software is only tested, not compiled.
    - **create_binary_archive**: Create a binary archive from the installation directory.
    - **collect_sha1**: Collect sha1 of a software from the install_ok_xxx file.
    - **nothing**: The software is left untouched. Only the environment file(s) are completed with
      the software environment variables.

    The executor mode for each software can sometimes be tricky to guess. The following rules
    determine which executor mode is used for each software. The rules are applied in this order
    (A > B > C > D):

    A. **software_list** parameter: If this parameter is not :const:`None`, all the softwares that
       are **NOT** in this list are "built" with executor mode **nothing**.
    B. **executor_mode** parameter: For softwares that **ARE** in the **software_list** parameter
       (all softwares if software_list is :const:`None`), the executor mode is determined by the
       parameter **executor_mode** if it is not "default".
    C. **default_executor_mode** option: If the **executor_mode** parameter is "default", the
       executor mode is determined by the option **default_executor_mode** if it is set. Note that
       in some projects, this option is set in the project default options values.
    D. **keep_if_no_diff**: If the option **default_executor_mode** is not set, the mode
       **keep_if_no_diff** is used.

    """
    return self._make(software_list, executor_mode)

  @yamm_command("Update command")
  def update(self, software_list=None, update_mode="default"):
    if software_list and update_mode == 'run_tests':
      switched = {}
      if not isinstance(software_list, list):
        software_list = [software_list]
      for softname in software_list:
        run_tests = self.current_command_options.get_option(softname, "run_tests")
        if not run_tests:
          switched[softname] = run_tests
          self.current_command_options.set_software_option(softname, "run_tests", True)
      ret = self._make(software_list, update_mode)
      for softname, run_test in switched.items():
        self.current_command_options.set_software_option(softname, "run_tests", run_test)
      return ret
    else:
      return self._make(software_list, update_mode)

  @yamm_command("Creation of the binary archives")
  def create_binary_archive(self, software_list=None):
    return self._make(software_list, "create_binary_archive")

  @yamm_command("Collection of sha1 of softwares")
  def collect_sha1(self, software_list=None):
    return self._make(software_list=software_list,
                      executor_mode="collect_sha1")

  @yamm_command("Installation from binary archives")
  def install_from_binary_archive(self, software_list=None):
    """
    Use binaries archives created by :py:meth:`~Project.create_binary_archive` to install the current command's softwares.

    """
    return self._make(software_list, "install_from_binary_archive")

  @yamm_command("Run tests command")
  def run_tests(self, software_list=None):
    """
      This command runs the tests for all softwares and for the version.
      The softwares themselves are not modified.

    """
    return self._make(software_list, "run_tests")

  @yamm_command("Delete of directories")
  def delete_directories(self, software_list=None, delete_src=False,
                         delete_build=False, delete_install=False):
    if software_list is None:
      software_list = ["ALL"]
    software_mode = "nothing"
    for software in self.current_command_softwares:
      # Step 1: configuration du logiciel
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      if (software.name in software_list) or (software_list == ["ALL"]):
        self._delete_directories_of(software, delete_src, delete_build, delete_install)

  @yamm_command("Creation of an archive from a software source directory")
  def create_software_source_tgz(self, software_name, tgz_topdirectory,
                                 delete_src_dir=True):
    return self._create_software_source_tgz(software_name, tgz_topdirectory,
                                            delete_src_dir)

  @yamm_command("Get list of used system Debian packages")
  def get_debian_packages(self):
    return self._get_debian_packages()

  @yamm_command("Get list of Debian packages dependencies for runtime")
  def get_debian_depends(self):
    return self._get_debian_depends()

  @yamm_command("Get list of Debian packages dependencies for build")
  def get_debian_build_depends(self):
    return self._get_debian_build_depends()

  @yamm_command("Compute debian runtime packages from software list")
  def compute_debian_runtime_packages_from_softwares(self, software_list=None):
      return self._compute_debian_packages_from_softwares(software_list)

  @yamm_command("Check installed Debian packages dependencies for runtime")
  def check_debian_packages_build(self):
      return self._check_debian_packages_build()

  @yamm_command("Check installed Debian packages dependencies for build")
  def check_debian_packages_exec(self):
      return self._check_debian_packages_exec()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from past.builtins import execfile


from PyQt4 import QtGui, QtCore
import os
import sys
import time

from yamm.core.base import misc
from yamm.core.framework.gui import gui_project
from yamm.core.framework.gui.start_widget import FrameworkIntroWidget


history_file_text = """#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) %year EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

history_files = %history_files
"""
history_file_text = misc.PercentTemplate(history_file_text)

class FrameworkProjectGui(QtCore.QObject):

  def __init__(self, app=None, yamm_class_name=""):
    QtCore.QObject.__init__(self)
    self.app = app
    if app is None:
      self.app = QtGui.QApplication(sys.argv)

    self.yamm_class_name = yamm_class_name
    self.projects = {}
    home_dir = os.path.expanduser("~")
    self.history_file_path = os.path.join(home_dir, ".yamm_gui_history.py")
    if self.yamm_class_name:
      self.history_file_path = os.path.join(home_dir, 
                      ".yamm_{0}_gui_history.py".format(self.yamm_class_name))
    self.history_files = [] #["", "", "", "", ""]

  def getIntroWidget(self):
    return FrameworkIntroWidget(self)

  def create_widgets(self):
    # YAMM GUI main window
    self.intro_widget = self.getIntroWidget()
    if self.yamm_class_name:
      self.intro_widget.setWindowTitle(self.tr("YAMM for {0}".\
                                        format(self.yamm_class_name.upper())))
    else:
      self.intro_widget.setWindowTitle(self.tr("YAMM"))
    self.intro_widget.setWindowIcon(
        QtGui.QIcon(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                 'gui/icons/yamm-icon.svg')))
    self.center_gui()

  def center_gui(self):
    screen = QtGui.QDesktopWidget().screenGeometry()
    size = self.intro_widget.geometry()
    self.intro_widget.move((screen.width() - size.width()) // 2,
                           (screen.height() - size.height()) // 2)

  def init_history(self):
    # Check if history exists
    if not os.path.exists(self.history_file_path):
      # Create history file
      with open(self.history_file_path, 'w') as history_file:
        history_file.write(history_file_text.substitute(
                                  history_files=self.history_files,
                                  year=time.strftime("%Y", time.localtime())))

    # Read history file
    execfile(self.history_file_path, globals())
    # history_files is obtained after the execution of the YAMM gui file
    # It is a list of strings
    self.history_files = globals().get("history_files", [])
    self.history_files.reverse()
    for project_fullname in self.history_files:
      if project_fullname.strip() != "":
        self.intro_widget.add_recent_project(project_fullname)
    self.history_files.reverse()

  def add_recent_project(self, project):

    # Add recent file in the history file
    self.history_files.reverse()
    if self.history_files.count(project.gui_filename) == 1:
      self.history_files.remove(project.gui_filename)
    self.history_files.append(project.gui_filename)
    self.history_files.reverse()
    if len(self.history_files) == 6:
      self.history_files = self.history_files[:-1]

    # Save history file
    with open(self.history_file_path, 'w') as history_file:
      history_file.write(history_file_text.substitute(
                                  history_files=self.history_files,
                                  year=time.strftime("%Y", time.localtime())))

    # Add recent file in the main widget list
    self.intro_widget.add_recent_project(project.gui_filename)

  def remove_recent_project(self, project_fullname):
    if project_fullname in list(self.projects.keys()):
      del self.projects[project_fullname]
    self.intro_widget.remove_recent_project(project_fullname)
    if project_fullname in self.history_files:
      self.history_files.remove(project_fullname)
      self.history_files.append("")
    # Save history file
    with open(self.history_file_path, 'w') as history_file:
      history_file.write(history_file_text.substitute(
                                  history_files=self.history_files,
                                  year=time.strftime("%Y", time.localtime())))

  def get_yamm_gui_project(self):
    return gui_project.FrameworkYammGuiProject()

  def add_new_project_with_wizard(self, wizard):
    # Create new project object
    new_project = self.get_yamm_gui_project()
    self.initYammProjectFromWizard(new_project, wizard)
    self.add_project(new_project)

  def initYammProjectFromWizard(self, new_project, wizard):
    new_project.project_name = wizard.project_name
    new_project.project_files_directory = wizard.project_files_directory
    new_project.version = wizard.version
    new_project.terminal = wizard.terminal
    new_project.set_env_files(wizard.project_files_directory)

    if wizard.flavour:
      new_project.version_flavour = wizard.flavour
      #new_project.add_global_option("version_flavour", wizard.flavour)
    new_project.add_global_option("top_directory",
                                  wizard.top_directory)
    new_project.add_global_option("version_directory",
                                  wizard.version_directory)
    new_project.add_global_option("archives_directory",
                                  wizard.archives_directory)
    new_project.add_global_option("parallel_make",
                                  wizard.parallel_make)
    new_project.add_global_option("continue_on_failed",
                                  wizard.continue_on_failed)
    new_project.add_global_option("clean_src_if_success",
                                  wizard.clean_src_if_success)
    new_project.add_global_option("clean_build_if_success",
                                  wizard.clean_build_if_success)
    new_project.add_global_option("software_remove_list",
                                  wizard.remove_list)

    for category, option in list(wizard.category_attributes.items()):
      option_name, option_value = option
      new_project.add_category_option(category, option_name, option_value)

  def add_project(self, new_project, create_gui_files=True):
    if create_gui_files:
      # Check project directory
      # TODO - More Checks
      if not os.path.isdir(new_project.project_files_directory):
        if os.path.exists(new_project.project_files_directory):
          QtGui.QMessageBox.critical(self.intro_widget, 
              self.tr('Cannot add project'),
              self.tr("Projet specified named directory is not a directory"),
              QtGui.QMessageBox.Ok)
          return
        else:
          try:
            os.mkdir(new_project.project_files_directory)
          except:
            QtGui.QMessageBox.critical(self.intro_widget, 
                self.tr('Cannot add project'),
                self.tr("Projet specified named directory cannot be created"),
                QtGui.QMessageBox.Ok)
            return

      # Create project files
      new_project.create_gui_project_file()
      new_project.create_yamm_config_project_file()
      new_project.create_yamm_project_files()

    self.projects[new_project.gui_filename] = new_project
    self.add_recent_project(new_project)
    self.intro_widget.open_yamm_gui_project(new_project)

  def start_gui(self):
    self.intro_widget.show()
    sys.exit(self.app.exec_())

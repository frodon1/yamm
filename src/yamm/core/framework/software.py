#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function
from builtins import str
from builtins import object
from collections import defaultdict
from itertools import chain
import codecs
import os
import re
import sys

from yamm.core.base import misc

from yamm.core.engine.config import ArchiveSoftwareConfig
from yamm.core.engine.config import SoftwareConfig
from yamm.core.engine.dependency import Dependency
from yamm.core.engine.softwares import ArchiveSoftware
from yamm.core.engine.softwares import ExecutorSoftware
from yamm.core.engine.softwares import RemoteSoftware
from yamm.core.engine.tasks.collect_sha1 import CollectSha1Task
from yamm.core.engine.tasks.compilation import AdditionalFile
from yamm.core.engine.tasks.compilation import CompileTask
from yamm.core.engine.tasks.create_binary_archive  import CreateBinaryArchiveTask
from yamm.core.engine.tasks.decompress import DecompressTask
from yamm.core.engine.tasks.get_remote import GetRemoteTask
from yamm.core.engine.tasks.install_binary_archive import InstallBinaryArchiveTask
from yamm.core.engine.tasks.post_install_config    import PostInstallConfigTask
from yamm.core.engine.tasks.remote      import RemoteTask


# To be loaded a module containing software should define an attribute software_name
# with a value equals to the name of class of the software
# software_name = "default"

class FrameworkSoftware(object):

  def __init__(self, *args, **kw):
    """ FrameworkSoftware class has 3 mandatory arguments:
    - name
    - version
    - verbose
    """
    name, version, verbose = args
    self.name = name  # kw.get('name', name)
    self.verbose = verbose  # kw.get('verbose', verbose)
    self.logger = misc.Log("[SOFTWARE %s]" % self.name, self.verbose)

    if not self.name.strip() or self.name == "NOT_CONFIGURED":
      self.logger.error("Please set a name to your software %s" % self.__class__.__name__)

    self.version = version  # kw.get('version', version)
    if self.version == "NOT_CONFIGURED":
      self.logger.error("Please set a version to your software %s" % self.__class__.__name__)

    self.sha1_version = None

    # The version scheme is the following:
    # VERSION[,SHA1][ ORDERED_VERSION]
    if self.version.strip() == "":
      self.set_ordered_version(self.version)
    else:
      version_split = self.version.strip().split()
      self.version = version_split[0]
      try:
        self.version, self.sha1_version = self.version.split(",")
      except ValueError:
        pass
      try:
        self.set_ordered_version(version_split[1])
      except IndexError:
        self.set_ordered_version(self.version)

    self.use_system_version = kw.get('use_system_version', False)

    self._py3 = False

    self.project = None

#######
#
# Variables internes
#
#######

    # Tasks for executor
    self.decompress_task = None
    self.get_remote_task = None
    self.remote_task = None
    self.compile_task = None
    self.create_binary_archive_task = None
    self.install_binary_archive_task = None
    self.post_install_config_task = None
    self.collect_sha1_task = None
    self.download_test_task_list = []
    self.post_build_test_task_list = []
    self.post_install_test_task_list = []
    self.post_move_test_task_list = []

    # For compile_task
    self.depend_list = []

    # For get and print informations
    self.infos = {}

    # Variables set before a command
    self.project_softwares = []
    self.project_softwares_dict = {}

#######
#
# Variables à définir obligatoirement
#
#######

    # For compile_task
    self.compil_type = ""

    # For mode archive
    self.archive_file_name = ""
    self.archive_type = ""

#######
#
# Variables optionnelles pouvant être définies par l'utilisateur
# Les utilisateurs doivent avoir la possibilité d'avoir le dernier
# mot sur ceux ci
#
#######

    self.src_directory = ""
    self.cmake_src_directory = ""
    self.build_directory = ""
    self.install_directory = ""
    self.backup_directory = ""
    self.additional_src_files = []
    self.binary_archive_url = ""
    self.binary_archive_topdir = None
    self.clean_src_if_success = False
    self.clean_build_if_success = False

    # For mode archive
    self.archive_address = ""

    # For compile task
    self.xterm_make = False
    self.shell = None

    self.repository_name = ""
#######
#
# Variables optionnelles pouvant être définies par l'utilisateur
# Les utilisateurs n'ont pas la possibilité d'avoir le dernier
# mot sur ceux ci
#
#######

    self.parallel_make = ""
    self.make_target = ""
    self.software_source_type = ""

#######
#
# Variables optionnelles ne pouvant pas être définies par l'utilisateur
#
#######

    # Variables prédéfinies qui peuvent être redefinies
    self.executor_software_name = ""

    # For dependency management
    self.software_dependency_list = []
    self.software_dependency_dict = defaultdict(lambda: [])

    # For mode get remote archives
    self.archive_download_mode = ""

    # For mode remote
    self.remote_type = ""
    self.remote_env = ""
    self.remote_config = defaultdict(defaultdict)
    self.remote_options = defaultdict(defaultdict)
    self.root_repository = ""
    self.tag = ""

    # For compile_task
    self.module_load = ""
    self.env_files = []
    self.user_dependency_command = ""
    self.egg_file = ""
    self.pro_file = ""
    self.gen_file = ""
    self.config_options = ""
    self.build_options = ""
    self.make_repeat = 1
    self.pre_configure_commands = []
    self.gen_commands = []
    self.post_configure_commands = []
    self.pre_build_commands = []
    self.post_build_commands = []
    self.pre_install_commands = []
    self.post_install_commands = []
    self.specific_configure_command = ""
    self.specific_build_command = ""
    self.specific_install_command = ""
    self.src_dir = "keep"
    self.build_dir = "delete_and_create"
    self.install_dir = "delete"
    self.disable_configure_generation = "no"
    self.disable_configure = "no"

    # For mode archive
    self.make_movable_archive_commands = []
    self.unmake_movable_archive_commands = []
    self.patches = []

    # For remote task
    self.remote_task_src_dir_policy = "keep"

    # For infos
    self.tips = ""

    # For tests
    self.test_suite_list = []
    self.test_suite_list_results = []
    self.run_tests = False

    self.can_delete_src = True
    self.src_decompress_dir = "delete"

    self.init_dependency_list()

#######
#
# Generic Methods
#
#######

  def first(self):
    return False

  def set_project(self, project):
    self.project = project

  def default_values_hook(self):
    pass

  def user_variables_hook(self):
    pass

  def set_ordered_version(self, version):
    if version.startswith("tags/"):
      self.ordered_version = version[5:]
    elif version.startswith("git_tag/"):
      self.ordered_version = version[8:]
    elif version.startswith("hg_tag/"):
      self.ordered_version = version[7:]
    else:
      self.ordered_version = version

  def get_build_str(self, specific_install_dir=""):
    """ Return a string which will be added in the build environment file.
    """
    pass

  def get_executor_software_name(self):
    executor_software_name = self.name.lower()
    if self.version != "":
      executor_software_name += "-" + misc.transform(self.version, dot=False, dash=False)
    return executor_software_name

  def update_executor_software_name_with_patches(self):
    if(self.software_source_type == "archive" and self.patches != []):
      self.logger.debug("Add EDFp%d to executor software name" % len(self.patches))
      exec_name = self.get_executor_software_name()
      end_exec_name = self.executor_software_name[len(exec_name):]
      self.executor_software_name = exec_name + "EDFp%d" % len(self.patches)
      self.executor_software_name += end_exec_name

  def get_substitute_files(self):
    topdirectory = self.project_options.get_global_option('top_directory')
    for root, dirs, files in os.walk(self.install_directory):
        dirs[:] = [d for d in dirs if not d.startswith('.')]
        for f in files:
            full_f = os.path.join(root, f)
            if os.path.islink(full_f):
                continue
            if f.startswith('.'):
                continue
            rel_f = os.path.relpath(full_f, self.install_directory)
            if not misc.istext(full_f):
                continue
            substitute = False
            for line in codecs.open(full_f).readlines():
                if re.search(topdirectory, line):
                    substitute = True
                    break
            if substitute:
                yield rel_f

  def set_command_env(self, project_softwares, project_options, software_mode, version_object, python_version):
    """
    Cette méthode appellée par le projet donne accés au logiciel
    des variables pour produire le software pour l'executeur
    """
    self.project_softwares = project_softwares
    self.project_softwares_dict = {}
    for software in self.project_softwares:
        self.project_softwares_dict[software.name] = software

    self.project_options = project_options
    self.software_mode = software_mode
    self.version_object = version_object
    self._py3 = version_object._py3
    self.python_version = python_version

    # Step 0: Init variables that can be changed by user (user do not have priority)
    if self.project_options.is_software_option(self.name, "software_parallel_make"):
      self.parallel_make = self.project_options.get_option(self.name, "software_parallel_make")
    if self.project_options.is_software_option(self.name, "software_make_target"):
      self.make_target = self.project_options.get_option(self.name, "software_make_target")
    if self.software_source_type == "":
      self.software_source_type = self.project_options.get_option(self.name, "source_type")

    # Step 1: calculate software executor name with patches
    self.executor_software_name = self.get_executor_software_name()

    # Step 2: Create dependency objet list
    for dependency_name in self.get_dependencies():
      if self.version_object.has_software(dependency_name):
        dependency_object = self.get_dependency_object_for(dependency_name)
        if dependency_object is None:
          self.logger.error("Software cannot return a dependency object for %s" % dependency_name)
        else:
          if isinstance(dependency_object, type([])):
            for depend in dependency_object:
              self.depend_list.append(depend)
          else:
            self.depend_list.append(dependency_object)
          self.update_configuration_with_dependency(dependency_name, self.version_object.get_software_ordered_version(dependency_name))

    # Step 3: Take into account patches
    self.init_patches()
    self.update_executor_software_name_with_patches()

    # Step 4: Default values for some variables
    self.src_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "src", self.executor_software_name)
    self.build_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "build", self.executor_software_name)
    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "install", self.executor_software_name)
    if self.project_options.get_option(self.name, "archive_remote_address"):
      self.archive_address = self.project_options.get_option(self.name, "archive_remote_address")
    self.xterm_make = self.project_options.get_option(self.name, "xterm_make")
    if self.project_options.get_option(self.name, "binary_archive_topdir") is not None:
      self.binary_archive_topdir = self.project_options.get_option(self.name, "binary_archive_topdir")
    if self.project_options.get_option(self.name, "binary_archive_url") is not None:
      self.binary_archive_url = self.project_options.get_option(self.name, "binary_archive_url")
    self.binary_archive_name = self.executor_software_name

    # Step 5: Default values hook
    self.default_values_hook()

    # Step 6: Init variables
    self.init_variables()

    self.backup_directory = self.install_directory + "_yamm_backup"

    # Step 7: deal with git based softwares (in remote mode)
    if self.remote_type == "git" and self.software_source_type == "remote":
      build_is_src = self.build_directory == self.src_directory
      install_is_src = self.install_directory == self.src_directory
      self.src_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                        "src", self.name)
      if build_is_src:
        self.build_directory = self.src_directory
      if install_is_src:
        self.install_directory = self.src_directory
      # Set specific env
      self.remote_env += 'GIT_SSL_NO_VERIFY=true '
      # Set git config http.sslVerify to false
      self.remote_config['git']["http.sslVerify"] = 0
      # Deal with the proxy
      # We must treat both cases where proxy is empty or not
      # If it is empty, it will be removed before the fetch command
      proxy = self.project_options.get_option(self.name, 'proxy_server')
      if self.project_options.get_option(self.name, 'git_config_proxy'):
        if proxy is None:
          proxy = ""
        # Save git configuration in git repository
        if proxy:
          self.remote_env += ' https_proxy=' + proxy
        self.remote_config['git']["remote.origin.proxy"] = proxy
        if not proxy:
          self.remote_config['git']["http.proxy"] = proxy
      else:
        if proxy:
          # Use git command with inline config
          self.remote_options['git']['remote.origin.proxy'] = proxy

    #
    # DO NOT CHANGE src_directory, build_directory or install_directory
    # BELOW THIS POINT EXCEPT VIA USER PROJECT OPTIONS
    #

    if self.remote_type in ["hg", "svn"] and self.software_source_type == "remote":
      # Deal with the proxy
      proxy = self.project_options.get_option(self.name, 'proxy_server')
      if proxy:
          self.remote_env += ' https_proxy=' + proxy
          self.remote_env += ' http_proxy=' + proxy

    # Step 8: Init variables that can be changed by user (user have priority)
    if self.project_options.is_software_option(self.name, "software_src_directory"):
      self.src_directory = self.project_options.get_option(self.name, "software_src_directory")
    if self.project_options.is_software_option(self.name, "software_build_directory"):
      self.build_directory = self.project_options.get_option(self.name, "software_build_directory")
    if self.project_options.is_software_option(self.name, "software_install_directory"):
      self.install_directory = self.project_options.get_option(self.name, "software_install_directory")
      self.backup_directory = self.install_directory + "_yamm_backup"
    if self.project_options.is_software_option(self.name, "archive_remote_address"):
      self.archive_address = self.project_options.get_option(self.name, "archive_remote_address")
    if self.project_options.is_software_option(self.name, "xterm_make"):
      self.xterm_make = self.project_options.get_option(self.name, "xterm_make")
    if self.project_options.is_software_option(self.name, "software_additional_src_files"):
      user_additional_src_files = self.project_options.get_option(self.name, "software_additional_src_files")
      for add_file in user_additional_src_files:
        if not isinstance(add_file, AdditionalFile):
          self.logger.error("additional file type has to be a compilation.AdditionalFile")
        # Step 1 Search if an additional file with the same name is already
        # in the liste
        for yamm_addfile in self.additional_src_files:
          if yamm_addfile.name == add_file.name:
            self.additional_src_files.remove(yamm_addfile)
        # Step 2 Add new file
        self.additional_src_files.append(add_file)
    if self.software_mode == "run_tests":
      self.run_tests = True
    # Add possibility to disable the tests on a software with the option "run_tests"
    if self.project_options.get_option(self.name, "run_tests") is not None:
      self.run_tests = self.project_options.get_option(self.name, "run_tests")
    if self.project_options.get_option(self.name, "clean_src_if_success"):
      self.clean_src_if_success = self.project_options.get_option(self.name, "clean_src_if_success")
    if self.project_options.get_option(self.name, "clean_build_if_success"):
      self.clean_build_if_success = self.project_options.get_option(self.name, "clean_build_if_success")

    # Step 9: user variables
    if self.project_options.is_software_option(self.name, "software_reset_config_options"):
      reset_config_options = self.project_options.get_option(self.name, "software_reset_config_options")
      if reset_config_options:
        self.config_options = ""
    if self.project_options.is_software_option(self.name, "software_additional_config_options"):
      additional_config_options = self.project_options.get_option(self.name, "software_additional_config_options")
      if self.compil_type not in ("autoconf", "cmake", "configure_with_space", "specific", "rsync", "python"):
        self.logger.warning('Option software_additional_config_options is not supported for softwares with compilation type %s' % self.compil_type)
      else:
        if self.compil_type == "specific":
          self.specific_configure_command += " " + additional_config_options
        else:
          self.config_options += " " + additional_config_options
    if self.project_options.is_software_option(self.name, "software_reset_build_options"):
      reset_build_options = self.project_options.get_option(self.name, "software_reset_build_options")
      if reset_build_options:
        self.build_options = ""
    if self.project_options.is_software_option(self.name, "software_additional_build_options"):
      additional_build_options = self.project_options.get_option(self.name, "software_additional_build_options")
      if self.compil_type not in ("make", "python"):
        self.logger.warning('Option software_additional_build_options is not supported for softwares with compilation type %s' % self.compil_type)
      else:
        self.build_options += " " + additional_build_options
    if self.project_options.is_software_option(self.name, "software_additional_env"):
      additional_env = self.project_options.get_option(self.name, "software_additional_env")
      self.user_dependency_command += additional_env

    modules = []
    module_load = self.project_options.get_option(self.name, "module_load")
    if module_load is not None:
        modules += [m for m in module_load.split(" ") if m not in modules]

    for dep in self.get_dependencies():
        if not self.version_object.catalog.has_software(dep):
            continue
        if dep not in self.project_softwares_dict and dep in self.version_object.system_softwares_dict:
            version = self.version_object.system_softwares_dict[dep][0]
            module = self.version_object.get_software_class(dep).get_module_load_name(version)
            if module and module not in modules:
                modules.append(module)
    if modules:
      self.module_load = ' '.join(modules)
      self.shell = "/bin/bash"

    if self.project_options.is_software_option(self.name, "software_additional_env_file"):
      env_file = self.project_options.get_option(self.name, "software_additional_env_file")
      if not os.path.exists(env_file):
        self.logger.error("software_additional_env_file does not exists: %s" % env_file)
      self.env_files.append(env_file)
    if self.project_options.is_software_option(self.name, "software_repository_name"):
      self.repository_name = self.project_options.get_option(self.name, "software_repository_name")
    if self.project_options.is_software_option(self.name, "binary_archive_url"):
      self.binary_archive_url = self.project_options.get_option(self.name, "binary_archive_url")

    # Add custom server for Python install softwares
    # to avoid the download from Pypi (doesnt work on clusters)
    if self.compil_type == "python":
      force_ftp_server = self.project_options.get_global_option("python_prereq_force_ftp_server")
      if force_ftp_server:
        prerequisites_server = self.project_options.get_global_option("python_prerequisites_ftp_server")
      else:
        prerequisites_server = self.project_options.get_global_option("python_prerequisites_server")
        prerequisites_server = os.path.join(prerequisites_server, 'simple/')
      if prerequisites_server:
        self.build_options += ' --index-url {0} '.format(prerequisites_server)

    # Step 10: user_variables_hook
    self.user_variables_hook()

  def create_tasks(self):

    # Decompress Task
    if self.software_source_type == "archive":
      if self.archive_type:
        self.decompress_task = DecompressTask(archive_type=self.archive_type,
                                              src_dir=self.src_decompress_dir,
                                              verbose_level=self.verbose)
      else:
        self.logger.error("archive type is mandatory for creating a decompress task for an archive software")

    # Get Remote Task
    if self.software_source_type == "archive":
      remote_address = self.archive_address
      tool = self.project_options.get_option(self.name, "archives_download_tool")
      mode = self.project_options.get_option(self.name, "archives_download_mode")
      if self.archive_download_mode != "":
        mode = self.archive_download_mode
      self.get_remote_task = GetRemoteTask(remote_address=remote_address,
                                           tool=tool,
                                           mode=mode,
                                           verbose_level=self.verbose)

    # Remote task
    if self.software_source_type == "remote":
      remote_type = self.remote_type
      remote_env = self.remote_env
      remote_config = self.remote_config
      remote_options = self.remote_options
      repository_name = self.repository_name
      root_repository = self.root_repository
      tag = self.tag
      sha1_version = self.sha1_version
      src_dir_policy = self.remote_task_src_dir_policy
      src_directory = self.src_directory
      use_compat_env = self.project_options.get_option(self.name, "use_compat_env")
      self.remote_task = RemoteTask(use_compat_env=use_compat_env,
                                    remote_type=remote_type,
                                    remote_env=remote_env,
                                    remote_config=remote_config,
                                    remote_options=remote_options,
                                    repository_name=repository_name,
                                    root_repository=root_repository,
                                    tag=tag,
                                    sha1_version=sha1_version,
                                    src_dir_policy=src_dir_policy,
                                    verbose_level=self.verbose)

    # Compile Task
    compil_type = self.compil_type
    software_source_type = self.software_source_type
    egg_file = self.egg_file
    pro_file = self.pro_file
    gen_file = self.gen_file
    config_options = self.config_options
    build_options = self.build_options
    make_repeat = self.make_repeat
    pre_configure_commands = self.pre_configure_commands
    gen_commands = self.gen_commands
    post_configure_commands = self.post_configure_commands
    pre_build_commands = self.pre_build_commands
    post_build_commands = self.post_build_commands
    pre_install_commands = self.pre_install_commands
    post_install_commands = self.post_install_commands
    specific_configure_command = self.specific_configure_command
    specific_build_command = self.specific_build_command
    specific_install_command = self.specific_install_command
    src_dir = self.src_dir
    build_dir = self.build_dir
    install_dir = self.install_dir
    patches = self.patches

    if self.project_options.get_global_option("backup_software"):
      if install_dir == "keep":
        install_dir = "backup_copy"
      else:
        install_dir = "backup"

    disable_configure_generation = self.disable_configure_generation
    disable_configure = self.disable_configure
    xterm_make = self.xterm_make
    shell = self.shell
    additional_src_files = self.additional_src_files

    self.compile_task = CompileTask(compil_type=compil_type,
                                    software_source_type=software_source_type,
                                    egg_file=egg_file,
                                    pro_file=pro_file,
                                    gen_file=gen_file,
                                    config_options=config_options,
                                    build_options=build_options,
                                    make_repeat=make_repeat,
                                    pre_configure_commands=pre_configure_commands,
                                    gen_commands=gen_commands,
                                    post_configure_commands=post_configure_commands,
                                    pre_build_commands=pre_build_commands,
                                    post_build_commands=post_build_commands,
                                    pre_install_commands=pre_install_commands,
                                    post_install_commands=post_install_commands,
                                    specific_configure_command=specific_configure_command,
                                    specific_build_command=specific_build_command,
                                    specific_install_command=specific_install_command,
                                    src_dir=src_dir,
                                    build_dir=build_dir,
                                    install_dir=install_dir,
                                    patches=patches,
                                    disable_configure_generation=disable_configure_generation,
                                    disable_configure=disable_configure,
                                    xterm_make=xterm_make,
                                    additional_src_files=additional_src_files,
                                    shell=shell,
                                    verbose_level=self.verbose,
                                    build_depends=self.get_debian_build_depends())

    if self.software_mode == "create_binary_archive":
      self.create_binary_archive_task = CreateBinaryArchiveTask(self.binary_archive_topdir,
                                                                self.binary_archive_name,
                                                                self.make_movable_archive_commands,
                                                                verbose_level=self.verbose)

    if self.software_mode == "collect_sha1":
      sha1_collection_file = self.project_options.get_global_option("sha1_collection_file")
      name_max_length = max([len(soft.name) for soft in self.project_softwares])
      version_max_length = max([len(soft.version) for soft in self.project_softwares])
      self.collect_sha1_task = CollectSha1Task(sha1_collection_file=sha1_collection_file,
                                               name_max_length=name_max_length,
                                               version_max_length=version_max_length,
                                               verbose_level=self.verbose, software_version=self.version)


    if self.software_mode == "install_from_binary_archive":
      real_url = self.binary_archive_url % {"name": self.binary_archive_name}
      unmake_movable_archive_commands = self.unmake_movable_archive_commands
      self.install_binary_archive_task = InstallBinaryArchiveTask(real_url,
            unmake_movable_archive_commands=unmake_movable_archive_commands,
            verbose_level=self.verbose)

    if self.software_mode == "post_install_config":
      post_install_commands = self.post_install_commands
      self.post_install_config_task = PostInstallConfigTask(post_install_commands=post_install_commands,
                                                            shell=shell,
                                                            verbose_level=self.verbose)

    # Test tasks
    if self.run_tests:
      self.create_download_test_tasks()
      if self.software_mode in ("build", "download_and_build",
                                "keep_if_installed", "keep_if_no_diff", "run_tests"):
        self.create_post_compile_test_tasks()
      elif self.software_mode == "install_from_binary_archive":
        self.create_post_move_test_tasks()

  def create_download_test_tasks(self):
    for test_suite in self.test_suite_list:
      if not test_suite.insource:
        self.download_test_task_list.append(test_suite.create_download_task())

  def create_post_compile_test_tasks(self):
    for test_suite in self.test_suite_list:
      (post_build, post_install) = test_suite.create_post_compile_tasks()
      self.post_build_test_task_list += post_build
      self.post_install_test_task_list += post_install

  def create_post_move_test_tasks(self):
    for test_suite in self.test_suite_list:
      self.post_move_test_task_list += test_suite.create_post_move_tasks()

  def get_test_log_files(self):
    test_log_files = []
    for test_suite in self.test_suite_list:
      test_log_files += test_suite.log_files
    return test_log_files

  def get_name_for_installer(self):
    """ Returns the name of the software as given in the environment files.
    """
    return self.executor_software_name

  def get_executor_software(self):

    # Step 0: on récupère les infos communes
    src_topdir = os.path.dirname(self.src_directory)

    # Step 1: on détermine de quel type de software il s'agit
    # software, ArchiveSoftware ou RemoteSoftware
    if self.software_source_type == "archive":
      # executor software configuration
      archive_dir = self.project_options.get_option(self.name, "archives_directory")
      software_config = ArchiveSoftwareConfig(src_topdir=src_topdir,
                                              archive_file_name=self.archive_file_name,
                                              archive_dir=archive_dir,
                                              patches=self.patches,
                                              parallel_make=self.parallel_make,
                                              make_target=self.make_target,
                                              src_directory=self.src_directory,
                                              cmake_src_directory=self.cmake_src_directory,
                                              build_directory=self.build_directory,
                                              install_directory=self.install_directory,
                                              backup_directory=self.backup_directory,
                                              clean_src_if_success=self.clean_src_if_success,
                                              clean_build_if_success=self.clean_build_if_success,
                                              can_delete_src=self.can_delete_src,
                                              python_version=self.python_version)

      # executor software
      executor_software = ArchiveSoftware(name=self.name,
                                           software_config=software_config,
                                           mode=self.software_mode,
                                           framework_software=self,
                                           get_remote_task=self.get_remote_task,
                                           decompress_task=self.decompress_task,
                                           compile_task=self.compile_task,
                                           create_binary_archive_task=self.create_binary_archive_task,
                                           install_binary_archive_task=self.install_binary_archive_task,
                                           post_install_config_task=self.post_install_config_task,
                                           collect_sha1_task=self.collect_sha1_task,
                                           download_test_task_list=self.download_test_task_list,
                                           post_build_test_task_list=self.post_build_test_task_list,
                                           post_install_test_task_list=self.post_install_test_task_list,
                                           post_move_test_task_list=self.post_move_test_task_list,
                                           module_load=self.module_load,
                                           env_files=self.env_files,
                                           user_dependency_command=self.user_dependency_command,
                                           depend_list=self.depend_list,
                                           is_python=self.is_python(),
                                           python_version=self.python_version,
                                           )
      return executor_software

    elif self.software_source_type == "software":
      # executor software configuration
      software_config = SoftwareConfig(src_topdir=src_topdir,
                                       parallel_make=self.parallel_make,
                                       make_target=self.make_target,
                                       src_directory=self.src_directory,
                                       cmake_src_directory=self.cmake_src_directory,
                                       build_directory=self.build_directory,
                                       install_directory=self.install_directory,
                                       backup_directory=self.backup_directory,
                                       clean_src_if_success=self.clean_src_if_success,
                                       clean_build_if_success=self.clean_build_if_success,
                                       can_delete_src=self.can_delete_src,
                                       python_version=self.python_version)

      # executor software
      executor_software = ExecutorSoftware(name=self.name,
                                           software_config=software_config,
                                           mode=self.software_mode,
                                           framework_software=self,
                                           compile_task=self.compile_task,
                                           create_binary_archive_task=self.create_binary_archive_task,
                                           install_binary_archive_task=self.install_binary_archive_task,
                                           post_install_config_task=self.post_install_config_task,
                                           collect_sha1_task=self.collect_sha1_task,
                                           download_test_task_list=self.download_test_task_list,
                                           post_build_test_task_list=self.post_build_test_task_list,
                                           post_install_test_task_list=self.post_install_test_task_list,
                                           post_move_test_task_list=self.post_move_test_task_list,
                                           module_load=self.module_load,
                                           env_files=self.env_files,
                                           user_dependency_command=self.user_dependency_command,
                                           depend_list=self.depend_list,
                                           is_python=self.is_python(),
                                           python_version=self.python_version,
                                           )
      return executor_software
    elif self.software_source_type == "remote":
      # executor software configuration
      software_config = SoftwareConfig(src_topdir=src_topdir,
                                        parallel_make=self.parallel_make,
                                        make_target=self.make_target,
                                        src_directory=self.src_directory,
                                        cmake_src_directory=self.cmake_src_directory,
                                        build_directory=self.build_directory,
                                        install_directory=self.install_directory,
                                        backup_directory=self.backup_directory,
                                        clean_src_if_success=self.clean_src_if_success,
                                        clean_build_if_success=self.clean_build_if_success,
                                        can_delete_src=self.can_delete_src,
                                        python_version=self.python_version)

      # executor software
      executor_software = RemoteSoftware(name=self.name,
                                         software_config=software_config,
                                         mode=self.software_mode,
                                         framework_software=self,
                                         remote_task=self.remote_task,
                                         compile_task=self.compile_task,
                                         create_binary_archive_task=self.create_binary_archive_task,
                                         install_binary_archive_task=self.install_binary_archive_task,
                                         post_install_config_task=self.post_install_config_task,
                                         collect_sha1_task=self.collect_sha1_task,
                                         download_test_task_list=self.download_test_task_list,
                                         post_build_test_task_list=self.post_build_test_task_list,
                                         post_install_test_task_list=self.post_install_test_task_list,
                                         post_move_test_task_list=self.post_move_test_task_list,
                                         module_load=self.module_load,
                                         env_files=self.env_files,
                                         user_dependency_command=self.user_dependency_command,
                                         depend_list=self.depend_list,
                                         is_python=self.is_python(),
                                         python_version=self.python_version,
                                         )
      return executor_software
    return None

  def get_archive_filename(self):
    return self.archive_file_name

  def get_dependencies(self, recursive=False):
      dependencies = set()
      if self.software_dependency_list:
          dependencies = set(self.software_dependency_list)
      else:
          dependencies = set(chain.from_iterable(list(self.software_dependency_dict.values())))
      if recursive:
          for depname in list(dependencies):
              try:
                  soft = self.project_softwares_dict[depname]
                  deps = soft.get_dependencies(recursive)
                  dependencies.update(set(deps))
              except KeyError as e:
                  pass
      dependencies = list(dependencies)
      dependencies.sort()
      return dependencies

  def get_build_dependencies(self):
    return self.software_dependency_dict['build']

  def get_exec_dependencies(self):
    return self.software_dependency_list or self.software_dependency_dict['exec']

  def is_build_dependency(self, dependency_name):
    return dependency_name in self.software_dependency_dict['build']

  def is_exec_dependency(self, dependency_name):
    return dependency_name in \
      list(chain(self.software_dependency_list,
                 self.software_dependency_dict['exec']))

  def isArtefact(self):
    """ Declare a software as an artefact used during the building process of Project.

    If the value is True, the software will not be use at runtime and will not be included into the installer.
    """
    return False

  def get_infos(self, light=False):
    """
    Cette méthode renvoie les informations à connaître
    pour installer ce logiciel
    """
    self.infos = {}
    self.infos["name"] = self.name
    self.infos["version"] = self.version
    self.infos["ordered_version"] = self.ordered_version
    if self.remote_type:
      self.infos['remote_type'] = self.remote_type
    self.infos['software_source_type'] = self.software_source_type
    if self.compil_type != "":
      self.infos["compil_type"] = self.compil_type
    if self.archive_file_name != "":
      self.infos["archive_file_name"] = self.archive_file_name
    if self.archive_type != "":
      self.infos["archive_type"] = self.archive_type
    if self.egg_file != "":
      self.infos["egg_file"] = self.egg_file
    if self.gen_file != "":
      self.infos["gen_file"] = self.gen_file
    if self.pro_file != "":
      self.infos["pro_file"] = self.pro_file
    if self.gen_commands != []:
      commands = ""
      for command in self.gen_commands:
        commands += command + " ; "
      self.infos["gen_commands"] = commands
    if self.user_dependency_command != "":
      self.infos["user_dependency_command"] = self.user_dependency_command
    if self.config_options != "":
      self.infos["config_options"] = self.config_options
    if self.specific_configure_command != "":
      self.infos["specific_configure_command"] = self.specific_configure_command
    if self.build_options != "":
      self.infos["build_options"] = self.build_options
    if self.specific_build_command != "":
      self.infos["specific_build_command"] = self.specific_build_command
    if self.specific_install_command != "":
      self.infos["specific_install_command"] = self.specific_install_command
    if self.pre_build_commands != []:
      commands = ""
      for command in self.pre_build_commands:
        commands += command + " ; "
      self.infos["pre_build_commands"] = commands
    if self.pre_install_commands != []:
      commands = ""
      for command in self.pre_install_commands:
        commands += command + " ; "
      self.infos["pre_install_commands"] = commands
    if self.post_install_commands != []:
      commands = ""
      for command in self.post_install_commands:
        commands += command + " ; "
      self.infos["post_install_commands"] = commands
    if not light:
      if self.make_movable_archive_commands != []:
        commands = ""
        for command in self.make_movable_archive_commands:
          commands += command + " ; "
        self.infos["make_movable_archive_commands"] = commands
    if self.src_directory != "":
      self.infos["src_directory"] = self.src_directory
    if self.build_directory != "":
      self.infos["build_directory"] = self.build_directory
    if self.install_directory != "":
      self.infos["install_directory"] = self.install_directory
    if self.tips != "":
      self.infos["tips"] = self.tips
    return self.infos

  def get_infos_as_tab(self):
    self.get_infos(light=True)
    tab = []
    logtab = []
    for key in sorted(self.infos.keys()):
      if key.endswith(".log"):
        logtab.append([key, "file://%s" % self.infos[key]])
      else:
        val = self.infos[key].split("\n")
        newval = []
        for v in val:
          lstrip_v = v.lstrip()
          nbspaces = len(v) - len(lstrip_v)
          newval.append("&nbsp;"*nbspaces + lstrip_v.strip())
        val = "<br/>".join(newval)
        val = val.strip()
        if val != "":
          val = ":info_raw:`%s`" % val
        tab.append([key, val])

    tab += logtab
    return tab

  def get_infos_as_string(self):
    self.get_infos()

    width = 40
    infos_str = ""
    infos_str += "Compilation informations for software %s\n" % (self.name)
    if "name" in list(self.infos.keys()):
      infos_str += "- Name:".ljust(width) + "%s\n" % (self.infos["name"])
    if "version" in list(self.infos.keys()):
      infos_str += "- Version:".ljust(width) + "%s\n" % (self.infos["version"])
    if "ordered_version" in list(self.infos.keys()):
      infos_str += "- Version for comparison:".ljust(width) + "%s\n" % (self.infos["ordered_version"])
    if "compil_type" in list(self.infos.keys()):
      infos_str += "- Compilation system:".ljust(width) + "%s\n" % (self.infos["compil_type"])
    infos_str += "- Source dir:".ljust(width) + "%s\n" % (self.infos["src_directory"])
    infos_str += "- Build dir:".ljust(width) + "%s\n" % (self.infos["build_directory"])
    infos_str += "- Install dir:".ljust(width) + "%s\n" % (self.infos["install_directory"])
    infos_str += "- Source type:".ljust(width) + "%s\n" % (self.infos["software_source_type"])
    if "remote_type" in list(self.infos.keys()):
      infos_str += "- Remote type:".ljust(width) + "%s\n" % (self.infos["remote_type"])

    if "archive_file_name" in list(self.infos.keys()):
      infos_str += "- Archive name:".ljust(width) + "%s\n" % (self.infos["archive_file_name"])
    if "archive_type" in list(self.infos.keys()):
      infos_str += "- Archive type:".ljust(width) + "%s\n" % (self.infos["archive_type"])
    if "egg_file" in list(self.infos.keys()):
      infos_str += "- Egg_file:".ljust(width) + "%s\n" % (self.infos["egg_file"])
    if "gen_file" in list(self.infos.keys()):
      infos_str += "- Gen_file:".ljust(width) + "%s\n" % (self.infos["gen_file"])
    if "pro_file" in list(self.infos.keys()):
      infos_str += "- Pro_file:".ljust(width) + "%s\n" % (self.infos["pro_file"])

    if "gen_commands" in list(self.infos.keys()):
      infos_str += "- Preconfigure commands:".ljust(width) + "%s\n" % (self.infos["gen_commands"])
    if "user_dependency_command" in list(self.infos.keys()):
      infos_str += "- Dependency commands:".ljust(width) + "%s\n" % (self.infos["user_dependency_command"])
    if "config_options" in list(self.infos.keys()):
      infos_str += "- Configure options:".ljust(width) + "%s\n" % (self.infos["config_options"])
    if "specific_configure_command" in list(self.infos.keys()):
      infos_str += "- Specific configure commands:".ljust(width) + "%s\n" % (self.infos["specific_configure_command"])
    if "build_options" in list(self.infos.keys()):
      infos_str += "- Build options:".ljust(width) + "%s\n" % (self.infos["build_options"])
    if "pre_build_commands" in list(self.infos.keys()):
      infos_str += "- Pre build commands:".ljust(width) + "%s\n" % (self.infos["pre_build_commands"])
    if "pre_install_commands" in list(self.infos.keys()):
      infos_str += "- Pre install commands:".ljust(width) + "%s\n" % (self.infos["pre_install_commands"])
    if "specific_build_command" in list(self.infos.keys()):
      infos_str += "- Specific build commands:".ljust(width) + "%s\n" % (self.infos["specific_build_command"])
    if "specific_install_command" in list(self.infos.keys()):
      infos_str += "- Specific install commands:".ljust(width) + "%s\n" % (self.infos["specific_install_command"])
    if "post_install_commands" in list(self.infos.keys()):
      infos_str += "- Post install commands:".ljust(width) + "%s\n" % (self.infos["post_install_commands"])
    if "make_movable_archive_commands" in list(self.infos.keys()):
      infos_str += "- Commands to make a movable archive:   %s\n" % (self.infos["make_movable_archive_commands"])
    if "tips" in list(self.infos.keys()):
      infos_str += "- Tips:".ljust(width) + "%s\n" % (self.infos["tips"])

    if self.software_dependency_list:
      infos_str += "- Dependencies:".ljust(width) + "%s\n" % (self.software_dependency_list)
    else:
      if self.get_build_dependencies():
        infos_str += "- Build dependencies:".ljust(width) + "%s\n" % (self.get_build_dependencies())

      if self.get_exec_dependencies():
        infos_str += "- Execution dependencies:".ljust(width) + "%s\n" % (self.get_exec_dependencies())

    return infos_str

  def print_infos(self):
    print(self.get_infos_as_string())
    print(str(self.get_infos_as_tab()))

  def replaceStringByTemplateInFile(self, string_to_replace, template_string,
                                    filename, sedSep=misc.sedSep()):
    """
      This method will insert sed commands into make_movable_archive_commands
      to replace a string by a template string. It can be used for example to remove the install path
      into the files to create movable binary archives.

      :param string string_to_replace: Defines the string to replace,
      :param string template_string: Defines the template string,
      :param string filename: Defines the name of the file, relatively to the install dir.
    """
    dirname = os.path.dirname(filename)
    template = os.path.join(dirname, '.%s.template' % os.path.basename(filename))
    self.make_movable_archive_commands.insert(0, "if [ ! -e {0} ]; then cp {1} {0}; chmod +w {0} ; fi ".format(template, filename))
    sedPattern = sedSep + string_to_replace + sedSep + template_string + sedSep
    self.make_movable_archive_commands.append("if [ -e \"{0}\" ]; then sed -i -e 's{1}g' \"{0}\"; fi".format(template, sedPattern))

  def replaceStringInFilePrePostInstall(self, command_list, string_to_replace, template_string,
                                    filename, absolute=False, new_filename="",
                                    sedSep=misc.sedSep(), fromSuffix=".conf"):
    """
      This method will insert sed commands into the given command list
      to replace a string by a template string. It can be used for example to update the install path
      into configuration files.

      :param list command_list: the commands list (pre-install, post-install)
      :param string string_to_replace: Defines the string to replace,
      :param string template_string: Defines the template string,
      :param string filename: Defines the name of the file (absolute path)
      :param bool absolute: If True, path of the file is absolute, else it is relative to self.install_directory
      :param string new_filename: Defines the target filename. If empty, the input file is the target file
      :param string sedSep: Defines the sed separator (default: given by misc.sedSep())
      :param string fromSuffix: Defines the suffix of the filename from which the input file should be a copy
    """
    sedPattern = sedSep + string_to_replace + sedSep + template_string + sedSep
    # If file ends with fromSuffix value, this should be the first time it is met
    # We copy it to a new file without the fromSuffix value extension
    if not absolute:
      filename = os.path.join("$CURRENT_SOFTWARE_INSTALL_DIR", filename)

    if new_filename:
      target_filename = new_filename
    else:
      target_filename = filename

    if fromSuffix:
      if filename.endswith(fromSuffix):
        target_filename = filename[:-len(fromSuffix)]
        conf_filename = filename
        command_list.insert(0, "if test -f {0}; then cp {0} {1}; fi; ".format(conf_filename, target_filename))
      else:
        conf_filename = filename + fromSuffix
        command_list.insert(0, "if test -f {0} && test ! -f {1}; then cp {0} {1}; fi; ".format(conf_filename, target_filename))
    else:
      if new_filename:
        command_list.insert(0, "cp {0} {1}; ".format(filename, target_filename))

    command_list.append("if [ -e \"{1}\" ]; then sed -i -e \"s{0}g\" \"{1}\"; fi".format(sedPattern, target_filename))
    return command_list

  def replaceStringInFilePreInstall(self, string_to_replace, template_string,
                                    filename, absolute=False, new_filename="",
                                    sedSep=misc.sedSep(), fromSuffix=".conf"):
    """
      This method will insert sed commands into pre_install_commands
      to replace a string by a template string. It can be used for example to update the install path
      into configuration files.

      :param string string_to_replace: Defines the string to replace,
      :param string template_string: Defines the template string,
      :param string filename: Defines the name of the file (absolute path)
      :param bool absolute: If True, path of the file is absolute, else it is relative to self.install_directory
      :param string new_filename: Defines the target filename. If empty, the input file is the target file
      :param string sedSep: Defines the sed separator (default: given by misc.sedSep())
      :param string fromSuffix: Defines the suffix of the filename from which the input file should be a copy
    """
    self.pre_install_commands = self.replaceStringInFilePrePostInstall(self.pre_install_commands,
        string_to_replace, template_string, filename, absolute,
        new_filename, sedSep, fromSuffix)

  def replaceStringInFilePostInstall(self, string_to_replace, template_string,
                                     filename, absolute=False, new_filename="",
                                     sedSep=misc.sedSep(), fromSuffix=".conf"):
    """
      This method will insert sed commands into post_install_commands
      to replace a string by a template string. It can be used for example to update the install path
      into configuration files.

      :param string string_to_replace: Defines the string to replace,
      :param string template_string: Defines the template string,
      :param string filename: Defines the name of the file (absolute path)
      :param bool absolute: If True, path of the file is absolute, else it is relative to self.install_directory
      :param string new_filename: Defines the target filename. If empty, the input file is the target file
      :param string sedSep: Defines the sed separator (default: given by misc.sedSep())
      :param string fromSuffix: Defines the suffix of the filename from which the input file should be a copy
    """
    self.post_install_commands = self.replaceStringInFilePrePostInstall(self.post_install_commands,
        string_to_replace, template_string, filename, absolute,
        new_filename, sedSep, fromSuffix)

  def replaceTemplateBySoftInstallPathInFile(self, template_string, software_name, filename):
    """
      This method will insert sed commands into unmake_movable_archive_commands
      to replace a template string by the install path of a software. It can be used for example to reset the install path
      into the files to install from movable binary archives.

      :param string template_string: Defines the template string,
      :param string software_name: Name of the software,
      :param string filename: Defines the name of the file, relatively to the install dir.
    """
    try:
        soft = self.project_softwares_dict[software_name]
    except KeyError:
        self.logger.error("While replacing template string, could not find software %s. "
                          "The file %s is not fully rebuilt." % (software_name, filename))

    # The sed command are done by the task InstallBinaryArchiveTask
    self.unmake_movable_archive_commands.append((template_string, soft, filename))

  def replaceSelfInstallPath(self, filename):
    """
      This method will insert sed commands into make_movable_archive_commands
      to replace the install path of the current software by "__INSTALL_PATH__".

      :param string filename: Defines the name of the file, relatively to the install dir.
    """
    realpath = os.path.realpath(self.install_directory)
    if len(realpath) > len(self.install_directory):
        self.replaceStringByTemplateInFile(realpath, "__INSTALL_PATH__", filename)
    self.replaceStringByTemplateInFile(self.install_directory, "__INSTALL_PATH__", filename)
    if (len(realpath) < len(self.install_directory)) or \
       (len(realpath) == len(self.install_directory) and realpath != self.install_directory):
        self.replaceStringByTemplateInFile(realpath, "__INSTALL_PATH__", filename)
    self.replaceTemplateBySoftInstallPathInFile("__INSTALL_PATH__", self.name, filename)

  def replaceSoftwareInstallPath(self, software_name, filename, template_string=""):
    """
      This method will insert sed commands into make_movable_archive_commands
      to replace the install path of another software by "__SOFTWARE_INSTALL_PATH__".

      :param string software_name: Defines the name of the software,
      :param string filename: Defines the name of the file, relatively to the install dir.
    """
    software_name = software_name.upper()
    try:
        soft = self.project_softwares_dict[software_name]
    except KeyError:
        return
    asoft_install_directory = soft.install_directory

    if not template_string.strip():
      template_string = "__" + software_name + "_INSTALL_PATH__"

    realpath = os.path.realpath(asoft_install_directory)
    if len(realpath) > len(asoft_install_directory):
      self.replaceStringByTemplateInFile(realpath, template_string, filename)

    self.replaceStringByTemplateInFile(asoft_install_directory, template_string, filename)

    if (len(realpath) < len(asoft_install_directory)) or \
       (len(realpath) == len(asoft_install_directory) and realpath != asoft_install_directory):
      self.replaceStringByTemplateInFile(realpath, template_string, filename)

    self.replaceTemplateBySoftInstallPathInFile(template_string, software_name, filename)

  def replacePythonHeaderInstallPath(self, filename):
    """
      This method will insert sed commands into make_movable_archive_commands
      to replace the install path of python in the header of the file by /usr/bin/env python.
      :param string filename: Defines the name of the file, relatively to the install dir.
    """
    try:
        soft = self.project_softwares_dict['PYTHON']
    except KeyError:
        return
    asoft_install_directory = soft.install_directory

    dirname = os.path.dirname(filename)
    template = os.path.join(dirname, '.%s.template' % os.path.basename(filename))

    self.make_movable_archive_commands.insert(0, "if [ ! -e {0} ]; then cp {1} {0}; chmod +w {0} ; fi ".format(template, filename))
    template_string = "#!/usr/bin/env python"
    if self._py3:
        template_string = "#!/usr/bin/env python3"
    sedSep = "~"
    realpath = os.path.realpath(asoft_install_directory)
    sedPattern = "1 s{0}{1}{0}{2}{0}".format(sedSep,
                                             ".*" + realpath + "/bin/python.*",
                                             template_string)
    if len(realpath) > len(asoft_install_directory):
      self.make_movable_archive_commands.append("if [ -e \"{0}\" ]; then sed -i -e '{1}' \"{0}\"; fi".format(template, sedPattern))

    sedPattern = "1 s{0}{1}{0}{2}{0}".format(sedSep, ".*" + asoft_install_directory + "/bin/python.*", template_string)
    self.make_movable_archive_commands.append("if [ -e \"{0}\" ]; then sed -i -e '{1}' \"{0}\"; fi" .format(template, sedPattern))

    if (len(realpath) < len(asoft_install_directory)) or \
       (len(realpath) == len(asoft_install_directory) and realpath != asoft_install_directory):
      sedPattern = "1 s{0}{1}{0}{2}{0}".format(sedSep, ".*" + realpath + "/bin/python.*", template_string)
      self.make_movable_archive_commands.append("if [ -e \"{0}\" ]; then sed -i -e '{1}' \"{0}\"; fi".format(template, sedPattern))

    self.replaceTemplateBySoftInstallPathInFile("", "PYTHON", filename)

  def configure_remote_software(self,
                                external_server="",
                                external_path="",
                                external_repo_name="",
                                external_submodules=None,
                                internal_server="",
                                internal_path="",
                                internal_repo_name="",
                                internal_submodules=None,
                                use_proxy=True):
    """ Configure the remote server for external and internal access.
    Usefull for external repositories mirrored locally.
    Sets the following variables for a software:
    - self.root_repository
    - self.repository_name
    And possibly update the option "proxy_server".
    :param: string external_server: address of server behind proxy
    :param: string external_path: directory of the remote in the external server
    :param: string external_repo_name: name of the repository in the external server
    :param: dict external_submodules: addresses of submodules repositories behind the proxy;
    key is the name of the submodule.
    :param: string internal_server: address of server in local network (no proxy needed)
    :param: string internal_path: directory of the remote in the internal server
    :param: string internal_repo_name: name of the repository in the internal server
    :param: dict internal_submodules: addresses of submodules repositories in local network;
    key is the name of the submodule.
    :param: bool use_proxy: flag to enable/disable the use of proxy for external server
    """
    if not external_server and not internal_server:
      self.logger.error("When configuring remote server: no server given !!")
    if self.project_options.get_option(self.name, "use_pleiade_mirrors") or not external_server:
      # Internal server => no proxy
      self.project_options.set_software_option(self.name, "proxy_server", "")
      self.remote_config['git']["remote.origin.proxy"] = ""
      if not internal_server:
        self.logger.error("When configuring remote server: no internal server given !!")
      self.root_repository = os.path.join(internal_server, internal_path)
      self.repository_name = internal_repo_name
      submodules = internal_submodules
      if not submodules and external_submodules:
        submodules = external_submodules
      if submodules:
        for submodule_name, submodule_address in list(submodules.items()):
            self.remote_config['git']['submodule.' + submodule_name + '.url'] = submodule_address

    else:
      # External server => proxy (IF IN EDF !!!!)
      if self.project_options.is_software_option(self.name, "occ_server"):
        external_server = self.project_options.get_option(self.name, "occ_server")
      if not internal_server:
        self.logger.error("When configuring remote server: no external server given !!")
      # Handle proxy
      if not self.project_options.get_option(self.name, "proxy_server"):
        if use_proxy:
          self.project_options.set_software_option(self.name, "proxy_server", "http://proxypac.edf.fr:3128")
      else:
        if not use_proxy:
          self.project_options.set_software_option(self.name, "proxy_server", "")
      self.root_repository = os.path.join(external_server, external_path)
      self.repository_name = external_repo_name
      if external_submodules:
        for submodule_name, submodule_address in list(external_submodules.items()):
            self.remote_config['git']['submodule.' + submodule_name + '.url'] = submodule_address

  def configure_remote_software_pleiade_git(self,
                                            internal_repo_name,
                                            internal_submodules=None,
                                            external_server="",
                                            external_path="",
                                            external_repo_name="",
                                            external_submodules=None,
                                            use_proxy=True):
    """
    """
    internal_server = "https://git.forge.pleiade.edf.fr"
    internal_path = "git"
    self.configure_remote_software(external_server,
                                   external_path,
                                   external_repo_name,
                                   external_submodules,
                                   internal_server,
                                   internal_path,
                                   internal_repo_name,
                                   internal_submodules,
                                   use_proxy)

  def configure_remote_software_pleiade_svn(self,
                                            internal_repo_name,
                                            external_server="",
                                            external_path="",
                                            external_repo_name="",
                                            use_proxy=True):
    """
    """
    internal_server = "https://svn.forge.pleiade.edf.fr"
    internal_path = "svn"
    self.configure_remote_software(external_server,
                                   external_path,
                                   external_repo_name,
                                   None,
                                   internal_server,
                                   internal_path,
                                   internal_repo_name,
                                   None,
                                   use_proxy)

#######
#
# Methods that can be overloaded
#
#######

  def init_dependency_list(self):
    self.software_dependency_list = []
    self.software_dependency_dict = defaultdict(lambda: [])

  def init_patches(self):
    self.patches = []

  def init_variables(self):
    pass

  @classmethod
  def get_module_load_name(cls, version, *args):
      pass

  def get_debian_dev_package(self):
    # Return the possible debian dev package names for the software
    # A list is allowed because a single software may have several packages
    return self.get_debian_package()

  def get_debian_package(self):
    # Return the possible debian package names for the software
    # A list is allowed because a package can change its name according to
    # the debian version
    return self.name.lower()

  def get_debian_depends(self):
    # Return a list of dependencies for the software to run
    # The list is for dependencies not listed in the yamm dependencies.
    return []

  def get_debian_build_depends(self):
    # Return a list of dependencies for the software to build
    # The list is for dependencies not listed in the yamm dependencies.
    return []

  def get_type(self):
    return "default"

  def get_dependency_object_for(self, dependency_software_name):
    if dependency_software_name in self.get_dependencies():
      return Dependency(name=dependency_software_name)
    else:
      return None

  def update_configuration_with_dependency(self, dependency_name, version_name):
    pass

  def get_env_str(self):
    """
      This method is aimed to be used to add sofware specific strings into a
      project environment file.
    """
    return ""

  def get_extra_test_path_list(self):
    """
    This method returns a list of paths to the software extra tests.
    Define each path relative to the software install directory.
    """
    return []

#######
#
# Méthodes spécifiques à Python
#
#######

  def is_python(self):
    """
    Retourne True si le logiciel est python
    """
    return False

  def get_python_version(self, py3=False):
    """
    Fonction à implémenter si le logiciel est Python
    Sinon renvoie la version majeure du Python utilisé par Yamm
    """
    return misc.compute_python_version(py3)[:-2]

  def get_version_for_archive(self):
    """
    Utility function to help building archive file names by removing VCS
    specific infos.
    """
    if self.version.startswith("tags/"):
      return self.version[5:]
    elif self.version.startswith("git_tag/"):
      return self.version[8:]
    elif self.version.startswith("hg_tag/"):
      return self.version[7:]
    elif self.version.startswith("branches/"):
      return self.version[9:]
    else:
      return misc.transform(self.version, dot=False, dash=False)

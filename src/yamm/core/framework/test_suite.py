#  Copyright (C) 2012 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from past.builtins import execfile
from builtins import str
from builtins import object
import datetime
import os
import subprocess
import time

from yamm.core.base.misc import VerboseLevels
from yamm.core.base.test_logger import TestLog
from yamm.core.engine.tasks.remote import RemoteTask
from yamm.core.engine.tasks.run_test_suite import RunTestSuite


class TestSuite(object):

    def __init__(self, name, soft_name="", soft_version="", version=None,
                 verbose=VerboseLevels.INFO):
        self.name = name
        self.version = version
        self.sha1_version = None
        if self.version is not None:
            try:
                self.version, self.sha1_version = self.version.split(",")
            except ValueError:
                pass
        self.verbose = verbose

        self.soft_name = soft_name
        self.soft_version = soft_version

        self.remote_task_src_dir_policy = "keep"
        self.src_directory = None
        self.work_directory = None
        self.remote_system_type = None
        self.remote_system_env = ""
        self.remote_system_config = {}
        self.remote_system_options = {}
        self.repository_name = None
        self.root_repository = None

        self.use_compat_env = False

        self.insource = True
        self.post_build = False
        self.post_install = False
        self.post_move = False
        self.test_result = 0
        self.failed_tests = []
        self.succeeded_tests = []
        self.known_errors = []
        self.test_log = None
        self.log_files = []
        self.test_log_files = {}  # key = str(test number), value = test log file

    def nbTests(self):
        return 0

    def nbKnownFailedTests(self):
        return len(self.known_errors)

    def nbFailedTests(self):
        return len(self.failed_tests)

    def nbSucceededTests(self):
        return len(self.succeeded_tests)

    def create_post_compile_tasks(self):
        post_build_tasks = []
        post_install_tasks = []
        real_tasks = []
        # if not self.insource:
        #     real_tasks.append(self.create_download_task())
        real_tasks.append(RunTestSuite(self))
        if self.post_build:
            post_build_tasks = real_tasks
        elif self.post_install:
            post_install_tasks = real_tasks
        return (post_build_tasks, post_install_tasks)

    def create_post_move_tasks(self):
        post_move_tasks = []
        # TODO
        return post_move_tasks

    def create_download_task(self):
        task = RemoteTask(use_compat_env=self.use_compat_env,
                          remote_type=self.remote_system_type,
                          remote_env=self.remote_system_env,
                          remote_config=self.remote_system_config,
                          remote_options=self.remote_system_options,
                          repository_name=self.repository_name,
                          root_repository=self.root_repository,
                          tag=self.version,
                          sha1_version=self.sha1_version,
                          src_dir_policy=self.remote_task_src_dir_policy,
                          src_directory=self.src_directory,
                          verbose_level=self.verbose)
        task.set_software_name(self.name)
        return task

    def create_test_log(self, command_log):
        return TestLog(self.name,
                       self.soft_name,
                       self.soft_version,
                       command_log.verbose_level,
                       command_log.log_directory,
                       command_log.print_console,
                       command_log.write_in_file,
                       command_log.log_files_basename)

    def execute_test(self, command_log, *args):
        return "Error, method execute_test is not implemented for test series %s" % self.name


class DownloadOnlyTestSuite(TestSuite):
    def execute_test(self, *args, **kwargs):
        return ""


class CommandTestSuite(TestSuite):
    def __init__(self, name, soft_name="", soft_version="", version=None,
                 command=None, verbose=VerboseLevels.INFO):
        TestSuite.__init__(self, name, soft_name, soft_version, version, verbose)
        self.command = command

    def nbTests(self):
        return 1

    def execute_test(self, command_log, dependency_command, work_directory, logfile, space):
        self.log_files.append(logfile)
        self.test_log_files["1"] = logfile
        msg = ""
        self.test_log = self.create_test_log(command_log)
        self.test_log.start_test(self)
        errors = ""
        comment = ""
        exe_tab = []
        self.test_log.print_begin_work("Run tests", space=space)
        if self.command is not None:
            command = "(cd " + work_directory + " && "
            if dependency_command:
                command += dependency_command
            command += self.command
            command += " ) > " + logfile + " 2>&1"
            self.test_log.print_debug("Command is: %s" % command)
            self.test_result = 0
            t_test = int(time.time())
            if self.verbose < VerboseLevels.DRY_RUN:
                self.test_result = subprocess.call(command, shell=True)
            t_test = int(time.time()) - t_test
            total_time = str(datetime.timedelta(seconds=t_test))

        state = "|OK|"
        if self.test_result != 0:
            log_file_link = '`{0} <file://{1}>`_'.format(os.path.basename(logfile), logfile)
            msg_error = "Result=%d, see log file %s for more details" % (self.test_result, log_file_link)
            comment = "See error [#]_."
            errors += "\n" + ".. [#] " + msg_error
            state = "|ERROR|"
            self.failed_tests = [self.command]
            msg = "Error in test, command was: %s" % command
        else:
            self.succeeded_tests = [self.command]
            self.test_log.print_end_work()

        exe_tab.append((self.name, state, str(total_time), comment))
        self.test_log.test_suite_end(exe_tab, errors)
        return msg


# Pre-defined test suites
class MakeCheckTestSuite(CommandTestSuite):
    def __init__(self, name="make_check", soft_name="", soft_version="",
                 version=None, parallel_flag="1", verbose=VerboseLevels.INFO):
        command = "make -j{0} check".format(parallel_flag)
        CommandTestSuite.__init__(self, name, soft_name, soft_version, version, command, verbose)
        self.post_build = True


class MakeInstallcheckTestSuite(CommandTestSuite):
    def __init__(self, name="make_installcheck", soft_name="", soft_version="",
                 version=None, parallel_flag="1", verbose=VerboseLevels.INFO):
        command = "make -j{0} installcheck".format(parallel_flag)
        CommandTestSuite.__init__(self, name, soft_name, soft_version, version, command, verbose)
        self.post_install = True


class MakeTestSuite(CommandTestSuite):
    def __init__(self, name="make_test", soft_name="", soft_version="",
                 version=None, parallel_flag="1", verbose=VerboseLevels.INFO):
        command = "make -j{0} test".format(parallel_flag)
        CommandTestSuite.__init__(self, name, soft_name, soft_version, version, command, verbose)
        self.post_build = True


class ScriptListTestSuite(TestSuite):

    def __init__(self, name, soft_name="", soft_version="", version=None, verbose=VerboseLevels.INFO):
        TestSuite.__init__(self, name, soft_name, soft_version, version, verbose)
        self.test_list = None
        self.test_list_file = None

    def nbTests(self):
        self.init_test_env()
        return len(self.test_list)

    def execute_test(self, command_log, dependency_command, work_directory, logfile, space):
        self.log_files.append(logfile)
        self.test_log = self.create_test_log(command_log)
        msg = self.init_test_env()
        if msg != "":
            return msg
        self.test_log.start_test(self)
        errors = ""
        exe_tab = []

        # Create the log directory
        test_suite_log_directory = os.path.join(work_directory, "logs", "test_" + self.name)
        if not os.path.isdir(test_suite_log_directory):
            os.makedirs(test_suite_log_directory)

        # Go to working directory
        current_directory = os.getcwd()
        self.test_log.print_debug("Test suite working directory: %s" % work_directory)
        os.chdir(work_directory)

        # Run the tests
        self.failed_tests = []
        test_index = 1
        for numtest, test in enumerate(self.test_list):
            comment = ""
            t_test = int(time.time())
            self.test_log.print_begin_work("Run test %d: %s" % (test_index, test.script_file), space=2)
            logfile = os.path.join(test_suite_log_directory, "Test-%d.log" % test_index)
            self.test_log_files[str(numtest)] = logfile
            command = dependency_command
            if test.interpreter is not None:
                command += test.interpreter + " "
            command += test.script_file
            command += " > %s 2>&1" % logfile
            self.test_log.print_debug("Running command: " + command)
            test_result = 0
            if self.verbose < VerboseLevels.DRY_RUN:
                test_result = subprocess.call(command, shell=True)
            t_test = int(time.time()) - t_test
            total_time = str(datetime.timedelta(seconds=t_test))
            state = "|OK|"
            if test_result == 0:
                self.succeeded_tests.append(test.script_file)
                self.test_log.print_end_work()
            else:
                msg_error = "Result=%d, see log file %s for more details" % (test_result, logfile)
                comment = "See error [#]_."
                errors += "\n" + ".. [#] " + msg_error
                self.test_log.print_end_work_with_warning("Test failed, " + msg_error)
                self.test_result |= test_result
                self.failed_tests.append(test.script_file)
                state = "|ERROR|"
            test_index += 1
            exe_tab.append((os.path.basename(test.script_file), state, str(total_time), comment))

        os.chdir(current_directory)

        self.test_log.test_suite_end(exe_tab, errors)
        if self.test_result != 0:
            return "Error in tests %s" % self.failed_tests
        return ""

    def init_test_env(self):
        if self.test_list is None and self.test_list_file is None:
            return "Error in tests, no script list defined for test series %s" % self.name

        if self.src_directory is not None:
            os.environ["%s_ROOT_DIR" % self.name] = self.src_directory

        # If the test list is not directly defined, load it from test_list_file
        if self.test_list is None:
            if not os.path.exists(self.test_list_file):
                return "Error in tests, test list file %s does not exist" % self.test_list_file
            test_list_context = {}
            execfile(self.test_list_file, test_list_context)
            self.test_list = test_list_context["test_list"]

        return ""


class TestScript(object):

    def __init__(self, script_file=None, interpreter=None):
        self.script_file = script_file
        self.interpreter = interpreter

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import object
import collections
import itertools

from yamm.core.base import misc


# It's mandatory to define version_name with the name of version class
# version_name = "version"
class FrameworkVersion(object):
    """
      .. py:currentmodule:: yamm.core.framework.FrameworkVersion

      Base class for versions. It defines the common parameters
      of YAMM versions.

      :param name: Name of the version
      :type name: string
      :param verbose: Verbose level of the version
      :type verbose: int
      :param flavour: name of the flavour (ie a variation of the version)
      :type flavour: string

    """

    def __init__(self, name='NOT CONFIGURED', verbose=0, flavour=""):
        self.name = name
        # software name / the version
        self.softwares = {}
        # software name / software version, [packages names], [packages versions]
        self.system_softwares_dict = collections.defaultdict(lambda: ['', [], []])
        # software name / (min, max)
        self.softwares_with_ranges_dict = collections.defaultdict(lambda: ['', ] * 2)
        # software name / the ordered version (with a ' ' in prefix)
        self.softwares_with_ordered_version_dict = {}
        # software name / list of groups
        self.software_to_groups_dict = {}
        # group name / list of softwares name
        self.group_to_softwares_dict = collections.defaultdict(set)
        # software name / list of parent software names
        self.software_parents_dict = collections.defaultdict(set)
        # list of software names being removed (used for the recursion)
        self.softwares_being_removed = []
        # List of softwares for which system packages should be used
        self.system_softwares = []
        # List of system dependencies (debian packages)
        self.debian_depends = []

        self.verbose = verbose
        self.logger = misc.Log('[VERSION ' + name + ']', verbose)
        self.catalog = None
        self.tasks = []
        self.flavour = flavour
        self.accepted_flavours = []

        if 'NOT CONFIGURED' == self.name:
            self.logger.error('Please set a name to your version {0}'
                              .format(self.__class__.__name__))

        # For tests
        self.test_suite_list = []

        # For python3
        self._py3 = False

    def configure_softwares(self):
        """
        This method is used to add softwares to the version.
        It is called during the initialisation of the object.
        """
        pass

    @misc.debian_compliant
    def add_debian_depends(self, depends):
        from yamm.core.base import debian_tools
        if not isinstance(depends, list):
            depends = [depends]
        depends = [dep for dep in depends if dep.strip() and debian_tools.exists(dep) and dep not in self.debian_depends]
        self.debian_depends += depends

    def add_debian_depend(self, depend):
        self.add_debian_depends(depend)

    def has_flavour(self, flavour):
        return flavour in self.accepted_flavours

    def check_flavour(self, flavour):
        if flavour and not self.has_flavour(flavour):
            msg = 'Version {0} will ignore the flavour ( {1} )'\
                .format(self.name, flavour)
            msg += '\nIt is not in the accepted flavours: {0}'\
                .format(self.accepted_flavours)
            self.logger.warning(msg)

    def update_configuration_with_flavour(self):
        """
        This method is used to update the configuration with
        a given flavour.
        It is called by :py:meth:`~version.update_flavour`.
        """
        self.check_flavour(self.flavour)

    def update_flavour(self, flavour):
        """
        This method updates the flavour with the given one.
        The new flavour must be in the authorized flavour list.

        :param string flavour: The new flavour of the version
        """
        if flavour:
            self.flavour = flavour
        self.update_configuration_with_flavour()

    def set_catalog(self, catalog):
        """
        This method sets the catalog of the version.

        :param catalog.catalog catalog: The catalog of the version
        """
        self.catalog = catalog

    def configure_tests(self, project_options):
        pass

    def init_configuration(self, catalog):
        self.catalog = catalog
        self.system_softwares = []
        self.system_softwares_dict.clear()
        self.softwares_with_ranges_dict.clear()
        self.softwares_with_ordered_version_dict.clear()
        self.software_to_groups_dict.clear()
        self.group_to_softwares_dict.clear()
        self.software_parents_dict.clear()
        self.softwares_being_removed = []
        self.configure_softwares()
        self.update_configuration_with_flavour()

    def update_configuration(self):
        self.update_software_parents()
        if self.system_softwares:
            self.set_system_versions()

    def update_software_parents(self):
        self.software_parents_dict.clear()
        for name, version in self.softwares.items():
            software = self.get_software(name, version)
            software.init_dependency_list()
            for depend_soft in software.get_dependencies():
                self.software_parents_dict[depend_soft].add(name)

    def add_soft_system_version(self, softnames):
        if type(softnames) is not list:
            softnames = [softnames]
        for softname in [name for name in softnames if name in self.softwares]:
            if softname not in self.system_softwares:
                self.system_softwares.append(softname)

    def set_system_versions(self, softwares_with_ranges=None, excludes=None):
        if not self.system_softwares:
            return
        for name in self.get_sorted_softwares():
            # Software may have been removed by recursion
            if name not in self.softwares:
                continue
            version = self.softwares[name]
            self.system_softwares_dict[name] = [version, [], []]
            if excludes and name in excludes:
                continue
            min_version, max_version = self.softwares_with_ranges_dict[name]
            if softwares_with_ranges and name in softwares_with_ranges:
                min_version, max_version = softwares_with_ranges[name]
            if name in self.system_softwares:
                self.set_soft_system_version(name, version, min_version,
                                             max_version)

    def set_soft_system_version(self, name, version, min_version, max_version):
        if not min_version and not max_version:
            return
        msg = '[{0}] Finding suitable system version'.format(name)
        if min_version:
            msg += ' (> {0}'.format(min_version)
        if max_version:
            if min_version:
                msg += ' and'
            else:
                msg += ' ('
            msg += ' < {0}'.format(max_version)
        if min_version or max_version:
            msg += ')'
        self.logger.debug()
        self.logger.debug(msg)
        try:
            version = self.softwares[name]
        except KeyError:
            if name not in self.system_softwares:
                self.logger.debug('[{0}] software not found'.format(name))
                return
            version = self.system_softwares_dict[name][0]
        soft = self.get_software(name, version)
        # If software has patches, do not use the system version
        if soft.patches:
            return

        # Only Debian/Ubuntu compatible for now
        if not misc.is_on_debian() and not misc.is_on_ubuntu():
            return

        # Initialize dev_packages to an empty list if
        # get_debian_dev_package returns None
        # (happens when method has no return statement)
        dev_packages = soft.get_debian_dev_package() or []
        if not isinstance(dev_packages, list):
            dev_packages = [dev_packages]
        from yamm.core.base import debian_tools
        dev_packages = [p for p in dev_packages if debian_tools.exists(p)]

        # Initialize runtime_packages to an empty list if
        # get_debian_package returns None
        # (happens when method has no return statement)
        runtime_packages = soft.get_debian_package() or []
        if not isinstance(runtime_packages, list):
            runtime_packages = [runtime_packages]
        runtime_packages = [p for p in runtime_packages
                            if debian_tools.exists(p)]

        packages = dev_packages + [p for p in runtime_packages
                                   if p not in dev_packages]
        if not packages:
            self.logger.debug('[{0}] No system package found'.format(name))
            return

        self.logger.debug('[{0}] Checking for packages: {1}'
                          .format(name, packages))

        soft.init_dependency_list()
        deps = [dep for dep in soft.get_exec_dependencies() if dep in self.softwares]
        if deps:
            self.logger.debug('[{0}] Some exec dependencies are compiled: {1}'
                              .format(name, deps))
            return

        not_installed = list(itertools.filterfalse(debian_tools.is_installed,
                                                   packages))
        if not_installed:
            self.logger.debug('[{0}] Packages not installed: {1}'
                              .format(name, not_installed))
            return

        system_versions = [debian_tools.get_system_version(p)
                           for p in packages]

        system_version = system_versions[0]
        if (min_version and
                debian_tools.version_compare(system_version,
                                             min_version) < 0):
            self.logger.debug('[{0}] System version {1} is lower then '
                              'min required: {2}'
                              .format(name, system_version, min_version))
            return
        if (max_version and
                debian_tools.version_compare(system_version,
                                             max_version) > 0):
            self.logger.debug('[{0}] System version {1} is higher then '
                              'max required: {2}'
                              .format(name, system_version, max_version))
            return

        self.logger.debug('[{0}] All good: using systeme packages'.format(name))

        self.remove_software(name, remove_deps=False)
        self.system_softwares_dict[name] = [version, packages, system_versions]
        return (packages, system_versions)

    def add_software(self, name, version, ordered_version=None,
                     sha1_version=None, groups=None,
                     min_version='', max_version=''):
        """
        This is a new method to add a software to the version.
        A min and max version can be given
        This method is used to add a software to the version.
        The ordered_version can be given to be used in comparison
        with other versions. For example if version is 2.0_alpha,
        ordered_version can be 2.0.

        :param string name: Name of the software
        :param string version: Version of the software
        :param string ordered_version: Version of the software used for comparison
        :param string sha1_version: Version of the software using sha1 if no tag
        :param list groups: Groups the software belongs to. This aims to ease
                            software management in a version configuration file
        :param string min_version: The minimal allowed version of the software
        :param string max_version: The maximal allowed version of the software
        """
        self.logger.debug()
        self.logger.debug('Adding %s' % name)
        msg = 'Software added: {0} | version={1}'.format(name, version)
        if ordered_version:
            self.softwares_with_ordered_version_dict[name] = ' ' + ordered_version
            msg += ' | ordered version={0}'.format(ordered_version.strip())
        if min_version:
            msg += ' | min version={0}'.format(min_version)
        else:
            min_version = misc.simplify(ordered_version or version)
        self.softwares_with_ranges_dict[name][0] = min_version
        if max_version:
            msg += ' | max version={0}'.format(max_version)
        self.softwares_with_ranges_dict[name][1] = max_version or ''
        self.softwares[name] = version
        self.system_softwares_dict[name] = [version, [], []]
        if sha1_version:
            self.softwares[name] += ',' + sha1_version
            msg += ' | sha1 version={0}'.format(sha1_version)
        groups = groups or []
        if groups != []:
            self.software_to_groups_dict[name] = groups
            for group in groups:
                self.group_to_softwares_dict[group].add(name)
            msg += ' | groups={0}'.format(groups)
        package = None
        soft_use_system_version = name in self.system_softwares
        if soft_use_system_version:
            msg += ' | use system={0}'.format(soft_use_system_version)
            package = self.set_soft_system_version(name, version,
                                                   min_version, max_version)
        if package:
            msg = 'Software {0} | version={1} | package(s)={2}'.format(name, version, package)

        try:
            current_version = self.softwares[name]
        except KeyError:
            current_version = None
            if name in self.system_softwares:
                current_version = self.system_softwares_dict[name][0]
        if current_version:
            software = self.get_software(name, current_version)
            software.init_dependency_list()
            for depend_soft in software.get_dependencies():
                self.software_parents_dict[depend_soft].add(name)
        self.logger.debug(msg)

    def add_software_in_groups(self, name, groups):
        if name not in self.softwares:
            return
        try:
            soft_groups = self.software_to_groups_dict[name]
            new_groups = list(set(soft_groups + groups))
            self.software_to_groups_dict[name] = new_groups
        except KeyError:
            pass

        # Update group_to_softwares with new groups
        for group in groups:
            self.group_to_softwares_dict[group].add(name)

    #######
    #
    # Methods to ease software management in version description files
    #
    #######

    def remove_software_groups(self, groups, remove_deps=False):
        for group in groups:
            self.remove_softwares(self.group_to_softwares_dict[group], remove_deps)

    def keep_only_software_groups(self, groups, remove_deps=False):
        all_soft_in_groups = list(itertools.chain.from_iterable(list(self.group_to_softwares_dict.values())))
        soft_not_ingroups = [soft for soft in self.softwares
                             if soft not in list(all_soft_in_groups)]
        self.remove_softwares(soft_not_ingroups, remove_deps)

    def set_list_of_softwares(self, list_of_softwares):
        # Iterate through keys, not dict otherwise:
        # RuntimeError: dictionary changed size during iteration
        for soft_name in list(self.softwares):
            if soft_name not in list_of_softwares:
                self.softwares.pop(soft_name)
        self.update_configuration()

    def insert_sorted(self, software_name, softwares_names,
                      sorted_softwares, current_check_cyclic):
        if software_name not in sorted_softwares:
            if software_name in current_check_cyclic:
                self.logger.error('Cyclic dependencies detected for %s'
                                  % software_name)
            current_check_cyclic.append(software_name)
            try:
                version = self.softwares[software_name]
            except KeyError:
                version = None
                if software_name in self.system_softwares:
                    version = self.system_softwares_dict[software_name][0]
            if version:
                software = self.get_software(software_name, version)
                software.init_dependency_list()
                dependencies = software.get_dependencies()
                for soft_depend in dependencies:
                    if soft_depend not in softwares_names:
                        continue
                    for current_soft in softwares_names:
                        if current_soft == soft_depend:
                            sorted_softwares, current_check_cyclic = \
                                self.insert_sorted(current_soft, softwares_names,
                                                   sorted_softwares,
                                                   current_check_cyclic)
            current_check_cyclic.pop()
            if software_name in softwares_names:
                sorted_softwares.append(software_name)
        return sorted_softwares, current_check_cyclic

    def get_sorted_softwares(self, softwares_names=None):
        sorted_softwares, current_check_cyclic = [], []
        softwares_names = softwares_names or list(self.softwares)
        for name in softwares_names:
            if name in self.softwares:
                sorted_softwares, current_check_cyclic = \
                    self.insert_sorted(name, softwares_names,
                                       sorted_softwares, current_check_cyclic)
        return sorted_softwares

    def get_sorted_system_softwares(self, softwares_names=None):
        sorted_softwares, current_check_cyclic = [], []
        softwares_names = softwares_names or list(self.system_softwares_dict)
        for name in softwares_names:
            if name in self.system_softwares_dict:
                sorted_softwares, current_check_cyclic = \
                    self.insert_sorted(name, softwares_names,
                                       sorted_softwares, current_check_cyclic)
        return sorted_softwares

    def remove_softwares(self, softwares_names, remove_deps=False):
        for name in self.get_sorted_softwares(softwares_names):
            self.remove_software(name, remove_deps)

    def remove_software(self, name, remove_deps=False, tab=''):
        self.logger.debug(tab + '[Removing {0}]'.format(name))
        if name not in self.softwares:
            self.logger.debug(tab + '[Removing {0}] {0} not in list of softwares: abort'.format(name))
            return
        software = self.get_software(name, self.softwares[name])
        if remove_deps:
            software.init_dependency_list()
            if not tab:
                self.logger.debug('')
            self.logger.debug(tab + '[Removing {0}] Checking dependencies: {1}'
                              .format(name, software.get_dependencies()))
            self.softwares_being_removed.append(name)

            for child in software.get_dependencies():
                if child not in self.softwares:
                    continue
                remove = True
                self.logger.debug(tab + '[Removing {0}] Checking parents of '
                                  '{1}: {2}'
                                  .format(name, child,
                                          self.software_parents_dict[child]))
                for parent in self.software_parents_dict[child]:
                    if parent not in self.softwares:
                        continue
                    if parent == name:
                        continue
                    if parent in self.softwares_being_removed:
                        continue
                    parent_soft = self.get_software(parent,
                                                    self.softwares[parent])
                    parent_soft.init_dependency_list()

                    self.logger.debug(tab + '[Removing {0}] Checking '
                                      'dependencies of {1}: {2}'
                                      .format(name, parent,
                                              parent_soft.get_dependencies()))
                    self.logger.debug(tab + '[Removing {0}] Softwares being '
                                      'removed: {1}'
                                      .format(name,
                                              self.softwares_being_removed))

                    for parent_child in parent_soft.get_dependencies():
                        if parent_child not in self.softwares:
                            continue
                        if parent_child == parent:
                            continue
                        if parent_child == child:
                            self.logger.debug(tab + '[Removing {0}] Dependency '
                                              'not removed: {1} (parent {2} '
                                              'has same dependency)'
                                              .format(name, child, parent))
                            remove = False
                            break
                    if not remove:
                        continue
                    # for parent in self.software_parents_dict[child]
                if remove:
                    self.logger.debug(tab +
                                      '[Removing {0}] Dependency removed : {1}'
                                      .format(name, child))
                    self.remove_software(child, remove_deps, tab + '\t')
                # for child in software.get_dependencies
            self.softwares_being_removed.pop()
            # if remove_deps

        self.softwares.pop(name)
        self.update_software_parents()
        self.logger.debug(tab + '[Removing {0}] Software removed: {0}'
                          .format(name))

    def get_software_list(self):
        return self.softwares

    def has_software(self, software_name):
        return software_name in self.softwares

    def get_software_version(self, software_name):
        try:
            return self.softwares[software_name]
        except KeyError:
            if software_name in self.system_softwares:
                return self.get_software_package(software_name)[0]
            else:
                self.logger.error('Cannot get a version for software {0}, '
                                  'it is not in the version'
                                  .format(software_name))

    def get_software_package(self, software_name):
        return self.system_softwares_dict[software_name]

    def get_software_ordered_version(self, software_name):
        ordered_version = self.softwares_with_ordered_version_dict\
            .get(software_name, self.get_software_version(software_name))
        return ordered_version.strip()

    def set_software_version(self, software_name, software_version,
                             software_ordered_version=None, sha1_version=None,
                             min_version='', max_version=''):
        min_version = min_version or misc.simplify(software_ordered_version or software_version)
        if sha1_version:
            software_version = ','.join([software_version, sha1_version])
        if software_ordered_version:
            software_ordered_version = ' ' + software_ordered_version.strip()
        else:
            try:
                del self.softwares_with_ordered_version_dict[software_name]
            except KeyError:
                pass

        if self.has_software(software_name):
            self.softwares[software_name] = software_version
            if software_ordered_version:
                self.softwares_with_ordered_version_dict[software_name] = software_ordered_version
            self.softwares_with_ranges_dict[software_name][0] = min_version
            self.softwares_with_ranges_dict[software_name][1] = max_version
        elif software_name in self.system_softwares:
            self.add_software(software_name, software_version, software_ordered_version,
                              sha1_version, None, min_version, max_version)
        else:
            self.logger.warning('Cannot change version of software {0}, \
                software unknown'.format(software_name))

    def set_software_min_version(self, name, version):
        self.softwares_with_ranges_dict[name][0] = version

    def set_software_max_version(self, name, version):
        self.softwares_with_ranges_dict[name][1] = version

    def set_software_minmax_versions(self, name, min_version, max_version):
        self.softwares_with_ranges_dict[name] = [min_version, max_version]

    def set_softwares_min_version(self, name_list, version):
        for name in name_list:
            self.set_software_min_version(name, version)

    def set_softwares_max_version(self, name_list, version):
        for name in name_list:
            self.set_software_max_version(name, version)

    def set_softwares_minmax_versions(self, name_list, min_version,
                                      max_version):
        for name in name_list:
            self.set_softwares_minmax_versions(name, min_version, max_version)

    def print_softwares(self):
        self.logger.info('Softwares in version {0}'.format(self.name))
        max_name_len = max([len(name) for name in self.system_softwares_dict])
        max_version_len = max([0, ] + [len(p[0])
                                       for p in self.system_softwares_dict.values()
                                       if p[1]])
        for name in self.get_sorted_system_softwares():
            msg = self.get_software_info(name, max_name_len, max_version_len)
            self.logger.info(msg)

    def get_software_info(self, name, max_name_len=0, max_version_len=0):
        msg = '- %s' % name.ljust(max_name_len)
        version, package, package_version = self.system_softwares_dict[name]
        msg += ' %s' % version.ljust(max_version_len)
        if package:
            msg += ' (used debian package: {0} {1})'.format(package,
                                                            package_version)
        return msg

    def get_packages(self):
        return self.system_softwares_dict

    def get_softwares(self):
        softwares = []
        for name, version in self.softwares.items():
            softwares.append(self.get_software(name, version))
        return softwares

    def get_package_infos(self, name):
        return self.system_softwares_dict[name]

    def get_software_class(self, name):
        return self.catalog.get_software_class(name)

    def get_software(self, name, version):
        version = version + self.softwares_with_ordered_version_dict.get(name, '')
        try:
            software = self.get_software_class(name)(
                name, version, self.verbose,
                use_system_version=name in self.system_softwares)
            if software.is_python():
                self._py3 = version.startswith('3')
            software._py3 = self._py3
            return software
        except TypeError as e:
            self.logger.error('[{0}] {1}'.format(name, e))

    def set_software_minimal_list(self, minimal_list):
        if not minimal_list:
            return []
        self.logger.debug()
        self.logger.debug('Set minimal list: {0}'.format(minimal_list))
        list_to_add = minimal_list
        list_done = []
        while list_to_add != []:
            for soft_name in list_to_add:
                # Check is already done
                if soft_name in list_done:
                    list_to_add.remove(soft_name)
                else:
                    # Add his dependencies to list_to_add
                    try:
                        version = self.softwares[soft_name]
                    except KeyError:
                        version = None
                        if soft_name in self.system_softwares:
                            version = self.system_softwares_dict[soft_name][0]
                    if version is None:
                        list_to_add.remove(soft_name)
                    else:
                        current_software = self.get_software(soft_name, version)
                        current_software.init_dependency_list()
                        list_done.append(soft_name)
                        list_to_add.remove(soft_name)
                        for depend_soft in current_software.get_dependencies():
                            if (depend_soft in self.softwares and
                                    depend_soft not in list_done and
                                    depend_soft not in list_to_add):
                                list_to_add.append(depend_soft)
        # Apply list_done to the version manager and remove softwares from list
        self.set_list_of_softwares(list_done)
        return list_done

    def add_tasks(self, tasks):
        self.tasks += tasks

    def add_post_compile_test_tasks(self):
        for test_suite in self.test_suite_list:
            if not test_suite.insource:
                self.tasks.append(test_suite.create_download_task())
            post_install = test_suite.create_post_compile_tasks()[1]
            self.tasks += post_install

    def add_post_move_test_tasks(self):
        for test_suite in self.test_suite_list:
            self.tasks.append(test_suite.create_download_task())
            self.tasks += test_suite.create_post_move_tasks()

    def get_software_list_for_category(self, category):
        software_category_list = set([])
        for software_name, version in self.softwares.items():
            software_class = self.get_software_class(software_name)
            version = version + self.softwares_with_ordered_version_dict\
                .get(software_name, '')
            software_object = software_class(software_name, version,
                                             self.verbose)
            if software_object.get_type() == category:
                software_category_list.add(software_name)
        return software_category_list

    def compare(self, other, category):
        s1 = self.get_software_list_for_category(category)
        s2 = other.get_software_list_for_category(category)
        compared_list = s1.union(s2)
        d1 = other.softwares
        d2 = self.softwares
        added_keys = misc.added_in_dict(d1, d2)
        removed_keys = misc.removed_in_dict(d1, d2)
        changed_keys = misc.changed_in_dict(d1, d2)
        self.logger.info('Added %ss' % category)
        for key in added_keys.intersection(compared_list):
            self.logger.info('+ %s : %s' % (key.ljust(20), d2[key]))
        self.logger.info('Removed %ss' % category)
        for key in removed_keys.intersection(compared_list):
            self.logger.info('- %s : %s' % (key.ljust(20), d1[key]))
        self.logger.info('Version changes in %ss' % (category))
        for key in changed_keys.intersection(compared_list):
            self.logger.info(u'\u00B1 %s : %s \u2192 %s' % (key.ljust(20),
                                                            d1[key].ljust(20),
                                                            d2[key].ljust(20)))

if __name__ == '__main__':

    print('Testing version class')
    ver = FrameworkVersion('0.1', 1)
    ver.add_software('test', '0.1')
    ver.print_softwares()
    ver.remove_software('test')

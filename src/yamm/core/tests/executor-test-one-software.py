# -*- coding: utf-8 -*-
__author__ = "André RIBES - 2011"

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from yamm.core.base.misc                import VerboseLevels
from yamm.core.engine.executor          import Executor
from yamm.core.engine.config            import CompilEnv
from yamm.core.engine.config            import ArchiveSoftwareConfig
from yamm.core.engine.softwares         import ArchiveSoftware
from yamm.core.engine.tasks.decompress  import DecompressTask
from yamm.core.engine.tasks.compilation import CompileTask
from yamm.core.framework.software       import FrameworkSoftware

main_topdir = "/tmp/eina"
test_path     = os.path.abspath(__file__)
dir_test_path = os.path.dirname(test_path)

# Executor config
executor_config = CompilEnv(main_topdir=main_topdir, parallel_make="8")

# Creation d'un logiciel avec un archive
software_config = ArchiveSoftwareConfig(src_directory = os.path.join(main_topdir, 'src'),
                                        build_directory = os.path.join(main_topdir, 'build'),
                                        install_directory = os.path.join(main_topdir, 'install'),
#                                         src_dir_name="eina_src",
                                        archive_file_name="eina-1.0.0.beta.tar.bz2",
                                        archive_dir=dir_test_path)
framework_software=FrameworkSoftware(name='eina', version='1.0.0')
framework_software.create_tasks()
software = ArchiveSoftware(name="eina",
                           framework_software=framework_software,
                           software_config=software_config,
                           decompress_task=DecompressTask(archive_type="tar.bz2"),
                           compile_task=CompileTask(compil_type="autoconf")
                           )

# Create executor
exe = Executor(executor_config)
exe.set_global_verbose_level(VerboseLevels.WARNING)
exe.add_software(software)
exe.execute()

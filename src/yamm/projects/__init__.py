import os

projects = ["compilers", "e17", "kde"]
projects += ["brtv", "bthm", "cocagne", "emc2", "nacre"]
projects += ["map", "openturns", "salome", "salome_cfd", "salome_meca"]
projects += ["salome_multiphysiques"]
projects += ["arpege", "salome_coeur", "salome_hydro"]

projects_with_gui = {}
for project in sorted(projects):
    yamm_project_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), project)
    project_gui_file = os.path.join(yamm_project_directory, 'bin',
                                    project + '_gui.py')
    if os.path.exists(project_gui_file):
        projects_with_gui[project] = project_gui_file

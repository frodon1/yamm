#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from yamm.core.framework.bin.yamm_command import FrameworkYammCommand
from yamm.core.framework.bin.yamm_command import FrameworkYammCommandsLauncher


class ArpegeYammCommandsLauncher(FrameworkYammCommandsLauncher):
  def __init__(self):
    FrameworkYammCommandsLauncher.__init__(self)
    self.notify_title = "YAMM for ARPEGE"
    self.commands["arpege_start_and_config_local_install"] = FrameworkYammCommand("Start and config local install", self.arpege_start_and_config_local_install)
    self.commands["arpege_start_offline_and_config_local_install"] = FrameworkYammCommand("Start offline and config local install", self.arpege_start_offline_and_config_local_install)
    self.commands["arpege_start_from_scratch_and_config_local_install"] = FrameworkYammCommand("Start from scratch and config local install", self.arpege_start_from_scratch_and_config_local_install)
    self.commands["arpege_config_local_install"] = FrameworkYammCommand("Config local install", self.arpege_config_local_install)
    self.commands["arpege_create_pack"] = FrameworkYammCommand("Create pack", self.arpege_create_pack)

  def arpege_start_and_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.start()
    if ret:
      return self.arpege_config_local_install(args, kw)
    return ret

  def arpege_start_offline_and_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.make(executor_mode="build")
    if ret:
      return self.arpege_config_local_install(args, kw)
    return ret

  def arpege_start_from_scratch_and_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    self.yamm_project.delete_directories(delete_install=True)
    ret = self.yamm_project.start()
    if ret:
      return self.arpege_config_local_install(args, kw)
    return ret

  def arpege_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.config_local_install()

  def arpege_create_pack(self, args, kw):
    self.yamm_project.print_configuration()
    try:
#      mode_recharge =  kw.get('mode_recharge', 1)
#      mode_projet =  kw.get('mode_projet', 1)
      unie =  bool(int(kw.get('unie', 1)))
      delete = bool(int(kw.get('delete', 0)))
      runnable = bool(int(kw.get('runnable', 0)))
      return self.yamm_project.create_pack(unie=unie,
                                           delete=delete,
                                           runnable=runnable),
                                           
    except:
      return 0

if __name__ == "__main__":
  ArpegeYammCommandsLauncher().run()

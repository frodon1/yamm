#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str
from PyQt4 import QtGui  # @UnresolvedImport

from yamm.core.framework.gui.basic_wizard import FrameworkBasicIntro
from yamm.core.framework.gui.basic_wizard import FrameworkBasicOptions
from yamm.core.framework.gui.basic_wizard import FrameworkBasicRemotes
from yamm.core.framework.gui.basic_wizard import FrameworkBasicWizard
from yamm.core.framework.gui.select_software_list import ManageSoftwareList
from yamm.projects.arpege.project import Project


class BasicWizard(FrameworkBasicWizard):

  def __init__(self, parent):
    self.oracle_sid_possible_values = ["TO_BE_DEFINED", "ARD", "NRD", "AUN", "INT", "QBC", "SBC"]
    self.compiler_possible_values = ["Intel", "GNU"]
    FrameworkBasicWizard.__init__(self, parent, "Create a ARPEGE project", Project())
    self.setWindowTitle("Create a ARPEGE project")

  def get_yamm_project(self):
    return Project()

  def end(self, result):
    FrameworkBasicWizard.end(self, result)
    if self.result:
      self.compiler = self.compiler_possible_values[self.field("compiler").toInt()[0]]
      self.var_directory = str(self.field("var_directory").toString())
      self.work_directory = str(self.field("work_directory").toString())
      self.adresse_musicale = str(self.field("adresse_musicale").toString())
      self.login_musicale = str(self.field("login_musicale").toString())
      self.oracle_sid = self.oracle_sid_possible_values[self.field("oracle_sid").toInt()[0]]
      self.login_oracle = str(self.field("login_oracle").toString())
      self.login_nacre = str(self.field("login_nacre").toString())
      self.mdp_nacre = str(self.field("mdp_nacre").toString())
      self.add_default_servers = False
      if self.field("add_default_servers").toBool():
        self.add_default_servers = True
      pass

  def add_pages(self):
    self.addPage(BasicIntro(self, self.get_yamm_project()))
    self.addPage(BasicOptions(self, self.get_yamm_project(), self.compiler_possible_values))
    self.addPage(BasicRemotes(self, self.get_yamm_project(), self.oracle_sid_possible_values))
    self.addPage(ManageSoftwareList(self, self.get_yamm_project()))

class BasicIntro(FrameworkBasicIntro):
  def __init__(self, parent, project):
    FrameworkBasicIntro.__init__(self, parent, project)

class BasicOptions(FrameworkBasicOptions):

  def __init__(self, parent, project, compiler_possible_values):
    self.compiler_possible_values = compiler_possible_values
    FrameworkBasicOptions.__init__(self, parent, project)

  def setWidgets(self):
    FrameworkBasicOptions.setWidgets(self)

    self.dir_var_lineEdit = QtGui.QLineEdit()
    self.topDirDependentWidgets.append(self.dir_var_lineEdit)
    self.widgets_in_directories_layout.append(["Var Directory:", self.dir_var_lineEdit])

    self.dir_work_lineEdit = QtGui.QLineEdit()
    self.topDirDependentWidgets.append(self.dir_work_lineEdit)
    self.widgets_in_directories_layout.append(["Work Directory:", self.dir_work_lineEdit])

    self.compilers_comboBox = QtGui.QComboBox()
    self.compilers_comboBox.addItems(self.compiler_possible_values)
    self.widgets_in_options_layout.insert(2, ["Compiler:", self.compilers_comboBox])


  def setPageLayout(self):
    FrameworkBasicOptions.setPageLayout(self)

  def registerFields(self):
    FrameworkBasicOptions.registerFields(self)
    self.registerField("var_directory", self.dir_var_lineEdit)
    self.registerField("work_directory", self.dir_work_lineEdit)

  def setDefaultValues(self):
    self.current_topDir = os.path.join(self.home_dir, "arpege")
    FrameworkBasicOptions.setDefaultValues(self)
    self.dir_var_lineEdit.setText(os.path.join(self.current_topDir, "var"))
    self.dir_work_lineEdit.setText(os.path.join(self.current_topDir, "work"))
    self.registerField("compiler*", self.compilers_comboBox)

class BasicRemotes(FrameworkBasicRemotes):

  def __init__(self, parent, project, oracle_sid_possible_values):
    self.oracle_sid_possible_values = oracle_sid_possible_values
    FrameworkBasicRemotes.__init__(self, parent, project)

  def setWidgets(self):
    FrameworkBasicRemotes.setWidgets(self)

    # Remote configuration
    rem_groupBox = QtGui.QGroupBox("Mandatory network options")
    rem_layout = QtGui.QGridLayout(rem_groupBox)
    label = QtGui.QLabel("Adresse musicale:")
    self.adresse_musicale_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(label, 0, 0)
    rem_layout.addWidget(self.adresse_musicale_lineEdit, 0, 1)

    label = QtGui.QLabel("Login musicale:")
    self.login_musicale_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(label, 1, 0)
    rem_layout.addWidget(self.login_musicale_lineEdit, 1, 1)

    label = QtGui.QLabel("Oracle SID:")
    self.oracle_sid_comboBox = QtGui.QComboBox()
    self.oracle_sid_comboBox.addItems(self.oracle_sid_possible_values)
    rem_layout.addWidget(label, 2, 0)
    rem_layout.addWidget(self.oracle_sid_comboBox, 2, 1)

    label = QtGui.QLabel("Login Oracle:")
    self.oracle_chaine_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(label, 3, 0)
    rem_layout.addWidget(self.oracle_chaine_lineEdit, 3, 1)

    label = QtGui.QLabel("Login Nacre:")
    self.login_nacre_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(label, 4, 0)
    rem_layout.addWidget(self.login_nacre_lineEdit, 4, 1)

    label = QtGui.QLabel("Mot de passe Nacre:")
    self.mdp_nacre_lineEdit = QtGui.QLineEdit()
    self.mdp_nacre_lineEdit.setEchoMode(QtGui.QLineEdit.Password)
    rem_layout.addWidget(label, 5, 0)
    rem_layout.addWidget(self.mdp_nacre_lineEdit, 5, 1)

    other_groupBox = QtGui.QGroupBox("Other options")
    other_layout = QtGui.QGridLayout(other_groupBox)
    self.add_default_servers_checkBox = QtGui.QCheckBox(self.trUtf8("Ajouter les serveurs SDE, SDR et SDP par défaut"))
    self.add_default_servers_checkBox.setChecked(True)
    other_layout.addWidget(self.add_default_servers_checkBox, 0, 0, 1, 2)

#    rem_groupBox.setLayout(rem_layout)
    self.widgets_in_layout.append(rem_groupBox)
    self.widgets_in_layout.append(other_groupBox)

  def registerFields(self):
    FrameworkBasicRemotes.registerFields(self)
    # Fields
    self.registerField("adresse_musicale*", self.adresse_musicale_lineEdit)
    self.registerField("login_musicale*", self.login_musicale_lineEdit)
    self.registerField("oracle_sid*", self.oracle_sid_comboBox)
    self.registerField("login_oracle*", self.oracle_chaine_lineEdit)
    self.registerField("login_nacre*", self.login_nacre_lineEdit)
    self.registerField("mdp_nacre*", self.mdp_nacre_lineEdit)
    self.registerField("add_default_servers", self.add_default_servers_checkBox)

  def setDefaultValues(self):
    FrameworkBasicRemotes.setDefaultValues(self)
    self.categoriesButtons["composante"].setChecked(True)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import str
from PyQt4 import QtGui, uic
import os

# This import is used by the file list_data_servers_widget.ui

from yamm.core.framework.gui import icons_rc #@UnusedImport


class ArpegeListDataServers(QtGui.QWidget):
  def __init__(self, parent):
    QtGui.QWidget.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            "list_data_servers_widget.ui"), self)
    self.sdr_model = QtGui.QStandardItemModel(self)
    self.sdp_model = QtGui.QStandardItemModel(self)
    self.sde_model = QtGui.QStandardItemModel(self)
    self.updateList()
    self.sdr_listView.setModel(self.sdr_model)
    self.sdp_listView.setModel(self.sdp_model)
    self.sde_listView.setModel(self.sde_model)

  def updateList(self):
    self.sdr_model.clear()
    self.sdp_model.clear()
    self.sde_model.clear()
    for sdr in sorted(self.parent().sdr_dict.keys()):
      self.sdr_model.appendRow(QtGui.QStandardItem(sdr))
    for sdp in sorted(self.parent().sdp_dict.keys()):
      self.sdp_model.appendRow(QtGui.QStandardItem(sdp))
    for sde in sorted(self.parent().sde_dict.keys()):
      self.sde_model.appendRow(QtGui.QStandardItem(sde))
      
  def editSdr(self, index):
    sdrName = str(self.sdr_model.data(index).toString())
    self.addSdr(self.parent.sdr_dict.get(sdrName, None))

  def editSdp(self, index):
    sdpName = str(self.sdp_model.data(index).toString())
    self.addSdp(self.parent.sdp_dict.get(sdpName, None))

  def editSde(self, index):
    sdeName = str(self.sde_model.data(index).toString())
    self.addSde(self.parent.sde_dict.get(sdeName, None))
    
  def add_data_server(self, model, view, data_server_dict, 
                      parent_add_method, data_server=None, 
                      host=None, port=None, title=None):
    w = ArpegeAddDataServer(self, data_server)
    if host is not None:
      w.host.setText(host)
    if port is not None:
      w.port.setValue(int(port))
    if title is not None:
      w.setWindowTitle(title)
    if w.exec_():
      name = str(w.name.text())
      update = False
      if name not in list(data_server_dict.keys()):
        update = True
      host = str(w.host.text())
      port = w.port.value()
      parent_add_method(name, host, port)
      if update:
        item = QtGui.QStandardItem(name)
        model.appendRow(item)
        view.scrollTo(item.index())

  def addSdp(self, sdp=None):
    self.add_data_server(model = self.sdp_model,
                         view = self.sdp_listView,
                         data_server_dict = self.parent.sdp_dict,
                         parent_add_method = self.parent.add_sdp,
                         data_server = sdp,
                         port = 24790 if sdp is None else sdp.port,
                         title = "SDP settings")

  def addSdr(self, sdr=None):
    self.add_data_server(model = self.sdr_model,
                         view = self.sdr_listView,
                         data_server_dict = self.parent.sdr_dict,
                         parent_add_method = self.parent.add_sdr,
                         data_server = sdr,
                         port = 24770 if sdr is None else sdr.port,
                         title = "SDR settings")

  def addSde(self, sde=None):
    self.add_data_server(model = self.sde_model,
                         view = self.sde_listView,
                         data_server_dict = self.parent.sde_dict,
                         parent_add_method = self.parent.add_sde,
                         data_server = sde,
                         port = 24780 if sde is None else sde.port,
                         title = "SDE settings")
                         
  def remove_data_server(self, model, view, data_server_dict):
    index = view.currentIndex()
    sdeName = str(model.data(index).toString())
    if sdeName in list(data_server_dict.keys()):
      del data_server_dict[sdeName]
      model.removeRow(index.row())

  def removeSdr(self):
    self.remove_data_server(model = self.sdr_model,
                            view = self.sdr_listView,
                            data_server_dict = self.parent.sdr_dict)
#    index = self.sdr_listView.currentIndex()
#    sdrName = str(self.sdr_model.data(index).toString())
#    if sdrName in self.parent.sdr_dict.keys():
#      del self.parent.sdr_dict[sdrName]
#      self.sdr_model.removeRow(index.row())

  def removeSdp(self):
    self.remove_data_server(model = self.sdp_model,
                            view = self.sdp_listView,
                            data_server_dict = self.parent.sdp_dict)
#    index = self.sde_listView.currentIndex()
#    sdeName = str(self.sde_model.data(index).toString())
#    if sdeName in self.parent.sde_dict.keys():
#      del self.parent.sde_dict[sdeName]
#      self.sde_model.removeRow(index.row())

  def removeSde(self):
    self.remove_data_server(model = self.sde_model,
                            view = self.sde_listView,
                            data_server_dict = self.parent.sde_dict)
#    index = self.sde_listView.currentIndex()
#    sdeName = str(self.sdr_model.data(index).toString())
#    if sdeName in self.parent.sde_dict.keys():
#      del self.parent.sde_dict[sdeName]
#      self.sde_model.removeRow(index.row())

class ArpegeAddDataServer(QtGui.QDialog):
  def __init__(self, parent, data_server=None):
    QtGui.QDialog.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            "add_data_server.ui"), self)
    if data_server is not None:
      self.name.setText(data_server.name)
      self.host.setText(data_server.host)
      self.port.setValue(data_server.port)
      self.name.setEnabled(False)
      self.host.setFocus()
    self.enable_ok_button()

  def enable_ok_button(self):
    len_name = len(self.name.text())
    len_host = len(self.host.text())

    enable_test = len_name > 0 and len_host > 0
    self.buttonBox.button(self.buttonBox.Ok).setEnabled(enable_test)

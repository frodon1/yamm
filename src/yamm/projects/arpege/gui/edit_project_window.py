#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.core.base import misc
from yamm.core.framework.gui  import edit_project_window
from yamm.projects.arpege     import project
from yamm.projects.arpege     import serveurs_donnees
from yamm.projects.arpege.gui import data_server
from yamm.projects.arpege.gui import gui_project


class EditProjectWindow(edit_project_window.FrameworkEditProjectWindow):

  def init_variables(self):
    edit_project_window.FrameworkEditProjectWindow.init_variables(self)
    self.sdr_dict = self.yamm_gui_project.sdr_dict
    self.sdp_dict = self.yamm_gui_project.sdp_dict
    self.sde_dict = self.yamm_gui_project.sde_dict
    
  def init_widgets(self):
    edit_project_window.FrameworkEditProjectWindow.init_widgets(self)
    self.data_servers_widget = data_server.ArpegeListDataServers(self)
    self.central_tab.addTab(self.data_servers_widget, "Data servers")
    self.init_data_servers_tab()

  def add_sdr(self, name, host="", port=24770):
    self.sdr_dict[name] = serveurs_donnees.SDR(misc.Log("ARPEGE GUI SDR"),
                                               name, host, port)

  def add_sdp(self, name, host="", port=24790):
    self.sdp_dict[name] = serveurs_donnees.SDP(misc.Log("ARPEGE GUI SDP"),
                                               name, host, port)

  def add_sde(self, name, host="", port=24780):
    self.sde_dict[name] = serveurs_donnees.SDE(misc.Log("ARPEGE GUI SDE"),
                                               name, host, port)
    
  def init_data_servers_tab(self):
    self.data_servers_widget.parent = self

  def get_empty_yamm_project(self):
    return project.Project()

  def get_empty_yamm_gui_project(self):
    return gui_project.YammGuiProject()

  def save_from_gui(self):
    ret = edit_project_window.FrameworkEditProjectWindow.save_from_gui(self)
    if ret:
      # Passage en revue des serveurs de données
      for sdr in list(self.sdr_dict.values()):
        self.yamm_gui_project.add_sdr(sdr)
      for sdp in list(self.sdp_dict.values()):
        self.yamm_gui_project.add_sdp(sdp)
      for sde in list(self.sde_dict.values()):
        self.yamm_gui_project.add_sde(sde)
      self.yamm_gui_project.create_gui_project_file()
    return ret

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from past.builtins import execfile
from builtins import str
from yamm.core.base import misc
from yamm.core.framework.gui.gui_project import FrameworkYammGuiProject
from yamm.projects.arpege.serveurs_donnees import SDR, SDP


gui_file = """
arpege_config_local_install    = %arpege_config_local_install
sdr_dict = %sdr_dict
sdp_dict = %sdp_dict
sde_dict = %sde_dict
"""
gui_file = misc.PercentTemplate(gui_file)

add_sdr_str = """
yamm_project.add_sdr("%name",
                    host="%host",
                    port=%port)
"""
add_sdr_str = misc.PercentTemplate(add_sdr_str)

add_sdp_str = """
yamm_project.add_sdp("%name",
                    host="%host",
                    port=%port)
"""
add_sdp_str = misc.PercentTemplate(add_sdp_str)

add_sde_str = """
yamm_project.add_sde("%name",
                    host="%host",
                    port=%port)
"""
add_sde_str = misc.PercentTemplate(add_sde_str)

class YammGuiProject(FrameworkYammGuiProject):

  def __init__(self, project_class="arpege"):
    FrameworkYammGuiProject. __init__(self, project_class)
    self.arpege_config_local_install = True
    self.sdr_dict = {}
    self.sdp_dict = {}
    self.sde_dict = {}

  def clear(self):
    FrameworkYammGuiProject.clear(self)
    self.arpege_config_local_install = True
    self.sdr_dict = {}
    self.sdp_dict = {}
    self.sde_dict = {}
    
  def add_sdr(self, sdr):
    self.sdr_dict[sdr.name] = sdr

  def add_sdp(self, sdp):
    self.sdp_dict[sdp.name] = sdp

  def add_sde(self, sde):
    self.sde_dict[sde.name] = sde
    
  def create_gui_project_file(self):
    FrameworkYammGuiProject.create_gui_project_file(self)
    filetext = gui_file.substitute(arpege_config_local_install=\
                                    self.arpege_config_local_install,
                                   sdr_dict=str(self.sdr_dict),
                                   sdp_dict=str(self.sdp_dict),
                                   sde_dict=str(self.sde_dict))
    with open(self.gui_filename, 'a') as gui_project_file:
      gui_project_file.write(filetext)

  def get_start_text_project_files(self):
    filetext = FrameworkYammGuiProject.get_start_text_project_files(self)
    for sdr in list(self.sdr_dict.values()):
      filetext += add_sdr_str.substitute(name=sdr.name,
                              host=sdr.host,
                              port=sdr.port)
    for sdp in list(self.sdp_dict.values()):
      filetext += add_sdp_str.substitute(name=sdp.name,
                              host=sdp.host,
                              port=sdp.port)
    for sde in list(self.sde_dict.values()):
      filetext += add_sde_str.substitute(name=sde.name,
                              host=sde.host,
                              port=sde.port)
    return filetext

  def load_project(self, filename):
    FrameworkYammGuiProject.load_project(self, filename)
    exec_dict = {}
    execfile(filename, exec_dict)
    self.arpege_config_local_install = \
      exec_dict.get("arpege_config_local_install", True)
    for sdr_name, sdr_parameters in list(exec_dict.get("sdr_dict", {}).items()):
      if type(sdr_parameters) is dict:
        self.sdr_dict[sdr_name] = SDR(misc.Log("ARPEGE GUI SDR"),
                                      **sdr_parameters)
      else:
        self.sdr_dict[sdr_name] = SDR(misc.Log("ARPEGE GUI SDR"),
                                      *sdr_parameters)
    for sdp_name, sdp_parameters in list(exec_dict.get("sdp_dict", {}).items()):
      if type(sdp_parameters) is dict:
        self.sdp_dict[sdp_name] = SDP(misc.Log("ARPEGE GUI SDP"),
                                      **sdp_parameters)
      else:
        self.sdp_dict[sdp_name] = SDP(misc.Log("ARPEGE GUI SDP"),
                                      *sdp_parameters)
    for sde_name, sde_parameters in list(exec_dict.get("sde_dict", {}).items()):
      if type(sde_parameters) is dict:
        self.sde_dict[sde_name] = SDP(misc.Log("ARPEGE GUI SDE"),
                                      **sde_parameters)
      else:
        self.sde_dict[sde_name] = SDP(misc.Log("ARPEGE GUI SDE"),
                                      *sde_parameters)
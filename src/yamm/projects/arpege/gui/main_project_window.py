#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from PyQt4 import QtGui, uic
import os

from yamm.core.framework.gui.main_project_window import FrameworkProjectWindow
from yamm.projects.arpege.gui     import edit_project_window
from yamm.projects.arpege.project import Project


class ProjectWindow(FrameworkProjectWindow):

  def __init__(self, parent, gui_project, yamm_gui):
    FrameworkProjectWindow.__init__(self, parent, gui_project, yamm_gui)
    self.arpege_bin_directory = os.path.realpath(os.path.join(\
                                    os.path.dirname(os.path.abspath(__file__)),
                                    os.pardir, "bin"))

  def init_widgets(self):
    FrameworkProjectWindow.init_widgets(self)
    self.arpege_config_local_install_checkbox = QtGui.QCheckBox("configure")
    self.arpege_config_local_install_button = QtGui.QPushButton(
                                                    "Configure local install")
    self.arpege_create_pack_button = QtGui.QPushButton("Create pack")
    
    self.actions_groupBox.layout().insertWidget(3, 
                                  self.arpege_config_local_install_checkbox)
    self.actions_groupBox.layout().addWidget(
                                  self.arpege_config_local_install_button)
    self.actions_groupBox.layout().addWidget(self.arpege_create_pack_button)
    
    self.arpege_config_local_install_checkbox.clicked.connect(
                                  self.arpege_update_config_local_install)
    self.arpege_config_local_install_button.clicked.connect(
                                  self.arpege_config_local_install_command)
    self.arpege_create_pack_button.clicked.connect(
                                  self.arpege_create_pack_command)
    
    self.arpege_config_local_install_checkbox.setChecked(
                                  self.gui_project.arpege_config_local_install)
    
    self.create_pack_window = uic.loadUi(os.path.join(
                                  os.path.dirname(os.path.abspath(__file__)),
                                  "create_pack.ui"))

  def arpege_update_config_local_install(self):
    self.gui_project.arpege_config_local_install = \
                        self.arpege_config_local_install_checkbox.isChecked()
    self.gui_project.create_gui_project_file()
    
  def start_command(self):
    if self.arpege_config_local_install_checkbox.isChecked():
      FrameworkProjectWindow.start_command(self, 
                                  "arpege_start_and_config_local_install")
    else:
      FrameworkProjectWindow.start_command(self)

  def start_offline_command(self):
    FrameworkProjectWindow.start_offline_command(self)

  def start_from_scratch_command(self):
    FrameworkProjectWindow.start_from_scratch_command(self)

  def arpege_config_local_install_command(self):
    command = self.prepare_command("arpege_config_local_install")
    self.exec_command(command)
    
  def arpege_create_pack_command(self):
    if self.create_pack_window.exec_():
      command = self.prepare_command("arpege_create_pack")
      kw = {}
#      kw['mode_recharge'] = int(self.create_pack_window.mode_recharge\
#                                                                .isChecked())
#      kw['mode_projet'] = int(self.create_pack_window.mode_projet.isChecked())
      kw['delete'] = int(self.create_pack_window.delete_pack.isChecked())
      kw['unie'] = int(self.create_pack_window.unie.isChecked())
      kw['runnable'] = int(self.create_pack_window.runnable.isChecked())
      self.exec_command(command, [], kw)

  def get_command_script(self):
    return os.path.join(self.arpege_bin_directory, "arpege_command.py")

  def get_yamm_project_edit_window(self):
    return edit_project_window.EditProjectWindow(self, self.yamm_gui, "edit", 
                                                 self.gui_project)

  def get_empty_yamm_project(self):
    return Project()

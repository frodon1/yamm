#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import shutil
import time
import datetime

from yamm.core.base import misc
from yamm.core.framework.installer import FrameworkInstaller
from yamm.projects.arpege.installer.strings import installer_decompress_usage
from yamm.projects.arpege.installer.strings import installer_decompress_template


class ArpegeInstaller(FrameworkInstaller):
  def __init__(self, name, logger, topdirectory, softwares, executor,
               mode, runnable = False, delete=False):
    FrameworkInstaller.__init__(self, name, logger, topdirectory, softwares,
                                executor, delete, runnable)
    self.arpege_version = self.executor.version_object.get_arpege_version()
    if not topdirectory:
      self.topdirectory = os.path.join(self.executor.options.get_global_option("packs_directory"))
    self.logger.verbose = misc.VerboseLevels.DEBUG
    self.logger.debug("Installer %s initialisation starts"%self.name)
    
    self.mode = mode
    self.delete = delete
    self.runnable = runnable
    
    self.logger.debug("Installer %s initialisation ends"%self.name)

  def begin(self):
    self.logger.debug("Installer %s begin step starts"%self.name)
    # Step 1: Create pack dir (remove existing one)
    if os.path.exists(self.topdirectory):
      misc.fast_delete(self.topdirectory)
    os.makedirs(self.topdirectory)
    self.logger.debug("Installer %s begin step ends"%self.name)
    return True

  def create(self):
    self.logger.debug("Installer %s create step starts"%self.name)
    t_tgz = int(time.time())
    for soft in list(self.map_name_to_softwares.values()):
      self.logger.debug("Create pack for {0}".format(soft.name))
      soft.create_pack(self.topdirectory, self.mode)
    self.logger.debug("Installer %s create step ends"%self.name) 
    t_tgz = int(time.time()) - t_tgz
    self.logger.info("Packs des composantes créés en {0}s".format(datetime.timedelta(seconds=t_tgz)))
    return True

  def end(self):
    self.logger.debug("Installer %s end step starts"%self.name)
    #self.logger.info("Installer %s end step starts"%self.name)
    installer_tgz_name = self.name
    installer_tgz_path = os.path.join(os.path.dirname(self.topdirectory),
                                      installer_tgz_name+".tar.gz")
    ret = self.create_pack(installer_tgz_path)
    if not ret:
      return ret
    if self.runnable:
      ret = self.create_runnable(installer_tgz_name)
      if not ret:
        return ret

    if self.delete:
      misc.fast_delete(self.topdirectory)
    return True
    self.logger.debug("Installer %s end step ends"%self.name)
    return True

  def get_decompress_content(self):
    installer_tgz_name = self.name + ".tar.gz"
    return installer_decompress_template.substitute(
        pack_dirname=os.path.basename(self.topdirectory),
        arpege_version=self.arpege_version,
        usage=installer_decompress_usage,
        installer_tgz_name=installer_tgz_name,
        title_deco="=" * len(self.arpege_version))

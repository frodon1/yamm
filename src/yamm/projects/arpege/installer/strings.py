#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.core.base import misc

installer_decompress_usage = """usage()
{
cat<<EOF
usage: $0 [-h] [-d] [-g] [-b builder]

Ce script installe Arpege dans une configuration avec des paramètres lus depuis 1
fichiers de configuration appelé builder

Options:
 -h    Affiche ce message
 -d    Decompresse seulement
 -g    Ne supprime pas le répertoire après installation
 -b    Fichier configuration principal (default: builder)
EOF
}"""

#installer_decompress_usage = misc.PercentTemplate(installer_decompress_usage)

installer_decompress_template = """
£{usage}
DECOMPRESS=0
KEEPDIR=0
BUILDER=""

while getopts ":dg:b:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    d)
      DECOMPRESS=1
      ;;
    g)
      KEEPDIR=1
      ;;
    b)
      BUILDER="$OPTARG"
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "L'option -$OPTARG nécessite un argument." >&2
      exit 1
      ;;
  esac
done

echo ""
echo "=================================£{title_deco}"
echo "Self Extracting Arpege £{arpege_version} Installer"
echo "=================================£{title_deco}"
echo ""

if test $DECOMPRESS = 0
then
  DEFAULTBUILDER="$(pwd)/builder"
  eval BUILDER="$BUILDER" # to expand the tilde
  test -z "$BUILDER" && BUILDER="$DEFAULTBUILDER"
  
  test ! -e "$BUILDER" && die "Fichier builder manquant"
fi

tail -n+$ARCHIVE "$0" > £{installer_tgz_name}

check £{installer_tgz_name}

if test -d £{pack_dirname}
then
  newname="£{pack_dirname}-$(date +"%F").bak"
  if test -d ${newname}
  then
    rm -rf ${newname}
  fi
  mv £{pack_dirname} ${newname}
fi

echo "Décompression de l'archive..."
bar_cat £{installer_tgz_name}
echo "Terminé"

rm £{installer_tgz_name}

if test $DECOMPRESS = 1
then
  exit 0
fi

cp "$BUILDER" £{pack_dirname}/builder
cd £{pack_dirname}

echo "Installation..."
./installeARPEGE builder
if test $? = 0; then
  cd ..
  if test $KEEPDIR = 0
  then
    rm -rf £{pack_dirname}
  fi
  echo "Terminé"
else
  echo "Erreur durant l'installation"
fi

notifyCmd=$(which notify-send)
if test -n "$notifyCmd"; then
    $notifyCmd "Arpege" "Installation d'Arpege £{arpege_version} terminée."
fi
"""

installer_decompress_template = misc.PoundTemplate(installer_decompress_template)


installer_cp_current_template = """
if not os.path.exists(%target):
  print "\x1b[1;31;47m WARNING \x1b[0m No such file : {0}".format(%target)
else:
  if not os.path.exists(%target_template):
    shutil.copyfile(%target,
                    %target_template)
  if not os.path.exists(%target_current):
    shutil.copyfile(%target_template,
                    %target_current)
"""
installer_cp_current_template = misc.PercentTemplate(installer_cp_current_template)

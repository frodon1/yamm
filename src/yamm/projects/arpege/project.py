#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from operator import attrgetter
import socket
import subprocess
import datetime

from yamm.core.base import misc
from yamm.core.base.misc import LoggerException
from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command
from yamm.projects.arpege import serveurs_donnees
from yamm.projects.arpege import versions as default_versions
from yamm.projects.arpege.installer.arpege_installer import ArpegeInstaller
from yamm.projects.arpege.softwares import composantes
from yamm.projects.arpege.softwares import prerequisites
from yamm.projects.nacre.softwares import composantes as nacre_composantes
from yamm.projects.nacre.softwares import prerequisites as nacre_prerequisites
from yamm.projects.nacre import project as nacre_project
  
def get_ftp_server():
  return nacre_project.get_ftp_server()

class Project(FrameworkProject):

  def __init__(self, log_name="ARPEGE", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.define_arpege_options()
    self.init_arpege_vars()

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="ARPEGE", verbose=self.verbose_level)
    
    # Import des prerequis de NACRE
    for module_name in nacre_prerequisites.softwares_list:
      self.add_software_in_catalog(
                  module_name, "yamm.projects.nacre.softwares.prerequisites")
    # Import des composantes de NACRE
    for module_name in nacre_composantes.softwares_list:
      self.add_software_in_catalog(
                  module_name,"yamm.projects.nacre.softwares.composantes")
      
    # Import des prerequis
    for module_name in prerequisites.softwares_list:
      self.add_software_in_catalog(
                  module_name, "yamm.projects.arpege.softwares.prerequisites")
    # Import des composantes
    for module_name in composantes.softwares_list:
      self.add_software_in_catalog(
                  module_name, "yamm.projects.arpege.softwares.composantes")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(
                  version_name, "yamm.projects.arpege.versions")
    self.logger.debug("End init catalog")

  def init_arpege_vars(self):
    # Dictionnaires des sdr
    # Clé = nom du sdr
    # Valeur = instance de serveurs_donnees.SDR
    self.sdr_dict = {}
    # Dictionnaires des sdp
    # Clé = nom du sdp
    # Valeur = instance de serveurs_donnees.SDP
    self.sdp_dict = {}
    # Dictionnaires des sde
    # Clé = nom du sde
    # Valeur = instance de serveurs_donnees.SDE
    self.sde_dict = {}

  def define_arpege_options(self):
    # Categories du projet ARPEGE
    self.options.add_category("prerequisite")
    self.options.add_category("composante")

    self.options.add_option("var_directory", "", ["global"])
    self.options.add_option("work_directory", "", ["global"])

    self.options.add_option("packs_directory", "", ["global"])
    # From version 2.3, SSL mode is required
    self.options.add_option("nacre_ssl_mode", "", ["global"],
                            ["NONE", "KEY", "CA"])
    self.options.add_option("nacre_ssl_path", "", ["global"])
    self.options.add_option("nacre_ssl_pass", "", ["global"])
    self.options.add_option("nacre_ssl_key", "", ["global"])
    self.options.add_option("nacre_ssl_cert", "", ["global"])
    self.options.add_option("nacre_ssl_ca", "", ["global"])

    # Options pour les dépôts
    self.options.add_option(
                      "vcs_username", "", ["global", "category", "software"])
    self.options.add_option(
                      "vcs_server", "", ["global", "category", "software"])

    self.options.add_option("install_ref", "", ["global"])

    self.options.add_option("nb_calcs", 1, ["global"])
    self.options.add_option("nb_thread_cox", 1, ["global"])
    self.options.add_option("base_port", 1, ["global"])

    self.options.add_option("login_oracle", "", ["global"])
    self.options.add_option(
        "oracle_sid", "", ["global"],
        ["ARD", "NRD", "AUN", "INT", "QBC", "SBC", "TO_BE_DEFINED"])

    self.options.add_option("tns_admin", "", ["global"])

    self.options.add_option("adresse_musicale", "", ["global"])
    self.options.add_option("login_musicale", "", ["global"])

    self.options.add_option("printer_name", "", ["global"])

    # options relatives aux calculs NACRE a ajouter 
    # au fichier de config "builder"
    self.options.add_option("indice_nacre", "", ["global"], ["A"])
    self.options.add_option("temps_verification_nacre", 1, ["global"])

    # machine d'installation du pack
    self.options.add_option("pack_machine", "", ["global"])

    # login et mdp nacre
    self.options.add_option("login_nacre", "", ["global"])
    self.options.add_option("mdp_nacre", "", ["global"])

    # Choix du compilateur
    self.options.add_option("compiler", "", ["global"], ["GNU", "Intel"])

    # Chiffrement des mots de passe
    self.options.add_option("fichier_mdp_musicale", "", ["global"])
    self.options.add_option("fichier_mdp_sinfonie", "", ["global"])
    self.options.add_option("fichier_mdp_chiffrement", "", ["global"])

#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option(
          "top_directory", os.path.join(os.environ["HOME"], "arpege"))
    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)
    
    # Set Specific var directory
    if not current_command_options.get_global_option("var_directory"):
      current_command_options.set_global_option(
          "var_directory", os.path.join(os.environ["HOME"], "arpege", "var"))
    # Set Specific work directory
    if not current_command_options.get_global_option("work_directory"):
      current_command_options.set_global_option(
          "work_directory", os.path.join(os.environ["HOME"], "arpege", "work"))
    # Set main directory for softwares
    if not current_command_options.get_global_option("main_software_directory"):
      current_command_options.set_global_option(
          "main_software_directory", current_command_options.get_global_option(
                                                        "version_directory"))

    # Set Specific packs directory
    if not current_command_options.get_global_option("packs_directory"):
      current_command_options.set_global_option(
          "packs_directory", os.path.join(
              current_command_options.get_global_option("top_directory"),
              "packs"))
      pass

    # Set Specific SSL mode
    if not current_command_options.get_global_option("nacre_ssl_mode"):
      current_command_options.set_global_option("nacre_ssl_mode", "NONE")
      pass
    if not current_command_options.get_global_option("nacre_ssl_path"):
      current_command_options.set_global_option("nacre_ssl_path",
        os.path.join(current_command_options.get_global_option(
                        "version_directory"),
                     "install", "home", "nacre", "etc", "ssl"))
      pass
    if not current_command_options.get_global_option("nacre_ssl_pass"):
      current_command_options.set_global_option("nacre_ssl_pass", "keypass")
      pass
    host = socket.getfqdn()
    if not current_command_options.get_global_option("nacre_ssl_key"):
      current_command_options.set_global_option(
          "nacre_ssl_key", "{0}_key.pem".format(host))
      pass
    if not current_command_options.get_global_option("nacre_ssl_cert"):
      current_command_options.set_global_option(
          "nacre_ssl_cert", "{0}_cert.pem".format(host))
      pass
    if not current_command_options.get_global_option("nacre_ssl_ca"):
      current_command_options.set_global_option(
          "nacre_ssl_ca", "{0}_cacert.pem".format(host))
      pass

    if not current_command_options.get_global_option("vcs_server"):
      current_command_options.set_global_option(
          "vcs_server", "git.forge.pleiade.edf.fr")
      pass
    if not current_command_options.get_global_option("vcs_username"):
      current_command_options.set_global_option("vcs_username", "")
      pass

    if not current_command_options.get_global_option("install_ref"):
      current_command_options.set_global_option("install_ref", "mono")
      pass

    if not current_command_options.get_global_option("nb_calcs"):
      current_command_options.set_global_option("nb_calcs", 8)
      pass
    if not current_command_options.get_global_option("nb_thread_cox"):
      current_command_options.set_global_option("nb_thread_cox", 1)
      pass
    if not current_command_options.get_global_option("base_port"):
      current_command_options.set_global_option("base_port", 24700)
      pass
    
    if not current_command_options.get_global_option("login_oracle"):
      current_command_options.set_global_option("login_oracle", "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("tns_admin"):
      current_command_options.set_global_option("tns_admin", 
                                                "TO_BE_DEFINED")
      pass
  
    if not current_command_options.get_global_option("adresse_musicale"):
      current_command_options.set_global_option(
          "adresse_musicale", "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("login_musicale"):
      current_command_options.set_global_option("login_musicale",
          "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("fichier_mdp_sinfonie"):
        current_command_options.set_global_option("fichier_mdp_sinfonie",
                                                  os.path.join(os.path.expanduser('~'),
                                                               '.config', 'arpege', 'mdp_sinfonie'))
        pass

    if not current_command_options.get_global_option("fichier_mdp_musicale"):
        current_command_options.set_global_option("fichier_mdp_musicale",
                                                  os.path.join(os.path.expanduser('~'),
                                                               '.config', 'arpege', 'mdp_musicale'))
        pass

    if not current_command_options.get_global_option("fichier_mdp_chiffrement"):
        current_command_options.set_global_option("fichier_mdp_chiffrement",
                                                  os.path.join(os.path.expanduser('~'),
                                                               '.config', 'arpege', 'mdp_chiffrement'))
        pass

    # options NACRE pour ARPEGE
    if not current_command_options.get_global_option("indice_nacre"):
      current_command_options.set_global_option("indice_nacre", "A")
      pass
    
    if not current_command_options.get_global_option(
        "temps_verification_nacre"):
      current_command_options.set_global_option("temps_verification_nacre", 10)
      pass

    # machine d'installation d'ARPEGE
    if not current_command_options.get_global_option("pack_machine"):
      current_command_options.set_global_option("pack_machine", "TO_BE_DEFINED")
      pass
    
    # login et mdp NACRE
    if not current_command_options.get_global_option("login_nacre"):
      current_command_options.set_global_option("login_nacre", "TO_BE_DEFINED")
      pass
    
    if not current_command_options.get_global_option("mdp_nacre"):
      current_command_options.set_global_option("mdp_nacre", "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("printer_name"):
      # Imprimante Clamart/B240
      current_command_options.set_global_option(
                                      "printer_name", "B_240-LXT642-IN-54570")
      pass

    if not current_command_options.get_global_option("compiler"):
      current_command_options.set_global_option("compiler", "Intel")
      pass

    if not current_command_options.is_category_option(
        "prerequisite", "source_type"):
      current_command_options.set_category_option(
          "prerequisite", "source_type", "archive")
    if not current_command_options.is_category_option(
        "composante", "source_type"):
      current_command_options.set_category_option(
        "composante", "source_type", "remote")
    pass

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):

    for option in ['var_directory', 'work_directory', 'packs_directory']:
      current_command_options.check_abs_path(option, logger=self.logger)
      
    for category in ['prerequisite', 'composante']:
      current_command_options.check_allowed_value('source_type',
                                       ["archive", "remote"],
                                       category=category,
                                       logger=self.logger)
      if current_command_options.is_category_option(category,
                                                    'main_software_directory'):
        current_command_options.check_abs_path('main_software_directory',
                                               category = category,
                                               logger=self.logger)

#######
#
# Print methods
#
#######

  def print_general_configuration(self, current_command_options):
    FrameworkProject.print_general_configuration(self, current_command_options)
    self.logger.info("ARPEGE Version       : %s" % self.version)
    pass

#######
#
# Méthodes internes pour les commandes
#
#######

  def update_configuration(self):
    # Platform
    misc.misc_platform = self.current_command_options.get_global_option("platform")
    if self.current_command_options.get_global_option("vcs_server") == "":
      self.current_command_options.set_global_option("vcs_server", "git.forge.pleiade.edf.fr")
      pass
    if self.current_command_options.get_global_option("archive_remote_address") == "":
      self.current_command_options.set_global_option("archive_remote_address", os.path.join(get_ftp_server()))
      pass
    pass

#######
#
# Méthodes spécifiques au projet Nacre
#
#######

  def add_sdr(self, name, host="", port=24770):
    """
    Méthode pour ajouter un sdr au projet
    """
    if name in list(self.sdr_dict.keys()):
      self.logger.error("Le SDR %s existe déjà." % name)
      return None

    if not port:
      self.logger.error("Le port du SDR {0} est obligatoire.".format(name))
      return None

    if not host:
      self.logger.error("L'hôte du SDR {0} est obligatoire.".format(name))
      return None

    port_to_host = {}
    for sdr in list(self.sdr_dict.values()):
      port_to_host[sdr.port] = sdr.host
      pass

    if port in list(port_to_host.keys()) and host == port_to_host[port]:
      self.logger.error("The SDR {0} avec le port {1} et l'hôte {2} existe déjà.".format(name, port, host))
      return None

    self.sdr_dict[name] = serveurs_donnees.SDR(self.logger, name, host, port)
    self.logger.info("SDR ajouté: {0}.".format(self.sdr_dict[name]))
    return self.sdr_dict[name]

  def add_sdp(self, name, host="", port=24780):
    """
    Méthode pour ajouter un sdp au projet
    """
    if name in list(self.sdp_dict.keys()):
      self.logger.error("Le SDP %s existe déjà." % name)
      return None

    if not port:
      self.logger.error("Le port du SDP {0} est obligatoire.".format(name))
      return None

    if not host:
      self.logger.error("L'hôte du SDP {0} est obligatoire.".format(name))
      return None

    port_to_host = {}
    for sdp in list(self.sdp_dict.values()):
      port_to_host[sdp.port] = sdp.host
      pass

    if port in list(port_to_host.keys()) and host == port_to_host[port]:
      self.logger.error("The SDP {0} avec le port {1} et l'hôte {2} existe déjà.".format(name, port, host))
      return None

    self.sdp_dict[name] = serveurs_donnees.SDP(self.logger, name, host, port)
    self.logger.info("SDP ajouté: {0}.".format(self.sdp_dict[name]))
    return self.sdp_dict[name]

  def add_sde(self, name, host="", port=24780):
    """
    Méthode pour ajouter un sde au projet
    """
    if name in list(self.sde_dict.keys()):
      self.logger.error("Le SDE %s existe déjà." % name)
      return None

    if not port:
      self.logger.error("Le port du SDE {0} est obligatoire.".format(name))
      return None

    if not host:
      self.logger.error("L'hôte du SDE {0} est obligatoire.".format(name))
      return None

    port_to_host = {}
    for sde in list(self.sde_dict.values()):
      port_to_host[sde.port] = sde.host
      pass

    if port in list(port_to_host.keys()) and host == port_to_host[port]:
      self.logger.error("The SDE {0} avec le port {1} et l'hôte {2} existe déjà.".format(name, port, host))
      return None

    self.sde_dict[name] = serveurs_donnees.SDE(self.logger, name, host, port)
    self.logger.info("SDE ajouté: {0}.".format(self.sde_dict[name]))
    return self.sde_dict[name]
    
  def config_sdr(self, config_file_sdr):
    liste_sdr = sorted(list(self.sdr_dict.values()), key=attrgetter('name'))
    if liste_sdr:
      self.logger.info("Configuration des SDR:")
    for sdr in liste_sdr:
      self.logger.info("{0}".format(sdr))
      if sdr.name.endswith("_P14"):
        name = sdr.name[:-4]
        subprocess.call(['sed', '-i', '-e', 's/\*MACHINE_{0}_EXP_P14.*/\*MACHINE_{0}_EXP_P14={1}/g'.format(name, sdr.host), config_file_sdr])
        subprocess.call(['sed', '-i', '-e', 's/\*PORT_{0}_EXP_P14.*/\*PORT_{0}_EXP_P14={1}/g'.format(name, sdr.port), config_file_sdr])
        subprocess.call(['sed', '-i', '-e', 's/\*MACHINE_{0}_ING_P14.*/\*MACHINE_{0}_ING_P14={1}/g'.format(name, sdr.host), config_file_sdr])
        subprocess.call(['sed', '-i', '-e', 's/\*PORT_{0}_ING_P14.*/\*PORT_{0}_ING_P14={1}/g'.format(name, sdr.port), config_file_sdr])
    pass
  
  def config_sdp(self, config_file_sdp):
    liste_sdp = sorted(list(self.sdp_dict.values()), key=attrgetter('name'))
    if liste_sdp:
      self.logger.info("Configuration des SDP:")
    for sdp in liste_sdp:
      self.logger.info("{0}".format(sdp))
      subprocess.call(['sed', '-i', '-e', 's/\*PORT_{0}_ING.*/\*PORT_{0}_ING={1}/g'.format(sdp.name, sdp.port), config_file_sdp])
      subprocess.call(['sed', '-i', '-e', 's/\*PORT_{0}_EXP.*/\*PORT_{0}_EXP={1}/g'.format(sdp.name, sdp.port), config_file_sdp])
    pass

  def config_sde(self, config_file_sde):
    liste_sde = sorted(list(self.sde_dict.values()), key=attrgetter('name'))
    if liste_sde:
      self.logger.info("Configuration des SDE:")
    for sde in liste_sde:
      self.logger.info("{0}".format(sde))
      subprocess.call(['sed', '-i', '-e', 's/\*MACHINE_{0}_EXP.*/\*MACHINE_{0}_EXP={1}/g'.format(sde.name, sde.host), config_file_sde])
      subprocess.call(['sed', '-i', '-e', 's/\*PORT_{0}_EXP.*/\*PORT_{0}_EXP={1}/g'.format(sde.name, sde.port), config_file_sde])
      subprocess.call(['sed', '-i', '-e', 's/\*PORT_{0}_APG_EXP.*/\*PORT_{0}_APG_EXP={1}/g'.format(sde.name, sde.port), config_file_sde])
      subprocess.call(['sed', '-i', '-e', 's/\*MACHINE_{0}_ING.*/\*MACHINE_{0}_ING={1}/g'.format(sde.name, sde.host), config_file_sde])
      subprocess.call(['sed', '-i', '-e', 's/\*PORT_{0}_ING.*/\*PORT_{0}_ING={1}/g'.format(sde.name, sde.port), config_file_sde])
    pass
  
  @yamm_command("Configuration of a local installation")
  def config_local_install(self):
#    """
#      This command configures the file depending on:
#      - the SDE declared in the option sde_list.
#      - the SDR
#      - the install directories of the softwares
#
#    """
#
#    # Prepare execution with software_mode = "nothing"
#    sdr_software_list = []
    for software in self.current_command_softwares:
      #software.software_mode = "nothing"
      software.set_command_env(self.current_command_softwares,
                                self.current_command_options,
                                "nothing",
                                self.current_command_version_object,
                                self.current_command_python_version)
      self.current_map_name_to_softwares[software.name] = software

    proc_inst_soft = self.current_map_name_to_softwares.get("PROCEDUREINSTALL", None)
    if not proc_inst_soft:
      return

    config_file = os.path.join(proc_inst_soft.install_directory,"builder")

    # Configuration des fichiers liés au SDR
    self.config_sdr(config_file)

    # Configuration des fichiers liés au SDP
    self.config_sdp(config_file)

    # Configuration des fichiers liés au SDE
    self.config_sde(config_file)

    # Configuration des softwares (appel à la méthode config_local_install des softwares)
    post_install_commands = {}
    for software in self.current_command_softwares:
      software.software_mode = "post_install_config"
      post_install_commands[software.name] = software.post_install_commands
      software.post_install_commands = []
      software.config_local_install()
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
    try:
      self.execute_command()
    except LoggerException as e:
      pass
    except Exception as e:
      raise e

    for software in self.current_command_softwares:
      software.software_mode = "nothing"
      software.post_install_commands = post_install_commands[software.name]
      
    var_directory = self.current_command_options.get_global_option("var_directory")
    if not os.path.exists(var_directory):
      os.makedirs(var_directory, 0o755)
    if not os.path.exists(os.path.join(var_directory,"MR")):
      os.makedirs(os.path.join(var_directory,"MR"), 0o755)
    if not os.path.exists(os.path.join(var_directory,"MP")):
      os.makedirs(os.path.join(var_directory,"MP"), 0o755)
      
    work_directory = self.current_command_options.get_global_option("work_directory")
    if not os.path.exists(work_directory):
      os.makedirs(work_directory, 0o755)
    if not os.path.exists(os.path.join(work_directory,"MR")):
      os.makedirs(os.path.join(work_directory,"MR"), 0o755)
    if not os.path.exists(os.path.join(work_directory,"MP")):
      os.makedirs(os.path.join(work_directory,"MP"), 0o755)
      
    return True

  @yamm_command("Creation of an installation pack")
  def create_pack(self, unie=False, runnable=False, delete=False):
    """
    This command creates the pack to be installed in pcy00307/9 or at UNIE

    :param bool mode_recharge: If True, generates the archive file in recharge mode
    :param bool mode_projet: If True, generates the archive file in project mode
    :param bool delete: If True, deletes the pack directory once the archives are created
    """     
    today = datetime.date.today().strftime('%d_%m_%Y') # Date au format 01_08_2016
    date_path = datetime.date.today().strftime('%Y_%m_%d') # Date au format 2016_08_01
    pack_base_name = "pack_arpege_{0}".format(self.get_version_object().get_arpege_version())
    
    # Create rep date_path if not exist
    pack_path = os.path.join(self.current_command_options.get_global_option("packs_directory"), date_path)
    if not os.path.exists(pack_path):
      os.makedirs(pack_path)
    
    if unie:
      pack_base_name += '_unie'
      # pour le pack unie, les bibliotheques ne sont pas fournies
      self.logger.info("Création d'un pack UNIE sans les bibliothèques neutroniques")
      for soft in self.current_command_softwares:
        if soft.name == "BIBLIO_ARPEGE":
          self.logger.info("software biblio")
          i = self.current_command_softwares.index(soft)
          del self.current_command_softwares[i]
 
    self.logger.info("Création des packs des composantes")
    for mode in ['MR', 'MP']:
      pack_name = '{0}_{1}_{2}'.format(pack_base_name, mode, today)

      # Step 1: Create installer instance
      pack_dir = os.path.join(pack_path, pack_name)
    
      arpege_installer = ArpegeInstaller(name=pack_name,
                                         logger=self.logger,
                                         topdirectory=pack_dir,
                                         softwares=self.current_command_softwares,
                                         executor=self.current_command_executor,
                                         mode=mode,
                                         runnable=runnable,
                                         delete=delete)

      arpege_installer.execute()
    return True

#######
#
# Commands
#
#######

if __name__ == "__main__":

  print("Testing arpege project class")
  pro = Project(verbose_level= VerboseLevels.WARNING)
  pro.set_version("DEV")
  pro.print_configuration()
#  pro.start()
  pro.create_pack()


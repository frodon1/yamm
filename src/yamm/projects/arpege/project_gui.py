#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.core.base.misc import Log, VerboseLevels
from yamm.core.framework.project_gui import FrameworkProjectGui
from yamm.projects.arpege import serveurs_donnees
from yamm.projects.arpege.gui import gui_project
from yamm.projects.arpege.gui import start_widget


class ProjectGui(FrameworkProjectGui):

  def __init__(self, app=None, yamm_class_name="arpege"):
    FrameworkProjectGui.__init__(self, app, yamm_class_name)

  def getIntroWidget(self):
    return start_widget.IntroWidget(self)

  def get_yamm_gui_project(self):
    return gui_project.YammGuiProject()

  def initYammProjectFromWizard(self, new_project, wizard):
    FrameworkProjectGui.initYammProjectFromWizard(self, new_project, wizard)
    if wizard.compiler:
      new_project.add_global_option("compiler", wizard.compiler)
    if wizard.var_directory:
      new_project.add_global_option("var_directory", wizard.var_directory)
    if wizard.work_directory:
      new_project.add_global_option("work_directory", wizard.work_directory)
    if wizard.adresse_musicale:
      new_project.add_global_option("adresse_musicale", wizard.adresse_musicale)
    if wizard.login_musicale:
      new_project.add_global_option("login_musicale", wizard.login_musicale)
    if wizard.oracle_sid:
      new_project.add_global_option("oracle_sid", wizard.oracle_sid)
    if wizard.login_oracle:
      new_project.add_global_option("login_oracle",
                                    wizard.login_oracle)
    if wizard.login_nacre:
      new_project.add_global_option("login_nacre", wizard.login_nacre)
    if wizard.mdp_nacre:
      new_project.add_global_option("mdp_nacre", wizard.mdp_nacre)
    if wizard.add_default_servers:
      new_project.add_sdr(serveurs_donnees.SDR(Log("[SDR_P14]",
                                                   VerboseLevels.DEBUG),
                                              "SDR_P14",
                                              host="localhost",
                                              port=24770))
      new_project.add_sde(serveurs_donnees.SDE(Log("[SDE]",
                                                   VerboseLevels.DEBUG),
                                              "SDE",
                                              host="localhost",
                                              port=24780))
      new_project.add_sdp(serveurs_donnees.SDP(Log("[SDP]",
                                                   VerboseLevels.DEBUG),
                                              "SDP",
                                              host="localhost",
                                              port=24790))

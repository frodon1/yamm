#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import str
from builtins import object
import socket
import copy

class DataServer(object):
  """
  Classe de base pour les serveurs de données d'étude et de référence
  """
  def __init__(self, logger, name, host, port):
    self.logger = logger
    self.name = name
    self.host = host if host != "localhost" else socket.getfqdn()
    self.port = port

  def __repr__(self):
    """
    This method will be used to serialize the data servers: only basic types must be serialized.
    This is why the logger is removed (it is a misc.Log instance).
    """
    __ds_dict__ = copy.deepcopy(self.__dict__)
    if 'logger' in __ds_dict__:
      del __ds_dict__['logger']
    return str(__ds_dict__)

class SDR(DataServer):
  def __init__(self, logger, name, host, port, *args):
    DataServer.__init__(self, logger, name, host, port)

class SDP(DataServer):
  def __init__(self, logger, name, host, port, *args):
    DataServer.__init__(self, logger, name, host, port)

class SDE(DataServer):
  def __init__(self, logger, name, host, port, *args):
    DataServer.__init__(self, logger, name, host, port)

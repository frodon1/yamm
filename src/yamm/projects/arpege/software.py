#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import string
import subprocess

from yamm.core.base import misc
from yamm.core.framework.software import FrameworkSoftware


class arpege_software(FrameworkSoftware):

  def __init__(self, name="NOT CONFIGURED", version="NOT CONFIGURED",
               verbose=misc.VerboseLevels.INFO, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)
    if self.get_type() == "composante":
        self.root_repository_template = string.Template('https://$server/git')

  def default_values_hook(self):
    pass

  def init_variables(self):
    pass

  def user_variables_hook(self):
    # Specific user options for modules
    pass

  def configure_git_composantes(self):
    if not self.project_options.is_software_option(self.name, 
                                                   "software_src_directory"):
      self.project_options.set_software_option(self.name,
        "software_src_directory",
        os.path.join(self.project_options.get_option(self.name, 
                                                     "top_directory"),
        "composantes", self.name))

  def get_type(self):
    """
    This method returns the type of software.
    There are 2 types in the NACRE project:

      - prerequisite
      - composante
    """
    return "type not defined for software %s" % self.name

  def create_pack(self, pack_dir, mode):
    """
    This method will return the command used to create the pack of the software.
    """

    if mode not in ['MR','MP']:
        raise ValueError("Le mode specifie n'est pas reconnu %s" % mode)
    with open(os.path.join(self.install_directory, '.yamm', 'python_install.log'), 'w') as f:
        subprocess.call(['python', 'install.py', pack_dir, mode],
                        cwd=self.install_directory, stdout=f, stderr=f)

  def config_local_install(self, dependency_name="ALL"):
    # This method should be used to reconfigure the software
    pass

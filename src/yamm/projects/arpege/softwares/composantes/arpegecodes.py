#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Thomas CLERC (EDF R&D)

import os
import subprocess

from yamm.core.base import misc
from yamm.projects.arpege.software import arpege_software
from yamm.projects.nacre.softwares.composantes.codes import CODES


software_name = "ARPEGECODES"

class ARPEGECODES(CODES, arpege_software):    

  def init_variables(self):
    CODES.init_variables(self)
    arpege_software.init_variables(self)
    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                          "install",
                                          self.executor_software_name.lower())
    self.compil_type              = "specific"
    self.specific_install_command = "mkdir -p ${CURRENT_SOFTWARE_INSTALL_DIR} ; "
    version_cox = "V3.11.0"
    if misc.compareVersions(self.ordered_version, "V3.5.0") >= 0:
        version_cox = "V3.13.0"
    arpege_codes = ['libellule_V4.2.0', 'coccinelle_%s' %version_cox, 'tridens_V2.3.3']
    for code in arpege_codes:
        self.specific_install_command += "cp $CURRENT_SOFTWARE_SRC_DIR/{0} $CURRENT_SOFTWARE_INSTALL_DIR ;".format(code)
    # to override the post_install_commands of CODES
    self.post_install_commands = []
    
  def init_dependency_list(self):
    self.software_dependency_list = ["NASH"]
    
  def config_local_install(self, dependency_name="ALL"):
    pass
    
  def create_pack(self, pack_dir, mode):
    if not os.path.exists(pack_dir):
      os.mkdir(pack_dir)
    subprocess.call("cp -r {0} {1} > create_pack.log 2>&1".format(self.install_directory, pack_dir), shell=True)
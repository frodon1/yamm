#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.arpege.software import arpege_software
from yamm.core.base import misc

software_name = "ARPEGECT"

class ARPEGECT(arpege_software):

  def init_variables(self):
    arpege_software.init_variables(self)
    
    if misc.compareVersions(self.ordered_version, "V350") >= 0:
      compilateur = self.project_options.get_global_option("compiler")
      if compilateur=="Intel":
        self.project_options.set_software_option(self.name, 'software_additional_env',
                                                 'export FC=ifort CC=icc CXX=icpc ;')

    if not self.project_options.is_software_option(self.name, "vcs_server"):
      self.project_options.set_software_option(software_name, "vcs_server", "git.forge.pleiade.edf.fr")

    version_for_archive = self.version
    if version_for_archive.startswith("git_tag/"):
      version_for_archive = version_for_archive[8:]
    
    self.archive_file_name    = "arpegect-" + version_for_archive + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.remote_type     = "git"
    self.configure_git_composantes()
    self.repository_name = "arpege.arpege-ct"
    self.root_repository = self.root_repository_template.substitute(server=self.project_options.get_option(self.name, "vcs_server"))
    self.tag             = self.version

    if misc.compareVersions(self.ordered_version, "V350") < 0:
      self.compil_type = "rsync"
      command = "export RARPEGE=$CURRENT_SOFTWARE_INSTALL_DIR ; "
      command += "export RLOOP=$CURRENT_SOFTWARE_INSTALL_DIR/LOOP_V2.3 ; "
      command += "export REXEC_LOOP=$CT_INSTALL_DIR/EXE/LOOP ; "
      command += "export REXEC_FORMOSA=$CT_INSTALL_DIR/EXE/LOOP/FORMOSA ; "
      command += "export REXEC=$CT_INSTALL_DIR/EXE ; "
      command += "export RMODULESMETIERS=$MODULESMETIERS_INSTALL_DIR ; "
      command += "export MODHISTO_INSTALL=$MODULE_HISTORIQUE_INSTALL_DIR ; "
      command += "make all"
      self.post_install_commands.append(command)
    else:
      self.compil_type = "cmake"
      self.config_options += ' -D LIB_MODULESMETIERS_DIR:PATH="$MODULESMETIERS_INSTALL_DIR/lib"'
      
      self.post_install_commands.append('cp ${CURRENT_SOFTWARE_SRC_DIR}/install.py ${CURRENT_SOFTWARE_INSTALL_DIR}')

  def init_dependency_list(self):
    self.software_dependency_list = ["CT", "MODULESMETIERS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = arpege_software.get_dependency_object_for(self, dependency_name)
    dependency_object.depend_of = ["install_path"]
    return dependency_object
  
  def get_type(self):
    return "composante"

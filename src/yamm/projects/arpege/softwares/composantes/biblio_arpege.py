#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Thomas CLERC (EDF R&D)

import os
import subprocess

from yamm.projects.arpege.software import arpege_software
from yamm.projects.nacre.softwares.composantes.biblio import BIBLIO


software_name = "BIBLIO_ARPEGE"

class BIBLIO_ARPEGE(BIBLIO, arpege_software):    

  def init_variables(self):
    BIBLIO.init_variables(self)
    arpege_software.init_variables(self)
    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                          "install",
                                          self.executor_software_name.lower())
    self.compil_type              = "specific"
    self.specific_install_command = "mkdir -p ${CURRENT_SOFTWARE_INSTALL_DIR} ; "
    biblio_arpege = ['biblio900.proc04', 'biblioN4.proc04', 'biblio1300.proc04', 'biblioEPR.htr',\
                     'bib_gab_PM_ALICANTE_P04_cox371.ref', 'bib_gab_GALICE_P04.ref', 'biblioN4.p04.alcade']
    for biblio in biblio_arpege:
      self.specific_install_command += "cp $CURRENT_SOFTWARE_SRC_DIR/{0} $CURRENT_SOFTWARE_INSTALL_DIR ;".format(biblio)
    # to override the post_install_commands of BIBLIO
    self.post_install_commands = []

  def config_local_install(self, dependency_name="ALL"):
    pass
    
  def create_pack(self, pack_dir, mode):
    if not os.path.exists(pack_dir):
      os.mkdir(pack_dir)
    subprocess.call("cp -r {0} {1} > create_pack.log 2>&1".format(self.install_directory, pack_dir), shell=True)  
#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.arpege.software import arpege_software


software_name = "GUITOOLS"

class GUITOOLS(arpege_software):

  def init_variables(self):
    arpege_software.init_variables(self)

    if not self.project_options.is_software_option(self.name, "vcs_server"):
      self.project_options.set_software_option(software_name, "vcs_server", "git.forge.pleiade.edf.fr")

    version_for_archive = self.version
    if version_for_archive.startswith("git_tag/"):
      version_for_archive = version_for_archive[8:]
    
    self.archive_file_name    = "guitools-" + version_for_archive + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.remote_type     = "git"
    self.configure_git_composantes()
    self.repository_name = "arpege.gui-tools"
    self.root_repository = self.root_repository_template.substitute(server=self.project_options.get_option(self.name, "vcs_server"))
    self.tag             = self.version
    
    self.compil_type = "rsync"
    self.post_install_commands.append("lrelease-qt4 res/dico/arpege.ts -qm bin/arpege.qm")

  def get_type(self):
    return "composante"

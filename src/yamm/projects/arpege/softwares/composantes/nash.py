#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
from yamm.core.base import misc
import subprocess

from yamm.projects.arpege.software import arpege_software
from yamm.projects.nacre.softwares.composantes.nacre_info import NACRE_INFO


software_name = "NASH"

class NASH(NACRE_INFO, arpege_software):

  def init_variables(self):
    NACRE_INFO.init_variables(self)
    arpege_software.init_variables(self)
    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                          "install",
                                          self.executor_software_name.lower())

    self.make_target = "nash.bin"
    self.specific_install_command = "mkdir -p ${CURRENT_SOFTWARE_INSTALL_DIR}/bin ; "
    self.specific_install_command += "cp ${CURRENT_SOFTWARE_BUILD_DIR}/src/nash/nash.bin ${CURRENT_SOFTWARE_INSTALL_DIR}/bin/ ;"
    # to override the post_install_commands of NACRE_INFO
    self.post_install_commands = []

  def config_local_install(self, dependency_name="ALL"):
    pass

  def init_dependency_list(self):
    NACRE_INFO.init_dependency_list(self)
    try:
      self.software_dependency_list.remove('POCO')
      self.software_dependency_list.remove('LIBTAR')
    except ValueError:
      pass
    self.software_dependency_list.append("POCO_APG")
    self.software_dependency_list.append("LIBTAR_APG")

  def get_dependency_object_for(self, dependency_name):
    dependency_object = NACRE_INFO.get_dependency_object_for(self, dependency_name)
    if dependency_name == "POCO_APG":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "POCO_DIR"
    if dependency_name == "LIBTAR_APG":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "LIBTAR_DIR"
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    NACRE_INFO.update_configuration_with_dependency(self, dependency_name, version_name)
    if dependency_name == "POCO_APG":
      self.config_options += " -DPOCO_DIR:PATH=$POCO_DIR "
    if dependency_name == "LIBTAR_APG":
      self.config_options += " -DLIBTAR_DIR:PATH=$LIBTAR_DIR "

  def create_pack(self, pack_dir, mode):
    if not os.path.exists(pack_dir):
      os.mkdir(pack_dir)
    pack_codes = os.path.join(pack_dir, "arpegecodes")
    if not os.path.exists(pack_codes):
      os.mkdir(pack_codes)
    subprocess.call("cp {0} {1} > create_pack.log 2>&1".format(os.path.join(self.install_directory, "bin/nash.bin"), pack_codes), shell=True)

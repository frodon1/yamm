#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Thomas CLERC (EDF R&D)

import os
import subprocess

from yamm.projects.arpege.software import arpege_software
from yamm.projects.bthm.softwares.composantes.bthm_notic import BTHM_NOTIC

software_name = "NOTIC_APG"

class NOTIC_APG(BTHM_NOTIC, arpege_software):

    def init_variables(self):
        BTHM_NOTIC.init_variables(self)
        arpege_software.init_variables(self)
        self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                              "install",
                                              self.executor_software_name.lower())

    def create_pack(self, pack_dir, mode):
        """Creation du pack
        on a besoin uniquement des fichiers model.py et histonotic.py. Ces fichiers contiennent les classes et methodes
        permettant de traiter le fichier histonotic et les assemblages du coeur avec ARPEGE.
        """
        if not os.path.exists(pack_dir):
          os.mkdir(pack_dir)
        pack_ct = os.path.join(pack_dir, "ct/EXE/notic/")
        if not os.path.exists(pack_ct):
          os.makedirs(pack_ct)
        subprocess.call("cp {0} {1} {2} {3} > create_pack.log 2>&1".format(os.path.join(self.install_directory, "notic/__init__.py"),
                                                                           os.path.join(self.install_directory, "notic/model.py"),
                                                                           os.path.join(self.install_directory, "notic/histonotic.py"),
                                                                           pack_ct), shell=True)

    def init_dependency_list(self):
        BTHM_NOTIC.init_dependency_list(self)
        self.software_dependency_list += ["CT", "ORACLE_CLIENT", "MODULE_HISTORIQUE"]

    def get_dependency_object_for(self, dependency_name):
        dependency_object = BTHM_NOTIC.get_dependency_object_for(self, dependency_name)
        if dependency_name in ["CT", "ORACLE_CLIENT", "MODULE_HISTORIQUE"]:
            dependency_object.depend_of = ["install_path"]
        return dependency_object

    def config_local_install(self, dependency_name="ALL", nacre_build=False):
        """Configuration :  on ne fait rien pour ARPEGE car le but est seulement de recuperer
           les fichiers model.py et histonotic.py.
        """
        pass

    def get_type(self):
        return "composante"

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import str
import os 

from yamm.projects.arpege.software import arpege_software


software_name = "PROCEDUREINSTALL"

class PROCEDUREINSTALL(arpege_software):

  def init_variables(self):
    arpege_software.init_variables(self)

    if not self.project_options.is_software_option(self.name, "vcs_server"):
      self.project_options.set_software_option(software_name, "vcs_server", "git.forge.pleiade.edf.fr")

    version_for_archive = self.version
    if version_for_archive.startswith("git_tag/"):
      version_for_archive = version_for_archive[8:]
    
    self.archive_file_name    = "procedureinstall-" + version_for_archive + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.remote_type     = "git"
    self.configure_git_composantes()
    self.repository_name = "arpege.procedure-install"
    self.root_repository = self.root_repository_template.substitute(server=self.project_options.get_option(self.name, "vcs_server"))
    self.tag             = self.version
    
    self.compil_type = "rsync"

  def init_dependency_list(self):
    self.software_dependency_list = ["ORACLE_CLIENT"]

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = arpege_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "ORACLE_CLIENT":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def config_local_install(self, dependency_name="ALL"):
    builder_file = "builder"
    
    if dependency_name == "ORACLE_CLIENT" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<path_oracle_home>" , "$ORACLE_CLIENT_INSTALL_DIR", builder_file)
      
    if dependency_name == "SELF" or dependency_name == "ALL":
      # paths des repertoires var et work
      self.replaceStringInFilePostInstall("<var_mr>" , 
            os.path.join(self.project_options.get_global_option("var_directory"),"MR"), 
            builder_file)
      self.replaceStringInFilePostInstall("<var_mp>" , 
            os.path.join(self.project_options.get_global_option("var_directory"),"MP"), 
            builder_file)
      self.replaceStringInFilePostInstall("<work_mr>" , 
            os.path.join(self.project_options.get_global_option("work_directory"),"MR"), 
            builder_file)
      self.replaceStringInFilePostInstall("<work_mp>" , 
            os.path.join(self.project_options.get_global_option("work_directory"),"MP"), 
            builder_file)
      # options NACRE
      self.replaceStringInFilePostInstall("<temps_verification_nacre>" , 
            str(self.project_options.get_global_option("temps_verification_nacre")), 
            builder_file)
      self.replaceStringInFilePostInstall("<indice_nacre>" , 
            self.project_options.get_global_option("indice_nacre"), 
            builder_file)
      # options relatives aux certificats ssl
      self.replaceStringInFilePostInstall("<nacre_ssl_mode>" , 
            self.project_options.get_global_option("nacre_ssl_mode"), 
            builder_file)
      self.replaceStringInFilePostInstall("<path_nacre_ssl>" , 
            self.project_options.get_global_option("nacre_ssl_path"), 
            builder_file)
      # informations de connexion a MUSICALE
      self.replaceStringInFilePostInstall("<adresse_musicale>" , 
            self.project_options.get_global_option("adresse_musicale"), 
            builder_file)
      self.replaceStringInFilePostInstall("<login_musicale>" , 
            self.project_options.get_global_option("login_musicale"), 
            builder_file)
      
      # connexion a ORACLE
      self.replaceStringInFilePostInstall("<oracle_sid>" , 
            str(self.project_options.get_global_option("oracle_sid")), 
            builder_file)
      self.replaceStringInFilePostInstall("<login_sinfonie>" , 
            self.project_options.get_global_option("login_oracle"), 
            builder_file)
      self.replaceStringInFilePostInstall("<path_tns_admin>" , 
            self.project_options.get_global_option("tns_admin"), 
            builder_file)
      
      # login et mdp nacre
      self.replaceStringInFilePostInstall("<login_nacre_ing>" , 
            self.project_options.get_global_option("login_nacre"), 
            builder_file)
      self.replaceStringInFilePostInstall("<passwd_nacre_ing>" , 
            self.project_options.get_global_option("mdp_nacre"), 
            builder_file)
      self.replaceStringInFilePostInstall("<login_nacre_exp>" , 
            self.project_options.get_global_option("login_nacre"), 
            builder_file)
      self.replaceStringInFilePostInstall("<passwd_nacre_exp>" , 
            self.project_options.get_global_option("mdp_nacre"), 
            builder_file)
            
  def get_type(self):
    return "composante"

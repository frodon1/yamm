#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import subprocess
from yamm.projects.arpege.software import arpege_software
from yamm.projects.nacre.softwares.prerequisites.poco import POCO

software_name = "POCO_APG"

class POCO_APG(POCO, arpege_software):

  def init_variables(self):
    POCO.init_variables(self)
    arpege_software.init_variables(self)

    self.post_install_commands = []
    
  def config_local_install(self, dependency_name="ALL"):
    pass

  def create_pack(self, pack_dir, mode):
    if not os.path.exists(pack_dir):
      os.mkdir(pack_dir)
    subprocess.call("rsync -r -l {0} {1} > create_pack.log 2>&1".format(os.path.join(self.install_directory,"lib"), pack_dir), shell=True)
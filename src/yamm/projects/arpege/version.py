#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.core.framework.version import FrameworkVersion
from yamm.core.base.misc import VerboseLevels

class ArpegeVersion(FrameworkVersion):
  """
    .. py:currentmodule:: yamm.projects.arpege.version.ArpegeVersion

    Base class for ARPEGE's versions. It adds new
    methods for specific capabilities of ARPEGE project.

    :param string name: name of the version.
    :param int verbose: verbose level of the version.

  """

  def __init__(self, name='NOT CONFIGURED', verbose=VerboseLevels.INFO, flavour=''):
    FrameworkVersion.__init__(self, name, verbose, flavour)
    self.version_integration = 'dev'
    
#######
#
# Méthodes qui doivent être surchargées
#
#######

  def get_arpege_version(self):
    """
      This method is optional. It should return the ARPEGE version targeted by
      the version.
    """
    return None


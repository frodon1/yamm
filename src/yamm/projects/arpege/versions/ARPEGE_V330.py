#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.arpege.version import ArpegeVersion


version_name = "V3-3-0"
version_value = "V3-3-0"
version_vcs = "master"

class ARPEGE_V330(ArpegeVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    ArpegeVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    self.add_software("ORACLE_CLIENT",     "10.2")
    self.add_software("POCO",              "1.4.6p4-all-edfp1")
    self.add_software("LIBTAR",            "1.2.20")
    self.add_software("MUSICALE",          version_vcs, "V2-3-0")
    self.add_software("NASH",              version_vcs, "V2-3-0")
    self.add_software("MODULE_HISTORIQUE", version_vcs, "V2-3-0")
    self.add_software("ARPEGECODES",       version_vcs, "V2-3-0")
    self.add_software("BIBLIO_ARPEGE",     version_vcs, "V2-3-0")

    self.add_software("ADMINISTRATION",    version_vcs, version_value)
    self.add_software("ARPEGECT",          version_vcs, version_value)
    self.add_software("CT",                version_vcs, version_value)
    self.add_software("GUITOOLS",          version_vcs, version_value)
    self.add_software("GUI",               version_vcs, version_value)
    self.add_software("MODULESMETIERS",    version_vcs, version_value)
    self.add_software("PROCEDUREINSTALL",  version_vcs, version_value)

  def get_arpege_version(self):
    return "03.03.00"

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.arpege.version import ArpegeVersion


version_name = "ARPEGE_V340"
version_value = "V3-4-0"
version_vcs = "master"
version_tag = "git_tag/ARPEGE_3-4-0"

version_composants_nacre = "git_tag/V2-4-0"

class ARPEGE_V340(ArpegeVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    ArpegeVersion.__init__(self, name, verbose, flavour)
    self.version_integration = version_vcs
    self.accepted_flavours = ['rc2', 'rc3']

  def configure_softwares(self):
    self.add_software("ORACLE_CLIENT",     "10.2")
    self.add_software("POCO",              "1.4.6p4-all-edfp1")
    self.add_software("LIBTAR",            "1.2.20")
    self.add_software("MUSICALE",          version_tag, "V2-4-0")
    self.add_software("NASH",              version_tag, "V2-4-0")
    self.add_software("MODULE_HISTORIQUE", version_tag, "V2-4-0")
    self.add_software("ARPEGECODES",       version_tag, "V2-4-0")
    self.add_software("BIBLIO_ARPEGE",     version_tag, "V2-4-0")
    self.add_software("NOTIC_APG",         version_tag, "V2-3-0")
    
    self.add_software("PROCEDUREINSTALL",  version_tag, version_value)

    self.add_software("ADMINISTRATION",    version_tag, version_value)
    self.add_software("ARPEGECT",          version_tag, version_value)
    self.add_software("CT",                version_tag, version_value)
    self.add_software("GUITOOLS",          version_tag, version_value)
    self.add_software("GUI",               version_tag, version_value)
    self.add_software("MODULESMETIERS",    version_tag, version_value)
    
  def update_configuration_with_flavour(self):
    ArpegeVersion.update_configuration_with_flavour(self)

    # Les modifications communes aux release candidates sont effectuées à la
    # fin (test 'rc' in self.flavour)
    if self.flavour == 'rc2':
      self.version_integration = 'git_tag/ARPEGE_3-4-0-rc2_int2'
    if self.flavour == 'rc3':
      self.version_integration = 'git_tag/ARPEGE_3-4-0-rc3'
      version_composants_nacre = 'git_tag/ARPEGE_3-4-0-rc3'

    if 'rc' in self.flavour:
      # composants NACRE
      for soft in ("MUSICALE","NASH", "MODULE_HISTORIQUE","ARPEGECODES", "BIBLIO_ARPEGE"):
        self.set_software_version(soft, version_composants_nacre, "V2-4-0")
      # composant ARPEGE
      for soft in ("ADMINISTRATION", "ARPEGECT","CT", "GUITOOLS", "GUI", "MODULESMETIERS", "PROCEDUREINSTALL"):
        self.set_software_version(soft, self.version_integration, version_value)
      # BTHM
      self.set_software_version("NOTIC_APG", self.version_integration, "V2-3-0")
      
  def get_arpege_version(self):
    return "3.4.0-%s"%self.flavour

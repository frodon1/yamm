#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.arpege.version import ArpegeVersion


version_name = "ARPEGE_V350"
version_value = "V3-5-0"
version_vcs = "integration_3-5-0"


class ARPEGE_V350(ArpegeVersion):

    def __init__(self, name=version_name, verbose=1, flavour=""):
        ArpegeVersion.__init__(self, name, verbose, flavour)
        self.version_integration = version_vcs
        self.accepted_flavours = ['int_%s' %str(i+1) for i in range(5)]
        self.accepted_flavours.append('rc1')
        self.accepted_flavours.append('rc2')
        self.accepted_flavours.append('rc3')
        self.accepted_flavours.append('rc4')
        self.accepted_flavours.append('release')
        self.accepted_flavours.append('final_tag')

    def configure_softwares(self):
        self.add_software("ORACLE_CLIENT", "10.2")
        self.add_software("POCO_APG", "1.4.6p4-all-edfp1")
        self.add_software("LIBTAR_APG", "1.2.20")
        self.add_software("MUSICALE", "git_tag/V2-4-0", "V2-4-0")
        self.add_software("NASH", "git_tag/V2-5-0-rc2", "V2-5-0")
        self.add_software("MODULE_HISTORIQUE", "git_tag/V2-4-0", "V2-4-0")
        self.add_software("BIBLIO_ARPEGE", "arpege", "V3-5-0")
        self.add_software("NOTIC_APG", "git_tag/ARPEGE_3-4-0", "V2-3-0")

        self.add_software("ADMINISTRATION", version_vcs, version_value)
        self.add_software("ARPEGECT", version_vcs, version_value)
        self.add_software("CT", version_vcs, version_value)
        self.add_software("GUITOOLS", version_vcs, version_value)
        self.add_software("MODULESMETIERS", version_vcs, version_value)
        self.add_software("PROCEDUREINSTALL", version_vcs, version_value)
        self.add_software("ARPEGECODES", version_vcs, version_value)

        self.add_software("GUI", version_vcs, version_value)

    def update_configuration_with_flavour(self):
        ArpegeVersion.update_configuration_with_flavour(self)
        
        basic_soft_list = ["ADMINISTRATION", "ARPEGECT", "CT", "GUITOOLS", 
                            "MODULESMETIERS", "PROCEDUREINSTALL", "GUI", "ARPEGECODES"]
        
        if 'int' in self.flavour:
            
            self.version_integration = 'git_tag/arpege_V3-5-0_%s' %self.flavour
            if 'int_3' in self.flavour:
                self.version_integration = 'git_tag/arpege_V3-5-0_int_3a'
            if 'int_4' in self.flavour:
                self.version_integration = 'git_tag/arpege_V3-5-0_int_4a'
            if 'int_5' in self.flavour:
                self.version_integration = 'git_tag/arpege_V3-5-0_int_5k'
                
        if 'rc' in self.flavour:
            self.version_integration = 'git_tag/arpege_V3-5-0-%s' %self.flavour
        
        if 'release' in self.flavour:
            self.version_integration = 'release_V3-5-0'
            
        if 'final_tag' in self.flavour:
            self.version_integration = 'git_tag/arpege_V3-5-0'
        
        for soft in basic_soft_list:
            self.set_software_version(soft, self.version_integration, version_value)
            

    def get_arpege_version(self):
        return "V3-5-0"

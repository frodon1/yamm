#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.arpege.versions.ARPEGE_V350 import ARPEGE_V350

version_name = "ARPEGE_V3501"
version_value = "V3-5-01"
version_vcs = "maintenance_V3-5-0"
final_flavour = "final_tag"


class ARPEGE_V3501(ARPEGE_V350):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        ARPEGE_V350.__init__(self, name, verbose, flavour)
        self.accepted_flavours = [final_flavour,]

    def configure_softwares(self):
        ARPEGE_V350.configure_softwares(self)
        self.update_flavour(final_flavour)

    def update_configuration_with_flavour(self):
        ARPEGE_V350.update_configuration_with_flavour(self)
        
        if final_flavour in self.flavour:
            self.set_software_version("ARPEGECT", version_vcs, version_value)
            self.set_software_version("GUI", version_vcs, version_value)

    def get_arpege_version(self):
        return "V3-5-01"

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.arpege.version import ArpegeVersion


version_name = "DEV"
version_value = "V3-6-0"
version_vcs = "master"


class DEV(ArpegeVersion):

    def __init__(self, name=version_name, verbose=1, flavour=""):
        ArpegeVersion.__init__(self, name, verbose, flavour)
        self.version_integration = version_vcs
        self.accepted_flavours = ["loop"]

    def configure_softwares(self):
        self.add_software("ORACLE_CLIENT", "10.2")
        self.add_software("POCO_APG", "1.4.6p4-all-edfp1")
        self.add_software("MUSICALE", "git_tag/V2-6-0-sprint1", "V2-5-0")
        self.add_software("NASH", "git_tag/V2-6-0-sprint1", "V2-6-0")
        self.add_software("MODULE_HISTORIQUE", "git_tag/V2-6-0-sprint1", "V2-5-0")
        self.add_software("ARPEGECODES", "git_tag/V2-5-0-rc6", "V2-5-0")
        self.add_software("BIBLIO_ARPEGE", "arpege", "V3-5-0")
        self.add_software("NOTIC_APG", "git_tag/BTHM_2-3-0", "V2-3-0")

        self.add_software("ADMINISTRATION", version_vcs, version_value)
        self.add_software("ARPEGECT", version_vcs, version_value)
        self.add_software("CT", version_vcs, version_value)
        self.add_software("GUI", version_vcs, version_value)
        self.add_software("GUITOOLS", version_vcs, version_value)
        self.add_software("MODULESMETIERS", version_vcs, version_value)
        self.add_software("PROCEDUREINSTALL", version_vcs, version_value)

    def update_configuration_with_flavour(self):
        ArpegeVersion.update_configuration_with_flavour(self)

        if "loop" in self.flavour:
            self.set_software_version("ARPEGECODES", "cox_loop", version_value)
            self.set_software_version("ARPEGECT", "cox_loop", version_value)

    def get_arpege_version(self):
        return "V3-6-0_dev"

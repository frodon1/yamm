#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from past.builtins import execfile
from yamm.core.base import misc
from yamm.core.framework.gui.gui_project import FrameworkYammGuiProject

gui_file = """
brtv_config_local_install    = %brtv_config_local_install
"""
gui_file = misc.PercentTemplate(gui_file)

class YammGuiProject(FrameworkYammGuiProject):

  def __init__(self, project_class="brtv"):
    FrameworkYammGuiProject. __init__(self, project_class)

    # To configure the local installation after a compilation
    self.brtv_config_local_install = True

  def clear(self):
    FrameworkYammGuiProject.clear(self)
    self.brtv_config_local_install = True

  def create_gui_project_file(self):
    FrameworkYammGuiProject.create_gui_project_file(self)
    filetext = gui_file.substitute(brtv_config_local_install=self.brtv_config_local_install)
    with open(self.gui_filename, 'a') as gui_project_file:
      gui_project_file.write(filetext)

  def load_project(self, filename):
    FrameworkYammGuiProject.load_project(self, filename)
    exec_dict = {}
    execfile(filename, exec_dict)
    self.brtv_config_local_install = exec_dict.get("brtv_config_local_install", True)

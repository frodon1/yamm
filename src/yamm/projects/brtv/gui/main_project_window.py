#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from PyQt4 import QtGui, uic  # @UnresolvedImport

from yamm.core.framework.gui.main_project_window import FrameworkProjectWindow

from yamm.projects.brtv.project import Project
from yamm.projects.brtv.gui     import edit_project_window

class ProjectWindow(FrameworkProjectWindow):

  def __init__(self, parent, gui_project, yamm_gui):
    FrameworkProjectWindow.__init__(self, parent, gui_project, yamm_gui)
    self.openturns_bin_directory  = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../bin")

  def init_widgets(self):
    FrameworkProjectWindow.init_widgets(self)

    self.brtv_config_local_install_checkbox = QtGui.QCheckBox("configure")
    self.brtv_config_local_install_button = QtGui.QPushButton("Configure local install")
    self.brtv_create_pack_button = QtGui.QPushButton("Create pack")

    self.actions_groupBox.layout().insertWidget(3, self.brtv_config_local_install_checkbox)
    self.actions_groupBox.layout().addWidget(self.brtv_config_local_install_button)
    self.actions_groupBox.layout().addWidget(self.brtv_create_pack_button)

    self.brtv_config_local_install_checkbox.clicked.connect(self.brtv_update_config_local_install)
    self.brtv_config_local_install_button.clicked.connect(self.brtv_config_local_install_command)
    self.brtv_create_pack_button.clicked.connect(self.brtv_create_pack_command)

    self.brtv_config_local_install_checkbox.setChecked(self.gui_project.brtv_config_local_install)
    
    self.pack_dlg = uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "create_pack.ui"))

  def brtv_update_config_local_install(self):
    self.gui_project.brtv_config_local_install = self.brtv_config_local_install_checkbox.isChecked()
    self.gui_project.create_gui_project_file()

  def start_command(self):
    if self.brtv_config_local_install_checkbox.isChecked():
      FrameworkProjectWindow.start_command(self, "brtv_start_and_config_local_install")
    else:
      FrameworkProjectWindow.start_command(self)

  def start_offline_command(self):
    if self.brtv_config_local_install_checkbox.isChecked():
      FrameworkProjectWindow.start_offline_command(self, "brtv_start_offline_and_config_local_install")
    else:
      FrameworkProjectWindow.start_offline_command(self)

  def start_from_scratch_command(self):
    if self.brtv_config_local_install_checkbox.isChecked():
      FrameworkProjectWindow.start_from_scratch_command(self, "brtv_start_from_scratch_and_config_local_install")
    else:
      FrameworkProjectWindow.start_from_scratch_command(self)

  def brtv_config_local_install_command(self):
    command = self.prepare_command("brtv_config_local_install")
    self.exec_command(command)

  def brtv_create_pack_command(self):
    if self.pack_dlg.exec_():
      command = self.prepare_command("brtv_create_pack")
      kw = {}
      kw['delete'] = 1 if self.pack_dlg.delete_pack.isChecked() else 0
      kw['runnable'] = 1 if self.pack_dlg.runnable.isChecked() else 0
      kw['default_install_dir'] = "$HOME/brtv"
      self.exec_command(command, [], kw)

  def get_command_script(self):
    return os.path.join(self.openturns_bin_directory, "brtv_command.py")

  def get_yamm_project_edit_window(self):
    return edit_project_window.EditProjectWindow(self, self.yamm_gui, "edit", self.gui_project)

  def get_empty_yamm_project(self):
    return Project()

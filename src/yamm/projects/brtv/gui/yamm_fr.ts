<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="fr_FR">
<context>
    <message>
        <location filename="start_widget.py" line="49"/>
        <source>Load recent</source>
        <translation>Load recent</translation>
    </message>
    <message>
        <location filename="start_widget.py" line="54s"/>
        <source>Remove recent</source>
        <translation>Remove recent</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>YAMM_TITLE</source>
        <translation>YAMM pour la BRTV</translation>
    </message>
    <message>
        <source>CREATE_PROJECT_BASIC</source>
        <translation>Création d&apos;un projet BRTV (Assistant)</translation>
    </message>
    <message>
        <source>CREATE_PROJECT_ADVANCED</source>
        <translation>Création d&apos;un projet BRTV (Avancé)</translation>
    </message>
    <message>
        <source>LOAD_PROJECT</source>
        <translation>Ouvre un projet BRTV</translation>
    </message>
    <message>
        <source>COPY_PROJECT</source>
        <translation>Copie un projet BRTV</translation>
    </message>
</context>
</TS>

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)
from __future__ import absolute_import, print_function

import os

from yamm.core.base import misc
from yamm.core.framework.installer import FrameworkInstaller

from .strings_template import BrtvInstallerStringsTemplate


class BrtvInstaller(FrameworkInstaller):
  def __init__(self, name, logger, topdirectory, softwares, executor,
               delete=False, runnable=False,
               default_install_dir=""):
    FrameworkInstaller.__init__(self, name, logger, topdirectory, softwares, executor, delete, runnable)

    self.stringsTemplate = BrtvInstallerStringsTemplate()

    self.default_install_dir = default_install_dir or "$HOME/brtv"

    self.brtv_version = self.executor.version_object.name
    oscard_soft = self.map_name_to_softwares.get("OSCARD", None)
    if not oscard_soft:
      self.logger.error("Le software OSCARD est manquant")
    
    self.oscard_dir = oscard_soft.get_executor_software_name()
    self.post_install_file_name = os.path.join(self.topdirectory, "brtv_post_install.py")

    if not topdirectory:
      self.topdirectory = os.path.join(self.executor.options.get_global_option("packs_directory"), "pack")

    #self.logger.info("Installer %s initialisation ends"%self.name)
    pass

  def begin(self):
    self.logger.debug("Installer %s begin step starts" % self.name)
    # Step 1: Create pack dir (remove existing one)
    if os.path.exists(self.topdirectory):
      misc.fast_delete(self.topdirectory)
    os.makedirs(self.topdirectory)
    # Prepare post install script
    # This script will be used at installation time to write the good paths
    # into config files.
    self.writePostInstallFile()
    self.logger.debug("Installer %s begin step ends" % self.name)
    return True

  def create(self):
    self.logger.debug("Installer %s create step starts" % self.name)
    # Step 0: création des archives des composantes

#    prerequisites_list = []

    # generation du README
    readme = """
* Auteur : Julien Holleville
* email  : julien.holleville@edf.fr

Le pack BRTV comporte les modules suivants :
- BRTV
- Thyc
- Thycox
- Oscard
- Coccinelle
- Tridens

Cette version de la BRTV gère les trois méthodologies THEMIS, MDA et MTC3D.
"""

    path_file = os.path.join(self.topdirectory, "README")
    with open(path_file, 'w') as configure_file:
      configure_file.write(readme)
    ##############################################################"

    # Step 4: install binary archives in installer directory
    software_mode = "install_from_binary_archive"

    for software in self.softwares:
      software_install_name = software.get_executor_software_name()
      # Sets the install directory for the current software
      installdir = os.path.join(self.topdirectory, software_install_name)
      self.executor.options.set_software_option(software.name, "software_install_directory", installdir)
      software.set_command_env(self.softwares, self.executor.options, software_mode, self.executor.version_object, self.python_version)
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.executor.add_software(executor_software)

      self.map_name_to_softwares[software.name] = software

    # The commands in unmake_movable_archive_commands must not be run: disable them
    for soft in self.executor.executor_softwares:
      if soft.install_binary_archive_task:
        soft.install_binary_archive_task.setRunPostInstallCommands(False)

    #self.log.start_command()
    self.executor.execute()

    self.logger.debug("Installer %s create step ends" % self.name)
    return True

  def end(self):
    self.logger.debug("Installer %s end step starts" % self.name)
    installer_tgz_path = os.path.join(os.path.dirname(self.topdirectory), self.name + ".tar.gz")
    ret = self.create_pack(installer_tgz_path)
    if not ret:
      return ret
    if self.runnable:
      ret = self.create_runnable(self.name)
    if self.delete:
      misc.fast_delete(self.topdirectory)
    self.logger.debug("Installer %s end step starts" % self.name)
    return ret
  
  def get_decompress_content(self):
    usage = self.stringsTemplate.get_installer_decompress_usage().substitute(default_install_dir=self.default_install_dir)
    return self.stringsTemplate.get_installer_decompress_template().substitute(
              brtv_version=self.brtv_version,
              installer_name=self.name,
              usage=usage,
              title_deco="=" * len(self.brtv_version),
              oscard_dir=self.oscard_dir,
              default_install_dir=self.default_install_dir)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 - 2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.core.base import misc
from yamm.core.framework.installer_files.strings_template import FrameworkInstallerStringsTemplate


class BrtvInstallerStringsTemplate(FrameworkInstallerStringsTemplate):

    def get_installer_decompress_usage(self):
        installer_decompress_usage = """usage()
{
cat<<EOF
usage: $0 [-h] [-t DIR] [-f]

The script installs BRTV in a given directory

Options:
 -h    Show this message
 -t    BRTV install directory (default is %{default_install_dir})
 -f    Force installation
EOF
}"""
        return misc.PercentTemplate(installer_decompress_usage)

    def get_installer_decompress_template(self):
        installer_decompress_template = """
%{usage}
TARGETDIR=""
FORCE=""

while getopts ":ht:f" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    t)
      TARGETDIR="$OPTARG"
      ;;
    f)
      FORCE="y"
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "L'option -$OPTARG nécessite un argument." >&2
      exit 1
      ;;
  esac
done

echo ""
echo "================================%{title_deco}"
echo "Self Extracting BRTV %{brtv_version} Installer"
echo "================================%{title_deco}"
echo ""

DEFAULTTARGETDIR="%{default_install_dir}"
test -z "$TARGETDIR" && echo -n "Enter the Brtv install directory [default=$DEFAULTTARGETDIR] : " && read -e -p "" TARGETDIR
eval TARGETDIR="$TARGETDIR" # to expand the tilde
test -z "$TARGETDIR" && TARGETDIR="$DEFAULTTARGETDIR"


if ! test -d "$TARGETDIR"; then
  mkdir -p "$TARGETDIR"
else
  if test -d "$TARGETDIR/%{installer_name}"; then
    test -z "$FORCE" && echo -n "The directory $TARGETDIR/%{installer_name} already exists, install anyway ? [y/N] " && read -p "" FORCE
    case "$FORCE" in
      y|Y|yes|YES|Yes|o|O|oui|OUI|Oui) FORCE="y" ;;
    esac
    if test "$FORCE" != "y"; then
      echo "Installation aborted"
      exit 1
    fi
  fi
fi


tail -n+$ARCHIVE "$0" > "$TARGETDIR"/%{installer_name}.tar.gz

cd "$TARGETDIR"

check "$TARGETDIR"/%{installer_name}.tar.gz

echo "Décompression de l'archive..."
bar_cat %{installer_name}.tar.gz
echo "Terminé"

rm %{installer_name}.tar.gz

cd %{installer_name}

echo "Configuration des chemins ..."
# apply post install script
./brtv_post_install.py
# delete post install script
rm ./brtv_post_install.py
echo "Terminé"

echo "Installation de Oscard (%{oscard_dir}) ..."
cd %{oscard_dir}/compilation
./postinstall.sh
rm ../outils/*.run
echo "Terminé"

notifyCmd=$(which notify-send)
if test -n "$notifyCmd"; then
    $notifyCmd "Brtv" "Installation de Brtv %{brtv_version} terminée."
fi
"""
        return misc.PercentTemplate(installer_decompress_template)

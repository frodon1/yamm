#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from yamm.core.base.misc import VerboseLevels, LoggerException
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command

from yamm.projects.nacre          import project       as nacre_project

from yamm.projects.brtv.softwares import prerequisites as default_prerequisites
from yamm.projects.brtv.softwares import composantes   as default_composantes
from yamm.projects.brtv           import versions      as default_versions

from yamm.projects.brtv.installer.brtv_installer import BrtvInstaller
  
def get_ftp_server():
  return nacre_project.get_ftp_server()

class Project(FrameworkProject):

  def __init__(self, log_name="BRTV", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.init_brtv_vars()
    self.define_brtv_options()
    pass

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="BRTV", verbose=self.verbose_level)
    # Import des prerequis
    for module_name in default_prerequisites.softwares_list:
      self.add_software_in_catalog(module_name, "yamm.projects.brtv.softwares.prerequisites")
    # Import des composantes
    for module_name in default_composantes.softwares_list:
      self.add_software_in_catalog(module_name, "yamm.projects.brtv.softwares.composantes")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.brtv.versions")
      pass
    self.logger.debug("End init catalog")
    pass

  def init_brtv_vars(self):
    pass

  def define_brtv_options(self):
    # Categories du projet NACRE
    self.options.add_category("prerequisite")
    self.options.add_category("composante")

    self.options.add_option("packs_directory", "", ["global"])

    # Options pour les dépôts
    self.options.add_option("vcs_username", "", ["global", "category", "software"])
    self.options.add_option("vcs_server", "", ["global", "category", "software"])


#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "brtv"))
      pass
    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)
    # Set main directory for softwares
    if not current_command_options.get_global_option("main_software_directory"):
      current_command_options.set_global_option("main_software_directory",
                                                os.path.join(current_command_options.get_global_option("version_directory")))
      pass

    # Set Specific packs directory
    if not current_command_options.get_global_option("packs_directory"):
      current_command_options.set_global_option("packs_directory",
                                                os.path.join(current_command_options.get_global_option("top_directory"), "packs"))
      pass

    if not current_command_options.get_global_option("vcs_server"):
      current_command_options.set_global_option("vcs_server", "noeyy727.noe.edf.fr")
      pass
    if not current_command_options.get_global_option("vcs_username"):
      current_command_options.set_global_option("vcs_username", "")
      pass

    if not current_command_options.is_category_option("prerequisite", "source_type"):
      current_command_options.set_category_option("prerequisite", "source_type", "remote")
    if not current_command_options.is_category_option("composante", "source_type"):
      current_command_options.set_category_option("composante", "source_type", "remote")
      
    if not current_command_options.is_category_option("composante", "binary_archive_url"):
      current_command_options.set_category_option("composante", "binary_archive_url",
                                                  os.path.join(current_command_options.get_category_option("composante", "binary_archive_topdir"), "%(name)s.tgz"))
    if not current_command_options.is_category_option("prerequisite", "binary_archive_url"):
      current_command_options.set_category_option("prerequisite", "binary_archive_url",
                                                  os.path.join(current_command_options.get_category_option("prerequisite", "binary_archive_topdir"), "%(name)s.tgz"))
    pass

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):

    for category in ['prerequisite', 'composante']:
      current_command_options.check_allowed_value('source_type',
                                       ["archive", "remote"],
                                       category=category,
                                       logger=self.logger)
      if current_command_options.is_category_option(category,
                                                    'main_software_directory'):
        current_command_options.check_abs_path('main_software_directory',
                                               category = category,
                                               logger=self.logger)

    current_command_options.check_abs_path('packs_directory',
                                           logger=self.logger)

#######
#
# Print methods
#
#######

  def print_general_configuration(self, current_command_options):
    FrameworkProject.print_general_configuration(self, current_command_options)
    self.logger.info("BRTV Version       : %s" % self.version)
    pass

#######
#
# Méthodes internes pour les commandes
#
#######

  def update_configuration(self):
    if self.current_command_options.get_global_option("vcs_server") == "":
      self.current_command_options.set_global_option("vcs_server", "noeyy727.noe.edf.fr")
      pass
    if self.current_command_options.get_global_option("archive_remote_address") == "":
      self.current_command_options.set_global_option("archive_remote_address", get_ftp_server())
      pass
    pass
  
#######
#
# Méthodes spécifiques au projet brtv
#
#######

  def _prepare_config_local_install(self):
    """
      This command will prepare the version for the configuration of the softwares.
      Typically all the config files that are regenerated from a template or from scratch
      are deleted.
    """
    # Configuration des softwares (appel à la méthode prepare_config_local_install des softwares)
    post_install_commands = {}
    software_mode = "post_install_config"
    for software in self.current_command_softwares:
      #software.software_mode = "nothing"
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               software_mode,
                               self.current_command_version_object,
                               self.current_command_python_version)
      post_install_commands[software.name] = software.post_install_commands
      software.post_install_commands = []
      software.prepare_config_local_install()
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
    success = False
    try:
      success = self.execute_command()
    except LoggerException:
      pass

    for software in self.current_command_softwares:
      software.software_mode = "nothing"
      software.post_install_commands = post_install_commands[software.name]
    
    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)
    return success

  def _config_local_install(self):
    """
      This command configures the file depending on.
    """
    
    # Suppression des fichiers de config des softwares
    config_files_list = []
    for software in self.current_command_softwares:
      config_files_list += software.get_config_files_list()
    config_files_list = list(set(config_files_list))
    for config_file in config_files_list:
      if os.path.exists(config_file):
        os.remove(config_file)

    # Configuration des softwares (appel à la méthode config_local_install des softwares)
    post_install_commands = {}
    software_mode = "post_install_config"
    # Prepare execution with software_mode = "nothing"
    for software in self.current_command_softwares:
      #software.software_mode = "nothing"
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               software_mode,
                               self.current_command_version_object,
                               self.current_command_python_version)
      post_install_commands[software.name] = software.post_install_commands
      software.post_install_commands = []
      software.config_local_install()
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
    try:
      self.execute_command()
    except LoggerException as e:
      pass
    except Exception as e:
      raise e

    for software in self.current_command_softwares:
      software.software_mode = "nothing"
      software.post_install_commands = post_install_commands[software.name]
    return True

  @yamm_command("Preparation of a local installation configuration")
  def prepare_config_local_install(self):
    return self._prepare_config_local_install()

  @yamm_command("Configuration of a local installation")
  def config_local_install(self):
    ret = self._prepare_config_local_install()
    if ret:
      ret = self._config_local_install()
    return ret

  @yamm_command("Creation of an installation pack")
  def create_pack(self, delete=False, runnable=False, default_install_dir=""):
    """
    This command creates the pack for BRTV
    :param string base_archive_name: Defines the base name of the archives of the components in the pack
    :param bool delete: If True, deletes the pack directory once the archives are created
    """

    ret = self._make(executor_mode = "create_binary_archive")
    if not ret:
      return ret
    
    pack_name = "brtv-{0}".format(self.version)
 
    # Step 1: Create installer instance
    pack_dir = os.path.join(self.current_command_options.get_global_option("packs_directory"),
                            pack_name)
 
    brtv_installer = BrtvInstaller(name=pack_name,
                                    logger=self.logger,
                                    topdirectory=pack_dir,
                                    softwares=self.current_command_softwares,
                                    executor=self.current_command_executor,
                                    delete=delete,
                                    runnable=runnable,
                                    default_install_dir=default_install_dir)
 
    return brtv_installer.execute()

#######
#
# Commands
#
#######

if __name__ == "__main__":

  print("Testing brtv project class")
  pro = Project(VerboseLevels.DEBUG)
  pro.set_version("V3_1")
  pro.options.set_global_option("archives_download_tool", "wget")
  pro.print_configuration()
  pro.download()
  #pro.start()


#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

import os

from yamm.core.framework.software import FrameworkSoftware


class BrtvSoftware(FrameworkSoftware):

  def default_values_hook(self):
    if self.get_type() == "prerequisite":
      self.src_directory = os.path.join(self.project_options.get_global_option("top_directory"), "prerequisites", "src", self.executor_software_name)
      self.build_directory = os.path.join(self.project_options.get_global_option("top_directory"), "prerequisites", "build", self.executor_software_name)
      self.install_directory = os.path.join(self.project_options.get_global_option("top_directory"), "prerequisites", "install", self.executor_software_name)

  def prepare_config_local_install(self):
    # This method should be used to prepare the configuration of the software
    removeFileTemplate = 'if test -f {0}; then echo "Deleting {0}"; /bin/rm -rf {0}; fi'
    for config_file in self.get_config_files_list():
      self.post_install_commands.append(removeFileTemplate.format(config_file))
    pass

  def config_local_install(self, dependency_name="ALL"):
    # This method should be used to reconfigure the software
    pass

  def get_type(self):
    """
    This method returns the type of software.
    There are 2 types in the NOTIC project:

      - prerequisite
      - composante
    """
    return "type not defined for software %s" % self.name

  def get_archive_pack_name(self, pack_dir, base_archive_name="brtv-install"):
    composante = self.get_pack_name()
    if not composante or self.get_type() != "composante":
      return ""
    return os.path.join(pack_dir, "{0}_{1}.tar.gz".format(base_archive_name, composante))


  def get_pack_name(self):
    """
    This method returns the name of the archive of the software in the pack.
    """
    return None

  def get_config_files_list(self):
    return []

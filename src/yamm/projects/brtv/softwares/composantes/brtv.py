#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.brtv.software import BrtvSoftware
import os
import string

software_name = "BRTV"

class BRTV(BrtvSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    BrtvSoftware.__init__(self, name, version, verbose, **kwargs)
    self.root_repository_template = string.Template('https://$server')

  def init_variables(self):
    BrtvSoftware.init_variables(self)
#     self.executor_software_name = self.name

    if not self.project_options.is_software_option(self.name, "vcs_server"):
      self.project_options.set_software_option(software_name, "vcs_server", "noeyy727.noe.edf.fr")

    self.archive_file_name = "brtv-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "svn"
    self.repository_name = "mfee/brtv_mtc3d"
    self.root_repository = self.root_repository_template.substitute(server=self.project_options.get_option(self.name, "vcs_server"))
    self.tag = self.version

    self.compil_type = "rsync"
    self.config_options = "--exclude 'outils' "

  def get_config_files_list(self):
    config_files_list = BrtvSoftware.get_config_files_list(self)
    config_files_list.append("config.sh")
    return config_files_list

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "SELF" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<BRTV_INSTALL_DIR>", self.install_directory, "config.sh")
    if dependency_name == "THYC" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<THYC_INSTALL_DIR>", "$THYC_INSTALL_DIR", "config.sh")
    if dependency_name == "THYCOX" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<THYCOX_INSTALL_DIR>", "$THYCOX_INSTALL_DIR", "config.sh")
    if dependency_name == "OSCARD" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<OSCARD_INSTALL_DIR>", "$OSCARD_INSTALL_DIR", "config.sh")
    if dependency_name == "COCCINELLE" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<COCCINELLE_INSTALL_DIR>", "$COCCINELLE_INSTALL_DIR", "config.sh")
    if dependency_name == "TRIDENS" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<TRIDENS_INSTALL_DIR>", "$TRIDENS_INSTALL_DIR", "config.sh")


  def init_dependency_list(self):
    self.software_dependency_list = ["THYC", "THYCOX", "OSCARD", "COCCINELLE", "TRIDENS"]

#  def update_configuration_with_dependency(self, dependency_name, version_name):
  def get_dependency_object_for(self, dependency_name):
    dependency_object = BrtvSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "THYC":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "THYCOX":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "OSCARD":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "COCCINELLE":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "TRIDENS":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return software_name

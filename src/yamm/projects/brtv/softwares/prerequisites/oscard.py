#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.brtv.software import BrtvSoftware

software_name = "OSCARD"

class OSCARD(BrtvSoftware):
  """
  Le logiciel OSCARD contient l'installeur Salome oscard.run,
  les binaires thyc, coccinelle, et ceux spécifiques aux couplages
  (moniteurs et convertisseurs) ainsi que les fichiers de lancement.
  """

  def init_variables(self):
    BrtvSoftware.init_variables(self)
    
    self.archive_file_name = "oscard-{0}.tar.gz".format(self.ordered_version)
    self.archive_type      = "tar.gz"
    
    self.compil_type       = "rsync"
    
    # Suppression de l'installation de Salome dans l'archive binaire : elle doit être regénérée à l'installation
    # de cette archive binaire (dans le pack par exemple)
    for directory in ("outils", "xml"):
      self.make_movable_archive_commands.append('for f in $(ls {0}); do if test -d {0}/$f; then echo "Suppression de {0}/$f"; rm -rf {0}/$f; fi; done;'.format(directory))

  def config_local_install(self, dependency_name = "ALL"):
    if dependency_name == "SELF" or dependency_name == "ALL":
      # Lancer le script de post installation
      self.post_install_commands = ["cd compilation; ./postinstall.sh"]
      pass

  def get_type(self):
    return "prerequisite"

  def get_pack_name(self):
    return software_name

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.brtv.software import BrtvSoftware
import os

software_name = "THYCOX"

class THYCOX(BrtvSoftware):
  """
  Le logiciel THYCOX contient à la fois le binaire thycox
  et le fichier de lancement.
  """

  def init_variables(self):
    BrtvSoftware.init_variables(self)
    
    self.archive_file_name = "thycox-{0}.tar.gz".format(self.ordered_version)
    self.archive_type      = "tar.gz"
    
    self.compil_type       = "rsync"

  def get_config_files_list(self):
    config_files_list = BrtvSoftware.get_config_files_list(self)
    config_files_list.append(os.path.join(self.install_directory, "config.sh"))
    return config_files_list

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "SELF" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<THYCOX_INSTALL_DIR>", self.install_directory, "config.sh")

  def get_type(self):
    return "prerequisite"

  def get_pack_name(self):
    return software_name
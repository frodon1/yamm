#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.core.framework.version import FrameworkVersion

version_name = "V3_1"
version_value = "tags/V3.1"

class V3_1(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    self.add_software("COCCINELLE", "v3.11")    
    self.add_software("THYCOX", "V2.10")
    self.add_software("THYC", "V5.1")
    self.add_software("OSCARD", "V4.4")
 
    self.add_software("BRTV", "branches/brtv_nacre_yamm", "V3.1")

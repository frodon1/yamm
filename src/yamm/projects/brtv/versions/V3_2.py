#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.core.framework.version import FrameworkVersion

version_name = "V3_2"
version_value = "trunk" # tags/V3.2-rc4 a venir

class V3_2(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    self.add_software("TRIDENS", "V2.3.3-rc2")
    self.add_software("COCCINELLE", "v3.11")
    self.add_software("THYCOX", "V2.10")
    self.add_software("THYC", "V5.1")
    self.add_software("OSCARD", "V4.4.2")
 
    self.add_software("BRTV", version_value, "V3.2")

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import shutil
import subprocess
import tempfile

from yamm.core.base import misc
from yamm.core.framework.installer import FrameworkInstaller


class BthmInstaller(FrameworkInstaller):
  def __init__(self, name, logger, topdirectory,
               softwares, executor,
               base_archive_name="bthm-install", delete=False):
    FrameworkInstaller.__init__(self, name, logger, topdirectory, softwares, executor)

    self.base_archive_name = base_archive_name
    self.delete = delete
    has_prerequisites = False
    self.etape = 1
    self.nb_etapes = 1

    self.notic_soft = self.map_name_to_softwares.get("BTHM_NOTIC", None)
    if not self.notic_soft:
      self.logger.error("Le software BTHM_NOTIC est manquant")

    for software in self.softwares:
      if software.get_type() == "prerequisite" and software.get_pack_name():
        has_prerequisites = True
      if software.get_type() == "composante" and software.get_pack_name():
        self.nb_etapes += 1
    if has_prerequisites:
      self.nb_etapes += 1

    if not topdirectory:
      self.topdirectory = os.path.join(self.executor.options.get_global_option("packs_directory"), "pack")

    #self.logger.info("Installer %s initialisation ends"%self.name)
    pass

  def begin(self):
    #self.logger.info("Installer %s begin step starts"%self.name)
    # Step 1: Create pack dir (remove existing one)
    if os.path.exists(self.topdirectory):
      misc.fast_delete(self.topdirectory)
    os.makedirs(self.topdirectory)
    #self.logger.info("Installer %s begin step ends"%self.name)
    return True

  def create(self):
    #self.logger.info("Installer %s create step starts"%self.name)
    # Step 0: création des archives des composantes

    prerequisites_list = []


    commands = """
#!/bin/bash
#
# Script de configuration de la bthm
#
# Auteur : J. Dereux
#
###########################################################
#
set -e
#
usage()
{
cat<<EOF
usage: $0 [-h] [-t DIR] [-o DIR] [-a DIR] [-d BDT]

Ce script installe la BTHM hors NACRE.

Options:
 -h    Affiche l'aide
 -t    Répertoire d'installation de la bthm (a renseigner obligatoirement) : le repertoire doit exister, il n'est pas cree par ce script
 -o    Chemin vers ORACLE (par defaut : vide) : peut etre renseigne ulterieurement dans notic/SETENV.sh
 -a    Chemin vers tns_admin.ora (par defaut : vide) : peut etre renseigne ulterieurement dans notic/SETENV.sh
 -d    Chemin vers les BDT a utiliser : peut etre renseigne ulterieurement dans notic/SETENV.sh
EOF
}
#
# Gestion des options de lancement
while getopts "ht:o:a:d:" OPTION
do
    case $OPTION in
    h)
      usage
      exit 1
      ;;
    t)
      PATH_INSTALL="$OPTARG"
      ;;
    o)
      PATH_ORACLE="$OPTARG"
      ;;
    a)
      PATH_TNS_ADMIN="$OPTARG"
      ;;
    d)
      PATH_BDT_NOTIC="$OPTARG"
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
    esac
done
#
[ ! -d "$PATH_INSTALL" ] && ( echo "Le repertoire d'installation est mal renseigne ou n'existe pas, il vaut : $PATH_INSTALL"; exit 1 )
[[ "$PATH_INSTALL" = /* ]] || ( echo "Le repertoire d'installation n'est pas un chemin absolu, il vaut : $PATH_INSTALL"; exit 1 )
#
FILE_DIR=$(dirname "$0")
#
cp README $PATH_INSTALL/
cp test_configuration_bthm.py $PATH_INSTALL/
#
# copie et decompression des archives dans $PATH_INSTALL
cd $FILE_DIR
for composanteTar in $(ls *.tar.gz)
do
    cp $composanteTar $PATH_INSTALL/
    cd $PATH_INSTALL/
    tar xzvf $composanteTar
    rm $composanteTar
    cd -
done
#
###########################################################
# Mise en place des chemins dans SETENV.sh
cd $PATH_INSTALL/notic
cp SETENV.sh.conf SETENV.sh
#
sed -i "s#<RepInstallBTHM>#$PATH_INSTALL#g" SETENV.sh
sed -i "s#<RepInstallNOTIC>#$PATH_INSTALL/notic#g" SETENV.sh
sed -i "s#<CheminMusicale>#$PATH_INSTALL/musicale#g" SETENV.sh
sed -i "s#<CheminCYRANO3>#$PATH_INSTALL/cyrano3#g" SETENV.sh
sed -i "s#<VersionCYRANO3>#c3res.V3-4a#g" SETENV.sh
sed -i "s#<CheminC3POST>#$PATH_INSTALL/cyrano3#g" SETENV.sh
sed -i "s#<VersionC3POST>#c3post-4.4#g" SETENV.sh
sed -i "s#<CheminModuleHistorique>#$PATH_INSTALL/module_historique#g" SETENV.sh
#
echo -e "\\nLes chemins des composants ont bien ete mis a jour dans $PATH_INSTALL/notic/SETENV.sh\\n"
#
if [ "$PATH_ORACLE" != "" ]; then
    # PATH_ORACLE ne peut pas terminer par / sinon oracle bugge...
    PATH_ORACLE=${PATH_ORACLE%/}
    sed -i "s#<oracle_home>#$PATH_ORACLE#g" SETENV.sh
    echo -e "Le PATH_ORACLE a bien ete mis a jour dans $PATH_INSTALL/notic/SETENV.sh\\n"
else
    echo -e "--> \e[41mATTENTION\e[49m : le chemin vers ORACLE est soit errone, soit non renseigne."
    echo -e "Veuillez remplacer la chaine <oracle_home> dans $PATH_INSTALL/notic/SETENV.sh\\n"
fi
if [ "$PATH_TNS_ADMIN" != "" ]; then
    sed -i "s#<tns_admin>#$PATH_TNS_ADMIN#g" SETENV.sh
    echo -e "Le PATH_TNS_ADMIN a bien ete mis a jour dans $PATH_INSTALL/notic/SETENV.sh\\n"
else
    echo -e "--> \e[41mATTENTION\e[49m : le chemin vers TNS_ADMIN est soit errone, soit non renseigne."
    echo -e "Veuillez remplacer la chaine <tns_admin> dans $PATH_INSTALL/notic/SETENV.sh\\n"
fi
if [ "$PATH_BDT_NOTIC" != "" ]; then
    sed -i "s#<RepDataDir>#$PATH_BDT_NOTIC#g" SETENV.sh
    echo -e "Le $PATH_BDT_NOTIC a bien ete mis a jour dans $PATH_INSTALL/notic/SETENV.sh\\n"
else
    echo -e "--> \e[41mATTENTION\e[49m : le chemin vers les BDT NOTIC est soit errone, soit non renseigne."
    echo -e "Veuillez remplacer la chaine <RepDataDir> dans $PATH_INSTALL/notic/SETENV.sh\\n"
fi
#
###########################################################
# Copie de private.key dans le module_historique
echo -e "\\n"
echo -e "--> \e[41mATTENTION\e[49m : merci de verifier que le fichier \e[41mprivate.key\e[49m comporte les bonnes informations\\n"
#
###########################################################
# Configuration de default.xml dans musicale
cd $PATH_INSTALL/musicale
cp default.xml.conf default.xml
#
sed -i "s#<adresse_musicale>#http://sinfonie-pi.edf.fr:3201#g" default.xml
sed -i "s#<mdp_musicale>#Hjdn2Pdm#g" default.xml
sed -i "s#<login_musicale>#NOTIC#g" default.xml
#
echo -e "--> \e[41mATTENTION\e[49m : merci de verifier que le fichier \e[41mmusicale/default.xml\e[49m comporte les bonnes informations.\\n"
#
echo -e "Fin de la configuration de la BTHM hors NACRE.\\n"
#
set +e
#

"""
    path_file = os.path.join(self.topdirectory, "configure.sh")
    with open(path_file, 'w') as configure_file:
      configure_file.write(commands)
    subprocess.call(['chmod', '755', path_file])
    ##############################################################"

    # generation du README
    readme = """
* Auteur : Julien Dereux
* email  : julien.dereux@edf.fr

Le pack BTHM v2.3.0 comporte les modules suivants :
- notic v2.3.0
- musicale v2.3.0 (numero de version pour NACRE)
- module_historique v1.4.0
- cyrano3 : cyrano3 v3.4 + c3post v4.4

Les chemins vers ORACLE et tns_admin.ora peuvent être renseignés au lancement
du script configure.sh : lancez
./configure.sh -h
pour plus d'informations.

Les chemins des différents modules peuvent être changés dans le script
notic/SETENV.sh

Cette version de la BTHM gère les assemblages resquelettés et transférés.
En conséquence, les chemins renseignés pour PATH_ORACLE et PATH_TNS_ADMIN doivent
pointer vers un client ORACLE compatible avec une version de SINFONIE supérieure
ou égale à 2.6.

Pour voir si la BTHM a été configurée correctement, vous pouvez lancer
./test_configuration_bthm.py
"""

    path_file = os.path.join(self.topdirectory, "README")
    with open(path_file, 'w') as configure_file:
      configure_file.write(readme)
    ##############################################################"

    # generation du cas test pour le configuration
    test_configuration = """#!/usr/bin/python
#
# Test de la configuration de la BTHM
# On cree une etude et on lance la BTHM:
#   - lancement de NOTIC en rappatriant tous les fichiers depuis MUSICALE
#   - lancement des calculs CYRANO3
# 
import os
import sys
import imp

import os.path as osp
from notic import HN_cyr3_rafale, HN_c3p
from notic.testlib import APITestRunner
from notic.model import CampaignInfo
from notic.api import Study

current_dir = os.getcwd()
test_name = 'test_configuration_bthm'
dir_test = osp.join(current_dir, test_name)

if osp.exists(dir_test):
    print "Le repertoire {0} existe deja, je sors en erreur".format(dir_test)
    sys.exit(1)

class BTHMRunner(APITestRunner):
    campaign_info = CampaignInfo('BU225PK')

    @classmethod
    def instantiate(cls, FUNC_TESTS_PATH, name, donnees, resultats):
        return cls(FUNC_TESTS_PATH, name, donnees, resultats)
        
    def setup_test_environment(self):
        if not self.campaign_info:
            raise ValueError('You must define self.campaign_info first')
        workdir = dir_test
        if not osp.exists(workdir):
            os.makedirs(workdir)
        self.path_study = osp.join(workdir, self.campaign_info.name)
        if osp.isdir(self.path_study):
            shutil.rmtree(self.path_study)
        os.mkdir(self.path_study)
        self.path_resultats = osp.join(self.path_study, 'RESULTATS_DIFF')
        os.mkdir(self.path_resultats)
        self.ref_path = osp.abspath(osp.join(self.base_path, self.test_name,
                                             'ref', self.campaign_info.name))
    def run_computation(self):
        comp_path = os.path.join(self.path_study, 'NOTIC')
        study = Study(comp_path, 'bdt_exploit_new.yaml')
        with study.setup_work_directory():
            comp = study.new_computation(self.campaign_info.name)
            comp.add_assemblies(names="FX1T9L")
            study.add_computation(comp)
            study.retrieve_files()
            comp.init_campaigns()
            comp.compute_power_history()
            comp.select_pins(mode='pen')
            comp.compare_histories()
            comp.select_cyrano3_pins('dep')
            study.generate_histories_report()
            study.generate_report()
            study.generate_cyrano3_dataset()
            study.move_output_files()
            # go out of notic dir but stay in the test dir
            os.chdir(os.pardir)
            HN_cyr3_rafale.run_cyra3_batch()
            HN_c3p.run_c3post(gestion='GESTION_CYCLADES')

RUNNER_CLASS = BTHMRunner

DONNEES_ENTREE = []
RESULTATS      = {}

runner = RUNNER_CLASS.instantiate(current_dir, test_name, DONNEES_ENTREE, RESULTATS)
runner.run()

dic_file = open(osp.join(dir_test,"BU225PK/C3POST/OUT/RESU.BTHM"),"r")
lines = dic_file.readlines()
if not ("6.710790E+01" in mot for mot in lines):
    print "Regression dans le fichier RESU.BTHM detectee : je sors en erreur"
    sys.exit(1)

print "Le test de configuration de la BTHM s'est bien deroule.\\n"

"""

    path_file = os.path.join(self.topdirectory, "test_configuration_bthm.py")
    with open(path_file, 'w') as configure_file:
      configure_file.write(test_configuration)
    subprocess.call(['chmod', '755', path_file])

    # Step 1: Création des archives des composantes
    tmpdir = tempfile.mkdtemp()
    for soft in list(self.map_name_to_softwares.values()):
      if soft.get_type() == "prerequisite" and soft.get_pack_name():
        prerequisites_list.append(soft)
      if soft.get_type() != "composante" or not soft.get_pack_name():
        continue
      self.logger.info("PACK {0:{fill}2}/{1:2} : Composante {2}...".format(self.etape, self.nb_etapes, soft.get_pack_name(), fill="0"))
      cmd = soft.create_pack(self.topdirectory, self.base_archive_name, tmpdir)
      if not cmd:
        self.logger.info("Composante {0} non créée...".format(soft.get_pack_name()))
        continue
      self.logger.debug(cmd)
      try:
        ret = subprocess.call(cmd, shell=True)
        if ret != 0:
          self.logger.error("Composante {0}: KO ({1})".format(soft.get_pack_name(), ret))
      except OSError as e:
        self.logger.error("Execution of command {0} failed: {0}".format(cmd, e))
      self.etape += 1
      pass
    misc.fast_delete(tmpdir)

    # Step 3: création de l'archive des prérequis (home/lib/[prerequis])
    if prerequisites_list:
      self.logger.info("PACK {0:{fill}2}/{1:2} : Composante prerequis...".format(self.etape, self.nb_etapes, fill="0"))
      tmpdir = tempfile.mkdtemp()
      libdir = os.path.join(tmpdir, "lib")
      os.makedirs(libdir)
      for prerequisite_soft in prerequisites_list:
        prerequisite_install_dir = prerequisite_soft.get_pack_name()
        if not prerequisite_install_dir:
          self.logger.error("Composante prerequis: KO")
        prerequisite_new_dir = os.path.join(libdir, prerequisite_install_dir)
        shutil.copytree(prerequisite_soft.install_directory, prerequisite_new_dir)
      cmd = "tar czf {1}/{2}_prerequis.tar.gz --exclude-vcs -C {0} lib".format(tmpdir, self.topdirectory, self.base_archive_name)
      self.logger.debug(cmd)
      try:
        ret = subprocess.call(cmd, shell=True)
        if ret != 0:
          self.logger.error("Composante prerequis: KO ({1})".format(soft.get_pack_name(), ret))
      except OSError as e:
        self.logger.error("Execution of command {0} failed: {0}".format(cmd, e))
      self.etape += 1
      misc.fast_delete(tmpdir)


    #self.logger.info("Installer %s create step ends"%self.name)
    return True

  def end(self):
    #self.logger.info("Installer %s end step starts"%self.name)
    self.logger.info("PACK {0:{fill}2}/{1:2} : Packaging...".format(self.etape, self.nb_etapes, fill="0"))
    installer_tgz_name = self.name + ".tar.gz"
    installer_tgz_path = os.path.join(os.path.dirname(self.topdirectory), installer_tgz_name)
    self.logger.info("Création de l'archive {0}".format(installer_tgz_name))
    ret = self.create_pack(installer_tgz_path)
    if ret and self.delete:
      misc.fast_delete(self.topdirectory)
    #self.logger.info("Installer %s end step starts"%self.name)
    return ret

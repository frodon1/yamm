#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

from __future__ import print_function
import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels, LoggerException
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command

from yamm.projects.nacre.softwares import prerequisites as nacre_prerequisites
from yamm.projects.nacre           import project       as nacre_project

from yamm.projects.bthm.softwares  import prerequisites as default_prerequisites
from yamm.projects.bthm.softwares  import composantes   as default_composantes
from yamm.projects.bthm            import versions      as default_versions

from yamm.projects.bthm.installer.bthm_installer import BthmInstaller

def get_ftp_server():
  return nacre_project.get_ftp_server()

class Project(FrameworkProject):

  def __init__(self, log_name="BTHM_HORS_NACRE", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.init_bthm_vars()
    self.define_bthm_options()
    pass

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="BTHM_HORS_NACRE", verbose=self.verbose_level)
    # Import des prerequis
    for name in nacre_prerequisites.softwares_list:
      self.add_software_in_catalog(name, "yamm.projects.nacre.softwares.prerequisites")
    for module_name in default_prerequisites.softwares_list:
      self.add_software_in_catalog(module_name, "yamm.projects.bthm.softwares.prerequisites")
    # Import des composantes
    for module_name in default_composantes.softwares_list:
      self.add_software_in_catalog(module_name, "yamm.projects.bthm.softwares.composantes")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.bthm.versions")
      pass
    self.logger.debug("End init catalog")
    pass

  def init_bthm_vars(self):
    pass

  def define_bthm_options(self):
    # Categories du projet NACRE
    self.options.add_category("prerequisite")
    self.options.add_category("composante")

    self.options.add_option("packs_directory", "", ["global"])

    # Options pour les dépôts
    self.options.add_option("vcs_username", "", ["global", "category", "software"])
    self.options.add_option("vcs_server", "", ["global", "category", "software"])

    self.options.add_option("install_ref", "", ["global"])


    self.options.add_option("chaine_connection_oracle", "", ["global"])
    self.options.add_option("oracle_sid", "", ["global"])

    self.options.add_option("adresse_musicale", "", ["global"])
    self.options.add_option("login_musicale", "", ["global"])
    self.options.add_option("mdp_musicale", "", ["global"])


#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "bthm"))
      pass
    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)
    # Set main directory for softwares
    if not current_command_options.get_global_option("main_software_directory"):
      current_command_options.set_global_option("main_software_directory", os.path.join(current_command_options.get_global_option("version_directory")))
      pass

    # Set Specific packs directory
    if not current_command_options.get_global_option("packs_directory"):
      current_command_options.set_global_option("packs_directory", os.path.join(current_command_options.get_global_option("top_directory"), "packs"))
      pass

    if not current_command_options.get_global_option("vcs_server"):
      current_command_options.set_global_option("vcs_server", "git.forge.pleiade.edf.fr")
      pass
    if not current_command_options.get_global_option("vcs_username"):
      current_command_options.set_global_option("vcs_username", "")
      pass

    if not current_command_options.get_global_option("chaine_connection_oracle"):
      current_command_options.set_global_option("chaine_connection_oracle", "nacre/C0JSY4P-56")
      pass
    if not current_command_options.get_global_option("oracle_sid"):
      current_command_options.set_global_option("oracle_sid", "NRD")
      pass

    if not current_command_options.get_global_option("adresse_musicale"):
      current_command_options.set_global_option("adresse_musicale", "http://sinfonie-pi.edf.fr:3201") # http://10.181.225.12:10201
      pass
    if not current_command_options.get_global_option("login_musicale"):
      current_command_options.set_global_option("login_musicale", "NOTIC")
      pass
    if not current_command_options.get_global_option("mdp_musicale"):
      current_command_options.set_global_option("mdp_musicale", "Hjdn2Pdm")
      pass


    if not current_command_options.is_category_option("prerequisite", "source_type"):
      current_command_options.set_category_option("prerequisite", "source_type", "archive")
    if not current_command_options.is_category_option("composante", "source_type"):
      current_command_options.set_category_option("composante", "source_type", "remote")
    pass

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):

    for category in ['prerequisite', 'composante']:
      current_command_options.check_allowed_value('source_type',
                                       ["archive", "remote"],
                                       category=category,
                                       logger=self.logger)
      if current_command_options.is_category_option(category,
                                                    'main_software_directory'):
        current_command_options.check_abs_path('main_software_directory',
                                               category = category,
                                               logger=self.logger)

    current_command_options.check_abs_path('packs_directory',
                                           logger=self.logger)

#######
#
# Print methods
#
#######

  def print_general_configuration(self, current_command_options):
    FrameworkProject.print_general_configuration(self, current_command_options)
    self.logger.info("BTHM Version       : %s" % self.version)
    pass

#######
#
# Méthodes internes pour les commandes
#
#######

  def update_configuration(self):
    if self.current_command_options.get_global_option("vcs_server") == "":
      self.current_command_options.set_global_option("vcs_server", "git.forge.pleiade.edf.fr")
      pass
    if self.current_command_options.get_global_option("archive_remote_address") == "":
      self.current_command_options.set_global_option("archive_remote_address", os.path.join(get_ftp_server()))
      pass
    pass

#######
#
# Méthodes spécifiques au projet bthm
#
#######

  @yamm_command("Configuration of local installation")
  def config_local_install(self):
    """
      This command configures the file depending on.
    """

    # Prepare execution with software_mode = "nothing"
    for software in self.current_command_softwares:
      #software.software_mode = "nothing"
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               "nothing",
                               self.current_command_version_object,
                               self.current_command_python_version)

    # Configuration des softwares (appel à la méthode config_local_install des softwares)
    post_install_commands = {}
    for software in self.current_command_softwares:
      software.software_mode = "post_install_config"
      post_install_commands[software.name] = software.post_install_commands
      software.post_install_commands = []
      software.config_local_install()
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
    try:
      self.execute_command()
    except LoggerException as e:
      pass
    except Exception as e:
      raise e

    for software in self.current_command_softwares:
      software.software_mode = "nothing"
      software.post_install_commands = post_install_commands[software.name]
    return True

  @yamm_command("Creation of an installation pack")
  def create_pack(self, base_archive_name="bthm-install", delete=False):
    """
    This command creates the pack for NOTIC
    :param string base_archive_name: Defines the base name of the archives of the components in the pack
    :param bool delete: If True, deletes the pack directory once the archives are created
    """

    pack_name = "pack_bthm-{0}".format(self.version)

    # Step 1: Create installer instance
    pack_dir = os.path.join(self.current_command_options.get_global_option("packs_directory"), "pack_bthm-{0}".format(self.version))

    bthm_installer = BthmInstaller(name=pack_name,
                                    logger=self.logger,
                                    topdirectory=pack_dir,
                                    softwares=self.current_command_softwares,
                                    executor=self.current_command_executor,
                                    base_archive_name=base_archive_name,
                                    delete=delete)

    return bthm_installer.execute()

#######
#
# Commands
#
#######

if __name__ == "__main__":

  print("Testing bthm project class")
  pro = Project(1)
  pro.set_version("V2_3_0")
  #pro.options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "bthm"))
  pro.print_configuration()
  pro.nothing()
  #pro.start()


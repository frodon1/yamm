#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

import os
import string
import tempfile

from yamm.core.base import misc
from yamm.core.framework.software import FrameworkSoftware


class BthmSoftware(FrameworkSoftware):
  def __init__(self, name="NOT CONFIGURED", version="NOT CONFIGURED",
               verbose=misc.VerboseLevels.INFO, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)
    if self.get_type() == "composante":
      self.root_repository_template = string.Template('https://$server/git')

  def default_values_hook(self):
    pass

  def init_variables(self):
    self.rep_home = os.path.join(self.project_options.get_option(self.name,
                                                                 "main_software_directory"),
                                 "install")

  def config_local_install(self, dependency_name="ALL"):
    # This method should be used to reconfigure the software
    pass

  def configure_git_composantes(self):
    self.project_options.set_software_option(self.name,
        "software_src_directory",
        os.path.join(self.project_options.get_option(self.name, "top_directory"),
        "composantes", self.name.capitalize()))

  def get_type(self):
    """
    This method returns the type of software.
    There are 2 types in the NOTIC project:

      - prerequisite
      - composante
    """
    return "type not defined for software %s" % self.name

  def create_pack(self, pack_dir, base_archive_name="notic-install", tmpdir=""):
    """
    This method will return the command used to create the pack of the software.
    """
    composante = self.get_pack_name()
    if not composante or self.get_type() != "composante":
      return ""
    delete = False
    if not tmpdir:
      delete = True
      tmpdir = tempfile.mkdtemp()
    pack_archive_filename = self.get_archive_pack_name(pack_dir, base_archive_name)
    if not pack_archive_filename:
      return ""
    cmd = "cd {0} ; rsync -az  --exclude '*.log' --exclude '.yamm' --exclude '.install_ok_*' . {1}/{2}  > {0}/pack.log 2>&1 ;".format(self.install_directory, tmpdir, composante)
    cmd += "cd {0} ; tar czf {1} {2} ; ".format(tmpdir, pack_archive_filename, composante)
    if delete:
      cmd += "rm -rf {0} ;".format(tmpdir)
    return cmd

  def get_archive_pack_name(self, pack_dir, base_archive_name="notic-install"):
    composante = self.get_pack_name()
    if not composante or self.get_type() != "composante":
      return ""
    return os.path.join(pack_dir, "{0}_{1}.tar.gz".format(base_archive_name, composante))


  def get_pack_name(self):
    """
    This method returns the name of the archive of the software in the pack.
    """
    return None

  def get_config_files_list(self):
    return []
  

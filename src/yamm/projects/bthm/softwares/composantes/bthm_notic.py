#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

from yamm.projects.bthm.software import BthmSoftware
import os

software_name = "BTHM_NOTIC"

class BTHM_NOTIC(BthmSoftware):

  def default_values_hook(self):
    self.executor_software_name = "notic"
    BthmSoftware.default_values_hook(self)

  def init_variables(self):
    BthmSoftware.init_variables(self)
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name.lower())

    if not self.project_options.is_software_option(self.name, "vcs_server"):
      self.project_options.set_software_option(software_name, "vcs_server", "git.forge.pleiade.edf.fr")

    version_for_archive = self.get_version_for_archive().replace('/', '-')

    self.archive_file_name = "notic-{0}.tar.gz".format(version_for_archive)
    self.archive_type = "tar.gz"

    self.compil_type = "rsync"

    self.remote_type = "git"
    self.configure_git_composantes()
    self.repository_name = "bthm.notic.git"
    self.root_repository = self.root_repository_template.substitute(server=self.project_options.get_option(self.name, "vcs_server"))
    self.tag = self.version

    self.config_local_install("SELF")

  def init_dependency_list(self):
    self.software_dependency_list = ["ORACLE_CLIENT", "MUSICALE", "MODULE_HISTORIQUE", "CYRANO3"]

  def get_config_files_list(self):
    config_files_list = BthmSoftware.get_config_files_list(self)
    config_files_list.append("SETENV.sh")
    return config_files_list

  def config_local_install(self, dependency_name="ALL", nacre_build=False):
    """
    config_local_install classique avec l'option nacre_build qui permet de passer dans CYRANO3
    lors de l'installation de la BTHM hors NACRE
    """
    if dependency_name == "SELF" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<RepInstallNOTIC>",
          "$CURRENT_SOFTWARE_INSTALL_DIR",
          "SETENV.sh")

    if dependency_name == "ORACLE_CLIENT" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<oracle_home>",
          "$ORACLE_CLIENT_INSTALL_DIR/instantclient", "SETENV.sh")
      tns_admin = self.project_options.get_global_option("tns_admin")
      if not tns_admin or tns_admin == "TO_BE_DEFINED":
        tns_admin = "$ORACLE_CLIENT_INSTALL_DIR"
      self.replaceStringInFilePostInstall("<tns_admin>", tns_admin, "SETENV.sh")

    if dependency_name == "MUSICALE" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminMusicale>", "$MUSICALE_INSTALL_DIR", "SETENV.sh")

    if dependency_name == "MODULE_HISTORIQUE" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminModuleHistorique>", "$MODULE_HISTORIQUE_INSTALL_DIR", "SETENV.sh", True)

    if not nacre_build and (dependency_name == "CYRANO3" or dependency_name == "ALL"):
      self.replaceStringInFilePostInstall("<CheminCYRANO3>", "$CYRANO3_INSTALL_DIR/cyrano3-3.4", "SETENV.sh", True)
      self.replaceStringInFilePostInstall("<CheminC3POST>", "$CYRANO3_INSTALL_DIR/c3post", "SETENV.sh", True)
      self.replaceStringInFilePostInstall("<VersionCYRANO3>", "c3res.V3-4a", "SETENV.sh", True)
      self.replaceStringInFilePostInstall("<VersionC3POST>", "c3post-4.4", "SETENV.sh", True)

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = BthmSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "ORACLE_CLIENT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MUSICALE":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MODULE_HISTORIQUE":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return self.executor_software_name

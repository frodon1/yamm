#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

from yamm.projects.bthm.software import BthmSoftware
from yamm.projects.nacre.softwares.composantes.codes import CODES as NACRE_CODES

import os

software_name = "CYRANO3"

class CYRANO3(NACRE_CODES, BthmSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    NACRE_CODES.__init__(self, name, version, verbose, **kwargs)
    BthmSoftware.__init__(self, name, version, verbose, **kwargs)

  def init_variables(self):
    # on initialise NACRE_CODES pour effectuer self.config_local_install("")
    NACRE_CODES.init_variables(self, config_mode="")
    BthmSoftware.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name.lower())

    # on n'extrait que CYRANO3 et C3POST
    self.make_target = '{0} {1}'.format(self.executable_cyr3, self.executable_c3post)

  def config_local_install(self, dependency_name="NOTIC"):
    self.add_cyrano3_post_install_commands(dependency_name)    

  def init_dependency_list(self):
    self.software_dependency_list = ["NOTIC"]

  def get_pack_name(self):
    return software_name.lower()

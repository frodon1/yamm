#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

from yamm.projects.bthm.software import BthmSoftware
from yamm.projects.nacre.softwares.composantes.module_historique import MODULE_HISTORIQUE as NACRE_MODULE_HISTORIQUE

import os

software_name = "MODULE_HISTORIQUE"

class MODULE_HISTORIQUE(NACRE_MODULE_HISTORIQUE, BthmSoftware):

  def init_variables(self):
    NACRE_MODULE_HISTORIQUE.init_variables(self)
    BthmSoftware.init_variables(self)
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name.lower())

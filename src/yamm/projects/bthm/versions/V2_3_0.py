#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.core.framework.version import FrameworkVersion

version_name = "V2_3_0"
# TODO : remplacer par BTHM_2-2-0 ou release-V2-2-0 a la fin
version_value = "git_tag/BTHM_2-3-0-rc1"

class V2_3_0(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    self.add_software("ORACLE_CLIENT",     "10.2")
    
    self.add_software("MUSICALE",          version_value, "V2-2-0")

    self.add_software("MODULE_HISTORIQUE", "integration_bthm_230", "V1-4-0")

    self.add_software("BTHM_NOTIC",         "master", "V2_3_0")
 
    # ce software contient cyrano3 v3.4 et c3post v4.4
    # WARNING : il faut indiquer la ordered_version de CODES issu de NACRE !
    self.add_software("CYRANO3",           version_value, "V2-4-0")

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Classe de définition du projet Cocagne pour une installation "stand alone" hors Salomé 
# (non utilisé par Salome-coeur)
#
##### Modules de creation d'un nouveau projet #########
from builtins import str
import os
import sys
import subprocess
import stat

yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command

from yamm.core.framework.installer import installer_bar_cat

##### Modules de definition de prerequis par defaut #########

from yamm.projects.salome.softwares import prerequisites as salome_prerequisites
from yamm.projects.salome.softwares import tools         as salome_tools
from yamm.projects.salome.softwares import modules       as salome_modules
from yamm.projects.salome           import project       as salome_project

from yamm.projects.cocagne     import softwares as cocagne_softwares
from yamm.projects.cocagne     import versions

######## Pour les prerequis deplacables

import datetime, time
import shutil
import tarfile

from yamm.projects.cocagne.installer import strings

from yamm.projects.cocagne.installer.strings import cocagne_installer_decompress_usage
from yamm.projects.cocagne.installer.strings import cocagne_installer_decompress_template

def get_ftp_server():
  return salome_project.get_ftp_server()

##### Classe mere du projet cocagne #########

COCAGNE_PUBLIC = "/data/projets/projets.002/andromede.005/cocagne/prerequisites/"

class Project(FrameworkProject):

    def __init__(self, log_name="COCAGNE", verbose_level=VerboseLevels.INFO):
        FrameworkProject.__init__(self, log_name, verbose_level)

        #Creation d'un fichier d'environnement specifique
        self.define_cocagne_specific_options()


    def init_catalog(self):
        """
        Definition des prerequis cocagne
        """
        self.logger.debug("Init catalog")
        self.catalog = FrameworkCatalog(name="COCAGNE", verbose=self.verbose_level)

        # Import des softwares
        for software_name in salome_prerequisites.modules_list:
            self.add_software_in_catalog(software_name, "yamm.projects.salome.softwares.prerequisites")

        for software_name in salome_tools.modules_list:
            self.add_software_in_catalog(software_name, "yamm.projects.salome.softwares.tools")

        for software_name in salome_modules.modules_list:
            self.add_software_in_catalog(software_name, "yamm.projects.salome.softwares.modules")

        for software_name in cocagne_softwares.softwares_list:
            self.add_software_in_catalog(software_name, "yamm.projects.cocagne.softwares")

        # Import des versions
        for version_name in versions.versions_list:
            self.add_version_in_catalog(version_name, "yamm.projects.cocagne.versions")

        self.logger.debug("End init catalog")

    def define_cocagne_specific_options(self):
        #Ajout d'une option de definition du nom du bashrc d environnement
        self.options.add_option("cocagne_env_file", "", ["global"])

        self.options.add_option("cocagne_public_dir", "", ["global"])

        # Options pour Python mega widget
        self.options.add_option("pmw_archive_address", "", ["software"])
        self.options.add_option("pmw_archive_filename", "", ["software"])


        # Options pour compatiblité avec SALOME
        self.options.add_option("salome_prerequisites_env_file", "", ["global"])
        self.options.add_option("salome_modules_env_file", "", ["global"])
        self.options.add_option("cocagne_archive_filename", "", ["software"])
        self.options.add_option("cocagne_archive_address", "", ["software"])
        self.options.add_option("cocagne_optim", "", ["software"])
        self.options.add_option("cocagne_precision", "", ["software"])
        self.options.add_option("cocagne_zipped_dklibs", "", ["software"])
        self.options.add_option("cocagne_zipped_saphybs", "", ["software"])
        self.options.add_option("cocagne_thm_libs", "", ["software"])
        self.options.add_option("cocagne_extract_tests", False, ["software"])
        self.options.add_category("prerequisite")
        self.options.add_category("tool")
        self.options.add_category("module")
        self.options.add_category("main")


    def set_default_options_values(self, current_command_options):
        # Set Specific top directory
        # top_directory = current_command_options.get_global_option("top_directory")
        #if not top_directory:
            #Renverra une erreur a la verification : l'utilisateur doit definir son top_directory
            #current_command_options.set_global_option("top_directory", "")

        # Framework project default values
        FrameworkProject.set_default_options_values(self, current_command_options)

        version_directory = current_command_options.get_global_option("version_directory")

        # Specific COCAGNE options
        if not current_command_options.get_global_option("cocagne_public_dir"):
            current_command_options.set_global_option("cocagne_public_dir", COCAGNE_PUBLIC)

        if not current_command_options.get_global_option("cocagne_env_file"):
            current_command_options.set_global_option("cocagne_env_file", "%s/%s.bashrc" % (version_directory, self.version))

        # Set main directory for softwares
        #if not current_command_options.get_global_option("main_software_directory"):
        #    current_command_options.set_global_option("main_software_directory", "%s/prerequisites" % (top_directory,) ) 
        if not current_command_options.is_category_option("tool", "main_software_directory"):
            current_command_options.set_category_option("tool", "main_software_directory", os.path.join(version_directory, "tools"))
        if not current_command_options.is_category_option("module", "main_software_directory"):
            current_command_options.set_category_option("module", "main_software_directory", os.path.join(version_directory, "modules"))
        if not current_command_options.is_category_option("prerequisite", "main_software_directory"):
            current_command_options.set_category_option("prerequisite", "main_software_directory", os.path.join(version_directory, "prerequisites"))
        if not current_command_options.is_category_option("main", "main_software_directory"):
            current_command_options.set_category_option("main", "main_software_directory", version_directory)

        # Specific configuration to use SALOME's software
        # Defines default source_type values for all software
        current_command_options.set_global_option("source_type", "archive")
        if not current_command_options.is_category_option("main", "source_type"):
          current_command_options.set_category_option("main", "source_type", "remote")
        if current_command_options.get_category_option("prerequisite", "archive_remote_address") == "":
          current_command_options.set_category_option("prerequisite", "archive_remote_address", os.path.join(get_ftp_server(), 'sources', 'prerequis'))
        if current_command_options.get_category_option("tool", "archive_remote_address") == "":
          current_command_options.set_category_option("tool", "archive_remote_address", os.path.join(get_ftp_server(), 'sources', 'tools'))
        if current_command_options.get_category_option("module", "archive_remote_address") == "":
          current_command_options.set_category_option("module", "archive_remote_address", os.path.join(get_ftp_server(), 'sources', 'modules'))
        current_command_options.set_global_option("salome_prerequisites_env_file", current_command_options.get_global_option("cocagne_env_file"))
        current_command_options.set_global_option("salome_modules_env_file", current_command_options.get_global_option("cocagne_env_file"))

        #Options to compile MED in standalone
        # cmake -DSALOME_MED_STANDALONE=ON 
        # -DSALOME_MED_ENABLE_PYTHON=OFF 
        # -DMEDFILE_ROOT_DIR=../Medfichier-310rc3-hdf51814 
        # -DHDF5_ROOT_DIR=…/Hdf5-1814 
        # -DSALOME_BUILD_TESTS=OFF 
        # -DSALOME_BUILD_DOC=OFF 
        # -DSALOME_MED_ENABLE_RENUMBER=OFF 
        # -DSALOME_MED_PARTITIONER_METIS=OFF 
        # -DSALOME_MED_PARTITIONER_SCOTCH=OFF 
        #-DCMAKE_INSTALL_PREFIX=[install_dir] MED_SRC
        current_command_options.set_software_option("MED", "software_additional_config_options", " -DSALOME_MED_STANDALONE=ON -DSALOME_MED_ENABLE_PYTHON=OFF -DSALOME_BUILD_TESTS=OFF -DSALOME_BUILD_DOC=OFF -DSALOME_MED_ENABLE_RENUMBER=OFF -DSALOME_MED_PARTITIONER_METIS=OFF -DSALOME_MED_PARTITIONER_SCOTCH=OFF ")

        #Répertoire de creation des archives binaires
        if not current_command_options.is_category_option("prerequisite", "binary_archive_topdir"):
            current_command_options.set_category_option("prerequisite", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("prerequisite", "main_software_directory"), "binary_archives"))
        if not current_command_options.is_category_option("tool", "binary_archive_topdir"):
            current_command_options.set_category_option("tool", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("tool", "main_software_directory"), "binary_archives"))
        if not current_command_options.is_category_option("module", "binary_archive_topdir"):
            current_command_options.set_category_option("module", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("module", "main_software_directory"), "binary_archives"))
        if not current_command_options.is_category_option("main", "binary_archive_topdir"):
            current_command_options.set_category_option("main", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("main", "main_software_directory"), "binary_archives"))

        #Repertoire de récupération des tar.gz pour l'archive deplacable (meme répertoire que binary_archive_topdir)
        if not current_command_options.is_category_option("prerequisite", "binary_archive_url"):
          current_command_options.set_category_option("prerequisite", "binary_archive_url", os.path.join(current_command_options.get_category_option("prerequisite", "main_software_directory"), "binary_archives", "%(name)s.tgz"))
        if not current_command_options.is_category_option("tool", "binary_archive_url"):
          current_command_options.set_category_option("tool", "binary_archive_url", os.path.join(current_command_options.get_category_option("tool", "main_software_directory"), "binary_archives", "%(name)s.tgz"))
        if not current_command_options.is_category_option("module", "binary_archive_url"):
          current_command_options.set_category_option("module", "binary_archive_url", os.path.join(current_command_options.get_category_option("module", "main_software_directory"), "binary_archives", "%(name)s.tgz"))
        if not current_command_options.is_category_option("main", "binary_archive_url"):
          current_command_options.set_category_option("main", "binary_archive_url", os.path.join(current_command_options.get_category_option("main", "main_software_directory"), "binary_archives", "%(name)s.tgz"))


    def check_specific_configuration(self, current_command_options):
        ### Verifie les options specifique

        # Pour cocagne, que le nom du fichier d'environnement a construire soit correct
        if current_command_options.get_global_option('cocagne_env_file') != "":
            current_command_options.check_abs_path('cocagne_env_file', logger=self.logger)
        else:
            self.logger.error("cocagne_env_file can not be empty !")

        # TODO Test top_directory

        #Test de l'acces a home cocagne (/data/projets/projets.002/andromede.005/cocagne/prerequisites/)
        #cocagne_public_dir = current_command_options.get_global_option("cocagne_public_dir")
        #if not os.path.isdir(cocagne_public_dir):
        #    self.logger.error("can not access to cocagne repository: file:%s" % cocagne_public_dir)

    def get_env_files(self):
        """
        This method returns the list of env files of the project
        """
        env_file = self.current_command_options.get_global_option("cocagne_env_file")
        return [env_file, ]

    def create_env_files(self, delete=True, installer=False):
        #Creation du fichier d'environnement en ajoutant les prerequis associés
        env_file = self.current_command_options.get_global_option("cocagne_env_file")

        env_file_directory = os.path.dirname(env_file)
        if os.path.exists(env_file):
            try:
                if delete:
                    os.remove(env_file)
                else:
                    return
            except:
                self.logger.exception("Error in deleting COCAGNE env file: %s" % env_file)

        command = "mkdir -p " + env_file_directory + " ; "
        ier = os.system(command)
        if ier != 0:
            self.logger.error("Creation of COCAGNE env file failed ! command was: %s" % command)

        pre_file = open(env_file, 'a')

        #if not installer:
            #Rien a ajouter : cocagne est un prerequis : son impact sur le fichier d environnement
            # sera defini dans la classe liee au prerequis cocagne
            #pre_file.write("#!/bin/bash\n")
            #pre_file.write("\n")
            #pre_file.write("### Env file generated by YAMM for COCAGNE ###\n")
            #pre_file.write("\n")
        #    pre_file.write(strings.installer_cocagne_prerequisite_header)
        #else:
        if True:
          pre_file.write(strings.installer_cocagne_prerequisite_header)
          has_prereq = False
          has_tools = False
          has_modules = False
          has_main = False
          for software in self.current_command_softwares:
            if software.get_type() == "prerequisite":
              has_prereq = True
            if software.get_type() == "tool":
              has_tools = True
            if software.get_type() == "module":
              has_modules = True
            #if software.get_type() == "main":
            #  has_main = True
            if has_prereq and has_tools and has_main:
              break
          if has_prereq:
            pre_file.write(strings.installer_cocagne_prerequisite_header_check_prereq)
          if has_tools:
            pre_file.write(strings.installer_cocagne_prerequisite_header_check_tools)
          if has_modules:
            pre_file.write(strings.installer_cocagne_prerequisite_header_check_modules)
          #if has_main:
          pre_file.write(strings.installer_cocagne_prerequisite_header_check_main)

        pre_file.close()


    def add_software_to_env_files_end_hook(self, executor_software):
        # a la fin de l installation du sofware executor_software, execute le code suivant
        # ici : ajoute une ligne dans le env.bashrc
        software = self.current_map_name_to_softwares[executor_software.name]
        if not self.is_build_only_software(executor_software.name) or \
                not self.current_command_options.get_global_option("separate_dependencies"):
            # Add software in the env file
            with open(self.current_command_options.get_global_option("cocagne_env_file"), "a") as env_file:
                env_file.write(software.get_prerequisite_str())

    def add_software_to_env_installer_files_end_hook(self, executor_software):

        software = self.current_map_name_to_softwares[executor_software.name]
        # Add software in the env file
        with open(self.current_command_options.get_global_option("cocagne_env_file"), "a") as env_file:
            env_file.write(software.get_prerequisite_str())

    @yamm_command("Creation of COCAGNE movable installer")
    def create_movable_installer(self, installer_topdirectory="", distrib="", tgz=True, delete=True, runnable=True):
        """
        This method creates a movable archive of Cocagne. 
        It is possible to generate only the installer directory structure, or the archive tgz file.
        By default the previous directory and archive or installer files are deleted.
        It the distribution is etch, then some specific libraries are added to make the installer "universal".

        :param string installer_topdirectory: Defines the installer directory, including the installer name.
        :param string distrib: Defines the Debian distribution on which the installer is being created.
        :param bool tgz: If True, generates the archive file.
        :param bool delete: If True, deletes the existing installer directory, archive and installer files.
        """
        if runnable:
            tgz = True

        # Step 0: init variables
        f = os.popen("uname -m")
        archi = f.read().strip()
        f.close()
        # The name of the installer depends on the operating system
        installer_name = self.version + "-" + archi

        # Calculates the installer directory full path
        movable_installer_topdirectory = os.path.join(self.current_command_options.get_global_option("top_directory"), "movable_installer")
        if installer_topdirectory == "":
          installer_topdirectory = os.path.join(movable_installer_topdirectory, installer_name)
        else:
          installer_name = os.path.basename(installer_topdirectory)
        self.logger.info("Installer name is %s" % installer_name)

        # Deletes the previous installer files
        if delete:
          if os.path.exists(installer_topdirectory + ".tgz"):
            os.remove(installer_topdirectory + ".tgz")
          if os.path.exists(os.path.join(movable_installer_topdirectory, "decompress")):
            os.remove(os.path.join(movable_installer_topdirectory, "decompress"))
          if os.path.exists(installer_topdirectory):
            try:
              misc.fast_delete(installer_topdirectory)
            except:
              self.logger.exception("Error in deleting directory" + installer_topdirectory)

        # Step 1: Create env files
        # Defines the new environnement files and save the current ones
        # current_command_options sera utilisé ans cette méthode et correspond au fichier dans movable installer
        # current_command_options est initialisé dans le projet par copie de options
        save_cocagne_env_file = ""
        if self.current_command_options.get_global_option("cocagne_env_file"):
          save_cocagne_env_file = self.current_command_options.get_global_option("cocagne_env_file")
        self.current_command_options.set_global_option("cocagne_env_file", "%s/%s.bashrc" % (installer_topdirectory, self.version,))

        # Add option for specific env files
        #Le fichier d'environnement de l'archive deplacable est créé
        self.create_env_files(installer=True)

        # Step 3: create binary archives
        # Avec "_" = methode sans le decorateur "yamm_command"
        # Les preprequis installés sont tar dans "binary_archives"
        if self.current_command_options.get_global_option("separate_dependencies"):
            movable_installer_software_list = [soft for soft in self.current_command_softwares if not self.is_build_only_software(soft.name)]
        else:
            movable_installer_software_list = self.current_command_softwares
        self._make(software_list=[s.name for s in movable_installer_software_list],
                   executor_mode="create_binary_archive")

        # Step 4: install binary archives in installer directory
        software_mode = "install_from_binary_archive"
        self.reset_command(endlog=False)
        # Attention : self.reset_command remet à None le current_command_options
        self.current_command_options.set_global_option("cocagne_env_file", "%s/%s.bashrc" % (installer_topdirectory, self.version,))

        # Les preprequis sont mis dans movable_installer
        for software in movable_installer_software_list:

          # Artefact softwares are used by yamm only for compilation purposes.
          # They are not real softwares
          if software.isArtefact():
            continue

          # Sets the install directory for the current software
          software_install_name = software.get_executor_software_name()
#           software_install_name = software.name.capitalize()
#           if software.version:
#             software_install_name += "_" + misc.transform(software.version)
          if software.get_type() == "main":
            installdir = installer_topdirectory
          elif software.get_type() == "prerequisite":
            installdir = os.path.join(installer_topdirectory, "prerequisites", software_install_name)
          elif software.get_type() == "tool":
            installdir = os.path.join(installer_topdirectory, "tools", software_install_name)
          elif software.get_type() == "module":
            installdir = os.path.join(installer_topdirectory, "modules", software_install_name)
          self.current_command_options.set_software_option(software.name, "software_install_directory", installdir)

          software.set_command_env(movable_installer_software_list,
                                   self.current_command_options,
                                   software_mode,
                                   self.current_command_version_object,
                                   self.current_command_python_version)
          software.create_tasks()

          # We don't clean the install directory for Cocagne since it is the base directory itself
          if software.get_type() == "main":
            software.install_binary_archive_task.delete_install_dir = False

          executor_software = software.get_executor_software()
          self.current_command_executor.add_software(executor_software)
          self.current_map_name_to_softwares[software.name] = software

        # The env files are filled
        self.current_command_executor.add_hooks(None, self.add_software_to_env_installer_files_end_hook)

        # The commands in unmake_movable_archive_commands must not be run: disable them
        for soft in self.current_command_executor.executor_softwares:
          if soft.install_binary_archive_task:
            soft.install_binary_archive_task.setRunPostInstallCommands(False)

        self.current_command_executor.execute()

        #Remplace les chemin en dur par les variables (-i, edite directement le fichier)
        prerequisites_file = self.current_command_options.get_global_option("cocagne_env_file")
        cmd = "sed -i -e s#%s#\${PREREQUISITES_ROOT_DIR}#g %s" % (os.path.join(installer_topdirectory, "prerequisites"), prerequisites_file)
        os.system(cmd)
        cmd = "sed -i -e s#%s#\${TOOLS_ROOT_DIR}#g %s" % (os.path.join(installer_topdirectory, "tools"), prerequisites_file)
        os.system(cmd)
        cmd = "sed -i -e s#%s#\${MODULES_ROOT_DIR}#g %s" % (os.path.join(installer_topdirectory, "modules"), prerequisites_file)
        os.system(cmd)
        cmd = "sed -i -e s#%s#\${MAIN_ROOT_DIR}#g %s" % (installer_topdirectory, prerequisites_file)
        os.system(cmd)
        #cmd="sed -i -e s#%s#\${MAIN_ROOT_DIR}#g %s" % (software_install_name, prerequisites_file)
        #os.system(cmd)

        # Step 6: create tgz
        if tgz:
          self.logger.info("Creating TGZ...")
          t_tgz = int(time.time())
          curdir = os.getcwd()

          installer_tgz_name = installer_name + ".tgz"
          installer_tgz_path = os.path.join(os.path.dirname(installer_topdirectory), installer_tgz_name)

          os.chdir(os.path.dirname(installer_topdirectory))
          try:
            tar = tarfile.open(installer_tgz_path, "w:gz")
            tar.errorlevel = 1
            tar.add(os.path.basename(installer_topdirectory))
            tar.close()
          except Exception:
            self.logger.exception("Cannot create archive %s." % installer_tgz_path)
          t_tgz = int(time.time()) - t_tgz
          self.logger.info(" ".join(["TGZ is done in", str(datetime.timedelta(seconds=t_tgz))]))
          if delete:
            misc.fast_delete(installer_topdirectory)

          os.chdir(curdir)




    # Step 7: create run file
    #Creation d'un auto-extracteur corrigeant les dépendances (chemin en dur dans info.py de cocagne)
    # Recupéré depuis Salome et simplifié
        if runnable:
            self.logger.info("Creating .run...")
            installer_run_name = os.path.join(os.path.dirname(installer_topdirectory), installer_name + ".run")
            

            usage = cocagne_installer_decompress_usage.substitute( cocagne_tag=self.version )
            ## fancy installer bar file
            # See http://www.theiling.de/projects/bar.html
            bar_cat = ""
            with open(installer_bar_cat) as installer_bar_cat_file:
                bar_cat = "".join(installer_bar_cat_file.readlines())

            ## Calculate the CRC
            crcsum = "0000000000"
            f = os.popen("cat \"%s\" | CMD_ENV=xpg4 cksum |sed -e 's/ /Z/' -e 's/ /Z/' |cut -dZ -f1" % installer_tgz_path)
            crcsum = f.read().strip()
            f.close()

            ## Calculate the MD5SUM
            cmd = """# Try to locate a MD5 binary
            OLD_PATH=$PATH
            PATH=${GUESS_MD5_PATH:-"$OLD_PATH:/bin:/usr/bin:/sbin:/usr/local/ssl/bin:/usr/local/bin:/opt/openssl/bin"}
            MD5_ARG=""
            MD5_PATH=`exec <&- 2>&-; which md5sum || type md5sum`
            test -x $MD5_PATH || MD5_PATH=`exec <&- 2>&-; which md5 || type md5`
            test -x $MD5_PATH || MD5_PATH=`exec <&- 2>&-; which digest || type digest`
            PATH=$OLD_PATH
            if test `basename $MD5_PATH` = digest; then
                    MD5_ARG="-a md5"
            fi
            if test -x "$MD5_PATH"; then
                    md5sum=`cat "%s" | eval "$MD5_PATH $MD5_ARG" | cut -b-32`;
                    echo "$md5sum"
            else
                    echo "none"
            fi
            """ % installer_tgz_path
            md5sum = "00000000000000000000000000000000"
            f = os.popen(cmd)
            md5sum = f.read().strip()
            f.close()

            ## create installer decompress file
            final_custom_command = ""
            if (bar_cat != "" and crcsum != "0000000000" and md5sum != "00000000000000000000000000000000"):
              installer_decompress_file_name = os.path.join(os.path.dirname(installer_topdirectory), "decompress")
              with open(installer_decompress_file_name, 'w') as installer_decompress_file:
                installer_decompress_file.write(cocagne_installer_decompress_template.substitute(cocagne_tag=self.version,
                                                                                     bar_cat=bar_cat,
                                                                                     archive_name=installer_name,
                                                                                     usage=usage,
                                                                                     title_deco="=" * len(self.version),
                                                                                     crcsum=crcsum,
                                                                                     md5sum=md5sum,
                                                                                     final_custom_command=final_custom_command))
              os.chmod(installer_decompress_file_name , stat.S_IRWXU)

              self.logger.info("Creating run file...")
              try:
                retcode = subprocess.call("cat %s %s > %s" % (installer_decompress_file_name, installer_tgz_path, installer_run_name), shell=True)
                if retcode < 0:
                  self.logger.error("installer cannot be created")
              except OSError as e:
                self.logger.exception("installer cannot be created: %s" %e)
              os.chmod("%s" % (installer_run_name) , stat.S_IRWXU)


        # Step 7: restore user env
        if save_cocagne_env_file != "":
          self.current_command_options.set_global_option("cocagne_env_file", save_cocagne_env_file)
        else:
          self.current_command_options.remove_global_option_value("save_cocagne_env_file")



if __name__ == "__main__":
  pro = Project(1)
  pro.options.set_global_option("top_directory", "/local00/tmp/essai_cocagne")
  pro.options.set_global_option("parallel_make", "8")
  pro.options.set_software_option("COCAGNE", "source_type", "remote")
  pro.set_version("Cocagne201")
  pro.nothing()

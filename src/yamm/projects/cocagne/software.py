#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Yann PORA (EDF R&D)

from yamm.core.framework.software import FrameworkSoftware

class CocagneSoftware(FrameworkSoftware):
  """
  Cette classe définit l'interface générique d'un logiciel cocagne
  """

  def get_type(self):
    """
      Cette méthode renvoie le type de logiciel.
    """
    return "type not defined for software %s" % self.name

  def get_build_str(self, specific_install_dir=""):
    """ Return a string which will be added in the build environment file.
    """
    return self.get_prerequisite_str(specific_install_dir)

  def get_prerequisite_str(self, specific_install_dir=""):
    """
    Cette méthode renvoie une chaine qui sera ajouté au fichier
    d'environnement des prerequis
    """
    return ""

  def get_module_str(self, specific_install_dir="", universal=False):
    """
    Cette méthode renvoie une chaine qui sera ajouté au fichier
    d'environnement des modules
    """
    return ""

  def isArtefact(self):
    return False

  def default_values_hook(self):
    """
    Cette méthode permet la compatibilité avec Salome (pour que Cocagne soit compilé
    correctement dans Salome-Coeur en sourçant les bons fichiers d'environnement).
    """
    self.env_files.append(self.project_options.get_global_option("build_env_file"))
    self.env_files.append(self.project_options.get_global_option("salome_prerequisites_env_file"))
    self.env_files.append(self.project_options.get_global_option("salome_modules_env_file"))


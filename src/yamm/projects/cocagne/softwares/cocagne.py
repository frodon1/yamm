# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA


# Classe de description du mode d'installation de Cocagne (configuration, compilation,...)
# Appelée directement pour Cocagne "stand alone".
# Une sous-classe de celle-ci est définie dans salome/softwares/tools pour Salome-coeur

from yamm.projects.cocagne.software import CocagneSoftware
from yamm.core.base import misc
from yamm.core.framework import test_suite
import os
import sys

software_name = "COCAGNE"
cocagne_template = """
#---------- Cocagne -------------------
export COCAGNE_ROOT_DIR="%install_dir"
export %ld_library_path=${COCAGNE_ROOT_DIR}/lib:${%ld_library_path}
export PYTHONPATH=$COCAGNE_ROOT_DIR/lib/python%python_version/site-packages/cocagne:${PYTHONPATH}
export PATH=$COCAGNE_ROOT_DIR/bin:${PATH}
"""
cocagne_template = misc.PercentTemplate(cocagne_template)

class COCAGNE(CocagneSoftware):

  def get_debian_depends(self):
    # Return a list of dependencies for the software to run
    # The list is for dependencies not listed in the yamm dependencies.
    return ['texlive-full', 'gcc', 'libatlas3-dev', 'pylint', 'inkscape']

  def set_archive_params(self):
    if not self.project_options.get_option(self.name, "cocagne_archive_filename"):
        self.archive_file_name = "cocagne-" + self.get_version_for_archive() + ".tar.gz"
        self.archive_type = "tar.gz"
    else:
        self.archive_file_name = self.project_options.get_option(self.name, "cocagne_archive_filename")
        self.archive_type = "tar.gz"

  def init_variables(self):

    self.set_archive_params()

    self.compil_type = "cmake"
    self.user_dependency_command = "unset SWIG_LIB; "
    # Release is already the default build type
    # self.config_options += " -DCMAKE_BUILD_TYPE=Release "

    self.config_options += " -DCOCAGNE_MD5_DATA_DIR=/data/projets/projets.002/andromede.005/cocagne/prerequisites/md5 "
    if misc.get_calibre_version() == '7':
        self.config_options += " -DC3THER_ROOT=/data/projets/projets.002/andromede.005/cocagne/prerequisites/calibre7/modules_th1d/TH1D-1.0rc4-2015-11-10_gcc4.4.5/src/c3ther "
        self.config_options += " -DTH1D_ROOT=/data/projets/projets.002/andromede.005/cocagne/prerequisites/calibre7/modules_th1d/TH1D-1.0rc4-2015-11-10_gcc4.4.5/src/th1d "

    self.config_options += " -DNBPROC=%s " % self.project_options.get_option(self.name, 'parallel_make')
    self.config_options += " -DPRECISION=double"
    self.config_options += " -DCMAKE_C_COMPILER=gcc "
    self.config_options += " -DCMAKE_CXX_COMPILER=g++ "
    self.config_options += " -DCMAKE_Fortran_COMPILER=gfortran "

    modules = []
    for dep in ('MEDFICHIER', 'MODAN', 'MEDCOUPLING'):
        if dep not in self.project_softwares_dict and dep in self.version_object.system_softwares_dict:
            version = self.version_object.system_softwares_dict[dep][0]
            module = self.version_object.get_software_class(dep).get_module_load_name(version)
            modules.append(module)
            if dep == 'MEDFICHIER':
                self.config_options += ' -DMED_ROOT=$(realpath $MED_LIB_DIR/..)'
            if dep == 'MODAN':
                self.config_options += ' -DMODAN_ROOT=$(realpath $MODAN_LIB_DIR/..)'
            
    if modules:
        self.project_options.set_software_option(self.name, 'module_load', ' '.join(modules))

    self.post_install_commands.append('cd $CURRENT_SOFTWARE_BUILD_DIR ;  make install-doc')

    # Unfortunately, this file is read-only. We make it writable first.
    file_to_replace = "./lib/python{0}/site-packages/cocagne/infos.py".format(self.python_version)
    self.make_movable_archive_commands.insert(0, "chmod u+w {0}".format(file_to_replace))

    # Compile Python files and remove original ones (deactivated for now because it causes problems on Calibre 7)
    bytecode_suffix = '.pyc'
#     if self.python_version.startswith('3'):
#         bytecode_suffix = '.{cache_tag}.opt-{optimization}.pyc'.format(
#             cache_tag=sys.implementation.cache_tag,  # @UndefinedVariable
#             optimization=str(sys.flags.optimize))
    if misc.get_calibre_version() == '9':
        pyscript = "import os;"
        pyscript += "import py_compile;"
        pyscript += "name_wo_ext = os.path.splitext('{}')[0];"
        pyscript += "bytecode_name = name_wo_ext + '%s';" % bytecode_suffix
        pyscript += "py_compile.compile('{}', cfile=bytecode_name);"
        pyscript += "os.remove('{}');"
        pyscript_for_bin = pyscript
        pyscript_for_bin += "os.chmod(bytecode_name, 0o777);"
        pyscript_for_bin += "os.rename(bytecode_name, '{}');"
        pyscript_for_lib = pyscript
#         pyscript_for_lib += "os.rename(name_wo_ext + '.pyo', name_wo_ext + '.pyc');"
        self.post_install_commands = []
        self.post_install_commands.append("cd bin ; find -name \"*.py\" -exec python -OO -c \"%s\" \;" % pyscript_for_bin)
        pythonlibdir = "lib/python%s/site-packages/cocagne" % self.python_version
        self.post_install_commands.append("cp {0} {0}.bak".format(file_to_replace))
        self.post_install_commands.append("cd %s ; find -name \"*.py\" -exec python -OO -c \"%s\" \;" % (pythonlibdir, pyscript_for_lib))
        file_to_replace_opt = os.path.splitext(file_to_replace)[0] + bytecode_suffix
        self.post_install_commands.append("mv {0}.bak {0}; rm {1}".format(file_to_replace, file_to_replace_opt))

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("cocagne")

    parallel_flag = self.project_options.get_option(self.name, "parallel_make")
    make_check = test_suite.MakeCheckTestSuite(name="make check COCAGNE",
                                               soft_name=self.name,
                                               soft_version=self.version,
                                               parallel_flag=parallel_flag)
    make_installcheck = test_suite.MakeInstallcheckTestSuite(name="make install check COCAGNE",
                                                             soft_name=self.name,
                                                             soft_version=self.version,
                                                             parallel_flag=parallel_flag)
    make_validation = test_suite.MakeInstallcheckTestSuite(name="make validation COCAGNE",
                                                           soft_name=self.name,
                                                           soft_version=self.version,
                                                           parallel_flag=parallel_flag)
    make_validation.command = 'make validation'

    self.test_suite_list = [make_check, make_installcheck, make_validation]

  def user_variables_hook(self):
    self.cmake_src_directory = os.path.join(self.src_directory, 'sources')

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "SWIG_FOR_COCAGNE",
                                              "DOCUTILS", "GRAPHVIZ", "DOXYGEN"]
    self.software_dependency_dict['exec'] = ["PYTHON", "HDF5", "H5PY", "MEDFICHIER",
                                             "TBB", "XDATA", "NUMPY", "PYQT",
                                             "EIGEN", "GNUPLOTPY", "MEDCOUPLING",
                                             "MODAN", "TH1D"]
    # Cocagne also depends on BLAS but we assume it is installed in the system. Indeed, the BLAS
    # in Salome is the one included by default with LAPACK and this one does not have the C interface
    # that is needed by Cocagne. Also, system implementations are generally more optimized than
    # default ones.

  def get_dependency_object_for(self, dependency_name):
    dependency_object = CocagneSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "install_path"]
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    if dependency_name == "SWIG_FOR_COCAGNE":
      dependency_object.depend_of = [ "install_path"]
    if dependency_name == "HDF5":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "HDF5_ROOT"
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "MED_ROOT"
    if dependency_name == "TBB":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "TBB_ROOT"
    if dependency_name == "BOOST":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "BOOST_ROOT"
    if dependency_name == "XDATA":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "XDATA_ROOT"
    if dependency_name == "NUMPY":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "NUMPY_ROOT"
    if dependency_name == "DOCUTILS":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "DOCUTILS_ROOT"
    if dependency_name == "EIGEN":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "EIGEN_ROOT"
    if dependency_name == "GNUPLOTPY":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "GNUPLOTPY_ROOT"
    if dependency_name == "MODAN":
      dependency_object.depend_of = [ "install_path"]
      dependency_object.specific_install_var_name = "MODAN_ROOT"
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    # executor_software_name: Permet de définir un nom de répertoire destination incluant les noms et versions de prerequis
    # config_options : option de cmake (phase de configuration)
    if dependency_name == "PYTHON":
      self.config_options += " -DCMAKE_INCLUDE_PATH=${PYTHON_INSTALL_DIR}/include "
      self.config_options += " -DCMAKE_LIBRARY_PATH=${PYTHON_INSTALL_DIR}/lib "
      self.config_options += " -DPYTHON_EXECUTABLE=${PYTHON_INSTALL_DIR}/bin/python "
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "SWIG_FOR_COCAGNE":
      self.config_options += " -DSWIG_EXECUTABLE=${SWIG_FOR_COCAGNE_INSTALL_DIR}/bin/swig "
      self.executor_software_name += "-sw" + misc.transform(version_name)
    elif dependency_name == "HDF5":
      self.executor_software_name += "-hdf5" + misc.transform(version_name)
    elif dependency_name == "MEDFICHIER":
      self.executor_software_name += "-med" + misc.transform(version_name)
    elif dependency_name == "TBB":
      self.executor_software_name += "-tbb" + misc.transform(version_name)
      self.config_options += " -DUSE_TBB=ON "
      self.config_options += " -DUSE_OPENMP=OFF "
    elif dependency_name == "BOOST":
      self.executor_software_name += "-bst" + misc.transform(version_name)
    elif dependency_name == "XDATA":
      self.executor_software_name += "-xdt" + misc.transform(version_name)
    elif dependency_name == "NUMPY":
      self.executor_software_name += "-num" + misc.transform(version_name)
    elif dependency_name == "DOCUTILS":
      self.executor_software_name += "-doc" + misc.transform(version_name)
    elif dependency_name == "EIGEN":
      self.executor_software_name += "-eig" + misc.transform(version_name)
    elif dependency_name == "MODAN":
      self.executor_software_name += "-mod" + misc.transform(version_name)

  def get_type(self):
    # Logiciel principal : il sera installé dans le repertoire père de prerequis et tools
    # Pour une utilisation "stand alone", il est complétement découplé de Salomé
    # Il n'est donc pas à placer parmis les prérequis/tools/modules
    return "main"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cocagne_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

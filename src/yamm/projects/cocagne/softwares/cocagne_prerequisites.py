#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Yann PORA (EDF R&D)

from yamm.projects.cocagne.software import CocagneSoftware

software_name = "COCAGNE_PREREQUISITES"

class COCAGNE_PREREQUISITES(CocagneSoftware):

  def isArtefact(self):
    return False
    return True

  def __init__(self, name, version, verbose, **kwargs):
    CocagneSoftware.__init__(self, name, version, verbose, **kwargs)
    self.include_path = ""
    self.software_source_type = "software"

  def init_variables(self):

    self.compil_type = "fake"

  def init_dependency_list(self):
    self.software_dependency_list = ["HDF5", "BOOST", "DOCUTILS", "TBB", "MED",
                                     "MEDFICHIER", "XDATA", "PMW", "H5PY",
                                     "SWIG"]  # , "PYTHON"] #numpy; pymw

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "MED":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory
    if dependency_name == "MEDFICHIER":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory
    elif dependency_name == "HDF5":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory
    elif dependency_name == "BOOST":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory
    elif dependency_name == "DOCUTILS":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory
    elif dependency_name == "TBB":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory
    elif dependency_name == "XDATA":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory
    elif dependency_name == "SWIG":
        software = self.project_softwares_dict[dependency_name]
        self.include_path += "export CPATH=%s/include:${CPATH}\n" % software.install_directory

  def get_prerequisite_str(self):
        msg = "\n#---- Cocagne prerequisites ------ \n"
        # msg += "export DKLD_LIBRARY_PATH=${LD_LIBRARY_PATH}\n"
        msg += "export LPATH=${LD_LIBRARY_PATH}\n"
        msg += self.include_path
        if "PYTHON" not in self.get_dependencies():
            msg += "export CPATH=/usr/include/python%s:${CPATH}\n" % self.python_version
        msg += "\n"
        msg += "export DKPATH=${PATH}\n"
        msg += "export DKLD_LIBRARY_PATH=${LD_LIBRARY_PATH}\n"
        msg += "export DKLPATH=${LPATH}\n"
        msg += "export DKCPATH=${CPATH}\n"
        msg += "export DKPYTHONPATH=${PYTHONPATH}\n"
        msg += "\n"

        msg += "# Fin de prise en compte des prerequis Cocagne"
        return msg

  def get_type(self):
    # return "module"
    return "prerequisite"

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Yann PORA (EDF R&D)

import os

from yamm.projects.cocagne.software import CocagneSoftware
from yamm.core.base import misc

software_name = "MODAN"
modan_template = """
#---------- Modan -------------------
MODAN_ROOT_DIR="{install_dir}"
export {ld_library_path}=${{MODAN_ROOT_DIR}}/lib:${{{ld_library_path}}}
export PYTHONPATH=${{MODAN_ROOT_DIR}}/lib/python{python_version}/site-packages:${{PYTHONPATH}}
export PATH=${{MODAN_ROOT_DIR}}/bin:${{PATH}}
"""
modan_configuration_template = """
#---------- Modan -------------------
MODAN_ROOT_DIR="{install_dir}"
ADD_TO_{ld_library_path}: %(MODAN_ROOT_DIR)s/lib
ADD_TO_PYTHONPATH=: $(MODAN_ROOT_DIR)s/lib/python{python_version}/site-packages
ADD_TO_PATH: %(MODAN_ROOT_DIR)s/bin
"""


class MODAN(CocagneSoftware):

    @classmethod
    def get_module_load_name(cls, version):
        split_version = [str(s) for s in misc.split_version(version)]
        return 'modan/' + '.'.join(split_version[:2])

    def get_debian_dev_package(self):
        return misc.get_calibre_package_name('modan', self.ordered_version)

    def init_variables(self):
        self.compil_type = "cmake"
        self.remote_type = "git"
        self.tag = self.version
        self.configure_remote_software_pleiade_git("modan")

    def user_variables_hook(self):
        self.cmake_src_directory = os.path.join(self.src_directory, 'sources')

    def init_dependency_list(self):
        self.software_dependency_dict['build'] = ["CMAKE",]
        self.software_dependency_dict['exec'] = ["PYTHON", "HDF5", "MEDFICHIER", "MEDCOUPLING"]

    def get_dependency_object_for(self, dependency_name):
        dependency_object = CocagneSoftware.get_dependency_object_for(self, dependency_name)
        if dependency_name == "PYTHON":
            dependency_object.depend_of = ["path", "install_path"]
        if dependency_name == "CMAKE":
            dependency_object.depend_of = ["path"]
        if dependency_name == "HDF5":
            dependency_object.depend_of = ["install_path"]
            dependency_object.specific_install_var_name = "HDF5HOME"
        if dependency_name == "MEDFICHIER":
            dependency_object.depend_of = ["install_path"]
            dependency_object.specific_install_var_name = "MED_ROOT"
        if dependency_name == "MEDCOUPLING":
            dependency_object.depend_of = ["install_path"]
            dependency_object.specific_install_var_name = "MEDCOUPLING_ROOT"
        return dependency_object

    def update_configuration_with_dependency(self, dependency_name, version_name):
        # executor_software_name: Permet de définir un nom de répertoire destination incluant les noms et versions de prerequis
        # config_options : option de cmake (phase de configuration)
        if dependency_name == "PYTHON":
            self.executor_software_name += "-py" + misc.transform(version_name)
        elif dependency_name == "HDF5":
            self.executor_software_name += "-hdf5" + misc.transform(version_name)
            self.config_options += ' -DHDF5HOME:FILEPATH=$HDF5HOME'
        elif dependency_name == "MEDFICHIER":
            self.executor_software_name += "-med" + misc.transform(version_name)
            self.config_options += ' -DMED_ROOT:FILEPATH=$MED_ROOT'
        elif dependency_name == "MEDCOUPLING":
            self.executor_software_name += "-mc" + misc.transform(version_name)
            self.config_options += ' -DMEDCOUPLING_ROOT:FILEPATH=$MEDCOUPLING_ROOT'

    def get_type(self):
        return "tool"

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return modan_template.format(install_dir=install_dir,
                                     ld_library_path=misc.get_ld_library_path(),
                                     python_version=self.python_version)

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return modan_configuration_template.format(install_dir=install_dir,
                                     ld_library_path=misc.get_ld_library_path(),
                                     python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Yann PORA (EDF R&D)

from yamm.projects.cocagne.software import CocagneSoftware
from yamm.core.base import misc
import os
import string

software_name = "TH1D"
th1d_template = """
#------- th1d ------
export THETIS_ROOT="%install_dir/thetis-%thetis_version"
export C3THER_ROOT="%install_dir/c3ther-%c3ther_version"
export TH1D_ROOT="%install_dir/th1d"
export %ld_library_path=${THETIS_ROOT}/lib:${%ld_library_path}
export %ld_library_path=${C3THER_ROOT}/lib:${%ld_library_path}
export %ld_library_path=${TH1D_ROOT}/lib:${%ld_library_path}
"""
th1d_template = misc.PercentTemplate(th1d_template)

th1d_configuration_template = """
#------- th1d ------
THETIS_ROOT="$install_dir/thetis-$thetis_version"
C3THER_ROOT="$install_dir/c3ther-$c3ther_version"
TH1D_ROOT="$install_dir/th1d"
ADD_TO_$ld_library_path: %(THETIS_ROOT)s/lib
ADD_TO_$ld_library_path: %(C3THER_ROOT)s/lib
ADD_TO_$ld_library_path: %(TH1D_ROOT)s/lib
"""
th1d_configuration_template = string.Template(th1d_configuration_template)


class TH1D(CocagneSoftware):

    def __init__(self, name, version, verbose, **kwargs):
        CocagneSoftware.__init__(self, name, version, verbose, **kwargs)
        # dictionnaire stockant les versions des differents composants de TH1D
        # [THETIS, C3THER]
        self.versions_to_codes = {}
        self.versions_to_codes["1.1-rc3"] = ["2.1", "1.4.2"]
        self.versions_to_codes["1.1-rc3-EDFp1"] = ["2.1", "1.4.2"]
        self.software_source_type = 'archive'

    def init_variables(self):
        self.archive_file_name = "TH1D-" + self.version + ".tar.gz"
        self.archive_type = "tar.gz"

        self.archive_address = os.path.join(misc.get_ftp_server(), 'F3C', 'EEM')  # , self.archive_file_name)

        self.compil_type = "specific"
        self.shell = "/bin/bash"  # for module load
        thetis_version, c3ther_version = self.versions_to_codes[self.version]
        parallel_make = self.project_options.get_option(self.name, "parallel_make")
        if parallel_make:
            parallel_make = ' -j%s' % parallel_make
        self.specific_install_command = """
            cd $CURRENT_SOFTWARE_BUILD_DIR &&
            mkdir build_thetis && cd build_thetis &&
            cmake $CURRENT_SOFTWARE_SRC_DIR/pre_requis/thetis -DCMAKE_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR/thetis-{0} \
            -DPRECISION=float -DUSE_OPENMP=ON && make{1} && make install &&

            cp -rvf $CURRENT_SOFTWARE_SRC_DIR/pre_requis_precompile/c3ther $CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2} &&
            chmod 644 $CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2}/include/* &&
            chmod 755 $CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2}/lib/* &&
            ln -sf libC3THER_v{2}_linux.gfortran49x.r4.64bits.so $CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2}/lib/libc3ther.so &&
            echo "#define C3THER_VERSION_MAJOR $(echo {2} | cut -d. -f1)" >> $CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2}/include/c3ther.h &&
            echo "#define C3THER_VERSION_MINOR $(echo {2} | cut -d. -f2)" >> $CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2}/include/c3ther.h &&
            echo "#define C3THER_VERSION_PATCH $(echo {2} | cut -d. -f3)" >> $CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2}/include/c3ther.h &&

            cd $CURRENT_SOFTWARE_BUILD_DIR &&
            mkdir build_th1d && cd build_th1d &&
            cmake $CURRENT_SOFTWARE_SRC_DIR/src/th1d -DCMAKE_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR/th1d \
            -DPRECISION=float -DGEN_PYTHON_INTERFACE=OFF \
            -DTHETIS_ROOT=$CURRENT_SOFTWARE_INSTALL_DIR/thetis-{0} \
            -DC3THER_ROOT=$CURRENT_SOFTWARE_INSTALL_DIR/c3ther-{2} && make{1} && make install
            """\
            .format(thetis_version, parallel_make, c3ther_version)

    def init_dependency_list(self):
        self.software_dependency_list = ["HDF5", "MEDFICHIER", "MEDCOUPLING", "PYTHON"]

    def get_type(self):
        return "tool"

    def get_name_for_installer(self):
        """ Returns the name of the software as given in the environment files.
        """
        return self.name

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        thetis_version, c3ther_version = self.versions_to_codes[self.version]
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return th1d_template.substitute(install_dir=install_dir,
                                        ld_library_path=misc.get_ld_library_path(),
                                        thetis_version=thetis_version,
                                        c3ther_version=c3ther_version)

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        thetis_version, c3ther_version = self.versions_to_codes[self.version]
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return th1d_configuration_template.substitute(install_dir=install_dir,
                                                      ld_library_path=misc.get_ld_library_path(),
                                                      thetis_version=thetis_version,
                                                      c3ther_version=c3ther_version)

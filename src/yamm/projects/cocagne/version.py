#!/usr/bin/env python
# -*- coding: utf-8 -*-
from yamm.core.framework.version import FrameworkVersion

class CocagneVersion(FrameworkVersion):
  """
    .. py:currentmodule:: yamm.projects.cocagne.CocagneVersion

    Base class for COCAGNE's versions. It adds new
    methods for specific capabilities of SALOME project.

    :param string name: name of the version.
    :param int verbose: verbose level of the version.

  """

  def __init__(self, name="NOT CONFIGURED", verbose=0, flavour = ""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

#######
#
# Méthodes qui doivent être surchargées
#
#######

  def get_cocagne_tag(self):
    """
      This method is mandatory. It should returns the COCAGNE version targeted by
      the version.

      This value is used to get modules sources files and to some files names.
    """
    return None

  def is_universal(self):
    """
      Have to returns True if this version is a correct COCAGNE universal version.

      :return bool: True if universal.
    """
    return False

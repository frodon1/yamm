#!/usr/bin/env python
# -*- coding: utf-8 *-
from yamm.projects.cocagne.version import CocagneVersion

version_name = "Cocagne120_Salome64_calibre7"
class Cocagne120_Salome64_calibre7(CocagneVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    CocagneVersion.__init__(self, name, verbose, flavour)

  def get_salome_tag(self):
    return version_name

  def configure_softwares(self):
    self.add_software("COCAGNE", "1.2.0")
    self.add_software("COCAGNE_PREREQUISITES", "1.2.0") # prerequis fantome pour terminer l'installation des prerequis au format cocagne
    self.add_software("HDF5", "1.6.9")
    self.add_software("BOOST", "1.46.1")
    self.add_software("DOCUTILS", "0.7")
    self.add_software("TBB", "3.0")
    self.add_software("MEDFICHIER", "2.3.6")
    self.add_software("XDATA", "0.9.2")
    self.add_software("PMW", "1.3.2")


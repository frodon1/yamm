# -*- coding: utf-8 -*-
from yamm.projects.cocagne.version import CocagneVersion

version_name = "Cocagne201"
class Cocagne201(CocagneVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    CocagneVersion.__init__(self, name, verbose, flavour)
    self.accepted_flavours=["gcc47"]

  def get_salome_tag(self):
    return version_name

  def configure_softwares(self):
    self.add_software("COCAGNE",    "git_tag/int_COCAGNE_02_00_01_4")
    self.add_software("HDF5",       "1.8.10")
    self.add_software("BOOST",      "1.52.0")
    self.add_software("DOCUTILS",   "0.10")
    self.add_software("TBB",        "3.0")
    self.add_software("MEDFICHIER", "3.0.6")
    self.add_software("XDATA",      "0.9.8")
    self.add_software("PMW",        "1.3.3b")
    self.add_software("H5PY",       "2.1.1")

  def update_configuration_with_flavour(self):
    CocagneVersion.update_configuration_with_flavour(self)

    if self.flavour == "gcc47":
      self.add_software("SWIG_FOR_COCAGNE", "1.3.40p1", "1.3.40")    

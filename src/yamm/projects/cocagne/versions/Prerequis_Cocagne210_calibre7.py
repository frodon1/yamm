#!/usr/bin/env python
# -*- coding: utf-8 *-
from yamm.projects.cocagne.version import CocagneVersion

#ATTENTION : Dérive de Salome pour avoir acces aux outils du projet salome 
# (ex: archive deplacable)

version_name = "Prerequis_Cocagne210_calibre7"
class Prerequis_Cocagne210_calibre7(CocagneVersion):

  def __init__(self, name=version_name, verbose=0):
    CocagneVersion.__init__(self, version_name, verbose)

  def get_salome_tag(self):
    return version_name

  def configure_softwares(self):

    self.add_software("HDF5", "1.8.10")
    self.add_software("DOCUTILS", "0.7")
    self.add_software("TBB", "3.0")
    self.add_software("MEDFICHIER", "3.0.6")
    self.add_software("XDATA", "0.9.8")
    self.add_software("PMW", "1.3.3b")
    self.add_software("H5PY", "2.1.1")
    self.add_software("SWIG", "1.3.40p1")        
    self.add_software("COCAGNE_PREREQUISITES", "2.1.0") # prerequis fantome pour terminer l'installation des prerequis au format cocagne

#!/usr/bin/env python
# -*- coding: utf-8 *-
from yamm.projects.cocagne.version import CocagneVersion

#ATTENTION : Dérive de Salome pour avoir acces aux outils du projet salome 
# (ex: archive deplacable)

version_name = "Prerequis_Cocagne230_calibre7"
class Prerequis_Cocagne230_calibre7(CocagneVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    CocagneVersion.__init__(self, version_name, verbose, flavour)
    self.accepted_flavours=["gcc47"]
    CocagneVersion.update_configuration_with_flavour(self)

  def get_salome_tag(self):
    return version_name

  def configure_softwares(self):

    self.add_software("CMAKE", "2.8.11") #3.0.2-1 https://cmake.org/files/v3.0/cmake-3.0.2-1.tar.bz2
    self.add_software("HDF5", "1.8.13") # Calibre 9
    self.add_software("DOCUTILS", "0.12") # Calibre 9
    self.add_software("TBB", "3.0") #4.2~20140122-5 # Calibre 9 https://www.threadingbuildingblocks.org/sites/default/files/software_releases/source/tbb42_20140310oss_src.tgz
    self.add_software("MED", "git_tag/master")
    self.add_software("MEDFICHIER", "3.1.0")
    self.add_software("XDATA", "0.9.8")
    self.add_software("PMW", "1.3.3b")
    self.add_software("NUMPY", "1.9.2") # pour CYTHON pour H5PY
    self.add_software("CYTHON", "0.23.2") # Prerequis necessaire à H5PY 
    self.add_software("H5PY", "2.5.0") # 2.5
    #self.add_software("SWIG", "1.3.40p1") #2.0.12  http://downloads.sourceforge.net/swig/swig-2.0.12.tar.gz
    self.add_software("SWIG", "2.0.12")   #Calibre 9
    self.add_software("COCAGNE_PREREQUISITES", "2.3.0") # prerequis fantome pour terminer l'installation des prerequis au format cocagne

  def update_configuration_with_flavour(self):
    CocagneVersion.update_configuration_with_flavour(self)

    if self.flavour == "gcc47":
      self.add_software("SWIG_FOR_COCAGNE", "1.3.40p1", "1.3.40")    


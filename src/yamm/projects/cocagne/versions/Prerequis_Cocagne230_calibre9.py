#!/usr/bin/env python
# -*- coding: utf-8 *-
from yamm.projects.cocagne.version import CocagneVersion

#ATTENTION : Dérive de Salome pour avoir acces aux outils du projet salome 
# (ex: archive deplacable)

version_name = "Prerequis_Cocagne230_calibre9"
class Prerequis_Cocagne230_calibre9(CocagneVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    CocagneVersion.__init__(self, version_name, verbose, flavour)
    self.accepted_flavours=["gcc47"]
    CocagneVersion.update_configuration_with_flavour(self)

  def get_salome_tag(self):
    return version_name

  def configure_softwares(self):

    self.add_software("MED", "git_tag/master")
    self.add_software("MEDFICHIER", "3.1.0")
    self.add_software("XDATA", "0.9.8")
    self.add_software("PMW", "1.3.3b")
    self.add_software("H5PY", "2.5.0") # 2.5
    self.add_software("COCAGNE_PREREQUISITES", "2.3.0") # prerequis fantome pour terminer l'installation des prerequis au format cocagne

  def update_configuration_with_flavour(self):
    CocagneVersion.update_configuration_with_flavour(self)

    if self.flavour == "gcc47":
      self.add_software("SWIG_FOR_COCAGNE", "1.3.40p1", "1.3.40")    


#!/usr/bin/env python
# -*- coding: utf-8 *-
from yamm.projects.cocagne.version import CocagneVersion

#ATTENTION : Dérive de Salome pour avoir acces aux outils du projet salome 
# (ex: archive deplacable)

version_name = "Prerequis_H5PY"
class Prerequis_H5PY(CocagneVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    CocagneVersion.__init__(self, name, verbose, flavour)

  def get_salome_tag(self):
    return version_name

  def configure_softwares(self):
    self.add_software("HDF5", "1.8.4")
    self.add_software("H5PY", "2.0.1")


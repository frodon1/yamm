#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function

import os
import sys

yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)
from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject

from yamm.projects.compilers import softwares as default_softwares
from yamm.projects.compilers import versions  as default_versions

def get_ftp_server():
  return os.path.join(misc.get_ftp_server(), 'YAMM', 'Public', 'compilers')

class Project(FrameworkProject):

  def __init__(self, log_name="COMPILERS", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.extra_python_env_file_name = "user_extra_compilers_env_file"
    self.define_compilers_options()

#######
#
# Méthodes d'initialisation internes
#
#######

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="COMPILERS", verbose=self.verbose_level)
    # Import des softwares
    for software_name in default_softwares.softwares_list:
      self.add_software_in_catalog(software_name, "yamm.projects.compilers.softwares")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.compilers.versions")
    self.logger.debug("End init catalog")

  def define_compilers_options(self):
    self.options.add_option("compilers_env_file", "", ["global"])
    self.options.add_option("user_extra_compilers_env_file", "", ["global"])

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):

    current_command_options.check_abs_path('main_software_directory',
                                           logger=self.logger)

    for option in ['compilers_env_file',
                   'user_extra_compilers_env_file']:
      if current_command_options.get_global_option(option) != "":
        current_command_options.check_abs_path(option, logger=self.logger)

#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "compilers"))
    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)
    # Set main directory for softwares
    if not current_command_options.get_global_option("main_software_directory"):
      current_command_options.set_global_option("main_software_directory", os.path.join(current_command_options.get_global_option("version_directory"), "softwares"))

    if not current_command_options.get_global_option("compilers_env_file"):
      current_command_options.set_global_option("compilers_env_file", "")
    if not current_command_options.get_global_option("user_extra_compilers_env_file"):
      current_command_options.set_global_option("user_extra_compilers_env_file", "")

#######
#
# Print methods
#
#######

  def print_directories(self, current_command_options):
    FrameworkProject.print_directories(self, current_command_options)
    self.logger.info("Softwares directory      : %s" % current_command_options.get_global_option("main_software_directory"))

  def print_general_configuration(self, current_command_options):
    FrameworkProject.print_general_configuration(self, current_command_options)
    self.logger.info("Compiler Version       : %s" % self.version)

#######
#
# Méthodes internes pour les commandes
#
#######

  def update_configuration(self):
    # Platform
    misc.misc_platform = self.current_command_options.get_global_option("platform")
    if self.current_command_options.get_global_option("archive_remote_address") == "":
      self.current_command_options.set_global_option("archive_remote_address", 
                                    os.path.join(get_ftp_server(), 'sources'))
    if not self.current_command_options.get_global_option("compilers_env_file"):
      self.current_command_options.set_global_option("compilers_env_file", 
                                    os.path.join(self.current_command_options.get_global_option("version_directory"), "compiler_env_%s.sh" % self.version))

  def create_env_files(self, delete=True):
    env_file = self.current_command_options.get_global_option("compilers_env_file")
    env_file_directory = os.path.dirname(env_file)
    if os.path.exists(env_file):
      try:
        if delete:
          os.remove(env_file)
        else:
          return
      except:
        self.logger.exception("Error in deleting compilers env file: %s" % env_file)

    # Adding user_extra_compilers_env_file
    user_extra_compilers_env_file = self.current_command_options.get_global_option("user_extra_compilers_env_file")
    if user_extra_compilers_env_file != "" and user_extra_compilers_env_file != env_file:
      command  = "mkdir -p " + env_file_directory + " ; "
      command += "cp " + user_extra_compilers_env_file + " " + env_file
      # Execute command
      ier = os.system(command)
      if ier != 0:
        self.logger.error("Copy of user extra env file failed ! command was: %s" % command)
    else:
      command  = "mkdir -p " + env_file_directory + " ; "
      ier = os.system(command)
      if ier != 0:
        self.logger.error("Creation of env file failed ! command was: %s" % command)

    # Starting prerequis file
    with open(env_file, 'a') as pre_file:
      pre_file.write("export LANG=C\n")

  def add_software_to_env_files_end_hook(self, executor_software):
    software = self.current_map_name_to_softwares[executor_software.name]
    # Add software in the env file
    with open(self.current_command_options.get_global_option("compilers_env_file"), "a") as env_file:
      env_file.write(software.get_env_str())

#######
#
# Commands
#
#######

if __name__ == "__main__":

  print("Testing compilers project class")
  pro = Project(1)
  pro.set_version("LLVM_2_9")
  pro.print_configuration()
  pro.nothing()


#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.compilers.software import CompilersSoftware
from yamm.core.base import misc
import os

software_name = "AUTOCONF"

autoconf_template = """
#------ autoconf ------
export AUTOCONFHOME=%install_dir
export PATH=${AUTOCONFHOME}/bin:${PATH}
"""
autoconf_template = misc.PercentTemplate(autoconf_template)

class AUTOCONF(CompilersSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    CompilersSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.archive_file_name    = "autoconf-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type          = "autoconf"

  def init_dependency_list(self):
    self.software_dependency_list = ["M4"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = CompilersSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "M4":
      dependency_object.depend_of = ["path"]
    return dependency_object

  def get_env_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return autoconf_template.substitute(install_dir=install_dir)


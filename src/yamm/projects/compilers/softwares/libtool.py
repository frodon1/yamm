#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.projects.compilers.software import CompilersSoftware
from yamm.core.base import misc

software_name = "LIBTOOL"

libtool_template = """
#------ libtool ------
export LIBTOOLHOME=%install_dir
export PATH=${LIBTOOLHOME}/bin:${PATH}
export %ld_library_path=${LIBTOOLHOME}/lib:${%ld_library_path}
"""
libtool_template = misc.PercentTemplate(libtool_template)

class LIBTOOL(CompilersSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    CompilersSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name    = "libtool-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type          = "autoconf"

  def get_env_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libtool_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

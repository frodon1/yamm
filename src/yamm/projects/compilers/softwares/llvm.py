#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.compilers.software import CompilersSoftware
from yamm.core.base import misc
import os

software_name = "LLVM"

llvm_template = """
#------ llvm ------
export LLVMHOME=%install_dir
export PATH=${LLVMHOME}/bin:${PATH}
export %ld_library_path=${LLVMHOME}/lib:${%ld_library_path}
export CC=${LLVMHOME}/bin/clang
export CXX=${LLVMHOME}/bin/clang
export LD=${LLVMHOME}/bin/llvm-ld
export AR=${LLVMHOME}/bin/llvm-ar
export NM=${LLVMHOME}/bin/llvm-nm
export RANLIB=${LLVMHOME}/bin/llvm-ranlib
export CFLAGS=-O4
"""
llvm_template = misc.PercentTemplate(llvm_template)

class LLVM(CompilersSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    CompilersSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.archive_file_name    = "llvm-" + self.version + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "autoconf"

  def init_dependency_list(self):
    self.software_dependency_list = ["CLANG"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = CompilersSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CLANG":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "CLANG_INSTALL_PATH"
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CLANG":
      self.pre_configure_commands.append("ln -s ${CLANG_INSTALL_PATH} tools/clang")

  def get_env_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return llvm_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

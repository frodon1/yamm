#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.compilers.software import CompilersSoftware
from yamm.core.base import misc
import os

software_name = "M4"

m4_template = """
#------ m4 ------
export M4HOME=%install_dir
export PATH=${M4HOME}/bin:$PATH
"""
m4_template = misc.PercentTemplate(m4_template)

class M4(CompilersSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    CompilersSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name    = "m4-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "autoconf"

  def get_env_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return m4_template.substitute(install_dir=install_dir)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function
import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject

from yamm.projects.e17 import softwares as default_softwares
from yamm.projects.e17 import versions  as default_versions

class Project(FrameworkProject):

  def __init__(self, log_name="E17", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)

#######
#
# Méthodes d'initialisation internes
#
#######

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="PROJECT", verbose=self.verbose_level)
    # Import des softwares
    for software_name in default_softwares.softwares_list:
      self.add_software_in_catalog(software_name, "yamm.projects.e17.softwares")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.e17.versions")
    self.logger.debug("End init catalog")

#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "e17"))
    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)
    # Set main directory for softwares
    if not current_command_options.get_global_option("main_software_directory"):
      current_command_options.set_global_option("main_software_directory", current_command_options.get_global_option("version_directory"))

#######
#
# Print methods
#
#######

  def print_general_configuration(self, current_command_options):
    FrameworkProject.print_general_configuration(self, current_command_options)
    self.logger.info("E17 Version       : %s" % self.version)

#######
#
# Méthodes internes pour les commandes
#
#######

  def update_configuration(self):
    # Platform
    misc.misc_platform = self.current_command_options.get_global_option("platform")
    if self.current_command_options.get_global_option("archive_remote_address") == "":
      self.current_command_options.set_global_option("archive_remote_address", 
                  os.path.join(misc.get_ftp_server(), 'YAMM', 'Public', "e17"))

#######
#
# Commands
#
#######

if __name__ == "__main__":

  print("Testing e17 project class")
  pro = Project(1)
  pro.set_version("E17_SVN")
  pro.print_configuration()
  pro.nothing()


#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.core.framework.software import FrameworkSoftware

software_name = "EINA"

class EINA(FrameworkSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)
    self.e17_main_svn_repository = "http://svn.enlightenment.org/svn/e/trunk"

  def init_variables(self):
    self.archive_file_name = "eina-" + self.version + ".tar.gz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "autoconf"
    self.gen_commands      = ["export NOCONFIGURE=TRUE ; sh autogen.sh"]

    self.remote_type     = "svn"
    self.repository_name = "eina"
    self.root_repository = self.e17_main_svn_repository
    self.tag             = self.version

    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "install", "e17_install")
    self.install_dir = "keep"

    if self.version == "trunk":
      self.tag = ""

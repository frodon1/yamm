#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.core.framework.software import FrameworkSoftware
from yamm.core.engine.dependency import Dependency

software_name = "EIO"

class EIO(FrameworkSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)
    self.e17_main_svn_repository = "http://svn.enlightenment.org/svn/e/trunk"

  def init_variables(self):
    self.archive_file_name = "eio-" + self.version + ".tar.gz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "autoconf"
    self.gen_commands      = ["export NOCONFIGURE=TRUE ; sh autogen.sh"]

    self.remote_type     = "svn"
    self.repository_name = "eio"
    self.root_repository = self.e17_main_svn_repository
    self.tag             = self.version

    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "install", "e17_install")
    self.install_dir = "keep"

    if self.version == "trunk":
      self.tag = ""

  def init_dependency_list(self):
    self.software_dependency_list = ["EINA", "EET", "ECORE"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "EINA":
      dependency_object = Dependency(name="EINA",
                                     depend_of=["pkg"])
    elif dependency_name == "EET":
      dependency_object = Dependency(name="EET",
                                     depend_of=["pkg"])
    elif dependency_name == "ECORE":
      dependency_object = Dependency(name="ECORE",
                                     depend_of=["pkg"])
    return dependency_object

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.core.framework.version import FrameworkVersion

version_name = "E17_SVN"

class E17_SVN(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    self.add_software("EINA", "trunk")
    self.add_software("EIO", "trunk")
    self.add_software("EET", "trunk")
    self.add_software("EVAS", "trunk")
    self.add_software("ECORE", "trunk")
    self.add_software("EMBRYO", "trunk")
    self.add_software("EDJE", "trunk")
    self.add_software("E_DBUS", "trunk")
    self.add_software("EFREET", "trunk")
    self.add_software("EEZE", "trunk")
    self.add_software("ETHUMB", "trunk")
    self.add_software("ELEMENTARY", "trunk")
    self.add_software("EMOTION", "trunk")
    self.add_software("EPHYSICS", "trunk")
    self.add_software("E", "trunk")
    self.add_software("DETOUR_THEME", "trunk")
    self.add_software("DARKNESS_THEME", "trunk")
    self.add_software("EFENNIHT_THEME", "trunk")
    self.add_software("TERMINOLOGY", "trunk")

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os, sys

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.projects.salome.gui.main_project_window import ProjectWindow as SalomeProjectWindow

from yamm.projects.emc2.project import Project
from yamm.projects.emc2.gui     import edit_project_window

class ProjectWindow(SalomeProjectWindow):

  def __init__(self, parent, gui_project, yamm_gui):
    SalomeProjectWindow.__init__(self, parent, gui_project, yamm_gui)
    self.emc2_bin_directory  = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../bin")

  def init_widgets(self):
    SalomeProjectWindow.init_widgets(self)
    self.specific_groupBox.setVisible(False)
    self.line_specific_actions.setVisible(False)

  def get_command_script(self):
    return os.path.join(self.emc2_bin_directory, "emc2_command.py")

  def get_yamm_project_edit_window(self):
    return edit_project_window.EditProjectWindow(self, self.yamm_gui, "edit", self.gui_project)

  def get_empty_yamm_project(self):
    return Project()

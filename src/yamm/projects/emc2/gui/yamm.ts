<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="intro.ui" line="14"/>
        <source>YAMM_TITLE</source>
        <translation>YAMM for EMC2</translation>
    </message>
    <message>
        <location filename="intro.ui" line="27"/>
        <source>CREATE_PROJECT_BASIC</source>
        <translation>Create an EMC2 Project (Basic)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="34"/>
        <source>CREATE_PROJECT_ADVANCED</source>
        <translation>Create an EMC2 Project (Advanced)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="48"/>
        <source>LOAD_PROJECT</source>
        <translation>Load an EMC2 Project</translation>
    </message>
    <message>
        <location filename="intro.ui" line="41"/>
        <source>COPY_PROJECT</source>
        <translation>Copy an EMC2 Project</translation>
    </message>
    <message>
        <location filename="intro.ui" line="72"/>
        <source>Recent projects</source>
        <translation></translation>
    </message>
</context>
</TS>

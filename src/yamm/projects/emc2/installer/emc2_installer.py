#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)


import os

from yamm.core.framework.installer import FrameworkInstaller
from yamm.projects.emc2.installer.strings_template import EMC2InstallerStringsTemplate
from yamm.projects.salome.installer.salome_installer import SalomeInstaller


class EMC2Installer(SalomeInstaller):
    """
    Installer class for EMC2
    NB: Universal installer doesn't work for now because of CBLAS that is taken from the system by Cocagne
    """
    def __init__(self, *args, **kwargs):

        framework_dict = {}
        framework_keys = ['delete', 'runnable', 'nosubdir']
        for k in framework_keys:
            framework_dict[k] = kwargs[k]
            del kwargs[k]
        if args:
            name, logger, topdirectory, softwares, executor = args
            FrameworkInstaller.__init__(self, name, logger, topdirectory, softwares,
                                        executor, **framework_dict)
        else:
            framework_keys = ['name', 'logger', 'topdirectory', 'softwares', 'executor']
            for k in framework_keys:
                framework_dict[k] = kwargs[k]
                del kwargs[k]
            FrameworkInstaller.__init__(self, **framework_dict)

        # Update default values
        kwargs.update(self.get_defaults())

        # To create the installer, we want to use the archives that were generated just before, not those on the remote server
        options = self.executor.options
        options.set_category_option("module", "binary_archive_url",
                                    os.path.join(options.get_category_option("module", "binary_archive_topdir"),
                                                 "%(name)s.tgz"))
        options.set_category_option("prerequisite", "binary_archive_url",
                                    os.path.join(options.get_category_option("prerequisite", "binary_archive_topdir"),
                                                 "%(name)s.tgz"))
        options.set_category_option("tool", "binary_archive_url",
                                    os.path.join(options.get_category_option("tool", "binary_archive_topdir"),
                                                 "%(name)s.tgz"))

        self.init_installer(**kwargs)

    def init_installer(self, **kwargs):
        SalomeInstaller.init_installer(self, **kwargs)
        self.post_install_file_name = os.path.join(self.topdirectory, "emc2_post_install.py")
        self.prerequisites_file = os.path.join(self.topdirectory, "emc2.sh")
        self.context_file = os.path.join(self.topdirectory, "emc2.cfg")
        self.modules_file = self.prerequisites_file

    def get_defaults(self):
        defaults = {}
        defaults["default_name"] = "EMC2"
        defaults["default_contact"] = "https://forge.pleiade.edf.fr/projects/danae"
        defaults["default_exec"] = "emc2_launcher"
        defaults["default_install_dir"] = "$HOME/emc2"
        defaults["default_appli_dir"] = '$HOME/emc2/EMC2'
        defaults["stringsTemplate"] = EMC2InstallerStringsTemplate()
        defaults["salome_version"] = self.executor.version_object.name
        # Ajout du lien emc2_launcher du emc2_launcher
        cmd = '(cd $APPLIDIR && ln -fs salome emc2_launcher)\n'

        # Lien vers le fichier de config du plugin EMC2
        if "PLUGIN_EMC2" in self.map_name_to_softwares:
            cmd += "\n(. emc2.sh && cd \"${APPLIDIR}/env.d\" && ln -fs \"${PLUGIN_EMC2_ROOT}/pluginEnvProducts.cfg\")\n"
        if not self.nosubdir:
            cmd += '(. emc2.sh'
            if 'AVALANCHE' in self.map_name_to_softwares:
                cmd += ' && ln -fs $AVALANCHE_ROOT_DIR avalanche'
            if 'FORMULAIRE' in self.map_name_to_softwares:
                cmd += ' && ln -fs $FORMULAIRE_ROOT_DIR formulaire'
            if 'COUPLEUR_T2C' in self.map_name_to_softwares:
                cmd += ' && ln -fs $HOME_T2C t2c'
            if 'PLUGIN_EMC2' in self.map_name_to_softwares:
                cmd += ' && ln -fs $PLUGIN_EMC2_ROOT plugin_emc2'
            cmd += ' && ln -fs $INTERFACE_MOTEUR_ROOT_DIR interface_moteur'
            cmd += ' && ln -fs $COCAGNE_ROOT_DIR cocagne'
            cmd += ' && ln -fs $HOME_C3THER_POUR_EMC2 c3ther'
            cmd += ' && ln -fs $HOME_THYC thyc'
            cmd += ')\n'
        defaults["final_custom_command"] = cmd
        return defaults

    def get_decompress_language(self):
        return 'LANGUAGE=""\n'

    def get_catalog_cmd(self):
        return ''

    def get_hpc_visu_servers_cmd(self):
        return ''

    def get_create_appli_language(self):
        return ''

    def create_additionnal_directories(self):
        pass

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

import os

from yamm.projects.salome.installer.strings_template import SalomeInstallerStringsTemplate
from yamm.core.base import misc


class EMC2InstallerStringsTemplate(SalomeInstallerStringsTemplate):

    def get_installer_salome_prerequisite_header_check_prereq(self, nosubdir=False):
        thedir = os.path.join('${ROOT_SALOME}', 'prerequisites')
        if nosubdir:
            thedir = '${ROOT_SALOME}'
        return """if test ! -d "{0}"; then
  echo "=========================================="
  echo "Error: EMC2 prerequisites path not found"
  echo "=========================================="
  exit 1
fi
export PREREQUISITES_ROOT_DIR="{0}"

""".format(thedir)

    def get_installer_salome_prerequisite_header_check_tools(self, nosubdir=False):
        thedir = os.path.join('${ROOT_SALOME}', 'tools')
        if nosubdir:
            thedir = '${ROOT_SALOME}'
        return """if test ! -d "{0}"; then
  echo "========================================="
  echo "Error: EMC2 tools path not found"
  echo "========================================="
  exit 1
fi
export TOOLS_ROOT_DIR="{0}"

""".format(thedir)

    def get_readme_env_files(self):
        return """
o emc2.sh                   : environnement file for the softwares
o emc2.cfg                  : context file for the softwares
"""

    def get_readme_other_files(self):
        return ''

    def get_readme_post_install_files(self):
        return """
o emc2_post_install.py                : post-install script used by create_appli
"""

    def get_installer_decompress_usage(self):
        installer_decompress_usage = """usage()
{
cat<<EOF
usage: $0 [-h] [-t DIR] [-a DIR] [-s DIR] [-d] [-m] [-D] [-p] [-f] [-q] [-v]

The script installs %{default_name} in a specified directory.
A desktop shortcut and a menu entry are added.

Options:
 -h    Show this message
 -t    %{default_name} install directory (default is %{default_install_dir})
 -a    %{default_name} virtual application directory (default is %{default_appli_dir})
 -s    %{default_name} temporary directory (default is %{default_temporary_path})
 -d    Do not install desktop file
 -m    Do not install menu entry
 -D    Equivalent to -dm. Do not install desktop file and menu entry
 -p    Do not install default preference file
 -f    Force installation
 -q    Quiet installation
 -v    Verbose mode
EOF
}

"""
        return misc.PercentTemplate(installer_decompress_usage)

    def get_installer_decompress_options(self):
        options = """TARGETDIR=""
APPLIDIR=""
TEMPDIR="%{default_temporary_path}"
DESKTOP="y"
MENU="y"
PREFS="n"
FORCE=""
QUIET=""
VERBOSE=""

while getopts ":ht:a:s:dmDpfqv" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    t)
      TARGETDIR="$OPTARG"
      ;;
    a)
      APPLIDIR="$OPTARG"
      ;;
    s)
      TEMPDIR="$OPTARG"
      ;;
    d)
      DESKTOP=""
      ;;
    m)
      MENU=""
      ;;
    D)
      DESKTOP=""
      MENU=""
      ;;
    p)
      PREFS=""
      ;;
    f)
      FORCE="y"
      ;;
    q)
      QUIET="y"
      ;;
    v)
      VERBOSE="y"
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done"""
        return misc.PercentTemplate(options)

    def get_installer_create_appli_command_template(self):
        template = 'HPC_CATALOG=""'
        template += SalomeInstallerStringsTemplate.get_installer_create_appli_command_template(self).template
        return misc.PercentTemplate(template)

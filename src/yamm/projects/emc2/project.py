#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)
from __future__ import print_function

import os
import sys
import platform
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base import misc
from yamm.core.framework.project import yamm_command

from yamm.core.base.misc import VerboseLevels
from yamm.projects.salome.project   import Project       as SalomeProject
from yamm.projects.emc2.software import EMC2Software
from yamm.projects.emc2.installer.emc2_installer import EMC2Installer

from yamm.projects.cocagne          import softwares     as cocagne_softwares
from yamm.projects.salome_coeur.softwares   import modules       as salome_coeur_modules

from yamm.projects.emc2.softwares  import tools         as default_tools
from yamm.projects.emc2.softwares  import modules       as default_modules
from yamm.projects.emc2.softwares  import prerequisites as default_prerequisites
from yamm.projects.emc2            import versions      as default_versions

from yamm.projects.salome.appli_tasks import CreateAppliTask


def get_ftp_server():
  return os.path.join(misc.get_ftp_server(), 'F3C', 'EEM')


class Project(SalomeProject):

  def __init__(self, log_name="EMC2", verbose_level=VerboseLevels.INFO):
    SalomeProject.__init__(self, log_name, verbose_level)
    self.init_emc2_vars()
    self.define_emc2_options()
    pass

  def init_catalog(self):
    SalomeProject.init_catalog(self)

    # Suppression des versions non définies dans EMC2
    for version_name in self.catalog.versions.keys():
      if version_name not in default_versions.versions_list:
        del self.catalog.versions[version_name]

    self.catalog.name = "EMC2_ODYSSEE"
    # Import des softwares COCAGNE
    for name in cocagne_softwares.softwares_list:
      self.add_software_in_catalog(name, "yamm.projects.cocagne.softwares")
    # Import des modules SALOME COEUR
    for name in salome_coeur_modules.modules_list:
      self.add_software_in_catalog(name, "yamm.projects.salome_coeur.softwares.modules")
    # Import des tools
    for module_name in default_tools.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.emc2.softwares.tools")
    # Import des modules
    for module_name in default_modules.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.emc2.softwares.modules")
    # Import des prerequis
    for module_name in default_prerequisites.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.emc2.softwares.prerequisites")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.emc2.versions")
      pass
    self.logger.debug("End init catalog")
    pass

  def init_emc2_vars(self):
    pass

  def define_emc2_options(self):
#     self.options.add_option("cocagne_archive_filename", "", ["software"])
#     self.options.add_option("BaseExploitation_path", "", ["global"])
#     self.options.add_option("Studies_path", "", ["global"])
#     self.options.add_option("Sideral_path", "", ["global"])
#     self.options.add_option("Dkzip_path", "", ["global"])
    pass

#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "emc2"))
      pass

    if not current_command_options.get_global_option("version_directory"):
      current_command_options.set_global_option("version_directory", os.path.join(current_command_options.get_global_option("top_directory"), self.version))

    if not current_command_options.get_global_option("salome_prerequisites_env_file"):
      current_command_options.set_global_option("salome_prerequisites_env_file", "%s/emc2.sh" % (current_command_options.get_global_option("version_directory")))
    if not current_command_options.get_global_option("salome_modules_env_file"):
      current_command_options.set_global_option("salome_modules_env_file", "%s/emc2.sh" % (current_command_options.get_global_option("version_directory")))
    if not current_command_options.get_global_option("salome_context_file"):
      current_command_options.set_global_option("salome_context_file", "%s/emc2.cfg" % (current_command_options.get_global_option("version_directory")))

#     if not current_command_options.get_global_option("BaseExploitation_path"):
#       current_command_options.set_global_option("BaseExploitation_path", "/data/projets/projets.001/base_exploit_andromede.087/")
#       pass
# 
#     if not current_command_options.get_global_option("Studies_path"):
#       current_command_options.set_global_option("Studies_path", os.environ["HOME"])
#       pass
# 
#     if not current_command_options.get_global_option("Sideral_path"):
#       current_command_options.set_global_option("Sideral_path", os.environ["HOME"])
#       pass
# 
#     if not current_command_options.get_global_option("Dkzip_path"):
#       current_command_options.set_global_option("Dkzip_path", "/data/projets/projets.001/gabv2.001/BibliothequesParc/DKLibs_2.0/")
#       pass

    # Disable Cocagne tests by default (saves time at compilation)
    if current_command_options.is_software_option("COCAGNE_EMC2_ODYSSEE", "software_additional_config_options"):
      additional_config_options = current_command_options.get_option("COCAGNE_EMC2_ODYSSEE", "software_additional_config_options")
    else:
      additional_config_options = ""
    current_command_options.set_software_option("COCAGNE_EMC2_ODYSSEE", "software_additional_config_options", " -DUSE_CHECKS=OFF " + additional_config_options)

    # Enable/Disable YACS GUI
    if current_command_options.is_software_option("YACS", "software_additional_config_options"):
      additional_config_options = current_command_options.get_option("YACS", "software_additional_config_options")
    else:
      additional_config_options = ""
    YACS_options =  ""
    if "PRODUCTION" in self.version_flavour: # Pas besoin de l'IHM de YACS en production
      YACS_options = " -DSALOME_BUILD_GUI=OFF "
    current_command_options.set_software_option("YACS", "software_additional_config_options", YACS_options + additional_config_options)

    # Disable some viewers when we have GUI
    if current_command_options.is_software_option("GUI", "software_additional_config_options"):
      additional_config_options = current_command_options.get_option("GUI", "software_additional_config_options")
    else:
      additional_config_options = ""
    GUI_options = ""
    if self.version_flavour is "YACS GUI ONLY": # Il faut desactiver les autres viewers
      GUI_options = " -DSALOME_USE_PVVIEWER=OFF -DSALOME_USE_VTKVIEWER=OFF -DSALOME_USE_OCCVIEWER=OFF -DSALOME_USE_GLVIEWER=OFF  -DSALOME_USE_PLOT2DVIEWER=OFF "
    current_command_options.set_software_option("GUI", "software_additional_config_options", GUI_options + additional_config_options)

    # Specific remote source_type for some prerequisites
    softs = ["C3THER_POUR_EMC2"]
    for soft in softs:
      if not current_command_options.is_software_option(soft, "source_type"):
        current_command_options.set_software_option(soft, "source_type", "remote")

    # Create appli by default
    if not current_command_options.get_global_option("create_appli"):
      current_command_options.set_global_option("create_appli", True)

    # Salome project default values
    SalomeProject.set_default_options_values(self, current_command_options)

    current_command_options.set_global_option('use_system_version', True)

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):
    SalomeProject.check_specific_configuration(self, current_command_options)
    pass

#######
#
# Print methods
#
#######

  def print_general_configuration(self, current_command_options):
    SalomeProject.print_general_configuration(self, current_command_options)
    self.logger.info("EMC2 Version          : %s" % self.version)
    pass

#######
#
# Méthodes internes pour les commandes
#
#######


  def create_env_files(self, delete=True, installer=False, nosubdir=False):
    self.create_context_file(delete, installer, nosubdir)
    self.create_prerequisites_file(delete, installer, nosubdir)
    self.create_modules_file(False)

  def update_configuration(self):
    # Change archive remote address for EMC2 private softwares
    # Those softwares inherit from EMC2Software
    for soft in self.catalog.softwares:
      software_class = self.catalog.get_software_class(soft)
      if issubclass(software_class, EMC2Software) and \
          self.current_command_options.get_option(soft, "archive_remote_address") == "":
        self.current_command_options.set_software_option(soft, "archive_remote_address", get_ftp_server())
    SalomeProject.update_configuration(self)


#######
#
# Lanceur
#
########

  def _customize_appli(self):
    plugin_emc2 = None
    module_load = []
    for software in self.current_command_softwares:
        software.set_command_env(self.current_command_softwares,
                                 self.current_command_options,
                                 'nothing',
                                 self.current_command_version_object,
                                 self.current_command_python_version)
        if software.name == "PLUGIN_EMC2":
            plugin_emc2 = software
        if software.module_load:
            module_load += [m for m in software.module_load.split() if m not in module_load]

    emc2_launcher = os.path.join(self.appli_directory, self._get_appli_launcher())
    try:
        os.remove(emc2_launcher)
    except:
        pass
    os.symlink('./salome', emc2_launcher)

    if plugin_emc2 is not None:
        plugin_EMC2_Env = os.path.join(self.appli_directory, "env.d", "pluginEnvProducts.cfg")
        os.symlink(os.path.join(plugin_emc2.install_directory, "pluginEnvProducts.cfg"),
                   plugin_EMC2_Env)

  @yamm_command("Creation of an EMC2pplication")
  def create_appli(self, appli_topdirectory="", appli_name="EMC2",
                   appli_catalogresources="", generates_env_files=True):
    ret = self._create_appli(appli_topdirectory, appli_name,
                             appli_catalogresources, generates_env_files)
    if not ret:
        return ret

    self._customize_appli()
    return ret

  def _get_appli_launcher(self):
    return 'emc2_launcher'

  @yamm_command("Launch of EMC2pplication")
  def run_appli(self, appli_topdirectory="", appli_name="EMC2", appli_options=""):
    return SalomeProject._run_appli(self, appli_topdirectory, appli_name, appli_options)

  def get_version_custom_tasks(self): # Pour eviter la creation de 2 applis avec 2 noms differents
    tasks = SalomeProject.get_version_custom_tasks(self)
    if tasks:
        appli_topdirectory = self.current_command_options.get_global_option("version_directory")
        self.appli_directory = os.path.join(appli_topdirectory, "EMC2")
        tasks[0].appli_directory = self.appli_directory
        for test_suite in self.current_command_version_object.test_suite_list:
            test_suite.appli_directory = self.appli_directory
    return tasks

#######
#
# Installeur
#
#######

  def get_installer_class(self):
    return EMC2Installer

  def get_installer_name(self):
    formatted_version = self.version.replace("V", "v").replace("_", ".")
    return "EMC2-{0}".format(formatted_version)


if __name__ == "__main__":

  print("Testing emc2 project class")
  pro = Project(VerboseLevels.DEBUG)
  pro.set_version("DEV")
  pro.print_configuration()
  pro.nothing()
#  pro.download()
#  pro.start()

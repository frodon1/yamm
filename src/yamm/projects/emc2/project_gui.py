#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.core.framework.project_gui import FrameworkProjectGui

from yamm.projects.emc2.gui import gui_project
from yamm.projects.emc2.gui import start_widget

class ProjectGui(FrameworkProjectGui):

  def __init__(self, app = None, yamm_class_name="emc2"):
    FrameworkProjectGui.__init__(self, app, yamm_class_name)

  def getIntroWidget(self):
    return start_widget.IntroWidget(self)

  def get_yamm_gui_project(self):
    return gui_project.YammGuiProject()

  def initYammProjectFromWizard(self, new_project, wizard):
    FrameworkProjectGui.initYammProjectFromWizard(self, new_project, wizard)

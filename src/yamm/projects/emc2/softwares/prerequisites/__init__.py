modules_list = ["chardet", "validate", "mako", "pylint", "astroid",
                 "logilab_common", "c3ther_pour_emc2",
                 "coupleur_t2c", "sic", "thyc", "thycox", "cpprest_sdk", "openssl",
                 "jedi", "wrapt", "lazy_object_proxy", "sideral", "intel", "isl"]

modules_list += ["pockets","sphinxcontrib_napoleon"]
modules_list += ["markupsafe"]
modules_list += ["rst2pdf"]
modules_list += ["nose"]
modules_list += ["gradyn_6groupes","gradyn_8groupes"]
modules_list += ["nose"]
modules_list += ["tabulate"]

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "C3THER_POUR_EMC2"
c3ther_template = """
#------ c3ther ------
export HOME_C3THER_POUR_EMC2="%install_dir"
export LD_LIBRARY_PATH=${HOME_C3THER_POUR_EMC2}/lib:$LD_LIBRARY_PATH
export PYTHONPATH=${HOME_C3THER_POUR_EMC2}/lib:$PYTHONPATH
export PYTHONPATH=${HOME_C3THER_POUR_EMC2}/interface:$PYTHONPATH
export PYTHONPATH=${HOME_C3THER_POUR_EMC2}/procedures:$PYTHONPATH
"""
c3ther_template = misc.PercentTemplate(c3ther_template)

c3ther_configuration_template = """
#------ c3ther ------
HOME_C3THER_POUR_EMC2="$install_dir"
ADD_TO_LD_LIBRARY_PATH: %(HOME_C3THER_POUR_EMC2)s/lib
ADD_TO_PYTHONPATH: %(HOME_C3THER_POUR_EMC2)s/lib
ADD_TO_PYTHONPATH: %(HOME_C3THER_POUR_EMC2)s/interface
ADD_TO_PYTHONPATH: %(HOME_C3THER_POUR_EMC2)s/procedures
"""
c3ther_configuration_template = string.Template(c3ther_configuration_template)

class C3THER_POUR_EMC2(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = "c3ther_pour_emc2-" + self.get_version_for_archive().lower() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "cmake"

    # No proxy for C3THER_POUR_EMC2
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "")

    self.remote_type = "git"
    self.repository_name = "c3ther_pour_emc2"
    self.root_repository = "https://git.forge.pleiade.edf.fr/git"
    self.tag = self.version

    if misc.get_calibre_version() == "9":  # Probleme avec detection C9 NUMPY natif
      self.config_options += " -DIS_CALIBRE_9:BOOL=ON "
    # self.post_install_commands.append("rm c3t*f90 *.o *.mod *.c")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "NUMPY"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    elif dependency_name == "NUMPY":
      dependency_object.depend_of = ["path", "python_path", "install_path"]
      self.config_options.replace("-DIS_CALIBRE_9:BOOL=ON", "")  # Pas de NUMPY natif
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return c3ther_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), ordered_version=self.ordered_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return c3ther_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), ordered_version=self.ordered_version)

  def get_extra_test_path_list(self):
    """
    This method returns a list of paths to the software extra tests.
    Define each path relative to the software install directory.
    """
    return ["tests"]

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.emc2.software import EMC2Software
from yamm.core.base import misc
import os
import string

software_name = "COUPLEUR_T2C"
t2c_template = """
#------ t2c ------
export HOME_T2C="%install_dir"
export PATH=${HOME_T2C}/bin:${PATH}
export PYTHONPATH=${PYTHONPATH}:${HOME_T2C}
"""
t2c_template = misc.PercentTemplate(t2c_template)

t2c_configuration_template = """
#------ t2c ------
HOME_T2C="$install_dir"
ADD_TO_PATH: %(HOME_T2C)s/bin
ADD_TO_PYTHONPATH: %(HOME_T2C)s
"""
t2c_configuration_template = string.Template(t2c_configuration_template)

class COUPLEUR_T2C(EMC2Software):

  def init_variables(self):
    EMC2Software.init_variables(self)

    self.archive_file_name = "t2c_" + self.get_version_for_archive().lower() + ".tar.gz"
    self.archive_type = "tar.gz"
    if self.project_options.get_option(self.name, "archive_remote_address") == "":
      self.archive_address = "http://ftp.pleiade.edf.fr/projets/F3C/EEM"

    self.compil_type = "rsync"

    self.remote_type = "svn"
    self.root_repository = "https://noeyy727.noe.edf.fr"
    self.repository_name = "mfee/t2c"
    self.tag = self.version

    self.shell = "/bin/bash"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["CHARDET", "VALIDATE", "THYC", "COCAGNE_EMC2_ODYSSEE"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = EMC2Software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CHARDET":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "VALIDATE":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "THYC":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "COCAGNE_EMC2_ODYSSEE":
      dependency_object.depend_of = ["python_path"]
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return t2c_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return t2c_configuration_template.substitute(install_dir=install_dir)

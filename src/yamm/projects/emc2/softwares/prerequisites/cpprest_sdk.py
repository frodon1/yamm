#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CPPREST_SDK"
cpprest_sdk_template = """
#------- cpprest_sdk ------
CPPREST_SDK_HOME="%install_dir"
export PATH=${CPPREST_SDK_HOME}/bin:${PATH}
export PYTHONPATH=${CPPREST_SDK_HOME}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
cpprest_sdk_template = misc.PercentTemplate(cpprest_sdk_template)

cpprest_sdk_configuration_template = """
#------- cpprest_sdk ------
CPPREST_SDK_HOME="$install_dir"
ADD_TO_PATH: %(CPPREST_SDK_HOME)s/bin
ADD_TO_PYTHONPATH: %(CPPREST_SDK_HOME)s/lib/python$python_version/site-packages
"""
cpprest_sdk_configuration_template = string.Template(cpprest_sdk_configuration_template)

class CPPREST_SDK(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "cpprest_sdk-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "specific"
    self.shell = "/bin/bash"
    self.specific_configure_command = " cd $CURRENT_SOFTWARE_BUILD_DIR && cmake -DCMAKE_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR  $CURRENT_SOFTWARE_SRC_DIR/Release "
    parallel_make = self.project_options.get_option(self.name, "parallel_make")
    self.specific_build_command = " cd $CURRENT_SOFTWARE_BUILD_DIR && make -j%s " % (parallel_make)
    self.specific_install_command = " cd $CURRENT_SOFTWARE_BUILD_DIR && make install"

    self.remote_type = "git"
    self.repository_name = "casablanca"
    self.root_repository = "https://git01.codeplex.com"
    self.tag = self.version
    # Set proxy :
    if not self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server",
                                        "http://proxypac.edf.fr:3128")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GCC", "OPENSSL", "BOOST"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "OPENSSL":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "GCC":
      dependency_object.depend_of = ["install_path"]  # gere par l environnement
    if dependency_name == "BOOST":
      dependency_object.depend_of = ["install_path", "ld_lib_path"]
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cpprest_sdk_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cpprest_sdk_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.emc2.software import EMC2Software
from yamm.core.base import misc
import os
import string

software_name = "GRADYN_6GROUPES"
gradyn_6groupes_template = """
#------ gradyn_6groupes ------
export HOME_GRADYN_6GROUPES="%install_dir"
export PATH=${HOME_GRADYN_6GROUPES}:$PATH
export GRADYN_ROOT_6G=${HOME_GRADYN_6GROUPES}
export AVALANCHE_GRADYN_EXE_6G=${HOME_GRADYN_6GROUPES}
export AVALANCHE_GRADYN_VERSION_6G=%ordered_version
"""
gradyn_6groupes_template = misc.PercentTemplate(gradyn_6groupes_template)

gradyn_6groupes_configuration_template = """
#------ gradyn_6groupes ------
HOME_GRADYN_6GROUPES="$install_dir"
ADD_TO_PATH: %(HOME_GRADYN_6GROUPES)s
GRADYN_ROOT_6G=%(HOME_GRADYN_6GROUPES)s
AVALANCHE_GRADYN_EXE_6G=%(HOME_GRADYN_6GROUPES)s
AVALANCHE_GRADYN_VERSION_6G=$ordered_version
"""
gradyn_6groupes_configuration_template = string.Template(gradyn_6groupes_configuration_template)

class GRADYN_6GROUPES(EMC2Software):

  def init_variables(self):
    EMC2Software.init_variables(self)

    self.archive_file_name = "gradyn-" + self.get_version_for_archive() + "-6groupes-C9.tar.gz"
    self.archive_type = "tar.gz"
    if self.project_options.get_option(self.name, "archive_remote_address") == "":
      self.archive_address = "http://ftp.pleiade.edf.fr/projets/F3C/EEM/" + self.archive_file_name

    self.compil_type = "rsync"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gradyn_6groupes_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gradyn_6groupes_configuration_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

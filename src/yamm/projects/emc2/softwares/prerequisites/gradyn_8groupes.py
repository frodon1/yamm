#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.emc2.software import EMC2Software
from yamm.core.base import misc
import os
import string

software_name = "GRADYN_8GROUPES"
gradyn_8groupes_template = """
#------ gradyn_8groupes ------
export HOME_GRADYN_8GROUPES="%install_dir"
export PATH=${HOME_GRADYN_8GROUPES}/bin:$PATH
export GRADYN_ROOT_8G=${HOME_GRADYN_8GROUPES}
export AVALANCHE_GRADYN_EXE_8G=${HOME_GRADYN_8GROUPES}/bin
export AVALANCHE_GRADYN_VERSION_8G=%ordered_version

"""
gradyn_8groupes_template = misc.PercentTemplate(gradyn_8groupes_template)

gradyn_8groupes_configuration_template = """
#------ gradyn_8groupes ------
HOME_GRADYN_8GROUPES="$install_dir"
ADD_TO_PATH: %(HOME_GRADYN_8GROUPES)s/bin
GRADYN_ROOT_8G=%(HOME_GRADYN_8GROUPES)s
AVALANCHE_GRADYN_EXE_8G=%(HOME_GRADYN_8GROUPES)s/bin
AVALANCHE_GRADYN_VERSION_8G=$ordered_version
"""
gradyn_8groupes_configuration_template = string.Template(gradyn_8groupes_configuration_template)

class GRADYN_8GROUPES(EMC2Software):

  def init_variables(self):
    EMC2Software.init_variables(self)

    self.archive_file_name = "gradyn-" + self.get_version_for_archive() + "-8groupes.tar.gz"
    self.archive_type = "tar.gz"
    #if self.project_options.get_option(self.name, "archive_remote_address") == "":
    #  self.archive_address = "http://ftp.pleiade.edf.fr/projets/F3C/EEM/" + self.archive_file_name
    self.compil_type = "specific"

    self.pre_configure_commands = [ "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR"]
    self.pre_configure_commands.append("tar --exclude-vcs -f - -c * | tar -C $CURRENT_SOFTWARE_INSTALL_DIR -f - -x ;")
    self.specific_install_command = "( cd $CURRENT_SOFTWARE_INSTALL_DIR && make clean && make )"
    self.post_install_commands.append("( cd $CURRENT_SOFTWARE_INSTALL_DIR/bin  && ln -s gradyn_* gradyn-2.2.99 )")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = []

  def get_dependency_object_for(self, dependency_name):
    dependency_object = EMC2Software.get_dependency_object_for(self, dependency_name)
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    pass  

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gradyn_8groupes_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gradyn_8groupes_configuration_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

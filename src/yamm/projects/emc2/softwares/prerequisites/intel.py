#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from yamm.projects.emc2.software import EMC2Software
from yamm.core.base import misc
import os
import string

software_name = "INTEL"
intel_template = """
#------ intel ------
export HOME_INTEL="%install_dir"
export PATH=${HOME_INTEL}/bin/intel64:$PATH
export LD_LIBRARY_PATH=${HOME_INTEL}/compiler/lib/intel64:$LD_LIBRARY_PATH
export INTEL_LICENSE_FILE=28518@10.122.1.12
"""
intel_template = misc.PercentTemplate(intel_template)

intel_configuration_template = """
#------ intel ------
HOME_INTEL="$install_dir"
INTEL_LICENSE_FILE=28518@10.122.1.12
ADD_TO_PATH: %(HOME_INTEL)s/bin/intel64
ADD_TO_LD_LIBRARY_PATH: %(HOME_INTEL)s/compiler/lib/intel64
"""
intel_configuration_template = string.Template(intel_configuration_template)

class INTEL(EMC2Software):

  def init_variables(self):
    EMC2Software.init_variables(self)

    self.archive_file_name = "intel-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"
    if self.project_options.get_option(self.name, "archive_remote_address") == "":
      self.archive_address = "http://ftp.pleiade.edf.fr/projets/F3C/EEM"

    self.compil_type = "rsync"

    # TODO
    self.remote_type = ""
    self.root_repository = ""
    self.repository_name = ""
    self.tag = self.version

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return intel_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), ordered_version=self.ordered_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return intel_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), ordered_version=self.ordered_version)

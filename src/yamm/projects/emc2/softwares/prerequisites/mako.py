#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "MAKO"
mako_template = """
#------- mako ------
MAKO_HOME="%install_dir"
export PYTHONPATH=${MAKO_HOME}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
mako_template = misc.PercentTemplate(mako_template)

mako_configuration_template = """
#------- mako ------
MAKO_HOME="$install_dir"
ADD_TO_PYTHONPATH: %(MAKO_HOME)s/lib/python$python_version/site-packages
"""
mako_configuration_template = string.Template(mako_configuration_template)

class MAKO(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "Mako-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.repository_name = "mako"
    self.root_repository = "https://github.com/zzzeek"
    self.tag = self.version
    # Set proxy :
    if not self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server",
                                        "http://proxypac.edf.fr:3128")
    self.compil_type = "python"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
    if dependency_name == "SETUPTOOLS":
      dependency_object.depend_of = ["python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    if dependency_name == "SETUPTOOLS":
      self.executor_software_name += "-set" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mako_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mako_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

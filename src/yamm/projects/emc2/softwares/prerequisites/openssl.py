#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "OPENSSL"
openssl_template = """
#------- openssl ------
OPENSSL_ROOT_DIR="%install_dir"
export PATH=${OPENSSL_ROOT_DIR}/bin:${PATH}
export LD_LIBRARY_PATH=${OPENSSL_ROOT_DIR}/lib:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=${OPENSSL_ROOT_DIR}/lib/pkgconfig:${PKG_CONFIG_PATH}
"""
openssl_template = misc.PercentTemplate(openssl_template)

openssl_configuration_template = """
#------- openssl ------
OPENSSL_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(OPENSSL_ROOT_DIR)s/bin
ADD_TO_LD_LIBRARY_PATH: %(OPENSSL_ROOT_DIR)s/lib
"""
openssl_configuration_template = string.Template(openssl_configuration_template)

class OPENSSL(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "openssl-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "specific"
    self.shell = "/bin/bash"
    self.specific_configure_command = " ./config shared --prefix=$CURRENT_SOFTWARE_INSTALL_DIR --openssldir=$CURRENT_SOFTWARE_INSTALL_DIR/openssl "
    parallel_make = self.project_options.get_option(self.name, "parallel_make")
    self.specific_build_command = "make -j%s " % (parallel_make)
    self.specific_install_command = "make test && make install"

    self.remote_type = "git"
    self.repository_name = "openssl.git"
    self.root_repository = "https://github.com/openssl"
    self.tag = self.version
    # Set proxy :
    if not self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server",
                                        "http://proxypac.edf.fr:3128")

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return openssl_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return openssl_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

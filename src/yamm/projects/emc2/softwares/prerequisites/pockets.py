#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "POCKETS"
pockets_template = """
#------- pockets ------
export POCKETS_ROOT_DIR="%install_dir"
export PATH=${POCKETS_ROOT_DIR}/bin:${PATH}
export PYTHONPATH=${POCKETS_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
pockets_template = misc.PercentTemplate(pockets_template)
# WARNING, the export of POCKETS_ROOT_DIR is used by kernel during Sphinx compilation (Cmake mode), do not remove

pockets_configuration_template = """
#------- pockets ------
POCKETS_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(POCKETS_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(POCKETS_ROOT_DIR)s/lib/python$python_version/site-packages
"""
pockets_configuration_template = string.Template(pockets_configuration_template)
# WARNING, the export of POCKETS_ROOT_DIR is used by kernel during Sphinx compilation (Cmake mode), do not remove

class POCKETS(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "pockets-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
        dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "SETUPTOOLS":
        dependency_object.depend_of = ["python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    if dependency_name == "SETUPTOOLS":
        self.executor_software_name += "-set" + misc.transform(version_name)
        if(misc.compareVersions("$SETUPTOOLSVERSION", "5.5.1") < 0):
            self.replacePythonHeaderInstallPath("bin/easy_install")
            self.replacePythonHeaderInstallPath("bin/easy_install-" + self.python_version)
  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pockets_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pockets_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

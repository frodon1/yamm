#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from yamm.projects.emc2.software import EMC2Software
from yamm.core.base import misc
import os
import string

software_name = "SIDERAL"
sideral_template = """
#------ sideral ------
export HOME_SIDERAL="%install_dir"
export PATH=${HOME_SIDERAL}:$PATH
export LD_LIBRARY_PATH=${HOME_SIDERAL}:$LD_LIBRARY_PATH
export AVALANCHE_SIDERAL_EXE=${HOME_SIDERAL}/sideral-%major.%minor
export AVALANCHE_SIDERAL_VERSION=%major.%minor.%patch
"""
sideral_template = misc.PercentTemplate(sideral_template)

sideral_configuration_template = """
#------ sideral ------
HOME_SIDERAL="$install_dir"
ADD_TO_PATH: %(HOME_SIDERAL)s
ADD_TO_LD_LIBRARY_PATH: %(HOME_SIDERAL)s
AVALANCHE_SIDERAL_EXE=%(HOME_SIDERAL)s/sideral-$major.$minor
AVALANCHE_SIDERAL_VERSION=$major.$minor.$patch
"""
sideral_configuration_template = string.Template(sideral_configuration_template)

class SIDERAL(EMC2Software):

  def init_variables(self):
    EMC2Software.init_variables(self)

    self.archive_file_name = "sideral-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"
    if self.project_options.get_option(self.name, "archive_remote_address") == "":
      self.archive_address = "http://ftp.pleiade.edf.fr/projets/F3C/EEM"

    self.compil_type = "rsync"

    # TODO
    self.remote_type = ""
    self.root_repository = ""
    self.repository_name = ""
    self.tag = self.version

    if self.version in ["2.3","2.4"]:
      if misc.get_calibre_version() != "7":  # Pas bonne version de la libssl sur C9
        self.post_install_commands.append("lib=$(dpkg -L libssl1.0.0 |grep libssl.so); if [ x\"$lib\" != \"x\" ]; then ln -s $lib libssl.so.0.9.8; fi")

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    maintenance = misc.maintenance(self.ordered_version)
    if maintenance == -1 :
      maintenance = 0
    return sideral_template.substitute(install_dir=install_dir, major=misc.major(self.ordered_version), minor=misc.minor(self.ordered_version), patch=maintenance)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    maintenance = misc.maintenance(self.ordered_version)
    if maintenance == -1 :
      maintenance = 0
    return sideral_configuration_template.substitute(install_dir=install_dir, major=misc.major(self.ordered_version), minor=misc.minor(self.ordered_version), patch=maintenance)


#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.emc2.software import EMC2Software
from yamm.core.base import misc
import os
import string

software_name = "THYC"
thyc_template = """
#------ thyc ------
export HOME_THYC="%install_dir"
export PATH=${HOME_THYC}/bin:$PATH
export PYTHONPATH=${PYTHONPATH}:${HOME_THYC}/pythyc
export VER_LOGICIEL=%ordered_version
export VER_THYC=%ordered_version
export VER_THYC_ECHANGEUR=%ordered_version
export AVALANCHE_THYC_ROOT=${HOME_THYC}
export AVALANCHE_THYC_EXE=${HOME_THYC}/bin/thyc
export AVALANCHE_THYC_CONFIG=${HOME_THYC}/config.sh
export AVALANCHE_THYC_VERSION=%ordered_version

"""
thyc_template = misc.PercentTemplate(thyc_template)

thyc_configuration_template = """
#------ thyc ------
HOME_THYC="$install_dir"
ADD_TO_PATH: %(HOME_THYC)s/bin:
ADD_TO_PYTHONPATH: %(HOME_THYC)s/pythyc
VER_LOGICIEL=$ordered_version
VER_THYC=$ordered_version
VER_THYC_ECHANGEUR=$ordered_version
AVALANCHE_THYC_ROOT=%(HOME_THYC)s
AVALANCHE_THYC_EXE=%(HOME_THYC)s/bin/thyc
AVALANCHE_THYC_CONFIG=%(HOME_THYC)s/config.sh
AVALANCHE_THYC_VERSION=$ordered_version
"""
thyc_configuration_template = string.Template(thyc_configuration_template)

class THYC(EMC2Software):

  def init_variables(self):
    EMC2Software.init_variables(self)

    self.archive_file_name = "THYC-" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"
    if self.project_options.get_option(self.name, "archive_remote_address") == "":
      self.archive_address = "http://ftp.pleiade.edf.fr/projets/F3C/EEM"

    self.compil_type = "specific"

    self.remote_type = "svn"
    self.root_repository = "https://noeyy727.noe.edf.fr"
    self.repository_name = "mfee/thyc"
    self.tag = self.version

    self.shell = "/bin/bash"
    self.pre_configure_commands = [ "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR"]
    self.pre_configure_commands.append("tar --exclude-vcs -f - -c * | tar -C $CURRENT_SOFTWARE_INSTALL_DIR -f - -x ;")
    self.pre_configure_commands.append("mv $CURRENT_SOFTWARE_INSTALL_DIR/{0}/* "
                                       "$CURRENT_SOFTWARE_INSTALL_DIR && "
                                       "rm -Rf  $CURRENT_SOFTWARE_INSTALL_DIR/{0}"
                                       .format(self.get_version_for_archive()))
    if 'PYTHON' not in self.project_softwares_dict:  # Python Natif 2.7
      self.replaceStringInFilePreInstall("/usr/include/python2.6" ,
                                         "/usr/include/python" + self.python_version,
                                         os.path.join("compileTHYC", "config.py"))
      self.replaceStringInFilePreInstall("-lpython2.6" ,
                                         "-lpython" + self.python_version,
                                         os.path.join("compileTHYC", "config.py"))
    self.specific_install_command = "cd $CURRENT_SOFTWARE_INSTALL_DIR && ./compile -c  && ./compile -j pythyc"
    self.post_install_commands.append("./install.sh ;")
    self.post_install_commands.append("rm -Rf pythyc && cp -Rp build/Linux_x86_64_intel_r8/pythyc .")
    dirToCopy_src = "$CURRENT_SOFTWARE_SRC_DIR/%s/pythyc/cas_tests" % self.get_version_for_archive()
    if self.ordered_version == "5.2.6":
        dirToCopy_src += "_pythyc"
    dirToCopy_dest = "$CURRENT_SOFTWARE_SRC_DIR/%s/pythyc/pythyc" % self.get_version_for_archive()
    self.post_install_commands.append("cp -Rp %s %s $CURRENT_SOFTWARE_INSTALL_DIR/pythyc"
                                      % (dirToCopy_src, dirToCopy_dest))
    self.post_install_commands.append("rm lib/libmed-3.1.0/bin/mdump && "
                                      "rm lib/libmed-3.1.0/bin/xmdump && "
                                      "(cd lib/libmed-3.1.0/bin && "
                                      "ln -s mdump3 mdump && "
                                      "ln -s xmdump3 xmdump)")
    self.replaceStringInFilePreInstall('/projets/projets.001/thyc.014/SALOME/MED/med-3.1.0/bin/mdump3',
                                       '%s/lib/libmed-3.1.0/bin/mdump3' % self.install_directory,
                                       'lib/libmed-3.1.0/bin/xmdump3')

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["MATPLOTLIB", "CHARDET", "PYTHON",
                                             "INTEL"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = EMC2Software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "MATPLOTLIB":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "INTEL":
      dependency_object.depend_of = ["path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "MATPLOTLIB":
      self.executor_software_name += "-mpl" + misc.transform(version_name)
    elif dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
      self.replaceStringInFilePreInstall("/usr/bin/python" , "/usr/bin/env python" , "compile")
      self.replaceStringInFilePreInstall("/usr/include/python2.6" ,
                                         "$PYTHON_INSTALL_DIR/include/python" + self.python_version,
                                         os.path.join("compileTHYC", "config.py"))
      self.replaceStringInFilePreInstall("-lpython2.6" ,
                                         "-L$PYTHON_INSTALL_DIR/lib -lpython" + self.python_version,
                                         os.path.join("compileTHYC", "config.py"))
    elif dependency_name == "INTEL":
      self.executor_software_name += "-if" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return thyc_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return thyc_configuration_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.emc2.software import EMC2Software
from yamm.core.base import misc
import os
import string

software_name = "THYCOX"
thycox_template = """
#------ thycox ------
export HOME_THYCOX="%install_dir"
export PATH=${HOME_THYCOX}/bin:$PATH
export AVALANCHE_THYCOX_ROOT=${HOME_THYCOX}
export AVALANCHE_THYCOX_EXE=${HOME_THYCOX}/bin/thycox
export AVALANCHE_THYCOX_CONFIG=${HOME_THYCOX}/config.sh
export AVALANCHE_THYCOX_VERSION=%ordered_version

"""
thycox_template = misc.PercentTemplate(thycox_template)

thycox_configuration_template = """
#------ thycox ------
HOME_THYCOX="$install_dir"
ADD_TO_PATH: %(HOME_THYCOX)s/bin
AVALANCHE_THYCOX_ROOT=%(HOME_THYCOX)s
AVALANCHE_THYCOX_EXE=%(HOME_THYCOX)s/bin/thycox
AVALANCHE_THYCOX_CONFIG=%(HOME_THYCOX)s/config.sh
AVALANCHE_THYCOX_VERSION=$ordered_version
"""
thycox_configuration_template = string.Template(thycox_configuration_template)

class THYCOX(EMC2Software):

  def init_variables(self):
    EMC2Software.init_variables(self)

    self.archive_file_name = "THYCOX-" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"
    if self.project_options.get_option(self.name, "archive_remote_address") == "":
      self.archive_address = "http://ftp.pleiade.edf.fr/projets/F3C/EEM"

    self.compil_type = "specific"

    # TODO
    self.remote_type = "svn"
    self.root_repository = "https://noeyy727.noe.edf.fr"
    self.repository_name = "mfee/thyc"
    self.tag = self.version

    self.shell = "/bin/bash"
    self.specific_install_command = "mv " + self.get_version_for_archive() + "/* .; rm -Rf " + self.get_version_for_archive() + "; tar --exclude-vcs -f - -c * | tar -C $CURRENT_SOFTWARE_INSTALL_DIR -f - -x ;"
    self.post_install_commands.append("./install.sh ;")
    if self.get_version_for_archive() == "V2.11-rc1":
        self.post_install_commands.append("/bin/sed -i 's/V2.10/V2.11-rc1/g' ./config.sh")  # # On corrige le numéro de version qui est erroné!!

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return thycox_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return thycox_configuration_template.substitute(install_dir=install_dir, ordered_version=self.ordered_version)

# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from yamm.projects.emc2.software import EMC2Software
from yamm.projects.cocagne.softwares.cocagne import COCAGNE
from yamm.core.base import misc
import os
import string

software_name = "COCAGNE_EMC2_ODYSSEE"
cocagne_template = """
#------ cocagne ------
export COCAGNE_ROOT_DIR="%install_dir"
export PYTHON_VERSION="%python_version"
export %ld_library_path=${COCAGNE_ROOT_DIR}/lib:${%ld_library_path}
export PYTHONPATH=${COCAGNE_ROOT_DIR}/lib/python${PYTHON_VERSION}/site-packages/cocagne:${PYTHONPATH}
export PATH=${COCAGNE_ROOT_DIR}/bin:${PATH}
export DKTHM_DATA=/libraries/thermalhydraulic
export DKLIBS=/libraries/dklib
export DKROOT=${COCAGNE_SRC_DIR}
"""
cocagne_template = misc.PercentTemplate(cocagne_template)


cocagne_configuration_template = """
#---------- Cocagne pour EMC2_ODYSSEE -------------------
COCAGNE_ROOT_DIR="$install_dir"
COCAGNE_SRC_DIR="$src_dir"
ADD_TO_$ld_library_path: %(COCAGNE_ROOT_DIR)s/lib
ADD_TO_PYTHONPATH: %(COCAGNE_ROOT_DIR)s/lib/python$python_version/site-packages/cocagne
ADD_TO_PATH: %(COCAGNE_ROOT_DIR)s/bin
DKTHM_DATA: /libraries/thermalhydraulic
DKLIBS: /libraries/dklib
DKROOT: %(COCAGNE_SRC_DIR)s
"""
cocagne_configuration_template = string.Template(cocagne_configuration_template)


class COCAGNE_EMC2_ODYSSEE(EMC2Software, COCAGNE):

    def set_archive_params(self):
        self.archive_file_name = "cocagne-" + self.get_version_for_archive() + ".tar.gz"
        self.archive_type = "tar.gz"

    def init_variables(self):
        EMC2Software.init_variables(self)
        COCAGNE.init_variables(self)

    def init_target_directories(self):
        pass

    def user_variables_hook(self):
        COCAGNE.user_variables_hook(self)

    def init_dependency_list(self):
        EMC2Software.init_dependency_list(self)
        COCAGNE.init_dependency_list(self)

    def get_type(self):
        return "tool"

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return cocagne_template.substitute(install_dir=install_dir,
                                           ld_library_path=misc.get_ld_library_path(),
                                           python_version=self.python_version,
                                           src_dir=self.src_directory)

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return cocagne_configuration_template.substitute(install_dir=install_dir,
                                                         ld_library_path=misc.get_ld_library_path(),
                                                         python_version=self.python_version,
                                                         src_dir=self.src_directory)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Thomas LAUFFENBURGER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "FORMULAIRE"

FORMULAIRE_template = """
# ------ Formulaire------------
export FORMULAIRE_ROOT_DIR="%install_dir"
export PYTHONPATH=${FORMULAIRE_ROOT_DIR}:${PYTHONPATH}
"""
FORMULAIRE_template = misc.PercentTemplate(FORMULAIRE_template)

FORMULAIRE_configuration_template = """
# ------ Formulaire -------------
FORMULAIRE_ROOT_DIR="$install_dir"
ADD_TO_PYTHONPATH: %(FORMULAIRE_ROOT_DIR)s

"""
FORMULAIRE_configuration_template = string.Template(FORMULAIRE_configuration_template)

class FORMULAIRE(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name    = "formulaire-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type         = "tar.gz"
    
    # No proxy for FORMULAIRE
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "") 

    self.remote_type     = "git"
    self.repository_name = "formulaire"
    self.root_repository = "https://git.forge.pleiade.edf.fr/git"
    self.tag             = self.version
    
    self.compil_type     = "cmake"
    
    self.config_options += " -DCMAKE_INSTALL_PREFIX:PATH=${CURRENT_SOFTWARE_INSTALL_DIR}"
    self.config_options += " -DCOCAGNE_ROOT:PATH=${COCAGNE_EEM_ANDROMEDE_INSTALL_DIR}"
    pass

  def user_variables_hook(self):
    self.cmake_src_directory = os.path.join(self.src_directory, 'sources')

  def init_dependency_list(self):
    self.software_dependency_list = ["CMAKE","COCAGNE_EEM_ANDROMEDE","THYC"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    if dependency_name == "COCAGNE_EEM_ANDROMEDE":
      dependency_object.depend_of = ["install_path","python_path"]
    if dependency_name == "THYC":
      dependency_object.depend_of = ["python_path"]      
    return dependency_object

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return FORMULAIRE_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return FORMULAIRE_configuration_template.substitute(install_dir=install_dir)
  
  def get_extra_test_path_list(self):
    """
    This method returns a list of paths to the software extra tests.
    Define each path relative to the software install directory.
    """
    return ["test"]

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Thomas LAUFFENBURGER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "INTERFACE_MOTEUR"

INTERFACE_MOTEUR_template = """
# ------ Interface_Moteur------------
export INTERFACE_MOTEUR_ROOT_DIR="%install_dir"
export LD_LIBRARY_PATH=${INTERFACE_MOTEUR_ROOT_DIR}/lib:${LD_LIBRARY_PATH}
export PYTHONPATH=${INTERFACE_MOTEUR_ROOT_DIR}/lib/python%python_version/site-packages/api:${PYTHONPATH}
export PATH=$INTERFACE_MOTEUR_ROOT_DIR/bin:${PATH}
"""
INTERFACE_MOTEUR_template = misc.PercentTemplate(INTERFACE_MOTEUR_template)

INTERFACE_MOTEUR_configuration_template = """
# ------ Interface_Moteur -------------
INTERFACE_MOTEUR_ROOT_DIR="$install_dir"
ADD_TO_LD_LIBRARY_PATH: %(INTERFACE_MOTEUR_ROOT_DIR)s/lib
ADD_TO_PYTHONPATH: %(INTERFACE_MOTEUR_ROOT_DIR)s/lib/python$python_version/site-packages/api
ADD_TO_PATH: %(INTERFACE_MOTEUR_ROOT_DIR)s/bin
"""
INTERFACE_MOTEUR_configuration_template = string.Template(INTERFACE_MOTEUR_configuration_template)

class INTERFACE_MOTEUR(SalomeSoftware):

  def init_variables(self):
    libPythonInstallPath='lib/python{0}/site-packages/api'.format(self.python_version)
    SalomeSoftware.init_variables(self)

    self.archive_file_name = "interface_moteur-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    # No proxy for INTERFACE_MOTEUR
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "")

    self.remote_type = "git"
    self.repository_name = "interface_moteur"
    self.root_repository = "https://git.forge.pleiade.edf.fr/git"
    self.tag = self.version

    self.compil_type = "cmake"

    self.replaceStringInFilePostInstall("<version>", self.version_object.name, os.path.join(libPythonInstallPath,"ClusterConfiguration.json"))
    self.replaceStringInFilePostInstall("<projet>", "emc2", os.path.join(libPythonInstallPath,"ClusterConfiguration.json"))
    self.replaceStringInFilePostInstall("<Chemin_Install>", self.install_directory, "tests/UnitTests/tester.py")
    self.replaceStringInFilePostInstall("<Chemin_Install>", self.install_directory, "tests/UserTests/userTester.py")
    self.replaceStringInFilePostInstall("<Chemin_Install>", self.install_directory, "tests/IntegrationTests/tester.py")
    self.replaceStringInFilePostInstall("<Chemin_Install>", os.path.join(self.install_directory,libPythonInstallPath), os.path.join(libPythonInstallPath,"InterfaceTools.py"))
    self.replaceStringInFilePostInstall("<Chemin_Install>", os.path.join(self.install_directory,libPythonInstallPath), os.path.join(libPythonInstallPath,"RemoteLauncher.py"))
    pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["KERNEL","CONFIGURATION","YACS","PLUGIN_EMC2"]
    self.software_dependency_dict['exec'] = ["YACS", "KERNEL", "CPPREST_SDK", 'PHY2S', "PLUGIN_EMC2",
                                             "COCAGNE_EMC2_ODYSSEE", "C3THER_POUR_EMC2", "THYC"] # pour le fichier de config de phy2s (à sortir à terme)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CONFIGURATION":
      dependency_object.depend_of = ["install_path"]
      self.config_options += " -DCONFIGURATION_ROOT_DIR:PATH=$CONFIGURATION_INSTALL_DIR " 
    if dependency_name == "KERNEL":
      dependency_object.depend_of = ["install_path"]
      self.config_options +=  " -DKERNEL_ROOT_DIR:PATH=$KERNEL_INSTALL_DIR " 
    if dependency_name == "YACS":
      dependency_object.depend_of = ["ld_lib_path", "python_path","install_path"]
      self.config_options +=  " -DYACS_ROOT_DIR:PATH=$YACS_INSTALL_DIR " 
    if dependency_name == "CPPREST_SDK":
      dependency_object.depend_of = ["install_path", "ld_lib_path"]
    if dependency_name == "PHY2S":
      dependency_object.depend_of = ["python_path", "install_path"]
    if dependency_name == "COCAGNE_EMC2_ODYSSEE":
      dependency_object.depend_of = ["python_path", "install_path"]
    if dependency_name == "C3THER_POUR_EMC2":
      dependency_object.depend_of = ["python_path", "install_path"]
    if dependency_name == "THYC":
      dependency_object.depend_of = ["python_path", "install_path"]
    if dependency_name == "PLUGIN_EMC2":
      dependency_object.depend_of = ["install_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):    
    libPythonInstallPath='lib/python{0}/site-packages/api'.format(self.python_version)
    EMC2ConfigTemplatePath=os.path.join(libPythonInstallPath,"Phy2SConfigTemplate.xml")
    if dependency_name == 'PHY2S':
      self.replaceStringInFilePostInstall("<Chemin_Install_Phy2S>", '$PHY2S_INSTALL_DIR', EMC2ConfigTemplatePath)
    if dependency_name == 'COCAGNE_EMC2_ODYSSEE':
      self.replaceStringInFilePostInstall("<Chemin_Install_Cocagne>", '$COCAGNE_EMC2_ODYSSEE_INSTALL_DIR', EMC2ConfigTemplatePath)
      self.replaceStringInFilePostInstall("<Chemin_Install_Cocagne>", '$COCAGNE_EMC2_ODYSSEE_INSTALL_DIR', "tests/IntegrationTests/tester.py")
    if dependency_name == 'C3THER_POUR_EMC2':
      self.replaceStringInFilePostInstall("<Chemin_Install_C3Ther>", '$C3THER_POUR_EMC2_INSTALL_DIR', EMC2ConfigTemplatePath)
      self.replaceStringInFilePostInstall("<Chemin_Install_C3Ther>", '$C3THER_POUR_EMC2_INSTALL_DIR', "tests/IntegrationTests/tester.py")
    if dependency_name == 'THYC':
      self.replaceStringInFilePostInstall("<Chemin_Install_Thyc>", '$THYC_INSTALL_DIR', EMC2ConfigTemplatePath)
      self.replaceStringInFilePostInstall("<Chemin_Install_Thyc>", '$THYC_INSTALL_DIR', "tests/IntegrationTests/tester.py")
    if dependency_name == 'PLUGIN_EMC2':
      self.replaceStringInFilePostInstall("<Chemin_Install_Plugin_EMC2>", '$PLUGIN_EMC2_INSTALL_DIR', os.path.join(libPythonInstallPath,"FileEMC2.py"))
      self.replaceStringInFilePostInstall("<Chemin_Install_Plugin_EMC2>", '$PLUGIN_EMC2_INSTALL_DIR', os.path.join(libPythonInstallPath,"RemoteLauncher.py"))
    pass

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return INTERFACE_MOTEUR_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return INTERFACE_MOTEUR_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_extra_test_path_list(self):
    """
    This method returns a list of paths to the software extra tests.
    Define each path relative to the software install directory.
    """
    return ["tests"]

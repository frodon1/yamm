#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PHY2S"

PHY2S_template = """
# ------ Phy2S------------
export PHY2S_ROOT_DIR="%install_dir"
export PYTHONPATH=${PHY2S_ROOT_DIR}:${PHY2S_ROOT_DIR}/%subdir:${PHY2S_ROOT_DIR}/fakeOCSForTest:${PYTHONPATH}
"""
PHY2S_template = misc.PercentTemplate(PHY2S_template)

PHY2S_configuration_template = """
# ------ Phy2S -------------
PHY2S_ROOT_DIR="$install_dir"
ADD_TO_PYTHONPATH: %(PHY2S_ROOT_DIR)s
ADD_TO_PYTHONPATH: %(PHY2S_ROOT_DIR)s/$subdir
ADD_TO_PYTHONPATH: %(PHY2S_ROOT_DIR)s/fakeOCSForTest
"""
PHY2S_configuration_template = string.Template(PHY2S_configuration_template)

class PHY2S(SalomeSoftware):
  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, software_name, version, verbose)
    self.subdir = 'tool'

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.subdir = 'src'

    self.archive_file_name    = "phy2s-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type         = "tar.gz"
    
    # No proxy for PHY2S
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "") 

    self.remote_type     = "git"
    self.repository_name = "phy2s"
    self.root_repository = "https://git.forge.pleiade.edf.fr/git"
    self.tag             = self.version
    
    self.compil_type     = "cmake"

#     if self.ordered_version < '2.0.1':
    self.replaceStringInFilePostInstall("<Chemin_Install>", self.install_directory, "tests/ToolEngineLev1/tester.py")
    self.replaceStringInFilePostInstall("<Chemin_Install>",self.install_directory,"tests/ToolUserLev0/tester.py")
    pass

  def init_dependency_list(self):
    self.software_dependency_list = ["YACS", "JEDI", "SCRAT"]
    
  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "YACS":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "JEDI":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "SCRAT":
      dependency_object.depend_of = ["python_path"]
    return dependency_object
  
  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return PHY2S_template.substitute(install_dir=install_dir,
                                     subdir=self.subdir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return PHY2S_configuration_template.substitute(install_dir=install_dir,
                                                   subdir=self.subdir)
  
  def get_extra_test_path_list(self):
    """
    This method returns a list of paths to the software extra tests.
    Define each path relative to the software install directory.
    """
    return ["tests"]

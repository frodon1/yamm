#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "SCRAT"

SCRAT_template = """
# ------ Scrat------------
export SCRAT_ROOT_DIR="%install_dir"
export PYTHONPATH=${SCRAT_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
SCRAT_template = misc.PercentTemplate(SCRAT_template)

SCRAT_configuration_template = """
# ------ Scrat -------------
SCRAT_ROOT_DIR="$install_dir"
ADD_TO_PYTHONPATH: %(SCRAT_ROOT_DIR)s/lib/python$python_version/site-packages
"""
SCRAT_configuration_template = string.Template(SCRAT_configuration_template)


class SCRAT(SalomeSoftware):

    def init_variables(self):
        SalomeSoftware.init_variables(self)

        self.archive_file_name = "scrat-" + self.get_version_for_archive() + ".tar.gz"
        self.archive_type = "tar.gz"

        # No proxy for SCRAT
        if self.project_options.get_option(self.name, "proxy_server"):
            self.project_options.set_software_option(self.name, "proxy_server", "")

        self.remote_type = "git"
        self.repository_name = "scrat"
        self.root_repository = "https://git.forge.pleiade.edf.fr/git"
        self.tag = self.version

        self.compil_type = "cmake"

    def get_type(self):
        return "tool"

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return SCRAT_template.substitute(install_dir=install_dir, python_version=self.python_version)

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return SCRAT_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

    def get_extra_test_path_list(self):
        """
        This method returns a list of paths to the software extra tests.
        Define each path relative to the software install directory.
        """
        return ["tests"]

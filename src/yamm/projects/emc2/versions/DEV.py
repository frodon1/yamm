#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.salome.versions.DEV import DEV as SALOME_DEV

version_name = "DEV"
version_value = "master"


class DEV(SALOME_DEV):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        SALOME_DEV.__init__(self, name, verbose, flavour)
        self.accepted_flavours = ["EMC2 PRODUCTION", "EMC2 YACS GUI ONLY", 
                                  "EMC2 PRODUCTION PORTHOS", "EMC2 PRODUCTION EOLE"]
        
        self.minimal_soft_list =  ["YACS", "H5PY", "SIC", "THYCOX", "COUPLEUR_T2C", "INTERFACE_MOTEUR"]
        self.minimal_soft_list += ["SPHINXCONTRIB_NAPOLEON", "NUMPYDOC", "TABULATE"] # formulaire
        self.minimal_soft_list += ["SIDERAL", "GRADYN_8GROUPES", "GRADYN_6GROUPES"] # formulaire 7N
        pass

    def get_salome_version(self):
        return version_value

    def configure_softwares(self):
        SALOME_DEV.configure_softwares(self)
        self.add_debian_depends(['modan-1.1', 'medcoupling-8.2.0', 'med-3.2serial'])

        # COCAGNE prerequisite
        # EIGEN 3.2 does not support vlx and is buggy
        self.add_software("EIGEN", "09a8e2186610", "3.3")  # 3.3alpha1
        self.add_software("XDATA", "0.9.11")
        self.add_software("MODAN", "1.1")
        self.add_software("TH1D", "1.1-rc3-EDFp1", "1.1")

        # THYC prerequisite
        # Natifs Calibre 9
        self.add_software("INTEL", "2015.5.223", groups=["base"])
        self.add_software("SIC", "V5.2-rc1")
        self.add_software("THYCOX", "V2.11-rc1", "2.11")
        self.add_software("COUPLEUR_T2C", "rev8")

        # FORMULAIRE prerequisite
        self.add_software("SPHINX", "1.3.5")
        self.add_software("SPHINXCONTRIB_NAPOLEON", "0.4.4")
        self.add_software("TABULATE", "0.7.7")
                
        # SEPTEN prerequisite
        # TODO ne plus les recompiler!!!
        self.add_software("GRADYN_8GROUPES", "2.2.99")
        self.add_software("GRADYN_6GROUPES", "2.2.0")
        self.add_software("SIDERAL", "2.4")
        
        # Specifique Environnement d Etude Metier
        self.add_software("PHY2S", "master")
        self.add_software("SCRAT", "master", "V1.0")
        self.add_software("INTERFACE_MOTEUR", "master")
        self.add_software("PLUGIN_EMC2", "master")
        self.add_software("JEDI", "git_tag/v0.10.0", "0.10.0")
        self.add_software("C3THER_POUR_EMC2", "master")
        self.add_software("COCAGNE_EMC2_ODYSSEE", "git_tag/COCAGNE_02_04_00_RC2", "2.4.0")
        self.add_software("THYC", "V5.2.6", "5.2.6")
        
        # Salome
        self.add_software("KERNEL", "master")
        self.add_software("YACS", "master")
        self.add_software("MEDFICHIER", "3.2.1", "3.2.1", groups=["base"], min_version='3.2')
        self.add_software('MEDCOUPLING', 'git_tag/V8_2_0', 'V8_2_0')
        self.remove_software("METIS", remove_deps=True)
        self.remove_software("SCOTCH", remove_deps=True)

        if "PORTHOS" in self.flavour:  # Logiciels NON DISPO dans SALOME_DEV  mais à rajouter pour Porthos
            # self.add_software("RST2PDF", "0.93") # c'etait pour AVALANCHE
            self.add_software("CHARDET", "2.3.0")  # Version identique a Calibre 9 et non dispo sur Porthos
            self.add_software("SETUPTOOLS", "5.5.1")  # Version identique a Calibre 9 et non dispo sur Porthos

        self.set_software_minimal_list(self.minimal_soft_list)

        if "EOLE" in self.flavour:
            self.remove_software("TCL", remove_deps=True)  # sinon recompile python...
            self.remove_software("TK", remove_deps=True)  # sinon recompile python...

        if "PRODUCTION" in self.flavour:
            self.remove_software("GUI", remove_deps=True)
            self.remove_software("PYQT", remove_deps=True)  # Tiré par XDATA...
            self.remove_software("QT", remove_deps=True)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.salome.versions.V8_0_0 import V8_0_0 as SALOME_V8_0_0

version_name = "V1_0"
version_value = "1.0"


class V1_0(SALOME_V8_0_0):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        SALOME_V8_0_0.__init__(self, name, verbose, flavour)
        self.accepted_flavours = []

    def configure_softwares(self):
        SALOME_V8_0_0.configure_softwares(self)
        # Pour COCAGNE les manquant :
        # Attention l'ordre est important sinon on perd KERNEL...
        self.add_software("XDATA", "0.9.11")
        minimal_soft_list = ["JOBMANAGER", "YACS", "AVALANCHE", "COUPLEUR_T2C",
                             "INTERFACE_EEM", "H5PY", "PYQT", "FORMULAIRE_F3C_TEST",
                             "SPHINXCONTRIB_NAPOLEON", "XDATA"]
        self.set_software_minimal_list(minimal_soft_list)

        # Attention l'ordre est important sinon on garde QT...
        self.remove_software("QSCINTILLA", remove_deps=True)
        self.remove_software("PARAVIEW", remove_deps=True)
        # self.remove_software("GUI", remove_deps=True)

        self.remove_software("TCL")
        self.remove_software("TK")
        self.remove_software("PYTHON")
        # self.remove_software("SIP")
        self.remove_software("CMAKE")
        self.remove_software("HDF5")
        self.remove_software("BOOST")
        self.remove_software("LAPACK")
        self.remove_software("SWIG_FOR_COCAGNE")
        self.remove_software("SWIG")
        self.remove_software("NUMPY")
        self.remove_software("GRAPHVIZ")
        self.remove_software("DOXYGEN")
        self.remove_software("DOCUTILS")
        self.remove_software("LIBXML2")
        self.remove_software("SETUPTOOLS")
        self.remove_software("PYGMENTS")
        self.remove_software("JINJA")
        self.remove_software("SPHINX")
        self.remove_software("CPPUNIT")
        self.remove_software("TBB")
        self.remove_software("GL2PS")
        self.remove_software("FREETYPE")
        self.remove_software("FREEIMAGE")
        # self.remove_software("QT")
        self.remove_software("SCIPY")
        self.remove_software("MATPLOTLIB")
        self.remove_software("CYTHON")

        # self.add_software("PYQT", "5.4", groups=["base"])
        self.add_software("EIGEN", "09a8e2186610", "3.3")  # 3.3alpha1
        self.add_software("MEDFICHIER", "3.1.0", groups=["base"])

        # Natifs Calibre 9
        self.add_software("INTEL", "2015.5.223", groups=["base"])

        # Specifique Environnement d Etude Metier
        self.add_software("SPHINX", "1.3.5")
        self.add_software("POCKETS", "0.3")
        self.add_software("SPHINXCONTRIB_NAPOLEON", "0.4.4")
        self.add_software("NUMPYDOC", "0.6.0")
        self.add_software("PMW", "2.0.0")
        self.add_software("AVALANCHE", "git_tag/VERSION_0.4.2_EEM", "0.4.2")
        self.add_software("COCAGNE_EEM_ANDROMEDE", "git_tag/COCAGNE_02_03_00", "2.3.0")
        self.add_software("PHY2S", "git_tag/V1_0_0rc2", "1.0.0")
        self.add_software("THYC", "V5.2-rc2", "5.2")
        self.add_software("SIC", "V5.2-rc1")
        self.add_software("THYCOX", "V2.11-rc1", "2.11")
        self.add_software("SIDERAL", "2.3")
        self.add_software("COUPLEUR_T2C", "rev8")
        self.add_software("VALIDATE", "1.0.1")
        self.add_software("INTERFACE_EEM", "git_tag/V1_0_0rc1", "1.0.0")
        self.add_software("C3THER_POUR_EEM", "git_tag/V1_0_0rc1", "1.0.0")
        self.add_software("MAKO", "1.0.0")
        self.add_software("JEDI", "git_tag/v0.9.0", "0.9.0")
        self.add_software("FORMULAIRE_F3C_TEST", "git_tag/V1_0_0rc3", "1.0.0")

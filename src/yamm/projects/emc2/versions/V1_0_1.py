#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.salome.versions.V8_2_0 import V8_2_0 as SALOME_V8_2_0

version_name = "V1_0_1"
version_value = "1.0.1"


class V1_0_1(SALOME_V8_2_0):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        SALOME_V8_2_0.__init__(self, name, verbose, flavour)
        self.accepted_flavours = ["EEM PRODUCTION", "EEM YACS GUI ONLY",
                                  "SALOME COEUR", "EEM PRODUCTION PORTHOS",
                                  "SALOME COEUR PORTHOS"]

    def configure_softwares(self):
        SALOME_V8_2_0.configure_softwares(self)
        self.add_software("KERNEL", "git_tag/accfa6fe")  # 8.2.0 + Fix CORBA stress tests
        self.add_software("YACS", "git_tag/be02895b")  # 8.2.0 + Fix issue 11667 about load state issue
        self.add_software("LIBBATCH", "git_tag/4ad209d8")  # 2.3.1 + Fix pour 'module' sur cluster
        self.add_software("QSCINTILLA", "2.9.3")
        self.add_software("XDATA", "0.9.11")
        self.add_software("MODULE_COCAGNE", "master")
        self.add_software("SALOMON", "master")
        self.add_software("CMAKE", "3.3.0")  # Version compatible Paraview et Cocagne
        self.add_software("SETUPTOOLS", "5.5.1")  # Version identique a Calibre 9 et non dispo sur Porthos

        minimal_soft_list = ["YACS", "H5PY", "XDATA"]
        if "SALOME COEUR" in self.flavour:
            minimal_soft_list += ["QSCINTILLA", "PARAVIS", "ADAO", "JOBMANAGER", "OPENTURNS"]
            minimal_soft_list += ["MODULE_COCAGNE", "SALOMON"]

        self.set_software_minimal_list(minimal_soft_list)

        if "SALOME COEUR" in self.flavour:
            self.remove_software("SMESH", remove_deps=True)

        if "PRODUCTION" in self.flavour:
            self.remove_software("GUI", remove_deps=True)
            self.remove_software("PYQT", remove_deps=True)  # Tiré par XDATA...

        # Attention l'ordre est important sinon on garde QT...
        if "SALOME COEUR" not in self.flavour:
            self.remove_software("QSCINTILLA", remove_deps=True)

        if "PORTHOS" in self.flavour:  # Logiciels NON DISPO dans SALOME_DEV  mais à rajouter pour Porthos
            self.add_software("RST2PDF", "0.93")
            self.add_software("CHARDET", "2.3.0")  # Version identique a Calibre 9 et non dispo sur Porthos

        self.add_software("EIGEN", "09a8e2186610", "3.3")  # 3.3alpha1
        self.add_software("MEDFICHIER", "3.2.1")  # Pour avoir le fix detection HDF5 sur cluster

        # Natifs Calibre 9
        self.add_software("INTEL", "2015.5.223", groups=["base"])

        # Specifique Environnement d Etude Metier
        self.add_software("SPHINX", "1.3.5")
        # self.add_software("POCKETS", "0.3")
        self.add_software("SPHINXCONTRIB_NAPOLEON", "0.4.4")
        self.add_software("NUMPYDOC", "0.6.0")
        self.add_software("PMW", "2.0.0")
        self.add_software("AVALANCHE", "git_tag/VERSION_0.7.0", "0.7.0")
        self.add_software("COCAGNE_EEM_ANDROMEDE", "git_tag/COCAGNE_02_03_01-02", "2.3.1-2")
        self.add_software("PHY2S", "git_tag/V1_0_1-1")
        self.add_software("THYC", "V5.2.6", "5.2.6")
        self.add_software("SIC", "V5.2-rc1")
        self.add_software("THYCOX", "V2.11-rc1", "2.11")
        self.add_software("SIDERAL", "2.4")
        self.add_software("GRADYN_8GROUPES", "2.2.99")
        self.add_software("GRADYN_6GROUPES", "2.2.0-2.2.0.1")
        self.add_software("COUPLEUR_T2C", "rev8")
        # self.add_software("VALIDATE", "1.0.1") installe sur EOLE
        self.add_software("INTERFACE_EEM", "git_tag/V1_0_1-3", "1.0.1-3")
        self.add_software("C3THER_POUR_EEM", "git_tag/V1_0_1", "1.0.1")
        # self.add_software("MAKO", "1.0.0") installe sur EOLE
        self.add_software("JEDI", "git_tag/v0.9.0", "0.9.0")
        self.add_software("FORMULAIRE_F3C_TEST", "git_tag/V1_0_1", "1.0.1")
        self.add_software("FORMULAIRE", "master")

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function
import os

from yamm.core.framework.software import FrameworkSoftware
from yamm.core.base import misc

def get_ftp_server():
  return os.path.join(misc.get_ftp_server(), 'YAMM/Public/KDE')

class KdeSoftware(FrameworkSoftware):

  def default_values_hook(self):
    self.env_files.append(self.project_options.get_global_option("kde_env_file"))
    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "install")
    self.install_dir = "keep"

    if self.get_type() == "module":
      self.archive_address = os.path.join(get_ftp_server(),self.version_object.name)
      if self.version > "4.8":
        self.archive_type = "tar.xz"
      else:
        self.archive_type = "tar.bz2"
    elif self.get_type() == "prerequisite":
      self.archive_address = os.path.join(get_ftp_server(),"prerequisites")
    else:
      print("You are not a good software sir %s" % self.name)


#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
from yamm.core.base import misc
from yamm.projects.kde.software import KdeSoftware

software_name = "GOOGLEGADGETS"

class GOOGLEGADGETS(KdeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    KdeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.archive_type         = "tar.bz2"
    self.compil_type          = "autoconf"
    self.archive_file_name    = "google-gadgets-for-linux-" + self.version + ".tar.bz2"
    self.config_options       = "--disable-werror"

  def get_type(self):
    return "prerequisite"

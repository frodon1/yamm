#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
from yamm.core.base import misc
from yamm.projects.kde.software import KdeSoftware
from yamm.core.engine.dependency import Dependency

software_name = "KDEWORKSPACE"

class KDEWORKSPACE(KdeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    KdeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.compil_type          = "cmake"
    self.config_options       = " -DCMAKE_BUILD_TYPE=Release "
    self.archive_file_name    = "kde-workspace-" + self.version + "." + self.archive_type

  def init_dependency_list(self):
    self.software_dependency_list = ["KACTIVITIES", "GOOGLEGADGETS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "KACTIVITIES":
      dependency_object = Dependency(name="KACTIVITIES")
    elif dependency_name == "GOOGLEGADGETS":
      dependency_object = Dependency(name="GOOGLEGADGETS")
    return dependency_object

  def get_type(self):
    return "module"

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.kde.software import KdeSoftware
from yamm.core.engine.dependency import Dependency

software_name = "KDEPREREQUISITES"

class KDEPREREQUISITES(KdeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    KdeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.compil_type          = "fake"

  def init_dependency_list(self):
    self.software_dependency_list  = ["LIBDBUSMENUQT", "AUTOMOC", "ATTICA"]
    self.software_dependency_list += ["POLKITQT", "STRIGI", "SOPRANO"]
    self.software_dependency_list += ["AKONADI", "CAGIBI", "LIBQZEITGEIST"]
    self.software_dependency_list += ["PHONON", "PHONON_GSTREAMER", "PRISON", "POPPLER"]
    self.software_dependency_list += ["SHARED_DESKTOP_ONTOLOGIES", "GRANTLEE", "GOOGLEGADGETS"]
    self.software_dependency_list += ["QJSON", "QIMAGEBLITZ"]
    self.software_dependency_list += ["OXYGENICONS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "LIBDBUSMENUQT":
      dependency_object = Dependency(name="LIBDBUSMENUQT")
    elif dependency_name == "AUTOMOC":
      dependency_object = Dependency(name="AUTOMOC")
    elif dependency_name == "ATTICA":
      dependency_object = Dependency(name="ATTICA")
    elif dependency_name == "POLKITQT":
      dependency_object = Dependency(name="POLKITQT")
    elif dependency_name == "STRIGI":
      dependency_object = Dependency(name="STRIGI")
    elif dependency_name == "SOPRANO":
      dependency_object = Dependency(name="SOPRANO")
    elif dependency_name == "AKONADI":
      dependency_object = Dependency(name="AKONADI")
    elif dependency_name == "CAGIBI":
      dependency_object = Dependency(name="CAGIBI")
    elif dependency_name == "LIBQZEITGEIST":
      dependency_object = Dependency(name="LIBQZEITGEIST")
    elif dependency_name == "PHONON":
      dependency_object = Dependency(name="PHONON")
    elif dependency_name == "PHONON_GSTREAMER":
      dependency_object = Dependency(name="PHONON_GSTREAMER")
    elif dependency_name == "PRISON":
      dependency_object = Dependency(name="PRISON")
    elif dependency_name == "POPPLER":
      dependency_object = Dependency(name="POPPLER")
    elif dependency_name == "SHARED_DESKTOP_ONTOLOGIES":
      dependency_object = Dependency(name="SHARED_DESKTOP_ONTOLOGIES")
    elif dependency_name == "GRANTLEE":
      dependency_object = Dependency(name="GRANTLEE")
    elif dependency_name == "GOOGLEGADGETS":
      dependency_object = Dependency(name="GOOGLEGADGETS")
    elif dependency_name == "QJSON":
      dependency_object = Dependency(name="QJSON")
    elif dependency_name == "QIMAGEBLITZ":
      dependency_object = Dependency(name="QIMAGEBLITZ")
    elif dependency_name == "OXYGENICONS":
      dependency_object = Dependency(name="OXYGENICONS")
    return dependency_object

  def get_type(self):
    return "module"

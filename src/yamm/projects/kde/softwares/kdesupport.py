#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.kde.software import KdeSoftware
from yamm.core.engine.dependency import Dependency

software_name = "KDESUPPORT"

class KDESUPPORT(KdeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    KdeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):

    #self.root_repository = "svn://anonsvn.kde.org/home/kde/trunk/"
    #self.repository_name = "kdesupport"
    #self.remote_type     = "svn"
    #self.tag             = self.version

    self.archive_address      = "fake"
    self.archive_file_name    = "kde-support-" + self.version + ".tar.bz2"

    self.archive_type         = "tar.bz2"
    self.compil_type          = "cmake"
    self.config_options       = " -DCMAKE_BUILD_TYPE=Release "

  def init_dependency_list(self):
    self.software_dependency_list  = ["LIBDBUSMENUQT", "AUTOMOC", "ATTICA"]
    self.software_dependency_list += ["POLKITQT", "STRIGI", "SOPRANO"]
    self.software_dependency_list += ["AKONADI", "CAGIBI", "LIBQZEITGEIST"]
    self.software_dependency_list += ["PHONON", "PHONON_GSTREAMER", "PRISON", "POPPLER"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "LIBDBUSMENUQT":
      dependency_object = Dependency(name="LIBDBUSMENUQT")
    elif dependency_name == "AUTOMOC":
      dependency_object = Dependency(name="AUTOMOC")
    elif dependency_name == "ATTICA":
      dependency_object = Dependency(name="ATTICA")
    elif dependency_name == "POLKITQT":
      dependency_object = Dependency(name="POLKITQT")
    elif dependency_name == "STRIGI":
      dependency_object = Dependency(name="STRIGI")
    elif dependency_name == "SOPRANO":
      dependency_object = Dependency(name="SOPRANO")
    elif dependency_name == "AKONADI":
      dependency_object = Dependency(name="AKONADI")
    elif dependency_name == "CAGIBI":
      dependency_object = Dependency(name="CAGIBI")
    elif dependency_name == "LIBQZEITGEIST":
      dependency_object = Dependency(name="LIBQZEITGEIST")
    elif dependency_name == "PHONON":
      dependency_object = Dependency(name="PHONON")
    elif dependency_name == "PHONON_GSTREAMER":
      dependency_object = Dependency(name="PHONON_GSTREAMER")
    elif dependency_name == "PRISON":
      dependency_object = Dependency(name="PRISON")
    elif dependency_name == "POPPLER":
      dependency_object = Dependency(name="POPPLER")
    return dependency_object

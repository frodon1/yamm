#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.projects.kde.software import KdeSoftware
from yamm.core.engine.dependency import Dependency

software_name = "PHONON"

class PHONON(KdeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    KdeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    self.qt_in_project = False

  def init_variables(self):
    self.archive_type         = "tar.xz"
    self.compil_type          = "cmake"
    self.config_options       = " -DCMAKE_BUILD_TYPE=Release "
    self.archive_file_name    = "phonon-" + self.version + ".tar.xz"

    if self.qt_in_project:
      for software in self.project_softwares:
        if software.name == "QT":
          qt_executor_software_name = software.executor_software_name
      self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), qt_executor_software_name, "install")

  def init_dependency_list(self):
    self.software_dependency_list = ["QT", "LIBQZEITGEIST"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "QT":
      dependency_object = Dependency(name="QT",
                                     depend_of=["path", "ld_lib_path"])
      self.qt_in_project = True
    elif dependency_name == "LIBQZEITGEIST":
      dependency_object = Dependency(name="LIBQZEITGEIST",
                                     depend_of=["path", "ld_lib_path"])
    return dependency_object

  def get_type(self):
    return "prerequisite"

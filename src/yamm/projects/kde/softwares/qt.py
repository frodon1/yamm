#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
from yamm.core.base import misc
from yamm.core.framework.software import FrameworkSoftware

software_name = "QT"

qt_template = """
# ------ Qt -------------
export QTDIR=%install_dir
export QTDIRINSTALL=%install_dir
export PATH=${QTDIRINSTALL}/bin:$PATH
export QT_PLUGIN_PATH=${QTDIRINSTALL}/plugins:$QT_PLUGIN_PATH
export %ld_library_path=${QTDIRINSTALL}/lib:${%ld_library_path}
"""
qt_template = misc.PercentTemplate(qt_template)

class QT(FrameworkSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.src_directory     = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), self.executor_software_name, "src")
    self.build_directory   = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), self.executor_software_name, "build")
    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), self.executor_software_name, "install")

    self.archive_file_name    = "qt-" + self.version + ".tar.bz2"

    self.archive_type         = "tar.bz2"
    self.compil_type          = "autoconf"

    self.config_options      = " -opensource -confirm-license -no-separate-debug-info "
    self.config_options     += " -fast -system-zlib -system-libpng -system-libjpeg -dbus -webkit -plugin-sql-mysql "
    self.config_options     += " -nomake examples -nomake demos -no-phonon "

  def get_env_str(self):
    return qt_template.substitute(install_dir=self.install_directory, ld_library_path=misc.get_ld_library_path())

  def get_type(self):
    return "prerequisite"

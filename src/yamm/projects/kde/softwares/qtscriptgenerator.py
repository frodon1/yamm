#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.projects.kde.software import KdeSoftware
from yamm.core.engine.dependency import Dependency

software_name = "QTSCRIPTGENERATOR"

class QTSCRIPTGENERATOR(KdeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    KdeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.archive_type         = "tar.gz"
    self.compil_type          = "qmake"
    self.pro_file             = "generator/generator.pro"
    self.archive_address      = "fake"
    self.archive_file_name    = "qtscriptgenerator-src-" + self.version + ".tar.gz"

    self.build_directory = os.path.join(self.src_directory, "generator")
    self.build_dir       = "keep_or_create"
    self.pre_install_commands = ["./generator"]
    self.can_delete_src  = False

  def init_dependency_list(self):
    self.software_dependency_list = ["QT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "QT":
      dependency_object = Dependency(name="QT")
    return dependency_object

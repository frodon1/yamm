#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.projects.kde.software import KdeSoftware
from yamm.core.engine.dependency import Dependency

software_name = "QTSCRIPTGENERATORPLUGIN"

class QTSCRIPTGENERATORPLUGIN(KdeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    KdeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    self.real_src_directory   = ""
    self.real_build_directory = ""
    
  def init_variables(self):

    if self.real_src_directory != "":
      self.src_directory   = self.real_src_directory
      self.build_directory = os.path.join(self.src_directory, "qtbindings")
      self.src_dir         = "keep"
      self.build_dir       = "keep_or_create"
      self.src_decompress_dir = "keep"

    self.archive_type         = "tar.gz"
    self.compil_type          = "qmake"
    self.pro_file             = "qtbindings/qtbindings.pro"
    self.archive_address      = "fake"
    self.archive_file_name    = "qtscriptgenerator-src-" + self.version + ".tar.gz"

  def init_dependency_list(self):
    self.software_dependency_list = ["QTSCRIPTGENERATOR"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "QTSCRIPTGENERATOR":
      dependency_object = Dependency(name="QTSCRIPTGENERATOR")
      for software in self.project_softwares:
        if software.name == "QTSCRIPTGENERATOR":
          self.real_src_directory   = software.src_directory
          self.real_build_directory = software.build_directory
    return dependency_object


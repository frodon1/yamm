#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.core.framework.version import FrameworkVersion

version_name = "KDE_4_8_1"

class KDE_4_8_1(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):

    # KDE PREREQUIS
    self.add_software("QT",                        "4.8.0")
    self.add_software("LIBDBUSMENUQT",             "0.9.0")
    self.add_software("AUTOMOC",                   "0.9.88")
    self.add_software("ATTICA",                    "0.3.0")
    self.add_software("EXIV2",                     "0.22")
    self.add_software("POLKITQT",                  "0.103.0")
    self.add_software("STRIGI",                    "0.7.7")
    self.add_software("SOPRANO",                   "2.7.4")
    self.add_software("CAGIBI",                    "0.2.0")
    self.add_software("LIBQZEITGEIST",             "0.8.0")
    self.add_software("PHONON",                    "4.6.0")
    self.add_software("PHONON_GSTREAMER",          "4.6.0")
    self.add_software("PRISON",                    "1.0")
    self.add_software("POPPLER",                   "0.18.4")
    self.add_software("SHARED_DESKTOP_ONTOLOGIES", "0.9.0")
    self.add_software("GRANTLEE",                  "4.8.0")
    self.add_software("GOOGLEGADGETS",             "0.11.2-debian")
    self.add_software("QJSON",                     "0.7.1")
    self.add_software("QIMAGEBLITZ",               "0.0.6")
    self.add_software("AKONADI",                   "1.7.0")

    # NEW in 4_8_1
    self.add_software("OXYGENICONS",               "4.8.1")
    self.add_software("KDEPREREQUISITES",          "4.8.1")

    # KDE DEV PLATFORM
    self.add_software("KDELIBS",     "4.8.1")
    self.add_software("LIBKTORRENT", "1.1.3")
    self.add_software("KDEPIMLIBS",  "4.8.1")
    self.add_software("KDERUNTIME",  "4.8.1")
    self.add_software("KDEBASEAPPS", "4.8.1")
    self.add_software("KATE",        "4.8.1")
    self.add_software("KONSOLE",     "4.8.1")
    self.add_software("KDESDK",      "4.8.1")

    # KDE WORKSPACE
    self.add_software("KACTIVITIES",  "4.8.1")
    self.add_software("KDEWORKSPACE", "4.8.1")
    self.add_software("LIBKEXIV2",    "4.8.1")
    self.add_software("LIBKIPI",      "4.8.1")

    # KDE APPLICATIONS
    self.add_software("KDEBASEARTWORK",  "4.8.1")
    self.add_software("KDEARTWORK",      "4.8.1")
    self.add_software("KDEMULTIMEDIA",   "4.8.1")
    self.add_software("KDENETWORK",      "4.8.1")
    self.add_software("MARBLE",          "4.8.1")
    self.add_software("KDEPLASMAADDONS", "4.8.1")
    self.add_software("KDEPIM",          "4.8.1")
    self.add_software("KDEPIMRUNTIME",   "4.8.1")
    self.add_software("OKULAR",          "4.8.1")
    self.add_software("GWENVIEW",        "4.8.1")

    # OTHER APPLICATIONS
    self.add_software("AMAROK", "2.5.0")

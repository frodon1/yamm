#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014, 2015 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from builtins import object


import os
import tempfile

from yamm.core.base import misc


class CreateAppliTask(object):
  """
    This task is called to create a MAP application
  """

  def __init__(self, appli_directory):
    self.appli_directory = appli_directory
    self.yamm_appli_log_dir = os.path.join(self.appli_directory, '.yamm')
  #

  def prepare_work(self, software_config, space, command_log):
    self.command_log = command_log
    self.space = space
    return ""
  #

  def execute(self, dependency_command, executor, current_software):
    self.command_log.print_begin_work("Create application", space=self.space)

    def __get_softwares_by_type(software_type):
      typed_softwares = []
      for software in executor.sorted_executor_softwares:
        framework_software = software.framework_software
        if framework_software.get_type() == software_type:
          typed_softwares.append(framework_software)
        if software == current_software:
          break
      return typed_softwares
    #
    prerequisites_softwares = __get_softwares_by_type("prerequisites")
    library_softwares = __get_softwares_by_type("library")
    components_softwares = __get_softwares_by_type("components")
    composites_softwares = __get_softwares_by_type("composites")
    tests_softwares = __get_softwares_by_type("tests")

    max_prerequisite_name_length = max([len(x.name) for x in prerequisites_softwares])
    max_library_name_length = max([len(x.name) for x in library_softwares])
    max_component_name_length = max([len(x.name) for x in components_softwares])
    max_composite_name_length = max([len(x.name) for x in composites_softwares])
    max_test_name_length = max([len(x.name) for x in tests_softwares])
    max_software_name_length = max(max_prerequisite_name_length, max_library_name_length, max_component_name_length, max_composite_name_length, max_test_name_length)

    # Create appli directory
    if not os.path.isdir(self.yamm_appli_log_dir):
      try:
        os.makedirs(self.yamm_appli_log_dir, 0o755)
      except:
        return "Error in creating application directory: %s" % self.yamm_appli_log_dir

    software_types = ["prerequisite", "library", "component", "composite"]
    software_type_name_max_length = max([len(x) for x in software_types])

    # Check if we have a KERNEL
    mapresources_software = None
    mapengine_software = None
    for soft in library_softwares:
      if soft.name == "MAPRESOURCES":
        mapresources_software = soft
      elif soft.name == "MAPENGINE":
        mapengine_software = soft
    if not mapresources_software:
      return "Cannot create a MAP application without MAPRESOURCES, please add MAPRESOURCES to your configuration"
    if not mapengine_software:
      return "Cannot create a MAP application without MAPENGINE, please add MAPENGINE to your configuration"

    def __softwares_config_text(section_title, software_type, softwares):
      msg = "  <%s>\n" % section_title
      for software in softwares:
        nbspace1 = software_type_name_max_length - len(software_type) + 1
        nbspace2 = max_software_name_length - len(software.name) + 1
        msg += "    <%s%sname=\"%s\"%spath=\"%s\"/>\n" % (software_type, nbspace1 * " ", software.name, nbspace2 * " ", software.install_directory)
      msg += "  </%s>\n" % section_title
      return msg
    #

    # Create MAP Application config file
    (fd, config_appli_filename) = tempfile.mkstemp(suffix=".xml", prefix="config_map_")
    with open(config_appli_filename, "w") as config_appli_file:
      os.close(fd)
      config_appli_file.write("<application>\n")
      config_appli_file.write("  <map_prerequisites path=\"" + executor.options.get_global_option("map_prerequisites_env_file") + "\"/>\n")
      config_appli_file.write(__softwares_config_text("prerequisites", "prerequisite", prerequisites_softwares))
      config_appli_file.write(__softwares_config_text("libraries", "library", library_softwares))
      config_appli_file.write(__softwares_config_text("components", "component", components_softwares))
      config_appli_file.write(__softwares_config_text("composites", "composite", composites_softwares))
      config_appli_file.write(__softwares_config_text("tests", "test", tests_softwares))
      config_appli_file.write("</application>\n")

    # Change permissions, this file should be readable by anyone
    os.chmod(config_appli_filename, 0o644)

    # Create MAP application
    command = ""
    if os.path.exists(os.path.join(self.appli_directory, "share", "map", "scripts", "appli_clean.sh")):
      command += "cd %s ; " % self.appli_directory
      command += os.path.join(self.appli_directory, "share", "map", "scripts", "appli_clean.sh -f ; ")
      command += "cd %s ; " % os.getcwd()
    command += os.path.join(mapresources_software.install_directory, "share", "map", "scripts", "appli_gen.py ")
    command += "--prefix=" + self.appli_directory + " "
    command += "--config=" + config_appli_filename
    command += " > " + os.path.join(self.yamm_appli_log_dir, "appli_gen.log") + " 2>&1 ;"

    # Execute command
    self.command_log.print_debug("Command is: %s" % command)
    ier = os.system(command)
    if ier != 0:
      return "Generation of application failed ! command was: %s" % command
    os.remove(config_appli_filename)
    self.command_log.print_end_work()
    self.command_log.print_info("A MAP application is generated in directory: %s" % self.appli_directory,
                                space=self.space + 2)
    self.command_log.print_info("You can run MAP with this command: %s" %
                                os.path.join(self.appli_directory, "bin", "map"),
                                space=self.space + 2)
    return ""
  #

#

class DeleteAppliTask(object):
  """
    This task is called to delete a MAP application
  """

  def __init__(self, appli_directory):
    self.appli_directory = appli_directory
  #

  def prepare_work(self, software_config, space, command_log):
    self.command_log = command_log
    self.space = space
    return ""
  #

  def execute(self, dependency_command, executor, current_software):
    self.command_log.print_begin_work("Delete application", space=self.space)

    try:
      misc.fast_delete(self.appli_directory)
    except:
      return "Cannot delete application directory %s" % self.appli_directory

    self.command_log.print_end_work()
    return ""
  #

#

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from __future__ import print_function
import os
import sys

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.core.framework.bin.yamm_command import FrameworkYammCommandsLauncher
from yamm.core.framework.bin.yamm_command import FrameworkYammCommand

class MapYammCommandsLauncher(FrameworkYammCommandsLauncher):
  def __init__(self):
    FrameworkYammCommandsLauncher.__init__(self)
    self.notify_title = "YAMM for MAP"
    self.commands["map_start_and_create_appli"] = FrameworkYammCommand("Start and config local install", self.map_start_and_create_appli)
    self.commands["map_start_offline_and_create_appli"] = FrameworkYammCommand("Start offline and config local install", self.map_start_offline_and_create_appli)
    self.commands["map_start_from_scratch_and_create_appli"] = FrameworkYammCommand("Start from scratch and config local install", self.map_start_from_scratch_and_create_appli)
    self.commands["map_create_appli"] = FrameworkYammCommand("Config local install", self.map_create_appli)
    self.commands["map_create_movable_installer"] = FrameworkYammCommand("Create movable installer", self.map_create_movable_installer)
    self.commands["map_run"] = FrameworkYammCommand("Run MAP", self.map_run)
    self.commands["map_run_with_options"] = FrameworkYammCommand("Run MAP with options", self.map_run_with_options)

  def map_start_and_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.start()
    if ret:
      return self.yamm_project.create_appli()
    return ret

  def map_start_offline_and_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.make(executor_mode="build")
    if ret:
      return self.yamm_project.create_appli()
    return ret

  def map_start_from_scratch_and_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    self.yamm_project.delete_directories(delete_install=True)
    ret = self.yamm_project.start()
    if ret:
      return self.yamm_project.create_appli()
    return ret

  def map_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.create_appli()

  def map_create_movable_installer(self, args, kw):
    self.yamm_project.print_configuration()
    try:
      installer_topdirectory = kw.get('installer_topdirectory', '')
      distrib                = kw.get('distrib', '')
      tgz                    = bool(int(kw.get('tgz', '1')))
      runnable               = bool(int(kw.get('runnable', '1')))
      delete                 = bool(int(kw.get('delete', '1')))
      universal              = bool(int(kw.get('universal', '0')))
      public                 = bool(int(kw.get('public', '0')))
      final_custom_command = kw.get('final_custom_command','')
      additional_commands = kw.get('additional_commands',[])
      gnu_tar_with_custom_compress_programm = kw.get('gnu_tar_with_custom_compress_programm','')
      return self.yamm_project.create_movable_installer(installer_topdirectory, distrib,
        tgz, runnable, delete, universal, public,
        final_custom_command, additional_commands, gnu_tar_with_custom_compress_programm)
    except Exception as e:
      print(e)
      return 0

  def map_run(self, args, kw):
    return self.yamm_project.run_appli()

  def map_run_with_options(self, args, kw):
    return self.yamm_project.run_appli(appli_options=kw.get('appli_options', ''))

#

if __name__ == "__main__":
  MapYammCommandsLauncher().run()

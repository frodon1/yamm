#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)
import sys
import os

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)
from yamm.core.framework.bin.yamm_infos import FrameworkYammInfos
from yamm.projects.map.project import Project

class MapYammInfos(FrameworkYammInfos):
  def get_project(self):
    return Project()
    
  def get_name(self):
    return "MAP"


if __name__ == "__main__":
  MapYammInfos().run()
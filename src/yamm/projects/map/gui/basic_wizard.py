#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from builtins import str
import os,sys
from PyQt4 import QtGui

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.core.framework.gui.basic_wizard import FrameworkBasicWizard
from yamm.core.framework.gui.basic_wizard import FrameworkBasicIntro
from yamm.core.framework.gui.basic_wizard import FrameworkBasicOptions
from yamm.core.framework.gui.basic_wizard import FrameworkBasicRemotes
from yamm.core.framework.gui.select_software_list import ManageSoftwareList

from yamm.projects.map.project import Project

class BasicWizard(FrameworkBasicWizard):

  def __init__(self, parent, title="Create a MAP project", project = Project()):
    FrameworkBasicWizard.__init__(self, parent, title, project)
    self.setWindowTitle("Create a MAP project")

  def get_yamm_project(self):
    return Project()

  def end(self, result):
    FrameworkBasicWizard.end(self, result)
    if self.result:
      self.map_create_appli = self.field("map_create_appli").toBool()
      self.map_server   = str(self.field("map_server").toString())
      self.map_username = str(self.field("map_username").toString())
      pass

  def add_pages(self):
    self.addPage(BasicIntro(self, self.get_yamm_project()))
    self.addPage(BasicOptions(self, self.get_yamm_project()))
    self.addPage(BasicRemotes(self, self.get_yamm_project()))
    self.addPage(ManageSoftwareList(self, self.get_yamm_project()))

class BasicIntro(FrameworkBasicIntro):
  def __init__(self, parent, project):
    FrameworkBasicIntro.__init__(self, parent, project)

class BasicOptions(FrameworkBasicOptions):
  def __init__(self, parent, project):
    FrameworkBasicOptions.__init__(self, parent, project)

  def setWidgets(self):
    FrameworkBasicOptions.setWidgets(self)
    self.opt_map_create_appli_button = QtGui.QPushButton("OFF")
    self.opt_map_create_appli_button.setCheckable(True)
    self.widgets_in_options_layout.insert(1,["Create appli after compilation:",self.opt_map_create_appli_button])
    # Signal and Slots
    self.opt_map_create_appli_button.toggled.connect(self.toggled_button)

  def setPageLayout(self):
    FrameworkBasicOptions.setPageLayout(self)

  def registerFields(self):
    FrameworkBasicOptions.registerFields(self)
    self.registerField("map_create_appli", self.opt_map_create_appli_button)

  def setDefaultValues(self):
    FrameworkBasicOptions.setDefaultValues(self)
    self.dir_top_lineEdit.setText(os.path.join(self.home_dir, "map"))
    self.dir_arch_lineEdit.setText(os.path.join(self.home_dir, "map", "archives"))
    self.dir_ver_lineEdit.setText(os.path.join(self.home_dir, "map", self.old_version))

class BasicRemotes(FrameworkBasicRemotes):
  def __init__(self, parent, project):
    FrameworkBasicRemotes.__init__(self, parent, project)

  def setWidgets(self):
    FrameworkBasicRemotes.setWidgets(self)

    # Remote configuration
    rem_groupBox = QtGui.QGroupBox("Remote configuration")
    rem_layout = QtGui.QGridLayout()
    rem_ser_label = QtGui.QLabel("MAP Server:")
    self.rem_ser_lineEdit = QtGui.QLineEdit()
    rem_user_label = QtGui.QLabel("MAP User:")
    self.rem_user_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(rem_ser_label, 0, 0)
    rem_layout.addWidget(self.rem_ser_lineEdit, 0, 1)
    rem_layout.addWidget(rem_user_label, 1, 0)
    rem_layout.addWidget(self.rem_user_lineEdit, 1, 1)
    rem_groupBox.setLayout(rem_layout)
    self.widgets_in_layout.append(rem_groupBox)

  def registerFields(self):
    FrameworkBasicRemotes.registerFields(self)
    # Fields
    self.registerField("map_server",         self.rem_ser_lineEdit)
    self.registerField("map_username",       self.rem_user_lineEdit)

  def setDefaultValues(self):
    FrameworkBasicRemotes.setDefaultValues(self)
    # Default Values
    self.rem_ser_lineEdit.setText("https://noeyy727.noe.edf.fr/mmc/map")
    self.rem_user_lineEdit.setText("")
    self.categoriesButtons["library"].setChecked(True)
    self.categoriesButtons["components"].setChecked(True)
    self.categoriesButtons["composites"].setChecked(True)
    self.categoriesButtons["tests"].setChecked(True)
    self.set_enabled_remotes()

  def toggled_button_remote_mode(self, status):
    FrameworkBasicRemotes.toggled_button_remote_mode(self, status)
    self.set_enabled_remotes()

  def set_enabled_remotes(self):
    one_category_is_archive = any([button.isChecked() for button in list(self.categoriesButtons.values())])
    self.rem_ser_lineEdit.setDisabled(not one_category_is_archive)
    self.rem_user_lineEdit.setDisabled(not one_category_is_archive)

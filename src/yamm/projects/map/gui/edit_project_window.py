#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os, sys

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.core.framework.gui.edit_project_window import FrameworkEditProjectWindow

from yamm.projects.map.project import Project
from yamm.projects.map.gui import gui_project

class EditProjectWindow(FrameworkEditProjectWindow):

  def get_empty_yamm_project(self):
    return Project()

  def get_empty_yamm_gui_project(self):
    return gui_project.YammGuiProject()

  def save_from_gui(self):
    ret = FrameworkEditProjectWindow.save_from_gui(self)
    if ret:
      self.yamm_gui_project.map_create_appli = True
      if self.mode == "edit":
        self.yamm_gui_project.map_create_appli = self.parent().\
                map_create_appli_checkBox.isChecked()
    return ret

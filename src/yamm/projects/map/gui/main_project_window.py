#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from builtins import str
import os, sys
from PyQt4 import QtGui, uic

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

# For syntax highlighting
from yamm.core.framework.gui import highlighter
from yamm.core.framework.gui.main_project_window import FrameworkProjectWindow

from yamm.projects.map.project import Project
from yamm.projects.map.gui     import edit_project_window

class ProjectWindow(FrameworkProjectWindow):

  def __init__(self, parent, gui_project, yamm_gui):
    FrameworkProjectWindow.__init__(self, parent, gui_project, yamm_gui)
    self.map_bin_directory  = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../bin")

  def init_widgets(self):
    FrameworkProjectWindow.init_widgets(self)

    self.map_create_appli_checkBox = QtGui.QCheckBox("Create appli")
    self.map_create_appli_button = QtGui.QPushButton("Create appli")
    self.map_create_installer_button = QtGui.QPushButton("Create Installer")

    self.actions_groupBox.layout().insertWidget(3,self.map_create_appli_checkBox)
    self.actions_groupBox.layout().addWidget(self.map_create_appli_button)
    self.actions_groupBox.layout().addWidget(self.map_create_installer_button)

    self.map_create_appli_checkBox.clicked.connect(self.map_update_create_appli)
    self.map_create_appli_button.clicked.connect(self.map_create_appli_command)
    self.map_create_installer_button.clicked.connect(self.map_create_installer_command)

    self.map_create_appli_checkBox.setChecked(self.gui_project.map_create_appli)

    self.map_run_button = QtGui.QPushButton("Run MAP")
    self.map_run_with_options_button = QtGui.QPushButton("Run MAP with options")
    self.specific_groupBox_layout = QtGui.QVBoxLayout()
    self.specific_groupBox_layout.addWidget(self.map_run_button)
    self.specific_groupBox_layout.addWidget(self.map_run_with_options_button)
    self.specific_groupBox.setLayout(self.specific_groupBox_layout)
    self.specific_groupBox.setTitle("Launch")
    self.specific_groupBox.setVisible(True)
    self.line_specific_actions.setVisible(True)

    self.map_run_button.clicked.connect(self.run_map)
    self.map_run_with_options_button.clicked.connect(self.run_map_options)
    
    self.w = CreateMovableInstallerDialog(self)

  def get_command_script(self):
    return os.path.join(self.map_bin_directory, "map_command.py")

  def get_yamm_project_edit_window(self):
    return edit_project_window.EditProjectWindow(self, self.yamm_gui, "edit", self.gui_project)

  def get_empty_yamm_project(self):
    return Project()

  def start_command(self):
    if self.map_create_appli_checkBox.isChecked():
      FrameworkProjectWindow.start_command(self, "map_start_and_create_appli")
    else:
      FrameworkProjectWindow.start_command(self)

  def start_offline_command(self):
    if self.map_create_appli_checkBox.isChecked():
      FrameworkProjectWindow.start_offline_command(self, "map_start_offline_and_create_appli")
    else:
      FrameworkProjectWindow.start_offline_command(self)

  def start_from_scratch_command(self):
    if self.map_create_appli_checkBox.isChecked():
      FrameworkProjectWindow.start_from_scratch_command(self, "map_start_from_scratch_and_create_appli")
    else:
      FrameworkProjectWindow.start_from_scratch_command(self)

  def map_update_create_appli(self):
    self.gui_project.map_create_appli = self.map_create_appli_checkBox.isChecked()
    self.gui_project.create_gui_project_file()

  def run_map(self):
    command = self.prepare_command("map_run")
    self.exec_command(command)

  def run_map_options(self):
    options, ret = QtGui.QInputDialog.getText(self,
                                              "Run Map with options",
                                              "Options")
    if ret:
      command = self.prepare_command("map_run_with_options")
      self.exec_command(command, [], {'appli_options':str(options)})

  def map_create_appli_command(self):
    command = self.prepare_command("map_create_appli")
    self.exec_command(command)

  def map_create_installer_command(self):   
    if self.w.exec_():
      kw = {}
      kw['installer_topdirectory'] = str(self.w.top_directory.text())
      kw['distrib'] = str(self.w.distrib.text())
      kw['tgz'] = 1 if self.w.tgz.isChecked() else 0
      kw['runnable'] = 1 if self.w.runnable.isChecked() else 0
      kw['delete'] = 1 if self.w.delete.isChecked() else 0
      kw['universal'] = 1 if self.w.universal.isChecked() else 0
      kw['public'] = 1 if self.w.public.isChecked() else 0
      kw['gnu_tar_with_custom_compress_programm'] = str(self.w.gnu_tar_with_custom_compress_programm.text())
      command = self.prepare_command("map_create_movable_installer")
      #print "args: {0}".format(args)
      self.exec_command(command, [], kw)

class CreateMovableInstallerDialog(QtGui.QDialog):
  def __init__(self, parent=0):
    QtGui.QDialog.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "create_movable_installer.ui"), self)
    highlighter.Highlighter(self.final_custom_command.document(),"bash")
    self.toggleAdvancedWidgetVisibility(False)
    self.pushButton.setVisible(False)
    self.advanced_widget.setVisible(False)

  def toggleAdvancedWidgetVisibility(self, visibility=None):
    widget_visibility = not self.advanced_widget.isVisible() if visibility is None else visibility
    self.advanced_widget.setVisible(widget_visibility)
    self.adjustSize()

  def editAdditionalCommand(self, index):
    command = str(self.additional_commands.currentItem().text())
    self.addAdditionalCommand(command, True)

  def addAdditionalCommand(self, command=None, edit=False):
    w = MapAddAdditionalCommand(self, command, edit)
    if w.exec_() and not w.edit:
      self.additional_commands.insertItem(self.additional_commands.count(), w.plainTextEdit.toPlainText())

  def removeAdditionalCommand(self):
    item = self.additional_commands.takeItem(self.additional_commands.currentRow())
    if item:
      del item

class MapAddAdditionalCommand(QtGui.QDialog):
  def __init__(self, parent, command=None, edit=False):
    QtGui.QDialog.__init__(self, parent)
    self.edit = edit
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "add_additional_command.ui"), self)
    highlighter.Highlighter(self.plainTextEdit.document(),"bash")
    if command is not None:
      self.plainTextEdit.setPlainText(command)

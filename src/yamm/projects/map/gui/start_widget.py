#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.core.framework.gui.start_widget import FrameworkIntroWidget

from yamm.projects.map.gui import basic_wizard
from yamm.projects.map.gui import main_project_window
from yamm.projects.map.gui import edit_project_window
from yamm.projects.map.gui import gui_project

class IntroWidget(FrameworkIntroWidget):

  def get_empty_yamm_gui_project(self):
    return gui_project.YammGuiProject()

  def get_basic_wizard_instance(self):
    return basic_wizard.BasicWizard(self)

  def get_main_project_window(self, yamm_gui_project):
    return main_project_window.ProjectWindow(self, yamm_gui_project, self.yamm_gui)

  def get_advanced_wizard_instance(self):
    return edit_project_window.EditProjectWindow(self, self.yamm_gui, "create")

  def getTranslationFile(self):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)),"yamm")

#!/usr/bin/env python
# -*- coding: utf-8 *-

from __future__ import print_function
import sys
import re
import subprocess

def is_reachable_host(network_host):
  try:
    ping = subprocess.Popen(["ping", "-n", "-c 1", "-w 5", network_host], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, error = ping.communicate()
    found = re.findall("1 packets transmitted, 1 received, 0% packet loss", out)
    successful = (out and not error and len(found) == 1)
    return successful
  except subprocess.CalledProcessError:
    # Couldn't get a ping
    return False
#

if __name__ == "__main__":
  if len(sys.argv) != 2:
    print("Usage: %s <url>"%sys.argv[0])
    sys.exit(1)

  print("Is reachable host:", is_reachable_host(sys.argv[1]))
#

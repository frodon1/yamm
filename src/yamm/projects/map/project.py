#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from __future__ import print_function
from __future__ import absolute_import
import os
import sys
import subprocess
# from yamm.core.base import misc
# from yamm.core.base.options import options
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command

from yamm.projects.map.softwares import components as default_components
from yamm.projects.map.softwares import composites as default_composites
from yamm.projects.map.softwares import library as default_library
from yamm.projects.map.softwares import prerequisites as default_prerequisites
from yamm.projects.map.softwares import tests as default_tests
from yamm.projects.map import versions as default_versions

from .appli_tasks import CreateAppliTask

class Project(FrameworkProject):

  def __init__(self, log_name="MAP", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.define_map_options()
  #

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="PROJECT", verbose=self.verbose_level)
    # Import prerequisites
    for software_name in default_prerequisites.softwares_list:
      self.add_software_in_catalog(software_name, "yamm.projects.map.softwares.prerequisites")
    # Import library
    for software_name in default_library.softwares_list:
      self.add_software_in_catalog(software_name, "yamm.projects.map.softwares.library")
    # Import components
    for software_name in default_components.softwares_list:
      self.add_software_in_catalog(software_name, "yamm.projects.map.softwares.components")
    # Import composites
    for software_name in default_composites.softwares_list:
      self.add_software_in_catalog(software_name, "yamm.projects.map.softwares.composites")
    # Import tests
    for software_name in default_tests.softwares_list:
      self.add_software_in_catalog(software_name, "yamm.projects.map.softwares.tests")
    # Import versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.map.versions")
    self.logger.debug("End init catalog")
  #

  def define_map_options(self):
    # MAP project categories
    self.options.add_category("prerequisites")
    self.options.add_category("library")
    self.options.add_category("components")
    self.options.add_category("composites")
    self.options.add_category("tests")

    # Options pour la compilation de la version
    self.options.add_option("map_prerequisites_env_file", "", ["global"])
    self.options.add_option("create_appli", False, ["global"])

    # repository options
    self.options.add_option("map_username", "", ["global", "category", "software"])
    self.options.add_option("map_server", "", ["global", "category", "software"])
  #

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.path.expanduser("~"), "map"))

    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)

    # Set default values for map project
    version_dir = current_command_options.get_global_option("version_directory")

    if not current_command_options.is_category_option("library", "main_software_directory"):
      current_command_options.set_category_option("library", "main_software_directory", os.path.join(version_dir, "library"))
    if not current_command_options.is_category_option("components", "main_software_directory"):
      current_command_options.set_category_option("components", "main_software_directory", os.path.join(version_dir, "components"))
    if not current_command_options.is_category_option("composites", "main_software_directory"):
      current_command_options.set_category_option("composites", "main_software_directory", os.path.join(version_dir, "composites"))
    if not current_command_options.is_category_option("prerequisites", "main_software_directory"):
      current_command_options.set_category_option("prerequisites", "main_software_directory", os.path.join(current_command_options.get_global_option("top_directory"), "prerequisites"))
    if not current_command_options.is_category_option("tests", "main_software_directory"):
      current_command_options.set_category_option("tests", "main_software_directory", os.path.join(version_dir, "tests"))

    if not current_command_options.is_category_option("library", "binary_archive_topdir"):
      current_command_options.set_category_option("library", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("library", "main_software_directory"), "binary_archives"))
    if not current_command_options.is_category_option("components", "binary_archive_topdir"):
      current_command_options.set_category_option("components", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("components", "main_software_directory"), "binary_archives"))
    if not current_command_options.is_category_option("composites", "binary_archive_topdir"):
      current_command_options.set_category_option("composites", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("composites", "main_software_directory"), "binary_archives"))
    if not current_command_options.is_category_option("prerequisites", "binary_archive_topdir"):
      current_command_options.set_category_option("prerequisites", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("prerequisites", "main_software_directory"), "binary_archives"))
    if not current_command_options.is_category_option("tests", "binary_archive_topdir"):
      current_command_options.set_category_option("tests", "binary_archive_topdir", os.path.join(current_command_options.get_category_option("tests", "main_software_directory"), "binary_archives"))

    if not current_command_options.is_category_option("library", "binary_archive_url"):
      current_command_options.set_category_option("library", "binary_archive_url", os.path.join(current_command_options.get_category_option("library", "binary_archive_topdir"), "%(name)s.tgz"))
    if not current_command_options.is_category_option("components", "binary_archive_url"):
      current_command_options.set_category_option("components", "binary_archive_url", os.path.join(current_command_options.get_category_option("components", "binary_archive_topdir"), "%(name)s.tgz"))
    if not current_command_options.is_category_option("composites", "binary_archive_url"):
      current_command_options.set_category_option("composites", "binary_archive_url", os.path.join(current_command_options.get_category_option("composites", "binary_archive_topdir"), "%(name)s.tgz"))
    if not current_command_options.is_category_option("prerequisites", "binary_archive_url"):
      current_command_options.set_category_option("prerequisites", "binary_archive_url", os.path.join(current_command_options.get_category_option("prerequisites", "binary_archive_topdir"), "%(name)s.tgz"))
    if not current_command_options.is_category_option("tests", "binary_archive_url"):
      current_command_options.set_category_option("tests", "binary_archive_url", os.path.join(current_command_options.get_category_option("tests", "binary_archive_topdir"), "%(name)s.tgz"))

    if not current_command_options.get_global_option("map_prerequisites_env_file"):
      current_command_options.set_global_option("map_prerequisites_env_file", "")

    if not current_command_options.is_category_option("library", "source_type"):
      current_command_options.set_category_option("library", "source_type", "remote")
    if not current_command_options.is_category_option("components", "source_type"):
      current_command_options.set_category_option("components", "source_type", "remote")
    if not current_command_options.is_category_option("composites", "source_type"):
      current_command_options.set_category_option("composites", "source_type", "remote")
    if not current_command_options.is_category_option("prerequisites", "source_type"):
      current_command_options.set_category_option("prerequisites", "source_type", "archive")
    if not current_command_options.is_category_option("tests", "source_type"):
      current_command_options.set_category_option("tests", "source_type", "remote")

    if not current_command_options.get_global_option("map_server"):
      current_command_options.set_global_option("map_server", "")
    if not current_command_options.get_global_option("map_username"):
      current_command_options.set_global_option("map_username", "")

    current_command_options.set_category_option("library", "archives_download_tool", "wget")
  #

  def check_specific_configuration(self, current_command_options):

    current_command_options.check_abs_path('main_software_directory',
                                           category='prerequisites',
                                           logger=self.logger)
    current_command_options.check_allowed_value('source_type',
                                     ["archive"],
                                     category='prerequisites',
                                     logger=self.logger)
    for category in ['library', 'components', 'composites', 'tests']:
      if current_command_options.is_category_option(category,
                                                    'main_software_directory'):
        current_command_options.check_abs_path('main_software_directory',
                                               category=category,
                                               logger=self.logger)
      current_command_options.check_allowed_value('source_type',
                                       ["archive", "remote"],
                                       category=category,
                                       logger=self.logger)

    for option in ['map_prerequisites_env_file', ]:
      if current_command_options.get_global_option(option) != "":
        current_command_options.check_abs_path(option, logger=self.logger)
  #

  def print_directories(self, current_command_options, max_width=-1):
    """
      This method prints directories values of the current command.

      Do not use it by yourself. Instead use :py:meth:`~yamm.core.framework.project.project.print_configuration` command.
    """

    prereq_main_software_dir_label = 'Prerequisistes directory'
    library_main_software_dir_label = 'Library directory'
    components_main_software_dir_label = 'Components directory'
    composites_main_software_dir_label = 'Composites directory'
    tests_main_software_dir_label = 'Tests directory'
    version_dir_label = 'Version directory'

    # Calculating max width of labels
    labels = []
    labels.append(prereq_main_software_dir_label)
    labels.append(library_main_software_dir_label)
    labels.append(components_main_software_dir_label)
    labels.append(composites_main_software_dir_label)
    labels.append(tests_main_software_dir_label)
    labels.append(version_dir_label)
    ljust_width = max([len(x) for x in labels])
    ljust_width = max(ljust_width, max_width)

    ljust_width = FrameworkProject.print_directories(self,
                                                     current_command_options,
                                                     ljust_width)
    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'prerequisite',
                  label=prereq_main_software_dir_label, width=ljust_width)
    current_command_options.print_global_option(
                  self.logger, 'version_directory',
                  label=version_dir_label, width=ljust_width)
    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'library',
                  label=library_main_software_dir_label, width=ljust_width)
    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'components',
                  label=components_main_software_dir_label, width=ljust_width)
    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'composites',
                  label=composites_main_software_dir_label, width=ljust_width)
    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'tests',
                  label=tests_main_software_dir_label, width=ljust_width)
    return ljust_width
#     self.logger.info("Library directory        : %s" % current_command_options.get_category_option("library", "main_software_directory"))
#     self.logger.info("Components directory     : %s" % current_command_options.get_category_option("components", "main_software_directory"))
#     self.logger.info("Composites directory     : %s" % current_command_options.get_category_option("composites", "main_software_directory"))
#     self.logger.info("Tests directory     : %s" % current_command_options.get_category_option("tests", "main_software_directory"))
  #

  def create_env_files(self, delete=True, installer=False):
    prereq_file = self.create_prerequisites_file(delete, installer)

    files = []
    files.append(os.path.basename(prereq_file))
    listing_file = os.path.join(self.current_command_options.get_global_option("version_directory"), ".map_env_files")
    with open(listing_file, 'w') as l_file:
      l_file.writelines("\n".join(files))
      l_file.write("\n")
  #

  def get_env_files(self):
    """
    This method returns the list of env files of the project
    """
    prereq_file = self.current_command_options.get_global_option("map_prerequisites_env_file")
    return [prereq_file]
  #

  def create_prerequisites_file(self, delete=True, installer=False):
    prerequisites_file = os.path.join(self.current_command_options.get_global_option("version_directory"), "map_prerequisites.sh")
    self.logger.info("Creating environment file : %s" % prerequisites_file)
    prerequisites_file_directory = os.path.dirname(prerequisites_file)
    if os.path.exists(prerequisites_file):
      try:
        if delete:
          os.remove(prerequisites_file)
        else:
          return
      except:
        self.logger.exception("Error in deleting prerequisites file: %s" % prerequisites_file)

    try:
      if not os.path.isdir(prerequisites_file_directory):
        os.makedirs(prerequisites_file_directory)
    except:
      self.logger.error("Creation of directory for prerequisites file failed ! Directory was: %s" % prerequisites_file_directory)

    # Starting prerequis file
    with open(prerequisites_file, 'w') as pre_file:
      pre_file.write("#!/bin/bash\n")
      pre_file.write("# MAP %s - Environnement file for prerequisites\n" % self.version)

    # Adding user_extra_prerequisites_env_file
    # user_extra_prerequisites_env_file = self.current_command_options.get_global_option("user_extra_prerequisites_env_file")
    # if user_extra_prerequisites_env_file != "" and user_extra_prerequisites_env_file != prerequisites_file:
    #   if not os.path.exists(user_extra_prerequisites_env_file):
    #     self.logger.error("User extra prerequisites file does not exist: %s" % user_extra_prerequisites_env_file)
    #   lines = []
    #   with open(user_extra_prerequisites_env_file) as f:
    #     lines = f.readlines()
    #   with open(prerequisites_file, 'a') as f:
    #     f.writelines(lines)

    # Continuing prerequisites file
    # with open(prerequisites_file, 'a') as pre_file:
    #   if not installer:
    #     pre_file.write(templates.map_prerequisite_file_header_template.substitute(root_map=self.current_command_options.get_global_option("version_directory")))
    #   else:
    #     pre_file.write(strings.installer_map_prerequisite_header)
    #     has_prereq, has_tools = self.__areSoftwaresPrerequisitesOrTools(self.current_command_softwares)
    #     if has_prereq:
    #       pre_file.write(strings.installer_map_prerequisite_header_check_prereq)
    #     if has_tools:
    #       pre_file.write(strings.installer_map_prerequisite_header_check_tools)
    return prerequisites_file
  #

  def update_configuration(self):
    map_server = "http://noeyy727.noe.edf.fr/mmc/map/trunk"
    if self.current_command_options.get_global_option("map_server") == "":
      self.current_command_options.set_global_option("map_server", map_server)

    from .ping_network_host import is_reachable_host
    inside_edf_rin = is_reachable_host("ftp.pleiade.edf.fr")
    if inside_edf_rin:
      if self.current_command_options.get_category_option("prerequisites", "archive_remote_address") == "":
        self.current_command_options.set_category_option("prerequisites", "archive_remote_address", "ftp://ftp.pleiade.edf.fr/projets/MAP")
      if self.current_command_options.get_category_option("library", "archive_remote_address") == "":
        self.current_command_options.set_category_option("library", "archive_remote_address", "ftp://ftp.pleiade.edf.fr/projets/MAP")
      if self.current_command_options.get_category_option("components", "archive_remote_address") == "":
        self.current_command_options.set_category_option("components", "archive_remote_address", "ftp://ftp.pleiade.edf.fr/projets/MAP")
      if self.current_command_options.get_category_option("composites", "archive_remote_address") == "":
        self.current_command_options.set_category_option("composites", "archive_remote_address", "ftp://ftp.pleiade.edf.fr/projets/MAP")
      if self.current_command_options.get_category_option("tests", "archive_remote_address") == "":
        self.current_command_options.set_category_option("tests", "archive_remote_address", "ftp://ftp.pleiade.edf.fr/projets/MAP")

    # Répertoires dépendants de la version
    if not self.current_command_options.get_global_option("map_prerequisites_env_file"):
      self.current_command_options.set_global_option("map_prerequisites_env_file",
                                                     os.path.join(self.current_command_options.get_global_option("version_directory"),
                                                                  "map_prerequisites.sh"))
  #

  def update_env_files_hook(self, software_list):
    update_name = software_list[0]
    if len(software_list) > 1:
      for soft_name in software_list[1:]:
        update_name += "_" + soft_name
    self.current_command_options.set_global_option("map_prerequisites_env_file",
                                                   os.path.join(self.current_command_options.get_global_option("version_directory"),
                                                                "map_prerequisites_update_%s.sh" % update_name))
    self.create_env_files()
  #
  def add_software_to_env_files_end_hook(self, executor_software):
    software = self.current_map_name_to_softwares[executor_software.name]
    if not software.isArtefact():
      if not self.is_build_only_software(executor_software.name) or \
          not self.current_command_options.get_global_option("separate_dependencies"):
        # Add software in the prerequisites file
        with open(self.current_command_options.get_global_option("map_prerequisites_env_file"), "a") as pre_file:
          pre_file.write(software.get_prerequisite_str())
  #
  def get_version_custom_tasks(self):
    tasks = []
    if self.current_command_options.get_global_option("create_appli"):
      if self.appli_directory is None:
        appli_name = "map_" + self.current_command_version_object.name
        appli_topdirectory = self.current_command_options.get_global_option("version_directory")
        self.appli_directory = os.path.join(appli_topdirectory, appli_name)
#      tasks.append(CreateAppliTask(self.appli_directory, self.appli_catalogresources))
      tasks.append(CreateAppliTask(self.appli_directory))
#    for test_suite in self.current_command_version_object.test_suite_list:
#      test_suite.appli_directory = self.appli_directory
    return tasks

  @yamm_command("Generation of MAP env files")
  def generates_env_files(self):
    """
      This command generates the MAP's environnement files. It uses the executor mode **nothing**.
    """
    software_mode = "nothing"
    for software in self.current_command_softwares:
      # Step 1: configuration du logiciel
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      # Force the mode to nothing
      software.software_mode = software_mode
      software.create_tasks()
      # Step 2: ajout du logiciel dans l'executeur
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
      self.current_map_name_to_softwares[software.name] = software
    # execution -> To be sure to create a correct prerequis file
    self.create_prerequisites_file()
    self.current_command_executor.add_hooks(None, self.add_software_to_env_files_end_hook)
    self.execute_command()
    # check generated env files
    self.check_generated_env_files()
  #

  @yamm_command("Creation of a MAP application")
  def create_appli(self, appli_topdirectory="", appli_name="map_", generates_env_files=True):  # , appli_catalogresources="", generates_env_files=True):
    """
      This command generates a MAP application with the current command configuration.

      :param string appli_topdirectory: Defines the application top directory.
      :param string appli_name: Defines the application name.
    """

    # Prepare execution with software_mode = "nothing"
    software_mode = "nothing"
    for software in self.current_command_softwares:
      # Step 1: configuration du logiciel
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               software_mode,
                               self.current_command_version_object,
                               self.current_command_python_version)
      # Force the mode to nothing
      software.software_mode = software_mode
      software.create_tasks()
      # Step 2: ajout du logiciel dans l'executeur
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
      self.current_map_name_to_softwares[software.name] = software
    # execution -> To be sure to create a correct prerequis file
    if generates_env_files:
      self.create_prerequisites_file()
      self.current_command_executor.add_hooks(None, self.add_software_to_env_files_end_hook)

    # Get parameters for create_appli task
#    if appli_name == "":
#      appli_name = "map_" + self.current_command_version_object.name
    if appli_name == "map_":
      appli_name += self.current_command_version_object.name
    if appli_topdirectory == "":
      appli_topdirectory = self.current_command_options.get_global_option("version_directory")
    self.appli_directory = os.path.join(appli_topdirectory, appli_name)
#    if appli_catalogresources != "":
#      self.appli_catalogresources = appli_catalogresources

    # Add create_appli task to the version
    self.current_command_options.set_global_option("create_appli", True)
    self.current_command_version_object.add_tasks(self.get_version_custom_tasks())

    self.execute_command()
    # check generated env files
    self.check_generated_env_files()
  #

  @yamm_command("Launch of MAP application")
  def run_appli(self, appli_topdirectory="", appli_name="map_", appli_options=""):
    """
      This command starts a MAP application. By default, it uses the default application
      generated by the command :py:meth:`~yamm.projects.map.project.Project.create_appli`. Another
      MAP application could be choosed with the command optional arguments.
      Additional options can be provided to the map command.
      The command returns when the MAP application is shutdown.

      :param string appli_topdirectory: Defines the application top directory.
      :param string appli_name: Defines the application name.
      :param string appli_options: Defines the application options.
    """
    # Create MAP Application config file
    # if appli_name == "":
    #   appli_name = "appli_" + self.current_command_version_object.name
    if appli_name == "map_":
      appli_name += self.current_command_version_object.name
    if appli_topdirectory == "":
      appli_topdirectory = self.current_command_options.get_global_option("version_directory")
    appli_path = os.path.join(appli_topdirectory, appli_name)
    self.end_command()

    # Test appli
    if os.path.exists(appli_path):
      if not os.path.isdir(appli_path):
        self.logger.error("Cannot find map application directory: %s" % appli_path)
    if not os.path.exists(os.path.join(appli_path, "bin", "map")):
        self.logger.error("Cannot find map: %s" % os.path.join(appli_path, "bin", "map"))

    cmd = os.path.join(".", appli_path, "bin", "map %s" % appli_options)
    try:
      retcode = subprocess.call(cmd, shell=True)
      if retcode < 0:
        self.logger.error("%s command failed" % cmd)
    except OSError as e:
      self.logger.exception("command was: %s, exception was: %s" % (cmd, e))


  @yamm_command("Creation of MAP movable installer")
  def create_movable_installer(self, installer_topdirectory="", distrib="",
                               tgz=True, runnable=True, delete=True, universal=False,
                               public=False, final_custom_command="", additional_commands=[],
                               gnu_tar_with_custom_compress_programm=""):
    raise("Not implemented yet")


if __name__ == "__main__":
  print("Testing map project class")
  pro = Project("MAP Project", VerboseLevels.WARNING)
  pro.set_global_option("parallel_make", "8")
  pro.set_global_option("continue_on_failed", False)
  # pro.options.set_global_option("top_directory", "/tmp/map")
  pro.set_global_option("top_directory", "/tmp/testMAPYamm")
  # pro.options.set_software_option("INIPARSER", "software_build_directory", "/tmp/mon_build_iniparser")
  # pro.options.set_software_option("MAPRESOURCES", "source_type", "archive")
  # pro.options.set_category_option("components", "executor_mode", "only_compile")
  # pro.options.set_software_option("MAPENGINE", "executor_mode", "only_compile")
  # pro.options.set_software_option("MAPRESOURCES", "executor_mode", "only_compile")
  # pro.options.set_software_option("MAPUTILS", "executor_mode", "only_compile")
  # pro.options.set_software_option("C_IMAGE_2D_FOULING_RATE", "executor_mode", "only_compile")
  # pro.options.set_software_option("C_IMAGE_3D_ALTITUDE_THICKNESS", "executor_mode", "only_compile")
  # pro.options.set_software_option("C_PRE_MORPHOLOGY_GRAVEL", "executor_mode", "only_compile")
  # pro.options.set_software_option("C_PRE_MORPHOLOGY_GRID_PROJECTION", "executor_mode", "only_compile")
  # pro.options.set_software_option("C_SOLVER_CRYSTAL_ALLOY_BEHAVIOUR", "executor_mode", "only_compile")
  # pro.options.set_software_option("C_SOLVER_ELASTICITY_FDVGRID", "executor_mode", "only_compile")

  # pro.options.set_software_option("C_SOLVER_CORROSION_EVOLUTION", "executor_mode", "only_compile")
  # pro.options.set_software_option("C_SOLVER_DIFFUSION_FDVGRID", "executor_mode", "only_compile")

  # pro.options.set_software_option("C_TRANSVERSE_EMPTY_C", "executor_mode", "only_compile")
  # pro.options.set_software_option("TEST_KELKINS", "executor_mode", "only_compile"))
  pro.set_software_option("TEST_MADNEX", "executor_mode", "only_compile")

  pro.set_version("V2014_1")
  pro.print_configuration()
  # pro.download()
  pro.start()
  pro.create_appli()
#

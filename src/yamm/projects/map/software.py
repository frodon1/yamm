#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.core.base import misc
from yamm.core.framework.software import FrameworkSoftware
# from test_suite import MapTestSuite
# from appli_tasks import CreateAppliTask, DeleteAppliTask
# from yamm.core.engine.dependency import Dependency


class MapSoftware(FrameworkSoftware):
  """
  This class defines the generic interface of a MAP software
  """

  def __init__(self, name="NOT CONFIGURED", version="NOT CONFIGURED",
               verbose=misc.VerboseLevels.INFO, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)

    # For module gui sorting
#    self.module_gui_sort = 10000

    # For modules that support both autotools and CMake compilation
#    self.cmake_config_options = ""

#    self.module_gui_type = misc.enum(pre_processing=10,
#                      solver = 20,
#                      post_processing = 30,
#                      supervision = 40,
#                      specific_preprocessing = 50,
#                      advanced_data_processing = 60,
#                      examples = 1000)

  def init_variables(self):
    if self.get_type() in ["library", "components", "composites", "tests"]:
      self.executor_software_name = self.name + "_" + misc.transform(self.version)
      self.src_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "src", self.executor_software_name)
      self.build_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "build", self.executor_software_name)
      self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"), "install", self.executor_software_name)
    pass

#######
#
# Méthodes appelées par le projet
#
#######

  def default_values_hook(self):
    self.env_files.append(self.project_options.get_global_option("build_env_file"))
    self.env_files.append(self.project_options.get_global_option("map_prerequisites_env_file"))
    self.env_files.append(self.project_options.get_global_option("map_libraries_env_file"))
    self.env_files.append(self.project_options.get_global_option("map_components_env_file"))
    self.env_files.append(self.project_options.get_global_option("map_composites_env_file"))
    self.env_files.append(self.project_options.get_global_option("map_tests_env_file"))

  # def user_variables_hook(self):
  #   # Specific user options for modules
  #   if self.get_type() == "module":
  #     if self.project_options.get_option(self.name, "cmake_compilation_modules"):
  #       if self.module_support_cmake_compilation():
  #         self.compil_type  = "cmake"
  #         self.gen_file     = ""
  #         self.gen_commands = []
  #     if self.compil_type == "cmake" and self.cmake_config_options != "":
  #       self.config_options = self.cmake_config_options
  #       if self.project_options.is_software_option(self.name, "software_additional_config_options"):
  #         self.config_options += self.project_options.get_option(self.name, "software_additional_config_options")
  #     if self.project_options.get_option(self.name, "modules_debug_mode"):
  #       if self.compil_type == "autoconf":
  #         self.config_options += " --enable-debug"
  #       elif self.compil_type == "cmake":
  #         self.config_options += " -DCMAKE_BUILD_TYPE=Debug "
  #     else:
  #       if self.compil_type == "autoconf":
  #         self.config_options += " --disable-debug --enable-production"
  #     if self.project_options.get_option(self.name, "make_modules_dev_docs"):
  #       if self.has_dev_docs():
  #         self.pre_install_commands.append("make dev_docs")

#######
#
# Specific methods for MAP project
#
#######

  def get_type(self):
    """
    This method returns the type of software.
    There are 4 types in the MAP project:

      - prerequisites
      - library
      - components
      - composites
      - tests
    """
    return "type not defined for software %s" % self.name

  def get_prerequisite_str(self, specific_install_dir=""):
    """
    This method returns a string which will be added in the
    environment file of the prerequisites.
    """
    return ""

  def get_configuration_str(self, specific_install_dir=""):
    """
    This method returns a string which will be added to the
    software configuration file.
    """
    return ""

  def get_relative_lib_path_str(self):
    """
    This method returns a string which is the relative path of the map
    module libraries.
    """
    if self.get_type() == "module":
      return "lib/map"
    return ""

  def get_module_str(self, specific_install_dir="", universal=False):
    """
    This method returns a string which will be added in the
    environment file of the modules
    """
    if self.get_type() == "module":
      install_dir = self.install_directory
      if specific_install_dir != "":
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
      envstr = "export " + self.get_module_name_for_appli() + "_ROOT_DIR=\"" + install_dir + "\"\n"
      if self.compil_type == "cmake":
        if self.get_relative_lib_path_str():
          envstr += "export LD_LIBRARY_PATH=\"" + os.path.join(install_dir, self.get_relative_lib_path_str()) + "\":${LD_LIBRARY_PATH}\n"
      return envstr
    else:
      return ""

  def isArtefact(self):
    """
    This method is used to declare a software as an artefact used during
    the building process of Map. If the value is True, the software will
    not be use at runtime and will not be included into the installer.
    """
    return False

  def has_map_module_gui(self):
    """
    This method returns a boolean. If True, the software has a GUI in Map.
    """
    return False

  def has_dev_docs(self):
    """
    This method returns a boolean. If True, the software has a developer's guide.
    """
    return False

  def get_module_name_for_appli(self):
    """
    This methos returns the name of the software as described in the application.
    Be default the name of the software is returned.
    """
    return self.name

  def module_support_cmake_compilation(self):
    """
    This method returns a boolean which indicate if the software is compatible
    with a compilation using cmake.
    """
    return False

  def replaceEnvFilePath(self, env_file_name, filename):
    """
      This method will insert sed commands into make_movable_archive_commands
      to replace the path of an environment file by "__ENV_DIRECTORY_PATH__/<env_file_name>".

      :param string env_file_name: Defines the name of the env file, relatively to the env files
      directory.
      :param string filename: Defines the name of the file where the replace operation will be done
      , relatively to the install dir.
    """
    template_string = os.path.join("__ENV_DIRECTORY_PATH__", env_file_name)
    prerequisites_env_file = self.project_options.get_global_option("map_prerequisites_env_file")
    env_directory = os.path.dirname(prerequisites_env_file)
    if env_directory:
      env_file_path = os.path.join(env_directory, env_file_name)
      self.replaceStringByTemplateInFile(env_file_path, template_string, filename)

  # def create_post_compile_test_tasks(self):
  #   appli_directory = None
  #   for test_suite in self.test_suite_list:
  #     if isinstance(test_suite, MapTestSuite) and test_suite.require_appli:
  #       if appli_directory is None:
  #         appli_directory = tempfile.mkdtemp(prefix = "appli_test_")
  #         self.post_install_test_task_list.append(CreateAppliTask(appli_directory))
  #       test_suite.appli_directory = appli_directory
  #     (post_build, post_install) = test_suite.create_post_compile_tasks()
  #     self.post_build_test_task_list += post_build
  #     self.post_install_test_task_list += post_install
  #   if appli_directory is not None:
  #     self.post_install_test_task_list.append(DeleteAppliTask(appli_directory))

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from yamm.projects.map.software import MapSoftware

software_name = "C_IMAGE_2D_BRIGHTNESS_EQUALIZER"

class C_IMAGE_2D_BRIGHTNESS_EQUALIZER(MapSoftware):

  def init_variables(self):
    self.archive_file_name    = software_name.lower() + "-" + self.get_version_for_archive() + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "cmake"

    self.remote_type     = "svn"
    self.root_repository = "https://noeyy727.noe.edf.fr"
    self.repository_name = "mmc/map"
    self.tag = self.version + "/src/MAP_components/image/c_image_2d_brightness_equalizer"
  #

  def get_type(self):
    return "components"
  #

#

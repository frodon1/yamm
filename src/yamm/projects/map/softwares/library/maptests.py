#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014, 2015 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc

software_name = "MAPTESTS"

maptests_template = """
#------ maptests ------
export MAPTESTS_DIR="%install_dir"
"""
maptests_template = misc.PercentTemplate(maptests_template)

class MAPTESTS(MapSoftware):

  def init_variables(self):
    self.archive_file_name    = software_name.lower() + "-" + self.get_version_for_archive() + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "cmake"

    self.remote_type     = "svn"
    self.root_repository = "https://noeyy727.noe.edf.fr"
    self.repository_name = "mmc/map"
    self.tag = self.version + "/src/tests"
  #

  def get_type(self):
    return "library"
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return maptests_template.substitute(install_dir=install_dir)
  #

#

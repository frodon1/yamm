#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014, 2015 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc

software_name = "MAPUTILS"

maputils_template = """
#------ maputils ------
export MAPUTILS_DIR="%install_dir"
export LD_LIBRARY_PATH=${MAPUTILS_DIR}/lib:${LD_LIBRARY_PATH}
"""
maputils_template = misc.PercentTemplate(maputils_template)

class MAPUTILS(MapSoftware):

  def init_variables(self):
    self.archive_file_name = software_name.lower() + "-" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.remote_type = "svn"
    self.root_repository = "https://noeyy727.noe.edf.fr"
    self.repository_name = "mmc/map"
    self.tag = self.version + "/src/MAP_library/mapc/utils"
  #

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["INIPARSER"]
  #

  def get_dependency_object_for(self, dependency_name):
    dependency_object = MapSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "INIPARSER":
      dependency_object.depend_of = ["install_path"]
    return dependency_object
  #

  def get_type(self):
    return "library"
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return maputils_template.substitute(install_dir=install_dir)
  #

#

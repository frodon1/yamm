softwares_list = []

# Public softwares
softwares_list += ["jedbrown-cmake-modules", "iniparser", "med", "sundials", "gmic", "neper", "petsc", "pink", "pyxl", "libnpy", "python_libmed", "assimulo", "fftw", "tfel"]

# EDF softwares
softwares_list += ["craft", "madnex", "kelkins"]

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc

software_name = "ASSIMULO"

assimulo_template = """
#------ assimulo ------
export ASSIMULO_DIR="%install_dir"
export PYTHONPATH=${ASSIMULO_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
assimulo_template = misc.PercentTemplate(assimulo_template)

class ASSIMULO(MapSoftware):

  def init_variables(self):
    self.archive_file_name = "Assimulo-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"
    self.build_options += "--sundials-home=${SUNDIALS_INSTALL_DIR}"

    remote_address = self.project_options.get_option(self.name, "archive_remote_address")
    if remote_address != "":
      remote_address += "/public/assimulo"
    else:
      remote_address = "http://pypi.python.org/packages/source/A/Assimulo"
    self.project_options.set_software_option(software_name, "archive_remote_address", remote_address)
  #

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["SUNDIALS"]
  #

  def get_dependency_object_for(self, dependency_name):
    dependency_object = MapSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "SUNDIALS":
      dependency_object.depend_of = ["path", "install_path", "ld_lib_path"]
    return dependency_object
  #

  def get_type(self):
    return "prerequisites"
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return assimulo_template.substitute(install_dir=install_dir, python_version=self.python_version)
  #

#

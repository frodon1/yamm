#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc

software_name = "INIPARSER"

iniparser_template = """
#------ iniparser ------
export INIPARSER_DIR="%install_dir"
export LD_LIBRARY_PATH=${INIPARSER_DIR}/lib:${LD_LIBRARY_PATH}
"""
iniparser_template = misc.PercentTemplate(iniparser_template)

class INIPARSER(MapSoftware):

  def init_variables(self):
    self.archive_file_name    = "iniparser-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "specific"
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_build_command = "cp -rf $CURRENT_SOFTWARE_SRC_DIR/* $CURRENT_SOFTWARE_BUILD_DIR/ ; cd $CURRENT_SOFTWARE_BUILD_DIR ; make -j%s"%parallel_make
    self.specific_install_command = "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib ; mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/include ; cd $CURRENT_SOFTWARE_BUILD_DIR ; cp src/*.h $CURRENT_SOFTWARE_INSTALL_DIR/include ; cp *.a *.so* $CURRENT_SOFTWARE_INSTALL_DIR/lib"

    remote_address = self.project_options.get_option(self.name, "archive_remote_address")
    if remote_address != "":
      remote_address += "/public/iniparser"
    else:
      remote_address = "http://ndevilla.free.fr/iniparser"
    self.project_options.set_software_option(software_name, "archive_remote_address", remote_address)
  #

  def get_type(self):
    return "prerequisites"
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return iniparser_template.substitute(install_dir=install_dir)
  #

#

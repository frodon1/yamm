#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from yamm.projects.map.software import MapSoftware
import os

software_name = "LIBNPY"

class LIBNPY(MapSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    MapSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "patches")

  def init_patches(self):
    MapSoftware.init_patches(self)
    self.patches.append(os.path.join(self.patch_directory, "libnpy-0.5_jsn.patch"))  
    
  def init_variables(self):
    self.archive_file_name    = "libnpy-" + self.version + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "specific"

    self.specific_build_command = "cp -rf $CURRENT_SOFTWARE_SRC_DIR/* $CURRENT_SOFTWARE_BUILD_DIR/ ; cd $CURRENT_SOFTWARE_BUILD_DIR/src ; make ; make check"
    self.specific_install_command = "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib ; mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/include ; cd $CURRENT_SOFTWARE_BUILD_DIR ; cp include/*.h $CURRENT_SOFTWARE_INSTALL_DIR/include ; cp lib/*.a $CURRENT_SOFTWARE_INSTALL_DIR/lib"

    remote_address = self.project_options.get_option(self.name, "archive_remote_address")
    if remote_address != "":
      remote_address += "/MAPshare/deps/libnpy"
    else:
      remote_address = "http://web.maths.unsw.edu.au/~mclean"
    self.project_options.set_software_option(software_name, "archive_remote_address", remote_address)
  #

  def get_type(self):
    return "prerequisites"
  #

#

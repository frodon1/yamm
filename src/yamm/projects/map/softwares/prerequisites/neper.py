#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc

software_name = "NEPER"

neper_template = """
#------ neper ------
export NEPER_DIR="%install_dir"
export PATH=${NEPER_DIR}/bin:${PATH}
"""
neper_template = misc.PercentTemplate(neper_template)

class NEPER(MapSoftware):

  def init_variables(self):
    self.archive_file_name    = "neper-" + self.version + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "specific"
    # :TODO: ugly solution to detect matheval library
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_build_command = "matheval_search=`/sbin/ldconfig -p | grep matheval | wc -l` ; matheval_available=\"OFF\" ; if [ ${matheval_search} -gt 0 ]; then matheval_available=\"ON\"; fi ; cd $CURRENT_SOFTWARE_BUILD_DIR ; cmake $CURRENT_SOFTWARE_SRC_DIR/src -DCMAKE_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR -DHAVE_LIBMATHEVAL=${matheval_available} ; make -j%s"%parallel_make
    self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR ; make install"

    remote_address = self.project_options.get_option(self.name, "archive_remote_address")
    if remote_address != "":
      remote_address += "/public/neper"
    else:
      remote_address = "http://sourceforge.net/projects/neper/files/neper/%s"%self.version
    self.project_options.set_software_option(software_name, "archive_remote_address", remote_address)
  #

  def get_type(self):
    return "prerequisites"
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return neper_template.substitute(install_dir=install_dir)
  #

#

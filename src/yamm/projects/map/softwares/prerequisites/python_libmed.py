#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc
from yamm.core.engine.dependency import Dependency

software_name = "PYTHON_LIBMED"

python_libmed_template = """
#------ python_libmed ------
export PYTHON_LIBMED_DIR="%install_dir"
export PYTHONPATH=${PYTHON_LIBMED_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
python_libmed_template = misc.PercentTemplate(python_libmed_template)

class PYTHON_LIBMED(MapSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    MapSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"  # Force archive mode

  def init_variables(self):
    self.archive_file_name = "python-libmed_" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    # self.compil_type          = "python"
    self.compil_type = "specific"
    self.specific_install_command = "export CC=mpicc ; export CFLAGS=-I${MED_INSTALL_DIR}/include ; export LDFLAGS=-L${MED_INSTALL_DIR}/lib ; cd $CURRENT_SOFTWARE_SRC_DIR ; python setup.py build --build-base=$CURRENT_SOFTWARE_BUILD_DIR install --prefix=$CURRENT_SOFTWARE_INSTALL_DIR ; unset CC ; unset CFLAGS ; unset LDFLAGS"

    remote_address = self.project_options.get_option(self.name, "archive_remote_address")
    if remote_address != "":
      remote_address += "/public/python-libmed"
    else:
      remote_address = "http://hg.python-science.org/python-libmed/archive"
    self.project_options.set_software_option(software_name, "archive_remote_address", remote_address)
  #

  def get_type(self):
    return "prerequisites"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["MED"]
  #

  def get_dependency_object_for(self, dependency_name):
    dependency_object = None
    if dependency_name == "MED":
      dependency_object = Dependency(name="MED", depend_of=["install_path"])
    return dependency_object
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return python_libmed_template.substitute(install_dir=install_dir, python_version=self.python_version)
  #

#

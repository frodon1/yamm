#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014, 2015 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc
import os

software_name = "PYXL"

pyxl_template = """
#------ pyxl ------
export PYXL_DIR="%install_dir"
export PYTHONPATH=${PYXL_DIR}/share/pyxl:${PYTHONPATH}
export PATH=${PYXL_DIR}/share/pyxl:${PATH}
"""
pyxl_template = misc.PercentTemplate(pyxl_template)

class PYXL(MapSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    MapSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "patches")

  def init_patches(self):
    MapSoftware.init_patches(self)
    self.patches.append(os.path.join(self.patch_directory, "PyXL-%s_use_numpy.patch")%self.version)

  def init_variables(self):
    self.archive_file_name    = "PyXL-" + self.version + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "specific"

    self.specific_install_command = "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/share/pyxl ; cd $CURRENT_SOFTWARE_SRC_DIR ; cp -rf * $CURRENT_SOFTWARE_INSTALL_DIR/share/pyxl"

    remote_address = self.project_options.get_option(self.name, "archive_remote_address")
    if remote_address != "":
      remote_address += "/public/pyxl"
    else:
      remote_address = "http://cbsu.tc.cornell.edu/staff/myers/PyXL"
    self.project_options.set_software_option(software_name, "archive_remote_address", remote_address)
  #

  def get_type(self):
    return "prerequisites"
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pyxl_template.substitute(install_dir=install_dir)
  #

#

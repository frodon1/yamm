#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

import os

from yamm.projects.map.software import MapSoftware
from yamm.core.base import misc

software_name = "SUNDIALS"

sundials_template = """
#------ sundials ------
export SUNDIALS_DIR="%install_dir"
export PATH=${SUNDIALS_DIR}/bin:${PATH}
"""
sundials_template = misc.PercentTemplate(sundials_template)

class SUNDIALS(MapSoftware):

  def init_variables(self):
    self.archive_file_name    = "sundials-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "autoconf"
    self.config_options += "--with-cflags=-fPIC"

    remote_address = self.project_options.get_option(self.name, "archive_remote_address")
    if remote_address != "":
      remote_address += "/public/sundials"
    else:
      remote_address = "http://ftp.mcs.anl.gov/pub/petsc/externalpackages"
    self.project_options.set_software_option(software_name, "archive_remote_address", remote_address)
  #

  def get_type(self):
    return "prerequisites"
  #

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return sundials_template.substitute(install_dir=install_dir)
  #

#

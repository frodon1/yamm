#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from yamm.projects.map.software import MapSoftware

software_name = "TEST_MADNEX"

class TEST_MADNEX(MapSoftware):

  def init_variables(self):
    self.archive_file_name = software_name.lower() + "-" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.remote_type = "svn"
    self.root_repository = "https://noeyy727.noe.edf.fr"
    self.repository_name = "mmc/map"
    self.tag = self.version + "/public/madnex/tests"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["MADNEX", "MAPSH"]
  #

  def get_dependency_object_for(self, dependency_name):
    dependency_object = MapSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "MADNEX":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "MAPSH":
      dependency_object.depend_of = ["install_path"]
    return dependency_object
  #

  def get_type(self):
    return "tests"
  #

#

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2014, 2015 EDF Lab
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Cédric AGUERRE (EDF Lab)

from yamm.core.framework.version import FrameworkVersion

version_name = "DEV"

class DEV(FrameworkVersion):

  def __init__(self, verbose=0, flavour = ""):
    FrameworkVersion.__init__(self, version_name, verbose, flavour)
    self.accepted_flavours=[]
  #

  def get_tag(self):
    return "DEV"
  #

  def configure_softwares(self):
    # prerequisites (public)
    self.add_software("JEDBROWN_CMAKE_MODULES", "de0ad5956185625c12f19da530d502022fe889d3")
    self.add_software("INIPARSER", "3.1")
    self.add_software("MED", "3.0.6")
    self.add_software("SUNDIALS", "2.5.0")
    self.add_software("GMIC", "1.6.0.0")
    self.add_software("NEPER", "2.0.2")
    self.add_software("PETSC", "3.1-p8")
    self.add_software("PINK", "5fec1b7dda8b")
    self.add_software("PYXL", "0.51")
    self.add_software("LIBNPY", "0.5")
    self.add_software("PYTHON_LIBMED", "0.1.0")
    self.add_software("ASSIMULO", "2.2")
    self.add_software("FFTW", "3.3.4")
    self.add_software("TFEL", "2.0.1")

    # prerequisites (EDF)
    self.add_software("CRAFT", "1.0.12f")
    self.add_software("MADNEX", "1.3.1")
    self.add_software("KELKINS", "f1375fa57135")

    # valid versions are either trunk or tags/2013.1, tags/2013.2, ...

    # library
    self.add_software("MAPRESOURCES", "trunk")
    self.add_software("MAPUTILS", "trunk")
    self.add_software("MAPCORE", "trunk")
    self.add_software("MAPENGINE", "trunk")
    self.add_software("MAPNUMERIC", "trunk")
    self.add_software("MAPSH", "trunk")
    self.add_software("MAPTESTS", "trunk")

    # components
    self.add_software("C_IMAGE_2D_ALIGN", "trunk")
    self.add_software("C_IMAGE_2D_BRIGHTNESS_EQUALIZER", "trunk")
    self.add_software("C_IMAGE_2D_FOULING_RATE", "trunk")
    self.add_software("C_IMAGE_2D_INCLUSION_STATISTICS", "trunk")
    self.add_software("C_IMAGE_2D_THRESHOLD", "trunk")
    self.add_software("C_IMAGE_2D_UNCURTAIN", "trunk")
    self.add_software("C_IMAGE_3D_ALTITUDE_THICKNESS", "trunk")
    self.add_software("C_IMAGE_DOMAIN_PARTITION", "trunk")
    self.add_software("C_IMAGE_INPAINT", "trunk")
    self.add_software("C_IMAGE_ORIENTATION", "trunk")
    self.add_software("C_IMAGE_WELD_ORIENTATION", "trunk")

    self.add_software("C_POST_COBWEBPLOT", "trunk")
    self.add_software("C_POST_DISTRIBUTION_PROPERTIES", "trunk")
    self.add_software("C_POST_EXPERIMENTAL_LOOPS", "trunk")
    self.add_software("C_POST_GRID_FIELD", "trunk")
    self.add_software("C_POST_IMAGE_CORRELATION", "trunk")
    self.add_software("C_POST_POLY_CHAOS", "trunk")
    self.add_software("C_POST_POLYMER_KINETICS", "trunk")
    self.add_software("C_POST_POLYMER_GRAPHIC", "trunk")
    self.add_software("C_POST_SCATTERPLOT_SENSITIVITY", "trunk")
    self.add_software("C_POST_TABLE_FFT", "trunk")

    self.add_software("C_PRE_CT_SPECIMEN_MESH", "trunk")
    self.add_software("C_PRE_IMAGE2MESH_2D", "trunk")
    self.add_software("C_PRE_IMAGE2MESH_3D", "trunk")
    self.add_software("C_PRE_INTERFACE_MESH", "trunk")
    self.add_software("C_PRE_MORPHOLOGY_GRAVEL", "trunk")
    self.add_software("C_PRE_MORPHOLOGY_GRID_PROJECTION", "trunk")
#    self.add_software("C_PRE_MORPHOLOGY_INCLUSIONS", "trunk")
    self.add_software("C_PRE_MORPHOLOGY_SYNTHESIS_FRACTAL_INTERFACE", "trunk")
    self.add_software("C_PRE_MORPHOLOGY_SYNTHESIS_SPHERES", "trunk")
    self.add_software("C_PRE_MORPHOLOGY_SYNTHESIS_VORONOI", "trunk")
    self.add_software("C_PRE_POLYCRYSTAL_ORIENTATION", "trunk")
    self.add_software("C_PRE_POLYMER_DATA_MANAGEMENT", "trunk")
    self.add_software("C_PRE_POLYMER_KINETICS_STUDY", "trunk")
    self.add_software("C_PRE_RANDOM_EXPERIMENTAL_DESIGN", "trunk")

    self.add_software("C_SOLVER_COMPUTATION_UNIT", "trunk")
    self.add_software("C_SOLVER_CONCRETE_ASR", "trunk")
    self.add_software("C_SOLVER_CORROSION_EVOLUTION", "trunk")
    self.add_software("C_SOLVER_CRYSTAL_ALLOY_BEHAVIOUR", "trunk")
    self.add_software("C_SOLVER_DIFFUSION_FDVGRID", "trunk")
    self.add_software("C_SOLVER_ELASTICITY_FDVGRID", "trunk")
    self.add_software("C_SOLVER_GENERALISED_CORROSION", "trunk")
    self.add_software("C_SOLVER_HOMOGENISATION_MECHANICS", "trunk")
    self.add_software("C_SOLVER_PRIMARY_CHEMISTRY", "trunk")
    self.add_software("C_SOLVER_STIFF_ODE_1D", "trunk")

    self.add_software("C_TRANSVERSE_CADEEX2MAP", "trunk")
    self.add_software("C_TRANSVERSE_DISPLAY_MAP", "trunk")
    self.add_software("C_TRANSVERSE_EMPTY_C", "trunk")
    self.add_software("C_TRANSVERSE_EMPTY_PYTHON", "trunk")
    self.add_software("C_TRANSVERSE_INTERPOLATION", "trunk")

    # composites
    self.add_software("S_SCC_3D_ANALYSIS", "trunk")
    self.add_software("S_SCC_CORIOLIS", "trunk")
    self.add_software("S_POLYMERS_STUDY", "trunk")
    self.add_software("S_METHODS_PROBABILISTIC_STUDY", "trunk")

    # tests
    self.add_software("TEST_ASSIMULO", "trunk")
    self.add_software("TEST_CRAFT", "trunk")
    self.add_software("TEST_GMIC", "trunk")
    self.add_software("TEST_KELKINS2", "trunk")
    self.add_software("TEST_MADNEX", "trunk")
    self.add_software("TEST_NEPER", "trunk")
    self.add_software("TEST_PINK", "trunk")
    self.add_software("TEST_TFEL", "trunk")
  #

#

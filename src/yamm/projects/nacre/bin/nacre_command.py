#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import sys

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.core.framework.bin.yamm_command import FrameworkYammCommandsLauncher
from yamm.core.framework.bin.yamm_command import FrameworkYammCommand

class NacreYammCommandsLauncher(FrameworkYammCommandsLauncher):
  def __init__(self):
    FrameworkYammCommandsLauncher.__init__(self)
    self.notify_title = "YAMM for NACRE"
    self.commands["nacre_start_and_config_local_install"] = FrameworkYammCommand("Start and config local install", self.nacre_start_and_config_local_install)
    self.commands["nacre_start_offline_and_config_local_install"] = FrameworkYammCommand("Start offline and config local install", self.nacre_start_offline_and_config_local_install)
    self.commands["nacre_start_from_scratch_and_config_local_install"] = FrameworkYammCommand("Start from scratch and config local install", self.nacre_start_from_scratch_and_config_local_install)
    self.commands["nacre_config_local_install"] = FrameworkYammCommand("Config local install", self.nacre_config_local_install)
    self.commands["nacre_create_pack"] = FrameworkYammCommand("Create pack", self.nacre_create_pack)
    self.commands["nacre_run_tests"] = FrameworkYammCommand("Run tests", self.nacre_run_tests)

  def nacre_start_and_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.start()
    if ret:
      return self.nacre_config_local_install(args, kw)
    return ret

  def nacre_start_offline_and_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.make(executor_mode="build")
    if ret:
      return self.nacre_config_local_install(args, kw)
    return ret

  def nacre_start_from_scratch_and_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    self.yamm_project.delete_directories(delete_install=True)
    ret = self.yamm_project.start()
    if ret:
      return self.nacre_config_local_install(args, kw)
    return ret

  def nacre_config_local_install(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.config_local_install()

  def nacre_create_pack(self, args, kw):
    self.yamm_project.print_configuration()
    try:
      unie =  bool(int(kw.get('unie', 1)))
      base_archive_name =  kw.get('base_archive_name', "nacre-install")
      delete =  bool(int(kw.get('delete', 0)))
      runnable = bool(int(kw.get('runnable', 0)))
      return self.yamm_project.create_pack(unie=unie,
                                           base_archive_name=base_archive_name,
                                           delete=delete,
                                           runnable=runnable)
    except:
      return 0

  def nacre_run_tests(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.run_tests()
    return ret

if __name__ == "__main__":
  NacreYammCommandsLauncher().run()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from __future__ import print_function
import sys
import os

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)
from yamm.core.framework.bin.yamm_infos import FrameworkYammInfos
from yamm.projects.nacre.project import Project

class NacreYammInfos(FrameworkYammInfos):
  def get_project(self):
    return Project()
    
  def get_name(self):
    return "NACRE"
    
  def set_parser_args(self):
    FrameworkYammInfos.set_parser_args(self)
    self.subparsers_groups["project"]["project_infos"].add_argument("--get-linux-packages", 
                             action="store_true", 
                             dest="print_packages",
                             help="print packages that you need to install to be able to build NACRE")
    self.subparsers_groups["project"]["project_infos"].add_argument("--get-problems", 
                             action="store_true", 
                             dest="print_problems",
                             help="print informations about some common problems")
    
    self.parser.set_defaults(print_packages=False)
    self.parser.set_defaults(print_problems=False)
    
  def parse_args(self, args):
    FrameworkYammInfos.parse_args(self, args)
    if vars(args).get('print_packages'):
      print("")
      print("Packages to add on a standard Calibre 7 station:")
      print("libqt3-mt-dev (for versions < 2.4.0)")
      print("libxerces-c2-dev")
      print("libtar-dev")
      print("")

    if vars(args).get('print_problems'):
      print("")
      print("This FAQ try to help you for some common problems found by other users")
      print("")
      print("Test if all the minimum packages are installed in your computer")
      print("python nacre_infos.py project-info --get-linux-packages")


if __name__ == "__main__":
  NacreYammInfos().run()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import str
import os, sys
from PyQt4 import QtGui

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.core.framework.gui.basic_wizard import FrameworkBasicWizard
from yamm.core.framework.gui.basic_wizard import FrameworkBasicIntro
from yamm.core.framework.gui.basic_wizard import FrameworkBasicOptions
from yamm.core.framework.gui.basic_wizard import FrameworkBasicRemotes
from yamm.core.framework.gui.select_software_list import ManageSoftwareList

from yamm.projects.nacre.project import Project

class BasicWizard(FrameworkBasicWizard):

  def __init__(self, parent):
    self.oracle_sid_possible_values = ["ARD", "NRD", "AUN", "INT", "QBC", "SBC", "TO_BE_DEFINED"]
    FrameworkBasicWizard.__init__(self, parent, "Create a NACRE project", Project())
    self.setWindowTitle("Create a NACRE project")

  def get_yamm_project(self):
    return Project()

  def end(self, result):
    FrameworkBasicWizard.end(self, result)
    if self.result:
      self.adresse_musicale = str(self.field("adresse_musicale").toString())
      self.login_musicale = str(self.field("login_musicale").toString())
      self.oracle_sid = self.oracle_sid_possible_values[self.field("oracle_sid").toInt()[0]]
      self.login_oracle = str(self.field("login_oracle").toString())
      pass

  def add_pages(self):
    self.addPage(BasicIntro(self, self.get_yamm_project()))
    self.addPage(BasicOptions(self, self.get_yamm_project()))
    self.addPage(BasicRemotes(self, self.get_yamm_project(), self.oracle_sid_possible_values))
    self.addPage(ManageSoftwareList(self, self.get_yamm_project()))

class BasicIntro(FrameworkBasicIntro):
  def __init__(self, parent, project):
    FrameworkBasicIntro.__init__(self, parent, project)

class BasicOptions(FrameworkBasicOptions):

  def __init__(self, parent, project):
    FrameworkBasicOptions.__init__(self, parent, project)

  def setPageLayout(self):
    FrameworkBasicOptions.setPageLayout(self)

  def registerFields(self):
    FrameworkBasicOptions.registerFields(self)

  def setDefaultValues(self):
    self.current_topDir = os.path.join(self.home_dir, "nacre")
    FrameworkBasicOptions.setDefaultValues(self)

class BasicRemotes(FrameworkBasicRemotes):

  def __init__(self, parent, project, oracle_sid_possible_values):
    self.oracle_sid_possible_values = oracle_sid_possible_values
    FrameworkBasicRemotes.__init__(self, parent, project)

  def setWidgets(self):
    FrameworkBasicRemotes.setWidgets(self)

    # Remote configuration
    rem_groupBox = QtGui.QGroupBox("Mandatory network options")
    rem_layout = QtGui.QGridLayout(rem_groupBox)
    label = QtGui.QLabel("Adresse musicale:")
    self.adresse_musicale_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(label, 0, 0)
    rem_layout.addWidget(self.adresse_musicale_lineEdit, 0, 1)

    label = QtGui.QLabel("Login musicale:")
    self.login_musicale_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(label, 1, 0)
    rem_layout.addWidget(self.login_musicale_lineEdit, 1, 1)

    label = QtGui.QLabel("Login Oracle:")
    self.login_oracle_lineEdit = QtGui.QLineEdit()
    rem_layout.addWidget(label, 2, 0)
    rem_layout.addWidget(self.login_oracle_lineEdit, 2, 1)

    label = QtGui.QLabel("Oracle SID:")
    self.oracle_sid_comboBox = QtGui.QComboBox()
    self.oracle_sid_comboBox.addItems(self.oracle_sid_possible_values)
    rem_layout.addWidget(label, 3, 0)
    rem_layout.addWidget(self.oracle_sid_comboBox, 3, 1)

#    rem_groupBox.setLayout(rem_layout)
    self.widgets_in_layout.append(rem_groupBox)

  def registerFields(self):
    FrameworkBasicRemotes.registerFields(self)
    # Fields
    self.registerField("adresse_musicale*", self.adresse_musicale_lineEdit)
    self.registerField("login_musicale*", self.login_musicale_lineEdit)
    self.registerField("oracle_sid", self.oracle_sid_comboBox)
    self.registerField("login_oracle*", self.login_oracle_lineEdit)

  def setDefaultValues(self):
    FrameworkBasicRemotes.setDefaultValues(self)
    self.categoriesButtons["composante"].setChecked(True)

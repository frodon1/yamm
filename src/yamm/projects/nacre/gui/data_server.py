#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import str
import os, sys
from PyQt4 import QtGui, uic

yamm_default_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../../..")
sys.path.append(yamm_default_directory)

# This import is used by the file list_data_servers_widget.ui
from yamm.core.framework.gui import icons_rc  # @UnusedImport
from yamm.projects.nacre import serveurs_donnees

class NacreListDataServers(QtGui.QWidget):
  def __init__(self, parent):
    QtGui.QWidget.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "list_data_servers_widget.ui"), self)
    self.sde_model = QtGui.QStandardItemModel(self)
    self.sdr_model = QtGui.QStandardItemModel(self)
    self.updateList()
    self.sde_listView.setModel(self.sde_model)
    self.sdr_listView.setModel(self.sdr_model)

  def updateList(self):
    self.sde_model.clear()
    self.sdr_model.clear()
    for sde in sorted(self.parent().sde_dict.keys()):
      self.sde_model.appendRow(QtGui.QStandardItem(sde))
    for sdr in sorted(self.parent().sdr_dict.keys()):
      self.sdr_model.appendRow(QtGui.QStandardItem(sdr))

  def editSde(self, index):
    sdeName = str(self.sde_model.data(index).toString())
    self.addSde(self.parent.sde_dict.get(sdeName, None))

  def editSdr(self, index):
    sdrName = str(self.sdr_model.data(index).toString())
    self.addSdr(self.parent.sdr_dict.get(sdrName, None))

  def addSde(self, sde=None):
    w = NacreAddSde(self, sde)
    if w.exec_():
      name = str(w.name.text())
      local_sde = w.localSettings.isChecked()
      update = False
      if name not in list(self.parent.sde_dict.keys()):
        update = True
      profils = []
      path = ""
      host = str(w.host.text())
      if local_sde:
        profils = " ".join(str(w.profils.text()).split()).split(',')
        path = str(w.path.text())
        host = ''
        if w.sde_use_hostname.isChecked():
          host = serveurs_donnees.socket.gethostname()
        if w.sde_use_fqdn.isChecked():
          host = serveurs_donnees.getfqdn()
      invites = " ".join(str(w.invites.text()).split()).split(',')
      port = w.port.value()
      port_polling = w.port_polling.value()
      sde_auth = w.sde_auth.isChecked()
      self.parent.add_sde(name, profils, host, path, port, port_polling, local_sde, invites, sde_auth)
      if update:
        item = QtGui.QStandardItem(name)
        self.sde_model.appendRow(item)
        self.sde_listView.scrollTo(item.index())

  def addSdr(self, sdr=None):
    w = NacreAddSdr(self, sdr)
    if w.exec_():
      name = str(w.name.text())
      newSdr = True
      if sdr:
        newSdr = False
      elif name in list(self.parent.sdr_dict.keys()):
        newSdr = False
      order = w.order.value()
      version = str(w.version.currentText())
      profils = " ".join(str(w.profils.text()).split()).split(',')
      host = str(w.host.text())
      port = w.port.value()
      origin = str(w.origin.text())
      start = w.start.isChecked()
      self.parent.add_sdr(name, order, version, profils, host, port, origin, start)
      if newSdr:
        item = QtGui.QStandardItem(name)
        self.sdr_model.appendRow(item)
        self.sdr_listView.scrollTo(item.index())

  def removeSde(self):
    index = self.sde_listView.currentIndex()
    sdeName = str(self.sde_model.data(index).toString())
    if sdeName in list(self.parent.sde_dict.keys()):
      del self.parent.sde_dict[sdeName]
      self.sde_model.removeRow(index.row())

  def removeSdr(self):
    index = self.sdr_listView.currentIndex()
    sdrName = str(self.sdr_model.data(index).toString())
    if sdrName in list(self.parent.sdr_dict.keys()):
      del self.parent.sdr_dict[sdrName]
      self.sdr_model.removeRow(index.row())

class NacreAddSde(QtGui.QDialog):
  def __init__(self, parent, sde=None):
    QtGui.QDialog.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "add_sde.ui"), self)
    if sde is not None:
      self.name.setText(sde.name)
      self.profils.setText(",".join(sde.profils))
      self.host.setText(sde.host)
      self.localSettings.setChecked(sde.path != "")
      self.path.setText(sde.path)
      self.port.setValue(sde.port)
      self.port_polling.setValue(sde.port_polling)
      self.sde_auth.setChecked(sde.sde_auth)
      self.invites.setText(",".join(sde.invites))
      self.name.setEnabled(False)
      self.host.setFocus()
    self.enable_ok_button()

  def enable_ok_button(self):
    len_name = len(self.name.text())
    len_host = len(self.host.text())
    len_path = len(self.path.text())

    enable_test = len_name > 0 and (len_host > 0 or self.localSettings.isChecked() and len_path > 0)
    self.buttonBox.button(self.buttonBox.Ok).setEnabled(enable_test)

class NacreAddSdr(QtGui.QDialog):
  def __init__(self, parent, sdr=None):
    QtGui.QDialog.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "add_sdr.ui"), self)
    if sdr is not None:
      self.name.setText(sdr.name)
      self.order.setValue(sdr.order)
      version_index = serveurs_donnees.sdr_versions.index(sdr.version)
      self.version.setCurrentIndex(version_index)
      self.profils.setText(",".join(sdr.profils))
      self.host.setText(sdr.host)
      self.port.setValue(sdr.port)
      self.origin.setText(sdr.origin)
      self.start.setChecked(sdr.start)

      self.name.setEnabled(False)
      self.order.setFocus()
    self.enable_ok_button()

  def enable_ok_button(self):
    len_name = len(self.name.text())
    len_host = len(self.host.text())
    enable_test = len_name > 0 and len_host > 0
    self.buttonBox.button(self.buttonBox.Ok).setEnabled(enable_test)


class NacreListSde(QtGui.QDialog):
  def __init__(self, parent, gui_project):
    QtGui.QDialog.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "list_sde.ui"), self)
    self.gui_project = gui_project
    self.sde_model = QtGui.QStandardItemModel(self)
    self.updateList()
    self.listView.setModel(self.sde_model)

  def updateList(self):
    self.sde_model.clear()
    for sde in sorted(self.gui_project.sde_dict.keys()):
      self.sde_model.appendRow(QtGui.QStandardItem(sde))

  def editSde(self, index):
    sdeName = str(self.sde_model.data(index).toString())
    self.addSde(self.gui_project.sde_dict.get(sdeName, None))

  def addSde(self, sde=None):
    w = NacreAddSde(self, sde)
    if w.exec_():
      name = str(w.name.text()).replace(' ', '_')
      local_sde = w.localSettings.isChecked()
      update = False
      if name not in list(self.gui_project.sde_dict.keys()):
        update = True
      profils = []
      path = ""
      if local_sde:
        profils = " ".join(str(w.profils.text()).split()).split(',')
        path = str(w.path.text())
      invites = " ".join(str(w.invites.text()).split()).split(',')
      host = str(w.host.text())
      port = w.port.value()
      port_polling = w.port_polling.value()
      sde_auth = w.sde_auth.isChecked()
      self.gui_project.add_sde(name, profils, host, path, port, port_polling, local_sde, invites, sde_auth)
      # self.gui_project.create_gui_project_file()
      if update:
        item = QtGui.QStandardItem(name)
        self.sde_model.appendRow(item)
        self.listView.scrollTo(item.index())

  def removeSde(self):
    index = self.listView.currentIndex()
    sdeName = str(self.sde_model.data(index).toString())
    if sdeName in list(self.gui_project.sde_dict.keys()):
      del self.gui_project.sde_dict[sdeName]
      self.gui_project.create_gui_project_file()
      self.sde_model.removeRow(index.row())

class NacreListSdr(QtGui.QDialog):
  def __init__(self, parent, gui_project):
    QtGui.QDialog.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "list_sdr.ui"), self)
    self.gui_project = gui_project
    self.sdr_model = QtGui.QStandardItemModel(self)
    self.updateList()
    self.listView.setModel(self.sdr_model)

  def updateList(self):
    self.sdr_model.clear()
    for sdr in sorted(self.gui_project.sdr_dict.keys()):
      self.sdr_model.appendRow(QtGui.QStandardItem(sdr))

  def editSdr(self, index):
    sdrName = str(self.sdr_model.data(index).toString())
    self.addSdr(self.gui_project.sdr_dict.get(sdrName, None))

  def addSdr(self, sdr=None):
    w = NacreAddSdr(self, sdr)
    if w.exec_():
      name = str(w.name.text())
      newSdr = True
      if sdr:
        newSdr = False
      elif name in list(self.gui_project.sde_dict.keys()):
        newSdr = False
      order = w.order.value()
      version = str(w.version.currentText())
      profils = " ".join(str(w.profils.text()).split()).split(',')
      host = str(w.host.text())
      port = w.port.value()
      origin = str(w.origin.text())
      start = w.start.isChecked()
      self.gui_project.add_sdr(name, order, version, profils, host, port, origin, start)
      # self.gui_project.create_gui_project_file()
      if newSdr:
        item = QtGui.QStandardItem(name)
        self.sdr_model.appendRow(item)
        self.listView.scrollTo(item.index())

  def removeSdr(self):
    index = self.listView.currentIndex()
    sdrName = str(self.sdr_model.data(index).toString())
    if sdrName in list(self.gui_project.sdr_dict.keys()):
      del self.gui_project.sdr_dict[sdrName]
      self.gui_project.create_gui_project_file()
      self.sdr_model.removeRow(index.row())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os, sys

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.core.base import misc
from yamm.core.framework.gui.edit_project_window import FrameworkEditProjectWindow

from yamm.projects.nacre     import project
from yamm.projects.nacre     import serveurs_donnees
from yamm.projects.nacre.gui import gui_project
from yamm.projects.nacre.gui import data_server

class EditProjectWindow(FrameworkEditProjectWindow):

  def init_variables(self):
    FrameworkEditProjectWindow.init_variables(self)
    self.sde_dict = self.yamm_gui_project.sde_dict
    self.sdr_dict = self.yamm_gui_project.sdr_dict

  def init_widgets(self):
    FrameworkEditProjectWindow.init_widgets(self)
    self.data_servers_widget = data_server.NacreListDataServers(self)
    self.central_tab.addTab(self.data_servers_widget, "Data servers")
    self.init_data_servers_tab()

  def add_sde(self, name, profils = None, host="", path="", port=24780,
              port_polling=24760, start=True, invites = None, sde_auth=False):
    self.sde_dict[name] = serveurs_donnees.SDE(misc.Log("NACRE GUI SDE"), name,
        profils, host, path, port, port_polling, start, invites, sde_auth)

  def add_sdr(self, name, order, version = "P04", profils = None, host="", port=24770, origin="", start=False):
    self.sdr_dict[name] = serveurs_donnees.SDR(misc.Log("NACRE GUI SDR"), name, order, version, profils, host, port, origin, start)

  def init_data_servers_tab(self):
    self.data_servers_widget.parent = self

  def get_empty_yamm_project(self):
    return project.Project()

  def get_empty_yamm_gui_project(self):
    return gui_project.YammGuiProject()

  def save_from_gui(self):
    ret = FrameworkEditProjectWindow.save_from_gui(self)
    if ret:
      self.yamm_gui_project.nacre_config_local_install = self.parent().\
              nacre_config_local_install_checkbox.isChecked()
  
      # Passage en revue des serveurs de données
      for sde in list(self.sde_dict.values()):
        self.yamm_gui_project.add_sde(sde)
      for sdr in list(self.sdr_dict.values()):
        self.yamm_gui_project.add_sdr(sdr)
      self.yamm_gui_project.create_gui_project_file()
    return ret

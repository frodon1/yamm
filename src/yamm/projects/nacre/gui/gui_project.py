#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from past.builtins import execfile
from builtins import str
from yamm.core.base import misc
from yamm.core.framework.gui.gui_project import FrameworkYammGuiProject

from yamm.projects.nacre.serveurs_donnees import SDE, SDR

gui_file = """
nacre_config_local_install    = %nacre_config_local_install
sde_dict = %sde_dict
sdr_dict = %sdr_dict
"""
gui_file = misc.PercentTemplate(gui_file)

add_sde_str = """
yamm_project.add_sde("%name",
                    profils = %profils,
                    host="%host",
                    path="%path",
                    port=%port,
                    port_polling=%port_polling,
                    start=%start,
                    invites=%invites,
                    sde_auth=%sde_auth)
"""
add_sde_str = misc.PercentTemplate(add_sde_str)

add_sdr_str = """
yamm_project.add_sdr("%name",
                    order = %order,
                    version = "%version",
                    profils = %profils,
                    host="%host",
                    port=%port,
                    origin="%origin",
                    start=%start)
"""
add_sdr_str = misc.PercentTemplate(add_sdr_str)

class YammGuiProject(FrameworkYammGuiProject):

  def __init__(self, project_class="nacre"):
    FrameworkYammGuiProject. __init__(self, project_class)

    # To configure the local installation after a compilation
    self.nacre_config_local_install = True
    self.sde_dict = {}
    self.sdr_dict = {}

  def clear(self):
    FrameworkYammGuiProject.clear(self)
    self.nacre_config_local_install = True
    self.sde_dict = {}
    self.sdr_dict = {}

  def add_sde(self, sde):
    self.sde_dict[sde.name] = sde

  def add_sdr(self, sdr):
    self.sdr_dict[sdr.name] = sdr

  def create_gui_project_file(self):
    FrameworkYammGuiProject.create_gui_project_file(self)
    filetext = gui_file.substitute(nacre_config_local_install=self.nacre_config_local_install,
                                   sde_dict=str(self.sde_dict),
                                   sdr_dict=str(self.sdr_dict))
    with open(self.gui_filename, 'a') as gui_project_file:
      gui_project_file.write(filetext)

  def get_start_text_project_files(self):
    filetext = FrameworkYammGuiProject.get_start_text_project_files(self)
    for sde in list(self.sde_dict.values()):
      filetext += add_sde_str.substitute(name=sde.name,
                              profils=str(sde.profils),
                              host=sde.host,
                              path=sde.path,
                              port=sde.port,
                              port_polling=sde.port_polling,
                              start=sde.start,
                              invites=str(sde.invites),
                              sde_auth=sde.sde_auth)
    for sdr in list(self.sdr_dict.values()):
      filetext += add_sdr_str.substitute(name=sdr.name,
                              order=sdr.order,
                              version=sdr.version,
                              profils=str(sdr.profils),
                              host=sdr.host,
                              port=sdr.port,
                              origin=sdr.origin,
                              start=sdr.start)
    return filetext

  def load_project(self, filename):
    FrameworkYammGuiProject.load_project(self, filename)
    exec_dict = {}
    execfile(filename, exec_dict)
    self.nacre_config_local_install = exec_dict.get("nacre_config_local_install", True)
    for sde_name, sde_parameters in list(exec_dict.get("sde_dict", {}).items()):
      if type(sde_parameters) is dict:
        self.sde_dict[sde_name] = SDE(misc.Log("NACRE GUI SDE"), **sde_parameters)
      else:
        self.sde_dict[sde_name] = SDE(misc.Log("NACRE GUI SDE"), *sde_parameters)
    for sdr_name, sdr_parameters in list(exec_dict.get("sdr_dict", {}).items()):
      if type(sdr_parameters) is dict:
        self.sdr_dict[sdr_name] = SDR(misc.Log("NACRE GUI SDR"), **sdr_parameters)
      else:
        self.sdr_dict[sdr_name] = SDR(misc.Log("NACRE GUI SDR"), *sdr_parameters)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os, sys
from PyQt4 import QtGui, uic

yamm_default_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../../..")
sys.path.append(yamm_default_directory)

from yamm.core.framework.gui.main_project_window import FrameworkProjectWindow

from yamm.projects.nacre.project import Project
from yamm.projects.nacre.gui     import edit_project_window

class ProjectWindow(FrameworkProjectWindow):

  def __init__(self, parent, gui_project, yamm_gui):
    FrameworkProjectWindow.__init__(self, parent, gui_project, yamm_gui)
    self.nacre_bin_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../bin")

  def init_widgets(self):
    FrameworkProjectWindow.init_widgets(self)

    self.nacre_config_local_install_checkbox = QtGui.QCheckBox("configure")
    self.nacre_config_local_install_button = QtGui.QPushButton("Configure local install")
    self.nacre_create_pack_button = QtGui.QPushButton("Create pack")
    self.nacre_run_tests_button = QtGui.QPushButton("Run tests")

    self.actions_groupBox.layout().insertWidget(3, self.nacre_config_local_install_checkbox)
    self.actions_groupBox.layout().addWidget(self.nacre_config_local_install_button)
    self.actions_groupBox.layout().addWidget(self.nacre_create_pack_button)
    self.actions_groupBox.layout().addWidget(self.nacre_run_tests_button)

    self.nacre_config_local_install_checkbox.clicked.connect(self.nacre_update_config_local_install)
    self.nacre_config_local_install_button.clicked.connect(self.nacre_config_local_install_command)
    self.nacre_create_pack_button.clicked.connect(self.nacre_create_pack_command)
    self.nacre_run_tests_button.clicked.connect(self.nacre_run_tests_command)

    self.nacre_config_local_install_checkbox.setChecked(self.gui_project.nacre_config_local_install)
    
    self.pack_dlg = uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "create_pack.ui"))

    #self.nacre_manage_sde_button = QtGui.QPushButton("Manage SDE")
    #self.nacre_manage_sdr_button = QtGui.QPushButton("Manage SDR")
    #self.specific_groupBox_layout = QtGui.QVBoxLayout()
    #self.specific_groupBox_layout.addWidget(self.nacre_manage_sde_button)
    #self.specific_groupBox_layout.addWidget(self.nacre_manage_sdr_button)
    #self.specific_groupBox.setLayout(self.specific_groupBox_layout)
    #self.specific_groupBox.setTitle("Data servers")
    #self.specific_groupBox.setVisible(True)
    # self.line_specific_actions.setVisible(True)

    #self.nacre_manage_sde_button.clicked.connect(self.nacre_manage_sde)
    #self.nacre_manage_sdr_button.clicked.connect(self.nacre_manage_sdr)

  def nacre_update_config_local_install(self):
    self.gui_project.nacre_config_local_install = self.nacre_config_local_install_checkbox.isChecked()
    self.gui_project.create_gui_project_file()

  def start_command(self):
    if self.nacre_config_local_install_checkbox.isChecked():
      FrameworkProjectWindow.start_command(self, "nacre_start_and_config_local_install")
    else:
      FrameworkProjectWindow.start_command(self)

  def start_offline_command(self):
    if self.nacre_config_local_install_checkbox.isChecked():
      FrameworkProjectWindow.start_offline_command(self, "nacre_start_offline_and_config_local_install")
    else:
      FrameworkProjectWindow.start_offline_command(self)

  def start_from_scratch_command(self):
    if self.nacre_config_local_install_checkbox.isChecked():
      FrameworkProjectWindow.start_from_scratch_command(self, "nacre_start_from_scratch_and_config_local_install")
    else:
      FrameworkProjectWindow.start_from_scratch_command(self)

  def nacre_config_local_install_command(self):
    command = self.prepare_command("nacre_config_local_install")
    self.exec_command(command)

  def nacre_create_pack_command(self):
    if self.pack_dlg.exec_():
      command = self.prepare_command("nacre_create_pack")
      kw = {}
      kw['unie'] = 1 if self.pack_dlg.unie.isChecked() else 0
      kw['delete'] = 1 if self.pack_dlg.delete_pack.isChecked() else 0
      kw['base_archive_name'] = "nacre-install"
      kw['runnable'] = 1 if self.pack_dlg.runnable.isChecked() else 0
      self.exec_command(command, [], kw)

  def nacre_run_tests_command(self):
    command = self.prepare_command("nacre_run_tests")
    self.exec_command(command)

  def get_command_script(self):
    return os.path.join(self.nacre_bin_directory, "nacre_command.py")

  def get_yamm_project_edit_window(self):
    return edit_project_window.EditProjectWindow(self, self.yamm_gui, "edit", self.gui_project)

  def get_empty_yamm_project(self):
    return Project()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import datetime
import os
import shutil
import subprocess
import tempfile
import time

from yamm.core.base import misc
from yamm.core.framework.installer import FrameworkInstaller
from yamm.projects.nacre.installer.strings import installer_decompress_usage
from yamm.projects.nacre.installer.strings import installer_decompress_template
from yamm.projects.brtv.software import BrtvSoftware

class NacreInstaller(FrameworkInstaller):
  def __init__(self, name, logger, topdirectory, softwares, executor,
               delete=False, runnable=False, base_archive_name="nacre-install"):
    FrameworkInstaller.__init__(self, name, logger, topdirectory, softwares,
                                executor, delete, runnable)

    self.base_archive_name = base_archive_name
    self.nacre_version = self.executor.version_object.get_nacre_version()
    has_prerequisites = False
    self.etape = 1
    self.nb_etapes = 1

    self.install_multi_soft = self.map_name_to_softwares.get("INSTALL_MULTI",
                                                             None)
    if not self.install_multi_soft:
      self.logger.error("Le software INSTALL_MULTI est manquant")
    self.nb_etapes += 1

    self.nacre_soft = self.map_name_to_softwares.get("NACRE_INFO", None)
    if not self.nacre_soft:
      self.logger.error("Le software NACRE_INFO est manquant")

    for software in self.softwares:
      if software.get_type() == "prerequisite" and software.get_pack_name():
        has_prerequisites = True
      if software.get_type() == "composante" and software.get_pack_name():
        self.nb_etapes += 1
    if has_prerequisites:
      self.nb_etapes += 1

    self.work_dir = os.path.join(self.executor.options.get_global_option("main_software_directory"), "install", "work")
    home_dir = os.path.join(self.executor.options.get_global_option("main_software_directory"), "install", "home")
    self.home_bin_dir = os.path.join(home_dir, "bin")
    self.home_lib_dir = os.path.join(home_dir, "lib")
    for directory in [self.work_dir, self.home_bin_dir, self.home_lib_dir]:
      if os.path.exists(directory):
        self.nb_etapes += 1

    if not topdirectory:
      self.topdirectory = os.path.join(self.executor.options.get_global_option("packs_directory"), "pack")

    #self.logger.info("Installer %s initialisation ends"%self.name)
    pass

  def begin(self):
    #self.logger.info("Installer %s begin step starts"%self.name)
    # Create pack dir (remove existing one)
    if not os.path.exists(self.topdirectory):
      os.makedirs(self.topdirectory)
    #self.logger.info("Installer %s begin step ends"%self.name)
    return True

  def create(self):
    #self.logger.info("Installer %s create step starts"%self.name)
    t_tgz = int(time.time())

    # Step 1: Copie des fichiers nécessaire pour l'installation
    self.logger.info("Composante d'installation...")
    rep_nacre_install = os.path.join(self.nacre_soft.install_directory,
                                     'install')
    for script in('inst_cnst', 'inst_fnct'):
#                   'install_nacre.sh', 'config_nacre.sh'):
      try:
        srcname = os.path.join(rep_nacre_install, script)
        self.logger.info("Copying file {0}".format(srcname))
        shutil.copy2(srcname, self.topdirectory)
      except (IOError, os.error) as why:
        self.logger.error("Files copy from NACRE_INFO failed: {0}".format(why))

    ignore = shutil.ignore_patterns('*.log', '.yamm')
    names = os.listdir(self.install_multi_soft.install_directory)
    ignored_names = ignore(self.install_multi_soft.install_directory, names)
    for name in names:
      if name in ignored_names:
        continue
      srcname = os.path.join(self.install_multi_soft.install_directory, name)
      dstname = os.path.join(self.topdirectory, name)
      try:
        self.logger.info("Copying file {0}".format(srcname))
        shutil.copy2(srcname, dstname)
      except (IOError, os.error) as why:
        self.logger.error("Files copy from INSTALL_MULTI failed: {0}".format(why))
        
    # Step 2: Création de l'archive du répertoire work
    if os.path.exists(self.work_dir):
      self.logger.info("Composante WORK...")
      target_file_path = "{0}/{1}_work.tar.gz".format(self.topdirectory, self.base_archive_name)
      cmd = "tar czf {0} --exclude-vcs --exclude=*.log --exclude=.yamm -C {1} .".format(target_file_path, self.work_dir)
      self.logger.debug(cmd)
      try:
        ret = subprocess.call(cmd, shell=True)
        if ret != 0:
          self.logger.error("Composante WORK: KO ({0})".format(ret))
      except OSError as e:
        self.logger.error("Execution of command {0} failed: {0}".format(cmd, e))
      misc.create_sha1(target_file_path, target_file_path+".sha1")

    # Step 3: création de l'archive des prérequis (home/lib/[prerequis])
    # On ne prend pas les prerequis qui viennent de la BRTV
    prerequisites_list = [soft for soft in self.softwares \
                          if soft.get_type() == "prerequisite" \
                          and soft.get_pack_name()
                          and not isinstance(soft, BrtvSoftware)]
    if prerequisites_list:
      self.logger.info("Composante prerequis...")
      tmpdir = tempfile.mkdtemp()
      libdir = os.path.join(tmpdir, "lib")
      os.makedirs(libdir)
      for prerequisite_soft in prerequisites_list:
        prerequisite_install_dir = prerequisite_soft.get_pack_name()
        if not prerequisite_install_dir:
          self.logger.error("Composante prerequis: KO")
        prerequisite_new_dir = os.path.join(libdir, prerequisite_install_dir)
        shutil.copytree(prerequisite_soft.install_directory, prerequisite_new_dir)
      target_file_path = "{0}/{1}_prerequis.tar.gz".format(self.topdirectory, self.base_archive_name)
      cmd = "tar czf {0} --exclude-vcs -C {1} lib".format(target_file_path, tmpdir)
      self.logger.debug(cmd)
      try:
        ret = subprocess.call(cmd, shell=True)
        if ret != 0:
          self.logger.error("Composante prerequis: KO ({0})".format(ret))
      except OSError as e:
        self.logger.error("Execution of command {0} failed: {0}".format(cmd, e))
      misc.create_sha1(target_file_path, target_file_path+".sha1")
      misc.fast_delete(tmpdir)
    
    t_tgz = int(time.time()) - t_tgz
    self.logger.info("Packs des composantes créés en {0}s".format(datetime.timedelta(seconds=t_tgz)))

    #self.logger.info("Installer %s create step ends"%self.name)
    return True

  def end(self):
    #self.logger.info("Installer %s end step starts"%self.name)
    installer_tgz_name = self.name
    installer_tgz_path = os.path.join(os.path.dirname(self.topdirectory),
                                      installer_tgz_name+".tar.gz")
    ret = self.create_pack(installer_tgz_path)
    if not ret:
      return ret
    if self.runnable:
      ret = self.create_runnable(installer_tgz_name)
      if not ret:
        return ret

    if self.delete:
      misc.fast_delete(self.topdirectory)
    return True
    #self.logger.info("Installer %s end step ends"%self.name)
  
  def get_decompress_content(self):
    installer_tgz_name = self.name + ".tar.gz"
    return installer_decompress_template.substitute(
        pack_dirname=os.path.basename(self.topdirectory),
        nacre_version=self.nacre_version,
        usage=installer_decompress_usage,
        installer_tgz_name=installer_tgz_name,
        title_deco="=" * len(self.nacre_version))

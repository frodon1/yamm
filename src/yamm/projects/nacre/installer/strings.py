#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.core.base import misc

installer_decompress_usage = """usage()
{
cat<<EOF
usage: $0 [-h] [-d] [-g] [-l inst_parm_loc] [-m inst_parm_multi]

Ce script installe Nacre dans une configuration maître/esclave avec des paramètres lus depuis 2
fichiers de configuration:
    - inst_parm_loc
    - inst_parm_multi

Options:
 -h    Affiche ce message
 -d    Decompresse seulement
 -g    Ne supprime pas le répertoire après installation
 -l    Fichier de paramètres spécifiques aux machines (default: inst_parm_loc)
 -m    Liste des machines et fichier de configuration principal (default: inst_parm_multi)
EOF
}"""

#installer_decompress_usage = misc.PercentTemplate(installer_decompress_usage)

installer_decompress_template = """
£{usage}
DECOMPRESS=0
KEEPDIR=0
INSTPARMLOC=""
INSTPARMMULTI=""

while getopts ":dgl:m:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    d)
      DECOMPRESS=1
      ;;
    g)
      KEEPDIR=1
      ;;
    l)
      INSTPARMLOC="$OPTARG"
      ;;
    m)
      INSTPARMMULTI="$OPTARG"
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "L'option -$OPTARG nécessite un argument." >&2
      exit 1
      ;;
  esac
done

echo ""
echo "=================================£{title_deco}"
echo "Self Extracting Nacre £{nacre_version} Installer"
echo "=================================£{title_deco}"
echo ""

if test $DECOMPRESS = 0
then
  DEFAULTINSTPARMLOC="$(pwd)/inst_parm_loc"
  eval INSTPARMLOC="$INSTPARMLOC" # to expand the tilde
  test -z "$INSTPARMLOC" && INSTPARMLOC="$DEFAULTINSTPARMLOC"
  
  DEFAULTINSTPARMMULTI="$(pwd)/inst_parm_multi"
  eval INSTPARMMULTI="$INSTPARMMULTI" # to expand the tilde
  test -z "$INSTPARMMULTI" && INSTPARMMULTI="$DEFAULTINSTPARMMULTI"
  
  test ! -e "$INSTPARMLOC" && die "Fichier inst_parm_loc manquant"
  test ! -e "$INSTPARMMULTI" && die "Fichier inst_parm_multi manquant"
fi

tail -n+$ARCHIVE "$0" > £{installer_tgz_name}

check £{installer_tgz_name}

if test -d £{pack_dirname}
then
  newname="£{pack_dirname}-$(date +"%F").bak"
  if test -d ${newname}
  then
    rm -rf ${newname}
  fi
  mv £{pack_dirname} ${newname}
fi

echo "Décompression de l'archive..."
bar_cat £{installer_tgz_name}
echo "Terminé"

rm £{installer_tgz_name}

if test $DECOMPRESS = 1
then
  exit 0
fi

cp "$INSTPARMLOC" £{pack_dirname}/inst_parm_loc
cp "$INSTPARMMULTI" £{pack_dirname}/inst_parm_multi
cd £{pack_dirname}

echo "Installation..."
./install_nacre_multi.sh
if test $? = 0; then
  cp CR.* ..
  cd ..
  if test $KEEPDIR = 0
  then
    rm -rf £{pack_dirname}
  fi
  echo "Terminé"
else
  echo "Erreur durant l'installation"
fi

notifyCmd=$(which notify-send)
if test -n "$notifyCmd"; then
    $notifyCmd "Nacre" "Installation de Nacre £{nacre_version} terminée."
fi
"""

installer_decompress_template = misc.PoundTemplate(installer_decompress_template)


installer_cp_current_template = """
if not os.path.exists(%target):
  print "\x1b[1;31;47m WARNING \x1b[0m No such file : {0}".format(%target)
else:
  if not os.path.exists(%target_template):
    shutil.copyfile(%target,
                    %target_template)
  if not os.path.exists(%target_current):
    shutil.copyfile(%target_template,
                    %target_current)
"""
installer_cp_current_template = misc.PercentTemplate(installer_cp_current_template)

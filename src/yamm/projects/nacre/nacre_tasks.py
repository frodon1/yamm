#  Copyright (C) 2012, 2014, 2015 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)


import os
import getpass
import subprocess
from yamm.core.engine.tasks.generic_task import GenericTask
from yamm.core.base.misc import VerboseLevels

class NacreTask(GenericTask):
  """
  This is a base class for NACRE task classes
  """
  
  def __init__(self, taskname='[NACRE TASK]', verbose_level=VerboseLevels.INFO,
               nacre_install_dir = None):
    GenericTask.__init__(self, taskname, verbose_level)
    self.nacre_install_dir = nacre_install_dir
    self.yamm_nacre_log_dir = os.path.join(self.nacre_install_dir, '.yamm')

  def prepare_work(self, software_config, space, command_log):
    self.command_log = command_log
    self.space = space
    return ""
  
  def createLogDirectory(self):
    # Create log directory
    if not os.path.isdir(self.yamm_nacre_log_dir):
      try:
        os.makedirs(self.yamm_nacre_log_dir, 0o755)
      except:
        return False
    return True
  
  def anotherNacreIsRunning(self):
    # Check if another version NACRE is already running
    self.command_log.print_debug('Check if another NACRE is running')
    command = "ps -f -u {0} |grep -v {1} |grep sdr |grep -v grep "\
              "|wc |awk '{{print $1}}'"\
              .format(getpass.getuser(), self.nacre_install_dir)
    self.command_log.print_debug("Command is: {0}".format(command))
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    nbProcess = int(process.communicate()[0].split()[0]) # Le split enleve le \n
    return nbProcess != 0
  
  def currentNacreIsRunning(self):
    # Check if another version NACRE is already running
    self.command_log.print_debug('Check if current NACRE is running')
    command = "ps -f -u {0} |grep {1} |grep sdr |grep -v grep "\
              "|wc |awk '{{print $1}}'"\
              .format(getpass.getuser(), self.nacre_install_dir)
    self.command_log.print_debug("Command is: {0}".format(command))
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    nbProcess = int(process.communicate()[0].split()[0]) # Le split enleve le \n
    return nbProcess != 0

class StartNacreTask(NacreTask):
  """
    This task is called to start NACRE
  """
  def __init__(self, taskname='[START NACRE TASK]', verbose_level=VerboseLevels.INFO,
               nacre_install_dir = None):
    NacreTask.__init__(self, taskname, verbose_level, nacre_install_dir)
    self.nacre_log_file = os.path.join(self.yamm_nacre_log_dir, 'nacre_start.log')

  def execute(self, dependency_command, executor, current_software):
    self.command_log.print_begin_work("Start Nacre", space = self.space)

    # Check if another version NACRE is already running
    if self.anotherNacreIsRunning():
      return "Error: another instance of NACRE is already running"

    # Check if the current version of NACRE is already running
    if self.currentNacreIsRunning():
      self.command_log.print_end_work('NACRE is already running')
      return ""
    
    # No instance of NACRE is running: start a new one
    if not self.createLogDirectory():
      return "Error in creating NACRE log directory: %s" % self.yamm_nacre_log_dir
      
    # Create SALOME application
    command = '( cd {0} ; '.format(os.path.join(self.nacre_install_dir,
                                          'install', 'home', 'nacre', 'bin'))
    command += './start_calcs ; sleep 1 ;'
    command += './start_services ; sleep 1 ;'
    command += './start_sdr ; sleep 1 ;'
    command += './start_sde ; sleep 1 ;'
    command += './start_sdp ; sleep 1 ;'
    command += ') > {0} 2>&1'.format(self.nacre_log_file)
    # Execute command
    msg = self.execute_command("Start NACRE", command)
    if msg:
      return msg

    self.command_log.print_end_work()
    self.command_log.print_info("A NACRE instance was started from directory: %s" % self.nacre_install_dir,
                                space = self.space + 2)
    return ""


class StopNacreTask(NacreTask):
  """
    This task is called to stop a running NACRE
  """
  def __init__(self, taskname='[STOP NACRE TASK]', verbose_level=VerboseLevels.INFO,
               nacre_install_dir = None):
    NacreTask.__init__(self, taskname, verbose_level, nacre_install_dir)
    self.nacre_log_file = os.path.join(self.yamm_nacre_log_dir, 'nacre_stop.log')

  def execute(self, dependency_command, executor, current_software):
    self.command_log.print_begin_work("Stop NACRE", space = self.space)

    # Check if the current version of NACRE is already stopped
    if not self.currentNacreIsRunning():
      self.command_log.print_end_work()
      return ""
    
    # No instance of NACRE is running: start a new one
    if not self.createLogDirectory():
      return "Error in creating NACRE log directory: %s" % self.yamm_nacre_log_dir
      
    # Create SALOME application
    command = '( cd {0} ; '.format(os.path.join(self.nacre_install_dir,
                                            'install', 'home', 'nacre', 'bin'))
    command += './stop_all'
    command += ') >> {0} 2>&1'.format(self.nacre_log_file)
    # Execute command
    msg = self.execute_command("Stop NACRE", command)
    if msg:
      return msg

    self.command_log.print_end_work()
    self.command_log.print_info("A NACRE instance was stopped from directory: %s" % self.nacre_install_dir,
                                space = self.space + 2)
    return ""

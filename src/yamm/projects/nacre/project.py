#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from __future__ import print_function
from __future__ import division
from builtins import range
from past.utils import old_div
import os
import sys
import socket
import shutil
import datetime
import locale
from operator import attrgetter
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.base.misc import LoggerException
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command

from yamm.projects.nacre.softwares import prerequisites as default_prerequisites
from yamm.projects.nacre.softwares import composantes   as default_composantes
from yamm.projects.nacre.softwares import tools         as default_tools
from yamm.projects.nacre           import versions      as default_versions

from yamm.projects.nacre import serveurs_donnees
from yamm.projects.nacre.installer.nacre_installer import NacreInstaller

from yamm.projects.brtv.software import BrtvSoftware

def get_ftp_server():
  return os.path.join(misc.get_ftp_server(), 'CASSIOPEE/NACRE/prerequisites')

class Project(FrameworkProject):

  def __init__(self, log_name="NACRE", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.define_nacre_options()
    self.init_nacre_vars()
    pass

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="NACRE", verbose=self.verbose_level)
    # Import des prerequis
    for prerequisite_name in default_prerequisites.softwares_list:
      self.add_software_in_catalog(prerequisite_name,
                                   "yamm.projects.nacre.softwares.prerequisites")
    # Import des composantes
    for module_name in default_composantes.softwares_list:
      self.add_software_in_catalog(module_name,
                                   "yamm.projects.nacre.softwares.composantes")
    # Import des tools
    for tool_name in default_tools.softwares_list:
      self.add_software_in_catalog(tool_name,
                                   "yamm.projects.nacre.softwares.tools")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.nacre.versions")
      pass
    self.logger.debug("End init catalog")
    pass

  def init_nacre_vars(self):
    # Dictionnaires des sde démarrés et non démarrés
    # Clé = nom du sde
    # Valeur = instance de serveurs_donnees.SDE
    self.sde_started = {}
    self.sde_not_started = {}
    # Dictionnaires des sdr démarrés et non démarrés
    # Clé = nom du sdr
    # Valeur = instance de serveurs_donnees.SDR
    self.sdr_started = {}
    self.sdr_not_started = {}
    self.sdp = 90

  def define_nacre_options(self):
    # Categories du projet NACRE
    self.options.add_category("prerequisite")
    self.options.add_category("tool")
    self.options.add_category("composante")

    self.options.add_option("packs_directory", "", ["global"])
    # From version 2.3, SSL mode is required
    self.options.add_option("nacre_ssl_mode", "", ["global"],
                            ["NONE", "KEY", "CA"])
    self.options.add_option("nacre_ssl_path", "", ["global"])
    self.options.add_option("nacre_ssl_pass", "", ["global"])
    self.options.add_option("nacre_ssl_key", "", ["global"])
    self.options.add_option("nacre_ssl_cert", "", ["global"])
    self.options.add_option("nacre_ssl_ca", "", ["global"])

    # Options pour les dépôts
    self.options.add_option("vcs_username", "",
                            ["global", "category", "software"])
    self.options.add_option("vcs_server", "",
                            ["global", "category", "software"])

    self.options.add_option("install_ref", "", ["global"])

    self.options.add_option("nb_calcs", 1, ["global"])
    self.options.add_option("nb_thread_cox", 1, ["global"])
    self.options.add_option("base_port", 1, ["global"])

    self.options.add_option("login_oracle", "", ["global"])
    self.options.add_option("oracle_sid", "", ["global"],
                            ["ARD", "NRD", "AUN", "INT", "QBC", "SBC",
                             "TO_BE_DEFINED"])

    self.options.add_option("tns_admin", "", ["global"])

    self.options.add_option("adresse_musicale", "", ["global"])
    self.options.add_option("login_musicale", "", ["global"])

    self.options.add_option("printer_name", "", ["global"])

    self.options.add_option("fichier_mdp_musicale", "", ["global"])
    self.options.add_option("fichier_mdp_sinfonie", "", ["global"])
    self.options.add_option("fichier_mdp_chiffrement", "", ["global"])

#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory",
                                                os.path.join(os.environ["HOME"],
                                                             "nacre"))
      pass
    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)
    # Set main directory for softwares
    if not current_command_options.get_global_option("main_software_directory"):
      current_command_options.set_global_option("main_software_directory",
          os.path.join(current_command_options.get_global_option("version_directory")))
      pass

    # Set Specific packs directory
    if not current_command_options.get_global_option("packs_directory"):
      current_command_options.set_global_option("packs_directory",
          os.path.join(current_command_options.get_global_option("top_directory"),
                       "packs"))
      pass

    # Set Specific SSL mode
    if not current_command_options.get_global_option("nacre_ssl_mode"):
      current_command_options.set_global_option("nacre_ssl_mode", "NONE")
      pass
    if not current_command_options.get_global_option("nacre_ssl_path"):
      current_command_options.set_global_option("nacre_ssl_path",
        os.path.join(current_command_options.get_global_option("version_directory"),
                     "install", "home", "nacre", "etc", "ssl"))
      pass
    if not current_command_options.get_global_option("nacre_ssl_pass"):
      current_command_options.set_global_option("nacre_ssl_pass", "keypass")
      pass
    host = socket.getfqdn().replace('.', '_')
    if not current_command_options.get_global_option("nacre_ssl_key"):
      current_command_options.set_global_option("nacre_ssl_key",
                                                "{0}_key.pem".format(host))
      pass
    if not current_command_options.get_global_option("nacre_ssl_cert"):
      current_command_options.set_global_option("nacre_ssl_cert",
                                                "{0}_cert.pem".format(host))
      pass
    if not current_command_options.get_global_option("nacre_ssl_ca"):
      current_command_options.set_global_option("nacre_ssl_ca",
                                                "{0}_cacert.pem".format(host))
      pass

    if not current_command_options.get_global_option("vcs_server"):
      current_command_options.set_global_option("vcs_server",
            "noev01tn.noe.edf.fr:9070/NCR")
      pass
    if not current_command_options.get_global_option("vcs_username"):
      current_command_options.set_global_option("vcs_username", "")
      pass

    if not current_command_options.get_global_option("install_ref"):
      current_command_options.set_global_option("install_ref", "mono")
      pass

    if not current_command_options.get_global_option("nb_calcs"):
      current_command_options.set_global_option("nb_calcs", 8)
      pass
    if not current_command_options.get_global_option("nb_thread_cox"):
      current_command_options.set_global_option("nb_thread_cox", 1)
      pass
    if not current_command_options.get_global_option("base_port"):
      current_command_options.set_global_option("base_port", 24700)
      pass

    if not current_command_options.get_global_option("login_oracle"):
      current_command_options.set_global_option("login_oracle", "NACRE")
      pass

    if not current_command_options.get_global_option("oracle_sid"):
      current_command_options.set_global_option("oracle_sid",
                                                "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("tns_admin"):
      current_command_options.set_global_option("tns_admin",
                                                "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("adresse_musicale"):
      current_command_options.set_global_option("adresse_musicale",
                                                "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("login_musicale"):
      current_command_options.set_global_option("login_musicale",
                                                "TO_BE_DEFINED")
      pass

    if not current_command_options.get_global_option("printer_name"):
      # Imprimante Clamart/B240
      current_command_options.set_global_option("printer_name",
                                                "B_240-LXT642-IN-54570")
      pass

    if not current_command_options.get_global_option("fichier_mdp_musicale"):
      current_command_options.set_global_option("fichier_mdp_musicale",
                                                os.path.join(os.path.expanduser('~'),
                                                             '.config', 'nacre', 'mdp_musicale'))

    if not current_command_options.get_global_option("fichier_mdp_sinfonie"):
      current_command_options.set_global_option("fichier_mdp_sinfonie",
                                                os.path.join(os.path.expanduser('~'),
                                                             '.config', 'nacre', 'mdp_sinfonie'))

    if not current_command_options.get_global_option("fichier_mdp_chiffrement"):
      current_command_options.set_global_option("fichier_mdp_chiffrement",
                                                os.path.join(os.path.expanduser('~'),
                                                             '.config', 'nacre', 'mdp_chiffrement'))

    if not current_command_options.is_category_option("prerequisite",
                                                      "source_type"):
      current_command_options.set_category_option("prerequisite",
                                                  "source_type", "archive")
    if not current_command_options.is_category_option("composante",
                                                      "source_type"):
      current_command_options.set_category_option("composante",
                                                  "source_type", "remote")
    if not current_command_options.is_category_option("tool",
                                                      "source_type"):
      current_command_options.set_category_option("tool",
                                                  "source_type", "remote")
    pass

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):

    for category in ['prerequisite', 'tool', 'composante']:
      if current_command_options.is_category_option(category,
                                                    'main_software_directory'):
        current_command_options.check_abs_path('main_software_directory',
                                               category=category,
                                               logger=self.logger)
      current_command_options.check_allowed_value('source_type',
                                       ["archive", "remote"],
                                       category=category,
                                       logger=self.logger)

    for option in ['packs_directory', 'nacre_ssl_path']:
      current_command_options.check_abs_path(option, logger=self.logger)

#######
#
# Méthodes internes pour les commandes
#
#######

  def update_configuration(self):
    # Platform
    misc.misc_platform = self.current_command_options.get_global_option("platform")
    if self.current_command_options.get_global_option("vcs_server") == "":
      self.current_command_options.set_global_option("vcs_server", "noev01tn.noe.edf.fr:9070/NCR")
      pass
    if self.current_command_options.get_global_option("archive_remote_address") == "":
      self.current_command_options.set_global_option("archive_remote_address", get_ftp_server())
      pass
    pass

#######
#
# Méthodes spécifiques au projet Nacre
#
#######

  def add_sdr(self, name, order, version="P04", profils=None, host="", port=24770, origin="", start=False):
    """
    Méthode pour ajouter un sdr au projet
    """
    if profils is None:
      profils = []
    if name in list(self.sdr_started.keys()) + list(self.sdr_not_started.keys()):
      self.logger.error("Le SDR %s existe déjà." % name)
      return None

    if start:
      if not host or host == "localhost":
        if self.get_global_option("nacre_ssl_mode") == 'NONE':
          host = serveurs_donnees.get_local_ip_address()
        else:
          host = serveurs_donnees.getfqdn()
        pass

      port_to_host = {}
      for mySdr in list(self.sdr_started.values()):
        port_to_host[mySdr.port] = mySdr.host
        pass

      if port in list(port_to_host.keys()) and host == port_to_host[port]:
        new_port = max(port_to_host.keys()) + 1
        self.logger.info("Le port {0} du SDR {2} est déjà utilisé. Il passe à {1}".format(port, new_port, name))
        port = new_port
        pass

      liste_order = [sdr.order for sdr in list(self.sdr_started.values())]
      max_liste_order = max(liste_order) if liste_order else 0
      if order in liste_order:
        self.logger.info("Le SDR {2} à l'ordre {0} est déjà utilisé. Il passe à {1}".format(order, max_liste_order + 1, name))
        order = max_liste_order + 1

      self.sdr_started[name] = serveurs_donnees.SDR(self.logger, name, order, version, profils, host, port, origin, start)
      self.logger.info("SDR démarré ajouté: {0}.".format(self.sdr_started[name]))
      return self.sdr_started[name]

    else:
      if not port:
        self.logger.error("Le port du SDR {0} est obligatoire.".format(name))
        return None

      if not host:
        self.logger.error("L'hôte du SDR {0} est obligatoire.".format(name))
        return None

      port_to_host = {}
      for mySdr in list(self.sdr_not_started.values()):
        port_to_host[mySdr.port] = mySdr.host
        pass

      if port in list(port_to_host.keys()) and host == port_to_host[port]:
        self.logger.error("The SDR {0} avec le port {1} et l'hôte {2} existe déjà.".format(name, port, host))
        return None

      self.sdr_not_started[name] = serveurs_donnees.SDR(self.logger, name, order, version, profils, host, port, origin, start)
      self.logger.info("SDR non démarré ajouté: {0}.".format(self.sdr_not_started[name]))
      return self.sdr_not_started[name]

    pass

  def add_sde(self, name, profils=None, invites=None, host="",
              path="", port=24780, port_polling=24760, start=True,
              sde_auth=False):
    """
    Méthode pour ajouter un sde au projet
    """
    if profils is None:
      profils = []
    if invites is None:
      invites = []
    if name in list(self.sde_started.keys()) + list(self.sde_not_started.keys()):
      self.logger.error("Le SDE %s existe déjà." % name)
      return None

    if start:
      if not host or host == "localhost":
        if self.get_global_option("nacre_ssl_mode") == 'NONE':
          host = serveurs_donnees.get_local_ip_address()
        else:
          host = serveurs_donnees.getfqdn()
        pass

      port_to_host = {}
      port_polling_to_host = {}
      current_sde_auth = None
      for mySde in list(self.sde_started.values()):
        port_to_host[mySde.port] = mySde.host
        port_polling_to_host[mySde.port_polling] = mySde.host
        current_sde_auth = mySde if mySde.sde_auth is True else None
        pass

      if sde_auth and current_sde_auth is not None:
        self.logger.error("Le SDE d'authentification est déjà défini: %s." % current_sde_auth.name)
        return None

      if port in list(port_to_host.keys()) and host == port_to_host[port]:
        new_port = max(port_to_host.keys()) + 1
        self.logger.info("Le port {0} est déjà utilisé. Il passe à {1}".format(port, new_port))
        port = new_port
        pass

      if port_polling in list(port_polling_to_host.keys()) and host == port_polling_to_host[port_polling]:
        new_port = max(port_polling_to_host.keys()) + 1
        self.logger.info("Le port polling {0} est déjà utilisé. Il passe à {1}".format(port_polling, new_port))
        port_polling = new_port
        pass

      self.sde_started[name] = serveurs_donnees.SDE(self.logger, name, profils, host,
                                    path, port, port_polling, start, invites, sde_auth)

      self.logger.info("SDE démaré ajouté: {0}.".format(self.sde_started[name]))
      return self.sde_started[name]

    else:
      if not port:
        self.logger.error("Le port du SDE {0} est obligatoire".format(name))
        return None

      if not host:
        self.logger.error("L'hôte du SDE {0} est obligatoire".format(name))
        return None

      port_to_host = {}
      port_polling_to_host = {}
      for mySde in list(self.sde_not_started.values()):
        port_to_host[mySde.port] = mySde.host
        port_polling_to_host[mySde.port_polling] = mySde.host
        pass

      if port in list(port_to_host.keys()) and host == port_to_host[port]:
        self.logger.error("Le SDE {0} avec le port {1} et l'hôte {2} existe déjà.".format(name, port, host))
        return None

      if port_polling in list(port_polling_to_host.keys()) and host == port_polling_to_host[port_polling]:
        self.logger.error("Le SDE {0} avec le port polling {1} et l'hôte {2} existe déjà.".format(name, port, host))
        return None

      self.sde_not_started[name] = serveurs_donnees.SDE(self.logger, name, profils, host, path, port, port_polling, start, invites)
      self.logger.info("SDE non démarré ajouté: {0}.".format(self.sde_not_started[name]))
      return self.sde_not_started[name]
    pass

  def set_sdp(self, port=90):
    """
    Méthode pour ajouter un sdp au projet
    """
    if not port:
      self.logger.error("The port of SDP is mandatory")
      return None

    self.sdp = port
    pass

  def config_inst_parm_files(self, nacre_install_dir):
    """
    Cette méthode écrit les fichiers de config pour Noah:
    - inst_parm_glob_noah
    - inst_parm_loc_noah
    """
    if self.get_global_option("nacre_ssl_mode") == 'NONE':
      host = serveurs_donnees.get_local_ip_address()
    else:
      host = serveurs_donnees.getfqdn()
    base_port = self.current_command_options.get_global_option("base_port")
    nb_calcs = self.current_command_options.get_global_option("nb_calcs")

    inst_parm_glob_noah_file = os.path.join(nacre_install_dir, "install", "inst_parm_glob_noah")
    inst_parm_loc_noah_file = os.path.join(nacre_install_dir, "install", "inst_parm_loc_noah")

    with open(inst_parm_glob_noah_file, 'w') as params_file:
      params_file.write("#Plage de numeros de ports utilises (ex: de 14000 a 14099 pour la valeur BasePort=140)\n")
      params_file.write("BasePort={0}\n".format(old_div(base_port, 100)))
      params_file.write("HoteSde={0}\n".format(host))
      params_file.write("HoteSdr={0}\n".format(host))
      params_file.write("HoteSdp={0}\n".format(host))
      params_file.write("SrvCalc_CalcsNb[1]={0}\n".format(nb_calcs))
      params_file.write("SrvCalcNb=1\n")
      pass

    with open(inst_parm_loc_noah_file, 'w') as params_file:
      params_file.write("LocalInstPath={0}\n".format(os.path.join(self.current_command_options.get_global_option("main_software_directory"), "install")))
      params_file.write("RepHomeNacre=$LocalInstPath/home\n")
      params_file.write("RepWorkNacre=$LocalInstPath/work\n")
      params_file.write("RepVarNacre=$LocalInstPath/var\n")
      pass
    pass

  def config_log_file(self, config_file_log_static, config_file_log_calc, config_file_log, property_base_file_sde):
    """
    Cette méthode configure le fichier de logs selon les calculateurs
    - fichier de log [rep_nacre]/bin/log.conf
    """
    if os.path.exists(config_file_log):
      os.remove(config_file_log)
    # Le fichier n'existe pas pour des versions < V2_3_0
    if os.path.exists(property_base_file_sde):
      shutil.copyfile(property_base_file_sde, config_file_log)
    # Le fichier n'existe pas pour des versions < V2_2_0
    if not os.path.exists(config_file_log_static):
      return
    config_txt = open(config_file_log_static).read()
    if not os.path.exists(config_file_log_calc):
      return
    log_calc = open(config_file_log_calc).read()
    with open(config_file_log, 'a') as config_file:
      config_file.write(config_txt)

    nb_calcs = self.current_command_options.get_global_option("nb_calcs")
    with open(config_file_log, 'a') as log_file:
      log_file.write("# loggers calculateurs (genere pendant l'installation)\n")
      for calc in range(nb_calcs):
        log_file.write(log_calc.replace("{ICALC}", "{0}".format(calc + 1)))
        pass
      pass
    pass

  def config_calcs(self, config_file_calc):
    """
    Cette méthode configure Nacre avec les calculateurs
    - fichier de config des calculateurs [rep_nacre]/etc/rock/proto-v1.nacre.cfg
    - fichier de config des paramètres [rep_nacre]/install/inst_parm_glob_[install_ref]
    """
    if self.get_global_option("nacre_ssl_mode") == 'NONE':
      host = serveurs_donnees.get_local_ip_address()
    else:
      host = serveurs_donnees.getfqdn()
    nb_calcs = self.current_command_options.get_global_option("nb_calcs")
    base_port = self.current_command_options.get_global_option("base_port")
    var_dir = os.path.join(self.current_command_options.get_global_option("main_software_directory"), "install", "var")
    fs_port = base_port + 1
    spool_mgr_dir = os.path.join(var_dir, "rock", "spool_qmgr")
    spool_calc_dir = os.path.join(var_dir, "rock", "spool_calcs")
    globals_dir = os.path.join(var_dir, "rock", "globals")
    tmp_qmgr_dir = os.path.join(var_dir, "rock", "tmp_qmgr")
    tmp_dir = os.path.join(var_dir, "tmp")
    if not os.path.exists(var_dir):
      os.makedirs(var_dir)
    if not os.path.exists(spool_mgr_dir):
      os.makedirs(spool_mgr_dir)
    if not os.path.exists(spool_calc_dir):
      os.makedirs(spool_calc_dir)
    if not os.path.exists(globals_dir):
      os.makedirs(globals_dir)
    if not os.path.exists(tmp_qmgr_dir):
      os.makedirs(tmp_qmgr_dir)
    if not os.path.exists(tmp_dir):
      os.makedirs(tmp_dir)
    os.chmod(tmp_qmgr_dir, 0o777)
    os.chmod(tmp_dir, 0o777)

    with open(config_file_calc, 'w') as calc_file:
      calc_file.write("<STUDY-MANAGER>\n")
      calc_file.write("\t<MANAGER machine=\"{0}\" port=\"{1}\" fs-port=\"{2}\" spool=\"{3}\" globals=\"{4}\" keep-results=\"120\"/>\n".format(host, base_port, fs_port, spool_mgr_dir, globals_dir))
      calc_port = fs_port
      for calc in range(nb_calcs):
        calc_port += 1
        calc_file.write("\t<CALCULATOR name=\"calc{0}\" machine=\"{1}\" port=\"{2}\" fs-port=\"{3}\" spool=\"{4}\">\n".format(calc + 1, host, calc_port, fs_port, spool_calc_dir))
        calc_file.write("\t\t<CODE name=\"ncg\" exe=\"ncg.so\"/>\n")
        calc_file.write("\t</CALCULATOR>\n")
        pass
      calc_file.write("</STUDY-MANAGER>\n")
      pass

    pass

  def config_sde(self, config_file_sde, ihm_file_sde, config_sh_file):
    """
    Cette méthode renseigne les fichiers de config SDE
    """

    open(config_file_sde, 'w').close()
    open(ihm_file_sde, 'w').close()

    if self.sde_started:
      self.logger.info("Configuration des SDE démarrés:")
      sde_auth = None
      for mySde in list(self.sde_started.values()):
        mySde.configure(config_file_sde=config_file_sde,
                      ihm_file_sde=ihm_file_sde,
                      config_file_nacre=config_sh_file)
        if mySde.sde_auth:
          sde_auth = mySde

    if misc.compareVersions(self.current_command_version_object.get_nacre_version(), "V2-3-0") >= 0:
      if sde_auth is None:
        self.logger.error("Pas de sde d'authentification identifié")
        return None

    if list(self.sde_not_started.values()):
      self.logger.info("Configuration des SDE non démarrés:")
      for mySde in list(self.sde_not_started.values()):
        mySde.configure(ihm_file_sde=ihm_file_sde)

  def config_sdr(self, config_file_sdr, admin_file_sdr, nacre_install_dir, versionIcarex, sdr_software_list=None):
    """
    Cette méthode renseigne les fichiers de config SDR
    """
    if sdr_software_list is None:
      sdr_software_list = []
    ref_allow_file = os.path.join(nacre_install_dir, "etc", "qtnacre", "ref_allow.txt")
    open(ref_allow_file, 'w').close()

    liste_sdr_demarres = sorted(list(self.sdr_started.values()), key=attrgetter('order'))
    liste_sdr_non_demarres = sorted(list(self.sdr_not_started.values()), key=attrgetter('order'))

    # Le fichier ref_allow.txt ne contient pas la même chose
    # avant et après NACRE 2.2.0
    with open(ref_allow_file, 'a') as sdr_file:
      # NACRE >= 2.2.0
      if misc.compareVersions(self.current_command_version_object.get_nacre_version(), "V2_2_0") >= 0:
        with open(ref_allow_file + ".conf") as sdr_file_conf:
          gestions = set()
          versions_sdr = set()
          len_gestion_max = 0
          for line in sdr_file_conf.readlines():
            if not line.strip():
              continue
            gestion = line.split()[0]
            sdr = line.split()[1]
            len_gestion_max = max(len_gestion_max, gestion)
            gestions.add(gestion)
            versions_sdr.add(sdr)
          for mySdr in liste_sdr_demarres:
            if mySdr.version in versions_sdr:
              for gestion in gestions:
                sdr_file.write("{0} {1}\n".format(gestion, mySdr.name))
              # break
            pass

          pass
        pass
      # NACRE < 2.2.0
      else:
        for mySdr in list(self.sdr_started.values()):
          for profil in ["developpement", "ingenierie", "espace"]:
            sdr_file.write("{0} {1}\n".format(profil.ljust(15), mySdr.name))
            pass
          pass
        pass
      pass

    open(config_file_sdr, 'w').close()
    open(admin_file_sdr, 'w').close()
    for profil in ["exploitation", "developpement", "ingenierie", "espace"]:
      init_profil_file = os.path.join(nacre_install_dir, "etc", "qtnacre", "init_" + profil + ".txt")
      open(init_profil_file, 'w').close()

    sdr_software_types_to_soft = {}
    for sdr_software in sdr_software_list:
      sdr_software_types_to_soft[sdr_software.sdr_type()] = sdr_software

    if liste_sdr_demarres:
      self.logger.info("Configuration des SDR démarrés:")
    for mySdr in liste_sdr_demarres:
      self.logger.info("{0}".format(mySdr))
      if mySdr.origin:
        if mySdr.origin in list(sdr_software_types_to_soft.keys()):
          origin_directory = sdr_software_types_to_soft[mySdr.origin].install_directory
          mySdr_directory = os.path.join(os.path.dirname(origin_directory), "sdr_" + mySdr.name)
          if os.path.exists(mySdr_directory):
            misc.fast_delete(mySdr_directory)
          shutil.copytree(origin_directory, mySdr_directory)
          pass
        else:
          self.logger.warning("SDR {0} origin does not exist: {1}".format(mySdr.name, mySdr.origin))
          continue
        pass
      else:
        sdr_software = sdr_software_types_to_soft.get(mySdr.name, None)
        if sdr_software:
          mySdr_directory = sdr_software.install_directory
          pass
        else:
          self.logger.warning("SDR {0} does not exist".format(mySdr.name))
          continue


      with open(admin_file_sdr, 'a') as sdr_file:
        self.logger.debug("Enregistrement dans le fichier {0}".format(admin_file_sdr))
        sdr_file.write("{0} {1} {2}\n".format(mySdr.name.ljust(10), mySdr.host, mySdr.port))
        pass

      with open(config_file_sdr, 'a') as sdr_file:
        self.logger.debug("Enregistrement dans le fichier {0}".format(config_file_sdr))
        sdr_file.write("SDR   {0} {1}\n".format(mySdr.port, mySdr_directory))
        pass

      if mySdr.profils:
        for profil in mySdr.profils:
          init_profil_file = os.path.join(nacre_install_dir, "etc", "qtnacre", "init_" + profil + ".txt")
          with open(init_profil_file, 'a') as sdr_file:
            sdr_file.write("{0} Historique historique icarex_ini.{1} icarex.{1}\n".format(mySdr.name.ljust(10), versionIcarex))
            sdr_file.write("{0} MiniDIC minidic icarex_ini.{1} icarex.{1}\n".format(mySdr.name.ljust(10), versionIcarex))
            sdr_file.write("{0} MiniDIC_avec_repare_carte repareminidic icarex_ini.{1} icarex.{1}\n".format(mySdr.name.ljust(10), versionIcarex))
            pass
          pass
        pass
      pass
    #
    if liste_sdr_non_demarres:
      self.logger.info("SDR non démarrés:")
    for mySdr in liste_sdr_non_demarres:
      # TODO
      pass
    pass

  def config_sdp(self, config_file_sdp, pub_allow_file_sdp, nacre_install_dir):
    """
    Cette méthode renseigne les fichiers de config SDP
    """
    open(config_file_sdp, 'w').close()
    open(pub_allow_file_sdp, 'w').close()
    if self.get_global_option("nacre_ssl_mode") == 'NONE':
      host = serveurs_donnees.get_local_ip_address()
    else:
      host = serveurs_donnees.getfqdn()
    base_port = self.current_command_options.get_global_option("base_port")
    work_dir = os.path.join(self.current_command_options.get_global_option("main_software_directory"), "install", "work")

    with open(pub_allow_file_sdp, 'w') as sdp_file:
      for profil in ["exploitation", "developpement", "ingenierie", "espace"]:
        sdp_file.write("{0} PUB:{1}:{2}\n".format(profil.ljust(15), host, base_port + self.sdp))
        pass
      pass
    with open(config_file_sdp, 'w') as sdp_file:
      sdp_file.write("SDP      {0} {1}".format(base_port + self.sdp, os.path.join(work_dir, "sdp")))
    pass

  def set_default_data_servers(self, work_dir, base_port):
    # Serveurs de données par défaut
    defaut_port_p04 = base_port + 71
    defaut_port_p14 = base_port + 70

    if not self.sde_started:
      self.add_sde("sde",
        profils=["exploitation", "developpement", "ingenierie", "espace"],
        path=os.path.join(work_dir, "sde"),
        port=base_port + 80, port_polling=base_port + 60, start=True,
        invites=[], sde_auth=True)

    # Les SDR additionnels ne doivent pas être les premiers dans l'ordre des SDR
    # et les ports par défaut (24770 et 24771) sont réservées aux SDR P04 et P14.
    order_increment = 0
    port_increment = 0
    min_default_port = min(defaut_port_p04, defaut_port_p14)
    max_default_port = max(defaut_port_p04, defaut_port_p14)
    for sdr in list(self.sdr_started.values()) + list(self.sdr_not_started.values()):
      if sdr.order == 0:
        order_increment = max(order_increment, 2)
      elif sdr.order == 1:
        order_increment = max(order_increment, 1)
      if sdr.port == min_default_port:
        port_increment = max(port_increment, 2)
      elif sdr.port == max_default_port:
        port_increment = max(port_increment, 1)

    if order_increment:
      self.logger.info("Les SDR additionnels ne peuvent pas être en position 0 et 1. Leurs positions sont augmentés de {0}.".format(order_increment))
    if port_increment:
      self.logger.info("Les SDR additionnels ne peuvent pas utiliser les ports par défaut ({0} and {1}). Leurs ports sont augmentés de {2}.".format(min_default_port, max_default_port, port_increment))

    sdr_names = list(self.sdr_started.keys()) + list(self.sdr_not_started.keys())
    for sdr in list(self.sdr_started.values()) + list(self.sdr_not_started.values()):
      # Gestion des noms des SDR. Les noms P04 et P14 sont réservés.
      old_sdr_name = sdr.name
      if old_sdr_name in ("P04", "P14"):
        suffixe = 1
        sdr.name += "_{0}".format(suffixe)
        while sdr.name in sdr_names:
          suffixe += 1
          sdr.name += "_{0}".format(suffixe)
        sdr_names.pop(sdr_names.index(old_sdr_name))
        sdr_names.append(sdr.name)
        self.logger.info("Le SDR {0} est renommé en {1} car {0} est un nom réservé".format(old_sdr_name, sdr.name))
        if old_sdr_name in list(self.sdr_started.keys()):
          self.sdr_started[sdr.name] = sdr
          del self.sdr_started[old_sdr_name]
        else:
          self.sdr_not_started[sdr.name] = sdr
          del self.sdr_not_started[old_sdr_name]
      if order_increment:
        self.logger.info("La position du SDR {0} passe de {1} à {2}".format(sdr.name, sdr.order, sdr.order + order_increment))
        sdr.order += order_increment
      if port_increment:
        self.logger.info("Le port du SDR {0} passe de {1} à {2}".format(sdr.name, sdr.port, sdr.port + port_increment))
        sdr.port += port_increment

    self.add_sdr("P04",
      order=1,
      version="P04",
      profils=["exploitation", "developpement", "ingenierie", "espace"],
      port=base_port + 71,
      start=True)

    self.add_sdr("P14",
      order=0,
      version="P14",
      profils=["exploitation", "developpement", "ingenierie", "espace"],
      port=base_port + 70,
      start=True)

    if self.sdr_not_started:
      self.logger.info("SDR non démarrés:")
      for sdr in sorted(list(self.sdr_not_started.values()), key=attrgetter('order')):
        self.logger.info("{0}".format(sdr))

  def _prepare_config_local_install(self):
    """
      This command will prepare the version for the configuration of the softwares.
      Typically all the config files that are regenerated from a template or from scratch
      are deleted.
    """
    # Configuration des softwares (appel à la méthode prepare_config_local_install des softwares)
    post_install_commands = {}
    software_mode = "post_install_config"
    for software in self.current_command_softwares:
      # software.software_mode = "nothing"
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               software_mode,
                               self.current_command_version_object,
                               self.current_command_python_version)
      post_install_commands[software.name] = software.post_install_commands
      software.post_install_commands = []
      software.prepare_config_local_install()
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
    success = False
    try:
      success = self.execute_command()
    except LoggerException:
      pass

    for software in self.current_command_softwares:
      software.software_mode = "nothing"
      software.post_install_commands = post_install_commands[software.name]

    # This command allows to use several commands one after the others in a single yamm command
    self.reset_command(endlog=False)
    return success

  def _config_local_install(self):
    """
      This command configures the file depending on:
      - the SDE declared in the option sde_list.
      - the SDR
      - the install directories of the softwares (nacre, icarex, ...)

    """

    # Prepare execution with software_mode = "nothing"
    sdr_software_list = []
    for software in self.current_command_softwares:
      # software.software_mode = "nothing"
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               "nothing",
                               self.current_command_version_object,
                               self.current_command_python_version)
      self.current_map_name_to_softwares[software.name] = software

      if software.is_sdr():
        sdr_software_list.append(software)

    # Get files to modify or create
    # - [NACRE_INSTALL_DIR]/etc/start_scripts config_sde => to create
    #
    nacre_soft = self.current_map_name_to_softwares.get("NACRE_INFO", None)
    if not nacre_soft:
      return

    codes_soft = self.current_map_name_to_softwares.get("CODES", None)
    if not codes_soft:
      return False

    icarex_soft = self.current_map_name_to_softwares.get("ICAREX", None)
    if not icarex_soft:
      return False
    versionIcarex = icarex_soft.ordered_version
    if versionIcarex.startswith("git_tag/"):
      versionIcarex = versionIcarex[8:]

    oracle_soft = self.current_map_name_to_softwares.get("ORACLE_CLIENT", None)
    if not oracle_soft:
      return False

    # self.logger.warning("{0}".format(dir(nacre_soft)))
    config_file_sde = os.path.join(nacre_soft.install_directory, "etc", "start_scripts", "config_sde")
    config_sh_file = os.path.join(nacre_soft.install_directory, "etc", "start_scripts", "config.sh")
    if os.path.exists(config_sh_file):
      os.remove(config_sh_file)
    ihm_file_sde = os.path.join(nacre_soft.install_directory, "etc", "qtnacre", "admin.cfg")
    config_file_sdr = os.path.join(nacre_soft.install_directory, "etc", "start_scripts", "config_sdr")
    admin_file_sdr = os.path.join(nacre_soft.install_directory, "etc", "sdr", "admin_sdr.cfg")
    config_file_calc = os.path.join(nacre_soft.install_directory, "etc", "rock", "proto-v1.nacre.cfg")
    config_file_log_static = os.path.join(nacre_soft.install_directory, "etc", "rock", "log.conf.static")
    config_file_log_calc = os.path.join(nacre_soft.install_directory, "etc", "rock", "log.conf.calc")
    property_base_file_sde = os.path.join(nacre_soft.install_directory, "etc", "rock", "sde.properties.base")
    if os.path.exists(property_base_file_sde):
      config_file_log = os.path.join(nacre_soft.install_directory, "bin", "sde.properties")
    else:
      config_file_log = os.path.join(nacre_soft.install_directory, "bin", "log.conf")
    config_file_sdp = os.path.join(nacre_soft.install_directory, "etc", "start_scripts", "config_sdp")
    pub_allow_file_sdp = os.path.join(nacre_soft.install_directory, "etc", "qtnacre", "pub_allow.txt")

    # Ajout des serveurs de données par défaut
    # Si aucun SDE n'est défini, on en ajoute un dans /work/sde
    # Ajout de 2 SDR par défaut en P04 et P14
    base_port = self.current_command_options.get_global_option("base_port")
    self.set_default_data_servers(nacre_soft.rep_work, base_port)

    self.config_inst_parm_files(nacre_soft.install_directory)

    # Configuration des calculateurs
    self.config_calcs(config_file_calc)

    # Configuration du fichier de log
    self.config_log_file(config_file_log_static, config_file_log_calc, config_file_log, property_base_file_sde)

    # Configuration des fichiers liés au SDE
    self.config_sde(config_file_sde, ihm_file_sde, config_sh_file)

    # Configuration des fichiers liés au SDR
    self.config_sdr(config_file_sdr, admin_file_sdr, nacre_soft.install_directory, versionIcarex, sdr_software_list)

    # Configuration des fichiers liés au SDP
    self.config_sdp(config_file_sdp, pub_allow_file_sdp, nacre_soft.install_directory)


    # Configuration des softwares (appel à la méthode config_local_install des softwares)
    post_install_commands = {}
    for software in self.current_command_softwares:
      software.software_mode = "post_install_config"
      post_install_commands[software.name] = software.post_install_commands
      software.post_install_commands = []
      software.config_local_install()
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
    success = False
    try:
      success = self.execute_command()
    except LoggerException:
      pass

    for software in self.current_command_softwares:
      software.software_mode = "nothing"
      software.post_install_commands = post_install_commands[software.name]

    # This command allows to use several commands one after the others in a sungle yamm command
    self.reset_command(endlog=False)
    return success

  @yamm_command("Preparation of a local installation configuration")
  def prepare_config_local_install(self):
    return self._prepare_config_local_install()

  @yamm_command("Configuration of a local installation")
  def config_local_install(self):
    ret = self._prepare_config_local_install()
    if ret:
      ret = self._config_local_install()
    if ret:
      # Final step: collect sha1s of softwares
      ret = self._make(executor_mode="collect_sha1")
    return ret

#     return self._config_local_install()

  @yamm_command("Creation of an installation pack")
  def create_pack(self, unie=True, base_archive_name="nacre-install",
                  delete=False, runnable=False):
    """
    This command creates the pack to be installed in pcy00307/9 or at UNIE
    NACRE and ICAREX need additional step from the build directory.
    The others archives are built from the source directories and from the configuration directory "work"

    :param bool unie: If True, generates the archive file for EDF UNIE
     (it does not include: the BIBLIO component, the bdt notic files)
    :param string base_archive_name: Defines the base name of the archives of the components in the pack
    :param bool delete: If True, deletes the pack directory once the archives are created
    :param bool runnable: If True, generates the installer file.
    """

    if not base_archive_name:
      self.logger.error("Le nom de base des archives est vide")

    locale.setlocale(locale.LC_ALL, '')
    today = datetime.date.today().strftime('%d%b%Y').replace('.', '')  # Date au format 23oct2013
    pack_base_name = "pack_nacre_{0}".format(self.version)
    if self.version_flavour:
      pack_base_name += "_{0}".format(self.version_flavour)
    if unie:
      pack_base_name += '_unie'
    pack_name = "{0}_{1}".format(pack_base_name, today)
    pack_dir = os.path.join(self.current_command_options\
                              .get_global_option("packs_directory"),
                            pack_base_name)

    binary_archive_list = []
    for software in self.current_command_softwares:
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               'nothing',
                               self.current_command_version_object,
                               self.current_command_python_version)
      soft_type = software.get_type()
      if not((soft_type == 'composante' and software.get_pack_name())
              or isinstance(software, BrtvSoftware)):
        continue
      if unie:
        if software.name in ("BIBLIO", "ORA", "HGAP"):
          continue
        if software.name == "NOTIC_PYTHON":
          # L'archive binaire est supprimée en mode unie pour être certain
          # d'avoir une archive sans les bdt.
          binary_archive_file = os.path.join(pack_dir, software.binary_archive_name)
          if (os.path.exists(binary_archive_file)):
            self.logger.debug('Deleting file %s' % binary_archive_file)
            os.remove(binary_archive_file)
          software.make_movable_archive_commands.append('rm -rf data/bdt')
      binary_archive_list.append(software.name)

    self.logger.info("Création des packs des composantes")
    binary_archive_topdir = self.current_command_options.get_global_option("binary_archive_topdir")
    self.current_command_options.set_global_option("binary_archive_topdir", pack_dir)
    ret = self._make(software_list=binary_archive_list, executor_mode="create_binary_archive")
    if not ret:
      self.logger.error("Erreur à la création des archives binaires")
      return False
    self.current_command_options.set_global_option("binary_archive_topdir", binary_archive_topdir)

    nacre_installer = NacreInstaller(name=pack_name,
                                      logger=self.logger,
                                      topdirectory=pack_dir,
                                      softwares=self.current_command_softwares,
                                      executor=self.current_command_executor,
                                      delete=delete,
                                      runnable=runnable,
                                      base_archive_name=base_archive_name)

    return nacre_installer.execute()

#######
#
# Commands
#
#######

if __name__ == "__main__":

  print("Testing nacre project class")
  pro = Project("NACRE Project", VerboseLevels.WARNING)
  pro.set_version("V2_2_0")
  pro.print_configuration()
  pro.nothing()


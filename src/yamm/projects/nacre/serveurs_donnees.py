#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import str
from builtins import object
import os
import socket
import copy

sdr_versions = ("P94", "P04", "P14")

def get_local_ip_address():
  """
  Returns the ip as seen by the local network
  """
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
  try:
    s.connect(('<broadcast>', 0))
    ip = s.getsockname()[0]
    s.close()
  except socket.error:
    ip = "127.0.1.1"
  return ip

def getfqdn(host='localhost'):
  if host == 'localhost':
    return socket.getfqdn()
  else:
    return socket.getfqdn(host)

def islocal(host):
  return socket.gethostbyname(host) == get_local_ip_address()
#   return socket.gethostbyname(host).split('.')[0] == '127'

class ServeurDonnees(object):
  """
  Classe de base pour les serveurs de données d'étude et de référence
  """
  def __init__(self, logger, name, profils, host, port, start):
    self.logger = logger
    self.name = name
    self.profils = [p.strip() for p in profils] or ["exploitation", "developpement", "ingenierie", "espace"]
#     self.host = getfqdn(host)
    self.host = get_local_ip_address()
    if host and host != 'localhost':
      self.host = host
#    self.host = host or get_local_ip_address()
    self.port = port
    self.start = start

  def __repr__(self):
    """
    This method will be used to serialize the data servers: only basic types must be serialized.
    This is why the logger is removed (it is a misc.Log instance).
    """
    __ds_dict__ = copy.deepcopy(self.__dict__)
    if 'logger' in __ds_dict__:
      del __ds_dict__['logger']
    return str(__ds_dict__)

class SDE(ServeurDonnees):
  def __init__(self, logger, name, profils, host, path, port, port_polling, start, invites, sde_auth=False, *args):
    ServeurDonnees.__init__(self, logger, name, profils, host, port, start)
    self.invites = invites or []
    self.path = path
    self.port_polling = port_polling
    self.sde_auth = sde_auth

  def configure(self, config_file_sde="", ihm_file_sde="", config_file_nacre=""):
    # Check that started SDE is on local machine
    if self.start:
      self.logger.info("Configuring local started SDE: " + self.name)
      if not os.path.exists(self.path):
        self.logger.info("The path {0} for SDE {1} is created.".format(self.path, self.name))
        os.makedirs(self.path)
      sde_admin_file = os.path.join(self.path, ".adminsde")
      if not os.path.exists(sde_admin_file):
        with file(sde_admin_file, 'a'):
          os.utime(sde_admin_file, None)
      sde_invites_file = os.path.join(self.path, ".invites")
      if os.path.exists(sde_invites_file):
        os.remove(sde_invites_file)
      if self.invites:
        self.logger.info("Writing the Guest file: {0}".format(sde_invites_file))
        with file(sde_invites_file, 'w') as f:
          f.write("\n".join([invite for invite in self.invites if invite]))
          f.write("\n")
    if self.start and config_file_sde:
      with open(config_file_sde, 'a') as sde_file:
        self.logger.debug("Writing config file {0} for SDE: \"{1}\"".format(config_file_sde, self.name))
        sde_file.write("SDE      {0} {1}\n".format(self.port, self.path))
        sde_file.write("POLLING  {0} {1}\n".format(self.port_polling, self.path))
      if self.sde_auth:
        input_cfg_file = config_file_nacre + ".conf"
        self.logger.info("Setting authentification SDE infos in file {0} for SDE: \"{1}\"".format(config_file_nacre, self.name))
        cfg_conf_file = open(input_cfg_file).readlines()
        with open(config_file_nacre, 'w') as cfg_file:
          for line in cfg_conf_file:
            if line.startswith("NACRE_MAINSDE_HOST"):
              key = line.split("=")[0]
              line = "=".join([key, self.host]) + "\n"
            if line.startswith("NACRE_MAINSDE_PORT"):
              key = line.split("=")[0]
              line = "=".join([key, str(self.port)]) + "\n"
            cfg_file.write(line)
    if ihm_file_sde:
      with open(ihm_file_sde, 'a') as sde_file:
        self.logger.debug("Writing IHM file {0} for SDE: \"{1}\"".format(ihm_file_sde, self.name))
        sde_file.write("{0} {1} {2} {3} : {4}\n".format(self.name.ljust(10), self.host, self.port, self.port_polling, " ".join(self.profils)))
        pass

class SDR(ServeurDonnees):
  def __init__(self, logger, name, order, version, profils, host, port, origin, start, *args):
    ServeurDonnees.__init__(self, logger, name, profils, host, port, start)
    self.order = order
    if version not in sdr_versions:
      self.logger.error("The version of SDR {0} must be in {1}".format(name, sdr_versions))
    self.version = version
    self.origin = origin


#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import string
import tempfile

from yamm.core.base import misc
from yamm.core.framework.software import FrameworkSoftware

class nacre_software(FrameworkSoftware):
  def __init__(self, name="NOT CONFIGURED", version="NOT CONFIGURED",
               verbose=misc.VerboseLevels.INFO, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)
    if self.get_type() in ('composante', 'tool'):
      self.root_repository_template = string.Template('http://$server')

  def init_variables(self):
    self.rep_home = os.path.join(self.project_options.get_option(self.name,
                                      "main_software_directory"),
                                 "install", "home")
    self.rep_var = os.path.join(self.project_options.get_option(self.name,
                                      "main_software_directory"),
                                "install", "var")
    self.rep_work = os.path.join(self.project_options.get_option(self.name,
                                      "main_software_directory"),
                                 "install", "work")
    self.rep_brtv = os.path.join(self.rep_home, "BRTV")

  def is_sdr(self):
    return False

  def sdr_type(self):
    return ""

  def get_executor_software_name(self):
    return FrameworkSoftware.get_executor_software_name(self)

  def default_values_hook(self):
    if self.get_pack_name():
      self.binary_archive_name = "nacre-install_{0}.tar.gz".format(self.get_pack_name())
    if self.get_type() == "prerequisite":
      self.src_directory = os.path.join(self.project_options.get_global_option("top_directory"),
                                        "prerequisites", "src", self.executor_software_name)
      self.build_directory = os.path.join(self.project_options.get_global_option("top_directory"),
                                          "prerequisites", "build", self.executor_software_name)
      self.install_directory = os.path.join(self.project_options.get_global_option("top_directory"),
                                            "prerequisites", "install", self.executor_software_name)

  def prepare_config_local_install(self):
    # This method should be used to prepare the configuration of the software
    removeFileTemplate = 'if test -f {0}; then echo "Deleting {0}"; /bin/rm -rf {0}; fi'
    for config_file in self.get_config_files_list():
      self.logger.debug("Deleting file: {1}".format(self.name, config_file))
      self.post_install_commands.append(removeFileTemplate.format(config_file))
    pass

  def config_local_install(self, dependency_name="ALL"):
    # This method should be used to reconfigure the software
    pass

  def configure_git_composantes(self):
    if not self.project_options.is_software_option(self.name, "software_src_directory"):
      self.project_options.set_software_option(self.name,
        "software_src_directory",
        os.path.join(self.project_options.get_option(self.name, "top_directory"),
        "composantes", self.name.capitalize()))

  def encrypt(self, file_in, file_out):
      if not file_in:
          self.logger.error('Aucun de fichier de mot de passe donné')
      fichier_mdp_chiffrement = self.project_options.get_global_option("fichier_mdp_chiffrement")
      if not fichier_mdp_chiffrement:
          self.logger.error("Aucun de fichier de chiffrement donné. Renseigner l'option 'fichier_mdp_chiffrement'")

      self.post_install_commands.append("openssl enc -aes-128-cbc -a "
                                        "-in %s -out %s -pass file:%s"
                                        % (file_in, file_out, fichier_mdp_chiffrement))

  def get_type(self):
    """
    This method returns the type of software.
    There are 2 types in the NACRE project:

      - prerequisite
      - composante
    """
    return "type not defined for software %s" % self.name

  def create_pack(self, pack_dir, base_archive_name="nacre-install", tmpdir=""):
    """
    This method will return the command used to create the pack of the software.
    """
    composante = self.get_pack_name()
    if not composante or self.get_type() != "composante":
      return ""
    delete = False
    if not tmpdir:
      delete = True
      tmpdir = tempfile.mkdtemp()
    pack_archive_filename = self.get_archive_pack_name(pack_dir, base_archive_name)
    if not pack_archive_filename:
      return ""
    cmd = "cd {0} ; rsync -az --exclude '*.log' --exclude '.yamm' --exclude '.install_ok*' . {1}/{2} > {0}/pack.log 2>&1;".format(self.install_directory, tmpdir, composante)
    cmd += "cd {0} ; tar czf {1} {2} ; ".format(tmpdir, pack_archive_filename, composante)
    if delete:
      cmd += "rm -rf {0} ;".format(tmpdir)
    return cmd

  def get_archive_pack_name(self, pack_dir, base_archive_name="nacre-install"):
    composante = self.get_pack_name()
    if not composante or self.get_type() != "composante":
      return ""
    return os.path.join(pack_dir, "{0}_{1}.tar.gz".format(base_archive_name, composante))


  def get_pack_name(self):
    """
    This method returns the name of the archive of the software in the pack.
    """
    return None

  def configure_remote_software_pleiade_git(self,
                                            internal_repo_name,
                                            internal_submodules=None,
                                            external_server="",
                                            external_path="",
                                            external_repo_name="",
                                            external_submodules=None,
                                            use_proxy=True):
    """
    """
    internal_server = "http://noev01tn.noe.edf.fr:9070"
    internal_path = "NCR"
    self.configure_remote_software(external_server,
                                   external_path,
                                   external_repo_name,
                                   external_submodules,
                                   internal_server,
                                   internal_path,
                                   internal_repo_name,
                                   internal_submodules,
                                   use_proxy)

  def get_config_files_list(self):
    return []

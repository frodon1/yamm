#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.nacre.software import nacre_software


software_name = "BIBLIO"

class BIBLIO(nacre_software):

  def get_config_files_list(self):
    config_files_list = nacre_software.get_config_files_list(self)
    config_files_list.append("$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_codes.sh")
    return config_files_list

  def init_variables(self):
    nacre_software.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name)

    self.archive_file_name = "biblio-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-biblio.git")
    self.configure_git_composantes()

    self.compil_type = "rsync"

  def init_dependency_list(self):
    self.software_dependency_list = ["NACRE_INFO"]

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "NACRE_INFO" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminBIB>" , "$CURRENT_SOFTWARE_INSTALL_DIR",
                                          "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_codes.sh", True)

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = nacre_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "NACRE_INFO":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return software_name

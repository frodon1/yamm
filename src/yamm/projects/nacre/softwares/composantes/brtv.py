#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.core.base import misc
from yamm.projects.brtv.softwares.composantes.brtv import BRTV as BRTV_STANDALONE
from yamm.projects.nacre.software import nacre_software


software_name = "BRTV"

class BRTV(BRTV_STANDALONE, nacre_software):

  def __init__(self, name, version, verbose, **kwargs):
    nacre_software.__init__(self, name, version, verbose, **kwargs)
    # dictionnaire stockant les versions des differents composants de la BRTV
    # [OSCARD, THYC, THYCOX, ARCHI_THYC]
    self.versions_to_couplage_thyc_thycox = {}
    if misc.compareVersions(self.ordered_version, "V3.1") >= 0:
      BRTV_STANDALONE.__init__(self, name, version, verbose)
    else:
      self.versions_to_couplage_thyc_thycox["V2.4.1"] = ["V4.2.0", "V4.4.0", "V2.8", "_ifort"]
      self.versions_to_couplage_thyc_thycox["V2.4.13"] = ["V4.3", "V5.0/build_Linux_x86_64", "V2.9", "_ifort"]
      self.versions_to_couplage_thyc_thycox["V2.6"] = ["V4.4", "V5.1/build_Linux_x86_64", "V2.10", "_intel_12.1.0"]

  def get_executor_software_name(self):
    if misc.compareVersions(self.ordered_version, "V3.1") >= 0:
      return self.name.lower()
    else:
      return nacre_software.get_executor_software_name(self)

  def get_config_files_list(self):
    config_files_list = nacre_software.get_config_files_list(self)
    if misc.compareVersions(self.ordered_version, "V3.1") >= 0:
      config_files_list = BRTV_STANDALONE.get_config_files_list(self)
      config_files_list.append(os.path.join(self.rep_brtv, ".versions"))
    else:
      config_files_list.append("$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_tiers.sh")
      config_files_list.append("$INSTALL_MULTI_INSTALL_DIR/post_install_BRTV.sh")
      config_files_list.append(os.path.join(self.install_directory, ".versions"))
    return config_files_list

  def init_variables(self):
    nacre_software.init_variables(self)
    # Version >= 3.1
    if misc.compareVersions(self.ordered_version, "V3.1") >= 0:
      BRTV_STANDALONE.init_variables(self)
      self.install_directory = os.path.join(self.rep_brtv, os.path.basename(self.executor_software_name))
      self.make_movable_archive_commands = []

    # Version <= 2.6
    else:
      self.executor_software_name = self.name
      self.install_directory = os.path.join(self.rep_home, self.name)

      self.archive_file_name = "brtv-" + self.get_version_for_archive() + ".tar.gz"
      self.archive_type = "tar.gz"

      self.remote_type = "git"
      self.configure_git_composantes()
      self.repository_name = "nacre-brtv.git"
      self.root_repository = self.root_repository_template.substitute(server=self.project_options.get_option(self.name, "vcs_server"))
      self.tag = self.version

      self.compil_type = "rsync"
      version_couplage = self.versions_to_couplage_thyc_thycox[self.ordered_version][0]
      for directory in ("COUPLAGE/" + version_couplage + "/outils", "COUPLAGE/" + version_couplage + "/xml"):
        self.make_movable_archive_commands.append('for f in $(ls {0}); do if test -d {0}/$f; then echo "Suppression de {0}/$f"; rm -rf {0}/$f; fi; done;'.format(directory))

  def init_dependency_list(self):
    self.software_dependency_list = ["NACRE_INFO"]
    if misc.compareVersions(self.ordered_version, "V3.1") >= 0:
      self.software_dependency_list += ["BRTV_THYC", "BRTV_THYCOX", "BRTV_OSCARD", "BRTV_COCCINELLE", "BRTV_TRIDENS"]
    else:
      self.software_dependency_list += ["CODES"]
      if misc.compareVersions(self.ordered_version, "V2.6") == 0:
        self.software_dependency_list += ["INSTALL_MULTI"]

  def config_local_install(self, dependency_name="ALL"):
    # All versions
    if dependency_name == "ALL":
      self.post_install_commands.append("echo \"BRTV_version_globale={0}\n\" >> {1}/.versions ;"
                                        .format(self.ordered_version, self.rep_brtv))
    if dependency_name == "NACRE_INFO" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminBRTV>" , "$CURRENT_SOFTWARE_INSTALL_DIR", "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_tiers.sh", True)


    # Versions dependent
    if misc.compareVersions(self.ordered_version, "V3.1") >= 0:
      # BRTV_STANDALONE.config_local_install(self, dependency_name)
      if dependency_name == "SELF" or dependency_name == "ALL":
        self.replaceStringInFilePostInstall("<BRTV_INSTALL_DIR>", self.install_directory, "config.sh")
      if dependency_name == "BRTV_THYC" or dependency_name == "ALL":
        self.replaceStringInFilePostInstall("<THYC_INSTALL_DIR>", "$BRTV_THYC_INSTALL_DIR", "config.sh")
      if dependency_name == "BRTV_THYCOX" or dependency_name == "ALL":
        self.replaceStringInFilePostInstall("<THYCOX_INSTALL_DIR>", "$BRTV_THYCOX_INSTALL_DIR", "config.sh")
      if dependency_name == "BRTV_OSCARD" or dependency_name == "ALL":
        self.replaceStringInFilePostInstall("<OSCARD_INSTALL_DIR>", "$BRTV_OSCARD_INSTALL_DIR", "config.sh")
        #rigel 1103 : arrêt de SALOME
        self.replaceStringInFilePostInstall("<OSCARD_INSTALL_DIR>", "$BRTV_OSCARD_INSTALL_DIR", "$NACRE_INFO_INSTALL_DIR/bin/stop_all", True)
      if dependency_name == "BRTV_COCCINELLE" or dependency_name == "ALL":
        self.replaceStringInFilePostInstall("<COCCINELLE_INSTALL_DIR>", "$BRTV_COCCINELLE_INSTALL_DIR", "config.sh")
      if dependency_name == "BRTV_TRIDENS" or dependency_name == "ALL":
        self.replaceStringInFilePostInstall("<TRIDENS_INSTALL_DIR>", "$BRTV_TRIDENS_INSTALL_DIR", "config.sh")

    else:
      link_files = []
      versions = self.versions_to_couplage_thyc_thycox.get(self.ordered_version, [])
      if not versions:
        self.logger.error("Impossible de déterminer les versions de couplage, thyc et thycos à partir de la version {0}".format(self.ordered_version))

      if dependency_name == "CODES" or dependency_name == "ALL":
        link_files.append(("../BRTV/THYC/THYC/%s/coeur_Linux_x86_64%s.exe" % (versions[1], versions[3]), "$CODES_INSTALL_DIR/thyc"))
        link_files.append(("../BRTV/THYC/Coeur/THYCOX/%s/thycox_Linux_x86_64.exe" % versions[2], "$CODES_INSTALL_DIR/thycox"))
      if dependency_name == "NACRE_INFO" or dependency_name == "ALL":
        self.replaceStringInFilePostInstall("<Version_BRTV>" , self.ordered_version, "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_tiers.sh", True)
        self.replaceStringInFilePostInstall("<Version_THYC_BRTV>" , versions[1], "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_tiers.sh", True)
      if dependency_name == "INSTALL_MULTI" or dependency_name == "ALL":
        if misc.compareVersions(self.ordered_version, "V2.6") == 0:
          self.replaceStringInFilePostInstall("<version_couplage>" , versions[0], "$INSTALL_MULTI_INSTALL_DIR/post_install_BRTV.sh", True)
          self.replaceStringInFilePostInstall("<version_thyc>" , versions[1], "$INSTALL_MULTI_INSTALL_DIR/post_install_BRTV.sh", True)
          self.replaceStringInFilePostInstall("<version_thycox>" , versions[2], "$INSTALL_MULTI_INSTALL_DIR/post_install_BRTV.sh", True)
          self.replaceStringInFilePostInstall("<version_globale>" , self.ordered_version, "$INSTALL_MULTI_INSTALL_DIR/post_install_BRTV.sh", True)
          self.replaceStringInFilePostInstall("<archi_thyc>" , versions[3], "$INSTALL_MULTI_INSTALL_DIR/post_install_BRTV.sh", True)
      if dependency_name == "ALL":
        self.post_install_commands.append("echo \"BRTV_version_couplage={0}\n\" >> {1}/.versions ;".format(versions[0], self.rep_brtv))
        self.post_install_commands.append("echo \"BRTV_version_thyc={0}\n\" >> {1}/.versions ;".format(versions[1], self.rep_brtv))
        self.post_install_commands.append("echo \"BRTV_version_thycox={0}\n\" >> {1}/.versions ;".format(versions[2], self.rep_brtv))
        self.post_install_commands.append("echo \"BRTV_archi_thyc={0}\n\" >> {1}/.versions".format(versions[3], self.rep_brtv))
        if misc.compareVersions(self.ordered_version, "V2.4.13") == 0:
          self.post_install_commands.append("cd COUPLAGE/%s/outils ; if test -e Salome_*.run; then ./postinstall.sh && rm -f Salome_*.run ; fi" % versions[0])
        if misc.compareVersions(self.ordered_version, "V2.6") == 0:
          self.post_install_commands.append("./install.sh")

      for link_file in link_files:
        self.post_install_commands.append("ln -fs {0} {1} ;".format(link_file[0], link_file[1]))
        pass

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = nacre_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "INSTALL_MULTI":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "NACRE_INFO":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "CODES":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BRTV_THYC":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BRTV_THYCOX":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BRTV_OSCARD":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BRTV_COCCINELLE":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BRTV_TRIDENS":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return software_name

  def default_values_hook(self):
    nacre_software.default_values_hook(self)

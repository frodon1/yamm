#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.nacre.software import nacre_software


software_name = "BTHM"

class BTHM(nacre_software):

  def init_variables(self):
    nacre_software.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name)

    self.archive_file_name = "bthm-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-notic")
    self.configure_git_composantes()

    self.compil_type = "rsync"

    executable_files = [os.path.join(self.install_directory, "SHELL", "build_histonotic.nacre.sh"),
                        os.path.join(self.install_directory, "SHELL", "histocamp.sh"),
                        os.path.join(self.install_directory, "SHELL", "notic.sh"),
                        os.path.join(self.install_directory, "SHELL", "cyr3_rafale.sh"),
                        ]

    for executable_file in executable_files:
      self.post_install_commands.append("chmod +x {0};".format(executable_file))
      pass

  def config_local_install(self, dependency_name="ALL"):

    if dependency_name == "SELF" or dependency_name == "ALL":
      link_files = []
      link_files.append(["./histocamp_Linux_x86_64_v210.exe", os.path.join(self.install_directory, "BIN", "histocamp.exe")])
      link_files.append(["./notic_Linux_x86_64_v210.exe", os.path.join(self.install_directory, "BIN", "notic.exe")])

      for link_file in link_files:
        self.post_install_commands.append("ln -fs {0} {1} ;".format(link_file[0], link_file[1]))

  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return software_name

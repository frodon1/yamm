#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.core.base import misc
from yamm.projects.nacre.software import nacre_software


software_name = "CODES"

class CODES(nacre_software):

  def __init__(self, name, version, verbose, **kwargs):
    nacre_software.__init__(self, name, version, verbose, **kwargs)
    self.executable_cyr3 = "c3res." + self.get_cyrano3_version()
    self.executable_c3post = "c3post-" + self.get_c3post_version()

  def get_cyrano3_version(self):
    if not misc.compareVersions(self.ordered_version, "V2-1-1") >= 0:
      raise ValueError("La version de CYRANO3 n'est pas connue pour cette version de NACRE : " + self.ordered_version)
    else:
      return "V3-4a"

  def get_c3post_version(self):
    if not misc.compareVersions(self.ordered_version, "V2-1-1") >= 0:
      raise ValueError("La version de C3POST n'est pas connue pour cette version de NACRE : " + self.ordered_version)
    else:
      return "4.4"

  def init_variables(self, config_mode="SELF"):
    nacre_software.init_variables(self)

    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name)

    self.archive_file_name = "codes-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-codes.git")
    self.configure_git_composantes()

    self.compil_type = "rsync"

    self.config_local_install(config_mode)

  def init_dependency_list(self):
    self.software_dependency_list = ["INSTALL_MULTI", "NACRE_INFO"]
    if misc.compareVersions(self.ordered_version, "V2-3-0") < 0:
      self.software_dependency_list.append("BTHM")
    else:
      if misc.compareVersions(self.ordered_version, "V2-4-0") < 0:
        self.software_dependency_list.append("NOTIC")

  def get_config_files_list(self):
    config_files_list = nacre_software.get_config_files_list(self)
    config_files_list.append("$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_codes.sh")
    config_files_list.append("$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_tiers.sh")
    if misc.compareVersions(self.ordered_version, "V2-3-0") >= 0 and \
       misc.compareVersions(self.ordered_version, "V2-4-0") < 0:
      config_files_list.append("$NOTIC_INSTALL_DIR/shell/Variables_notic_appli.sh")
    return config_files_list

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "NACRE_INFO" or dependency_name == "ALL":
      nb_thread_cox = self.project_options.get_global_option("nb_thread_cox")
      self.replaceStringInFilePostInstall(
                  "<CheminCODES>" ,
                  "$CURRENT_SOFTWARE_INSTALL_DIR",
                  "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_codes.sh",
                  True)
      self.replaceStringInFilePostInstall(
                  "OMP_NUM_THREADS=1",
                  "OMP_NUM_THREADS=%d" % nb_thread_cox,
                  "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_codes.sh",
                  True)
      self.replaceStringInFilePostInstall(
                  "<CheminCODES>",
                  "$CURRENT_SOFTWARE_INSTALL_DIR",
                  "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_tiers.sh",
                  True)

    # Starting from version V2-3-0, files in CODES_NACRE are merged in to CODES
    if misc.compareVersions(self.ordered_version, "V2-3-0") >= 0:
      if dependency_name == "SELF" or dependency_name == "ALL":

        version_for_calap = self.ordered_version
        if version_for_calap.startswith("git_tag/"):
          version_for_calap = version_for_calap[8:].replace('-', '.')

        executable_files = ["cobraIIIc_V3.0.1.sh",
                            "efluve_V3.4.3e.sh",
                            "calap.sh"
                            ]

        for executable_file in executable_files:
          self.replaceStringInFilePostInstall("<CheminCodes>" ,
                                              self.install_directory,
                                              executable_file)
          self.post_install_commands.append("chmod +x {0};"\
                                            .format(executable_file))

        link_files = []
        # A partir de NACRE 2.1.1, on garde calap.sh comme lien vers calap_V2.1.0.sh
        # La version de calap est modifiée en conséquence
        link_files.append(["./calap.sh", "calap_V2.1.0.sh"])
        for link_file in link_files:
          self.post_install_commands.append("ln -fs {0} {1} ;"\
                                            .format(link_file[0], link_file[1]))

      if misc.compareVersions(self.ordered_version, "V2-3-0") >= 0:
        self.add_cyrano3_post_install_commands(dependency_name)

  def add_cyrano3_post_install_commands(self, dependency_name="ALL"):
    if dependency_name == "NOTIC" or dependency_name == "ALL":
      if misc.compareVersions(self.ordered_version, "V2-3-0") >= 0 and \
          misc.compareVersions(self.ordered_version, "V2-4-0") < 0:
        notic_env = "$NOTIC_INSTALL_DIR/shell/Variables_notic_appli.sh"
        self.replaceStringInFilePostInstall("<CheminCYRANO3>",
                              "$CURRENT_SOFTWARE_INSTALL_DIR", notic_env, True)
        self.replaceStringInFilePostInstall("<VersionCYRANO3>",
                              self.executable_cyr3, notic_env, True)
        self.replaceStringInFilePostInstall("<CheminC3POST>",
                              "$CURRENT_SOFTWARE_INSTALL_DIR", notic_env, True)
        self.replaceStringInFilePostInstall("<VersionC3POST>",
                              self.executable_c3post, notic_env, True)
    if dependency_name == "INSTALL_MULTI" or dependency_name == "ALL":
      if misc.compareVersions(self.ordered_version, "V2-3-0") >= 0:
        # No need post install : links are good
        install_multi_env = "$INSTALL_MULTI_INSTALL_DIR/install_locale.sh"
        self.replaceStringInFilePostInstall("$$dir_prog/post_install_CODES.sh",
                                            "#$$dir_prog/post_install_CODES.sh",
                                            install_multi_env,
                                            True,
                                            sedSep="|")
    if dependency_name == "SELF" or dependency_name == "ALL":
      link_files = []
      link_files.append([self.executable_c3post, "c3post"])
      link_files.append([self.executable_cyr3, "cyrano3_V3-4a"])
      link_files.append([self.executable_cyr3, "cyrano3"])
      for link_file in link_files:
        self.post_install_commands.append("ln -fs {0} {1} ;".format(link_file[0], link_file[1]))

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = nacre_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "NACRE_INFO":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "NOTIC":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "INSTALL_MULTI":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return software_name

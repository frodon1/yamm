#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Ndeye Fatou DIOUF (CGI)

import os

from yamm.projects.nacre.software import nacre_software


software_name = "HGAP"


class HGAP(nacre_software):

    def init_variables(self):
        nacre_software.init_variables(self)
        self.executor_software_name = self.name
        self.install_directory = os.path.join(self.rep_home, self.executor_software_name)
	
        self.archive_file_name = "HGAP-" + self.get_version_for_archive() + ".tar.gz"
        self.archive_type = "tar.gz"

        self.remote_type = "git"
        self.tag = self.version

        internal_server = "http://noev01tn.noe.edf.fr:9070/NCR"
        internal_path = ""
        internal_repo_name = "nacre-hgap.git"
        internal_submodules=None
        external_server=""
        external_path=""
        external_repo_name="HGAP"
        external_submodules=None
        use_proxy=True

        self.configure_remote_software(external_server,
                                   external_path,
                                   external_repo_name,
                                   external_submodules,
                                   internal_server,
                                   internal_path,
                                   internal_repo_name,
                                   internal_submodules,
                                   use_proxy)


        self.configure_git_composantes()

        self.compil_type = "rsync"

    def get_type(self):
        return "composante"

    def get_pack_name(self):
        return software_name

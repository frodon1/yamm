#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.core.base import misc
from yamm.core.framework import test_suite
from yamm.projects.nacre.software import nacre_software
import datetime


software_name = "ICAREX"

class ICAREX(nacre_software):

  def init_variables(self):
    nacre_software.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name.lower())

    self.archive_file_name = "icarex-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-icarex.git")
    self.configure_git_composantes()
    self.shell = "/bin/bash"

    if misc.compareVersions(self.ordered_version, "v3.13.0") < 0:
      self.compil_type = "specific"
      self.pre_configure_commands = ["mkdir $CURRENT_SOFTWARE_INSTALL_DIR ;",
                                   "cp -R * $CURRENT_SOFTWARE_INSTALL_DIR ;"]
      self.replaceStringInFilePreInstall("<CheminIcarex>" , "$CURRENT_SOFTWARE_INSTALL_DIR", "compil.sh")
      self.replaceStringInFilePreInstall("<VersionIcarex>" , self.ordered_version, "compil.sh")
      self.replaceStringInFilePreInstall("<_RepHomeNacre_>" , self.rep_home, "compil.sh")
      self.replaceStringInFilePreInstall("<VersionIcarex>" , self.ordered_version, "varmake")
      self.pre_install_commands.append("chmod +x $CURRENT_SOFTWARE_INSTALL_DIR/compil.sh ; ")
      self.pre_install_commands.append("mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib ; ")

      self.specific_install_command = "cd $CURRENT_SOFTWARE_INSTALL_DIR ; "
      if misc.compareVersions(self.ordered_version, "v3.11.0") < 0:
        # Ajout de l'instruction "set -e" pour faire arreter le script en cas d'erreur
        self.specific_install_command += "sed -i \"16i if test -n \$ICAREX_DEBUG; then set -e; fi\" compil.sh ; "
      self.specific_install_command += "export ICAREX_DEBUG=1 ; ./compil.sh all ; "

    else:
      self.compil_type = "cmake"
      self.config_options += " -DVERSION_ICAREX={0} -DVERSION_ICAREX_INI={0} ".format(self.ordered_version)
      self.user_dependency_command += " export FC=ifort ; "

      parallel_make = self.project_options.get_option(self.name, "parallel_make")
      tests_list = ['ICAREX_INI', 'ICAREX_GENE', 'ICAREX_POST',
                    'IRADIA', 'LIBCAP', 'XENON0D', 'SIMUSPIN',
                    'CAS_TESTS']
      for testname in tests_list:
        command = 'make ARGS="-j {0} -R {1}" test'.format(parallel_make, testname)
        tests_icarex = test_suite.CommandTestSuite(name=testname,
                                                   soft_name=self.name,
                                                   soft_version=self.version,
                                                   command=command)
        tests_icarex.post_build = True
        self.test_suite_list.append(tests_icarex)

  def init_dependency_list(self):
    self.software_dependency_list = ["NACRE_INFO", 'CODES', "ICAREX_TESTS"]

  def get_config_files_list(self):
    config_files_list = nacre_software.get_config_files_list(self)
    config_files_list.append("$NACRE_INFO_ROOT_DIR/etc/start_scripts/config_codes.sh")
    return config_files_list

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "NACRE_INFO" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminIcarex>" ,
                                          "$CURRENT_SOFTWARE_INSTALL_DIR",
                                          "$NACRE_INFO_ROOT_DIR/etc/start_scripts/config_codes.sh",
                                          absolute=True)

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = nacre_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "NACRE_INFO":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "NACRE_INFO_ROOT_DIR"
    if dependency_name == "CODES":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "CODES_ROOT_DIR"
    if dependency_name == "ICAREX_TESTS":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "ICAREX_TESTS_SRC"
    return dependency_object

  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return software_name.lower()

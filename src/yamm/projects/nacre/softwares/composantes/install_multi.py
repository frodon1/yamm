#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.nacre.software import nacre_software


software_name = "INSTALL_MULTI"

class INSTALL_MULTI(nacre_software):

  def init_variables(self):
    nacre_software.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.project_options.get_option(self.name, "main_software_directory"),
                                          "install", self.executor_software_name.lower())

    self.archive_file_name = "install_multi-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-install-multi.git")
    self.configure_git_composantes()

    self.compil_type = "rsync"

  def get_type(self):
    return "composante"

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.nacre.software import nacre_software


software_name = "MODULE_HISTORIQUE"

class MODULE_HISTORIQUE(nacre_software):

  def init_variables(self):
    nacre_software.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_home, self.executor_software_name.lower())

    self.archive_file_name = "module_historique-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-module-historique.git")
    self.configure_git_composantes()

    self.compil_type = "rsync"
    self.config_option = "--exclude test"

    self.config_local_install("SELF")

  def get_type(self):
    return "composante"

  def get_config_files_list(self):
    config_files_list = nacre_software.get_config_files_list(self)
    config_files_list.append("src/templates/private.key")
    config_files_list.append("env.sh")
    return config_files_list

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "SELF" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminModuleHistorique>", "$CURRENT_SOFTWARE_INSTALL_DIR", "env.sh")
      self.replaceStringInFilePostInstall("<_ChaineConnexionBDAss_>" ,
                                          self.project_options.get_global_option("login_oracle"),
                                          "src/templates/private.key")
      self.replaceStringInFilePostInstall("<_oracle_sid_>" ,
                                          self.project_options.get_global_option("oracle_sid"),
                                          "src/templates/private.key")

    if dependency_name == "ORACLE_CLIENT" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<oracle_home>" ,
                                          "$ORACLE_CLIENT_INSTALL_DIR/instantclient",
                                          "env.sh")
      tns_admin = self.project_options.get_global_option("tns_admin")
      if not tns_admin or tns_admin == "TO_BE_DEFINED":
        tns_admin = "$ORACLE_CLIENT_INSTALL_DIR"
      self.replaceStringInFilePostInstall("<tns_admin>", tns_admin, "env.sh")

    if dependency_name == "MUSICALE" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminMusicale>", "$MUSICALE_INSTALL_DIR", "env.sh")

  def init_dependency_list(self):
    self.software_dependency_list = ["ORACLE_CLIENT", "MUSICALE"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = nacre_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "ORACLE_CLIENT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MUSICALE":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_pack_name(self):
    return software_name.lower()

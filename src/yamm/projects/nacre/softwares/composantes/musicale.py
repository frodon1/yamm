#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.core.base import misc
from yamm.projects.nacre.software import nacre_software


software_name = "MUSICALE"


class MUSICALE(nacre_software):

    def init_variables(self):
        nacre_software.init_variables(self)
        self.executor_software_name = self.name
        self.install_directory = os.path.join(self.rep_home, self.executor_software_name.lower())

        self.archive_file_name = "musicale-" + self.get_version_for_archive() + ".tar.gz"
        self.archive_type = "tar.gz"

        self.remote_type = "git"
        self.tag = self.version
        self.configure_remote_software_pleiade_git("nacre-musicale.git")
        self.configure_git_composantes()

        self.compil_type = "rsync"

        self.config_local_install("SELF")

    def get_config_files_list(self):
        config_files_list = nacre_software.get_config_files_list(self)
        config_files_list.append("default.xml")
        return config_files_list

    def config_local_install(self, dependency_name="ALL"):
        if dependency_name == "SELF" or dependency_name == "ALL":
            default_file = "default.xml"
            self.replaceStringInFilePostInstall("<adresse_musicale>",
                                                self.project_options.get_global_option("adresse_musicale"),
                                                default_file)
            self.replaceStringInFilePostInstall("<login_musicale>",
                                                self.project_options.get_global_option("login_musicale"),
                                                default_file)

    def update_configuration_with_dependency(self, dependency_name, version_name):
        self.config_local_install(dependency_name)

    def get_type(self):
        return "composante"

    def get_pack_name(self):
        return software_name.lower()

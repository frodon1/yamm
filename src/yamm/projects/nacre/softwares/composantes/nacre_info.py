#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from builtins import str
import os

from yamm.core.base import misc
from yamm.projects.nacre.software import nacre_software


software_name = "NACRE_INFO"

class NACRE_INFO(nacre_software):


  def init_variables(self):
    nacre_software.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_home, "nacre")

    self.archive_file_name = "nacre-{0}.tar.gz".format(self.get_version_for_archive())
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-info.git")
    self.configure_git_composantes()

    self.compil_type = "cmake"
    self.config_options += " -G\"CodeBlocks - Unix Makefiles\""

    self.config_local_install("SELF")

    executable_files = [os.path.join(self.install_directory, "usr", "scripts", "validation.py"),
                        os.path.join(self.install_directory, "usr", "scripts", "txt2DCU.sh"),
                        os.path.join(self.install_directory, "etc", "start_scripts", "config.sh"),
                        os.path.join(self.install_directory, "bin", "start_*"),
                        os.path.join(self.install_directory, "bin", "stop_*"),
                        os.path.join(self.install_directory, "bin", "diffuse*"),
                        os.path.join(self.install_directory, "bin", "nettoieCalcs"),
                        os.path.join(self.install_directory, "bin", "nash"),
                        os.path.join(self.install_directory, "bin", "nash_noerr"),
                        os.path.join(self.install_directory, "bin", "publish"),
                        os.path.join(self.install_directory, "bin", "migre")]

    for executable_file in executable_files:
      self.post_install_commands.append("chmod +x {0};".format(executable_file))

  def encrypt_musicale(self, file_out):
      fichier_mdp = self.project_options.get_global_option("fichier_mdp_musicale")
      self.encrypt(fichier_mdp, file_out)

  def encrypt_sinfonie(self, file_out):
      fichier_mdp = self.project_options.get_global_option("fichier_mdp_sinfonie")
      self.encrypt(fichier_mdp, file_out)

  def get_config_files_list(self):
    config_files_list = nacre_software.get_config_files_list(self)
    config_files_list.append("usr/scripts/validation.py")
    config_files_list.append("usr/scripts/txt2DCU.sh")
    config_files_list.append("etc/start_scripts/config.sh")
    config_files_list.append("etc/start_scripts/config_codes.sh")
    config_files_list.append("etc/start_scripts/config_tiers.sh")
    config_files_list.append("etc/valid/validation.xml")
    config_files_list.append("bin/start_admin")
    config_files_list.append("bin/start_qtnacre")
    config_files_list.append("bin/start_xml2std")
    config_files_list.append("bin/start_sde")
    config_files_list.append("bin/start_sdr")
    config_files_list.append("bin/start_sdp")
    config_files_list.append("bin/start_services")
    config_files_list.append("bin/nash")
    config_files_list.append("bin/nash_noerr")
    config_files_list.append("bin/publish")
    config_files_list.append("bin/start_std2dcu")
    config_files_list.append("bin/migre")
    config_files_list.append("bin/start_calcs")
    config_files_list.append("bin/start_pvm")
    config_files_list.append("bin/stop_pvm")
    config_files_list.append("bin/diffuseCalcs")
    config_files_list.append("bin/nettoieCalcs")
    config_files_list.append("bin/start_diffuse")
    config_files_list.append("bin/stop_diffuse")
    config_files_list.append("etc/qtnacre/exploitation")
    config_files_list.append("etc/qtnacre/developpement")
    config_files_list.append("etc/qtnacre/ingenierie")
    config_files_list.append("etc/qtnacre/ingenierie.met")
    config_files_list.append("etc/qtnacre/espace")
    return config_files_list

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "SELF" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "usr/scripts/validation.py")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.rep_home         , "usr/scripts/txt2DCU.sh")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.rep_home         , "etc/start_scripts/config.sh")
      self.replaceStringInFilePostInstall("<_RepVarNacre_>"  , self.rep_var          , "etc/start_scripts/config.sh")
      for prereq in ("Qt", "Libtar", "Xerces", "Zlib", "Poco"):
        if prereq.upper() not in self.software_dependency_list:
          self.replaceStringInFilePostInstall("<Chemin%s>" % prereq, "/usr", "etc/start_scripts/config.sh")
      self.replaceStringInFilePostInstall("<_PrinterNacre_>" , "lp"                  , "etc/start_scripts/config.sh")
      self.replaceStringInFilePostInstall("<_PrinterOption_>", "-d"                  , "etc/start_scripts/config.sh")
      self.replaceStringInFilePostInstall("<_PrinterName_>"  , self.project_options.get_global_option("printer_name")
                                                                                    , "etc/start_scripts/config.sh")
      if misc.compareVersions(self.ordered_version, "V2-3-0") >= 0:
        ssl_mode = self.project_options.get_global_option("nacre_ssl_mode")
        ssl_path = self.project_options.get_global_option("nacre_ssl_path")
        ssl_pass = self.project_options.get_global_option("nacre_ssl_pass")
        ssl_key = self.project_options.get_global_option("nacre_ssl_key")
        ssl_cert = self.project_options.get_global_option("nacre_ssl_cert")
        ssl_ca = self.project_options.get_global_option("nacre_ssl_ca")
        self.replaceStringInFilePostInstall("<SSL_mode>", ssl_mode, "etc/start_scripts/config.sh")
        self.replaceStringInFilePostInstall("<SSL_path>", ssl_path, "etc/start_scripts/config.sh")
        self.replaceStringInFilePostInstall("<SSL_pass>", ssl_pass, "etc/start_scripts/config.sh")
        self.replaceStringInFilePostInstall("<SSL_key>", ssl_key, "etc/start_scripts/config.sh")
        self.replaceStringInFilePostInstall("<SSL_cert>", ssl_cert, "etc/start_scripts/config.sh")
        self.replaceStringInFilePostInstall("<SSL_ca>", ssl_ca, "etc/start_scripts/config.sh")

      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.rep_home         , "etc/start_scripts/config_codes.sh")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.rep_home         , "etc/start_scripts/config_tiers.sh")
      self.replaceStringInFilePostInstall("<CheminPython>" , "/usr"                , "etc/start_scripts/config_tiers.sh")

      self.replaceStringInFilePostInstall("<_RepNacre_>"     , self.install_directory, "etc/valid/validation.xml")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_admin")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_qtnacre")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_xml2std")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_sde")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_sdr")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_sdp")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_services")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/nash")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/nash_noerr")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/publish")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_std2dcu")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/migre")
      self.replaceStringInFilePostInstall("<calcTotalNb>"    , str(self.project_options.get_global_option("nb_calcs"))
                                                                                    , "bin/start_calcs")
      self.replaceStringInFilePostInstall("<calcFstNum>"     , "1"                   , "bin/start_calcs")
      self.replaceStringInFilePostInstall("<_RepHomeNacre_>" , self.install_directory, "bin/start_calcs")

      self.replaceStringInFilePostInstall("<RepHomeNacre>"   , self.rep_home         , "bin/diffuseCalcs")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "bin/diffuseCalcs")
      self.replaceStringInFilePostInstall("<RepHomeNacre>"   , self.rep_home         , "bin/nettoieCalcs")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "bin/nettoieCalcs")
      self.replaceStringInFilePostInstall("<InstallRef>"     , "mono"                , "bin/nettoieCalcs")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "bin/start_diffuse")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "bin/stop_diffuse")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "etc/qtnacre/exploitation")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "etc/qtnacre/developpement")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "etc/qtnacre/ingenierie")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "etc/qtnacre/ingenierie.met")
      self.replaceStringInFilePostInstall("<RepNacre>"       , self.install_directory, "etc/qtnacre/espace")

    if dependency_name == "MUSICALE" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminMusicale>" , "$MUSICALE_INSTALL_DIR", "etc/start_scripts/config_tiers.sh")
      fichier_mdp_chiffrement = self.project_options.get_global_option("fichier_mdp_chiffrement")
      if not fichier_mdp_chiffrement:
          self.logger.error("Aucun de fichier de chiffrement donné. Renseigner l'option 'fichier_mdp_chiffrement'")
      self.replaceStringInFilePostInstall("<source_mdp_musicale>" , 'file:%s' % fichier_mdp_chiffrement, "etc/start_scripts/config.sh")
      fichier_mdp_musicale = "$MUSICALE_INSTALL_DIR/mdp_musicale"
      self.encrypt_musicale(fichier_mdp_musicale)

    if dependency_name == "MODULE_HISTORIQUE" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminModuleHistorique>" , "$MODULE_HISTORIQUE_INSTALL_DIR",
                                          "etc/start_scripts/config_tiers.sh")
      fichier_mdp_chiffrement = self.project_options.get_global_option("fichier_mdp_chiffrement")
      if not fichier_mdp_chiffrement:
          self.logger.error("Aucun de fichier de chiffrement donné. Renseigner l'option 'fichier_mdp_chiffrement'")
      self.replaceStringInFilePostInstall("<source_mdp_sinfonie>" , 'file:%s' % fichier_mdp_chiffrement, "etc/start_scripts/config.sh")
      fichier_mdp_sinfonie = "$MODULE_HISTORIQUE_INSTALL_DIR/src/templates/mdp_sinfonie"
      self.encrypt_sinfonie(fichier_mdp_sinfonie)

    if dependency_name == "POCO" or dependency_name == "ALL":
            self.replaceStringInFilePostInstall("<CheminPoco>", "$POCO_DIR", "etc/start_scripts/config.sh")

    if dependency_name == "LIBTAR" or dependency_name == "ALL":
            self.replaceStringInFilePostInstall("<CheminLibtar>", "$LIBTAR_DIR", "etc/start_scripts/config.sh")

    if dependency_name == "ORA" or dependency_name == "ALL":
            self.replaceStringInFilePostInstall("<CheminORA>", "$ORA_INSTALL_DIR", "etc/start_scripts/config_codes.sh")
            self.replaceStringInFilePostInstall("<CheminORA>", "$ORA_INSTALL_DIR", "bin/diffuseCalcs")

    if dependency_name == "HGAP" or dependency_name == "ALL":
            self.replaceStringInFilePostInstall("<CheminHGAP>", "$HGAP_INSTALL_DIR", "etc/start_scripts/config_codes.sh")
            self.replaceStringInFilePostInstall("<CheminHGAP>", "$HGAP_INSTALL_DIR", "bin/diffuseCalcs")

    if dependency_name == "XERCES" or dependency_name == "ALL":
            self.replaceStringInFilePostInstall("<CheminXerces>", "$XERCES_DIR", "etc/start_scripts/config.sh")

  def init_dependency_list(self):
      self.software_dependency_list = ["MODULE_HISTORIQUE", "MUSICALE", "POCO", "LIBTAR", "ORA", "HGAP"]
      if misc.get_calibre_version() == '9':
        self.software_dependency_list.append("XERCES")

  def get_dependency_object_for(self, dependency_name):
    dependency_object = nacre_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "MUSICALE":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MODULE_HISTORIQUE":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "POCO":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "POCO_DIR"
    if dependency_name == "LIBTAR":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "LIBTAR_DIR"
    if dependency_name == "ORA":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "HGAP":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "XERCES":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "XERCES_DIR"
    return dependency_object


  def get_type(self):
    return "composante"

  def get_pack_name(self):
    return "Nacre_Informatique"

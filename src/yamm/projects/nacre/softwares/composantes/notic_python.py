#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Julien DEREUX (EDF R&D)

import os

from yamm.projects.bthm.softwares.composantes.bthm_notic import BTHM_NOTIC
from yamm.projects.nacre.software import nacre_software


software_name = "NOTIC_PYTHON"

class NOTIC_PYTHON(BTHM_NOTIC, nacre_software):

  def init_variables(self):
    BTHM_NOTIC.init_variables(self)
    nacre_software.init_variables(self)
    
    self.install_directory = os.path.join(self.rep_home, "notic")
    
    self.configure_remote_software_pleiade_git("nacre-notic-python.git")

  def init_dependency_list(self):
    BTHM_NOTIC.init_dependency_list(self)
    self.software_dependency_list = [a for a in self.software_dependency_list if a != "CYRANO3"]
    self.software_dependency_list += ["NACRE_INFO", "CODES"]
    
  def config_local_install(self, dependency_name = "ALL"):
    BTHM_NOTIC.config_local_install(self, dependency_name, nacre_build = True)
    
    # chemin vers les BDT notic
    if dependency_name == "SELF" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<RepDataDir>",
                        "$CURRENT_SOFTWARE_INSTALL_DIR/data/bdt", "SETENV.sh")
    
    if dependency_name == "NACRE_INFO" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall(
          "<CheminNotic>",
          "$CURRENT_SOFTWARE_INSTALL_DIR",
          "$NACRE_INFO_INSTALL_DIR/etc/start_scripts/config_tiers.sh",
          True)
      self.replaceStringInFilePostInstall(
          "<RepDataDirNotic>",
          "$CURRENT_SOFTWARE_INSTALL_DIR/data/bdt",
          "$NACRE_INFO_INSTALL_DIR/bin/diffuseCalcs",
          True)
     
    # CODES remplace CYRANO3 pour l'installation de notic dans NACRE
    if dependency_name == "CODES" or dependency_name == "ALL":
      self.replaceStringInFilePostInstall("<CheminCYRANO3>",
                                          "$CODES_INSTALL_DIR", "SETENV.sh")
      self.replaceStringInFilePostInstall("<CheminC3POST>",
                                          "$CODES_INSTALL_DIR", "SETENV.sh")
      self.replaceStringInFilePostInstall("<VersionCYRANO3>",
                                          "c3res.V3-4a", "SETENV.sh")
      self.replaceStringInFilePostInstall("<VersionC3POST>",
                                          "c3post-4.4", "SETENV.sh")

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = BTHM_NOTIC.get_dependency_object_for(self,
                                                             dependency_name)
    if dependency_name == "NACRE_INFO":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "CODES":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "composante"
  
  def get_pack_name(self):
    return BTHM_NOTIC.get_pack_name(self)

  def default_values_hook(self):
    BTHM_NOTIC.default_values_hook(self)
    nacre_software.default_values_hook(self)

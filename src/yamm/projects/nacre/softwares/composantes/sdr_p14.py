#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.nacre.software import nacre_software


software_name = "SDR_P14"

class SDR_P14(nacre_software):

  def init_variables(self):
    nacre_software.init_variables(self)
    self.executor_software_name = self.name
    self.install_directory = os.path.join(self.rep_work, self.executor_software_name.lower())

    self.archive_file_name = "sdr_p14-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("nacre-sdr-p14.git")
    self.configure_git_composantes()

    self.compil_type = "rsync"

  def init_dependency_list(self):
    self.software_dependency_list = ["MUSICALE"]

  def config_local_install(self, dependency_name="ALL"):
    if dependency_name == "MUSICALE" or dependency_name == "ALL":
      self.post_install_commands.append("cp -f $MUSICALE_INSTALL_DIR/default.xml $CURRENT_SOFTWARE_INSTALL_DIR/configuration/ ;")

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.config_local_install(dependency_name)

  def get_dependency_object_for(self, dependency_name):
    dependency_object = nacre_software.get_dependency_object_for(self, dependency_name)
    if dependency_name == "MUSICALE":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def is_sdr(self):
    return True

  def sdr_type(self):
    return "P14"

  def get_type(self):
    return "composante"

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.brtv.softwares.prerequisites.coccinelle import COCCINELLE
from yamm.projects.nacre.project import get_ftp_server
from yamm.projects.nacre.software import nacre_software


software_name = "BRTV_COCCINELLE"

class BRTV_COCCINELLE(COCCINELLE, nacre_software):
  """
  Le logiciel BRTV_COCCINELLE contient le binaire coccinelle.
  """

  def __init__(self, name, version, verbose, **kwargs):
    nacre_software.__init__(self, name, version, verbose, **kwargs)
    COCCINELLE.__init__(self, name, version, verbose, **kwargs)

  def get_executor_software_name(self):
    return self.name.lower()

  def init_variables(self):
#     if not self.project_options.is_software_option(self.name, "vcs_server"):
#       self.project_options.set_software_option(software_name, "vcs_server", "noeyy727.noe.edf.fr")
    nacre_software.init_variables(self)
    COCCINELLE.init_variables(self)
    self.install_directory = os.path.join(self.rep_brtv, os.path.basename(self.executor_software_name))
    self.archive_address = get_ftp_server()
    self.remote_type = "archive"

  def get_type(self):
    return COCCINELLE.get_type(self)

  def default_values_hook(self):
    COCCINELLE.default_values_hook(self)
    nacre_software.default_values_hook(self)

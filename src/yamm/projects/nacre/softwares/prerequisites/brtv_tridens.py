#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.brtv.softwares.prerequisites.tridens import TRIDENS
from yamm.projects.nacre.project import get_ftp_server
from yamm.projects.nacre.software import nacre_software


software_name = "BRTV_TRIDENS"

class BRTV_TRIDENS(TRIDENS, nacre_software):
  """
  Le logiciel BRTV_TRIDENS contient le binaire tridens.
  """

  def __init__(self, name, version, verbose, **kwargs):
    nacre_software.__init__(self, name, version, verbose, **kwargs)
    TRIDENS.__init__(self, name, version, verbose, **kwargs)

  def get_executor_software_name(self):
    return self.name.lower()

  def init_variables(self):
    nacre_software.init_variables(self)
    TRIDENS.init_variables(self)
    self.install_directory = os.path.join(self.rep_brtv, os.path.basename(self.executor_software_name))
    self.archive_address = get_ftp_server()
    self.remote_type = "archive"

  def get_type(self):
    return TRIDENS.get_type(self)

  def default_values_hook(self):
    TRIDENS.default_values_hook(self)
    nacre_software.default_values_hook(self)

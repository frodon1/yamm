#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.projects.nacre.software import nacre_software


software_name = "ORACLE_CLIENT"

class ORACLE_CLIENT(nacre_software):
  """
  Le logiciel ORACLE_CLIENT contient à la fois les binaires oracles
  et le fichier de configuration "tnsnames.ora".
  """

  def init_variables(self):
    nacre_software.init_variables(self)
    
    self.archive_file_name = "oracle-client-" + self.version + ".tar.gz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "rsync"
    
    self.config_local_install("SELF")

  def config_local_install(self, dependency_name = "ALL"):
    if dependency_name == "SELF" or dependency_name == "ALL":
      # lien symbolique vers libclntsh.so.10.1 pour la compilation du noyau d'arpege
      if os.path.exists(self.install_directory) and not os.path.exists(os.path.join(self.install_directory,"instantclient/libclntsh.so")):
          os.symlink(os.path.join(self.install_directory,"instantclient/libclntsh.so.10.1"),os.path.join(self.install_directory,"instantclient/libclntsh.so"))   
    
  def get_type(self):
    return "prerequisite"
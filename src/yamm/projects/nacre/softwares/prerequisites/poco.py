#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os
import tempfile

from yamm.core.base import misc
from yamm.projects.nacre.software import nacre_software


software_name = "POCO"

class POCO(nacre_software):

  def init_variables(self):
    nacre_software.init_variables(self)
    # We check that the top directory does not contain symbolic links as it is not supported by Poco compilation procedure
    # If any symlink is detected, then the source directory is moved to /tmp
    top_directory = os.path.join(self.project_options.get_global_option("top_directory"))
    has_sym_link = False
    dir_in_test = os.path.sep
    for dirname in top_directory.split(os.path.sep):
      dir_in_test = os.path.join(dir_in_test, dirname)
      if os.path.islink(dir_in_test):
        has_sym_link = True
        break
      pass
    if has_sym_link:
      tmpdir = tempfile.mkdtemp(dir="/tmp")
      self.src_directory = os.path.join(tmpdir,"poco-"+self.ordered_version)

    self.archive_file_name = "poco-" + self.version + ".tar.gz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "autoconf"
    self.config_options += " --no-tests --no-samples --shared "
    if misc.compareVersions(self.ordered_version, "1.4.6")>=0:
      self.config_options += " --omit=Data/MySQL,Data/ODBC"

  def get_type(self):
    return "prerequisite"

  def get_pack_name(self):
    return software_name.lower()
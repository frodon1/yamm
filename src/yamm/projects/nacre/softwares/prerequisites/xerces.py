#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.nacre.software import nacre_software


software_name = "XERCES"


class XERCES(nacre_software):

    def init_variables(self):
        nacre_software.init_variables(self)

        self.archive_file_name = "xerces-c-src_" + self.version.replace('.', '_') + ".tar.gz"
        self.archive_type = "tar.gz"
        self.compil_type = "specific"
        compil_env = "export XERCESCROOT=%s ;" % self.src_directory
        self.specific_configure_command = compil_env
        self.specific_configure_command += "cd src/xercesc && ./runConfigure -plinux -b64 -cgcc -xg++ -P%s" % self.install_directory
        self.specific_build_command = compil_env
        self.specific_build_command += "cd src/xercesc && make"
        self.specific_install_command = compil_env
        self.specific_install_command += "cd src/xercesc && make install && make clean"

    def get_type(self):
        return "prerequisite"

    def get_pack_name(self):
        return software_name.lower()

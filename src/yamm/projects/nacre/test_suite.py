# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.core.framework.test_suite import CommandTestSuite, DownloadOnlyTestSuite
from yamm.projects.nacre.nacre_tasks import StartNacreTask, StopNacreTask
from yamm.core.base.misc import VerboseLevels

class NacreDownloadOnlyTestSuite(DownloadOnlyTestSuite):
  def __init__(self, name, soft_name="", soft_version="", version = None, verbose=VerboseLevels.INFO):
    DownloadOnlyTestSuite.__init__(self, name, soft_name, soft_version, version, verbose)
    self.insource = False
    self.post_install = True
    self.post_move = True
    self.remote_system_type = "git"
    self.repository_name = "valinacre.git"
    self.root_repository = "http://noev01tn.noe.edf.fr:9070/NCR"

class NacreTestSuite(CommandTestSuite):

  def __init__(self, name, soft_name="", soft_version="", version = None,
               nacre_install_dir = None, nacre_tests_dir = None):
    CommandTestSuite.__init__(self, name, soft_name, soft_version, version)
    self.nacre_tests_dir = nacre_tests_dir
    self.start_nacre_task = StartNacreTask(nacre_install_dir = nacre_install_dir)
#     self.stop_nacre_task = StopNacreTask(nacre_install_dir = nacre_install_dir)
    self.insource = True
    self.post_install = True
    self.post_move = True
  
  def create_post_compile_tasks(self):
    post_build_tasks, post_install_tasks = CommandTestSuite.create_post_compile_tasks(self)
    if self.post_build:
      post_build_tasks.insert(0, self.start_nacre_task)
#       post_build_tasks.append(self.stop_nacre_task)
    elif self.post_install:
      post_install_tasks.insert(0, self.start_nacre_task)
#       post_install_tasks.append(self.stop_nacre_task)
    return (post_build_tasks, post_install_tasks)

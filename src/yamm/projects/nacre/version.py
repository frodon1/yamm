#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)
import os

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.version import FrameworkVersion
from yamm.projects.nacre.test_suite import NacreDownloadOnlyTestSuite, NacreTestSuite


class NacreVersion(FrameworkVersion):
  """
    .. py:currentmodule:: yamm.projects.salome.NacreVersion

    Base class for NACRE's versions. It adds new
    methods for specific capabilities of NACRE project.

    :param string name: name of the version.
    :param int verbose: verbose level of the version.

  """
  
  def __init__(self, name='NOT CONFIGURED', verbose=VerboseLevels.INFO, flavour=''):
    FrameworkVersion.__init__(self, name, verbose, flavour)
    self.version_integration = 'dev'

#######
#
# Méthodes qui doivent être surchargées
#
#######

  def get_nacre_version(self):
    """
      This method is optional. It should return the NACRE version targeted by
      the version.
    """
    return None


  def configure_tests(self, project_options):
    version_directory = project_options.get_global_option('version_directory')
    nacre_home = os.path.join(version_directory, 'install', 'home')
    
    # Create the task to download the git repository nacre_tests
    nacretests_download = NacreDownloadOnlyTestSuite('NACRE_TESTS', self.name,
                                version=self.version_integration)
    nacretests_src_directory = os.path.join(version_directory, 'test_src',
                                 self.name + "_" + nacretests_download.name \
                                 + "_" + nacretests_download.version)
    nacretests_download.src_directory = nacretests_src_directory
    nacretests_download.work_directory = nacretests_src_directory
    
    # Create the tasks to run the tests
    nacretests_dcu = NacreTestSuite('NACRE_TESTS_DCU', self.name,
                                version=self.version_integration,
                                nacre_install_dir=version_directory,
                                nacre_tests_dir = nacretests_download.src_directory) 
    nacretests_ncg = NacreTestSuite('NACRE_TESTS_NCG', self.name,
                                version=self.version_integration,
                                nacre_install_dir=version_directory,
                                nacre_tests_dir=nacretests_src_directory) 
    nacretests_nash = NacreTestSuite('NACRE_TESTS_NASH', self.name,
                                version=self.version_integration,
                                nacre_install_dir=version_directory,
                                nacre_tests_dir=nacretests_src_directory)
    
    for test in (nacretests_dcu, nacretests_ncg, nacretests_nash):
      test.work_directory = os.path.join(version_directory, 'test_workdir',
                                         self.name + "_" + test.name + "_" \
                                         + test.version)
      if (os.path.exists(test.work_directory)):
        misc.fast_delete(test.work_directory)
      
    nacretests_dcu.command = '{0}/TS_COMPOSANT_NACRE/scripts/dcu/run_test \
                              -d {1} -r {2}'\
                              .format(nacretests_dcu.nacre_tests_dir,
                                      nacre_home,
                                      nacretests_dcu.work_directory)
    nacretests_ncg.command = '{0}/TS_COMPOSANT_NACRE/scripts/ncg/run_test \
                              -d {1} -r {2}'\
                              .format(nacretests_ncg.nacre_tests_dir,
                                      nacre_home,
                                      nacretests_ncg.work_directory)
    nacretests_nash.command = '{0}/TS_COMPOSANT_NACRE/scripts/nash/run_test \
                              -d {1} -r {2}'\
                              .format(nacretests_nash.nacre_tests_dir,
                                      nacre_home,
                                      nacretests_nash.work_directory)
   
    self.test_suite_list = [nacretests_download, nacretests_dcu, 
                            nacretests_ncg, nacretests_nash]

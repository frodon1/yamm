#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.version import NacreVersion

version_name = "DEV"
version_value = "V2-5-0"
version_vcs = "dev"


class DEV(NacreVersion):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        NacreVersion.__init__(self, name, verbose, flavour)
        self.accepted_flavours = ['tests',
                                  'rigel_1143',
                                  'rigel_1117',
                                  'rigel_965',
                                  'rigel_1117',
                                  'rigel_1173',
                                  'rigel_1251',
                                  'rigel_1253',
                                  'rigel_1328',
                                  'integration_brtv'
                                  ]

    def configure_softwares(self):
        # prerequis
        self.add_software("ORACLE_CLIENT", "10.2")
        self.add_software("POCO", "1.4.6p4-all-edfp1")
        self.add_software("LIBTAR", "1.2.20")
        # composantes
        self.add_software("SDP", version_vcs, version_value)
        self.add_software("SDR_P14", version_vcs, version_value)
        self.add_software("SDR_PN3C", version_vcs, version_value)
        self.add_software("BIBLIO", version_vcs, version_value)
        self.add_software('CODES', version_vcs, version_value)
        self.add_software("MUSICALE", version_vcs, version_value)
        self.add_software("INSTALL_MULTI", version_vcs, version_value)
        self.add_software("ESPACE", version_vcs, version_value)
        self.add_software("FORMULAIRE_AVANCE", version_vcs, version_value)
        self.add_software("MODULE_HISTORIQUE", version_vcs, version_value)
        self.add_software("NACRE_INFO", version_vcs, version_value)
        self.add_software("ICAREX", version_vcs, "v3.13.0")
        # BTHM
        self.add_software("NOTIC_PYTHON", "git_tag/BTHM_2-3-0", "V2-3-0")
        # BRTV
        self.add_software("BRTV", "tags/V3.2-rc5", "V3.2")
        self.add_software("BRTV_TRIDENS", "V2.3.3")
        self.add_software("BRTV_COCCINELLE", "v3.11")
        self.add_software("BRTV_THYCOX", "V2.10")
        self.add_software("BRTV_THYC", "V5.1")
        self.add_software("BRTV_OSCARD", "V4.4")

    def update_configuration_with_flavour(self):
        NacreVersion.update_configuration_with_flavour(self)
        if self.flavour == 'rigel_1143':
            self.set_software_version('NACRE_INFO', "nka/rigel_1143", version_value)
            self.set_software_version('MODULE_HISTORIQUE', "nka/rigel_1143", version_value)
            self.set_software_version('MUSICALE', "nka/rigel_1143", version_value)
            self.set_software_version('INSTALL_MULTI', "nka/rigel_1143", version_value)
        if self.flavour == 'rigel_1328':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1328", version_value)
        if self.flavour == 'rigel_965':
            self.set_software_version('NACRE_INFO', "gdd/rigel965_affichage_graphe", version_value)
        if self.flavour == 'rigel_1251':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1251", version_value)
            self.set_software_version('SDR_P14', "gdd/rigel_1251", version_value)
            self.set_software_version('SDR_PN3C', "gdd/rigel_1251", version_value)
        if self.flavour == 'rigel_1253':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1253", version_value)
        if self.flavour == 'rigel_1173':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1173", version_value)
        if self.flavour == 'rigel_1117':
            self.set_software_version('INSTALL_MULTI', "gdd/RIGEL1117_sortie_ora", version_value)
            self.set_software_version('NACRE_INFO', "gdd/RIGEL1117_sortie_ora", version_value)
            self.set_software_version('ICAREX', "tcl/RIGEL1117_sortie_ora", "v3.14.0")
            self.set_software_version('SDR_P14', "gdd/RIGEL1117_sortie_ora", version_value)
            self.set_software_version('SDR_PN3C', "gdd/RIGEL1117_sortie_ora", version_value)
            self.add_software('ORA', "gdd/RIGEL1117_sortie_ora", version_value)
        if 'tests' in self.flavour:
            self.add_software("NOAH", version_vcs)
            self.add_software("VALINACRE", 'master')
            self.add_software("ICAREX_TESTS", version_vcs)
            self.add_software("NACRE_TESTS", version_vcs)
        if 'integration_brtv' in self.flavour:
            self.set_software_version('BRTV', "tags/V3.3-rc2", "V3.3")
            self.set_software_version('BRTV_THYC', "V5.2", "V5.2")
            self.set_software_version('BRTV_THYCOX', "V2.11", "V2.11")
            self.set_software_version('BRTV_OSCARD', "V5.1", "V5.1")
            self.set_software_version('BRTV_COCCINELLE', "v3.13", "v3.13")
            for soft in ('NACRE_INFO', 'SDR_P14'):
                self.set_software_version(soft, 'tcl/rigel1373_integration_brtv', version_value)

    def get_nacre_version(self):
        return "V2-5-0"

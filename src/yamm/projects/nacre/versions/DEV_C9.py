#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.versions.DEV import DEV

version_name = "DEV_C9"
version_value = "V2-5-0"
version_vcs = "calibre9"


class DEV_C9(DEV):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        DEV.__init__(self, name, verbose, flavour)

    def configure_softwares(self):
        # prerequis
        DEV.configure_softwares(self)
        self.remove_software("LIBTAR")
        self.add_software("XERCES", "2.8.0")
        self.set_software_version('NACRE_INFO', version_vcs, version_value)
        self.set_software_version('MUSICALE', version_vcs, version_value)
        self.set_software_version('SDR_P14', version_vcs, version_value)
        self.set_software_version('SDR_PN3C', version_vcs, version_value)
        self.set_software_version('ICAREX', version_vcs, "v3.13.0")
        self.set_software_version('CODES', version_vcs, version_value)

    def update_configuration_with_flavour(self):
        DEV.update_configuration_with_flavour(self)
        if self.flavour == 'rigel_1173':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1173_c9", version_value)
        if self.flavour == 'rigel_1143':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1143_c9", version_value)
            self.set_software_version('MUSICALE', "gdd/rigel_1143_c9", version_value)
        if self.flavour == 'rigel_1117':
            self.set_software_version('NACRE_INFO', "gdd/RIGEL1117_sortie_ora-c9", version_value)
            self.set_software_version('ICAREX', "gdd/RIGEL1117_sortie_ora-c9", "v3.14.0")
            self.set_software_version('SDR_P14', "gdd/RIGEL1117_sortie_ora-c9", version_value)
            self.set_software_version('SDR_PN3C', "gdd/RIGEL1117_sortie_ora-c9", version_value)
        if self.flavour == 'rigel_1328':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1328-c9", version_value)
        if self.flavour == 'rigel_965':
            self.set_software_version('NACRE_INFO', "gdd/rigel965_affichage_graphe-c9", version_value)
        if self.flavour == 'rigel_1117':
            self.set_software_version('NACRE_INFO', "gdd/RIGEL1117_sortie_ora-c9", version_value)
            self.set_software_version('ICAREX', "gdd/RIGEL1117_sortie_ora-c9", "v3.14.0")
            self.set_software_version('SDR_P14', "gdd/RIGEL1117_sortie_ora-c9", version_value)
            self.set_software_version('SDR_PN3C', "gdd/RIGEL1117_sortie_ora-c9", version_value)
        if self.flavour == 'rigel_1251':
            self.set_software_version('NACRE_INFO', "gdd/rigel_1251-c9", version_value)
            self.set_software_version('SDR_P14', "gdd/rigel_1251-c9", version_value)
            self.set_software_version('SDR_PN3C', "gdd/rigel_1251-c9", version_value)

    def get_nacre_version(self):
        return "V2-5-0"

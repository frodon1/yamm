#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.version import NacreVersion

version_name = "V2_4_0"
version_value = "V2-4-0"
version_vcs = "git_tag/V2-4-0"


class V2_4_0(NacreVersion):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        NacreVersion.__init__(self, name, verbose, flavour)
        self.accepted_flavours = ['valinacre']

    def configure_softwares(self):
        # prerequis
        self.add_software("ORACLE_CLIENT", "10.2")
        self.add_software("POCO", "1.4.6p4-all-edfp1")
        self.add_software("LIBTAR", "1.2.20")
        # composantes
        self.add_software("SDP", version_vcs, version_value)
        self.add_software("SDR_P14", version_vcs, version_value)
        self.add_software("SDR_PN3C", version_vcs, version_value)
        self.add_software("BIBLIO", version_vcs, version_value)
        self.add_software('CODES', version_vcs, version_value)
        self.add_software("MUSICALE", version_vcs, version_value)
        self.add_software("INSTALL_MULTI", version_vcs, version_value)
        self.add_software("ESPACE", version_vcs, version_value)
        self.add_software("FORMULAIRE_AVANCE", version_vcs, version_value)
        self.add_software("MODULE_HISTORIQUE", version_vcs, version_value)
        self.add_software("NACRE_INFO", version_vcs, version_value)
        self.add_software("ICAREX", version_vcs, "v3.13.0")
        # BTHM
        self.add_software("NOTIC_PYTHON", "git_tag/BTHM_2-3-0", "V2-3-0")
        # BRTV
        self.add_software("BRTV", "tags/V3.2-rc5", "V3.2")
        self.add_software("BRTV_TRIDENS", "V2.3.3")
        self.add_software("BRTV_COCCINELLE", "v3.11")
        self.add_software("BRTV_THYCOX", "V2.10")
        self.add_software("BRTV_THYC", "V5.1")
        self.add_software("BRTV_OSCARD", "V4.4")

    def update_configuration_with_flavour(self):
        NacreVersion.update_configuration_with_flavour(self)

        if 'valinacre' in self.flavour:
            self.add_software("PYKKA", "1.2.1")
            self.add_software("VALINACRE", 'gdd/recette', 'V2-4-0')

    def get_nacre_version(self):
        return "V2-4-0"

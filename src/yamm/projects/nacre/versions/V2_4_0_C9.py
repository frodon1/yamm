#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.versions.V2_4_0 import V2_4_0

version_name = "V2_4_0_C9"
version_value = "V2-4-0"
version_vcs = "release-V2-4-0_c9"


class V2_4_0_C9(V2_4_0):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        V2_4_0.__init__(self, name, verbose, flavour)

    def configure_softwares(self):
        V2_4_0.configure_softwares(self)
        self.remove_software("LIBTAR")
        self.add_software("XERCES", "2.8.0")
        self.set_software_version('NACRE_INFO', version_vcs, version_value)
        self.set_software_version('SDR_P14', version_vcs, version_value)
        self.set_software_version('SDR_PN3C', version_vcs, version_value)
        self.set_software_version('ICAREX', version_vcs, "v3.13.0")
        self.set_software_version('MUSICALE', version_vcs, version_value)
        self.set_software_version('CODES', version_vcs, version_value)

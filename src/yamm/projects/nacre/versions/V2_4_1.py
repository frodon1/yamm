#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.versions.V2_4_0 import V2_4_0

version_name = "V2_4_1"
version_value = "V2-4-1"
version_vcs = "maintenance-V2-4-0"


class V2_4_1(V2_4_0):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        V2_4_0.__init__(self, name, verbose, flavour)
        self.accepted_flavours = ['tests']

    def configure_softwares(self):
        # prerequis
        V2_4_0.configure_softwares(self)
        # composantes
        self.set_software_version("SDP", version_vcs, version_value)
        self.set_software_version("SDR_P14", version_vcs, version_value)
        self.set_software_version("SDR_PN3C", version_vcs, version_value)
        self.set_software_version("BIBLIO", version_vcs, version_value)
        self.set_software_version('CODES', version_vcs, version_value)
        self.set_software_version("MUSICALE", version_vcs, version_value)
        self.set_software_version("INSTALL_MULTI", version_vcs, version_value)
        self.set_software_version("ESPACE", version_vcs, version_value)
        self.set_software_version("FORMULAIRE_AVANCE", version_vcs, version_value)
        self.set_software_version("MODULE_HISTORIQUE", version_vcs, version_value)
        self.set_software_version("NACRE_INFO", version_vcs, version_value)
        self.set_software_version("ICAREX", version_vcs, "v3.13.1")

    def update_configuration_with_flavour(self):
        V2_4_0.update_configuration_with_flavour(self)
        if 'tests' in self.flavour:
            self.add_software("NOAH", version_vcs, version_value)
            self.add_software("VALINACRE", 'tcl/maj_ref_240', version_value)
            self.add_software("ICAREX_TESTS", version_vcs, version_value)
            self.add_software("NACRE_TESTS", version_vcs, version_value)

    def get_nacre_version(self):
        return "V2-4-1"

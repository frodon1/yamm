#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.version import NacreVersion

version_name = "V2_5_0"
version_value = "V2-5-0"
version_vcs = "master"


class V2_5_0(NacreVersion):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        NacreVersion.__init__(self, name, verbose, flavour)
        self.version_integration = version_vcs
        self.accepted_flavours = ["int_1","int_2","int_3","int_4","int_5","rc1","rc2","rc3","rc4","rc5","rc6"]

    def configure_softwares(self):
        # prerequis
        self.add_software("ORACLE_CLIENT", "10.2")
        self.add_software("POCO", "1.4.6p4-all-edfp1")
        self.add_software("LIBTAR", "1.2.20")
        # composantes
        self.add_software("SDP", version_vcs, version_value)
        self.add_software("SDR_P14", version_vcs, version_value)
        self.add_software("SDR_PN3C", version_vcs, version_value)
        self.add_software("BIBLIO", version_vcs, version_value)
        self.add_software('CODES', version_vcs, version_value)
        self.add_software("MUSICALE", version_vcs, version_value)
        self.add_software("INSTALL_MULTI", version_vcs, version_value)
        self.add_software("ESPACE", version_vcs, version_value)
        self.add_software("FORMULAIRE_AVANCE", version_vcs, version_value)
        self.add_software("MODULE_HISTORIQUE", version_vcs, version_value)
        self.add_software("NACRE_INFO", version_vcs, version_value)
        self.add_software("ICAREX", version_vcs, "v3.14.0")
        self.add_software('ORA', version_vcs, version_value)
        # Tools
        self.add_software('VALINACRE', version_vcs, version_value)
        self.add_software('ICAREX_TESTS', version_vcs, version_value)
        # BTHM
        self.add_software("NOTIC_PYTHON", "git_tag/BTHM_2-3-0", "V2-3-0")
        # BRTV
        self.add_software("BRTV", "tags/V3.3", "V3.3")
        self.add_software("BRTV_TRIDENS", "V2.3.3")
        self.add_software("BRTV_COCCINELLE", "v3.13")
        self.add_software("BRTV_THYCOX", "V2.11")
        self.add_software("BRTV_THYC", "V5.2")
        self.add_software("BRTV_OSCARD", "V5.1")

    def update_configuration_with_flavour(self):
        NacreVersion.update_configuration_with_flavour(self)

        # Modifications communes aux release candidates
        if 'rc' in self.flavour:
            self.version_integration = 'git_tag/{0}-{1}'.format(version_value, self.flavour)
            for soft in ('SDP', 'SDR_P14', 'SDR_PN3C',
                         'BIBLIO', 'CODES', 'MUSICALE', 'INSTALL_MULTI',
                         'ESPACE', 'FORMULAIRE_AVANCE',
                         'MODULE_HISTORIQUE', 'NACRE_INFO', 'ORA'):
                self.set_software_version(soft, self.version_integration, version_value)
            self.add_software("ICAREX", self.version_integration, "v3.14.0")
            # Ajout des softwares de tests
            self.add_software("ICAREX_TESTS", self.version_integration, version_value)
            self.add_software("VALINACRE", self.version_integration, version_value)

        if "int_" in self.flavour:
            self.version_integration = 'git_tag/{0}_{1}'.format(version_value, self.flavour)
            for soft in ('SDP', 'SDR_P14', 'SDR_PN3C',
                         'BIBLIO', 'CODES', 'MUSICALE', 'INSTALL_MULTI',
                         'ESPACE', 'FORMULAIRE_AVANCE',
                         'MODULE_HISTORIQUE', 'NACRE_INFO', 'ORA'):
                self.set_software_version(soft, self.version_integration, version_value)
            self.add_software("ICAREX", self.version_integration, "v3.14.0")
            # Ajout des softwares de tests
            self.add_software("ICAREX_TESTS", self.version_integration, version_value)
            self.add_software("VALINACRE", self.version_integration, version_value)
            # Ajout BRTV 
            if int(self.flavour.strip("int_")) < 3:
                self.add_software("BRTV", "tags/V3.2", "V3.2")
                self.add_software("BRTV_COCCINELLE", "v3.11")
                self.add_software("BRTV_THYCOX", "V2.10")
                self.add_software("BRTV_THYC", "V5.1")
                self.add_software("BRTV_OSCARD", "V4.4")


    def get_nacre_version(self):
        return "V2-5-0"

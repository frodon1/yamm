#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.versions.V2_5_0 import V2_5_0

version_name = "V2_5_0_C9"
version_value = "V2-5-0"
version_vcs = "asa/migration_calibre_9"


class V2_5_0_C9(V2_5_0):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        V2_5_0.__init__(self, name, verbose, flavour)

    def configure_softwares(self):
        V2_5_0.configure_softwares(self)
        self.remove_software("LIBTAR")
        self.add_software("XERCES", "2.8.0")
        self.set_software_version('NACRE_INFO', version_vcs, version_value)
        self.set_software_version('SDR_P14', version_vcs, version_value)
        self.set_software_version('SDR_PN3C', version_vcs, version_value)
        self.set_software_version('CODES', version_vcs, version_value)
        self.set_software_version('ICAREX', version_vcs, "v3.14.0")
        self.set_software_version('MUSICALE', version_vcs, version_value)

    def update_configuration_with_flavour(self):
        V2_5_0.update_configuration_with_flavour(self)

        if "test_icarex" == self.flavour:
            self.version_integration = 'qbu/integration_nacre_2_5_0_icarex_c9'
            self.set_software_version('SDR_P14', self.version_integration, version_value)
            self.set_software_version('SDR_PN3C', self.version_integration, version_value)
            self.set_software_version('ICAREX', self.version_integration, 'v3.14.0')

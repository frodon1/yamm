# -*- coding: utf-8 -*-
versions_list = ["DEV", "DEV_C9"]
# V2.4
versions_list += ['V2_4_0', 'V2_4_0_C9']
versions_list += ['V2_4_1', 'V2_4_1_C9']
# V2.5
versions_list += ['V2_5_0', 'V2_5_0_C9']
# V2.6
versions_list += ['V2_6_0']

LAST_STABLE = 'V2_5_0'


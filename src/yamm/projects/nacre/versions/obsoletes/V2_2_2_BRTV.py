#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.versions.V2_2_2 import V2_2_2

version_name = "V2_2_2_BRTV"
version_value = "V2-2-2"
version_vcs = "release-V2-2-2"

class V2_2_2_BRTV(V2_2_2):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    V2_2_2.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    V2_2_2.configure_softwares(self)
    self.add_software("BRTV",              "master", "V2.6")   
    self.add_software("SDR_P14",           "JD-integration_BRTV_2-6-0", version_value)
    self.add_software("SDR_PN3C",          "JD-integration_BRTV_2-6-0", version_value)
    self.add_software("NACRE_INFO",        "JD-integration_BRTV_2-6-0", version_value)

  def get_nacre_version(self):
    return "V2-2-2"

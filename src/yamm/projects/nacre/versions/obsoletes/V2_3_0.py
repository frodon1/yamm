#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.nacre.version import NacreVersion

version_name = "V2_3_0"
version_value = "V2-3-0"
version_vcs = "git_tag/V2-3-0"

class V2_3_0(NacreVersion):

  def __init__(self, name=version_name, verbose=0, flavour = ""):
    NacreVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    self.add_software("ORACLE_CLIENT",     "10.2")
    self.add_software("POCO",              "1.4.6p4-all-edfp1")
    self.add_software("LIBTAR",            "1.2.20")

    self.add_software("SDP",               version_vcs, version_value)
    self.add_software("SDR_P14",           version_vcs, version_value)
    self.add_software("SDR_PN3C",          version_vcs, version_value)
    
    self.add_software("BIBLIO",            version_vcs, version_value)
    self.add_software("CODES_NACRE",       version_vcs, version_value)
    self.add_software("CODES",             version_vcs, version_value)
    self.add_software("MUSICALE",          version_vcs, version_value)
    self.add_software("BRTV",              version_vcs, "V2.6")
    self.add_software("NOTIC",             "git_tag/BTHM_2-2-0", version_value)
    self.add_software("ESPACE",            version_vcs, version_value)
    self.add_software("FORMULAIRE_AVANCE", version_vcs, version_value)

    self.add_software("MODULE_HISTORIQUE", version_vcs, version_value)
    self.add_software("NACRE_INFO",        version_vcs, version_value)
    self.add_software("ICAREX",            version_vcs, "v3.12.0")

    self.add_software("INSTALL_MULTI",     version_vcs, version_value)

  def get_nacre_version(self):
    return "V2-3-0"

# -*- coding: utf-8 -*-
# V2.2
versions_list += ["V2_2_0", "V2_2_1", "V2_2_2"]
versions_list += ["V2_2_2_BTHM"]
versions_list += ["V2_2_2_BRTV"]
# V2.3
versions_list += ["V2_3_0", "V2_3_0_1"]

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="intro.ui" line="14"/>
        <source>YAMM_TITLE</source>
        <translation>YAMM for OPENTURNS</translation>
    </message>
    <message>
        <location filename="intro.ui" line="27"/>
        <source>CREATE_PROJECT_BASIC</source>
        <translation>Create a OPENTURNS Project (Basic)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="34"/>
        <source>CREATE_PROJECT_ADVANCED</source>
        <translation>Create a OPENTURNS Project (Advanced)</translation>
    </message>
    <message>
        <location filename="intro.ui" line="48"/>
        <source>LOAD_PROJECT</source>
        <translation>Load a OPENTURNS Project</translation>
    </message>
    <message>
        <location filename="intro.ui" line="41"/>
        <source>COPY_PROJECT</source>
        <translation>Copy a OPENTURNS Project</translation>
    </message>
    <message>
        <location filename="intro.ui" line="72"/>
        <source>Recent projects</source>
        <translation></translation>
    </message>
</context>
</TS>

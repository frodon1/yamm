#  Copyright (C) 2012 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.core.base import misc

installer_openturns_prerequisite_header = """#!/bin/bash
if test -z "${ROOT_OPENTURNS}" ; then
  export ROOT_OPENTURNS="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"
fi

if test -z "${ROOT_OPENTURNS}" ; then
  echo "======================================="
  echo "Error: OpenTURNS install path not found"
  echo "======================================="
fi

export LC_NUMERIC=C

"""
installer_openturns_prerequisite_header_check_prereq = """if test ! -d "${ROOT_OPENTURNS}/prerequisites"; then
  echo "======================================"
  echo "Error: OpenTURNS install path is wrong"
  echo "======================================"
fi
export PREREQUISITES_ROOT_DIR="${ROOT_OPENTURNS}/prerequisites"

"""
installer_openturns_prerequisite_header_check_tools = """if test ! -d "${ROOT_OPENTURNS}/tools"; then
  echo "======================================"
  echo "Error: OpenTURNS install path is wrong"
  echo "======================================"
fi
export TOOLS_ROOT_DIR="${ROOT_OPENTURNS}/tools"

"""

installer_cp_current_template = """
if not os.path.exists(%target_template):
  shutil.copyfile(%target, %target_template)
if not os.path.exists(%target_current):
  shutil.copyfile(%target_template, %target_current)
"""
installer_cp_current_template = misc.PercentTemplate(installer_cp_current_template)

installer_decompress_usage = """usage()
{
cat<<EOF
usage: $0 [-h] [-t DIR] [-f] [-v]

The script installs OpenTURNS in a specified directory.

Options:
 -h    Show this message
 -t    OpenTURNS install directory (default is $HOME/openturns)
 -f    Force installation
 -v    Verbose mode
EOF
}

"""
installer_decompress_usage = misc.PercentTemplate(installer_decompress_usage)

installer_decompress_template = """#!/bin/bash
echo ""
echo "====================================%{title_deco}"
echo "Self Extracting OpenTURNS %{version} Installer"
echo "====================================%{title_deco}"
echo ""

CRCsum=%{crcsum}
MD5=%{md5sum}

%{bar_cat}

export BAR_CMD="tar xzf -"
export BAR_ETA=0
export BAR_CLEAR=1

die()
{
  echo "FATAL ERROR: $* (status $?)" 1>&2
  exit 1
}

# Inspired from http://www.megastep.org/makeself/
check()
{
    OLD_PATH="$PATH"
    PATH=${GUESS_MD5_PATH:-"$OLD_PATH:/bin:/usr/bin:/sbin:/usr/local/ssl/bin:/usr/local/bin:/opt/openssl/bin"}
    MD5_ARG=""
    MD5_PATH=`exec <&- 2>&-; which md5sum || type md5sum`
    test -x "$MD5_PATH" || MD5_PATH=`exec <&- 2>&-; which md5 || type md5`
    test -x "$MD5_PATH" || MD5_PATH=`exec <&- 2>&-; which digest || type digest`
    PATH="$OLD_PATH"

    echo "Verifying archive integrity..."
    verb=$2

    crc=`echo $CRCsum | cut -d" " -f 1`
    if test -x "$MD5_PATH"; then
      if test `basename $MD5_PATH` = digest; then
        MD5_ARG="-a md5"
      fi
      md5=`echo $MD5 | cut -d" " -f 1`
      if test $md5 = "00000000000000000000000000000000"; then
        test x$verb = xy && echo " $1 does not contain an embedded MD5 checksum." >&2
      else
        md5sum=`eval "$MD5_PATH $MD5_ARG" $1 | cut -b-32`;
        if test "$md5sum" != "$md5"; then
          echo "Error in MD5 checksums: $md5sum is different from $md5" >&2
          rm $1
          exit 2
        else
          test x$verb = xy && echo "MD5 checksums are OK." >&2
        fi
        crc="0000000000"; verb=n
      fi
    fi
    if test $crc = "0000000000"; then
      test x$verb = xy && echo " $1 does not contain a CRC checksum." >&2
    else
      sum1=`CMD_ENV=xpg4 cksum $1 | awk '{print $1}'`
      if test "$sum1" = "$crc"; then
        test x$verb = xy && echo "CRC checksums are OK." >&2
      else
        echo "Error in checksums: $sum1 is different from $crc"
        rm $1
        exit 2;
      fi
    fi

    echo "All good."
}

%{usage}
TARGETDIR=""
FORCE=""
VERBOSE=""

while getopts "ht:fv" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    t)
      TARGETDIR=$OPTARG
      ;;
    f)
      FORCE="y"
      ;;
    v)
      VERBOSE="y"
      ;;
    ?)
      usage
      exit 1
      ;;
  esac
done

ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0`

DEFAULTTARGETDIR=$HOME/openturns
[ "$TARGETDIR" = "" ] && echo -n "Enter the OpenTURNS install directory [default=$DEFAULTTARGETDIR] : " && read -e -p "" TARGETDIR
eval TARGETDIR=$TARGETDIR # to expand the tilde
[ "$TARGETDIR" = "" ] && TARGETDIR=$DEFAULTTARGETDIR

if test -d "$TARGETDIR/%{archive_name}"; then
  [ "$FORCE" = "" ] && echo -n "The directory $TARGETDIR/%{archive_name} already exists, install anyway ? [y/N] " && read -p "" FORCE
  case "$FORCE" in
    y|Y|yes|YES|Yes|o|O|oui|OUI|Oui) FORCE="y" ;;
  esac
  if test "$FORCE" != "y"; then
    echo "Installation aborted"
    exit 1
  fi
fi
  
mkdir -p $TARGETDIR || die "mkdir failed"

echo "Installation of OpenTURNS %{version} in $TARGETDIR/%{archive_name} ..."

tail -n+$ARCHIVE $0 > $TARGETDIR/%{archive_name}.tgz

cd $TARGETDIR

check $TARGETDIR/%{archive_name}.tgz $VERBOSE

if test "$VERBOSE" = "y"; then
  tar xvzf %{archive_name}.tgz
else
  bar_cat %{archive_name}.tgz
fi

echo "Post install ..."
. $TARGETDIR/%{archive_name}/%{env_filename}
$TARGETDIR/%{archive_name}/openturns_post_install.py

echo "Done"

cat $TARGETDIR/%{archive_name}/README

rm %{archive_name}.tgz

exit 0

__ARCHIVE_BELOW__
"""

installer_decompress_template = misc.PercentTemplate(installer_decompress_template)


installer_openturns_launch_python_template = """#!/bin/bash
ROOT_OPENTURNS="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"
. $ROOT_OPENTURNS/%{env_script_name}
python "$@"
"""
installer_openturns_launch_python_template = misc.PercentTemplate(installer_openturns_launch_python_template)

installer_eficas_wrapper_template = """#!/bin/bash
ROOT_OPENTURNS="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"
. $ROOT_OPENTURNS/%{env_script_name}
python $EFICAS_ROOT/Openturns_Wrapper/qtEficas_openturns_wrapper.py "$@"
"""
installer_eficas_wrapper_template = misc.PercentTemplate(installer_eficas_wrapper_template)

installer_eficas_study_template = """#!/bin/bash
ROOT_OPENTURNS="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"
. $ROOT_OPENTURNS/%{env_script_name}
python $EFICAS_ROOT/Openturns_Study/qtEficas_openturns_study.py "$@"
"""
installer_eficas_study_template = misc.PercentTemplate(installer_eficas_study_template)

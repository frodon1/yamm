# -*- coding: utf-8 -*-
#  Copyright (C) 2012 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)


from __future__ import print_function
from builtins import str
import sys
import os
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base import misc

import subprocess
import time
import shutil
import tarfile
import datetime
import stat

from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command
from yamm.core.framework.installer import installer_bar_cat

from yamm.projects.salome.softwares import prerequisites as salome_prerequisites
from yamm.projects.salome.softwares import tools         as salome_tools
from yamm.projects.salome           import project       as SalomeProject

from yamm.projects.openturns import versions  as default_versions
from yamm.projects.openturns.installer import strings

def get_ftp_server():
  return SalomeProject.get_ftp_server()

class Project(FrameworkProject):

  def __init__(self, log_name="OPENTURNS", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.define_openturns_specific_options()

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="OPENTURNS", verbose=self.verbose_level)
    # Import softwares
    for software_name in salome_prerequisites.modules_list:
        self.add_software_in_catalog(software_name, "yamm.projects.salome.softwares.prerequisites")

    self.add_software_in_catalog("matplotlib", "yamm.projects.openturns.softwares.prerequisites")  # overloads salome config for matplotlib

    for software_name in salome_tools.modules_list:
        self.add_software_in_catalog(software_name, "yamm.projects.salome.softwares.tools")
    # Import versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.openturns.versions")
    self.logger.debug("End init catalog")

  def define_openturns_specific_options(self):
    self.options.add_option("openturns_env_file", "", ["global"])
    self.options.add_option("occ_username", "", ["global", "category", "software"])

    # Add two categories
    self.options.add_category("prerequisite")
    self.options.add_category("tool")

    # Salome compatibility
    self.options.add_option("salome_prerequisites_env_file", "", ["global"])
    self.options.add_option("salome_modules_env_file", "", ["global"])

  def check_specific_configuration(self, current_command_options):

    if current_command_options.get_global_option('openturns_env_file') != "":
      current_command_options.check_abs_path('openturns_env_file',
                                             logger=self.logger)

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "openturns"))
    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)
    # Specific Openturns options
    if not current_command_options.get_global_option("openturns_env_file"):
      current_command_options.set_global_option("openturns_env_file", os.path.join(current_command_options.get_global_option("version_directory"), "env_openturns_" + self.version + ".sh"))
    # Set main directory for softwares
    if not current_command_options.get_global_option("main_software_directory"):
      current_command_options.set_global_option("main_software_directory", os.path.join(current_command_options.get_global_option("version_directory")))
    current_command_options.set_global_option("salome_prerequisites_env_file", current_command_options.get_global_option("openturns_env_file"))
    current_command_options.set_global_option("salome_modules_env_file", current_command_options.get_global_option("openturns_env_file"))

    if not current_command_options.is_category_option("prerequisite", "binary_archive_url"):
      current_command_options.set_category_option("prerequisite", "binary_archive_url", os.path.join(current_command_options.get_global_option("top_directory"), "binary_archives", "%(name)s.tgz"))
    if not current_command_options.is_category_option("tool", "binary_archive_url"):
      current_command_options.set_category_option("tool", "binary_archive_url", os.path.join(current_command_options.get_global_option("top_directory"), "binary_archives", "%(name)s.tgz"))

    if not current_command_options.is_category_option("prerequisite", "source_type"):
      current_command_options.set_category_option("prerequisite", "source_type", "archive")
    if not current_command_options.is_category_option("tool", "source_type"):
      current_command_options.set_category_option("tool", "source_type", "remote")

  def create_env_files(self, delete=True, installer=False):

    env_file = self.current_command_options.get_global_option("openturns_env_file")
    env_file_directory = os.path.dirname(env_file)
    if os.path.exists(env_file):
      try:
        if delete:
          os.remove(env_file)
        else:
          return
      except:
        self.logger.exception("Error in deleting OpenTURNS env file: %s" % env_file)

    if not os.path.exists(env_file_directory):
      try:
        os.makedirs(env_file_directory)
      except:
        self.logger.exception("Creation of OpenTURNS env file directory failed: %s" % env_file_directory)

    with open(env_file, 'a') as pre_file:
      if not installer:
        pre_file.write("#!/bin/sh\n")
        pre_file.write("# OpenTURNS env file generated by YAMM\n")
      else:
        pre_file.write(strings.installer_openturns_prerequisite_header)
        has_prereq = False
        has_tools = False
        for software in self.current_command_softwares:
          if software.get_type() == "prerequisite":
            has_prereq = True
          if software.get_type() == "tool":
            has_tools = True
          if has_prereq and has_tools:
            break
        if has_prereq:
          pre_file.write(strings.installer_openturns_prerequisite_header_check_prereq)
        if has_tools:
          pre_file.write(strings.installer_openturns_prerequisite_header_check_tools)

  def get_env_files(self):
    """
    This method returns the list of env files of the project
    """
    env_file = self.current_command_options.get_global_option("openturns_env_file")
    return [env_file]

  def add_software_to_env_files_end_hook(self, executor_software):
    software = self.current_map_name_to_softwares[executor_software.name]
    if software.isArtefact():
      return
    if not self.is_build_only_software(executor_software.name) or \
        not self.current_command_options.get_global_option("separate_dependencies"):
      # Add software in the env file
      with open(self.current_command_options.get_global_option("openturns_env_file"), "a") as env_file:
        env_file.write(software.get_prerequisite_str())

  def add_software_to_env_installer_files_end_hook(self, executor_software):
    software = self.current_map_name_to_softwares[executor_software.name]
    # Reset the software directory install name to the one used in the installer
    software_install_name = software.get_executor_software_name()
    if not software.isArtefact():
      # Add software in the env file
      env_file = open(self.current_command_options.get_global_option("openturns_env_file"), "a")
      software.executor_software_name = software_install_name
      if software.get_type() == "tool":
        env_file.write(software.get_prerequisite_str("${TOOLS_ROOT_DIR}"))
      else:
        env_file.write(software.get_prerequisite_str("${PREREQUISITES_ROOT_DIR}"))
      env_file.close()

  def update_configuration(self):
    # Platform
    misc.misc_platform = self.current_command_options.get_global_option("platform")
    # Default for EDF
    if self.current_command_options.get_category_option("prerequisite", "archive_remote_address") == "":
      self.current_command_options.set_category_option("prerequisite", "archive_remote_address", os.path.join(get_ftp_server(), "sources/prerequis"))
    if self.current_command_options.get_category_option("tool", "archive_remote_address") == "":
      self.current_command_options.set_category_option("tool", "archive_remote_address", os.path.join(get_ftp_server(), "sources/tools"))

  @yamm_command("Creation of OPENTURNS movable installer")
  def create_movable_installer(self, installer_topdirectory="", distrib="",
                               tgz=True, runnable=True, delete=True, universal=False,
                               platform=None):
    """
    This method creates a movable archive of OpenTURNS. By default the archive is made runnable.
    It is possible to generate only the installer directory structure, or the archive tgz file.
    By default the previous directory and archive or installer files are deleted.
    It the distribution is etch, then some specific libraries are added to make the installer "universal".

    :param string installer_topdirectory: Defines the installer directory, including the installer name.
    :param string distrib: Defines the Debian distribution on which the installer is being created.
    :param bool tgz: If True, generates the archive file.
    :param bool runnable: If True, generates the installer file.
    :param bool delete: If True, deletes the existing installer directory, archive and installer files.
    """

    # Step 0: create binary archives
    if self.current_command_options.get_global_option("separate_dependencies"):
      movable_installer_software_list = [soft for soft in self.current_command_softwares if not self.is_build_only_software(soft.name)]
    else:
      movable_installer_software_list = self.current_command_softwares
    ret = self._make(software_list=[s.name for s in movable_installer_software_list],
                     executor_mode="create_binary_archive")
    if not ret:
      return ""

    # Step 1: init variables
    openturns_tag = self.get_version_object().get_tag()
    if runnable:
      tgz = True
    # Get the platform
    if platform is None:
      platform = open("/etc/issue").read()
      platform = platform.split("\\")[0]  # remove the trailing \\n \\l\n\n
      platform = platform.strip()
    platform_no_space = platform.replace(" ", "-")
    # The name of the installer depends on the operating system
    openturns_installer_name = "OpenTURNS-" + openturns_tag + "-" + platform_no_space

    # Calculates the installer directory full path
    movable_installer_topdirectory = os.path.join(self.current_command_options.get_global_option("version_directory"), "movable_installer")
    if installer_topdirectory == "":
      installer_topdirectory = os.path.join(movable_installer_topdirectory, openturns_installer_name)
    else:
      openturns_installer_name = os.path.basename(installer_topdirectory)
    self.logger.info("Installer name is %s.run" % openturns_installer_name)

    # Deletes the previous installer files
    if delete:
      if os.path.exists(installer_topdirectory + ".run"):
        os.remove(installer_topdirectory + ".run")
      if os.path.exists(installer_topdirectory + ".run.sha1"):
        os.remove(installer_topdirectory + ".run.sha1")
      if os.path.exists(installer_topdirectory + ".tgz"):
        os.remove(installer_topdirectory + ".tgz")
      if os.path.exists(os.path.join(movable_installer_topdirectory, "decompress")):
        os.remove(os.path.join(movable_installer_topdirectory, "decompress"))
      if os.path.exists(installer_topdirectory):
        try:
          misc.fast_delete(installer_topdirectory)
        except:
          self.logger.exception("Error in deleting directory" + installer_topdirectory)

    # Step 2: Create env file
    # Defines the new environment file and save the current one
    installer_env_file = os.path.join(installer_topdirectory, "env_openturns_%s.sh" % self.version)
    save_openturns_env_file = ""
    if self.current_command_options.get_global_option("openturns_env_file"):
      save_openturns_env_file = self.current_command_options.get_global_option("openturns_env_file")
    self.current_command_options.set_global_option("openturns_env_file", installer_env_file)

    # Add option for specific env files
    self.create_env_files(installer=True)

    # Step 3: If universal -> add debian for salome
    if universal:
      libs_list = []
      if distrib == "etch":
        libs_list = ["/usr/lib/libg2c.so.0",
                    "/usr/lib/libicudata.so.36",
                    "/usr/lib/libicuuc.so.36",
                    "/lib/libreadline.so.5",
                    "/usr/lib/libgfortran.so.1",
                    "/usr/lib/libicui18n.so.36",
                    "/usr/lib/libpng12.so.0",
                    "/usr/lib/libssl.so.0.9.8",  # Python_266/lib/python2.6/lib-dynload/_hashlib.so
                    "/usr/lib/libcrypto.so.0.9.8",  # Python_266/lib/python2.6/lib-dynload/_hashlib.so
                    "/usr/lib/libpq.so.4",  # Qt_463p1/plugins/sqldrivers/libqsqlpsql.so
                    "/usr/lib/libsqlite.so.0",  # Qt_463p1/plugins/sqldrivers/libqsqlite2.so
                    "/usr/lib/liblualib50.so.5.0",  # Graphviz_2263/lib/graphviz/lua/libgv_lua.so et gv.so
                    "/usr/lib/liblua50.so.5.0",  # Graphviz_2263/lib/graphviz/lua/libgv_lua.so et gv.so
                    ]
      with open(installer_env_file, 'a') as env_file:
        env_file.write("export DEBIANFOROPENTURNS=${PREREQUISITES_ROOT_DIR}/debianForOpenTURNS\n")
        env_file.write("export LD_LIBRARY_PATH=/lib64:${DEBIANFOROPENTURNS}/lib:${LD_LIBRARY_PATH}\n")
      libdirectory = os.path.join(installer_topdirectory, "prerequisites", "debianForOpenTURNS", "lib")
      if not os.path.exists(libdirectory):
        os.makedirs(libdirectory, 0o755)
      for file_to_copy in libs_list:
        if os.path.exists(file_to_copy):
          shutil.copy(file_to_copy, os.path.join(installer_topdirectory, "prerequisites", "debianForOpenTURNS", "lib"))
        else:
          self.logger.error("Cannot find file %s" % file_to_copy)

    # Step 4: prepare post install script
    # This script will be used at installation time to write the good paths
    # into some well defined prerequisites files (ex: pyuic4).
    post_install_file_name = os.path.join(installer_topdirectory, "openturns_post_install.py")
    with open(post_install_file_name, 'a') as post_install_file:
      post_install_file.write("#!/usr/bin/env python\n")
      post_install_file.write("import os\n")
      post_install_file.write("import shutil\n")
      post_install_file.write("import stat\n")
      post_install_file.write("d = {}\n")

    # Step 5: install binary archives in installer directory
    self.reset_command(endlog=False)
    software_mode = "install_from_binary_archive"
    # We set again openturns_env_file because it has been reset by reset_command
    self.current_command_options.set_global_option("openturns_env_file", installer_env_file)

    openturns_doc_install_dir_rel = ""
    post_install_lines = []
    for software in movable_installer_software_list:
      # Artefact softwares are used by yamm only for compilation purposes.
      # They are not real softwares
      if software.isArtefact():
        continue
      software_install_name = software.get_executor_software_name()
#       software_install_name = software.name.capitalize()
#       if software.version:
#         software_install_name += "_" + misc.transform(software.version)
      if software.get_type() == "prerequisite":
        subdir = "prerequisites"
        envvar = "PREREQUISITES_ROOT_DIR"
      if software.get_type() == "tool":
        subdir = "tools"
        envvar = "TOOLS_ROOT_DIR"
      # Sets the install directory for the current software
      installdir = os.path.join(installer_topdirectory, subdir, software_install_name)
      if software.name == "OPENTURNS_DOC":
        openturns_doc_install_dir_rel = os.path.join(subdir, software_install_name)
      self.current_command_options.set_software_option(software.name, "software_install_directory", installdir)
      software.set_command_env(movable_installer_software_list, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
      self.current_map_name_to_softwares[software.name] = software
      # post install lines
      with open(post_install_file_name, 'a') as post_install_file:
        post_install_file.write("d[\"__%s_INSTALL_PATH__\"] = os.path.join(os.getenv(\"%s\"),\"%s\")\n" % (software.name, envvar, software_install_name))
      # If the software defines some movable archive commands with sed commands,
      # The reverse sed commands are written in the post install script.
      # The new sed commands will use install directories given by the env files.
      for command in software.make_movable_archive_commands:
        target_current = ""
        if command.split()[0].startswith("sed"):
          # target is the file to modify
          # it is the last element given by split() (removing any trailing ";"),
          target = command.split()
          if ";" in target:
            target.pop(target.index(";"))
          target = target[-1]
          if target.endswith(";"):
            # if no whitespace before the ";"
            target = target[:-1]
          # there will be 3 files:
          # - the original file: not touched
          # - a .template file: a copy of original file
          # - a .template.current file: the file begin modified, based on the template one
          if not target.endswith(".template"):
            target = target + ".template"
          target_py = "os.path.join(os.getenv(\"%s\"), \"%s\" , \"%s\")" % (envvar, software_install_name, target[:-9])
          target_template_py = "os.path.join(os.getenv(\"%s\"), \"%s\" , \"%s\")" % (envvar, software_install_name, target)
          target_current_py = "os.path.join(os.getenv(\"%s\"), \"%s\" , \"%s.current\")" % (envvar, software_install_name, target)
          target_template = os.path.join("${%s}" % envvar, software_install_name, target)
          target_current = target_template + ".current"
          target = target_template[:-9]
          post_install_lines.append(strings.installer_cp_current_template.substitute(target=target_py,
                                                                             target_template=target_template_py,
                                                                             target_current=target_current_py))

          # __INSTALL_PATH__ replaced the install path of current software at compilation time written in some files
          if "__INSTALL_PATH__" not in command:
            # the command does not depend of current software install path
            # !!! IMPORTANT: the sed commands must use the sed separator defined in misc.py
            args = command.split(misc.sedSep())
            # We search for a sed line which depends on the install directory of another software
            # For example if it depends on Qt, it will be written __QT_INSTALL_PATH__
            # If we find one, the sed line in the post install script is written
            key = None
            key_is_pre = True
            for arg in args:
              if "_INSTALL_PATH__" in arg:
                key = arg
                break
              if "_ROOT_DIR" in arg:
                key = arg
                key_is_pre = False
                break
            if key:
              if key_is_pre:
                post_install_lines.append("os.system(\"sed -i -e 's#%s#\"+d[\"%s\"]+\"#g' %s\")\n" % (key, key, target_current))
              else:
                post_install_lines.append("os.system(\"sed -i -e 's#%s#\"+os.getenv(\"%s\")+\"#g' %s\")\n" % (key, key, target_current))
          else:
            # the current software install path is found
            key = "__%s_INSTALL_PATH__" % software.name
            post_install_lines.append("os.system(\"sed -i -e 's#__INSTALL_PATH__#\"+d[\"%s\"]+\"#g' %s \")\n" % (key, target_current))
        if target_current:
          post_install_lines.append("os.system(\"cp %s %s\")\n" % (target_current, target))
          post_install_lines.append("os.system(\"chmod +x %s\")\n" % target)
    with open(post_install_file_name, 'a') as post_install_file:
      post_install_file.writelines(post_install_lines)

    # The env files are filled
    self.current_command_executor.add_hooks(None, self.add_software_to_env_installer_files_end_hook)

    # The commands in unmake_movable_archive_commands must not be run: disable them
    for soft in self.current_command_executor.executor_softwares:
      if soft.install_binary_archive_task:
        soft.install_binary_archive_task.setRunPostInstallCommands(False)

    self.execute_command()
    os.chmod(post_install_file_name , 0o777)

    # Step 6: add misc files
    # # README
    readme_file_name = os.path.join(os.path.dirname(os.path.abspath(__file__)), "installer_files", "README")
    with open(readme_file_name) as readme_file:
      readme = "".join(readme_file.readlines())
    readme = misc.PercentTemplate(readme)

    contact = "https://forge.pleiade.edf.fr/projects/incertitudes"
    if universal:
      contact = "http://www.openturns.org/"

    readme_file_name = os.path.join(installer_topdirectory, "README")
    with open(readme_file_name, 'w') as readme_file:
      readme_file.write(readme.substitute(tag=openturns_tag,
                                        version=self.version,
                                        gcc_version=misc.get_gcc_version(),
                                        platform=platform,
                                        contact=contact))

    # # python_openturns
    env_filename = os.path.basename(installer_env_file)
    python_openturns_file_name = os.path.join(installer_topdirectory, "python_openturns")
    with open(python_openturns_file_name, 'w') as python_openturns_file:
      python_openturns_file.write(strings.installer_openturns_launch_python_template.substitute(env_script_name=env_filename))
    os.chmod(python_openturns_file_name , stat.S_IRWXU)

    # # eficas_wrapper
    eficas_wrapper_file_name = os.path.join(installer_topdirectory, "eficas_wrapper")
    with open(eficas_wrapper_file_name, 'w') as eficas_wrapper_file:
      eficas_wrapper_file.write(strings.installer_eficas_wrapper_template.substitute(env_script_name=env_filename))
    os.chmod(eficas_wrapper_file_name , stat.S_IRWXU)

    # # eficas_study
    eficas_study_file_name = os.path.join(installer_topdirectory, "eficas_study")
    with open(eficas_study_file_name, 'w') as eficas_study_file:
      eficas_study_file.write(strings.installer_eficas_study_template.substitute(env_script_name=env_filename))
    os.chmod(eficas_study_file_name , stat.S_IRWXU)

    # # link to doc
    curdir = os.getcwd()
    os.chdir(installer_topdirectory)
    os.symlink(os.path.join(openturns_doc_install_dir_rel, "share", "doc", "openturns-doc"), "doc")
    os.chdir(curdir)

    # Step 7: create tgz
    if tgz:
      self.logger.info("Creating TGZ...")
      t_tgz = int(time.time())
      curdir = os.getcwd()

      installer_tgz_name = openturns_installer_name + ".tar.gz"
      installer_tgz_path = os.path.join(os.path.dirname(installer_topdirectory), installer_tgz_name)

      os.chdir(os.path.dirname(installer_topdirectory))
      try:
        tar = tarfile.open(installer_tgz_path, "w:gz")
        tar.errorlevel = 1
        tar.add(os.path.basename(installer_topdirectory))
        tar.close()
      except Exception:
        self.logger.exception("Cannot create archive %s." % installer_tgz_path)
      t_tgz = int(time.time()) - t_tgz
      self.logger.info(" ".join(["TGZ is done in", str(datetime.timedelta(seconds=t_tgz))]))
      if delete:
        misc.fast_delete(installer_topdirectory)

    # Step 8: create run file
      if runnable:
        installer_run_name = openturns_installer_name + ".run"
        usage = strings.installer_decompress_usage.substitute(version=openturns_tag)

        with open(installer_bar_cat) as installer_bar_cat_file:
          bar_cat = "".join(installer_bar_cat_file.readlines())

        # # Calculate the CRC
        crcsum = "0000000000"
        f = os.popen("cat \"%s\" | CMD_ENV=xpg4 cksum |sed -e 's/ /Z/' -e 's/ /Z/' |cut -dZ -f1" % installer_tgz_path)
        crcsum = f.read().strip()
        f.close()

        # # Calculate the MD5SUM
        cmd = """# Try to locate a MD5 binary
        OLD_PATH=$PATH
        PATH=${GUESS_MD5_PATH:-"$OLD_PATH:/bin:/usr/bin:/sbin:/usr/local/ssl/bin:/usr/local/bin:/opt/openssl/bin"}
        MD5_ARG=""
        MD5_PATH=`exec <&- 2>&-; which md5sum || type md5sum`
        test -x $MD5_PATH || MD5_PATH=`exec <&- 2>&-; which md5 || type md5`
        test -x $MD5_PATH || MD5_PATH=`exec <&- 2>&-; which digest || type digest`
        PATH=$OLD_PATH
        if test `basename $MD5_PATH` = digest; then
                MD5_ARG="-a md5"
        fi
        if test -x "$MD5_PATH"; then
                md5sum=`cat "%s" | eval "$MD5_PATH $MD5_ARG" | cut -b-32`;
                echo "$md5sum"
        else
                echo "none"
        fi
        """ % installer_tgz_path
        md5sum = "00000000000000000000000000000000"
        f = os.popen(cmd)
        md5sum = f.read().strip()
        f.close()

        # # create installer decompress file
        installer_decompress_file_name = os.path.join(os.path.dirname(installer_topdirectory), "decompress")
        with open(installer_decompress_file_name, 'w') as installer_decompress_file:
          installer_decompress_file.write(strings.installer_decompress_template.substitute(version=openturns_tag,
                                                                                 bar_cat=bar_cat,
                                                                                 archive_name=openturns_installer_name,
                                                                                 usage=usage,
                                                                                 title_deco="=" * len(openturns_tag),
                                                                                 crcsum=crcsum,
                                                                                 md5sum=md5sum,
                                                                                 env_filename=env_filename))
        os.chmod(installer_decompress_file_name , stat.S_IRWXU)

        self.logger.info("Creating run file...")
        t_run = int(time.time())
        subprocess.call("cat decompress %s > %s" % (installer_tgz_name, installer_run_name), shell=True)
        os.chmod("%s" % (installer_run_name) , stat.S_IRWXU)
    # Step 9: Create sha1sum of run file
        sha1sum_command = "sha1sum " + installer_run_name + " > " + installer_run_name + ".sha1"
        subprocess.call(sha1sum_command, shell=True)
        t_run = int(time.time()) - t_run
        self.logger.info(" ".join(["RUN file is done in", str(datetime.timedelta(seconds=t_run))]))

      os.chdir(curdir)

    # Step 10: restore user env
    if save_openturns_env_file != "":
      self.current_command_options.set_global_option("openturns_env_file", save_openturns_env_file)
    else:
      self.current_command_options.remove_global_option_value("openturns_env_file")

if __name__ == "__main__":

  print("Testing openturns project class")
  pro = Project("OpenTurns Project", VerboseLevels.INFO)
  pro.set_global_option("top_directory", "/tmp/openturns")
  pro.set_software_option("EFICASV1", "occ_username", "anonymous")
  pro.set_version("V1_2_1")
  pro.print_configuration()
  pro.download()
  pro.nothing()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

import os

from yamm.core.base import misc
from yamm.projects.salome.softwares.prerequisites.matplotlib import MATPLOTLIB as SALOME_MATPLOTLIB


software_name = "MATPLOTLIB"
MATPLOTLIB_template = """
#------- Matplotlib ------
export MATPLOTLIB_HOME="%install_dir"
export PYTHONPATH=${MATPLOTLIB_HOME}/lib/python%python_version/site-packages:${PYTHONPATH}
export MPLCONFIGDIR="${HOME}/.config/salome/matplotlib-%mpl_version" # Useful to avoid changing the config files used by the system matplotlib
"""
MATPLOTLIB_template = misc.PercentTemplate(MATPLOTLIB_template)

MATPLOTLIB_configuration_template = """
#------- Matplotlib ------
MATPLOTLIB_HOME="$install_dir"
ADD_TO_PYTHONPATH: %(MATPLOTLIB_HOME)s/lib/python$python_version/site-packages
MPLCONFIGDIR="$${HOME}/.config/salome/matplotlib-$mpl_version" # Useful to avoid changing the config files used by the system matplotlib
"""

class MATPLOTLIB(SALOME_MATPLOTLIB):

  def __init__(self, name, version, verbose, **kwargs):
    SALOME_MATPLOTLIB.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

  def init_patches(self):
    # Instead of patching the configuration file of matplotlib, a environment variable is used.
    self.user_dependency_command += " export MPLSETUPCFG=" + self.patch_directory + "/matplotlib_setup_ot.cfg"

  def init_variables(self):
    if self.version != "1.4.3":
      return SALOME_MATPLOTLIB.init_variables(self)
    #
    self.archive_file_name = "matplotlib-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"

#  Copyright (C) 2012 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.core.framework.version import FrameworkVersion

version_name = "V0_15_1"

class V0_15_1(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):

    # prerequisites
    self.add_software("PYTHON",     "2.6.6")
    self.add_software("SWIG",       "2.0.4")
    self.add_software("CMAKE",      "2.8.5")
    self.add_software("LAPACK",     "3.3.1")
    self.add_software("GRAPHVIZ",   "2.26.3")
    self.add_software("DOXYGEN",    "1.7.3_patch")
    self.add_software("LIBXML2",    "2.7.8")
    self.add_software("TBB",        "3.0")
    self.add_software("QT",         "4.6.3p1")
    self.add_software("SIP",        "4.11.2")
    self.add_software("PYQT",       "4.8.1")
    self.add_software("NUMPY",      "1.4.1")
    self.add_software("R",          "2.7.2")
    self.add_software("RPY2",       "2.0.8")
    self.add_software("ROTRPACKAGE","1.4.4")

    # tools
    self.add_software("PREOPENTURNS_TOOL", "0.15.1")
    self.add_software("OPENTURNS_TOOL",    "0.15.1")
    self.add_software("EFICASV1",          "V6_main")

  def get_tag(self):
    return "0.15.1"

from __future__ import absolute_import
#  Copyright (C) 2012 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from .V0_15_1 import V0_15_1

version_name = "V0_15_1_Calibre7"

class V0_15_1_Calibre7(V0_15_1):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    V0_15_1.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    V0_15_1.configure_softwares(self)

    self.remove_software("PYTHON")
    self.remove_software("SWIG")
    self.remove_software("CMAKE")
    self.remove_software("LAPACK")
    self.remove_software("NUMPY")
    self.remove_software("DOXYGEN")
    self.remove_software("LIBXML2")
    self.remove_software("SIP")
    self.remove_software("PYQT")
    self.remove_software("R")

# -*- coding: utf-8 -*-
#  Copyright (C) 2012, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.core.framework.version import FrameworkVersion

version_name = "V1_3_1"

class V1_3_1(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)
    self.accepted_flavours = ["calibre_7"]

  def get_tag(self):
    return "1.3.1"

  def configure_softwares(self):

    # prerequisites
    self.add_software("PYTHON",     "2.7.3")
    self.add_software("SWIG",       "2.0.8")
    self.add_software("CMAKE",      "2.8.10.2")
    self.add_software("LAPACK",     "3.4.2")
    self.add_software("GRAPHVIZ",   "2.28.0")
    self.add_software("DOXYGEN",    "1.8.3.1")
    self.add_software("LIBXML2",    "2.9.0")
    self.add_software("TBB",        "3.0")
    self.add_software("NUMPY",      "1.7.1")
    self.add_software("SCIPY",      "0.12.0")
    self.add_software("MATPLOTLIB", "1.1.0")
    self.add_software("R",          "2.15.1")
    self.add_software("ROTRPACKAGE","1.4.5")
    self.add_software("TRALICS",    "2.15.0")
    self.add_software("LIBXSLT",    "1.1.28")
    self.add_software("MUPARSER",   "1.32")

    # tools
    self.add_software("PREOPENTURNS_TOOL", "1.3.1")
    self.add_software("OPENTURNS_TOOL",    "1.3.1")
    self.add_software("OPENTURNS_SUBSET",  "1.2d")
    self.add_software("OPENTURNS_ADIRSAM", "1.0d")
    self.add_software("OPENTURNS_DISTFUNC","0.4d")
    self.add_software("OPENTURNS_SVM",     "0.1d")
    self.add_software("OPENTURNS_DOC",     "edf-r180", "13.11.2")
    self.add_software("EFICASV1",          "V7_4_0")

  def update_configuration_with_flavour(self):
    FrameworkVersion.update_configuration_with_flavour(self)

    if self.flavour == "calibre_7":
      self.remove_software("PYTHON")
      self.remove_software("SWIG")
      self.remove_software("CMAKE")
      self.remove_software("LAPACK")
      self.remove_software("GRAPHVIZ")
      self.remove_software("DOXYGEN")
      self.remove_software("LIBXML2")
      self.remove_software("NUMPY")
      self.remove_software("SCIPY")
      self.remove_software("R")
      self.remove_software("LIBXSLT")
      # We use our own version of Matplotlib because Calibre 7 version is too old

# Projet Salome

La branche de construction (dans Yamm) de **Salome-EDF** est la branche **`salome_applications`** pour la **version de développement**.

Lors du passage en **période de Release de Salome** (ouverture de la branche `VX_Y_BR` en Russie), nous **ouvrons une branche `salome/VX_Y_BR`** sur Yamm (nous pouvons fusionner cette branche BR de Yamm dans la branche salome\_applications, mais on évite de faire l'inverse !).

Pour Salome-EDF à base de **V8.2.0**, nous utilisons la **branche `salome/V8_2_BR` de Yamm**.

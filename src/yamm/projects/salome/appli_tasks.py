#  Copyright (C) 2012, 2014, 2015 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from __future__ import with_statement

from builtins import str
from builtins import object

from operator import attrgetter
import os
import subprocess
import tempfile

from yamm.core.base import misc


def create_config_appli(config_appli_filename,
                        prerequisites_file_path,
                        context_file_path,
                        sha1_collection_file_path,
                        env_modules=None,
                        modules_softwares=None,
                        extra_test_softwares=None):
    env_modules = env_modules or []
    # modules_softwares is a dict with:
    # key = framework software
    # value = install_path
    modules_softwares = modules_softwares or {}
    # extra_test_softwares is a dict with:
    # key = framework software
    # value = extra_test_path
    extra_test_softwares = extra_test_softwares or []

    # Sort modules
    sorted_modules_softwares = sorted(modules_softwares, key=attrgetter('module_gui_sort'))
    max_name_size = -1
    for soft in sorted_modules_softwares:
        if len(soft.name) > max_name_size:
            max_name_size = len(soft.name)

    max_test_name_size = -1
    # update formatting with extra tests
    for soft in extra_test_softwares:
        if len(soft.name) > max_test_name_size:
            max_test_name_size = len(soft.name)

    # Create SALOME Application config file
    with open(config_appli_filename, "w") as config_appli_file:
        def write_config(lvl=0, txt='', s='\t', endl='\n'):
            if txt:
                config_appli_file.write(lvl * s + txt + endl)

        # Add prerequisites and context files
        write_config(txt="<application>")
        write_config(lvl=1, txt="<prerequisites path=%r/>" % prerequisites_file_path)
        write_config(lvl=1, txt="<context path=%r/>" % context_file_path)
        write_config(lvl=1, txt="<sha1_collections path=%r/>" % sha1_collection_file_path)

        # Add env modules
        if env_modules:
            write_config(lvl=1, txt="<env_modules>")
            for env_module in env_modules:
                write_config(lvl=2, txt="<env_module name=%r/>" % env_module)
            write_config(lvl=1, txt="</env_modules>")

        # Add modules
        samples_line = ""
        write_config(lvl=1, txt="<modules>")
        for module in sorted_modules_softwares:
            if not module.isArtefact():
                if (module.name == "SAMPLES"):
                    samples_line = "<samples path=%r/>" % modules_softwares[module]
                else:
                    nbspace = max_name_size - len(module.get_name_for_installer()) + 1
                    txt = "<module name=%r%spath=%r/>" % (module.get_module_name_for_appli(),
                                                          nbspace * " ",
                                                          modules_softwares[module])
                    write_config(lvl=2, txt=txt)
        write_config(lvl=1, txt="</modules>")

        # Add SAMPLES
        if samples_line != "":
            write_config(lvl=1, txt=samples_line)

        # Add extra tests
        if extra_test_softwares:
            write_config(lvl=1, txt="<extra_tests>")
            for extra_test, extra_test_path_list in extra_test_softwares.items():
                nbspace = max_test_name_size - len(extra_test.get_name_for_installer()) + 1
                for extra_test_path in extra_test_path_list:
                    write_config(lvl=2, txt="<extra_test name=%r%spath=%r/>" %
                                            (extra_test.get_module_name_for_appli(),
                                             nbspace * " ",
                                             extra_test_path))
                    pass
                pass
            write_config(lvl=1, txt="</extra_tests>")
            pass

        # End of SALOME Application config file
        write_config(lvl=0, txt="</application>")


class CreateAppliTask(object):
    """
      This task is called to create a SALOME application
    """

    def __init__(self, appli_directory, appli_catalogresources=None):
        self.appli_directory = appli_directory
        self.yamm_appli_log_dir = os.path.join(self.appli_directory, '.yamm')
        self.appli_catalogresources = appli_catalogresources

    def prepare_work(self, software_config, space, command_log):
        self.command_log = command_log
        self.space = space
        return ""

    def execute(self, dependency_command, executor, current_software):
        self.command_log.print_begin_work("Create application", space=self.space)

        # Get modules softwares & softwares with extra tests
        modules_softwares = {}
        extra_test_softwares = {}
        env_modules = []

        for software in executor.sorted_executor_softwares:
            framework_software = software.framework_software
            if framework_software.get_type() == "module":
                modules_softwares[framework_software] = framework_software.install_directory
            if framework_software.module_load:
                env_modules += [m for m in framework_software.module_load.split() if m and m not in env_modules]
            extra_test_path_list = framework_software.get_extra_test_path_list()
            if extra_test_path_list:
                extra_test_softwares[framework_software] = [os.path.join(framework_software.install_directory,
                                                                         extra_test_path)
                                                            for extra_test_path in extra_test_path_list]
            if software == current_software:
                break

        # Check if we have a KERNEL
        salome_kernel_software = None
        env_files = []
        for soft in modules_softwares:
            if soft.name == "KERNEL":
                salome_kernel_software = soft
                env_files = soft.env_files
                break
        if not salome_kernel_software:
            return "Cannot create a SALOME application without a KERNEL, please add KERNEL to your configuration"

        # Create appli directory
        if not os.path.isdir(self.yamm_appli_log_dir):
            try:
                os.makedirs(self.yamm_appli_log_dir, 0o755)
            except:
                return "Error in creating application directory: %s" % self.yamm_appli_log_dir

        # Create SALOME Application config file
        (fd, config_appli_filename) = tempfile.mkstemp(suffix=".xml", prefix="config_appli_")
        create_config_appli(config_appli_filename,
                            executor.options.get_global_option("salome_prerequisites_env_file"),
                            executor.options.get_global_option("salome_context_file"),
                            executor.options.get_global_option("salome_sha1_collections_file"),
                            env_modules,
                            modules_softwares,
                            extra_test_softwares)
        os.close(fd)

        # Change permissions, this file should be readable by anyone
        os.chmod(config_appli_filename, 0o644)

        # Create SALOME application
        command = ""
        for env_file in env_files:
            if env_file and os.path.exists(env_file):
                command += ". " + env_file + " ; "
        if os.path.exists(os.path.join(self.appli_directory, "bin", "salome", "appli_clean.sh")):
            command += "cd %s ; " % self.appli_directory
            command += os.path.join(self.appli_directory, "bin", "salome", "appli_clean.sh -f ; ")
            command += "cd %s ; " % os.getcwd()
        command += os.path.join(salome_kernel_software.install_directory, "bin", "salome", "appli_gen.py ")
        command += "--prefix=" + self.appli_directory + " "
        command += "--config=" + config_appli_filename
        if self.appli_catalogresources is not None:
            if not os.path.isfile(self.appli_catalogresources):
                self.command_log.print_warning("The catalog %s is not a file." % self.appli_catalogresources)
            else:
                command += " ; cp " + self.appli_catalogresources + " " + \
                                  os.path.join(self.appli_directory, "CatalogResources.xml")
        command += " > " + os.path.join(self.yamm_appli_log_dir, "appli_gen.log") + " 2>&1 ;"

        # Execute command
        self.command_log.print_debug("Command is: %s" % command)
        ier = os.system(command)
        if ier != 0:
            return "Generation of application failed ! command was: %s" % command
        os.remove(config_appli_filename)
        self.command_log.print_end_work()
        self.command_log.print_info("A SALOME application is generated in directory: %s" % self.appli_directory,
                                    space=self.space + 2)
        self.command_log.print_info("You can run SALOME with this command: %s" %
                                    os.path.join(self.appli_directory, "salome"),
                                    space=self.space + 2)
        return ""


class DeleteAppliTask(object):
    """
      This task is called to delete a SALOME application
    """

    def __init__(self, appli_directory):
        self.appli_directory = appli_directory

    def prepare_work(self, software_config, space, command_log):
        self.command_log = command_log
        self.space = space
        return ""

    def execute(self, dependency_command, executor, current_software):
        self.command_log.print_begin_work("Delete application", space=self.space)

        try:
            misc.fast_delete(self.appli_directory)
        except:
            return "Cannot delete application directory %s" % self.appli_directory

        self.command_log.print_end_work()
        return ""


class RunAppliTestsTask(object):
    """
      This task is called to run salome tests in a SALOME application
    """

    def __init__(self, appli_directory, runner='salome',
                 tests=None, labels=None, options=None):
        self.appli_directory = appli_directory
        self.yamm_appli_log_dir = os.path.join(self.appli_directory, '.yamm')
        self.runner = runner
        self.tests = tests
        self.labels = labels
        self.options = options

    def prepare_work(self, software_config, space, command_log):
        self.command_log = command_log
        self.space = space
        return ""

    def execute(self, dependency_command, executor, current_software):
        self.command_log.print_begin_work("Run application tests", space=self.space)

        if not os.path.exists(os.path.join(self.appli_directory, self.runner)):
            return "Appli runner %s does not exist: impossible to run the tests" % self.runner

        cmd = '(%s' % self.runner
        if self.tests:
            cmd += ' -R %s' % self.tests
        if self.labels:
            cmd += ' -L %s' % self.labels
        if self.options:
            cmd += ' %s' % self.options
        cmd += ") 2>&1 ;"
        log_file = os.path.join(self.yamm_appli_log_dir, "appli_tests.log")
        try:
            ier = subprocess.call(cmd, cwd=self.appli_directory, shell=True,
                                  stdout=log_file, stderr=log_file)
            if ier != 0:
                raise Exception('Bad return code: %d. Commande was %s' % (ier, cmd))
        except Exception as e:
            return "Cannot run application tests: %s" % str(e)

        self.command_log.print_end_work()
        return ""

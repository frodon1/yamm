#!/usr/bin/env python
# -*- coding: utf-8 *-

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from yamm.core.framework.bin.yamm_build_software import FrameworkYammBuildSoftware

class SalomeYammBuildSoftware(FrameworkYammBuildSoftware):
  def __init__(self):
    FrameworkYammBuildSoftware.__init__(self)
    self.possible_project_variable_names += ["salome_project"]
    self.notify_title = "SALOME"

if __name__ == "__main__":
  SalomeYammBuildSoftware().run()

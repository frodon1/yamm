#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from past.builtins import execfile
from optparse import OptionParser, OptionGroup
from yamm.projects.salome.project import Project

parser = OptionParser(usage="usage: %prog [options]")

group = OptionGroup(parser, "Options with config file")
group.add_option("-c",
                 "--config",
                 dest="config_file",
                 help="configuration file (default = %default). "
                 "A configuration file with an object salome_project containing all the informations for using commands in the project",
                 default="config.py")
parser.add_option_group(group)

group = OptionGroup(parser, "Options without config file")
group.add_option("-v",
                 "--version",
                 dest="salome_version",
                 help="SALOME version to build (mandatory if no config file used)",
                 default = "")
group.add_option("-d",
                 "--debug",
                 dest="debug_mode",
                 help="Enable YAMM debug mode",
                 action="store_true",
                 default = False)
group.add_option("-r",
                 "--remote",
                 dest="remote_mode",
                 help="Enable remote mode",
                 action="store_true",
                 default = False)
group.add_option("-u",
                 "--user",
                 dest="user_remote_mode",
                 help="User remote mode",
                 default = "")
parser.add_option_group(group)

(options, args) = parser.parse_args()

if os.path.exists(options.config_file):
  # Case with config file
  config = {}
  execfile(options.config_file, config)
  config["salome_project"].start()
  sys.exit(0)

if options.debug_mode:
  salome_project = Project(1)
else:
  salome_project = Project()

if options.salome_version == "":
  print()
  print("It\'s mandatory to define a SALOME version")
  print("Exit...")
  print()
  sys.exit(1)

salome_project.set_version(options.salome_version)
salome_project.print_configuration()

# Archive mode
if not options.remote_mode:
  salome_project.options.set_category_option("module", "source_type", "archive")
  salome_project.options.set_category_option("tool", "source_type", "archive")
# Remote mode
else:
  if options.user_remote_mode == "":
    print()
    print("It\'s mandatory to define a user for remote mode")
    print("Exit...")
    print()
    sys.exit(1)
  salome_project.options.set_global_option("occ_username", options.user_remote_mode)
salome_project.start()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import zip

from yamm.core.framework.bin.yamm_command import FrameworkYammCommandsLauncher
from yamm.core.framework.bin.yamm_command import FrameworkYammCommand

class SalomeYammCommandsLauncher(FrameworkYammCommandsLauncher):
  def __init__(self):
    FrameworkYammCommandsLauncher.__init__(self)
    self.notify_title = "YAMM for SALOME"
    self.commands["salome_start_and_create_appli"] = FrameworkYammCommand("Start and config local install", self.salome_start_and_create_appli)
    self.commands["salome_start_offline_and_create_appli"] = FrameworkYammCommand("Start offline and config local install", self.salome_start_offline_and_create_appli)
    self.commands["salome_start_from_scratch_and_create_appli"] = FrameworkYammCommand("Start from scratch and config local install", self.salome_start_from_scratch_and_create_appli)
    self.commands["salome_create_appli"] = FrameworkYammCommand("Config local install", self.salome_create_appli)
    self.commands["salome_create_movable_installer"] = FrameworkYammCommand("Create movable installer", self.salome_create_movable_installer)
    self.commands["salome_run"] = FrameworkYammCommand("Run Salome", self.salome_run)
    self.commands["salome_run_with_options"] = FrameworkYammCommand("Run Salome with options", self.salome_run_with_options)

  def salome_start_and_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.start()
    if ret:
      return self.yamm_project.create_appli()
    return ret

  def salome_start_offline_and_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    ret = self.yamm_project.make(executor_mode="build")
    if ret:
      return self.yamm_project.create_appli()
    return ret

  def salome_start_from_scratch_and_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    self.yamm_project.delete_directories(delete_install=True)
    ret = self.yamm_project.start()
    if ret:
      return self.yamm_project.create_appli()
    return ret

  def salome_create_appli(self, args, kw):
    self.yamm_project.print_configuration()
    return self.yamm_project.create_appli()

  def salome_create_movable_installer(self, args, kw):
    self.yamm_project.print_configuration()
    try:
      installer_topdirectory = kw.get('installer_topdirectory', '')
      distrib                = kw.get('distrib', '')
      tgz                    = bool(int(kw.get('tgz', '1')))
      runnable               = bool(int(kw.get('runnable', '1')))
      delete                 = bool(int(kw.get('delete', '1')))
      universal              = bool(int(kw.get('universal', '0')))
      public                 = bool(int(kw.get('public', '0')))
      nosubdir = bool(int(kw.get('nosubdir', '0')))
      ##final_custom_command   = args[7]
      ##additional_commands = []
      ##for cmd in args[8].split('$$'):
        ##if cmd:
          ##additional_commands.append(cmd)
      ##gnu_tar_with_custom_compress_programm = args[9]
      ##return self.yamm_project.create_pack(bool(int(retd)), bool(int(unie)), base_archive_name, bool(int(delete)))
      final_custom_command = kw.get('final_custom_command','')
      additional_commands = kw.get('additional_commands',[])
      software_install_dirs_str = kw.get('software_install_dirs', '')
      softnames = []
      softdirs = []
      for software_install_dirs_val in software_install_dirs_str.split('$$'):
        if software_install_dirs_val:
          softname, softdir = software_install_dirs_val.split('=')
          softnames.append(softname)
          softdirs.append(softdir)
      software_install_dirs = dict(list(zip(softnames,softdirs)))
      gnu_tar_with_custom_compress_programm = kw.get('gnu_tar_with_custom_compress_programm','')
      default_install_dir = kw.get('default_install_dir','')
      default_appli_dir = kw.get('default_appli_dir','')
      default_exec = kw.get('default_exec','')
      default_name = kw.get('default_name','')
      default_icon_path = kw.get('default_icon_path','')
      default_contact = kw.get('default_contact','')
      default_config_file = kw.get('default_config_file','')
      #print "installer_topdirectory: \"{0}\"".format(installer_topdirectory)
      #print "installer_topdirectory: \"{0}\"".format(distrib)
      #print "tgz: {0}".format(tgz)
      #print "runnable: {0}".format(runnable)
      #print "delete: {0}".format(delete)
      #print "universal: {0}".format(universal)
      #print "public: {0}".format(public)
      #print "final_custom_command: \"{0}\"".format(final_custom_command)
      #print "additional_commands: {0}".format(additional_commands)
      #print "gnu_tar_with_custom_compress_programm: \"{0}\"".format(gnu_tar_with_custom_compress_programm)
      return self.yamm_project.create_movable_installer(installer_topdirectory=installer_topdirectory,
                  distrib=distrib,
                  tgz=tgz,
                  runnable=runnable,
                  delete=delete,
                  universal=universal,
                  public=public,
                  final_custom_command=final_custom_command,
                  additional_commands=additional_commands,
                  gnu_tar_with_custom_compress_programm=gnu_tar_with_custom_compress_programm,
                  default_install_dir=default_install_dir,
                  default_appli_dir=default_appli_dir,
                  default_exec=default_exec,
                  default_name=default_name,
                  default_icon_path=default_icon_path,
                  default_contact=default_contact,
                  default_config_file=default_config_file,
                  software_install_dirs=software_install_dirs,
                  nosubdir=nosubdir)
    except Exception as e:
      print(e)
      return 0

  def salome_run(self, args, kw):
    return self.yamm_project.run_appli()

  def salome_run_with_options(self, args, kw):
    return self.yamm_project.run_appli(appli_options=kw.get('appli_options', ''))

if __name__ == "__main__":
  SalomeYammCommandsLauncher().run()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)
from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from yamm.core.framework.bin.yamm_infos import FrameworkYammInfos  # @IgnorePep8
from yamm.projects.salome.project import Project  # @IgnorePep8


class SalomeYammInfos(FrameworkYammInfos):
    def get_project(self):
        return Project()

    def get_name(self):
        return "SALOME"

    def set_parser_args(self):
        FrameworkYammInfos.set_parser_args(self)
        self.subparsers_groups["project"]["project_infos"]\
            .add_argument("--get-linux-packages",
                          action="store_true",
                          dest="print_packages",
                          help="print packages that you need to install to be "
                          "able to build SALOME")
        self.subparsers_groups["project"]["project_infos"]\
            .add_argument("--get-problems",
                          action="store_true",
                          dest="print_problems",
                          help="print informations about some common problems")

    def parse_args(self, args):
        FrameworkYammInfos.parse_args(self, args)
        if vars(args).get('print_packages'):
            print("")
            print("Debian and UBUNTU Linux (Minimal packages list after standard installation):")
            print("g++")
            print("gfortran")
            print("libx11-dev")
            print("automake")
            print("autoconf")
            print("libtool")
            print("libglu1-mesa-dev")
            print("libxmu-headers")
            print("libxt-dev")
            print("libxmu-dev")
            print("flex")
            print("bison")
            print("zlib1g-dev")
            print("libbz2-dev")
            print("libfontconfig1-dev")
            print("libreadline6-dev")
            print("libdbus-1-dev")
            print("libperl-dev")
            print("libpcre3-dev  (for Swig 2.0.3)")
            print("libjpeg62-turbo-dev (for PIL)")
            print("libxi-dev (for gl2ps)")
            print("libssl-dev (for python hashlib module)")
            print("libsqlite3-dev (for python sqlite module)")
            print("libmpfr-dev (for CGAL for PADDER)")
            print("libgmp-dev (for PADDER)")
            print("python-docutils (or PADDER)")
            print("gperf (for Qt5WebKit)")
            print("TeX Live")
            print("texlive-science")
            print("libsnappy-dev")
            print("libsrtp0-dev")
            print("libxcomposite-dev")
            print("libcap-dev")
            print("libegl1-mesa-dev")
            print("libxtst-dev")
            print("libpci-dev")
            print("libnss3-dev")
            print("libasound2-dev")
            print("dvipng")
            print("freeglut3-dev")
            print("To solve the font issue of GEOM and SHAPER:")
            print("xfonts-scalable")
            print("xfonts-75dpi")
            print("")
            print("If you are using GNOME, please also install:")
            print("libgtk2.0-dev")
            print("")
            print("If you have NVIDIA drivers please install after:")
            print("nvidia-current-dev for UBUNTU")
            print("or")
            print("nvidia-glx-dev for DEBIAN")
            print("")
            print("For EDF users please add:")
            print("libpng12-dev")
            print("gnuplot")
            print("autopoint (for modsaturne)")
            print("x11proto-print-dev (for LessTif and Grace)")
            print("")
            print("For cmake compilation (and SMESH):")
            print("f2c")
            print("")
            print("Debian command:")
            print("apt-get install g++ gfortran libx11-dev automake autoconf "
                  "libtool libglu1-mesa-dev libxmu-headers libxt-dev libxmu-dev "
                  "flex bison zlib1g-dev libbz2-dev libfontconfig1-dev "
                  "libreadline6-dev libdbus-1-dev libperl-dev libpcre3-dev "
                  "libjpeg62-dev liblcms1-dev libxi-dev libssl-dev libsqlite3-dev "
                  "libmpfr-dev libgmp-dev python-docutils libgtk2.0-dev "
                  "libpng12-dev gnuplot libssl-dev libsqlite3-dev autopoint "
                  "x11proto-print-dev f2c dvipng")
            print('')
            print('To compile with the maximum of system packages:')
            print('- add the option "use_system_version":')
            print('\t yamm_project.set_global_option("use_system_version", True)')
            print('- install the following packages:')
            print('python-lxml python-pkgconfig python-gnuplot '
                  'libscotch-dev libmetis-dev libxslt1-dev python-scipy '
                  'libmuparser-dev python-h5py cython tralics libtogl-dev '
                  'libopencv-dev libtbb-dev libfreetype6-dev libfreeimage-dev '
                  'python-omniorb python-numpy libxml2 swig libcppunit-dev '
                  'libgl2ps-dev python-sphinx python-pygments python-jinja2 '
                  'doxygen graphviz python-docutils python-setuptools '
                  'libcgns-dev libcgal-dev liblapack-dev libboost-all-dev '
                  'libhdf5-dev libomniorb4-dev tix-dev python tk8.5-dev '
                  'tcl8.5-dev dvipng')

        if vars(args).get('print_problems'):
            print("")
            print("This FAQ try to help you for some common problems found by other users")
            print("")
            print("Firstly, test if all the minimum packages are installed in your computer")
            print("python salome_infos.py project-info --get-linux-packages")
            print("Secondly, these FAQ is done with project with FULL installation of SALOME")
            print("Thirdly, try to compile with parallel flag = 1")
            print("")
            print("")
            print("MEDFICHIER compilation problem:")
            print("A problem occurs when linking with HDF5 like this:")
            print("undefined reference to i_len")
            print("This probably means the C compiler of HDF is not compatible with the Fortran compiler found by MEDFICHIER by default")
            print("Change f77 default compiler by a compatible one")
            print("")
            print("")
            print("PYTHON does not compile:")
            print("You have an error like this:")
            print("update Py_single_input in Python.h")
            print("This probably means that a parallel compilation of Python failed, try with an another parallel flag")
            print("")
            print("")
            print("PARAVIEW compilation problem:")
            print("A problem occurs when linking with gl2ps")
            print("Check that the library libgl2ps.so is well installed. If it is not the case, check that the library /usr/lib/libglx.so is present.")
            print("If not, create the link: ln -s /usr/lib/libglx.so.1 /usr/lib/libglx.so")
            print("Reinstall gl2ps and Paraview")
            print("")


if __name__ == "__main__":
    SalomeYammInfos().run()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str
from PyQt4 import QtGui

from yamm.core.framework.gui.basic_wizard import FrameworkBasicWizard
from yamm.core.framework.gui.basic_wizard import FrameworkBasicIntro
from yamm.core.framework.gui.basic_wizard import FrameworkBasicOptions
from yamm.core.framework.gui.basic_wizard import FrameworkBasicRemotes
from yamm.core.framework.gui.select_software_list import ManageSoftwareList

from yamm.projects.salome.project import Project

class BasicWizard(FrameworkBasicWizard):

  def __init__(self, parent, title="Create a SALOME project", project=Project()):
    FrameworkBasicWizard.__init__(self, parent, title, project)
    self.setWindowTitle("Create a SALOME project")

  def get_yamm_project(self):
    return Project()

  def end(self, result):
    FrameworkBasicWizard.end(self, result)
    if self.result:
      self.salome_create_appli = self.field("salome_create_appli").toBool()
      self.occ_server   = str(self.field("occ_server").toString())
      self.occ_username = str(self.field("occ_username").toString())
      self.use_pleiade_mirrors = self.field("use_pleiade_mirrors").toBool()
      pass

  def add_pages(self):
    self.addPage(BasicIntro(self, self.get_yamm_project()))
    self.addPage(BasicOptions(self, self.get_yamm_project()))
    self.addPage(BasicRemotes(self, self.get_yamm_project()))
    self.addPage(ManageSoftwareList(self, self.get_yamm_project()))

class BasicIntro(FrameworkBasicIntro):
  def __init__(self, parent, project):
    FrameworkBasicIntro.__init__(self, parent, project)

class BasicOptions(FrameworkBasicOptions):
  def __init__(self, parent, project):
    FrameworkBasicOptions.__init__(self, parent, project)
    self.current_topDir = os.path.join(self.home_dir, "salome")
    self.setDefaultValues()

  def setWidgets(self):
    FrameworkBasicOptions.setWidgets(self)
    self.opt_salome_create_appli_button = QtGui.QPushButton("OFF")
    self.opt_salome_create_appli_button.setCheckable(True)
    self.widgets_in_options_layout.insert(1, ["Create appli after compilation:", self.opt_salome_create_appli_button])
    # Signal and Slots
    self.opt_salome_create_appli_button.toggled.connect(self.toggled_button)

  def setPageLayout(self):
    FrameworkBasicOptions.setPageLayout(self)

  def registerFields(self):
    FrameworkBasicOptions.registerFields(self)
    self.registerField("salome_create_appli", self.opt_salome_create_appli_button)

  def setDefaultValues(self, project_name="salome"):
    self.current_topDir = os.path.join(self.home_dir, project_name)
    FrameworkBasicOptions.setDefaultValues(self)

class BasicRemotes(FrameworkBasicRemotes):
  def __init__(self, parent, project):
    FrameworkBasicRemotes.__init__(self, parent, project)

  def setWidgets(self):
    FrameworkBasicRemotes.setWidgets(self)

    # Remote configuration
    rem_groupBox = QtGui.QGroupBox("Remote configuration")
    rem_layout = QtGui.QGridLayout()
    rem_ser_label = QtGui.QLabel("OCC Server:")
    self.rem_ser_lineEdit = QtGui.QLineEdit()
    rem_user_label = QtGui.QLabel("OCC User:")
    self.rem_user_lineEdit = QtGui.QLineEdit()
    # PLEIADE mirrors
    rem_pleiade_label = QtGui.QLabel(self.tr("Use PLEIADE mirrors:"))
    self.opt_use_pleiade_mirrors_button = QtGui.QPushButton("OFF")
    self.opt_use_pleiade_mirrors_button.setCheckable(True)
    self.opt_use_pleiade_mirrors_button.toggled.connect(self.toggled_button_on_off)

    rem_layout.addWidget(rem_ser_label, 0, 0)
    rem_layout.addWidget(self.rem_ser_lineEdit, 0, 1)
    rem_layout.addWidget(rem_user_label, 1, 0)
    rem_layout.addWidget(self.rem_user_lineEdit, 1, 1)
    rem_layout.addWidget(rem_pleiade_label, 2, 0)
    rem_layout.addWidget(self.opt_use_pleiade_mirrors_button, 2, 1)
    rem_groupBox.setLayout(rem_layout)
    self.widgets_in_layout.append(rem_groupBox)

  def registerFields(self):
    FrameworkBasicRemotes.registerFields(self)
    # Fields
    self.registerField("occ_server",         self.rem_ser_lineEdit)
    self.registerField("occ_username",       self.rem_user_lineEdit)
    self.registerField("use_pleiade_mirrors", self.opt_use_pleiade_mirrors_button)

  def setDefaultValues(self):
    FrameworkBasicRemotes.setDefaultValues(self)
    # Default Values
    self.rem_ser_lineEdit.setText("git.salome-platform.org")
    self.rem_user_lineEdit.setText("")
    self.categoriesButtons["tool"].setChecked(True)
    self.categoriesButtons["module"].setChecked(True)
    self.opt_use_pleiade_mirrors_button.setChecked(False)
    self.set_enabled_remotes()

  def toggled_button_remote_mode(self, status):
    FrameworkBasicRemotes.toggled_button_remote_mode(self, status)
    self.set_enabled_remotes()

  def toggled_button_on_off(self, status):
    FrameworkBasicRemotes.toggled_button_on_off(self, status)
    self.set_enabled_remotes()

  def set_enabled_remotes(self):
    one_category_is_archive = any([button.isChecked() for button in list(self.categoriesButtons.values())])
    use_pleiade_mirror = self.opt_use_pleiade_mirrors_button.isChecked()
    self.rem_ser_lineEdit.setDisabled(use_pleiade_mirror or not one_category_is_archive)
    self.rem_user_lineEdit.setDisabled(use_pleiade_mirror or not one_category_is_archive)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from past.builtins import execfile
from yamm.core.base import misc
from yamm.core.framework.gui.gui_project import FrameworkYammGuiProject

gui_file = """
salome_create_appli = %salome_create_appli
"""
gui_file = misc.PercentTemplate(gui_file)

class YammGuiProject(FrameworkYammGuiProject):

  def __init__(self, project_class="salome"):
    FrameworkYammGuiProject. __init__(self, project_class)
    # Create appli
    self.salome_create_appli = True

  def clear(self):
    FrameworkYammGuiProject.clear(self)
    self.salome_create_appli = True

  def create_gui_project_file(self):
    FrameworkYammGuiProject.create_gui_project_file(self)
    filetext = gui_file.substitute(salome_create_appli=self.salome_create_appli)
    with open(self.gui_filename, 'a') as gui_project_file:
      gui_project_file.write(filetext)

  def load_project(self, filename):
    FrameworkYammGuiProject.load_project(self, filename)
    exec_dict = {}
    execfile(filename, exec_dict)
    self.salome_create_appli = exec_dict.get("salome_create_appli", True)
    if "salome_version" in list(exec_dict.keys()):
      self.version = exec_dict["salome_version"]

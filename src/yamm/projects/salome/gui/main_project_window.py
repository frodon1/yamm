#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from past.builtins import execfile
from builtins import str
from builtins import range
from PyQt4 import QtGui, uic

# For syntax highlighting
from yamm.core.framework.gui import highlighter
from yamm.core.framework.gui.main_project_window import FrameworkProjectWindow

from yamm.projects.salome.project import Project
from yamm.projects.salome.gui     import edit_project_window

class ProjectWindow(FrameworkProjectWindow):

  def __init__(self, parent, gui_project, yamm_gui):
    FrameworkProjectWindow.__init__(self, parent, gui_project, yamm_gui)
    self.salome_bin_directory  = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../bin")

  def init_widgets(self):
    FrameworkProjectWindow.init_widgets(self)

    self.salome_create_appli_checkBox = QtGui.QCheckBox("Create appli")
    self.salome_create_appli_button = QtGui.QPushButton("Create appli")
    self.salome_create_installer_button = QtGui.QPushButton("Create Installer")

    self.actions_groupBox.layout().insertWidget(3,self.salome_create_appli_checkBox)
    self.actions_groupBox.layout().addWidget(self.salome_create_appli_button)
    self.actions_groupBox.layout().addWidget(self.salome_create_installer_button)

    self.salome_create_appli_checkBox.clicked.connect(self.salome_update_create_appli)
    self.salome_create_appli_button.clicked.connect(self.salome_create_appli_command)
    self.salome_create_installer_button.clicked.connect(self.salome_create_installer_command)

    self.salome_create_appli_checkBox.setChecked(self.gui_project.salome_create_appli)

    self.salome_run_button = QtGui.QPushButton("Run SALOME")
    self.salome_run_with_options_button = QtGui.QPushButton("Run SALOME with options")
    self.specific_groupBox_layout = QtGui.QVBoxLayout()
    self.specific_groupBox_layout.addWidget(self.salome_run_button)
    self.specific_groupBox_layout.addWidget(self.salome_run_with_options_button)
    self.specific_groupBox.setLayout(self.specific_groupBox_layout)
    self.specific_groupBox.setTitle("Launch")
    self.specific_groupBox.setVisible(True)
    self.line_specific_actions.setVisible(True)

    self.salome_run_button.clicked.connect(self.run_salome)
    self.salome_run_with_options_button.clicked.connect(self.run_salome_options)
    
    self.w = CreateMovableInstallerDialog(self, self.gui_project)

  def get_command_script(self):
    return os.path.join(self.salome_bin_directory, "salome_command.py")

  def get_yamm_project_edit_window(self):
    return edit_project_window.EditProjectWindow(self, self.yamm_gui, "edit", self.gui_project)

  def get_empty_yamm_project(self):
    return Project()
  
  ## BEGIN of SALOME commands ##

  def start_command(self):
    if self.salome_create_appli_checkBox.isChecked():
      FrameworkProjectWindow.start_command(self, "salome_start_and_create_appli")
    else:
      FrameworkProjectWindow.start_command(self)

  def start_offline_command(self):
    if self.salome_create_appli_checkBox.isChecked():
      FrameworkProjectWindow.start_offline_command(self, "salome_start_offline_and_create_appli")
    else:
      FrameworkProjectWindow.start_offline_command(self)

  def start_from_scratch_command(self):
    if self.salome_create_appli_checkBox.isChecked():
      FrameworkProjectWindow.start_from_scratch_command(self, "salome_start_from_scratch_and_create_appli")
    else:
      FrameworkProjectWindow.start_from_scratch_command(self)

  def salome_update_create_appli(self):
    self.gui_project.salome_create_appli = self.salome_create_appli_checkBox.isChecked()
    self.gui_project.create_gui_project_file()

  def run_salome(self):
    command = self.prepare_command("salome_run")
    self.exec_command(command)

  def run_salome_options(self):
    options, ret = QtGui.QInputDialog.getText(self,
                                              "Run Salome with options",
                                              "Options")
    if ret:
      command = self.prepare_command("salome_run_with_options")
      self.exec_command(command, [], {'appli_options':str(options)})

  def salome_create_appli_command(self):
    command = self.prepare_command("salome_create_appli")
    self.exec_command(command)

  def salome_create_installer_command(self):    
    if self.w.exec_():
      kw = {}
      kw['installer_topdirectory'] = str(self.w.top_directory.text())
      kw['distrib'] = str(self.w.distrib.text())
      kw['tgz'] = 1 if self.w.tgz.isChecked() else 0
      kw['runnable'] = 1 if self.w.runnable.isChecked() else 0
      kw['delete'] = 1 if self.w.delete_installer.isChecked() else 0
      kw['universal'] = 1 if self.w.universal.isChecked() else 0
      kw['public'] = 1 if self.w.public_version.isChecked() else 0
      kw['nosubdir'] = 1 if self.w.nosubdir.isChecked() else 0
      software_install_dir_list = []
      for row in range(self.w.customDirectories.rowCount()):
        softname = str(self.w.customDirectories.indexWidget(self.w.customDirectories.model().index(row, 0)).currentText())
        softdir = str(self.w.customDirectories.item(row, 1).text())
        software_install_dir_list.append("{0}={1}".format(softname, softdir))
      kw['software_install_dirs'] = "$$".join(software_install_dir_list)
        
#      kw['final_custom_command'] = str(w.final_custom_command.toPlainText())
#      #args.append("\\\"\\\"\\\"{0}\\\"\\\"\\\"".format(w.final_custom_command.toPlainText()))
#      additional_commands = []
#      for row in range(w.additional_commands.count()):
#        cmd = str(w.additional_commands.item(row).text()).replace('"',"'")
#        print "cmd: \'{0}\'".format(cmd)
#        additional_commands.append(cmd)
#      print "additional_commands: {0}".format(additional_commands)
#      #args.append("§§".join(additional_commands))
#      kw['additional_commands'] = "§§".join(additional_commands)
      kw['gnu_tar_with_custom_compress_programm'] = str(self.w.gnu_tar_with_custom_compress_programm.text())
      kw['default_install_dir'] = str(self.w.default_install_dir.text())
      kw['default_appli_dir'] = str(self.w.default_appli_dir.text())
      kw['default_exec'] = str(self.w.default_exec.text())
      kw['default_name'] = str(self.w.default_name.text())
      kw['default_icon_path'] = str(self.w.default_icon_path.text())
      kw['default_contact'] = str(self.w.default_contact.text())
      kw['default_config_file'] = str(self.w.default_config_file.text())
      command = self.prepare_command("salome_create_movable_installer")
      #print "args: {0}".format(args)
      self.exec_command(command, [], kw)

class CreateMovableInstallerDialog(QtGui.QDialog):
  def __init__(self, parent, gui_project):
    QtGui.QDialog.__init__(self, parent)
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "create_movable_installer.ui"), self)
    highlighter.Highlighter(self.final_custom_command.document(),"bash")
    self.toggleAdvancedWidgetVisibility(False)
#    self.customDirectories.cellChanged.connect(self.resizeCustomDirectories)
    self.tabWidget.removeTab(self.tabWidget.indexOf(self.tab_add_build_commands))
    self.tabWidget.removeTab(self.tabWidget.indexOf(self.tab_final_exec_command))
    self.softwaresList = []
    gui_project.create_yamm_config_project_file()
    exec_dict = {}
    execfile(gui_project.config_filename, exec_dict)
    yamm_project = exec_dict.get('yamm_project', None)
    if yamm_project:
      yamm_project.begin_command()
      self.softwaresList = [soft.name for soft in yamm_project.current_command_softwares]
      yamm_project.end_command()
#    self.tab_add_build_commands.setVisible(False)
#    self.tab_final_exec_command.setVisible(False)
#    self.pushButton.setVisible(False)
#    self.advanced_widget.setVisible(False)
    self.customDirectories.resizeColumnsToContents()

  def resizeCustomDirectories(self):
    self.customDirectories.resizeColumnsToContents()
    self.customDirectories.horizontalHeader().setStretchLastSection(True)

  def toggleAdvancedWidgetVisibility(self, visibility=None):
    widget_visibility = not self.advancedWidget.isVisible() if visibility is None else visibility
    self.advancedWidget.setVisible(widget_visibility)
    self.adjustSize()

  def editAdditionalCommand(self, index):
    command = str(self.additional_commands.currentItem().text())
    self.addAdditionalCommand(command, True)

  def addAdditionalCommand(self, command=None, edit=False):
    w = SalomeAddAdditionalCommand(self, command, edit)
    if w.exec_() and not w.edit:
      self.additional_commands.insertItem(self.additional_commands.count(), w.plainTextEdit.toPlainText())
  
  def addCustomDir(self):
    nbRows = self.customDirectories.rowCount()
    self.customDirectories.setRowCount(nbRows+1)
    combo = QtGui.QComboBox()
    combo.currentIndexChanged.connect(self.customDirectories.selectRow)
    combo.currentIndexChanged.connect(self.resizeCustomDirectories)
    combo.addItems(self.softwaresList)
    self.customDirectories.setCellWidget(nbRows, 0, combo)
    self.customDirectories.setItem(nbRows, 1, QtGui.QTableWidgetItem("INSTALLDIR"))
    self.resizeCustomDirectories()
#    self.tabWidget.adjustSize()
#    self.adjustSize()
    pass
  
  def removeCustomDir(self):
    selectedItems = self.customDirectories.selectedItems()
    for item in selectedItems:
      self.customDirectories.removeRow(item.row())
    self.resizeCustomDirectories()
#    self.tabWidget.adjustSize()
#    self.adjustSize()
    pass

  def removeAdditionalCommand(self):
    item = self.additional_commands.takeItem(self.additional_commands.currentRow())
    if item:
      del item

class SalomeAddAdditionalCommand(QtGui.QDialog):
  def __init__(self, parent, command=None, edit=False):
    QtGui.QDialog.__init__(self, parent)
    self.edit = edit
    uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)), "add_additional_command.ui"), self)
    highlighter.Highlighter(self.plainTextEdit.document(),"bash")
    if command is not None:
      self.plainTextEdit.setPlainText(command)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015, 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from __future__ import with_statement

from operator import attrgetter
import glob
import os
import shutil
import subprocess
import tempfile

from yamm.core.base import misc
from yamm.core.framework.installer import FrameworkInstaller
from yamm.projects.salome.installer.strings_template import SalomeInstallerStringsTemplate
from yamm.projects.salome.appli_tasks import create_config_appli


libs_debianForSalome = {
  "jessie" : ["/usr/lib/x86_64-linux-gnu/libicui18n.so.52",
              "/usr/lib/x86_64-linux-gnu/libicui18n.so.52.1",

              "/usr/lib/x86_64-linux-gnu/libicuuc.so.52",
              "/usr/lib/x86_64-linux-gnu/libicuuc.so.52.1",

              "/usr/lib/x86_64-linux-gnu/libicudata.so.52",
              "/usr/lib/x86_64-linux-gnu/libicudata.so.52.1",

              "/usr/lib/x86_64-linux-gnu/libgfortran.so.3",
              "/usr/lib/x86_64-linux-gnu/libgfortran.so.3.0.0",

              "/usr/lib/x86_64-linux-gnu/libcrypto.so.1.0.0", # a partir d'ici pour Debian 9
              "/usr/lib/x86_64-linux-gnu/libssl.so.1.0.0",

              "/usr/lib/x86_64-linux-gnu/libjpeg.so.62",
              "/usr/lib/x86_64-linux-gnu/libjpeg.so.62.1.0",

              "/usr/lib/x86_64-linux-gnu/libmysqlclient.so.18",
              "/usr/lib/x86_64-linux-gnu/libmysqlclient.so.18.0.0",

              "/usr/lib/x86_64-linux-gnu/libmng.so.1",
              "/usr/lib/x86_64-linux-gnu/libmng.so.1.1.0.10",

              "/usr/lib/x86_64-linux-gnu/libpq.so.5",
              "/usr/lib/x86_64-linux-gnu/libpq.so.5.7",

              "/lib/x86_64-linux-gnu/libbz2.so.1",
              "/lib/x86_64-linux-gnu/libbz2.so.1.0",
              "/lib/x86_64-linux-gnu/libbz2.so.1.0.4",

              "/usr/lib/x86_64-linux-gnu/libpng12.so.0",
              "/usr/lib/x86_64-linux-gnu/libnvidia-glsi.so.340.102",
              "/usr/lib/x86_64-linux-gnu/libproxy.so.1",

              "/usr/lib/x86_64-linux-gnu/libjasper.so.1"
              "/usr/lib/x86_64-linux-gnu/libjasper.so.1.0.0"
  ],
  "squeeze":["/usr/lib/libpng12.so.0",
             "/usr/lib/libssl.so.0.9.8",  # Python_<xyz>/lib/python<x.y>/lib-dynload/_hashlib.so
             "/usr/lib/libcrypto.so.0.9.8",  # Python_<xyz>/lib/python<x.y>/lib-dynload/_hashlib.so
             "/lib/libbz2.so.1.0",
             "/usr/lib/libgl2ps.so.0",
             "/usr/lib/libgfortran.so.3",
             "/usr/lib/libicudata.so.44",
             "/usr/lib/libicui18n.so.44",
             "/usr/lib/libicuuc.so.44",
             "/usr/lib/libz.so.1",
             "/usr/lib/libtiff.so.4",
             "/usr/lib/libjpeg.so.62",
             "/usr/lib/libproxy.so.0",
             "/usr/lib/libgcrypt.so.11"  #Needed for Salome_CFD Catalyst feature
             ],
  "squeeze_gcc484":["/usr/lib/libpng12.so.0",
             "/usr/lib/libssl.so.0.9.8",  # Python_<xyz>/lib/python<x.y>/lib-dynload/_hashlib.so
             "/usr/lib/libcrypto.so.0.9.8",  # Python_<xyz>/lib/python<x.y>/lib-dynload/_hashlib.so
             "/lib/libbz2.so.1.0",
             "/usr/lib/libicudata.so.44",
             "/usr/lib/libicui18n.so.44",
             "/usr/lib/libicuuc.so.44",
             "/usr/lib/libz.so.1",
             "/usr/lib/libtiff.so.4",
             "/usr/lib/libjpeg.so.62",
             "/usr/lib/libpq.so.5",  # qgis
             "/usr/lib/libnetcdf.so.6",  # CloudCompare, qgis
             "/usr/lib/libhdf5_hl.so.6",  # libnetcdf.so.6
             "/usr/lib/libhdf5.so.6"],  # libnetcdf.so.6
  "wheezy":[],
}

class SalomeInstaller(FrameworkInstaller):
  def __init__(self, name, logger, topdirectory, softwares, executor, delete=True, runnable=True,
               distrib="", tgz=True, universal=False, public=False,
               final_custom_command="", additional_commands=[], gnu_tar_with_custom_compress_programm="",
               default_install_dir="", default_appli_dir="", default_exec="",
               default_name="", default_icon_path="", default_contact="", default_config_file="",
               software_install_dirs=None, default_temporary_path="", stringsTemplate=None,
               nosubdir=False, salome_version=None):

    FrameworkInstaller.__init__(self, name, logger, topdirectory, softwares,
                                executor, delete, runnable, nosubdir=nosubdir)

    self.init_installer(distrib, tgz, universal, public, final_custom_command, additional_commands,
                        gnu_tar_with_custom_compress_programm, default_install_dir, default_appli_dir, default_exec,
                        default_name, default_icon_path, default_contact, default_config_file, software_install_dirs,
                        default_temporary_path, stringsTemplate, salome_version)

  def init_installer(self, distrib="", tgz=True, universal=False, public=False,
                     final_custom_command="", additional_commands=[], gnu_tar_with_custom_compress_programm="",
                     default_install_dir="", default_appli_dir="", default_exec="",
                     default_name="", default_icon_path="", default_contact="", default_config_file="",
                     software_install_dirs=None, default_temporary_path="", stringsTemplate=None, salome_version=None):
    self.distrib = distrib
    self.tgz = self.runnable or tgz
    self.logger.info("Installer %s initialisation starts" % self.name)

    self.stringsTemplate = stringsTemplate or SalomeInstallerStringsTemplate()
    self.salome_version = self.executor.version_object.get_salome_version()
    self.salome_flavour = self.executor.version_object.flavour
    self.universal = self.executor.version_object.is_universal() or universal
    self.isPublic = self.executor.version_object.is_public() or public
    # The public versions are always distributed as universal packages
    self.universal = self.isPublic or self.executor.version_object.is_universal() or universal

    self.final_custom_command = final_custom_command
    self.additional_commands = additional_commands[:]
    self.gnu_tar_with_custom_compress_programm = gnu_tar_with_custom_compress_programm

    self.software_install_dirs = software_install_dirs or {}

    self.default_install_dir = default_install_dir or "$HOME/salome"
    self.default_appli_dir = default_appli_dir or os.path.join(self.default_install_dir, "appli_" + self.salome_version)
    self.default_exec = default_exec or "salome"
    self.default_name = default_name or "Salome"
    self.default_desktop_filename = "{0}_{1}.desktop".format(self.default_name, self.salome_version)
    # Path to Salome icon if GUI module is present
    self.guiIconPath = default_icon_path
    self.guiIconPathStr = default_icon_path
    if self.guiIconPath == "" and 'GUI' in self.map_name_to_softwares:
        software = self.map_name_to_softwares['GUI']
        software_install_name = self.get_software_install_path(software)
        subdir = self.__get_subdir_from_software_type(software)
        installdir = os.path.join(self.topdirectory, subdir, software_install_name)
        self.guiIconPath = os.path.join(installdir, "share", "salome", "resources", "gui", "icon_about.png")
        self.guiIconPathStr = os.path.join('"${GUI_ROOT_DIR}"',
                                           "share", "salome", "resources",
                                           "gui", "icon_about.png")
    contact = "https://salome.der.edf.fr"
    if self.universal:
        contact = "http://salome-platform.org"
    self.default_contact = default_contact or contact

    config_file = "SalomeApprc." + self.executor.version_object.get_salome_dot_version()
    self.default_config_file = default_config_file or config_file
    self.default_temporary_path = default_temporary_path

    self.prerequisites_file = os.path.join(self.topdirectory, "salome_prerequisites.sh")
    self.context_file = os.path.join(self.topdirectory, "salome_context.cfg")
    self.modules_file = os.path.join(self.topdirectory, "salome_modules.sh")
    self.post_install_file_name = os.path.join(self.topdirectory, "salome_post_install.py")
    self.hpc_visu_servers_file_name = os.path.join(self.topdirectory, "salome_hpc_visu_servers.py")
    self.servers_pvsc = os.path.join(self.topdirectory, "servers.pvsc")
    self.config_appli_filename = os.path.join(self.topdirectory, ".config_appli_template.xml")

    self.salome_hpc_visu_servers = ""
    self.catalog_resources = ""

    # Check if we have a KERNEL
    self.has_salome_kernel_software = 'KERNEL' in self.map_name_to_softwares

    self.logger.info("Installer %s initialisation ends" % self.name)
    pass

  def begin(self):
    self.logger.info("Installer %s begin step starts" % self.name)
    # Step 1: Create env files

    if self.universal:
      libs_list = []
      if self.distrib.startswith("etch"):
        libs_list = libs_debianForSalome["etch_all"]

      if self.distrib in libs_debianForSalome:
        libs_list += libs_debianForSalome[self.distrib]

      # Should not occur: env files must be created by project before call to begin()
      if not os.path.exists(self.prerequisites_file):
        self.logger.error("Cannot find prerequisite file %s" % self.prerequisites_file)
      if not os.path.exists(self.context_file):
        self.logger.error("Cannot find context file %s" % self.contex_file)

      with open(self.prerequisites_file, 'a') as pre_file:
        pre_file.write("export DEBIANFORSALOME=${PREREQUISITES_ROOT_DIR}/debianForSalome\n")
        pre_file.write("export LD_LIBRARY_PATH=${DEBIANFORSALOME}/lib:/lib64:${LD_LIBRARY_PATH}\n")
        if "cfd" in self.salome_flavour:
          pre_file.write("export LIBRARY_PATH=${DEBIANFORSALOME}/lib:${LIBRARY_PATH}\n")
          pre_file.write("export LIBRARY_PATH=/usr/lib/x86_64-linux-gnu:${LIBRARY_PATH}\n")

      # /lib64 must not be added to the beginning of LD_LIBRARY_PATH  with ctx_file
      # with open(self.context_file, 'a') as ctx_file:
      #  ctx_file.write("DEBIANFORSALOME=%(PREREQUISITES_ROOT_DIR)s/debianForSalome\n")
      #  ctx_file.write("ADD_TO_LD_LIBRARY_PATH: %(DEBIANFORSALOME)s/lib:/lib64\n")

      subdir = "" if self.nosubdir else "prerequisites"
      libdirectory = os.path.join(self.topdirectory, subdir, "debianForSalome", "lib")
      if not os.path.exists(libdirectory):
          os.makedirs(libdirectory, 0o755)
      for file_to_copy in libs_list:
        if os.path.exists(file_to_copy):
          shutil.copy(file_to_copy, libdirectory)
          symbolic_link = os.path.join(libdirectory, file_to_copy.split("/")[-1].split("so")[0] + "so")
          if os.path.exists(symbolic_link):
            os.remove(symbolic_link)
          owd = os.getcwd()
          try:
            os.chdir(libdirectory)
            os.symlink(file_to_copy.split("/")[-1], symbolic_link)
          finally:
            os.chdir(owd)
        else:
          self.logger.warning("Cannot find file %s. File is skipped" % file_to_copy)

    # Step 2 create some directories
    self.create_additionnal_directories()

    self.writePostInstallFile()

    self.logger.info("Installer %s begin step ends" % self.name)
    return True

  def create_additionnal_directories(self):
    # Creates the Salome Python plugins directory
    if not os.path.exists(os.path.join(self.topdirectory, "plugins")):
      os.makedirs(os.path.join(self.topdirectory, "plugins"), 0o755)

    # Copy plugin files
    for f in glob.glob(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                    os.pardir, "installer_files", "plugins",
                                    r'*_plugins.py')):
      shutil.copy(f, os.path.join(self.topdirectory, "plugins"))
    # Creates the Salome preferences directory
    if not os.path.exists(os.path.join(self.topdirectory, ".config")):
      os.makedirs(os.path.join(self.topdirectory, ".config"), 0o755)


  def get_software_install_path(self, software):
    if software.name in self.software_install_dirs:
      software_install_name = self.software_install_dirs[software.name]
    else:
      software_install_name = software.get_executor_software_name()
      if software.get_type() == "module":
        software_install_name = software.name + "_" + misc.transform(software.version)
    return software_install_name

  def get_post_install_dict_substitute_string(self, software):
      substitute_string = ''
      installdir = ''
      software_install_name = self.get_software_install_path(software)
      if software.name in self.software_install_dirs:
          substitute_string = "os.path.join(os.getenv('ROOT_SALOME'), '%s')" % (software_install_name)
          installdir = software_install_name
      else:
          if software.get_type() == "module":
              substitute_string = "os.getenv('%s_ROOT_DIR')" % (software.get_name_for_installer())
              installdir = "${%s_ROOT_DIR}" % software.get_name_for_installer()
          else:
              if software.get_type() == "prerequisite":
                  substitute_string = "os.path.join(os.getenv('PREREQUISITES_ROOT_DIR'), '%s')" % (software_install_name)
                  installdir = os.path.join("${PREREQUISITES_ROOT_DIR}", software_install_name)
              elif software.get_type() == "tool":
                  substitute_string = "os.path.join(os.getenv('TOOLS_ROOT_DIR'), '%s')" % (software_install_name)
                  installdir = os.path.join("${TOOLS_ROOT_DIR}", software_install_name)
      return substitute_string, installdir

  def __get_subdir_from_software_type(self, software):
    if self.nosubdir or software.name in self.software_install_dirs:
      subdir = ""
    else:
      if software.get_type() == "prerequisite":
        subdir = "prerequisites"
      elif software.get_type() == "tool":
        subdir = "tools"
      elif software.get_type() == "module":
        subdir = "modules"
    return subdir
  #

  def create(self):
    self.logger.info("Installer %s create step starts" % self.name)


    # Check if we have a DISTENE
    distene_install_dir = ""
    for software in self.softwares:
      # install binary archives in installer directory
      software_mode = "install_from_binary_archive"
      # Artefact softwares are used by yamm only for compilation purposes.
      # They are not real softwares
      if software.isArtefact():
        continue

      software_install_name = self.get_software_install_path(software)
      if software_install_name == '':
        self.logger.info('software_install_name pour %s est vide: software_mode = nothing' % software.name)
        software_mode = 'nothing'
      subdir = self.__get_subdir_from_software_type(software)
      # Sets the install directory for the current software
      if software.name in self.software_install_dirs:
        installdir = os.path.join(self.topdirectory, software_install_name)
      else:
        installdir = os.path.join(self.topdirectory, subdir, software_install_name)
      self.executor.options.set_software_option(software.name, "software_install_directory", installdir)

      if software.name == "DISTENE":
        distene_install_dir = installdir
      software.set_command_env(self.softwares,
                               self.executor.options,
                               software_mode,
                               self.executor.version_object,
                               self.python_version)
      software.create_tasks()
      executor_software = software.get_executor_software()
      self.executor.add_software(executor_software)

      self.map_name_to_softwares[software.name] = software

    # The env files are filled
    self.executor.add_hooks(None, self.add_software_to_env_installer_files_end_hook)

    # The commands in unmake_movable_archive_commands must not be run: disable them
    for soft in self.executor.executor_softwares:
      if soft.install_binary_archive_task:
        soft.install_binary_archive_task.setRunPostInstallCommands(False)

    self.executor.execute()

    # /lib64 must be added to the end of LD_LIBRARY_PATH with ctx_file
    if self.universal:
      with open(self.context_file, 'a') as ctx_file:
        ctx_file.write("\n")
        ctx_file.write("DEBIANFORSALOME=%(PREREQUISITES_ROOT_DIR)s/debianForSalome\n")
        ctx_file.write("ADD_TO_LD_LIBRARY_PATH: %(DEBIANFORSALOME)s/lib:/lib64\n")
        if "cfd" in self.salome_flavour:
          ctx_file.write("ADD_TO_LIBRARY_PATH: %(DEBIANFORSALOME)s/lib\n")
          ctx_file.write("ADD_TO_LIBRARY_PATH: /usr/lib/x86_64-linux-gnu\n")

    if "neptune" in self.salome_flavour:
      with open(self.context_file, 'a') as ctx_file:
        ctx_file.write("ADD_TO_PYTHONPATH: %(CS_ROOT_DIR)s/lib/python"+self.python_version+"/site-packages/neptune_cfd\n")
      with open(self.prerequisites_file, 'a') as pre_file:
        pre_file.write("export PYTHONPATH=${CS_ROOT_DIR}/lib/python"+self.python_version+"/site-packages/neptune_cfd:${PYTHONPATH}\n")
      with open(self.context_file, 'a') as ctx_file:
        ctx_file.write("ADD_TO_PYTHONPATH: %(SATURNE_DBG_ROOT_DIR)s/lib/python"+self.python_version+"/site-packages/neptune_cfd\n")
      with open(self.prerequisites_file, 'a') as pre_file:
        pre_file.write("export PYTHONPATH=${SATURNE_DBG_ROOT_DIR}/lib/python"+self.python_version+"/site-packages/neptune_cfd:${PYTHONPATH}\n")

    # Change temporary directory
    if self.default_temporary_path != "":
      with open(self.prerequisites_file, 'a') as pre_file:
        pre_file.write("\n")
        pre_file.write("#------- Temporary directory -----\n")
        pre_file.write("export SALOME_TMP_DIR=" + self.default_temporary_path + "\n")
        pre_file.write("export TMP_DIR=" + self.default_temporary_path + "\n")
      with open(self.context_file, 'a') as ctx_file:
        ctx_file.write("\n")
        ctx_file.write("#------- Temporary directory -----\n")
        ctx_file.write("SALOME_TMP_DIR=" + self.default_temporary_path + "\n")
        ctx_file.write("TMP_DIR=" + self.default_temporary_path + "\n")

    # Remove Distene directory
    if distene_install_dir:
      if os.path.isdir(distene_install_dir):
        misc.fast_delete(distene_install_dir)
    self.logger.info("Installer %s create step ends" % self.name)
    return True

  def end(self):
    self.logger.info("Installer %s end step starts" % self.name)
    with open(self.modules_file, 'a') as mod_file:
      salomeAppLine = 'export SalomeAppConfig="${ROOT_SALOME}"'
      if self.guiIconPathStr != "":
        salomeAppLine += ":" + self.guiIconPathStr
      salomeAppLine += "\n"
      mod_file.write(salomeAppLine)

    # Step 5: add misc files
    # # README
    readme_file_name = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                    os.pardir, "installer_files", "README")
    with open(readme_file_name) as readme_file:
      readme_lines = readme_file.readlines()
      if self.nosubdir:
        # Supprime les lignes mentionnant les sous-répertoires
        patterns = ('o modules', 'o prerequisites', 'o tools')
        readme_lines = [l for l in readme_lines if not any(l.startswith(s) for s in patterns)]
      readme = "".join(readme_lines)
      readme = misc.PercentTemplate(readme)

    # Get the platform (remove the trailing \\n \\l\n\n
    with open("/etc/issue") as f:
      platform = f.read().strip()
      if platform.endswith("\\n \\l"):
        platform = platform[:-5]

    readme_file_name = os.path.join(self.topdirectory, "README")
    if readme != "":
      with open(readme_file_name, 'w') as readme_file:
        readme_file.write(readme.substitute(default_name=self.default_name,
                                            default_exec=self.default_exec,
                                            env_files=self.stringsTemplate.get_readme_env_files(),
                                            post_install_file=self.stringsTemplate.get_readme_post_install_files(),
                                            other_files=self.stringsTemplate.get_readme_other_files(),
                                            salome_version=self.salome_version,
                                            version_major=misc.major(self.salome_version),
                                            gcc_version=misc.get_gcc_version(),
                                            platform=platform,
                                            contact=self.default_contact))


    # # CatalogResources
    if not self.universal:
      self.catalog_resources = self.get_catalog_cmd()
      # # To execute servers.pvsc creation script if needed
      self.salome_hpc_visu_servers = self.get_hpc_visu_servers_cmd()

    # # patched files
    patchdir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                    os.pardir, "installer_files", "patches",
                                    self.salome_version)
    if os.path.exists(patchdir):
      shutil.copytree(patchdir, os.path.join(self.topdirectory, ".patches", self.salome_version))
      shutil.copy(os.path.join(patchdir, os.pardir, "patch.sh"),
                  os.path.join(self.topdirectory, ".patches"))

    # # config appli template
    self.create_config_appli()

    help = self.get_create_appli_help()

    # # create_appli.sh
    language = self.get_create_appli_language()
    create_appli = self.stringsTemplate.get_installer_create_appli_template().substitute(
                      salome_version=self.salome_version,
                      desktop_file_name=self.default_desktop_filename,
                      help=help,
                      language=language,
                      default_exec=self.default_exec,
                      default_name=self.default_name,
                      default_appli_dir=self.default_appli_dir,
                      salome_hpc_visu_servers=self.salome_hpc_visu_servers,
                      catalog_resources=self.catalog_resources,
                      post_install_file_name=os.path.basename(self.post_install_file_name),
                      prerequisites_file=os.path.basename(self.prerequisites_file),
                      modules_file=os.path.basename(self.modules_file),
                      context_file=os.path.basename(self.context_file))
    create_appli_file_name = os.path.join(self.topdirectory, "create_appli.sh")
    with open(create_appli_file_name, 'w') as create_appli_file:
      create_appli_file.write(create_appli)
    os.chmod(create_appli_file_name , 0o777)  # All rights for all

    # # Salome icon
    if self.guiIconPath != "":
      shutil.copyfile(self.guiIconPath,
                      os.path.join(self.topdirectory, "icon.png"))

      # # Desktop file (only if GUI module is present)
      desktop_file_name = os.path.join(self.topdirectory, "." + self.default_desktop_filename)
      with open(desktop_file_name, 'w') as desktop_file:
        desktop_file.write(self.stringsTemplate.get_installer_desktop_file_template().substitute(
          default_name='%s %s' % (self.default_name, self.salome_version),
          default_exec=self.default_exec))

    # Step 6: run additional commands provided by user
    for command in self.additional_commands:
      try:
        ret = subprocess.call(command, shell=True)
        if ret != 0:
          self.logger.error("Command %s failed with signal %s" % (command, ret))
      except OSErro as e:
        self.logger.error("Execution of command %s failed: %s" % (command, e))

    # Step 6: create tgz
    if self.tgz:
      installer_tgz_path = os.path.join(os.path.dirname(self.topdirectory), self.name + ".tar.gz")
      ret = self.create_pack(installer_tgz_path)
      if not ret:
        return ret
    # Step 7: create run file
      if self.runnable:
        ret = self.create_runnable(self.name)
        if not ret:
          return ret
      if self.delete:
        misc.fast_delete(self.topdirectory)

    return True

  def get_create_appli_help(self):
    return self.stringsTemplate.get_installer_create_appli_help_template().substitute(
                      default_config_file=self.default_config_file)

  def get_create_appli_language(self):
    return self.stringsTemplate.get_installer_create_appli_language_template().substitute(
                      default_config_file=self.default_config_file)

  def get_catalog_cmd(self):
    catalog_file = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                    os.pardir, "installer_files", "config",
                                    'create_catalog.py')
    create_catalog_lines = open(catalog_file).read()
    new_catalog = os.path.join(self.topdirectory, 'create_catalog.py')
    with open(new_catalog, 'w') as catalog_file:
        catalog_file.write(create_catalog_lines.replace("<salome_version>",
                                                        self.salome_version))

    os.chmod(new_catalog , 0o777)  # All rights for all
    return """
# Create the Catalog Resources file
if test -n "$HPC_CATALOG"; then
  CREATE_CATALOG_OPT=" -q -f ${APPLIDIR}/CatalogResources.xml"
  if test -z "$QUIET"; then
    echo "Creating the Catalog Resources file in ${APPLIDIR}/CatalogResources.xml"
    CREATE_CATALOG_OPT=" -f ${APPLIDIR}/CatalogResources.xml"
  fi
  ./create_catalog.py $CREATE_CATALOG_OPT
  cp create_catalog.py ${APPLIDIR}
fi
"""

  def get_hpc_visu_servers_cmd(self):
    # server files
    confdir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           os.pardir, "installer_files", "config")
    if os.path.exists(confdir):
      shutil.copy(os.path.join(confdir, "salome_hpc_visu_servers.py"), self.hpc_visu_servers_file_name)
      shutil.copy(os.path.join(confdir, "servers.pvsc"), self.servers_pvsc)

    os.chmod(self.hpc_visu_servers_file_name, 0o777)  # All rights for all
    os.chmod(self.servers_pvsc, 0o777)  # All rights for all
    cmd = "\n# apply hpc_visu_servers script"
    cmd += "\n./salome_hpc_visu_servers.py"
    return cmd

  def get_decompress_create_appli_cmd(self):
    return self.stringsTemplate.get_installer_create_appli_command_template().substitute(
        appli_dir=os.path.basename(self.default_appli_dir),
        salome_version=self.salome_version,
        archive_name=self.name)

  def get_decompress_usage(self):
    return self.stringsTemplate.get_installer_decompress_usage().substitute(
      default_name=self.default_name,
      default_appli_dir=self.default_appli_dir,
      default_install_dir=self.default_install_dir,
      default_temporary_path=self.default_temporary_path or tempfile.gettempdir())

  def get_decompress_options(self):
    return self.stringsTemplate.get_installer_decompress_options().substitute(
      default_temporary_path=self.default_temporary_path or tempfile.gettempdir())

  def get_decompress_language(self):
    return self.stringsTemplate.get_installer_decompress_language().substitute(default_name=self.default_name)

  def get_decompress_content(self):
    content = ''
    create_appli_command = ""
    if self.has_salome_kernel_software:
      create_appli_command = self.get_decompress_create_appli_cmd()
    usage = self.get_decompress_usage()
    options = self.get_decompress_options()
    language = self.get_decompress_language()

    title_deco = "=" * (len(self.default_name) + len(self.salome_version))
    content += self.stringsTemplate.get_installer_decompress_template().substitute(
            default_temporary_path=self.default_temporary_path or tempfile.gettempdir(),
            salome_version=self.salome_version,
            archive_name=self.name,
            usage=usage,
            options=options,
            create_appli_command=create_appli_command,
            title_deco=title_deco,
            language=language,
            final_custom_command=self.final_custom_command,
            default_name=self.default_name,
            default_install_dir=self.default_install_dir,
            prerequisites_file=os.path.basename(self.prerequisites_file),
            context_file=os.path.basename(self.context_file),
            default_config_file=self.default_config_file)

    if self.debian_depends:
        debian_depends = '"\necho "'.join(self.debian_depends)
        debian_command = 'Debian/Ubuntu: apt-get install -y %s' % (' '.join(self.debian_depends))
        debian_command += '\nEDF Calibre 9: pkcon install -y %s' % (' '.join(self.debian_depends))
        content += """
echo ""
echo "============================== WARNING ============================"
echo "This installer needs the following Debian packages to be installed:"
echo "{}"
echo ""
echo "Use one of the following commands to install them:"
echo "{}"
echo "==================================================================="
echo ""

""".format(debian_depends, debian_command)
    return content


  def create_config_appli(self):
    self.logger.info("[%s] Create config appli step starts" % self.name)
    modules_softwares = {}
    extra_test_softwares = {}
    for software in self.softwares:
      subdir = self.__get_subdir_from_software_type(software)
      software_install_name = self.get_software_install_path(software)
      if software.get_type() == "module":
        install_path = os.path.join("${ROOT_SALOME}", subdir, software_install_name)
        modules_softwares[software] = install_path
      extra_test_path_list = software.get_extra_test_path_list()
      if extra_test_path_list:
        extra_test_softwares[software] = [os.path.join("${ROOT_SALOME}",
                                                       subdir,
                                                       software_install_name,
                                                       extra_test_path)
                                          for extra_test_path in extra_test_path_list]

    # Create SALOME Application config file
    create_config_appli(self.config_appli_filename,
                        os.path.join('${ROOT_SALOME}', os.path.basename(self.prerequisites_file)),
                        os.path.join('${ROOT_SALOME}', os.path.basename(self.context_file)),
                        os.path.join('${ROOT_SALOME}', os.path.basename(self.sha1_collection_file)),
                        self.module_load,
                        modules_softwares,
                        extra_test_softwares)


  def add_software_to_env_installer_files_end_hook(self, executor_software):
    software = self.map_name_to_softwares[executor_software.name]
    if software.isArtefact():
      return
    # Reset the software directory install name to the one used in the installer
    software_install_name = software.get_executor_software_name()
    # Add software in the prerequisites file
    with open(self.prerequisites_file, "a") as pre_file:
      if software.get_type() == "module":
        pre_file.write(software.get_prerequisite_str("${MODULES_ROOT_DIR}"))
      else:
        software.executor_software_name = software_install_name
        if software.get_type() == "tool":
          pre_file.write(software.get_prerequisite_str("${TOOLS_ROOT_DIR}"))
        else:
          pre_file.write(software.get_prerequisite_str("${PREREQUISITES_ROOT_DIR}"))
    # Add software to the configuration file
    with open(self.context_file, "a") as ctx_file:
      if software.get_type() == "module":
        ctx_file.write(software.get_configuration_str("%(MODULES_ROOT_DIR)s"))
      else:
        software.executor_software_name = software_install_name
        if software.get_type() == "tool":
            ctx_file.write(software.get_configuration_str("%(TOOLS_ROOT_DIR)s"))
        else:
            ctx_file.write(software.get_configuration_str("%(PREREQUISITES_ROOT_DIR)s"))
    # Add software in the modules file
    with open(self.modules_file, "a") as mod_file:
      subdir = "" if self.nosubdir else "modules"
      mod_file.write(software.get_module_str(os.path.join("${ROOT_SALOME}", subdir)))

  pass

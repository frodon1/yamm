#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 - 2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os

from yamm.core.base import misc
from yamm.core.framework.installer_files.strings_template import FrameworkInstallerStringsTemplate


class SalomeInstallerStringsTemplate(FrameworkInstallerStringsTemplate):

  def get_installer_salome_prerequisite_header(self):
    return """
export ROOT_SALOME="__ROOT_SALOME__"
export LC_NUMERIC=C

# To be sure to display icons in Gnome, we use the Gnome Configuration tool
gconfTool=`which gconftool-2`
if test -n "$gconfTool"; then
  $gconfTool --type boolean --set /desktop/gnome/interface/buttons_have_icons true
  $gconfTool --type boolean --set /desktop/gnome/interface/menus_have_icons true
fi

export SALOME_PLUGINS_PATH="${ROOT_SALOME}/plugins":${SALOME_PLUGINS_PATH}

"""

  def get_installer_salome_context_header(self):
    return """
ROOT_SALOME="__ROOT_SALOME__"
LC_NUMERIC=C

ADD_TO_SALOME_PLUGINS_PATH:"%(ROOT_SALOME)s/plugins"

"""

  def get_installer_salome_prerequisite_header_check_prereq(self, nosubdir=False):
    thedir = os.path.join('${ROOT_SALOME}', 'prerequisites')
    if nosubdir:
      thedir = '${ROOT_SALOME}'
    return """if test ! -d "{0}"; then
  echo "=========================================="
  echo "Error: SALOME prerequisites path not found"
  echo "=========================================="
  exit 1
fi
export PREREQUISITES_ROOT_DIR="{0}"

""".format(thedir)

  def get_installer_salome_context_header_check_prereq(self, nosubdir=False):
    thedir = os.path.join('%(ROOT_SALOME)s', 'prerequisites')
    if nosubdir:
      thedir = '%(ROOT_SALOME)s'
    return """PREREQUISITES_ROOT_DIR="{0}"
""".format(thedir)

  def get_installer_salome_prerequisite_header_check_tools(self, nosubdir=False):
    thedir = os.path.join('${ROOT_SALOME}', 'tools')
    if nosubdir:
      thedir = '${ROOT_SALOME}'
    return """if test ! -d "{0}"; then
  echo "=================================="
  echo "Error: SALOME tools path not found"
  echo "=================================="
  exit 1
fi
export TOOLS_ROOT_DIR="{0}"

""".format(thedir)

  def get_installer_salome_context_header_check_tools(self, nosubdir=False):
    thedir = os.path.join('%(ROOT_SALOME)s', 'tools')
    if nosubdir:
      thedir = '%(ROOT_SALOME)s'
    return """TOOLS_ROOT_DIR="{0}"
""".format(thedir)

  def get_installer_salome_prerequisite_header_check_modules(self, nosubdir=False):
    return """if test -d "${ROOT_SALOME}/modules"; then
  export MODULES_ROOT_DIR="${ROOT_SALOME}/modules"
fi

"""

  def get_installer_salome_context_header_check_modules(self, nosubdir=False):
    thedir = os.path.join('%(ROOT_SALOME)s', 'modules')
    if nosubdir:
      thedir = '%(ROOT_SALOME)s'
    return """MODULES_ROOT_DIR="{0}"
""".format(thedir)

  def get_readme_env_files(self):
    return """
o salome_modules.sh                  : environnement file for the Salome modules
o salome_prerequisites.sh            : environnement file for the Salome prerequisites and tools
o salome_context.cfg                 : context file for the Salome prerequisites and tools
"""

  def get_readme_other_files(self):
    return """
o salome_%{salome_version}.desktop   : a Salome desktop file to be used as launcher (if GUI module is present)
o icon_salome%{version_major}.png    : the Salome icon (to be used to create a desktop shortcut)
"""

  def get_readme_post_install_files(self):
    return """
o salome_post_install.py             : post-install script used by create_appli
    """

  def get_installer_decompress_usage(self):
    installer_decompress_usage = """usage()
{
cat<<EOF
usage: $0 [-h] [-t DIR] [-a DIR] [-s DIR] [-d] [-m] [-c] [-D] [-l LANGUAGE] [-p] [-f] [-q] [-v]

The script installs %{default_name} in a specified directory.
A desktop shortcut and a menu entry are added.

Options:
 -h    Show this message
 -t    %{default_name} install directory (default is %{default_install_dir})
 -a    %{default_name} virtual application directory (default is %{default_appli_dir})
 -s    %{default_name} temporary directory (default is %{default_temporary_path})
 -d    Do not install desktop file
 -m    Do not install menu entry
 -c    Do not install catalog for HPC resources
 -D    Equivalent to -dm. Do not install desktop file and menu entry
 -l    Choose language (English or French, default is English)
 -p    Do not install default preference file
 -f    Force installation
 -q    Quiet installation
 -v    Verbose mode
EOF
}

"""
    return misc.PercentTemplate(installer_decompress_usage)

  def get_installer_decompress_language(self):
    language = """if test "$PREFS" = "n"; then
  DEFAULTLANGUAGE=""
  test -z "$LANGUAGE" && echo -n "%{default_name} is in English, do you want to be in French ? [y/N] : " && read -e -p "" LANGUAGE
  test -z "$LANGUAGE" && LANGUAGE=$DEFAULTLANGUAGE
  case "$LANGUAGE" in
  y|Y|yes|YES|Yes|o|O|oui|OUI|Oui) LANGUAGE="fr" ;;
  *) LANGUAGE=$DEFAULTLANGUAGE ;;
  esac
fi
    """
    return misc.PercentTemplate(language)

  def get_installer_decompress_options(self):
    options = """TARGETDIR=""
APPLIDIR=""
TEMPDIR="%{default_temporary_path}"
DESKTOP="y"
MENU="y"
HPC_CATALOG="y"
LANGUAGE=""
PREFS="n"
FORCE=""
QUIET=""
VERBOSE=""

while getopts ":ht:a:s:dmcDl:pfqv" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    t)
      TARGETDIR="$OPTARG"
      ;;
    a)
      APPLIDIR="$OPTARG"
      ;;
    s)
      TEMPDIR="$OPTARG"
      ;;
    d)
      DESKTOP=""
      ;;
    m)
      MENU=""
      ;;
    c)
      HPC_CATALOG=""
      ;;
    D)
      DESKTOP=""
      MENU=""
      ;;
    l)
      LANGUAGE="$OPTARG"
      ;;
    p)
      PREFS=""
      ;;
    f)
      FORCE="y"
      ;;
    q)
      QUIET="y"
      ;;
    v)
      VERBOSE="y"
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done"""
    return misc.PercentTemplate(options)

  def get_installer_decompress_template(self):
    installer_decompress_template = """
%{usage}
%{options}

if test -z "$QUIET"; then
  echo ""
  echo "===========================%{title_deco}"
  echo "Self Extracting %{default_name} %{salome_version} Installer"
  echo "===========================%{title_deco}"
  echo ""
fi

DEFAULTTARGETDIR="%{default_install_dir}"
test -z "$TARGETDIR" && echo -n "Enter the %{default_name} install directory [default=$DEFAULTTARGETDIR] : " && read -e -p "" TARGETDIR
eval TARGETDIR="$TARGETDIR" # to expand the tilde
test -z "$TARGETDIR" && TARGETDIR="$DEFAULTTARGETDIR"

if test -d "$TARGETDIR/%{archive_name}"; then
  test -z "$FORCE" && echo -n "The directory $TARGETDIR/%{archive_name} already exists, install anyway ? [y/N] " && read -p "" FORCE
  case "$FORCE" in
    y|Y|yes|YES|Yes|o|O|oui|OUI|Oui) FORCE="y" ;;
  esac
  if test "$FORCE" != "y"; then
    echo "Installation aborted"
    exit 1
  fi
fi

%{language}
mkdir -p $TARGETDIR || die "mkdir failed"

if test -z "$QUIET"; then
  echo "Installation of %{default_name} %{salome_version} in $TARGETDIR/%{archive_name} ..."
fi

tail -n+$ARCHIVE "$0" > "$TARGETDIR"/%{archive_name}.tgz

cd "$TARGETDIR"

check "$TARGETDIR"/%{archive_name}.tgz "$VERBOSE"

if test "$QUIET" = "y"; then
  tar xzf %{archive_name}.tgz
else
  if test "$VERBOSE" = "y"; then
    tar xvzf %{archive_name}.tgz
  else
    bar_cat %{archive_name}.tgz
  fi
  echo "Done"
fi

rm %{archive_name}.tgz

sed -i "s#__ROOT_SALOME__#${TARGETDIR}/%{archive_name}#g" %{archive_name}/%{prerequisites_file}
sed -i "s#__ROOT_SALOME__#${TARGETDIR}/%{archive_name}#g" %{archive_name}/%{context_file}

if test ! -z "$TEMPDIR"; then
  if test "$TEMPDIR" != "/tmp"; then
    {
    echo ""
    echo "#------- Temporary directory -----"
    echo "export SALOME_TMP_DIR=$TEMPDIR"
    echo "export TMP_DIR=$TEMPDIR"
    } >> %{archive_name}/%{prerequisites_file}
    {
    echo ""
    echo "#------- Temporary directory -----"
    echo "SALOME_TMP_DIR=$TEMPDIR"
    echo "TMP_DIR=$TEMPDIR"
    } >> %{archive_name}/%{context_file}
  fi
fi

TEMPDIR=$(grep "SALOME_TMP_DIR" %{archive_name}/%{context_file} | cut -d '=' -f2)
if [ ! -z "$TEMPDIR" ]; then
  if [ ! -d "$TEMPDIR" ]; then
    echo "Temporary directory ${TEMPDIR} does not exist, ${TEMPDIR} is replaced by /tmp !"
    sed -i '/Temporary directory/,+3d' %{archive_name}/%{prerequisites_file}
    sed -i '/Temporary directory/,+3d' %{archive_name}/%{context_file}
  fi
fi

%{create_appli_command}

%{final_custom_command}

if test -z "$QUIET"; then
  notifyCmd=$(which notify-send)
  if test -n "$notifyCmd"; then
    if test "$LANGUAGE" = "fr"; then
      $notifyCmd -i "${TARGETDIR}/%{archive_name}/icon.png" "%{default_name}" "Installation de %{default_name} %{salome_version} terminee."
    else
      $notifyCmd -i "${TARGETDIR}/%{archive_name}/icon.png" "%{default_name}" "Installation of %{default_name} %{salome_version} done."
    fi
  fi
fi
"""

    return misc.PercentTemplate(installer_decompress_template)

  def get_installer_create_appli_command_template(self):
    installer_create_appli_command_template = """

cd "$TARGETDIR/%{archive_name}"

OPTIONS=""

if test -z "$APPLIDIR"; then
  APPLIDIR=$TARGETDIR/%{appli_dir}
fi
eval APPLIDIR="$APPLIDIR"
OPTIONS="$OPTIONS -a $APPLIDIR"

if test -z "$TEMPDIR"; then
  TEMPDIR=/tmp
fi
OPTION="$OPTION -s $TEMPDIR"

if test "$VERBOSE" = "y"; then
  OPTIONS="$OPTIONS -v"
fi
if test "$QUIET" = "y"; then
  OPTIONS="$OPTIONS -q"
fi
if test -z "$DESKTOP"; then
  OPTIONS="$OPTIONS -d"
fi
if test -z "$MENU"; then
  OPTIONS="$OPTIONS -m"
fi
if test -z "$HPC_CATALOG"; then
  OPTIONS="$OPTIONS -c"
fi
case "$LANGUAGE" in
 fr)
   OPTIONS="$OPTIONS -l fr" ;;
esac
if test -z "$PREFS"; then
  OPTIONS="$OPTIONS -p"
fi

./create_appli.sh $OPTIONS

"""
    return misc.PercentTemplate(installer_create_appli_command_template)

  def get_installer_desktop_file_template(self):
    installer_desktop_file_template = """[Desktop Entry]
Type=Application
Terminal=false
Exec=APPLIDIR/%{default_exec} %%f
Name=%{default_name}
Icon=SALOMEDIR/icon.png
"""

    return misc.PercentTemplate(installer_desktop_file_template)

  def get_installer_create_appli_help_template(self):
    template = """
# Force help Browser to Firefox
#
config_file="$HOME/.config/salome/%{default_config_file}"
if test ! -d "$HOME"/.config/salome; then
   mkdir -p "$HOME"/.config/salome  || die "mkdir failed"
fi
help_section_begin="  <section name=\\"ExternalBrowser\\">"
help_section_body="    <parameter value=\\"true\\" name=\\"use_external_browser\\"/>\n    <parameter value=\\"firefox\\" name=\\"application\\"/>"
help_section_end="  </section>"
if test -e "$config_file"; then
  sed -i "s#.*</document>.*#${help_section_begin}\\
${help_section_body}\\
${help_section_end}\\
</document>#g" "$config_file"
else 
   cat << EOF > "$config_file"
<!DOCTYPE document>
<document>
${help_section_begin}
${help_section_body}
${help_section_end}
 </document>
EOF
fi
"""
    return misc.PercentTemplate(template)

  def get_installer_create_appli_language_template(self):
    template = """# Change language if requested
# * If the preferences file is installed
#   - if language section exists, it is modified
#   - if not, language section is added
# * if not, the preference file is created with the language section
#
if test -n "$LANGUAGE"; then
  config_file="$HOME/.config/salome/%{default_config_file}"
  if test ! -d "$HOME"/.config/salome; then
     mkdir -p "$HOME"/.config/salome  || die "mkdir failed"
  fi
  language_section_begin="  <section name=\\"language\\">"
  language_section_body="    <parameter value=\\"${LANGUAGE}\\" name=\\"language\\"/>"
  language_section_end="  </section>"
  if test -z "$QUIET"; then
    echo "Setting the language to $LANGUAGE in the preferences file $config_file"
  fi
  if test -e "$config_file"; then
    if test -n "$(grep language $config_file)"; then
      sed -i "s#.*\\" name=\\"language\\".*#${language_section_body}#g" "$config_file"
    else
      sed -i "s#.*</document>.*#${language_section_begin}\\
${language_section_body}\\
${language_section_end}\\
</document>#g" "$config_file"
    fi
  else
    cat << EOF > "$config_file"
<!DOCTYPE document>
<document>
${language_section_begin}
${language_section_body}
${language_section_end}
 </document>
EOF
  fi
fi
"""
    return misc.PercentTemplate(template)

  def get_installer_create_appli_template(self):
    installer_create_appli_template = """#!/bin/bash
die()
{
  echo "FATAL ERROR: $* (status $?)" 1>&2
  exit 1
}

usage()
{
cat<<EOF
usage: $0 options

The script installs a %{default_name} virtual application in a specified directory.
A desktop shortcut and a menu entry are added.

Options:
 -h    Show this message
 -a    Application directory (default is %{default_appli_dir})
 -d    Do not install desktop file
 -s    Salome temporary directory (default is /tmp)
 -m    Do not install menu entry
 -c    Do not install catalog for HPC resources
 -D    Equivalent to -dm. Do not install desktop file and menu entry
 -l    Choose language (English or French, default is English)
 -p    Do not install default preference file
 -q    Quiet installation
 -v    Verbose mode
EOF
}

check_path()
{
  if test -n "$1" ; then
    arr=$(echo "$1" | tr ":" "\n")
    for x in $arr
    do
      if test ! -d "$x" ; then
       echo "Warning: the directory $x is in a path variable but does not exist"
      fi
    done
  fi
}

APPLIDIR=""
TEMPDIR="/tmp"
DESKTOP="y"
MENU="y"
HPC_CATALOG="y"
LANGUAGE=""
PREFS="y"
QUIET=""
VERBOSE=""
while getopts ":ha:s:dmcDl:pqv" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
      ;;
    a)
      APPLIDIR="$OPTARG"
      ;;
    s)
      TEMPDIR="$OPTARG"
      ;;
    d)
      DESKTOP=""
      ;;
    m)
      MENU=""
      ;;
    c)
      HPC_CATALOG=""
      ;;
    D)
      DESKTOP=""
      MENU=""
      ;;
    l)
      LANGUAGE=$OPTARG
      ;;
    p)
      PREFS=""
      ;;
    q)
      QUIET="y"
      ;;
    v)
      VERBOSE="y"
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done


if test -z "$SALOMEDIR"; then
  SALOMEDIR=`python -c "import os; print os.path.normpath(os.path.dirname(os.path.abspath(os.path.realpath('$0'))))" 2>/dev/null`
fi

. "${SALOMEDIR}"/%{prerequisites_file}
. "${SALOMEDIR}"/%{modules_file}

# Change this directory as wanted
DEFAULTAPPLIDIR="%{default_appli_dir}"

test -z "$APPLIDIR" && echo -n "Enter %{default_name} virtual application directory [default="$DEFAULTAPPLIDIR"] : " && read -e -p "" APPLIDIR
test -z "$APPLIDIR" && APPLIDIR="$DEFAULTAPPLIDIR"
if test -z "$QUIET"; then
  echo "Creating %{default_name} application in $APPLIDIR ..."
fi
mkdir -p "$APPLIDIR" || die "mkdir failed"

cd "$APPLIDIR"
sed "s#\${ROOT_SALOME}#${SALOMEDIR}#g" "${SALOMEDIR}"/.config_appli_template.xml > config_appli.xml

# Retrieve KERNEL install directory from config_appli.xml file
# using awk with the file separator being " or '.
# We use a regexp for that and print the second to last value :
KERNELDIR=$(cat config_appli.xml |grep KERNEL|awk -v FS=[\\"\\'] '{print $(NF-1)}')
if test "${VERBOSE}" = "y"; then
  python "${KERNELDIR}"/bin/salome/appli_gen.py
else
  python "${KERNELDIR}"/bin/salome/appli_gen.py > appli_%{salome_version}.log 2>&1
fi

cd "$SALOMEDIR"

# Install the menu and desktop files if requested
if test -e "${SALOMEDIR}"/.%{desktop_file_name}; then
  sed "s#SALOMEDIR#${SALOMEDIR}#g" "${SALOMEDIR}"/.%{desktop_file_name} > "${SALOMEDIR}"/%{desktop_file_name}
  sed -i "s#APPLIDIR#${APPLIDIR}#g" "${SALOMEDIR}"/%{desktop_file_name}
  sed -i "s#HOME#${HOME}#g" "${SALOMEDIR}"/%{desktop_file_name}
fi

if test -n "$MENU"; then
  if test -e "${SALOMEDIR}"/%{desktop_file_name}; then
    if test ! -d "$HOME"/.local/share/applications; then
      mkdir -p "$HOME"/.local/share/applications  || die "mkdir failed"
    fi
    desktop-file-install --dir="$HOME"/.local/share/applications --copy-name-to-generic-name \
                         --add-category=Education --add-category=Science --add-mime-type=application/x-hdf \
                          "${SALOMEDIR}"/%{desktop_file_name} > desktop-file-install-menu.log 2>&1
    chmod +x "$HOME"/.local/share/applications/%{desktop_file_name}
  fi
fi

if test -n "$DESKTOP"; then
  DESKTOPDIR="$HOME/Desktop"
  if test ! -e "$DESKTOPDIR"; then
    DESKTOPDIR="$HOME/Bureau"
    if test ! -e "$DESKTOPDIR"; then
      echo "Desktop directory not found."
      DESKTOPDIR=""
    fi
  fi
  if test -n "${DESKTOPDIR}"; then
    if test -e "${SALOMEDIR}"/%{desktop_file_name}; then
    desktop-file-install --dir="${DESKTOPDIR}" --copy-name-to-generic-name \
                         --add-category=Education --add-category=Science --add-mime-type=application/x-hdf \
                          "${SALOMEDIR}"/%{desktop_file_name} > desktop-file-install-desktop.log 2>&1
    chmod +x "${DESKTOPDIR}"/%{desktop_file_name}
    fi
  fi
fi

%{catalog_resources}
%{help}
%{language}

if test ! -z "$TEMPDIR"; then
  if test "$TEMPDIR" != "/tmp"; then
    {
    echo ""
    echo "#------- Temporary directory -----"
    echo "export SALOME_TMP_DIR=$TEMPDIR"
    echo "export TMP_DIR=$TEMPDIR"
    } >> %{prerequisites_file}
    {
    echo ""
    echo "#------- Temporary directory -----"
    echo "SALOME_TMP_DIR=$TEMPDIR"
    echo "TMP_DIR=$TEMPDIR"
    } >> %{context_file}
  fi
fi

TEMPDIR=$(grep "SALOME_TMP_DIR" %{context_file} | cut -d '=' -f2)
if [ ! -z "$TEMPDIR" ]; then
  if [ ! -d "$TEMPDIR" ]; then
    echo "Temporary directory ${TEMPDIR} does not exist, ${TEMPDIR} is replaced by /tmp !"
    sed -i '/Temporary directory/,+3d' %{prerequisites_file}
    sed -i '/Temporary directory/,+3d' %{context_file}
  fi
fi

if test -z "$QUIET"; then
  echo "%{default_name} post-installation ..."
fi

# Put here additional commands after creation of the appli
# Replace the keyword: #APPLI_CUSTOM_COMMANDS

#APPLI_CUSTOM_COMMANDS

# End of additional commands
cd "$SALOMEDIR"

# check paths
if test -z "$QUIET"; then
  check_path ${LD_LIBRARY_PATH}
  check_path ${PATH}
  check_path ${PYTHONPATH}
else
  check_path "${LD_LIBRARY_PATH}" 1>/dev/null 2>&1
  check_path "${PATH}" 1>/dev/null 2>&1
  check_path "${PYTHONPATH}" 1>/dev/null 2>&1
fi

# apply patches
if test -d .patches; then
  ./.patches/patch.sh
  # remove patch directory
  rm -rf .patches
fi

# apply post install script
./%{post_install_file_name}

%{salome_hpc_visu_servers}

if test -z "$QUIET"; then
  echo "The %{default_name} application was successfully created."
  echo "To run the application:"
  echo "\$ ${APPLIDIR}/%{default_exec}"
fi

#${APPLIDIR}/%{default_exec} $*
"""
    return misc.PercentTemplate(installer_create_appli_template)

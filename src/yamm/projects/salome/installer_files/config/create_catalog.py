#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generates a new catalog of resources (file CatalogResources.xml) and configures
SALOME for the current user on the main HPC ressources used at EDF R&D.
You need acces rights to the remote resources in order to perform the
configuration.
If you don't have the required rights for a resource, it will not be added to
the catalog."""

from builtins import input
from builtins import object
from collections import defaultdict
import getpass
import logging
import optparse
import os
import subprocess
import sys

# lxml module is not always available
try:
  import lxml.etree as ET
except ImportError:
  import xml.etree.ElementTree as ET


SALOME_VERSION = "<salome_version>"


class Resource(object):
    def __init__(self, name, version, logger):
        self.name = name
        self.version = version
        self.logger = logger
        self.set_defaults()

    def set_defaults(self):
        self.hostname = "%s.hpc.edf.fr" % self.name

        remote_salome_dir = self.get_remote_salome_dir()
        self.remote_appligen = os.path.join(remote_salome_dir, 'modules',
                                            'install',
                                            'KERNEL_%s' % self.version,
                                            'bin', 'salome', 'appli_gen.py')
        self.remote_salome_appli = os.path.join(remote_salome_dir,
                                                "appli_%s" % self.version)

        username = getpass.getuser()
        self.remote_virtual_appli = os.path.join("/scratch", username,
                                                 "appli_%s" % self.version)
        self.working_dir = os.path.join("/scratch", username, "workingdir")

    def get_remote_salome_dir(self):
        return "/home/projets/salome/logiciels/salome/%s" % self.version

    def prepare(self):
        stdout = sys.stdout
        stderr = sys.stderr
        if self.logger.level == logging.INFO:
            FNULL = open(os.devnull, 'w')
            stdout = FNULL
            stderr = FNULL
        ret = subprocess.call(["ssh", "-oStrictHostKeyChecking=no",
                               "-oBatchMode=yes",
                               self.hostname,
                               self.remote_appligen,
                               '--prefix=' + self.remote_virtual_appli,
                               '--config=' + self.remote_salome_appli + "/config_appli.xml"],
                              stdout=stdout, stderr=stderr)
        if self.logger.level == logging.INFO:
            FNULL.close()
        return ret

    def addToCatalog(self, catalog):
        m = ET.Element('machine')
        m.set("name", self.name)
        m.set("hostname", self.hostname)
        m.set("appliPath", self.remote_virtual_appli)
        m.set("type", "cluster")
        m.set("protocol", "ssh")
        m.set("iprotocol", "ssh")
        m.set("workingDirectory", self.working_dir)
        m.set("canLaunchBatchJobs", "true")
        m.set("canRunContainers", "false")
        m.set("batch", "slurm")
        m.set("mpi", "no mpi")
        catalog.getroot().append(m)


class ResourceEole(Resource):
    def get_remote_salome_dir(self):
        return "/projets/salome/logiciels/salome/%s" % self.version


class ResourceAster5(Resource):
    def set_defaults(self):
        self.hostname = "aster5"
        self.remote_virtual_appli = "/home/rd-ap-simumeca/salomemeca/appli"
        self.remote_salome_appli = "/home/rd-ap-simumeca/salomemeca/appli"
        self.working_dir = ""

    def prepare(self):
        return 0


if __name__ == '__main__':
    usage = """usage: %prog [options]
Generates a new catalog of resources (file CatalogResources.xml) and configures
SALOME for the current user on the main HPC ressources used at EDF R&D.
You need acces rights to the remote resources in order to perform the
configuration.
If you don't have the required rights for a resource, it will not be added to
the catalog."""
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-v", "--version", help="SALOME version (default: %default)",
                      default=SALOME_VERSION)
    parser.add_option("-q", "--quiet", action="store_true",
                      help="Quiet mode")
    parser.add_option("-f", "--file", dest="filename",
                      help="Catalog Resources file path (default: %default).",
                      default="CatalogResources.xml")
    parser.add_option("-i", "--interactiv", dest="interactiv",
                      action="store_true",
                      help="Interactiv mode - ask confirmation before adding a resource.",
                      default=False)
    parser.add_option("-a", "--always-add", dest="always", action="store_true",
                      help="Add the resource to the catalog even if the remote\
                      configuration fails.",
                      default=False)
    parser.add_option("-n", "--no-remote", dest="noremote", action="store_true",
                      help="Do not try to execute the configuration on the \
                      remote resource.",
                      default=False)
    (options, args) = parser.parse_args()

    logger = logging.getLogger('CREATE_CATALOG')
    formatter = logging.Formatter('%(asctime)s - %(name)s - '
                                  '%(levelname)s - %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    if options.quiet:
        logger.setLevel(logging.INFO)

    catalog = ET.ElementTree(ET.Element('resources'))

    # Ce dictionnaire relie un cluster à une classe
    # Par défaut la classe Resource est renvoyée
    cluster_to_resource = defaultdict(lambda: Resource)
    cluster_to_resource['eole'] = ResourceEole
    cluster_to_resource['aster5'] = ResourceAster5

    for cluster in ["porthos", "athos", "eole", "aster5"]:
        r = cluster_to_resource[cluster](cluster, options.version, logger)
        logger.debug("Processing resource %s." % r.name)
        rep = "y"
        if options.interactiv:
            rep = input("Configure %s? (y|n)" % r.name)
            while rep not in ("y", "n"):
                rep = input("Configure %s? (y|n)" % r.name)
        if rep == "y":
            if options.noremote:
                ok = 0
            else:
                ok = r.prepare()
            logger.debug("Adding resource %s to the catalog." % r.name)
            if ok == 0:
                r.addToCatalog(catalog)
            else:
                logger.warning("%s configuration failed! "
                               "Verify your acces rights to %s and try again."
                               % (r.name, r.hostname))
                if options.always:
                    r.addToCatalog(catalog)
                else:
                    logger.info("%s was not added to the resource catalog."
                                % r.name)
    # pretty_print works only with lxml
    try:
      catalog.write(options.filename, pretty_print=True)
    except:
      catalog.write(options.filename)

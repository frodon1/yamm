#!/bin/bash
# This script is meant to be run in a Salome environnement
# by using $APPLIDIR/runSession for example


reps="\
  V6_5_0"

for rep in $reps; do
  cd .patches/$rep
  ./patch.sh
done
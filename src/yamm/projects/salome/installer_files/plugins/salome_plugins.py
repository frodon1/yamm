from __future__ import print_function
#@PydevCodeAnalysisIgnore
import salome_pluginsmanager

def console_python(context):
  import os
  import salome_version
  ld_library_path= os.getenv("LD_LIBRARY_PATH")
  os.system('xterm -T "SALOME %s - External Python console" -e env LD_LIBRARY_PATH=%s python &'%(salome_version.getVersion(full=True),ld_library_path))

# already defined in share/salome/plugins/gui/demo/salome_plugins.py
def shell_salome(context):
    import os,subprocess
    import salome_version
    version = salome_version.getVersion(full=True)
    kernel_appli_dir = os.environ['KERNEL_ROOT_DIR']
    command = ""
    if os.path.exists("/usr/bin/xterm"):
      command = 'xterm -T "SALOME %s - Shell session" -e %s/salome shell &' % (version, kernel_appli_dir)
    elif os.path.exists("/usr/bin/gnome-terminal"):
      command = 'gnome-terminal -t "SALOME %s - Shell session" -e %s/salome shell &' % (version, kernel_appli_dir)
    else:
      print("Neither xterm nor gnome-terminal is installed.")
    
    if command is not "":
      try:
        subprocess.check_call(command, shell = True)
      except Exception as e:
        print("Error: ",e)


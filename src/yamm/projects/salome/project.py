#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from __future__ import print_function
from __future__ import absolute_import

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 4
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from yamm.core.base import misc
from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.catalog import FrameworkCatalog
from yamm.core.framework.project import FrameworkProject
from yamm.core.framework.project import yamm_command
from yamm.projects.salome import versions as default_versions
from yamm.projects.salome.appli_tasks import CreateAppliTask
from yamm.projects.salome.installer.salome_installer import SalomeInstaller
from yamm.projects.salome.installer.strings_template import SalomeInstallerStringsTemplate
from yamm.projects.salome.softwares import modules as default_modules
from yamm.projects.salome.softwares import prerequisites as default_prerequisites
from yamm.projects.salome.softwares import tools as default_tools
from yamm.projects.salome.string_templates import templates
import copy
import glob
import platform
import shutil
import stat
import subprocess

def get_ftp_server():
  return os.path.join(misc.get_ftp_server(), 'YAMM', 'Public', 'SALOME')

class Project(FrameworkProject):
  """
    .. py:currentmodule:: yamm.projects.salome.Project

    This class provides a project to build the
    SALOME platform.

    :param verbose:  Verbose level of the project.
    :type verbose: int


  """

  def __init__(self, log_name="SALOME", verbose_level=VerboseLevels.INFO):
    FrameworkProject.__init__(self, log_name, verbose_level)
    self.extra_python_env_file_name = "user_extra_prerequisites_env_file"
    self.appli_directory = None
    self.appli_catalogresources = None
    self.stringsTemplate = SalomeInstallerStringsTemplate()
    self.define_salome_options()

#######
#
# Méthodes d'initialisation internes
#
#######

  def init_catalog(self):
    self.logger.debug("Init catalog")
    self.catalog = FrameworkCatalog(name="PROJECT", verbose=self.verbose_level)
    # Import des prerequis
    for module_name in default_prerequisites.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.salome.softwares.prerequisites")
    # Import des tools
    for module_name in default_tools.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.salome.softwares.tools")
    # Import des modules
    for module_name in default_modules.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.salome.softwares.modules")

    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.salome.versions")
    self.logger.debug("End init catalog")

  def define_salome_options(self):
    # Categories du projet SALOME
    self.options.add_category("prerequisite")
    self.options.add_category("tool")
    self.options.add_category("module")

    # Options pour la compilation de la version
    self.options.add_option("cmake_compilation_modules", True, ["global", "category", "software"])
    self.options.add_option("modules_debug_mode", True, ["global", "category", "software"])
    self.options.add_option("tools_debug_mode", True, ["global", "category", "software"])
    self.options.add_option("make_modules_dev_docs", False, ["global", "category", "software"])
    self.options.add_option("salome_prerequisites_env_file", "", ["global"])
    self.options.add_option("salome_context_file", "", ["global"])
    self.options.add_option("salome_sha1_collections_file", "", ["global"])
    self.options.add_option("salome_modules_env_file", "", ["global"])
    self.options.add_option("user_extra_prerequisites_env_file", "", ["global"])
    self.options.add_option("user_extra_modules_env_file", "", ["global"])
    self.options.add_option("user_context_file", "", ["global"])
    self.options.add_option("create_appli", False, ["global"])

    # Options pour les dépôts
    self.options.add_option("occ_username", "", ["global", "category", "software"])
    self.options.add_option("occ_server", "", ["global", "category", "software"])

    # Options pour le dépot des binaires
    self.options.add_option("release_repository", "", ["global"])

#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):

    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "salome"))

    # Framework project default values
    FrameworkProject.set_default_options_values(self, current_command_options)

    # Set default values for salome project
    default_main_software_directory = os.path.join(current_command_options.get_global_option("version_directory"))
    if not current_command_options.is_category_option("module", "main_software_directory"):
      main_software_directory = os.path.join(current_command_options.get_global_option("version_directory"), "modules")
      current_command_options.set_category_option("module", "main_software_directory", main_software_directory)
    module_main_software_directory = current_command_options.get_category_option("module", "main_software_directory")
    if not current_command_options.is_category_option("tool", "main_software_directory"):
      main_software_directory = os.path.join(current_command_options.get_global_option("version_directory"), "tools")
      current_command_options.set_category_option("tool", "main_software_directory", main_software_directory)
    tool_main_software_directory = current_command_options.get_category_option("tool", "main_software_directory")
    if not current_command_options.is_category_option("prerequisite", "main_software_directory"):
      main_software_directory = os.path.join(current_command_options.get_global_option("top_directory"), "prerequisites")
      current_command_options.set_category_option("prerequisite", "main_software_directory", main_software_directory)
    prerequisite_main_software_directory = current_command_options.get_category_option("prerequisite", "main_software_directory")

    if not current_command_options.is_category_option("module", "binary_archive_topdir"):
      current_command_options.set_category_option("module", "binary_archive_topdir", os.path.join(module_main_software_directory, "binary_archives"))
    if not current_command_options.is_category_option("tool", "binary_archive_topdir"):
      current_command_options.set_category_option("tool", "binary_archive_topdir", os.path.join(tool_main_software_directory, "binary_archives"))
    if not current_command_options.is_category_option("prerequisite", "binary_archive_topdir"):
      current_command_options.set_category_option("prerequisite", "binary_archive_topdir", os.path.join(prerequisite_main_software_directory, "binary_archives"))

    if not current_command_options.is_category_option("module", "binary_archive_url"):
      current_command_options.set_category_option("module", "binary_archive_url", os.path.join(current_command_options.get_category_option("module", "binary_archive_topdir"), "%(name)s.tgz"))
    if not current_command_options.is_category_option("prerequisite", "binary_archive_url"):
      current_command_options.set_category_option("prerequisite", "binary_archive_url", os.path.join(current_command_options.get_category_option("prerequisite", "binary_archive_topdir"), "%(name)s.tgz"))
    if not current_command_options.is_category_option("tool", "binary_archive_url"):
      current_command_options.set_category_option("tool", "binary_archive_url", os.path.join(current_command_options.get_category_option("tool", "binary_archive_topdir"), "%(name)s.tgz"))

    if current_command_options.get_global_option("cmake_compilation_modules") is None:
      current_command_options.set_global_option("cmake_compilation_modules", False)
    if current_command_options.get_global_option("modules_debug_mode") is None:
      current_command_options.set_global_option("modules_debug_mode", False)
    if current_command_options.get_global_option("make_modules_dev_docs") is None:
      current_command_options.set_global_option("make_modules_dev_docs", False)
    if current_command_options.get_global_option("tools_debug_mode") is None:
      current_command_options.set_global_option("tools_debug_mode", False)

    if not current_command_options.is_category_option("module", "software_only_list"):
      current_command_options.set_category_option("module", "software_only_list", [])
    if not current_command_options.is_category_option("prerequisite", "software_only_list"):
      current_command_options.set_category_option("prerequisite", "software_only_list", [])
    if not current_command_options.is_category_option("tool", "software_only_list"):
      current_command_options.set_category_option("tool", "software_only_list", [])

    if not current_command_options.get_global_option("salome_prerequisites_env_file"):
      current_command_options.set_global_option("salome_prerequisites_env_file", "")
    if not current_command_options.get_global_option("salome_context_file"):
      current_command_options.set_global_option("salome_context_file", "")
    if not current_command_options.get_global_option("salome_modules_env_file"):
      current_command_options.set_global_option("salome_modules_env_file", "")
    if not current_command_options.get_global_option("user_extra_prerequisites_env_file"):
      current_command_options.set_global_option("user_extra_prerequisites_env_file", "")
    if not current_command_options.get_global_option("user_context_file"):
      current_command_options.set_global_option("user_context_file", "")
    if not current_command_options.get_global_option("user_extra_modules_env_file"):
      current_command_options.set_global_option("user_extra_modules_env_file", "")
    if current_command_options.get_global_option("create_appli") is None:
      current_command_options.set_global_option("create_appli", False)

    if not current_command_options.get_global_option("release_repository"):
      current_command_options.set_global_option("release_repository", "")

    if not current_command_options.is_category_option("prerequisite", "source_type"):
      current_command_options.set_category_option("prerequisite", "source_type", "archive")
    if not current_command_options.is_category_option("module", "source_type"):
      current_command_options.set_category_option("module", "source_type", "remote")
    if not current_command_options.is_category_option("tool", "source_type"):
      current_command_options.set_category_option("tool", "source_type", "remote")

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):

    current_command_options.check_abs_path('main_software_directory',
                                           category='prerequisite',
                                           logger=self.logger)
    for category in ['module', 'tool']:
      if current_command_options.is_category_option(category,
                                                    'main_software_directory'):
        current_command_options.check_abs_path('main_software_directory',
                                               category=category,
                                               logger=self.logger)

    for category in ['prerequisite', 'module', 'tool']:
      current_command_options.check_allowed_value('source_type',
                                       ["archive", "remote"],
                                       category=category,
                                       logger=self.logger)

    for option in ['salome_prerequisites_env_file',
                   'salome_context_file',
                   'salome_modules_env_file',
                   'user_extra_prerequisites_env_file',
                   'user_context_file',
                   'user_extra_modules_env_file']:
      if current_command_options.get_global_option(option) != "":
        current_command_options.check_abs_path(option, logger=self.logger)


#######
#
# Méthodes d'introspection/affichage
#
#######

  def compare_with_version(self, other, category=""):
    if category not in ("prerequisite", "tool", "module"):
      self.logger.warning("Category must be 'prerequisite', 'tool' or  'module'")
      return
    FrameworkProject.compare_with_version(self, other, category)


  def print_directories(self, current_command_options, max_width=-1):
    """
    This method prints directories values of the current command.

    Do not use it by yourself. Instead use
    :py:meth:`~yamm.core.framework.project.FrameworkProject.print_configuration`
    command.
    """

    prereq_main_software_dir_label = 'Prerequisistes directory'
    module_main_software_dir_label = 'Modules directory'
    tool_main_software_dir_label = 'Tools directory'
    version_dir_label = 'Version directory'

    # Calculating max width of labels
    labels = []
    labels.append(prereq_main_software_dir_label)
    labels.append(module_main_software_dir_label)
    labels.append(tool_main_software_dir_label)
    labels.append(version_dir_label)
    ljust_width = max([len(x) for x in labels])
    ljust_width = max(ljust_width, max_width)

    ljust_width = FrameworkProject.print_directories(self,
                                                     current_command_options,
                                                     ljust_width)

    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'prerequisite',
                  label=prereq_main_software_dir_label, width=ljust_width)
    current_command_options.print_global_option(
                  self.logger, 'version_directory',
                  label=version_dir_label, width=ljust_width)
    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'module',
                  label=module_main_software_dir_label, width=ljust_width)
    current_command_options.print_category_option(
                  self.logger, 'main_software_directory', 'tool',
                  label=tool_main_software_dir_label, width=ljust_width)
    return ljust_width

  def print_general_configuration(self, current_cmd_opts, max_width=-1):
    """
      This method prints general configuration values of the current command.

      Do not use it by yourself. Instead use :py:meth:`~yamm.core.framework.project.FrameworkProject.print_configuration` command.
    """

    return FrameworkProject.print_general_configuration(self,
                                                        current_cmd_opts,
                                                        max_width)

  def print_version(self, version_object, current_command_python_version,
                    max_width=-1):
    """
      This method prints specific version values of the current command.

      Do not use it by yourself. Instead use :py:meth:`~yamm.core.framework.project.FrameworkProject.print_configuration` command.
    """
    salome_label = 'Salome Git branch/tag'
    ljust_width = max(len(salome_label), max_width)
    ljust_width = FrameworkProject.print_version(self,
                                                 version_object,
                                                 current_command_python_version,
                                                 ljust_width)
    self.logger.info('{0} : {1}'.format(salome_label.ljust(ljust_width),
                                        version_object.get_salome_tag()))
    return ljust_width

#######
#
# Méthodes internes pour les commandes
#
#######

  def create_env_files(self, delete=True, installer=False, nosubdir=False):
    FrameworkProject.create_env_files(self, delete)
    self.create_context_file(delete, installer, nosubdir)
    prereq_file = self.create_prerequisites_file(delete, installer, nosubdir)
    module_file = self.create_modules_file(delete)

    files = []
    files.append(os.path.basename(prereq_file))
    files.append(os.path.basename(module_file))
    listing_file = os.path.join(self.current_command_options.get_global_option("version_directory"),
                                ".salome_env_files")
    with open(listing_file, 'w') as l_file:
      l_file.writelines("\n".join(files))
      l_file.write("\n")

  def get_env_files(self):
    """
    This method returns the list of env files of the project
    """
    prereq_file = self.current_command_options.get_global_option("salome_prerequisites_env_file")
    module_file = self.current_command_options.get_global_option("salome_modules_env_file")
    return [prereq_file, module_file]
  #

  # return a tuple of booleans (has_prereq, has_tools)
  def __areSoftwaresPrerequisitesOrTools(self, softwares):
    has_mod = False
    has_prereq = False
    has_tools = False
    for software in softwares:
      if software.get_type() == "module":
        has_mod = True
      if software.get_type() == "prerequisite":
        has_prereq = True
      if software.get_type() == "tool":
        has_tools = True
      if has_prereq and has_tools and has_mod:
        break
    return (has_mod, has_prereq, has_tools)
  #

  def create_context_file(self, delete=True, installer=False, nosubdir=False):
    context_file = self.current_command_options.get_global_option("salome_context_file")
    self.logger.info("Creating context file : %s" % context_file)
    # check directory and file existence
    context_file_directory = os.path.dirname(context_file)
    if os.path.exists(context_file):
      try:
        if delete:
          os.remove(context_file)
        else:
          return
      except:
        self.logger.exception("Error in deleting context file: %s" % context_file)

    try:
      if not os.path.isdir(context_file_directory):
        os.makedirs(context_file_directory)
    except:
      self.logger.error("Creation of directory for context file failed ! Directory was: %s" % context_file_directory)

    # Create file
    with open(context_file, "w") as ctx_file:
      ctx_file.write('[SALOME Configuration]\n')

    # Adding user_context_file
    user_context_file = self.current_command_options.get_global_option("user_context_file")
    if user_context_file != "" and user_context_file != context_file:
      if not os.path.exists(user_context_file):
        self.logger.error("User context file does not exist: %s" % user_context_file)
      lines = []
      with open(user_context_file) as f:
        lines = f.readlines()
      with open(context_file, 'a') as f:
        f.writelines(lines)

    with open(context_file, 'a') as ctx_file:
      if not installer:
        ctx_file.write(templates.salome_prerequisite_file_header_configuration_template.substitute(root_salome=self.current_command_options.get_global_option("version_directory")))
      else:
        ctx_file.write(self.stringsTemplate.get_installer_salome_context_header())
        has_mod, has_prereq, has_tools = self.__areSoftwaresPrerequisitesOrTools(self.current_command_softwares)
        if has_mod:
          ctx_file.write(self.stringsTemplate.get_installer_salome_context_header_check_modules())
        if has_prereq:
          ctx_file.write(self.stringsTemplate.get_installer_salome_context_header_check_prereq(nosubdir))
        if has_tools:
          ctx_file.write(self.stringsTemplate.get_installer_salome_context_header_check_tools(nosubdir))

    return context_file

  def create_prerequisites_file(self, delete=True, installer=False, nosubdir=False):
    prerequisites_file = self.current_command_options.get_global_option("salome_prerequisites_env_file")
    self.logger.info("Creating environment file : %s" % prerequisites_file)
    prerequisites_file_directory = os.path.dirname(prerequisites_file)
    if os.path.exists(prerequisites_file):
      try:
        if delete:
          os.remove(prerequisites_file)
        else:
          return
      except:
        self.logger.exception("Error in deleting prerequisites file: %s" % prerequisites_file)

    try:
      if not os.path.isdir(prerequisites_file_directory):
        os.makedirs(prerequisites_file_directory)
    except:
      self.logger.error("Creation of directory for prerequisites file failed ! Directory was: %s" % prerequisites_file_directory)

    # Starting prerequis file
    with open(prerequisites_file, 'w') as pre_file:
      pre_file.write("#!/bin/bash\n")
      pre_file.write("# %s %s - Environnement file for the prerequisites and tools\n" % (self.name, self.version))

    # Adding user_extra_prerequisites_env_file
    user_extra_prerequisites_env_file = self.current_command_options.get_global_option("user_extra_prerequisites_env_file")
    if user_extra_prerequisites_env_file != "" and user_extra_prerequisites_env_file != prerequisites_file:
      if not os.path.exists(user_extra_prerequisites_env_file):
        self.logger.error("User extra prerequisites file does not exist: %s" % user_extra_prerequisites_env_file)
      lines = []
      with open(user_extra_prerequisites_env_file) as f:
        lines = f.readlines()
      with open(prerequisites_file, 'a') as f:
        f.writelines(lines)

    # Continuing prerequisites file
    with open(prerequisites_file, 'a') as pre_file:
      if not installer:
        pre_file.write(templates.salome_prerequisite_file_header_template.substitute(root_salome=self.current_command_options.get_global_option("version_directory")))
      else:
        pre_file.write(self.stringsTemplate.get_installer_salome_prerequisite_header())
        has_mod, has_prereq, has_tools = self.__areSoftwaresPrerequisitesOrTools(self.current_command_softwares)
        if has_mod:
          pre_file.write(self.stringsTemplate.get_installer_salome_prerequisite_header_check_modules(nosubdir))
        if has_prereq:
          pre_file.write(self.stringsTemplate.get_installer_salome_prerequisite_header_check_prereq(nosubdir))
        if has_tools:
          pre_file.write(self.stringsTemplate.get_installer_salome_prerequisite_header_check_tools(nosubdir))
    return prerequisites_file

  def create_modules_file(self, delete=True):
    modules_file = self.current_command_options.get_global_option("salome_modules_env_file")
    self.logger.info("Creating environment file : %s" % modules_file)
    modules_file_directory = os.path.dirname(modules_file)
    if os.path.exists(modules_file):
      try:
        if delete:
          os.remove(modules_file)
        else:
          return
      except:
        self.logger.exception("Error in deleting modules file %s" % modules_file)

    try:
      if not os.path.isdir(modules_file_directory):
        os.makedirs(modules_file_directory)
    except:
      self.logger.error("Creation of directory for module file failed ! Directory was: %s" % modules_file_directory)

    # Starting modules file
    mode = 'w' if delete else 'a'
    with open(modules_file, mode) as mod_file:
      if mode == 'w':
        mod_file.write("#!/bin/bash\n")
      mod_file.write("# SALOME %s - Environnement file for the modules\n" % self.version)

    # Adding user_extra_modules_env_file
    user_extra_modules_env_file = self.current_command_options.get_global_option("user_extra_modules_env_file")
    if user_extra_modules_env_file != "" and user_extra_modules_env_file != modules_file:
      if not os.path.exists(user_extra_modules_env_file):
        self.logger.error("User extra module file does not exist: %s" % user_extra_modules_env_file)
      lines = []
      with open(user_extra_modules_env_file) as f:
        lines = f.readlines()
      with open(modules_file, 'a') as f:
        f.writelines(lines)

    return modules_file

  def update_configuration(self):
    # Platform
    misc.misc_platform = self.current_command_options.get_global_option("platform")
    # Défaut à EDF
    # TODO Faire une option pour savoir sur quel site yamm est executé (EDF ou autre)
    # if self.current_command_options.get_global_option("occ_server") == "":
    #  self.current_command_options.set_global_option("occ_server", "git.salome-platform.org")
    if self.current_command_options.get_category_option("prerequisite", "archive_remote_address") == "":
      self.current_command_options.set_category_option("prerequisite", "archive_remote_address", os.path.join(get_ftp_server(), "sources/prerequis"))
    if self.current_command_options.get_category_option("tool", "archive_remote_address") == "":
      self.current_command_options.set_category_option("tool", "archive_remote_address", os.path.join(get_ftp_server(), "sources/tools"))
    if self.current_command_options.get_category_option("module", "archive_remote_address") == "":
      self.current_command_options.set_category_option("module", "archive_remote_address", os.path.join(get_ftp_server(), "sources/modules"))

    if self.current_command_options.get_global_option("release_repository") == "":
      self.current_command_options.set_global_option("release_repository", "/Releases/")

    # Répertoires dépendants de la version
    if not self.current_command_options.get_global_option("salome_prerequisites_env_file"):
      self.current_command_options.set_global_option("salome_prerequisites_env_file", os.path.join(self.current_command_options.get_global_option("version_directory"), "salome_prerequisites.sh"))
    if not self.current_command_options.get_global_option("salome_context_file"):
      self.current_command_options.set_global_option("salome_context_file", os.path.join(self.current_command_options.get_global_option("version_directory"), "salome_context.cfg"))
    if not self.current_command_options.get_global_option("salome_sha1_collections_file"):
      self.current_command_options.set_global_option("salome_sha1_collections_file", os.path.join(self.current_command_options.get_global_option("version_directory"), "sha1_collections.txt"))
    if not self.current_command_options.get_global_option("salome_modules_env_file"):
      self.current_command_options.set_global_option("salome_modules_env_file", os.path.join(self.current_command_options.get_global_option("version_directory"), "salome_modules.sh"))

  def software_only_list_hook(self):
    if self.current_command_options.get_category_option("prerequisite", "software_only_list") != []:
      prerequisite_only_list = self.current_command_options.get_category_option("prerequisite", "software_only_list")
      iter_list = copy.copy(self.current_command_softwares)
      for current_software in iter_list:
        if current_software.get_type() == "prerequisite":
          if not current_software.name in prerequisite_only_list:
            self.current_command_version_object.remove_software(current_software.name)
            self.current_command_softwares.remove(current_software)
    if self.current_command_options.get_category_option("module", "software_only_list") != []:
      module_only_list = self.current_command_options.get_category_option("module", "software_only_list")
      iter_list = copy.copy(self.current_command_softwares)
      for current_software in iter_list:
        if current_software.get_type() == "module":
          if not current_software.name in module_only_list:
            self.current_command_version_object.remove_software(current_software.name)
            self.current_command_softwares.remove(current_software)
    if self.current_command_options.get_category_option("tool", "software_only_list") != []:
      tool_only_list = self.current_command_options.get_category_option("tool", "software_only_list")
      iter_list = copy.copy(self.current_command_softwares)
      for current_software in iter_list:
        if current_software.get_type() == "tool":
          if not current_software.name in tool_only_list:
            self.current_command_version_object.remove_software(current_software.name)
            self.current_command_softwares.remove(current_software)

  def update_env_files_hook(self, software_list):
    update_name = software_list[0]
    if len(software_list) > 1:
      for soft_name in software_list[1:]:
        update_name += "_" + soft_name
    self.current_command_options.set_global_option("salome_prerequisites_env_file", os.path.join(self.current_command_options.get_global_option("version_directory"), "salome_prerequisites_update_%s.sh" % update_name))
    self.current_command_options.set_global_option("salome_modules_env_file", os.path.join(self.current_command_options.get_global_option("version_directory"), "salome_modules_update_%s.sh" % update_name))
    self.create_env_files()

  def get_version_custom_tasks(self):
    tasks = []
    if self.current_command_options.get_global_option("create_appli"):
      if self.appli_directory is None:
        appli_name = "appli_" + self.current_command_version_object.name
        appli_topdirectory = self.current_command_options.get_global_option("version_directory")
        self.appli_directory = os.path.join(appli_topdirectory, appli_name)
      tasks.append(CreateAppliTask(self.appli_directory, self.appli_catalogresources))
    for test_suite in self.current_command_version_object.test_suite_list:
      test_suite.appli_directory = self.appli_directory
    return tasks

  def add_software_to_env_files_end_hook(self, executor_software):
    software = self.current_map_name_to_softwares[executor_software.name]
    if not software.isArtefact():
      if not self.is_build_only_software(executor_software.name) or \
          not self.current_command_options.get_global_option("separate_dependencies"):
        with open(self.current_command_options.get_global_option("salome_prerequisites_env_file"), "a") as pre_file:
          pre_file.write(software.get_prerequisite_str())

        # Add software in the context file
        with open(self.current_command_options.get_global_option("salome_context_file"), "a") as ctx_file:
          ctx_file.write(software.get_configuration_str())

      # Add software in the modules file
      with open(self.current_command_options.get_global_option("salome_modules_env_file"), "a") as mod_file:
        mod_file.write(software.get_module_str())

#######
#
# Méthodes contenant les commandes
#
#######

  def _create_appli(self, appli_topdirectory="", appli_name="", appli_catalogresources="", generates_env_files=True):
    """
      This command generates a SALOME application with the current command configuration.

      :param string appli_topdirectory: Defines the application top directory.
      :param string appli_name: Defines the application name.
      :param string appli_catalogresources: Defines the catalog resources file. It will be copied into the application directory.
      :param bool generates_env_files: If True, generates SALOME environnement files.
    """

    # Prepare execution with software_mode = "nothing"
    software_mode = "nothing"
    for software in self.current_command_softwares:
      # Step 1: configuration du logiciel
      software.set_command_env(self.current_command_softwares,
                               self.current_command_options,
                               software_mode,
                               self.current_command_version_object,
                               self.current_command_python_version)
      software.create_tasks()
      # Step 2: ajout du logiciel dans l'executeur
      executor_software = software.get_executor_software()
      self.current_command_executor.add_software(executor_software)
      self.current_map_name_to_softwares[software.name] = software
    # execution -> To be sure to create a correct prerequis file
    if generates_env_files:
      self.create_env_files()
      self.current_command_executor.add_hooks(self.add_software_to_env_files_begin_hook,
                                              self.add_software_to_env_files_end_hook)

    # Get parameters for create_appli task
    if appli_name == "":
      appli_name = "appli_" + self.current_command_version_object.name
    if appli_topdirectory == "":
      appli_topdirectory = self.current_command_options.get_global_option("version_directory")
    self.appli_directory = os.path.join(appli_topdirectory, appli_name)
    if appli_catalogresources != "":
      self.appli_catalogresources = appli_catalogresources

    # Add create_appli task to the version
    self.current_command_options.set_global_option("create_appli", True)
    self.current_command_version_object.add_tasks(self.get_version_custom_tasks())

    ret = self.execute_command()
    if ret:
      # check generated env files
      self.check_generated_env_files()

      # This command allows to use several commands one after the others in a single yamm command
      self.reset_command(endlog=False)

    return ret

  def _get_appli_launcher(self):
      return 'salome'

  def _run_appli(self, appli_topdirectory="", appli_name="", appli_options=""):
    """
      This command starts a SALOME application. By default, it uses the default application
      generated by the command :py:meth:`~yamm.projects.salome.project.Project.create_appli`. An another
      SALOME application could be choosed with the command optional arguments.
      Additional options can be provided to the salome command.
      The command returns when the SALOME application is shutdown.

      :param string appli_topdirectory: Defines the application top directory.
      :param string appli_name: Defines the application name.
      :param string appli_options: Defines the application options.
    """
    # Create SALOME Application config file
    if appli_name == "":
      appli_name = "appli_" + self.current_command_version_object.name
    if appli_topdirectory == "":
      appli_topdirectory = self.current_command_options.get_global_option("version_directory")
    appli_path = os.path.join(appli_topdirectory, appli_name)

    # Test appli
    if os.path.exists(appli_path):
      if not os.path.isdir(appli_path):
        self.logger.error("Cannot find salome application directory: %s" % appli_path)
    if not os.path.exists(os.path.join(appli_path, "salome")):
        self.logger.error("Cannot find salome: %s" % os.path.join(appli_path, "salome"))

    cmd = os.path.join(appli_path, "%s %s" % (self._get_appli_launcher(), appli_options))
    try:
      retcode = subprocess.call(cmd, shell=True)
      if retcode != 0:
        self.logger.error("%s command failed" % cmd)
    except OSError as e:
      self.logger.exception("command was: %s, exception was: %s" % (cmd, e))
    return retcode == 0

#######
#
# Méthodes contenant les commandes
#
#######

  @yamm_command("Creation of a SALOME application")
  def create_appli(self, appli_topdirectory="", appli_name="", appli_catalogresources="", generates_env_files=True):
    ret = self._create_appli(appli_topdirectory, appli_name, appli_catalogresources, generates_env_files)
    if ret:
      # Final step: collect sha1s of softwares
      ret = self._make(executor_mode="collect_sha1")
    return ret

  @yamm_command("Launch of SALOME application")
  def run_appli(self, appli_topdirectory="", appli_name="", appli_options=""):
    """
      This command starts a SALOME application. By default, it uses the default application
      generated by the command :py:meth:`~yamm.projects.salome.project.Project.create_appli`. An another
      SALOME application could be choosed with the command optional arguments.
      Additional options can be provided to the salome command.
      The command returns when the SALOME application is shutdown.

      :param string appli_topdirectory: Defines the application top directory.
      :param string appli_name: Defines the application name.
      :param string appli_options: Defines the application options.
    """
    return self._run_appli(appli_topdirectory, appli_name, appli_options)

  @yamm_command("Creation of dependency graph (dot format)")
  def plot_dot(self, label=None):
    current_command_softwares = self.current_command_softwares

    removed_graph = """\n subgraph cluster_removed {%removed_nodes
    label = "Softwares not compiled";
    }
    """
    removed_graph = misc.PercentTemplate(removed_graph)

    prereq_graph = """\n subgraph cluster_prerequisites {%prereq_nodes
    color=red;
    label = "Prerequisites";
    }
    """
    prereq_graph = misc.PercentTemplate(prereq_graph)

    tools_graph = """\n subgraph cluster_tools {%tools_nodes
    color=green;
    label = "Tools";
    }
    """
    tools_graph = misc.PercentTemplate(tools_graph)

    # style=filled;%modules_nodes
    modules_graph = """\n subgraph cluster_modules {%modules_nodes
    color=blue;
    label = "Modules";
    }
    """
    modules_graph = misc.PercentTemplate(modules_graph)

    plot = "digraph Project_dependency_diagram {"
    plot += "\n rankdir=LR; ratio = fill;"
    if label is None:
      plot += "\n label = \"Global dependency diagram of SALOME softwares (version %s)\";" % self.version
    else:
      plot += "\n label = \"Dependency diagram of %s software (SALOME version %s)\";" % (label, self.version)

    soft_types = {"prerequisite":[],
                  "tool":[],
                  "module":[],
                  "other":[]}
    txt_nodes = {"prerequisite":"",
                 "tool":"",
                 "module":"",
                 "other":"",
                 "removed":""}
    soft_added = {"prerequisite":[],
                  "tool":[],
                  "module":[],
                  "other":[],
                  "removed":[]}

    current_command_softwares_names = [soft.name for soft in current_command_softwares]

    for soft in current_command_softwares:
      know_type = False
      if soft.get_type() in soft_types:
        soft_types[soft.get_type()].append(soft.name)
        know_type = True
      else:
        soft_types["other"] = soft.name

      if soft.get_dependencies() == []:
        txt = "\n    %s;" % soft.name
        if know_type:
          txt_nodes[soft.get_type()] += txt
        else:
          txt_nodes["other"] += txt
      else:
        for dep_name in soft.get_dependencies():
          txt = "\n    %s -> %s;" % (soft.name, dep_name)
          if know_type and dep_name in soft_types[soft.get_type()]:
            txt_nodes[soft.get_type()] += txt
            soft_added[soft.get_type()].append(dep_name)
          else:
            txt_nodes["other"] += txt
            soft_added["other"].append(dep_name)

          # Cas des softwares retirés de la version
          if dep_name not in current_command_softwares_names:
            txt_nodes["removed"] += "\n    %s;" % dep_name
            soft_added["removed"].append(dep_name)

    for soft in current_command_softwares:
      if soft.name not in soft_added[soft.get_type()]:
        if soft.get_type() in soft_types:
          txt_nodes[soft.get_type()] += "\n    %s;" % soft.name
        else:
          txt_nodes["other"] += "\n    %s;" % soft.name

    plot += "\n"
    plot += prereq_graph.substitute(prereq_nodes=txt_nodes["prerequisite"])
    plot += tools_graph.substitute(tools_nodes=txt_nodes["tool"])
    plot += modules_graph.substitute(modules_nodes=txt_nodes["module"])
    plot += removed_graph.substitute(removed_nodes=txt_nodes["removed"])
    plot += "\n"
    plot += txt_nodes["other"]
    plot += "\n}"
    return plot

  @yamm_command("Creation of SALOME post-install file")
  def create_post_install_file(self, directory, software_install_dirs=None):
    """
    This method is meant to be used for debug only. It allows to only generate the post install script
    in a chosen directory

    :param string directory: The directory where you want to generate the post install script
    """
    salome_installer = self.get_installer_class()(name="debug",
                                       logger=self.logger,
                                       topdirectory=directory,
                                       softwares=self.current_command_softwares,
                                       map_name_to_softwares=self.current_map_name_to_softwares,
                                       python_version=self.current_command_python_version,
                                       executor=self.current_command_executor,
                                       software_install_dirs=software_install_dirs)

    self.configure_installer(salome_installer)
    salome_installer.writePostInstallFile()

#######
#
# Installer commands
#
#######

  def configure_installer(self, installer):
    # Command parameters must be reset here
    self.reset_command(command_name="configure_installer", endlog=False)
    installer.softwares = self.current_command_softwares
    installer.map_name_to_softwares = self.current_map_name_to_softwares
    installer.python_version = self.current_command_python_version
    installer.executor = self.current_command_executor

  def get_installer_class(self):
    return SalomeInstaller

  def get_installer_name(self):
    return "Salome-" + self.version + "-" + platform.machine()

  @yamm_command("Creation of SALOME movable installer")
  def create_movable_installer(self, installer_topdirectory="", distrib="",
              tgz=True, runnable=True, delete=True, universal=False,
              public=False, final_custom_command="", additional_commands=[],
              gnu_tar_with_custom_compress_programm="",
              default_install_dir="", default_appli_dir="", default_exec="",
              default_name="", default_icon_path="", default_contact="", default_config_file="",
              software_install_dirs=None, default_temporary_path="", nosubdir=False,
              yamm_sha1_suffix=False):
    """
    This method creates a movable archive of Salome. By default the archive is made runnable.
    It is possible to generate only the installer directory structure, or the archive tgz file.
    By default the previous directory and archive or installer files are deleted.
    It the distribution is etch, then some specific libraries are added to make the installer "universal".

    :param string installer_topdirectory: Defines the installer directory, including the installer name.
    :param string distrib: Defines the Debian distribution on which the installer is being created.
    :param bool tgz: If True, generates the archive file.
    :param bool runnable: If True, generates the installer file.
    :param bool delete: If True, deletes the existing installer directory, archive and installer files.
    :param bool universal: If True, adds specific libraries in order to make the installer be universal.
    :param bool public: If True, execute specific packaging tasks for public distribution of the installer (namely there is no mention of eficas catalogs). The universal parameter is also set to True
    :param string final_custom_command: Optionnal final commands to be done by the installer when installed.
    :param list of string additional_commands: Optionnal commands to be done during the installer construction, before the archive is created.
    :param string gnu_tar_with_custom_compress_programm:  if not empty gnu tar is used to create the tgz. The programm given can be gzip for standard compression or another
                                                          like pigz for a multi-threaded compression. This mostly useful for quicker debug
    :param string default_install_dir: Defines the directory where Salome will be installed (default to $HOME/salome)
    :param string default_appli_dir: Defines the directory where the application will be installed (default to $HOME/salome/appli_[VERSION]).
    :param string default_exec: Defines the name of the executable in the application (default to salome)
    :param string default_name: Defines the name of the application (default to Salome)
    :param string default_icon_path: Defines the path if the application icon (default to GUI_ROOT_DIR/share/salome/resources/gui/icon_about.png).
    :param string default_contact: Defines the address of the contact (default to https://salome.der.edf.fr or http://salome-platform.org).
    :param string default_config_file: Defines the name of the Salome config file in $HOME/.config/salome (default to SalomeApprc.[VERSION]).
    :param dictionnary software_install_dirs: Optionnal install directories of softwares, relative to the top directory of installer
    :param string default_temporary_path: Defines a new temporary directory (default to /tmp)
    :param bool nosubdir: if True, do not use the software type subdirectories
    """

    # Step 0: create binary archives
    if self.current_command_options.get_global_option("separate_dependencies"):
      movable_installer_software_list = [soft for soft in self.current_command_softwares if not self.is_build_only_software(soft.name)]
    else:
      movable_installer_software_list = self.current_command_softwares
    if software_install_dirs:
      for k, w in software_install_dirs.items():  
       for x in movable_installer_software_list:
          if x.name == k and w == '':
            movable_installer_software_list.remove(x)
    ret = self._make(software_list=[s.name for s in movable_installer_software_list],
                     executor_mode="create_binary_archive")
    if not ret:
      return ""

    # Step 1: init variables
    if not tgz:
      runnable = False

    # The name of the installer depends on the operating system
    if universal or distrib == "":
      salome_installer_name = self.get_installer_name()
    else:
      salome_installer_name = "Salome-" + self.version + "-" + distrib 

    if yamm_sha1_suffix:
        if installer_topdirectory:
            installer_topdirectory += '_' + misc.get_current_yamm_sha1_short()
        else:
            salome_installer_name += '_' + misc.get_current_yamm_sha1_short()

    # Calculates the installer directory full path

    movable_installer_topdirectory = os.path.join(self.current_command_options.get_global_option("version_directory"), "movable_installer")
    if installer_topdirectory == "":
      installer_topdirectory = os.path.join(movable_installer_topdirectory, salome_installer_name)
    else:
      salome_installer_name = os.path.basename(installer_topdirectory)

    # Defines the new environnement files and save the current ones
    save_salome_prerequisites_env_file = ""
    save_salome_context_file = ""
    save_salome_sha1_collections_file = ""
    save_salome_modules_env_file = ""
    bn_prerequisites_env_file = "salome_prerequisites.sh"
    bn_context_file = "salome_context.cfg"
    bn_sha1_collections_file = "sha1_collections.txt"
    bn_modules_env_file = "salome_modules.sh"

    if self.current_command_options.get_global_option("salome_prerequisites_env_file"):
      save_salome_prerequisites_env_file = self.current_command_options.get_global_option("salome_prerequisites_env_file")
      bn_prerequisites_env_file = os.path.basename(save_salome_prerequisites_env_file)
    if self.current_command_options.get_global_option("salome_context_file"):
      save_salome_context_file = self.current_command_options.get_global_option("salome_context_file")
      bn_context_file = os.path.basename(save_salome_context_file)
    if self.current_command_options.get_global_option("salome_sha1_collections_file"):
      save_salome_sha1_collections_file = self.current_command_options.get_global_option("salome_sha1_collections_file")
      bn_sha1_collections_file = os.path.basename(save_salome_sha1_collections_file)
    if self.current_command_options.get_global_option("salome_modules_env_file"):
      save_salome_modules_env_file = self.current_command_options.get_global_option("salome_modules_env_file")
      bn_modules_env_file = os.path.basename(save_salome_modules_env_file)

    self.current_command_options.set_global_option("salome_prerequisites_env_file",
                                                   os.path.join(installer_topdirectory, bn_prerequisites_env_file))
    self.current_command_options.set_global_option("salome_context_file",
                                                   os.path.join(installer_topdirectory, bn_context_file))
    self.current_command_options.set_global_option("salome_sha1_collections_file",
                                                   os.path.join(installer_topdirectory, bn_sha1_collections_file))
    self.current_command_options.set_global_option("salome_modules_env_file",
                                                   os.path.join(installer_topdirectory, bn_modules_env_file))

    # Deletes the previous installer files
    if delete:
      if os.path.exists(installer_topdirectory + ".run"):
        os.remove(installer_topdirectory + ".run")
      if os.path.exists(installer_topdirectory + ".run.sha1"):
        os.remove(installer_topdirectory + ".run.sha1")
      if os.path.exists(installer_topdirectory + ".tgz"):
        os.remove(installer_topdirectory + ".tgz")
      if os.path.exists(os.path.join(installer_topdirectory, "decompress")):
        os.remove(os.path.join(installer_topdirectory, "decompress"))
      if os.path.exists(installer_topdirectory):
        try:
          misc.fast_delete(installer_topdirectory)
        except:
          self.logger.exception("Error in deleting directory" + installer_topdirectory)

    # Add option for specific env files

    # Step 2: create the environment files
    self.create_env_files(installer=True, nosubdir=nosubdir)

    # Step 3: Create installer instance
    salome_installer = self.get_installer_class()(name=salome_installer_name,
                           logger=self.logger,
                           topdirectory=installer_topdirectory,
                           softwares=movable_installer_software_list,
                           executor=self.current_command_executor,
                           delete=delete,
                           runnable=runnable,
                           distrib=distrib,
                           tgz=tgz,
                           universal=universal,
                           public=public,
                           final_custom_command=final_custom_command,
                           additional_commands=additional_commands,
                           gnu_tar_with_custom_compress_programm=gnu_tar_with_custom_compress_programm,
                           default_install_dir=default_install_dir,
                           default_appli_dir=default_appli_dir,
                           default_exec=default_exec,
                           default_name=default_name,
                           default_icon_path=default_icon_path,
                           default_contact=default_contact,
                           default_config_file=default_config_file,
                           software_install_dirs=software_install_dirs,
                           default_temporary_path=default_temporary_path,
                           nosubdir=nosubdir)

    ret = salome_installer.execute()

    # Step 7: restore user env
    if save_salome_prerequisites_env_file != "":
      self.current_command_options.set_global_option("salome_prerequisites_env_file", save_salome_prerequisites_env_file)
    else:
      self.current_command_options.remove_global_option_value("salome_prerequisites_env_file")
    if save_salome_context_file != "":
      self.current_command_options.set_global_option("salome_context_file", save_salome_context_file)
    else:
      self.current_command_options.remove_global_option_value("salome_context_file")
    if save_salome_sha1_collections_file != "":
      self.current_command_options.set_global_option("salome_sha1_collections_file", save_salome_sha1_collections_file)
    else:
      self.current_command_options.remove_global_option_value("salome_sha1_collections_file")
    if save_salome_modules_env_file != "":
      self.current_command_options.set_global_option("salome_modules_env_file", save_salome_modules_env_file)
    else:
      self.current_command_options.remove_global_option_value("salome_modules_env_file")

    return installer_topdirectory

#######
#
# Publication commands
#
#######

  @yamm_command("Creation of unique documentation directory")
  def create_modules_doc_directory(self):
    """
      This command creates a unique doc directory for all SALOME modules. This is useful for the online publication of the SALOME documentation."
    """
    # Check software
    command_current_software_list = []
    for current_software in self.current_command_softwares:
      if current_software.get_type() == "module":
        command_current_software_list.append(current_software)

    doc_distrib_directory = os.path.join(self.current_command_options.get_category_option("module", "main_software_directory"), "doc")

    software_mode = "nothing"
    for software in command_current_software_list:
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      software_install_directory = software.install_directory
      if software_install_directory == "":
        self.logger.error("The software install directory is empty")

      software_doc_directory = os.path.join(software_install_directory, "share", "doc", "salome")
      doc_install_directory = os.path.join(doc_distrib_directory, software.name)

      # Remove the install directory if it exists
      if os.path.exists(doc_install_directory):
        try:
          misc.fast_delete(doc_install_directory)
        except:
          self.logger.exception(" ".join(["In deleting directory", doc_install_directory]))

      # Copy the doc in the install directory (created by copytree)
      if os.path.exists(software_doc_directory):
        # Logs variables
        # version_directory = self.current_command_options.get_global_option("version_directory")
        # log_file = os.path.join(version_directory, "logs", "create_modules_doc_directory.log")
        try:
          shutil.copytree(software_doc_directory, doc_install_directory)
          self.logger.info("The documentation of %s has been added in %s" % (software.name, doc_install_directory))
        except:
          self.logger.exception(" ".join(["In copying directory", software_doc_directory]))
        # Change the permissions of the created files (it can occur that there is no write permissions for some files
        # which causes an error when the command is used several times or for the doc publication)
        for dirpath, _, filenames in os.walk(software_doc_directory):
          for filename in filenames:
            path = os.path.join(dirpath, filename)
            os.chmod(path, stat.S_IWUSR | stat.S_IRUSR | stat.S_IROTH | stat.S_IRGRP)
      else:
        self.logger.warning("%s does not exist" % software_doc_directory)

#######
#
# Publication commands
#
#######

  def publish_command(self, *args, **kw):
    # to enable child's projects to overload publish command
    cmd = self.alternate_publish_command(args, kw)
    if cmd:
      return cmd

    # if we use the default project's publish commands:
    # we use rsync by default,
    # but we need for some publications to use lftp (ie in projets/YAMM's folder on pleiade)
    server, local_path, remote_path, is_recursive, useLftp, logfile = args
    if useLftp:
      cmd = "lftp -e \"open %s ; mirror -R %s %s ; quit\" 2>%s" % (server, local_path, remote_path, logfile)
      if not is_recursive:
        cmd = "lftp -e \"open %s ; put -O %s %s ; quit\" 2>%s" % (server, remote_path, local_path, logfile)
    else:
      if not is_recursive:
        cmd = "rsync --times %s  %s:%s 2>%s" % (local_path, server, remote_path, logfile)
    return cmd

  # method to overload to change the publication method in child's projects
  def alternate_publish_command(self, *args, **kw):
    pass

  def _publish(self, local_path, remote_path, remote_repository="", copyMode="", is_recursive=False, log_prefix="publish", useLftp=False):
    """
      This command publishes the file or directory located at local_path on a remote repository
      at remote_path.
      The default remote repository is the one specified in the option release_repository.

      :param string local_path: location of the file or directory to be published
      :param string remote_path: location of the publication directory relatively to
      remote_repository
      :param string remote_repository: directory on a remote machine
      (if empty the value of option release_repository is used)
      :param bool is_recursive: if True the content of a directory is copied recursively
      :param string log_prefix: name of the logfile
      :param string copyMode: distant such as Pleiade or local such as TGVD.
    """

    # Init variables
    publication_server = "ftp.pleiade.edf.fr"
    if copyMode == "":
      copyMode = "distant"
    if remote_repository == "":
      remote_repository = self.current_command_options.get_global_option("release_repository")
    remote_directory = os.path.join(remote_repository, remote_path)
    # Logs variables
    log_directory = self.current_command_options.get_global_option("log_directory")
    logfile = os.path.join(log_directory, log_prefix + ".log")

    # Copy on remote repository
    self.logger.info("Publishing %s in %s" % (local_path, remote_directory))
    cmd = self.publish_command(publication_server, local_path, remote_directory, is_recursive, useLftp, logfile)
    if copyMode == "local":
      try:
        if not is_recursive:
          shutil.copyfile(local_path, os.path.join(remote_directory, os.path.basename(local_path)))
        else:
          shutil.copytree(local_path, remote_directory)
      except shutil.Error:
        self.logger.error("Publication failed, local copy of %s to %s failed." % (local_path, os.path.join(remote_directory, os.path.basename(local_path))))
      except IOError:
        self.logger.error("Publication failed, permission issue or missing target folder with local copy of %s to %s." % (local_path, os.path.join(remote_directory, os.path.basename(local_path))))
    elif copyMode == "distant":
      try:
        retcode = subprocess.call(cmd, shell=True)
        if retcode < 0:
          self.logger.error("Publication failed, command was: %s" % cmd)
      except OSError as e:
        self.logger.exception("Publication failed, command was: %s, exception was: %s" % (cmd, e))
    else:
        self.logger.exception("Publication failed, argument copyMode \"%s\" is neither distant nor local. Please make up your mind." % (copyMode))
    self.logger.info("%s has been published in %s" % (local_path, remote_directory))

  def _publish_installer(self, installer_name="", platform="", remote_repository="", copyMode=""):

    # Init variables
    version_directory = self.current_command_options.get_global_option("version_directory")
    movable_installer_topdirectory = os.path.join(version_directory, "movable_installer")
    if installer_name == "":
      installer_name = "Salome-" + self.version + "-" + platform.machine()

    # Installer file
    extension = ".run"
    installer_file = os.path.join(movable_installer_topdirectory, installer_name + extension)
    installer_remote_path = os.path.join(self.version, platform)

    # Publication
    self._publish(installer_file, installer_remote_path, remote_repository, copyMode, log_prefix="publish_installer")

  def _publish_logs(self, platform="", remote_repository=""):

    # Init variables
    logs = self.current_command_options.get_global_option("log_directory")
    logs_remote_path = os.path.join(self.version, platform, "logs")

    # Publication
    self._publish(logs, logs_remote_path, remote_repository, is_recursive=True, log_prefix="publish_logs")

  @yamm_command("Publication of local files to a remote server")
  def publish(self, local_path, remote_path, remote_repository="", is_recursive=False, log_prefix="publish"):
    return self._publish(local_path, remote_path, remote_repository, is_recursive, log_prefix)

  @yamm_command("Publication of a SALOME installer to a remote server")
  def publish_installer(self, installer_name="", platform="", remote_repository="", copyMode=""):
    return self._publish_installer(installer_name, platform, remote_repository, copyMode)

  @yamm_command("Publication of YAMM log files to a remote server")
  def publish_logs(self, platform="", remote_repository=""):
    return self._publish_logs(platform, remote_repository)

  @yamm_command("Publication of the SALOME modules documentation to a remote server")
  def publish_doc(self, platform="", remote_repository=""):
    # Init variables
    doc_distrib_directory = os.path.join(self.current_command_options.get_category_option("module", "main_software_directory"), "doc")
    doc_remote_path = self.version

    # Publication
    self._publish(doc_distrib_directory, doc_remote_path, remote_repository, is_recursive=True, log_prefix="publish_doc")

  @yamm_command("Publication of the SALOME sources to a remote server")
  def publish_sources(self):
    # Init variables
    module_tgz_topdirectory = os.path.join(self.current_command_options.get_category_option("module", "main_software_directory"), "tgz")
    tool_tgz_topdirectory = os.path.join(self.current_command_options.get_category_option("tool", "main_software_directory"), "tgz")
    modules_remote_repository = "/projets/YAMM/Public/SALOME/sources/modules/"
    tools_remote_repository = "/projets/YAMM/Public/SALOME/sources/tools/"

    # Publication
    for f in glob.glob(os.path.join(module_tgz_topdirectory, r'*.tgz*')):
      self._publish(f, "", modules_remote_repository, is_recursive=False, log_prefix="publish_module_sources", useLftp=True)

    for f in glob.glob(os.path.join(tool_tgz_topdirectory, r'*.*gz*')):
      self._publish(f, "", tools_remote_repository, is_recursive=False, log_prefix="publish_tool_sources", useLftp=True)

  @yamm_command("Publication of the SALOME binaries to a remote server")
  def publish_binaries(self):
    # Init variables
    modules_binaries_topdirectory = os.path.join(self.current_command_options.get_category_option("module", "main_software_directory"), "binary_archives")
    tools_binaries_topdirectory = os.path.join(self.current_command_options.get_category_option("tool", "main_software_directory"), "binary_archives")
    prerequisites_binaries_topdirectory = os.path.join(self.current_command_options.get_global_option("top_directory"), "prerequisites", "binary_archives")
    modules_remote_repository = "/projets/YAMM/Public/SALOME/binary-archives/modules/"
    tools_remote_repository = "/projets/YAMM/Public/SALOME/binary-archives/tools/"
    prerequisites_remote_repository = "/projets/YAMM/Public/SALOME/binary-archives/prerequisites/"

    # Publication
    for f in glob.glob(os.path.join(modules_binaries_topdirectory, r'*.tgz*')):
      self._publish(f, "", modules_remote_repository, is_recursive=False, log_prefix="publish_modules_binaries", useLftp=True)

    for f in glob.glob(os.path.join(tools_binaries_topdirectory, r'*.*gz*')):
      self._publish(f, "", tools_remote_repository, is_recursive=False, log_prefix="publish_tools_binaries", useLftp=True)

    for f in glob.glob(os.path.join(prerequisites_binaries_topdirectory, r'*.*gz*')):
      self._publish(f, "", prerequisites_remote_repository, is_recursive=False, log_prefix="publish_prerequisites_binaries", useLftp=True)

  @yamm_command("Publication of a SALOME release (installer + logs) to a remote server")
  def publish_release(self, installer_name="", platform="", remote_repository=""):
    self._publish_installer(installer_name, platform, remote_repository)
    self._publish_logs(platform, remote_repository)

  @yamm_command("Publication of the tests reports to a remote server")
  def publish_test_reports(self, platform="", remote_repository=""):

    # Init variables
    log_directory = self.current_command_options.get_global_option("log_directory")
    test_logs = os.path.join(log_directory, self.version, "*")
    test_logs_remote_path = os.path.join(self.version, platform, "test_logs")

    # Publication
    self._publish(test_logs, test_logs_remote_path, remote_repository, is_recursive=True)


#######
#
# Source tarballs commands
#
#######

  @yamm_command("Creation of archives for the modules sources")
  def create_modules_source_tgz(self, tgz_topdirectory=None):
    """
      This command creates the source archives of all SALOME modules.
    """
    # Check software
    command_current_software_list = []
    for current_software in self.current_command_softwares:
      if current_software.get_type() == "module":
        command_current_software_list.append(current_software)
    if tgz_topdirectory is None:
        tgz_topdirectory = os.path.join(self.current_command_options.get_category_option("module", "main_software_directory"), "tgz")

    try:
      if not os.path.isdir(tgz_topdirectory):
        os.makedirs(tgz_topdirectory)
    except:
      self.logger.error("Creation of directory for archive files failed ! Directory was: %s" % tgz_topdirectory)

    for software in command_current_software_list:
      self._create_software_source_tgz(software.name, tgz_topdirectory)

  @yamm_command("Creation of archives for the tools sources")
  def create_tools_source_tgz(self, tgz_topdirectory=None):
    """
      This command creates the source archives of all tools softwares.
    """
    # Check software
    command_current_software_list = []
    for current_software in self.current_command_softwares:
      if current_software.get_type() == "tool":
        command_current_software_list.append(current_software)
    if tgz_topdirectory is None:
        tgz_topdirectory = os.path.join(self.current_command_options.get_category_option("tool", "main_software_directory"), "tgz")

    try:
      if not os.path.isdir(tgz_topdirectory):
        os.makedirs(tgz_topdirectory)
    except:
      self.logger.error("Creation of directory for archive files failed ! Directory was: %s" % tgz_topdirectory)

    for software in command_current_software_list:
      self._create_software_source_tgz(software.name, tgz_topdirectory)

#######
#
# Delete directories commands
#
#######

  @yamm_command("Delete of the prerequisites directories")
  def delete_prerequisite_directories(self, software_list=None, delete_src=False, delete_build=False, delete_install=False):
    """
      This command deletes for each prerequisite in the **software_list** the directories src, build and install. Options permits
      to set a list of software and to define for the list, type of directories that are targeted.

      :param [string] software_list: List of prerequisites to process.
      :param bool delete_src: If True, deletes source directory.
      :param bool delete_build: If True, deletes build directory.
      :param bool delete_install: If True, deletes install directory.
    """
    if software_list is None:
      software_list = ["ALL"]

    software_mode = "nothing"
    for software in self.current_command_softwares:
      # Step 1: configuration du logiciel
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      if software.get_type() == "prerequisite":
        if (software.name in software_list) or (software_list == ["ALL"]):
          self._delete_directories_of(software, delete_src, delete_build, delete_install)

  @yamm_command("Delete of the modules directories")
  def delete_module_directories(self, software_list=None, delete_src=False, delete_build=False, delete_install=False):
    """
      This command deletes for each module in the **software_list** the directories src, build and install. Options permits
      to set a list of software and to define for the list, type of directories that are targeted.

      :param [string] software_list: List of modules to process.
      :param bool delete_src: If True, deletes source directory.
      :param bool delete_build: If True, deletes build directory.
      :param bool delete_install: If True, deletes install directory.
    """
    if software_list is None:
      software_list = ["ALL"]

    software_mode = "nothing"
    for software in self.current_command_softwares:
      # Step 1: configuration du logiciel
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      if software.get_type() == "module":
        if (software.name in software_list) or (software_list == ["ALL"]):
          self._delete_directories_of(software, delete_src, delete_build, delete_install)

  @yamm_command("Delete of the tools directories")
  def delete_tool_directories(self, software_list=None, delete_src=False, delete_build=False, delete_install=False):
    """
      This command deletes for each tool in the **software_list** the directories src, build and install. Options permits
      to set a list of software and to define for the list, type of directories that are targeted.

      :param [string] software_list: List of tools to process.
      :param bool delete_src: If True, deletes source directory.
      :param bool delete_build: If True, deletes build directory.
      :param bool delete_install: If True, deletes install directory.
    """
    if software_list is None:
      software_list = ["ALL"]

    software_mode = "nothing"
    for software in self.current_command_softwares:
      # Step 1: configuration du logiciel
      software.set_command_env(self.current_command_softwares, self.current_command_options, software_mode, self.current_command_version_object, self.current_command_python_version)
      if software.get_type() == "tool":
        if (software.name in software_list) or (software_list == ["ALL"]):
          self._delete_directories_of(software, delete_src, delete_build, delete_install)




if __name__ == "__main__":

  print("Testing salome project class")
  pro = Project("Project SALOME", VerboseLevels.INFO)
  pro.set_version("DEV")
  pro.set_global_option("parallel_make", "8")
  pro.set_global_option("write_soft_infos", False)
  pro.set_global_option("top_directory", "/tmp/salome")
  # pro.options.set_software_option("HDF5", "xterm_make", True)
  # pro.options.set_software_option("HDF5", "run_tests", True)
  # pro.options.set_software_option("MEDFICHIER", "software_build_directory", "/tmp/mon_build_med")
  # pro.options.set_software_option("MEDFICHIER", "run_tests", True)
  pro.print_configuration()
  # pro.start()
  pro.nothing()

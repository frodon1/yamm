#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.core.framework.project_gui import FrameworkProjectGui

from yamm.projects.salome.gui import gui_project
from yamm.projects.salome.gui import start_widget

class ProjectGui(FrameworkProjectGui):

  def __init__(self, app = None, yamm_class_name="salome"):
    FrameworkProjectGui.__init__(self, app, yamm_class_name)

  def getIntroWidget(self):
    return start_widget.IntroWidget(self)

  def get_yamm_gui_project(self):
    return gui_project.YammGuiProject()

  def initYammProjectFromWizard(self, new_project, wizard):
    FrameworkProjectGui.initYammProjectFromWizard(self, new_project, wizard)
    new_project.salome_create_appli = wizard.salome_create_appli
    if wizard.occ_server:
      new_project.add_global_option("occ_server", wizard.occ_server)
    if wizard.occ_username:
      new_project.add_global_option("occ_username", wizard.occ_username)
    if wizard.use_pleiade_mirrors:
      new_project.add_global_option("use_pleiade_mirrors", wizard.use_pleiade_mirrors)

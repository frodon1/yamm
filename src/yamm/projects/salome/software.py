#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import absolute_import
import os
import tempfile

from .appli_tasks import CreateAppliTask, DeleteAppliTask
from .test_suite import SalomeTestSuite
from yamm.core.base import misc
from yamm.core.framework.software import FrameworkSoftware


class SalomeSoftware(FrameworkSoftware):
  """
  This class defines the generic interface of a Salome software
  """
  def __init__(self, name="NOT CONFIGURED", version="NOT CONFIGURED",
               verbose=misc.VerboseLevels.INFO, **kwargs):
    FrameworkSoftware.__init__(self, name, version, verbose, **kwargs)

    # For module gui sorting
    self.module_gui_sort = 10000

    self.module_gui_type = misc.enum(pre_processing=10,
                      solver=20,
                      post_processing=30,
                      supervision=40,
                      specific_preprocessing=50,
                      advanced_data_processing=60,
                      examples=1000)

  def get_debian_dev_package(self):
    if self.get_type() == 'module':
        return 'salome-' + self.name.lower() + 'dev'
    return FrameworkSoftware.get_debian_dev_package(self)

  def get_debian_package(self):
    if self.get_type() == 'module':
        return 'salome-' + self.name.lower()
    return FrameworkSoftware.get_debian_package(self)

  def get_executor_software_name(self):
    executor_software_name = self.name.capitalize()
    if self.version != "":
      executor_software_name += "-" + misc.transform(self.version)
    if self.get_type() == "module":
      executor_software_name = self.name + "_" + misc.transform(self.version)
    return executor_software_name

  def init_variables(self):
    pass

#######
#
# Méthodes appelées par le projet
#
#######

  def default_values_hook(self):
    self.env_files.append(self.project_options.get_global_option("salome_prerequisites_env_file"))
    self.env_files.append(self.project_options.get_global_option("salome_modules_env_file"))

  def user_variables_hook(self):
    # Specific user options for modules
    if self.get_type() == "module":
      if self.project_options.get_option(self.name, "make_modules_dev_docs"):
        if self.has_dev_docs():
          self.pre_install_commands.append("make dev_docs")

    build_type_defined = "-DCMAKE_BUILD_TYPE" in self.config_options
    if self.compil_type == "cmake" and build_type_defined:
      return

    if self.get_type() == "module":
      if self.project_options.get_option(self.name, "modules_debug_mode"):
        if self.compil_type == "autoconf":
          self.config_options += " --enable-debug"
        elif self.compil_type == "cmake":
          self.config_options += " -DCMAKE_BUILD_TYPE=Debug "
      else:
        if self.compil_type == "cmake":
          self.config_options += " -DCMAKE_BUILD_TYPE=Release "
    elif self.get_type() == "tool":
      if self.project_options.get_option(self.name, "tools_debug_mode"):
        if self.compil_type == "cmake":
          self.config_options += " -DCMAKE_BUILD_TYPE=Debug "
      else:
        if self.compil_type == "cmake":
          self.config_options += " -DCMAKE_BUILD_TYPE=Release "

#######
#
# Specific methods for SALOME project
#
#######

  def configure_occ_software(self, path="", repo_name=""):
    """ Configure the remote server for external and internal access.

    Dedicated to SALOME modules hosted in salome-platform.org.
    Usefull for external repositories mirrored locally.
    Sets the following variables for a software:

      - self.root_repository
      - self.repository_name

    And possibly update the option "proxy_server".
    """
    external_server = "https://git.salome-platform.org"
    if self.project_options.get_option(self.name, "occ_username"):
      external_path = os.path.join("git", path)
    else:
      external_path = os.path.join("gitpub", path)
    internal_server = "https://git.forge.pleiade.edf.fr"
    internal_path = "git"
    internal_repo_name = path + "-salome." + repo_name

    self.configure_remote_software_pleiade_git(internal_repo_name,
                                               external_server=external_server,
                                               external_path=external_path,
                                               external_repo_name=repo_name)


  def get_type(self):
    """Return the type of software.

    There are 3 types in the Salome project:

      - prerequisite
      - module
      - tool
    """
    return "type not defined for software %s" % self.name

  def get_build_str(self, specific_install_dir=""):
    """ Return a string which will be added in the build environment file.
    """
    return self.get_prerequisite_str(specific_install_dir)

  def get_prerequisite_str(self, specific_install_dir=""):
    """ Return a string which will be added in the environment file of the prerequisites.
    """
    return ""

  def get_configuration_str(self, specific_install_dir=""):
    """ Return a string which will be added to the software configuration file.
    """
    return ""

  def get_relative_lib_path_str(self):
    """ Returns a string which is the relative path of the salome module libraries.
    """
    if self.get_type() == "module":
      return "lib/salome"
    return ""

  def get_module_str(self, specific_install_dir="", universal=False):
    """ Returns a string which will be added in the environment file of the modules
    """
    if self.get_type() == "module":
      envstr = "\n#------ %s ------\n" % self.name
      install_dir = self.install_directory
      if specific_install_dir != "":
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
      envstr += "export " + self.get_module_name_for_appli() + "_ROOT_DIR=\"" + install_dir + "\"\n"
      if self.compil_type == "cmake":
        if self.get_relative_lib_path_str():
          envstr += "export LD_LIBRARY_PATH=\"" + os.path.join(install_dir, self.get_relative_lib_path_str()) + "\":${LD_LIBRARY_PATH}\n"
      return envstr
    else:
      return ""

  def has_salome_module_gui(self):
    """ Declare that a module has a GUI in SALOME.
    """
    return False

  def has_dev_docs(self):
    """ Declare that a software has a developer's guide.
    """
    return False

  def get_module_name_for_appli(self):
    """ Returns the name of the software as described in the application.
    """
    return self.name

  def get_name_for_installer(self):
    """ Returns the name of the software as given in the environment files.
    """
    return self.get_module_name_for_appli()

  def replaceEnvFilePath(self, env_file_name, filename):
    """ Insert sed commands into make_movable_archive_commands.

      The goal is to replace the path of an environment file by "__ENV_DIRECTORY_PATH__/<env_file_name>".

      :param string env_file_name: Defines the name of the env file, relatively to the env files
      directory.
      :param string filename: Defines the name of the file where the replace operation will be done
      , relatively to the install dir.
    """
    template_string = os.path.join("__ENV_DIRECTORY_PATH__", env_file_name)
    prerequisites_env_file = self.project_options.get_global_option("salome_prerequisites_env_file")
    env_directory = os.path.dirname(prerequisites_env_file)
    if env_directory:
      env_file_path = os.path.join(env_directory, env_file_name)
      self.replaceStringByTemplateInFile(env_file_path, template_string, filename)

  def create_post_compile_test_tasks(self):
    appli_directory = None
    for test_suite in self.test_suite_list:
      if isinstance(test_suite, SalomeTestSuite) and test_suite.require_appli:
        if appli_directory is None:
          appli_directory = tempfile.mkdtemp(prefix="appli_test_")
          self.post_install_test_task_list.append(CreateAppliTask(appli_directory))
        test_suite.appli_directory = appli_directory
      (post_build, post_install) = test_suite.create_post_compile_tasks()
      self.post_build_test_task_list += post_build
      self.post_install_test_task_list += post_install
    if appli_directory is not None:
      self.post_install_test_task_list.append(DeleteAppliTask(appli_directory))

modules_list = ["kernel", "gui", "yacs", "geom", "med", "smesh", "paravis", "jobmanager"]
modules_list += ["prenetgenplugin", "netgenplugin", "dsccode", "calculator", "component"]
modules_list += ["pycalculator", "pyhello1", "hello1", "light", "samples"]
modules_list += ["blsurfplugin", "ghs3dplugin", "hexoticplugin", "ghs3dprlplugin", "hybridplugin"]
modules_list += ["gmshplugin"]
modules_list += ["eficas", "homard", "genericsolver", "openturns", "ot"]
modules_list += ["hexablock", "hexablockplugin", "pylight", "adao"]
modules_list += ["atomic", "atomgen", "atomsolv"]
modules_list += ["edfdoc", "parametric"]
modules_list += ["modulecarmel", "moduleinterpol", "modulemagnetothermic"]
modules_list += ["modulecarmelcnd"]
# SALOME_CFD
modules_list += ["syrthes","cfdstudy"]
# Shaper
modules_list += ["shaper"]
# Documentation
modules_list += ["documentation"]
# Non regression tests
modules_list += ["non_regression_tests"]
modules_list += ["non_regression_tests_confidential"]

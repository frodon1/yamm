#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
import string

software_name = "BLSURFPLUGIN"
class BLSURFPLUGIN(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="plugins", repo_name="blsurfplugin.git")

    self.compil_type = "cmake"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "DOXYGEN", "CMAKE",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["SMESH", "MESHGEMS"]

  def get_type(self):
    return "module"

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "MESHGEMS":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def has_salome_module_gui(self):
    return False

  def has_dev_docs(self):
    return True

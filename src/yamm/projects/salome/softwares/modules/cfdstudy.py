#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  Author : Martine PAOLILLO (EDF R&D)

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware
import string
import os

software_name = "CFDSTUDY"
cfdstudy_template = """
#------- Code Saturne ------
export CS_ROOT_DIR=%install_dir
export PATH=${CS_ROOT_DIR}/bin:${PATH}
export PATH=${CS_ROOT_DIR}/libexec/code_saturne:${PATH}
export %ld_library_path=${CS_ROOT_DIR}/lib:${LD_LIBRARY_PATH}
export PYTHONPATH=${CS_ROOT_DIR}/bin:${PYTHONPATH}
export PYTHONPATH=${CS_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
export PYTHONPATH=${CS_ROOT_DIR}/lib/python%python_version/site-packages/code_saturne:${PYTHONPATH}
export SATURNE_INSTALL_DIR=${CS_ROOT_DIR}
"""
# SATURNE_INSTALL_DIR variable is needed by SYRTHES code for coupling between SATURNE or NEPTUNE
cfdstudy_template = misc.PercentTemplate(cfdstudy_template)

cfdstudy_configuration_template = """
#------- Code Saturne ------
CS_ROOT_DIR=$install_dir
ADD_TO_PATH: %(CS_ROOT_DIR)s/bin
ADD_TO_PATH: %(CS_ROOT_DIR)s/libexec/code_saturne
ADD_TO_$ld_library_path: "%(CS_ROOT_DIR)s/lib"
ADD_TO_PYTHONPATH: %(CS_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(CS_ROOT_DIR)s/lib/python$python_version/site-packages
ADD_TO_PYTHONPATH: %(CS_ROOT_DIR)s/lib/python$python_version/site-packages/code_saturne
SATURNE_INSTALL_DIR=%(CS_ROOT_DIR)s
"""
cfdstudy_configuration_template = string.Template(cfdstudy_configuration_template)

class CFDSTUDY(SalomeSoftware):
   
  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.solver + 1
      
  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = "CFDSTUDY_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "svn"
    self.tag = self.version
    self.configure_remote_software(internal_server="https://svn.isais.edf.fr",
                                   internal_path="mfee",
                                   internal_repo_name="saturne")

    self.compil_type = "autoconf"
    self.config_options += " --with-salome=$ROOT_SALOME "
    self.config_options += " --enable-relocatable "
    self.config_options += " --enable-silent-rules FC=gfortran "
    self.config_options += " --without-salome-med "
    self.gen_file = "sbin/bootstrap"
    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.user_dependency_command += " export PATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build)):$PATH"
    
    # Installation de la documentation cfd-study et de la documentation code-saturne
    self.post_install_commands = [self.get_make_doc_cmd()]

    # Remplacement des paths qui ne sont pas fait automatiquement dans code_saturne/cs_config.py,etc/salome/fsi_appli_config.xml et etc/code_saturne.cfg, etc/neptune_cfd.cfg
    libdir = "lib/python%s/site-packages/" % self.python_version

    prereq_file = self.project_options.get_global_option('salome_prerequisites_env_file')
    context_file = self.project_options.get_global_option('salome_context_file')
    module_file = self.project_options.get_global_option('salome_modules_env_file')
    
    self.replaceStringByTemplateInFile(prereq_file, os.path.join("__ENV_DIRECTORY_PATH__", os.path.basename(prereq_file)),
                                       "etc/salome/fsi_appli_config.xml")
    self.replaceStringByTemplateInFile(prereq_file, os.path.join("__ENV_DIRECTORY_PATH__", os.path.basename(prereq_file)),
                                       libdir + "code_saturne/cs_config.py")
    self.replaceStringByTemplateInFile(context_file, os.path.join("__ENV_DIRECTORY_PATH__", os.path.basename(context_file)),
                                       "etc/salome/fsi_appli_config.xml")
    self.replaceStringByTemplateInFile(context_file, os.path.join("__ENV_DIRECTORY_PATH__", os.path.basename(context_file)),
                                       libdir + "code_saturne/cs_config.py")
    self.replaceStringByTemplateInFile(module_file, os.path.join("__ENV_DIRECTORY_PATH__", os.path.basename(module_file)),
                                       libdir + "code_saturne/cs_config.py")

  def get_make_doc_cmd(self):
    """ Cette méthode permet d'éviter d'avoir la doc dans SATURNE_DBG
    """
    return 'cd $CURRENT_SOFTWARE_BUILD_DIR ; make doc  && make install-doc ; '

  def get_neptune_name(self):
    return "NEPTUNE"

  def get_saturnedbg_name(self):
    return "SATURNE_DBG"

  def get_neptunedbg_name(self):
    return "NEPTUNE_DBG"

  def init_dependency_list(self):

    self.software_dependency_dict['build'] += ["DOXYGEN", "SPHINX", "SETUPTOOLS", "CMAKE"]
    self.software_dependency_dict['exec'] += ["LIBXML2", "METIS", "SCOTCH", "HDF5", "CGNSLIB",
                                              "MEDFICHIER", "LIBCCMIO","KERNEL", "GUI", "YACS",
                                              "SMESH","PARAVIS", "OPENMPI", "GCC",
                                              "PARAVIEW_OSMESA", "EOS", "COOLPROP"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    for names in self.software_dependency_dict.values():
      if dependency_name in names:
        dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "LIBXML2":
      self.config_options += " --with-libxml2=$LIBXML2_INSTALL_DIR "

    if dependency_name == "METIS":
      self.config_options += " --with-metis=$METIS_INSTALL_DIR "

    if dependency_name == "SCOTCH":
      self.config_options += " --with-scotch=$SCOTCH_INSTALL_DIR "

    if dependency_name == "HDF5":
      self.config_options += " --with-hdf5=$HDF5_INSTALL_DIR "

    if dependency_name == "CGNSLIB":
      self.config_options += " --with-cgns=$CGNSLIB_INSTALL_DIR "

    if dependency_name == "MEDFICHIER":
      self.config_options += " --with-med=$MEDFICHIER_INSTALL_DIR "

    if dependency_name == "LIBCCMIO":
      self.config_options += " --with-ccm=$LIBCCMIO_INSTALL_DIR/arch/Linux_x86_64 "
      
    if dependency_name == "COOLPROP":
      self.config_options += " --with-coolprop=$COOLPROP_INSTALL_DIR "

    if dependency_name == "KERNEL":
      self.config_options += " --with-salome-kernel=$KERNEL_INSTALL_DIR "

    if dependency_name == "GUI":
      self.config_options += " --with-salome-gui=$GUI_INSTALL_DIR "

    if dependency_name == "YACS":
      self.config_options += " --with-salome-yacs=$YACS_INSTALL_DIR "

    if dependency_name == "OPENMPI":
      self.config_options += " --with-mpi=$OPENMPI_INSTALL_DIR "

    if dependency_name == "GCC":
      self.user_dependency_command = "CC=cc CXX=$GCC_INSTALL_DIR/bin/c++ FC=$GCC_INSTALL_DIR/bin/gfortran "

    if dependency_name == "PARAVIEW_OSMESA":
      self.config_options += " --with-catalyst=$PARAVIEW_OSMESA_INSTALL_DIR "
      self.config_options += " --disable-catalyst-as-plugin "

    if dependency_name == "EOS":
      self.config_options += " --with-eos=$EOS_INSTALL_DIR "
    
  def has_salome_module_gui(self):
    return True

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cfdstudy_template.substitute(install_dir=install_dir,
                                             ld_library_path=misc.get_ld_library_path(),
                                             python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    # place dans env.d/envProducts.cfg
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cfdstudy_configuration_template.substitute(install_dir=install_dir,
                                                           ld_library_path=misc.get_ld_library_path(),
                                                           python_version=self.python_version)

  def get_module_name_for_appli(self):
    return "CFDSTUDY"

  def get_type(self):
    return "module"

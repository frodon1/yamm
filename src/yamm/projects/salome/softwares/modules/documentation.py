#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware

software_name = "DOCUMENTATION"
class DOCUMENTATION(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    self.modules_with_doc = ["KERNEL", "GUI", "GEOM", "SMESH", "YACS", "JOBMANAGER",
                             "PARAVIS", "NETGENPLUGIN", "GHS3DPLUGIN",
                             "HEXOTICPLUGIN", "MED", "HOMARD", "HEXABLOCK"]
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="tools", repo_name="documentation.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = self.modules_with_doc

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name in self.modules_with_doc:
      directory_gui = "${0}_ROOT_DIR/share/doc/salome/gui/{0} ".format(dependency_name)
      self.post_install_commands.append("if test -d {0}; then cp -r {0} $CURRENT_SOFTWARE_INSTALL_DIR/gui; fi ".format(directory_gui))
      directory_tui = "${0}_ROOT_DIR/share/doc/salome/tui/{0} ".format(dependency_name)
      self.post_install_commands.append("if test -e {0}/index.html; then cp -r {0} $CURRENT_SOFTWARE_INSTALL_DIR/tui; fi ".format(directory_tui))


  def get_type(self):
    return "module"

  def get_module_str(self, specific_install_dir="", universal=False):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return "export " + self.get_module_name_for_appli() + "_ROOT_DIR=\"" + install_dir + "\"\n"

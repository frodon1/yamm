#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import string
import os

from yamm.projects.salome.software import SalomeSoftware

software_name = "EDFDOC"
class EDFDOC(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 200000

  def get_debian_build_depends(self):
    return ['texlive-latex-base', 'texlive-lang-french', 'texlive-latex-extra', 'texlive-fonts-recommended']

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    # No proxy for EDFDOC
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "")

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("salome-plateform.module-edf-doc.git")

    self.compil_type = "cmake"
    self.pre_configure_commands = \
      ['cd doc_latex && '
      'pdflatex -interaction nonstopmode *.tex && '
      'pdflatex -interaction nonstopmode *.tex && '
      'mv *.pdf ../pdfs && cd ..',
      'python components.py',
      'rm -rf adm_local doc idl resources src' ,
      'mv ' + self.get_module_name_for_appli() + '_SRC/* .',
      'rm -rf ' + self.get_module_name_for_appli() + '_SRC',
      'cp SalomeApp.xml doc']

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "YACSGEN", "PYTHON", "SETUPTOOLS",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["GUI", "GEOM", "SMESH", "YACS",
                                             "EFICAS_NOUVEAU"]

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CONFIGURATION":
      self.config_options += ' -DCMAKE_MODULE_PATH="${CONFIGURATION_CMAKE_DIR}" '


  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

  def get_module_name_for_appli(self):
    return "EDFDoc"

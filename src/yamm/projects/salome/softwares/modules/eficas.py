#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "EFICAS"
class EFICAS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    # EFICAS is placed just before the examples in the GUI
    self.module_gui_sort = self.module_gui_type.examples - 10

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    # No proxy for EFICAS
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "")

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("eficas.module-eficas")

    if misc.compareVersions(self.ordered_version, "V8_3_0") <= 0:
        self.compil_type = "autoconf"
        self.gen_commands += ["./clean_configure", "./build_configure "]
    else:
        self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.user_dependency_command += " export PATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build)):$PATH"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS", "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["GUI", "EFICAS_NOUVEAU"]

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

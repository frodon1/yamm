#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2013, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import string

from yamm.projects.salome.software import SalomeSoftware

software_name = "GEOM"
class GEOM(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.pre_processing + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="geom.git")

    self.compil_type = "cmake"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "CMAKE", "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["KERNEL", "GUI", "OCC", "PARAVIEW",
                                             "QT", "PYTHON", "OMNIORB",
                                             "LIBXML2", "OPENCV", "FREETYPE"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "OPENCV":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "FREETYPE":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "SWIG":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "OPENCV":
      self.config_options += " -DSALOME_GEOM_USE_OPENCV=ON "

    if dependency_name == "CONFIGURATION":
      self.config_options += ' -DCMAKE_MODULE_PATH="${CONFIGURATION_CMAKE_DIR}" '
    if dependency_name == "FREETYPE":
      self.config_options += ' -DFREETYPE_INCLUDE_DIR_freetype2="${FREETYPE_INSTALL_DIR}/include/freetype2/freetype" '
      self.config_options += ' -DFREETYPE_INCLUDE_DIR_ft2build="${FREETYPE_INSTALL_DIR}/include/freetype2/freetype" '
      self.config_options += ' -DFREETYPE_LIBRARY="${FREETYPE_INSTALL_DIR}/lib" '
    if dependency_name == "SWIG":
      self.config_options += " -DSWIG_EXECUTABLE:PATH=${SWIG_INSTALL_DIR}/bin/swig "

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

  def has_dev_docs(self):
    return True

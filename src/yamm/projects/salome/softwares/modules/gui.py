#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2013, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string

software_name = "GUI"
class GUI(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="gui.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    # If system Qt5 is used, FindSalomeQt5 dost not detect all required components
    # => we set them manually
    try:
      qtversion = misc.major(self.version_object.get_software_version('QT'))
      if qtversion == 5 and 'QT' not in self.project_softwares_dict:
        qtcomponents = {'Qt5Core': 'qtbase5-dev',
                        'Qt5X11Extras': 'libqt5x11extras5-dev',
                        'Qt5Test': 'qtbase5-dev',
                        'Qt5OpenGL': 'libqt5opengl5-dev',
                        'Qt5PrintSupport': 'qtbase5-dev',
                        'Qt5Xml': 'qtbase5-dev',
                        'Qt5WebEngine': 'qtwebengine5-dev',
                        'Qt5WebEngineWidgets': 'qtwebengine5-dev'}
        for qtcomponent, package in qtcomponents.items():
          self.config_options += ' -D{0}_DIR:FILEPATH=$(dirname $(dpkg -L {1} |grep {0}/{0}Config.cmake))' \
                                 .format(qtcomponent, package)
    except misc.LoggerException:
      # Qt not defined in version
      pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["KERNEL", "OCC", "QT", "PYQT",
                                             "SIP", "QWT", "PYTHON", "LIBXML2",
                                             "OMNIORB", "GL2PS", "HDF5",
                                             "PARAVIEW"]

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "QWT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "QT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "PYQT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "OCC":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "PARAVIEW":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "SWIG":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "QWT":
      # The path to the library is explicitly given to avoid detection of the system
      # qwt (see details below).
      # In FindQwt.cmake libqwt-qt4 is searched first to avoid linking with a system
      # libqwt linked with Qt3. If liqwt-qt4 is found on th system then the
      # library corresponding to the actual Qwt install given through -DQWT_ROOT_DIR
      # is not used and the build fails.
      self.config_options += " -DQWT_LIBRARY=$QWT_INSTALL_DIR/lib/libqwt.so"

    if dependency_name == "CONFIGURATION":
      self.config_options += ' -DCMAKE_MODULE_PATH="${CONFIGURATION_CMAKE_DIR}" '

    if dependency_name == "PARAVIEW":
      self.config_options += " -DPARAVIEW_VERSION=${PVVERSION}"

    if dependency_name == "OCC":
      self.config_options += " -DCAS_ROOT_DIR=$OCC_INSTALL_DIR"

    major = misc.major(self.ordered_version)
    if dependency_name == "QT":
      self.config_options += " -DQT${QTVERSION}_ROOT_DIR=$QT_INSTALL_DIR "
      self.config_options += " -DSALOME_BUILD_WITH_QT${QTVERSION}=ON "

    if dependency_name == "PYQT":
      try:
        qtversion = misc.major(self.version_object.get_software_version('QT'))
        self.config_options += " -DPYQT%s_ROOT_DIR=$PYQT_INSTALL_DIR" % qtversion
      except misc.LoggerException:
        # Qt not defined in version
        pass

    if dependency_name == "SIP":
      self.config_options += " -DCMAKE_INCLUDE_DIRECTORIES_BEFORE=ON"
    
    if dependency_name == "SWIG":
      self.config_options += " -DSWIG_EXECUTABLE:PATH=${SWIG_INSTALL_DIR}/bin/swig "

  def has_dev_docs(self):
    return True

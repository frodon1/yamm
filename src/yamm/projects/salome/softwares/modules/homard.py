#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

# Imports necessary for the automated tests
from yamm.core.framework.test_suite import TestScript
from yamm.projects.salome.test_suite import SalomeTestSuite, SalomeTestGroup

import string
import os

software_name = "HOMARD"
class HOMARD(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.specific_preprocessing + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="homard.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"
    # Creation of the test suite
    autotests = SalomeTestSuite(name="module-homard",
                                soft_name=self.name,
                                soft_version=self.version,)
    autotests.post_install = True
    autotests.post_move = True
    test_directory = os.path.join(self.install_directory, "bin", "salome", "test")
    test_group = SalomeTestGroup()
    test_group.tests.append(TestScript(os.path.join(test_directory, "test_1.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "test_2.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "test_3.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "test_4.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "test_5.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "test_util.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "test_1.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "tutorial_2.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "tutorial_3.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "tutorial_4.py"), "python"))
    test_group.tests.append(TestScript(os.path.join(test_directory, "tutorial_5.py"), "python"))
    autotests.test_list = [test_group]
    autotests.require_appli = True
    self.test_suite_list = [autotests]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "DOCUTILS", "SPHINX",
                                              "CMAKE", "SETUPTOOLS",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["SMESH", "GUI", "HOMARD_TOOL",
                                             'MEDCOUPLING']

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2013, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
import string

software_name = "KERNEL"
class KERNEL(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 0

  def get_debian_depends(self):
    return ['freeglut3', 'libpng12-0', 'libpng16-16', 'libxmu6']

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="kernel.git")

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["PYTHON", "HDF5", "BOOST",
                                             "OMNIORB", "OMNIORBPY", "NUMPY",
                                             "LIBXML2", "LIBBATCH", "CPPUNIT","GDB","SIX"]

  def get_type(self):
    return "module"

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "SWIG":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "HDF5":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BOOST":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "OMNIORB":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "OMNIORBPY":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "GRAPHVIZ":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "LIBXML2":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "CPPUNIT":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CPPUNIT":
      self.config_options += " --with-cppunit=${CPPUNIT_INSTALL_DIR} "
      self.config_options += " -DSALOME_BUILD_TESTS=ON "
    if dependency_name == "LIBBATCH":
      self.config_options += " -DSALOME_USE_LIBBATCH=ON "
    if dependency_name == "CONFIGURATION":
      self.config_options += " -DCMAKE_MODULE_PATH=${CONFIGURATION_CMAKE_DIR} "
    if dependency_name == "SWIG":
      self.config_options += " -DSWIG_EXECUTABLE:PATH=${SWIG_INSTALL_DIR}/bin/swig "
    pass

  def has_dev_docs(self):
    return True

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string

software_name = "MED"
class MED(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.post_processing + 2

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="med.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["GUI", "MEDCOUPLING", "MEDFICHIER"]

    if misc.compareVersions(self.ordered_version, "V7_7_1") <= 0:
      self.software_dependency_dict['exec'] += ["METIS", "SCOTCH"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "MEDCOUPLING":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "METIS":
      dependency_object.depend_of = ["install_path", "ld_lib_path"]
    if dependency_name == "SCOTCH":
      dependency_object.depend_of = ["install_path", "path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "MEDCOUPLING":
      self.config_options += " -DMEDCOUPLING_ROOT_DIR=${MEDCOUPLING_INSTALL_DIR} "
    elif dependency_name == "MEDFICHIER":
      self.config_options += " -DMEDFILE_ROOT_DIR=${MEDFICHIER_INSTALL_DIR} "
    elif dependency_name == "METIS":
      self.config_options += " -DMMETIS_ROOT_DIR=${METIS_INSTALL_DIR} "
    elif dependency_name == "SCOTCH":
      self.config_options += " -DSCOTCH_ROOT_DIR=${SCOTCH_INSTALL_DIR} "

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

  def module_support_cmake_compilation(self):
    return True

  def has_dev_docs(self):
    return True

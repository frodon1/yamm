#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Martine PAOLILLO (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware

software_name = "CARMEL3D"
class CARMEL3D(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("carmel.module-carmel3d")

    self.compil_type = "cmake"
    self.pre_configure_commands = ['echo $pwd',
                       'python components.py',
                       'rm -rf doc idl resources src' ,
                       'mv CARMEL3D_SRC/* .', 'cp -f CMakeLists.txt.tmp src/CARMEL3D/CMakeLists.txt']

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "YACSGEN"]
    self.software_dependency_dict['exec'] = ["YACS", "JOBMANAGER", "EFICAS",
                                             "CODE_CARMEL3D"]

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware

software_name = "NON_REGRESSION_TESTS"
class NON_REGRESSION_TESTS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("salome-tests.non_regression_tests.git")

    self.compil_type = "cmake"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE",]
    self.software_dependency_dict['exec'] = ["GUI","GEOM","SMESH","MEDCOUPLING","BLSURFPLUGIN", 
                                             "NETGENPLUGIN", "GHS3DPLUGIN", "HEXOTICPLUGIN",
                                             "HOMARD", "HYBRIDPLUGIN", "PARAVIS"]
  
  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "GUI":
      self.config_options +=" -DWITH_GUI_TESTS:BOOL=ON " #ported: OK
    if dependency_name == "SMESH":
      self.config_options +=" -DWITH_GEOMSMESH_TESTS:BOOL=ON " #ported: OK
      self.config_options +=" -DWITH_USECASES_TESTS:BOOL=ON " #ported: failed tests to be checked and fixed
      self.config_options +=" -DWITH_LARGECASES_TESTS:BOOL=ON " #ported: OK
    if dependency_name == "MEDCOUPLING":
      self.config_options += " -DWITH_MEDCOUPLING_TESTS:BOOL=ON " #ported: OK
    if dependency_name == "YACS":
      self.config_options += " -DWITH_YACS_TESTS:BOOL=OFF"
    if dependency_name == "PARAVIS":
      self.config_options += " -DWITH_PARAVIS_TESTS:BOOL=ON" #ported: OK

  def get_type(self):
    return "module"

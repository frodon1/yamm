# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.projects.salome.test_suite import SalomeTestSuite
import string
import os

software_name = "OPENTURNS"
class OPENTURNS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.advanced_data_processing + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    if misc.compareVersions(self.ordered_version, "V7_7_1") <= 0:
      self.configure_remote_software_pleiade_git("incertitudes.salome-openturns.git")
    else:
      self.configure_remote_software_pleiade_git("incertitudes.otsalomemodule.git")

    self.compil_type = "cmake"

    # Force recomputation of src_directory. Since we use git, the default one is wrong.
    if self.software_source_type == "remote":
      self.src_directory = os.path.join(\
          self.project_options.get_option(self.name, "main_software_directory"), "src", self.name)

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    autotests = SalomeTestSuite(name="module-openturns-tests",
                                soft_name=self.name,
                                soft_version=self.version,
                                version=self.version)
    autotests.src_directory = os.path.join(self.src_directory, "test")
    autotests.post_install = True
    autotests.post_move = True
    autotests.test_list_file = os.path.join(autotests.src_directory, "test_list.py")
    autotests.require_appli = True
    self.test_suite_list = [autotests]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["KERNEL", "YACS", "OPENTURNS_DOC"]
    if misc.compareVersions(self.ordered_version, "V7_7_1") <= 0:
      self.software_dependency_dict['exec'] = ["OPENTURNS_TOOL"]
    else:
      self.software_dependency_dict['exec'] = ["OTGUI"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "OPENTURNS_DOC":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "OPENTURNS_TOOL":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "OTGUI":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "YACS":
      dependency_object.depend_of = ["install_path"]
    return dependency_object


  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

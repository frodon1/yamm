#  Copyright (C) 2012, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

import string

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware

software_name = "PARAMETRIC"
class PARAMETRIC(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 43  # 40 for advanced modules + 3 for the order within this group

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    version_for_archive = misc.transform(self.version)
    self.archive_file_name = self.name + "_SRC_" + version_for_archive + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="parametric.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["KERNEL", "GUI", "PYTHON"]
    # H5PY is a runtime-only dependency for PARAMETRIC, but since PARAMETRIC is quickly compiled,
    # we add this dependency explicitly here so that software_minimal_list option works properly.
    self.software_dependency_dict['exec'] += ["H5PY"]

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.config_options += " -DPYTHON_ROOT_DIR=${PYTHONHOME}"

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011-2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string
import os

software_name = "PARAVIS"
paravis_template = """
#------- Paravis ------
export PARAVIS_DIR=%install_dir
export PV_PLUGIN_PATH="${PARAVIS_DIR}/lib/paraview":${PV_PLUGIN_PATH}
export PV_MACRO_PATH=${HOME}/.config/salome/Macros
"""
paravis_template = misc.PercentTemplate(paravis_template)

paravis_configuration_template = """
#------- Paravis ------
PARAVIS_DIR=$install_dir
ADD_TO_PV_PLUGIN_PATH: %(PARAVIS_DIR)s/lib/paraview
PV_MACRO_PATH=$$HOME/.config/salome/Macros
"""
paravis_configuration_template = string.Template(paravis_configuration_template)

class PARAVIS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.post_processing + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)


    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="paravis.git")

    self.compil_type = "cmake"
    self.config_options += " -DVTK_DIR=${PVHOME}/lib/cmake/paraview-${PVVERSION} "
    self.config_options += " -DSALOME_PARAVIS_MINIMAL_CORBA=ON "

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["MED", "PARAVIEW", "GEOM",
                                             "SMESH", "PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PARAVIEW":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.config_options += " -DPYTHON_ROOT_DIR=${PYTHONHOME}"

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return paravis_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    # place dans env.d/envProducts.cfg
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return paravis_configuration_template.substitute(install_dir=install_dir)

  def module_support_cmake_compilation(self):
    return False

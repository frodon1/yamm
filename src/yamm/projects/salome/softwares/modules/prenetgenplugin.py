#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from .netgenplugin import NETGENPLUGIN


software_name = "PRENETGENPLUGIN"
class PRENETGENPLUGIN(NETGENPLUGIN):

  def __init__(self, name, version, verbose, **kwargs):
    NETGENPLUGIN.__init__(self, name, version, verbose, **kwargs)
    self.tips = "This module does not exist, it's an artefact of YAMM to correctly compile NETGEN"

  def init_variables(self):
    NETGENPLUGIN.init_variables(self)

    self.compil_type = "specific"
    self.specific_install_command = "cp src/NETGEN/netgen* $CURRENT_SOFTWARE_INSTALL_DIR ; "

  def init_dependency_list(self):
    self.software_dependency_list = []
    self.software_dependency_dict.clear()

  def isArtefact(self):
    return True

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2013 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware

software_name = "SHAPER"
class SHAPER(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.pre_processing + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"
    if self.project_options.get_option(self.name, "use_pleiade_mirrors"):
      self.project_options.set_software_option(self.name, "use_pleiade_mirrors", False)

    self.remote_type = "git"
    self.repository_name = "shaper.git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="shaper.git")

    # Some post install command in order to load SHAPER as a standard module in a virtual application
    self.post_install_commands.append("cd ${CURRENT_SOFTWARE_INSTALL_DIR}")
    self.post_install_commands.append("cp share/salome/resources/shaper/LightApp.xml share/salome/resources/shaper/SalomeApp.xml")
    # Create a symbolic link towards the .so files
    self.post_install_commands.append("mkdir lib ; cd lib ; ln -fs ../bin salome")

    # Temporary needed additional environment TO BE REMOVED
    self.user_dependency_command += " export LIB=$LD_LIBRARY_PATH "

    self.tag = self.version

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE"]
    self.software_dependency_dict['exec'] = ["SOLVESPACE", "PLANEGCS", "GUI",
                                             "FREETYPE", "QT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "FREETYPE":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "PLANECGS":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "QT":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "FREETYPE":
      self.user_dependency_command += " export FREETYPE_ROOT_DIR=$FREETYPE_INSTALL_DIR "
    if dependency_name == "QT":
      self.config_options += " -DQT5_ROOT_DIR=${QT_INSTALL_DIR} " 

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

  def module_support_cmake_compilation(self):
    return True

  # The environment has to be completed with some variables to
  # find the plugins and some other stuff, just like for PARAVIS
  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return_string = "\n"
    return_string += "#------- Shaper ------\n"
    return_string += "SHAPER_ROOT_DIR=" + install_dir + "\n"
    return_string += "ADD_TO_PATH: %(SHAPER_ROOT_DIR)s/bin/salome:%(SHAPER_ROOT_DIR)s/lib/salome\n"
    return_string += "ADD_TO_PYTHONPATH: %(SHAPER_ROOT_DIR)s/bin/salome:%(SHAPER_ROOT_DIR)s/lib/salome\n"
    return_string += "ADD_TO_LD_LIBRARY_PATH: %(SHAPER_ROOT_DIR)s/bin/salome:%(SHAPER_ROOT_DIR)s/lib/salome\n"
    return_string += "PLUGINS_CONFIG_FILE=%(SHAPER_ROOT_DIR)s/share/salome/resources/shaper\n"
    return_string += "SHAPERResources=%(SHAPER_ROOT_DIR)s/share/salome/resources/shaper\n"

    return return_string

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return_string = "\n"
    return_string += "#------- Shaper ------\n"
    return_string += "export SHAPER_ROOT_DIR=" + install_dir + "\n"
    return_string += "export PATH=${SHAPER_ROOT_DIR}/bin/salome:${SHAPER_ROOT_DIR}/lib/salome:${PATH}"
    return_string += "export PYTHONPATH=${SHAPER_ROOT_DIR}/bin/salome:${SHAPER_ROOT_DIR}/lib/salome:${PYTHONPATH}\n"
    return_string += "export LD_LIBRARY_PATH=${SHAPER_ROOT_DIR}/bin/salome:${SHAPER_ROOT_DIR}/lib/salome:${LD_LIBRARY_PATH}\n"
    return_string += "export PLUGINS_CONFIG_FILE=${SHAPER_ROOT_DIR}/share/salome/resources/shaper\n"
    return_string += "export SHAPERResources=${SHAPER_ROOT_DIR}/share/salome/resources/shaper\n"

    return return_string

  def get_module_name_for_appli(self):
    return "SHAPER"

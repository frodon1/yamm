#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2013, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import string
import os
import glob

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

# Imports necessary for the automated tests
from yamm.core.framework.test_suite import TestScript
from yamm.projects.salome.test_suite import SalomeTestSuite, SalomeTestGroup

software_name = "SMESH"
class SMESH(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.pre_processing + 2

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="smesh.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    # Creation of the test suite
    autotests = SalomeTestSuite(name="module-smesh",
                                  soft_name=self.name,
                                  soft_version=self.version)
    autotests.post_install = True
    autotests.post_move = True
    test_directory = os.path.join(self.install_directory, "bin", "salome")
    netgen_dependant_tests = ["SMESH_test5.py",
                              "SMESH_mechanic_netgen.py",
                              "SMESH_fixation_tetra.py",
                              "SMESH_box3_tetra.py",
                              "SMESH_flight_skin.py",
                              "SMESH_fixation_netgen.py",
                              "SMESH_Nut.py",
                              "SMESH_box_tetra.py",
                              "SMESH_box.py",
                              "SMESH_box2_tetra.py",
                              "SMESH_mechanic_tetra.py",
                              "SMESH_Partition1_tetra.py"]
    test_group = SalomeTestGroup()
    for f in glob.glob(os.path.join(test_directory, r"SMESH_*.py")):
      # if os.path.basename(f) in netgen_dependant_tests:
        # continue
      test_group.tests.append(TestScript(f, "python"))
    autotests.test_list = [test_group]
    autotests.known_errors = netgen_dependant_tests
    autotests.require_appli = True
    self.test_suite_list = [autotests]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS", "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["KERNEL", "GUI", "GEOM",
                                             "MEDFICHIER", "NUMPY", "CGNSLIB",
                                             "TBB", "PADDER", "QWT"]


  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CGNSLIB":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "TBB":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "QWT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "SWIG":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "module"

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CONFIGURATION":
      self.config_options    +=" -DCMAKE_MODULE_PATH=\"${CONFIGURATION_CMAKE_DIR}\" "
    if dependency_name == "PADDER":
      self.config_options += " -DPADDERHOME:STRING=${PADDERHOME} "
    if dependency_name == "TBB":
      if misc.split_version(self.ordered_version) < [7, 3]:  # Versions inferior to V7_3_0
        self.config_options += " --with-TBB=${TBB_INSTALL_DIR}"
      else:
        self.config_options += " -DSALOME_SMESH_USE_TBB=ON "
    if dependency_name == "CGNSLIB":
      if misc.split_version(self.ordered_version) < [7, 3]:
        self.config_options += " --with-cgns=${CGNSLIB_INSTALL_DIR}"
      else:
        self.config_options += " -DCGNS_ROOT_DIR=${CGNSLIB_INSTALL_DIR} "
        self.config_options += " -DSALOME_SMESH_USE_CGNS=ON "
    if dependency_name == "MEDFICHIER" and misc.split_version(self.ordered_version) >= [7, 3]:
      self.config_options += " -DMEDFILE_ROOT_DIR=$MEDFICHIER_INSTALL_DIR "
    if dependency_name == "QWT":
      self.config_options += " -DQWT_ROOT_DIR=$QWT_INSTALL_DIR "
    if dependency_name == "SWIG":
      self.config_options += " -DSWIG_EXECUTABLE:PATH=${SWIG_INSTALL_DIR}/bin/swig "

  def has_salome_module_gui(self):
    return True

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)

    return_string = "\n"
    return_string += "#------- SMESH -------\n"
    return_string += "SMESH_DIR=" + install_dir + "\n"
    return_string += "export PYTHONPATH=${SMESH_DIR}/share/salome/plugins/smesh/Verima:${PYTHONPATH}\n"

    return return_string

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)

    return_string = "\n"
    return_string += "#------- SMESH -------\n"
    return_string += "SMESH_DIR=" + install_dir + "\n"
    return_string += "ADD_TO_PYTHONPATH=%(SMESH_DIR)s/share/salome/plugins/smesh/Verima\n"

    return return_string


  def module_support_cmake_compilation(self):
    return True

  def has_dev_docs(self):
    return True

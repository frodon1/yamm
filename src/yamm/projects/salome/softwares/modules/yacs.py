#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import string
import os

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.framework import test_suite

software_name = "YACS"
class YACS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.supervision + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="yacs.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    self.test_suite_list = [test_suite.MakeTestSuite(name="make test YACS",
                                                     soft_name=self.name,
                                                     soft_version=self.version)]

  def init_dependency_list(self):
    # QScintilla is formally not needed anymore in Yacs.
    # It has been removed from the dependency list, we keep that comment in
    # case CoTech decides to roll back that change.
    self.software_dependency_dict['build'] = ["SWIG", "GRAPHVIZ", "DOXYGEN",
                                              "DOCUTILS", "SPHINX", "CMAKE",
                                              "SETUPTOOLS", "CONFIGURATION",
                                              "YACSGEN"]
    self.software_dependency_dict['exec'] = ["KERNEL", "GUI", "EXPAT",
                                             "CPPUNIT"]

  def get_type(self):
    return "module"

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CPPUNIT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "QSCINTILLA":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "EXPAT":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "EXPAT_ROOT_DIR"
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "GUI":
      self.config_options += " -DSALOME_BUILD_GUI=ON "

  def has_salome_module_gui(self):
    return True

  def has_dev_docs(self):
    return True

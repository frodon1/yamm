# -*- coding: utf-8 -*-
modules_list = ["m4", "autoconf", "automake", "libtool"]
modules_list += ["gmp", "mpfr", "mpc", "cloog", "binutils", "gcc","gdb"]
modules_list += ["hdf5", "python", "pythonstartup", "boost", "swig", "omniorb", "omniorbpy", "cmake", "lapack"]
modules_list += ["e2fsprogs" , "zmq", "vishnu"]
modules_list += ["logservice", "diet", "libcrypt"]  # Pour compiler les versions anterieures a 7.3.0
modules_list += ["numpy", "expat", "graphviz", "doxygen", "libxml2", "lxml", "swig", "setuptools"]
modules_list += ["pygments", "jinja", "docutils", "sphinx"]
modules_list += ["freeimage", "freetype", "ftgl", "gl2ps", "occ"]
modules_list += ["qt", "sip", "pyqt", "qwt"]
modules_list += ["paraview", "qscintilla", "metis", "scotch", "togl", "tcl", "tk", "tix", "pil", 'pillow']
modules_list += ["distene", "blsurf", "ghs3d", "hexotic", "meshgems", "tepal", "yams"]
modules_list += ["matplotlib", "gnuplotpy"]
modules_list += ["r", "rpy2", "rpy", "boot", "sensitivity", "rotrpackage", "scipy", "numeric", "vtk"]
modules_list += ["cgnslib", "tbb", "ipython"]
modules_list += ["opencv", "pyqwt", "fltk", "gmsh", "cgal"]
modules_list += ["cppunit", "h5py", "tralics"]
modules_list += ["lesstif", "grace", "bison", "libxslt", "muparser", "nlopt"]
modules_list += ["solvespace", "eigen", "planegcs"]
modules_list += ["numpydoc", "setuptools_otw"]

# for salome_hydro
modules_list += ["proj", "geos", "gdal", "grass", "gsl", "gpsbabel", "requests", "networkx", "pyopengl"]
modules_list += ["libspatialite", "spatialindex", "qwtpolar", "psycopg2", "fiona", "qca", "qgis"]
modules_list += ["cork", "cloudcompare", "sqlite", "saga", "shapely", "wxwidgets", "jasper", "ncurses"]

# for salome_meca
modules_list += ["zcracks"]

# for mpi
modules_list += ["openmpi", "envhpcopenmpi", "mpi4py"]

# for testing
modules_list += ["test_software"]

# for CARMEL3D SALOME_CND
modules_list += ["pycrypto", "ecdsa", "paramiko"]

# for SALOME_CFD
modules_list += ["libccmio", "coolprop"]
# for CODE_SYRTHES in SALOME_CFD: compatibility python 2, 3 into new SYRTHES GUI QT5
modules_list += [ "future"]
# for SALOME_CFD with catalyst
modules_list += ["osmesa","paraview_osmesa"]

# for Salome-coeur
modules_list += ["pmw"]

# prerequisite for h5py
modules_list += ["cython", "pkgconfig"]

# prerequisite for cocagne
modules_list += ["eigen", "gnuplot"]

# prerequisite for python3
modules_list += ["six"]

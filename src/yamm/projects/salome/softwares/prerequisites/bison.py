#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware

software_name = "BISON"

class BISON(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "bison-" + self.version + ".tar.gz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "autoconf"

  def get_type(self):
    return "prerequisite"

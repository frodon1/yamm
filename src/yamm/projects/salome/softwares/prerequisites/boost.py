# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "BOOST"
boost_template = """
#------ boost ---------
export BOOSTDIR="%install_dir"  # For check_boost.m4
export BOOST_ROOT="$BOOSTDIR"   # For CGAL
export %ld_library_path=$BOOST_ROOT/lib:$%ld_library_path
"""
boost_template = misc.PercentTemplate(boost_template)

boost_configuration_template = """
#------ boost ---------
BOOSTDIR="$install_dir"  # For check_boost.m4
BOOST_ROOT="%(BOOSTDIR)s"   # For CGAL
ADD_TO_$ld_library_path: %(BOOST_ROOT)s/lib
"""
boost_configuration_template = string.Template(boost_configuration_template)

class BOOST(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    if "full" in version:
      self.build_type = "full"
    elif "salome" in version:
      self.build_type = "salome"
    else:
      self.build_type = "salome"
    self.specific_configure_command_add = ""

  def get_debian_dev_package(self):
    return 'libboost-all-dev'

  def init_variables(self):
    # Les archives des sources de boost sont distribuées
    # avec une version de type x_y_z et pas x.y.z
    archive_version = self.ordered_version.replace(".", "_")
    self.archive_file_name = "boost_" + archive_version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_configure_command = "sh bootstrap.sh --prefix=$CURRENT_SOFTWARE_INSTALL_DIR "
    self.specific_configure_command += self.specific_configure_command_add

    self.specific_install_command = "./bjam -j%s --layout=tagged " % parallel_make
    if self.build_type == "full":
      self.specific_install_command += "--build-type=complete "
    elif self.build_type == "salome":
      self.specific_install_command += "--with-program_options --with-thread --with-system "
      self.specific_install_command += "--with-signals --with-regex --with-date_time --with-filesystem "
      self.specific_install_command += "--with-test --with-serialization --with-random --with-chrono "
      self.specific_install_command += "variant=release link=shared threading=multi runtime-link=shared "
    self.specific_install_command += "install"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.specific_configure_command_add += "--with-python=$PYTHON_INSTALL_DIR/bin/python"
      self.executor_software_name += "-py" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return boost_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return boost_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

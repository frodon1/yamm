#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "BOOT"
BOOT_template = """
#------ boot -----------
export R_LIBS="%install_dir":${R_LIBS}
"""
BOOT_template = misc.PercentTemplate(BOOT_template)

BOOT_configuration_template = """
#------ boot -----------
R_LIBS="$install_dir":${R_LIBS}
"""
BOOT_configuration_template = string.Template(BOOT_configuration_template)

class BOOT(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "boot_" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    self.specific_install_command = "R CMD INSTALL -l $CURRENT_SOFTWARE_INSTALL_DIR " + self.project_options.get_global_option("archives_directory") + "/" + self.archive_file_name

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["R"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "R":
      specific_bin_path = "lib" + "64"*misc.is64() + "/R/bin"
      specific_lib_path = "lib" + "64"*misc.is64() + "/R/lib"
      dependency_object.depend_of = ["path", "ld_lib_path"]
      dependency_object.specific_bin_path = specific_bin_path
      dependency_object.specific_lib_path = specific_lib_path
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "R":
      self.executor_software_name += "-r" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return BOOT_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return BOOT_configuration_template.substitute(install_dir=install_dir)

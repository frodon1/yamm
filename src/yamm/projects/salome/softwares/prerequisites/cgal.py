#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Guillaume Boulant

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CGAL"
cgal_template = """
#------ CGAL ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
"""
cgal_template = misc.PercentTemplate(cgal_template)

cgal_configuration_template = """
#------ CGAL ------
ADD_TO_$ld_library_path: "$install_dir/lib"
"""
cgal_configuration_template = string.Template(cgal_configuration_template)

# RNC: Qt dependency removed, otherwise it fails on Calibre5. With the cmake variables
#    "-DWITH_CGAL_Qt3:BOOL=OFF "
#    "-DWITH_CGAL_Qt4:BOOL=OFF "
#      It doesn't seem to be any problem anymore.
#
# GDD: QT dependency is kept, otherwise compilation fails on Debian.
# Actually it seems to depend on the system qt3 and qt4 installed libraries too.
#
# WARN: the Qt dependency is remove because CGAL does not compile with
# the version used with SALOME. Then CGAL will be compiled with a
# Calibre7 version of Qt (older). This is not satisfying but not
# important because padder does not use the Qt part of CGAL.
#

class CGAL(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
    self.cmake36 = False

  def get_debian_dev_package(self):
    return 'libcgal-dev'

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    # Patch if build with cmake 3.6.2.
    if self.cmake36:
      self.patches += [os.path.join(self.patch_directory,"cgal_cmake352.patch")]

  def init_variables(self):
    self.archive_file_name = "CGAL-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.config_options += "-DWITH_CGAL_Qt3:BOOL=OFF "
    self.config_options += "-DWITH_CGAL_Qt4:BOOL=OFF "
    # Support de GMP désactivé:
    # avec le support activé, si libgmp est détecté
    # et pas libmpfr, la compilation de Padder
    # échoue ensuite sur certaines plate-formes (Calibre-6 notamment)
    self.config_options += "-DWITH_GMP:BOOL=OFF "
    self.config_options += "-DWITH_GMPXX:BOOL=OFF "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["BOOST", "LAPACK"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "BOOST":
      dependency_object.depend_of = [ "ld_lib_path"]
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "LAPACK":
      dependency_object.depend_of = [ "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    self.executor_software_name += "-%s%s" % (dependency_name.lower(), misc.transform(version_name))
    if dependency_name == "CMAKE":
      self.cmake36 = version_name.startswith('3.6')

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cgal_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cgal_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CGNSLIB"
cgnslib_template = """
#------ CGNS Library ------
CGNSHOME=%install_dir
export PATH=${CGNSHOME}/bin:$PATH
export %ld_library_path=${CGNSHOME}/lib:${%ld_library_path}
"""
cgnslib_template = misc.PercentTemplate(cgnslib_template)

cgnslib_configuration_template = """
#------ CGNS Library ------
CGNSHOME=$install_dir
ADD_TO_PATH: %(CGNSHOME)s/bin
ADD_TO_$ld_library_path: %(CGNSHOME)s/lib
"""
cgnslib_configuration_template = string.Template(cgnslib_configuration_template)

class CGNSLIB(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "cgnslib-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.config_options += "-DENABLE_HDF5:BOOL=ON "
    self.config_options += "-DCMAKE_BUILD_TYPE:STRING=Release "

    if 'HDF5' not in self.project_softwares_dict:
      hdf5_pkgconfig = 'hdf5-serial'
      if 'ENVHPCOPENMPI' in self.project_softwares_dict or 'OPENMPI' in self.project_softwares_dict:
          hdf5_pkgconfig = 'hdf5-openmpi'
      hdf5librep = '$(pkg-config --libs-only-L %s | cut -c 3-)' % hdf5_pkgconfig
      hdf5increp = '$(pkg-config --cflags-only-I %s | cut -c 3-)' % hdf5_pkgconfig
      self.config_options += '-DCMAKE_LIBRARY_PATH="%s" ' % hdf5librep
      self.config_options += '-DCMAKE_INCLUDE_PATH="%s" ' % hdf5increp

  def get_debian_dev_package(self):
    return 'libcgns-dev'

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["HDF5"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    if dependency_name == "HDF5":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "HDF5":
      self.config_options += "-DHDF5_INCLUDE_PATH=${HDF5_INSTALL_DIR}/include "
      self.config_options += "-DHDF5_LIBRARY=${HDF5_INSTALL_DIR}/lib/libhdf5.so "
      self.executor_software_name += "-hd" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cgnslib_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cgnslib_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

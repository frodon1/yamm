#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CLOOG"

cloog_template = """
#------ cloog ------
export CLOOGHOME="%install_dir"
export PATH=${CLOOGHOME}/bin:${PATH}
export %ld_library_path=${CLOOGHOME}/lib:${%ld_library_path}
"""
cloog_template = misc.PercentTemplate(cloog_template)

cloog_configuration_template = """
#------ cloog ------
CLOOGHOME="$install_dir"
ADD_TO_PATH: %(CLOOGHOME)s/bin
ADD_TO_$ld_library_path: %(CLOOGHOME)s/lib
"""
cloog_configuration_template = string.Template(cloog_configuration_template)

class CLOOG(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "cloog-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "autoconf"
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")
    # if self.use_new_commands() :
    # self.config_options  += "--with-isl=bundled "

  def get_debian_dev_package(self):
    return ['libcloog-isl-dev', 'libcloog-ppl-dev']

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    patches = []
    if self.ordered_version[:6] == "0.18.2":
        patches = ["add-cmake-files.diff"]  # Patch officiel Debian Jessie Paquet : cloog-isl (0.18.2-1 et autres)

    for p in patches:
      self.patches.append(os.path.join(self.patch_directory, p))

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GMP", "MPFR" , "ISL"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "GMP":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MPFR":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "ISL":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    # if self.use_new_commands():
    if dependency_name == "GMP":
      self.config_options += "--with-gmp-prefix=${GMP_INSTALL_DIR} "
      self.executor_software_name += "-gmp" + misc.transform(version_name)
    if dependency_name == "MPFR":
      self.config_options += "--with-mpfr=${MPFR_INSTALL_DIR} "
      self.executor_software_name += "-mpfr" + misc.transform(version_name)
    if dependency_name == "ISL":
      self.config_options += "--with-isl-prefix=${ISL_INSTALL_DIR} "
      self.executor_software_name += "-isl" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cloog_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cloog_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

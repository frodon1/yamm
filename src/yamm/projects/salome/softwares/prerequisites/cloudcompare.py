#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CLOUDCOMPARE"
cloudcompare_template = """
#------ cloudcompare ------
export CLOUDCOMPAREHOME="%install_dir"
export PATH=${CLOUDCOMPAREHOME}/%binrep:$PATH
export %ld_library_path=${CLOUDCOMPAREHOME}/lib:${%ld_library_path}
"""
cloudcompare_template = misc.PercentTemplate(cloudcompare_template)

cloudcompare_configuration_template = """
#------ cloudcompare ------
CLOUDCOMPAREHOME="$install_dir"
ADD_TO_PATH: %(CLOUDCOMPAREHOME)s/$binrep
ADD_TO_$ld_library_path: %(CLOUDCOMPAREHOME)s/lib
"""
cloudcompare_configuration_template = string.Template(cloudcompare_configuration_template)

class CLOUDCOMPARE(SalomeSoftware):
  def __init__(self, name=software_name, version="NOT CONFIGURED",
               verbose=misc.VerboseLevels.INFO, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.binrep = "CloudCompare"

  def init_variables(self):
    if self.ordered_version > 'v2.7':
      self.binrep = "bin"
    self.archive_file_name    = "cloudCompare-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type     = "cmake"
    #self.config_options += " -DQT5_ROOT_PATH:PATH=${QTDIR} "
    self.config_options += " -DINSTALL_QBLUR_PLUGIN:BOOL=ON "
    self.config_options += " -DINSTALL_QCORK_PLUGIN:BOOL=OFF "
    self.config_options += " -DINSTALL_QDUMMY_PLUGIN:BOOL=ON "
    self.config_options += " -DINSTALL_QEDL_PLUGIN:BOOL=ON "
    self.config_options += " -DINSTALL_QHPR_PLUGIN:BOOL=ON "
    self.config_options += " -DINSTALL_QKINECT_PLUGIN:BOOL=OFF "
    self.config_options += " -DINSTALL_QPCL_PLUGIN:BOOL=OFF "
    self.config_options += " -DINSTALL_QPCV_PLUGIN:BOOL=ON "
    self.config_options += " -DINSTALL_QPOISSON_RECON_PLUGIN:BOOL=ON "
    self.config_options += " -DINSTALL_QRANSAC_SD_PLUGIN:BOOL=ON "
    self.config_options += " -DINSTALL_QSRA_PLUGIN:BOOL=OFF "
    self.config_options += " -DINSTALL_QSSAO_PLUGIN:BOOL=ON "
    self.config_options += " -DOPTION_USE_GDAL:BOOL=ON "
    self.config_options += " -DOPTION_USE_SHAPE_LIB:BOOL=ON "
    self.config_options += " -DBoost_DIR:PATH=${BOOSTHOME} "
    self.config_options += " -DGDAL_INCLUDE_DIR:PATH=${GDALHOME}/include "

#   self.config_options += " -DCORK_INCLUDE_DIR:PATH=${CORKHOME}/include "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["QT", "GDAL"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cloudcompare_template.substitute(install_dir=install_dir,
                                            binrep=self.binrep,
                                            ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cloudcompare_configuration_template.substitute(install_dir=install_dir,
                                                          binrep=self.binrep,
                                                          ld_library_path=misc.get_ld_library_path())

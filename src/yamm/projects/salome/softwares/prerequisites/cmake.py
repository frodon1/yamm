#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CMAKE"
cmake_template = """
#------ Cmake -----------
export PATH="%install_dir/bin":$PATH
"""
cmake_template = misc.PercentTemplate(cmake_template)

cmake_configuration_template = """
#------ Cmake -----------
ADD_TO_PATH: "$install_dir/bin"
"""
cmake_configuration_template = string.Template(cmake_configuration_template)

class CMAKE(SalomeSoftware):

  def get_debian_build_depends(self):
    # List taken from Debian control file
    return ["libarchive-dev", "libbz2-dev",
            "libcurl4-openssl-dev", "libcurl-ssl-dev",
            "libexpat1-dev", "libjsoncpp-dev", "liblzma-dev",
            "libncurses5-dev", "procps", "python-sphinx",
            "qtbase5-dev", "zlib1g-dev"]

  def init_variables(self):
    self.archive_file_name = "cmake-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GCC"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "GCC":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cmake_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cmake_configuration_template.substitute(install_dir=install_dir)

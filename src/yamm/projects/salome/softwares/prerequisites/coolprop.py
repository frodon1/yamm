#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "COOLPROP"
coolprop_template = """
#------ coolprop ------
export COOLPROPHOME="%install_dir"
export %ld_library_path=${COOLPROPHOME}/shared_library/Linux/64bit:${%ld_library_path}
"""
coolprop_template = misc.PercentTemplate(coolprop_template)

coolprop_configuration_template = """
#------ coolprop ------
COOLPROPHOME="$install_dir"
ADD_TO_$ld_library_path: %(COOLPROPHOME)s/shared_library/Linux/64bit
"""
coolprop_configuration_template = string.Template(coolprop_configuration_template)

class COOLPROP(SalomeSoftware):
  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)

  def init_variables(self):
    self.archive_file_name    = "CoolProp-" + self.version + ".tgz"
    self.archive_type         = "tar.gz"

    self.compil_type     = "cmake"
    self.config_options += " -DCOOLPROP_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR "
    self.config_options += " -DCOOLPROP_SHARED_LIBRARY=ON "

    self.post_install_commands += ["cp  $CURRENT_SOFTWARE_SRC_DIR/include/CoolProp.h $CURRENT_SOFTWARE_INSTALL_DIR/shared_library/  "]
    self.post_install_commands += ["cp  $CURRENT_SOFTWARE_SRC_DIR/include/PlatformDetermination.h $CURRENT_SOFTWARE_INSTALL_DIR/shared_library/  "]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = []

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return coolprop_template.substitute(install_dir=install_dir,
                                            ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return coolprop_configuration_template.substitute(install_dir=install_dir,
                                                          ld_library_path=misc.get_ld_library_path())

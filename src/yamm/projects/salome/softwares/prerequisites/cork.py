#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CORK"

cork_template = """
#------ cork ------
export CORKHOME="%install_dir"
export PATH=${CORKHOME}/bin:$PATH
export %ld_library_path=${CORKHOME}/lib:${%ld_library_path}
"""
cork_template = misc.PercentTemplate(cork_template)

cork_configuration_template = """
#------ cork ------
CORKHOME="$install_dir"
ADD_TO_PATH: %(CORKHOME)s/bin
ADD_TO_$ld_library_path: %(CORKHOME)s/lib
"""
cork_configuration_template = string.Template(cork_configuration_template)

class CORK(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "cork-" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.compil_type = "specific"

    patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

    self.specific_configure_command = "cd $CURRENT_SOFTWARE_SRC_DIR; "
    self.specific_configure_command += "cp " + os.path.join(patch_directory, "cork_makeConstants") + " makeConstants ; "
    self.specific_configure_command += "patch -p1 < " + os.path.join(patch_directory, "cork.patch") + " ; "
    self.specific_configure_command += "cp -rp $CURRENT_SOFTWARE_SRC_DIR $CURRENT_SOFTWARE_INSTALL_DIR ; "
    self.specific_install_command = "cd $CURRENT_SOFTWARE_INSTALL_DIR; make ; "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GMP"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cork_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cork_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

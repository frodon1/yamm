#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2012, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Boris Daix (EDF R&D)

import os
import string
from yamm.core.base import misc
from yamm.core.base.misc import transform, PercentTemplate
from yamm.projects.salome.software import SalomeSoftware

software_name = "DIET"

diet_template = """
#------ DIET ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
"""
diet_template = PercentTemplate(diet_template)

diet_configuration_template = """
#------ DIET ------
ADD_TO_$ld_library_path: "$install_dir/lib"
"""
diet_tconfiguration_emplate = string.Template(diet_configuration_template)

class DIET(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "diet-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.config_options += "-DDIET_ADMIN_API:BOOL=ON "
    self.config_options += "-DDIET_USE_ALT_BATCH:BOOL=ON "
    self.config_options += "-DDIET_USE_CORI:BOOL=ON "
    self.config_options += "-DDIET_USE_LOG:BOOL=ON "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["BOOST", "OMNIORB", "LOGSERVICE"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    elif dependency_name == "BOOST":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "OMNIORB":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "LOGSERVICE":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + transform(version_name)
    elif dependency_name == "BOOST":
      self.executor_software_name += "-bst" + transform(version_name)
      self.config_options += "-DBOOST_ROOT:PATH=$BOOST_INSTALL_DIR "
    elif dependency_name == "OMNIORB":
      self.executor_software_name += "-orb" + transform(version_name)
      self.config_options += "-DOMNIORB4_DIR:PATH=$OMNIORB_INSTALL_DIR "
    elif dependency_name == "LOGSERVICE":
      self.executor_software_name += "-ls" + transform(version_name)
      self.config_options += "-DLOGSERVICE_DIR:PATH=$LOGSERVICE_INSTALL_DIR "

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return diet_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return diet_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

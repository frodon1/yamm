#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware

software_name = "DISTENE"

DISTENE_template = """
# ------- DISTENE ---------
export DISTENE_LICENSE_FILE="Use global envvar: DLIM8VAR"
export DLIM8VAR="dlim8 1:1:29030@10.122.51.15/005056010130::9de2ce714bcb5ec8ced9926a1087298ebeca6affe852286633abe41cb4a85b4b"
export DISTENE_LICENCE_FILE_FOR_YAMS='"$DISTENE_LICENSE_FILE"'
export DISTENE_LICENCE_FILE_FOR_MGCLEANER='"$DISTENE_LICENSE_FILE"'
"""

DISTENE_configuration_template = """
# ------- DISTENE ---------
DISTENE_LICENSE_FILE="Use global envvar: DLIM8VAR"
DLIM8VAR="dlim8 1:1:29030@10.122.51.15/005056010130::9de2ce714bcb5ec8ced9926a1087298ebeca6affe852286633abe41cb4a85b4b"
DISTENE_LICENCE_FILE_FOR_YAMS='%(DISTENE_LICENSE_FILE)s'
DISTENE_LICENCE_FILE_FOR_MGCLEANER='%(DISTENE_LICENSE_FILE)s'
"""

class DISTENE(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.tips = "It's a YAMM artifact to write the licence information into the prerequisite file"
    self.software_source_type = "software"

  def init_variables(self):
    self.compil_type       = "fake"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    return DISTENE_template

  def get_configuration_str(self, specific_install_dir=""):
    return DISTENE_configuration_template

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "DOCUTILS"
docutils_template = """
#------- docutils ------
export DOCUTILS_ROOT_DIR="%install_dir"
export PATH=${DOCUTILS_ROOT_DIR}/bin:${PATH}
export PYTHONPATH=${DOCUTILS_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
docutils_template = misc.PercentTemplate(docutils_template)
# WARNING, the export of DOCUTILS_ROOT_DIR is used by kernel during Sphinx compilation (Cmake mode), do not remove

docutils_configuration_template = """
#------- docutils ------
DOCUTILS_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(DOCUTILS_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(DOCUTILS_ROOT_DIR)s/lib/python$python_version/site-packages
"""
docutils_configuration_template = string.Template(docutils_configuration_template)

class DOCUTILS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.python_install_dir = "/usr"

  def get_debian_dev_package(self):
    if self._py3:
        return 'python3-docutils'
    return 'python-docutils'

  def init_variables(self):
    self.archive_file_name = "docutils-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"
    self.post_install_commands.append("cd bin ; ln -s rst2html.py rst2html;")
    self.post_install_commands.append("cd bin ; ln -s rst2latex.py rst2latex;")
    self.post_install_commands.append("cd bin ; ln -s rst2man.py rst2man;")
    self.make_movable_archive_commands.append('for f in `find . -name *.py`; do sed -i -e "s|#!%s/bin/python|#!/usr/bin/env python|g" $f; done;' % (self.python_install_dir))

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
      try:
        soft = self.project_softwares_dict['PYTHON']
        if soft.is_python():
          self.python_install_dir = soft.install_directory
      except KeyError:
        pass

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return docutils_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return docutils_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

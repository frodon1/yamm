#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "DOXYGEN"
doxygen_template = """
#------ doxygen ------
export DOXYGEN_ROOT_DIR="%install_dir"
export PATH=${DOXYGEN_ROOT_DIR}/bin:${PATH}
"""
doxygen_template = misc.PercentTemplate(doxygen_template)
# WARNING, DOXYGEN_ROOT_DIR is used by KERNEL during configuration process (CMake mode)

doxygen_configuration_template = """
#------ doxygen ------
DOXYGEN_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(DOXYGEN_ROOT_DIR)s/bin
"""
doxygen_configuration_template = string.Template(doxygen_configuration_template)

class DOXYGEN(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.tips = "Doxygen has to be compiled in it's source directory"
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if self.version == "1.8.0":
      self.patches.append(os.path.join(self.patch_directory, "doxygen-1.8.0.patch"))

  def init_variables(self):
    self.archive_file_name = "doxygen-" + self.version + ".src.tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "configure_with_space"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GRAPHVIZ"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "GRAPHVIZ":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "GRAPHVIZ":
      self.config_options += " --dot $GRAPHVIZ_INSTALL_DIR/bin/dot "
      self.executor_software_name += "-gr" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return doxygen_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return doxygen_configuration_template.substitute(install_dir=install_dir)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "ENVHPCOPENMPI"

envhpcopenmpi_template = """
# ------- ENVHPCOPENMPI ---------
export ENVHPCOPENMPIHOME="${EBROOTOPENMPI}"
export %ld_library_path="${ENVHPCOPENMPIHOME}"/lib:${%ld_library_path}
export PATH="${ENVHPCOPENMPIHOME}"/bin:${PATH}
export MPI_ROOT_DIR="${ENVHPCOPENMPIHOME}"
"""
envhpcopenmpi_template = misc.PercentTemplate(envhpcopenmpi_template)

envhpcopenmpi_configuration_template = """
# ------- ENVHPCOPENMPI ---------
ENVHPCOPENMPIHOME="$${EBROOTOPENMPI}"
ADD_TO_$ld_library_path: %(ENVHPCOPENMPIHOME)s/lib
ADD_TO_PATH: %(ENVHPCOPENMPIHOME)s/bin
MPI_ROOT_DIR=%(ENVHPCOPENMPIHOME)s
"""
envhpcopenmpi_configuration_template = string.Template(envhpcopenmpi_configuration_template)

class ENVHPCOPENMPI(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.tips = "It's a YAMM artifact to write the envhpcopenmpi variables into Salome environment."
    self.software_source_type = "software"

  def init_variables(self):
    self.compil_type = "fake"

  # Astuce inelegante pour s'assurer que MPI4PY sera présent car dependance
  # de H5py uniquement en mode MPI.
  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["MPI4PY"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self):
    return envhpcopenmpi_template.substitute(ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self):
    return envhpcopenmpi_configuration_template.substitute(ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "EXPAT"
expat_template = """
#------ expat ------
EXPAT_DIR="%install_dir"
export PATH=${EXPAT_DIR}/bin:${PATH}
export %ld_library_path=${EXPAT_DIR}/lib:${%ld_library_path}
"""
expat_template = misc.PercentTemplate(expat_template)

expat_configuration_template = """
#------ expat ------
EXPAT_DIR="$install_dir"
ADD_TO_PATH: %(EXPAT_DIR)s/bin
ADD_TO_$ld_library_path: %(EXPAT_DIR)s/lib
"""
expat_configuration_template = string.Template(expat_configuration_template)

class EXPAT(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name    = "expat-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "autoconf"

  def get_debian_dev_package(self):
    return 'libexpat1-dev'

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return expat_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return expat_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

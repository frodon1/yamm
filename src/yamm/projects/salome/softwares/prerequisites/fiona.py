#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "FIONA"

fiona_template = """
#------ fiona ------
export FIONAHOME="%install_dir"
export PYTHONPATH="${FIONAHOME}/lib/python%python_version/site-packages":${PYTHONPATH}
"""
fiona_template = misc.PercentTemplate(fiona_template)

fiona_configuration_template = """
#------ fiona ------
FIONAHOME="$install_dir"
ADD_TO_PYTHONPATH: %(FIONAHOME)s/lib/python$python_version/site-packages
"""
fiona_configuration_template = string.Template(fiona_configuration_template)

class FIONA(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name    = "Fiona-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type          = "python"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS"]
    self.software_dependency_dict['exec'] =  ["PYTHON","GDAL"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return fiona_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return fiona_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

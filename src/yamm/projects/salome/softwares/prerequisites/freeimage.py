# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud NEDELEC (OCC)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "FREEIMAGE"
FreeImage_template = """
#------ FreeImage ------
FREEIMAGE_ROOT="%install_dir"
export FreeImage_version=%FreeImage_version
export %ld_library_path=${FREEIMAGE_ROOT}/lib:${%ld_library_path}
"""
FreeImage_template = misc.PercentTemplate(FreeImage_template)

FreeImage_configuration_template = """
#------ FreeImage ------
FREEIMAGE_ROOT="$install_dir"
export FreeImage_version=$FreeImage_version
ADD_TO_$ld_library_path: %(FREEIMAGE_ROOT)s/lib
"""
FreeImage_configuration_template = string.Template(FreeImage_configuration_template)

class FREEIMAGE(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
    return 'libfreeimage-dev'

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if self.version == "3.15.4":
      self.patches.append(os.path.join(self.patch_directory, "freeimage_include_missing_and_fix_install.patch"))

  def init_variables(self):
    self.archive_file_name    = "FreeImage-" + self.version + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "specific"
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_configure_command = "cp -r $CURRENT_SOFTWARE_SRC_DIR/* $CURRENT_SOFTWARE_BUILD_DIR;"
    if 'occ6' in self.ordered_version:
      self.specific_build_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make -j%s -f Makefile.fip ;" %parallel_make
      self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make DESTDIR=$CURRENT_SOFTWARE_INSTALL_DIR -f Makefile.fip install;"
    else:
      self.specific_build_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make -j%s -f Makefile.gnu ;" %parallel_make
      self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make DESTDIR=$CURRENT_SOFTWARE_INSTALL_DIR -f Makefile.gnu install;"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return FreeImage_template.substitute(FreeImage_version=self.ordered_version, install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return FreeImage_configuration_template.substitute(FreeImage_version=self.ordered_version, install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

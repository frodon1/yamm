# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud NEDELEC (OCC)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "FREETYPE"
freetype_template = """
#------ freetype ------
FREETYPE_ROOT="%install_dir"
export PATH=${FREETYPE_ROOT}/bin:$PATH
export %ld_library_path=${FREETYPE_ROOT}/lib:${%ld_library_path}
"""
freetype_template = misc.PercentTemplate(freetype_template)

freetype_configuration_template = """
#------ freetype ------
FREETYPE_ROOT="$install_dir"
ADD_TO_PATH: %(FREETYPE_ROOT)s/bin
ADD_TO_$ld_library_path: %(FREETYPE_ROOT)s/lib
"""
freetype_configuration_template = string.Template(freetype_configuration_template)

class FREETYPE(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'libfreetype6-dev'

  def init_variables(self):
    self.archive_file_name    = "freetype-" + self.version + ".tar.bz2"
    self.archive_type         = "tar.bz2"
    self.compil_type          = "autoconf"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return freetype_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return freetype_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

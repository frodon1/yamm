#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "FTGL"
ftgl_template = """
#------ FTGL ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
"""
ftgl_template = misc.PercentTemplate(ftgl_template)

ftgl_configuration_template = """
#------ FTGL ------
ADD_TO_$ld_library_path: "$install_dir/lib"
"""
ftgl_configuration_template = string.Template(ftgl_configuration_template)

class FTGL(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "ftgl-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    if self.version[:5] == "2.1.2":
      self.specific_configure_command = "cd unix ; ./configure --enable-shared --with-gl-inc=/usr/include/GL --with-gl-lib=/usr/lib --prefix=$CURRENT_SOFTWARE_INSTALL_DIR "
      self.specific_build_command = "cd unix ; make "
      self.specific_install_command = "cd $CURRENT_SOFTWARE_SRC_DIR/unix ; make install "
    else:
      self.specific_configure_command = "./configure --enable-shared --with-gl-inc=/usr/include/GL --with-gl-lib=/usr/lib --prefix=$CURRENT_SOFTWARE_INSTALL_DIR "
      self.specific_build_command = "make "
      self.specific_install_command = "cd $CURRENT_SOFTWARE_SRC_DIR ; make install "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["DOXYGEN"]
    self.software_dependency_dict['exec'] = []

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "DOXYGEN":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "DOXYGEN":
      self.config_options += "-DDOXYGEN_EXECUTABLE=${DOXYGEN_INSTALL_DIR}/bin/doxygen "
      self.executor_software_name += "-dox" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ftgl_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ftgl_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

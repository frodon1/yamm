#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "FUTURE"
future_template = """
#------- future ------
export FUTURE_ROOT_DIR="%install_dir"
export PATH=${FUTURE_ROOT_DIR}/bin:${PATH}
export PYTHONPATH=${FUTURE_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
future_template = misc.PercentTemplate(future_template)
# WARNING, the export of FUTURE_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

future_configuration_template = """
#------- future ------
FUTURE_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(FUTURE_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(FUTURE_ROOT_DIR)s/lib/python$python_version/site-packages
"""
future_configuration_template = string.Template(future_configuration_template)
# WARNING, the export of FUTURE_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

class FUTURE(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'python-future'

  def init_variables(self):
    self.archive_file_name = "Future-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"
    force_ftp_server = self.project_options.get_global_option("python_prereq_force_ftp_server")
    if force_ftp_server:
      prerequisites_server = self.project_options.get_global_option("python_prerequisites_ftp_server")
    else:
      prerequisites_server = self.project_options.get_global_option("python_prerequisites_server")
    if prerequisites_server:
        self.build_options = " develop --find-links=" + prerequisites_server

    self.replaceSelfInstallPath("lib/python2.7/site-packages/easy-install.pth")
    self.replaceSelfInstallPath("easy-install.pth")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON"]#MP, "PYGMENTS", "JINJA",
                                           #MP  "DOCUTILS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    elif dependency_name == "SETUPTOOLS":
      dependency_object.depend_of = ["python_path"]
#MP    elif dependency_name == "PYGMENTS":
#MP      dependency_object.depend_of = ["python_path"]
#MP    elif dependency_name == "JINJA":
#MP      dependency_object.depend_of = ["python_path"]
#MP    elif dependency_name == "DOCUTILS":
#MP      dependency_object.depend_of = ["python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
#MP      self.replacePythonHeaderInstallPath("bin/future-build")
#MP      self.replacePythonHeaderInstallPath("bin/future-quickstart")
#MP      self.replacePythonHeaderInstallPath("bin/future-autogen")
#MP      self.replacePythonHeaderInstallPath("bin/pygmentize")
    elif dependency_name == "SETUPTOOLS":
      self.executor_software_name += "-set" + misc.transform(version_name)
#MP    elif dependency_name == "PYGMENTS":
#MP      self.executor_software_name += "-pyg" + misc.transform(version_name)
#MP      self.replaceSoftwareInstallPath(dependency_name, "lib/python%s/site-packages/easy-install.pth" % (self.python_version))
#MP      self.replaceSoftwareInstallPath(dependency_name, "easy-install.pth")
#MP    elif dependency_name == "JINJA":
#MP      self.executor_software_name += "-jin" + misc.transform(version_name)
#MP      self.replaceSoftwareInstallPath(dependency_name, "lib/python%s/site-packages/easy-install.pth" % (self.python_version))
#MP      self.replaceSoftwareInstallPath(dependency_name, "easy-install.pth")
#MP    elif dependency_name == "DOCUTILS":
#MP      self.executor_software_name += "-doc" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return future_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return future_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

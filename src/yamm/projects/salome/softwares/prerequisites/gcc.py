#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GCC"

gcc_template = """
#------ gcc ------
export GCCHOME="%install_dir"
export PATH=${GCCHOME}/bin:${PATH}
export %ld_library_path=${GCCHOME}/lib:${GCCHOME}/lib64:${%ld_library_path}
export CXX=${GCCHOME}/bin/g++
export CPP=${GCCHOME}/bin/cpp
export CXXCPP=${GCCHOME}/bin/cpp
export F77=${GCCHOME}/bin/gfortran
export CC=${GCCHOME}/bin/gcc
"""
gcc_template = misc.PercentTemplate(gcc_template)

gcc_configuration_template = """
#------ gcc ------
GCCHOME="$install_dir"
ADD_TO_PATH: %(GCCHOME)s/bin
ADD_TO_$ld_library_path: %(GCCHOME)s/lib64
ADD_TO_$ld_library_path: %(GCCHOME)s/lib
CXX=%(GCCHOME)s/bin/g++
CPP=%(GCCHOME)s/bin/cpp
CXXCPP=%(GCCHOME)s/bin/cpp
F77=%(GCCHOME)s/bin/gfortran
CC=%(GCCHOME)s/bin/gcc
"""
gcc_configuration_template = string.Template(gcc_configuration_template)

class GCC(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "gcc-" + self.version + ".tar.bz2"
    self.archive_type = "tar.bz2"

    self.compil_type = "autoconf"
    self.config_options += " -v --enable-languages=c,c++,fortran "
    self.config_options += " --enable-shared "
    self.config_options += " --enable-threads=posix "
    self.config_options += " --enable-bootstrap "
    self.config_options += " --enable-nls "
    self.config_options += " --enable-__cxa_atexit "
    self.config_options += " --enable-checking=release "
    # La version de gcc doit être juste après 'GCC' sinon Numpy ne la détecte pas
    self.config_options += " --with-pkgversion='GCC %s PR-2015/02/04' " % self.version
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_build_command = "make -j%s bootstrap "  %(parallel_make)

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GMP", "MPFR", "MPC", "CLOOG",
                                             "BINUTILS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "GMP":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MPFR":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MPC":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "CLOOG":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BINUTILS":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    # if self.use_new_commands():
    if dependency_name == "GMP":
      self.config_options += "--with-gmp=${GMP_INSTALL_DIR} "
      self.executor_software_name += "-gmp" + misc.transform(version_name)
    if dependency_name == "MPFR":
      self.config_options += "--with-mpfr=${MPFR_INSTALL_DIR} "
      self.executor_software_name += "-mpfr" + misc.transform(version_name)
    if dependency_name == "MPC":
      self.config_options += "--with-mpc=${MPC_INSTALL_DIR} "
      self.executor_software_name += "-mpc" + misc.transform(version_name)
    if dependency_name == "CLOOG":
      self.config_options += "--with-cloog=${CLOOG_INSTALL_DIR} "
      self.executor_software_name += "-cloog" + misc.transform(version_name)
    if dependency_name == "BINUTILS":
      self.config_options += "--with-binutils=${BINUTILS_INSTALL_DIR} "
      self.executor_software_name += "-bin" + misc.transform(version_name)


  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gcc_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gcc_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

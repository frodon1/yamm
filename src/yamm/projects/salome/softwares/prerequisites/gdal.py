#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GDAL"

gdal_template = """
#------ gdal ------
export GDALHOME="%install_dir"
export PATH=${GDALHOME}/bin:${PATH}
export %ld_library_path=${GDALHOME}/lib:${%ld_library_path}
export PYTHONPATH="%install_dir"/lib/python%python_version/site-packages:${PYTHONPATH}
export GDAL_DATA=${GDALHOME}/share/gdal
"""
gdal_template = misc.PercentTemplate(gdal_template)

gdal_configuration_template = """
#------ gdal ------
GDALHOME="$install_dir"
ADD_TO_PATH: %(GDALHOME)s/bin
ADD_TO_$ld_library_path: %(GDALHOME)s/lib
ADD_TO_PYTHONPATH: "$install_dir/lib/python$python_version/site-packages"
GDAL_DATA=%(GDALHOME)s/share/gdal
"""
gdal_configuration_template = string.Template(gdal_configuration_template)

class GDAL(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "gdal-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "specific"
    self.specific_configure_command = "cd $CURRENT_SOFTWARE_SRC_DIR; "
    self.specific_configure_command += ' ./configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR '
    self.specific_configure_command += " --with-pcraster=internal "
    self.specific_configure_command += " --with-png=internal "
    self.specific_configure_command += " --with-libtiff=internal "
    self.specific_configure_command += " --with-geotiff=internal "
    self.specific_configure_command += " --with-jpeg=internal "
    self.specific_configure_command += " --with-gif=internal "
    self.specific_configure_command += " --with-python --without-libtool "
    self.specific_configure_command += " --with-geos=yes "
    self.specific_configure_command += " --with-sqlite3=${SQLITE_ROOT}"
    self.specific_configure_command += " --with-hdf5=${HDF5HOME} "

    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_build_command = "cd $CURRENT_SOFTWARE_SRC_DIR; make -j%s; " %(parallel_make)
    self.specific_install_command = "cd $CURRENT_SOFTWARE_SRC_DIR; make install;"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GEOS", "HDF5", "SQLITE"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gdal_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gdal_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

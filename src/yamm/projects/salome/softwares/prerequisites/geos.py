#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GEOS"

geos_template = """
#------ geos ------
export GEOSHOME="%install_dir"
export PATH=${GEOSHOME}/bin:${PATH}
export %ld_library_path=${GEOSHOME}/lib:${%ld_library_path}
export PYTHONPATH="${GEOSHOME}/lib/python%python_version/site-packages":${PYTHONPATH}
"""
geos_template = misc.PercentTemplate(geos_template)

geos_configuration_template = """
#------ geos ------
GEOSHOME="$install_dir"
ADD_TO_PATH: %(GEOSHOME)s/bin
ADD_TO_$ld_library_path: %(GEOSHOME)s/lib
ADD_TO_PYTHONPATH: %(GEOSHOME)s/lib/python$python_version/site-packages
"""
geos_configuration_template = string.Template(geos_configuration_template)

class GEOS(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "geos-" + self.version + ".tar.bz2"
    self.archive_type = "tar.bz2"

    self.compil_type = "autoconf"
    self.config_options += " --enable-python "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return geos_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return geos_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

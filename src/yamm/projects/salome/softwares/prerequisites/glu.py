#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GLU"

class GLU(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "glu-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.install_dir = "keep"

    self.compil_type = "autoconf"
    self.config_options = " -enable-osmesa "
    self.config_options += " --enable-shared --disable-static "
    try:
      soft = self.project_softwares_dict['OSMESA']
      self.make_movable_archive_commands += soft.make_movable_archive_commands
      self.install_directory = soft.install_directory
    except KeyError:
      self.logger.error("OSMESA est absent")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["OSMESA"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "OSMESA":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "OSMESA":
      self.user_dependency_command = "export PKG_CONFIG_PATH=${OSMESA_INSTALL_DIR}/lib/pkgconfig"

  def get_type(self):
    return "prerequisite"

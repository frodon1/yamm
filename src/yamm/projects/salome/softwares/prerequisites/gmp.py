#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GMP"

gmp_template = """
#------ gmp ------
export GMPHOME="%install_dir"
export %ld_library_path=${GMPHOME}/lib:${%ld_library_path}
"""
gmp_template = misc.PercentTemplate(gmp_template)

gmp_configuration_template = """
#------ gmp ------
GMPHOME="$install_dir"
ADD_TO_$ld_library_path: %(GMPHOME)s/lib
"""
gmp_configuration_template = string.Template(gmp_configuration_template)

class GMP(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "gmp-" + self.version + ".tar.bz2"
    self.archive_type = "tar.bz2"

    self.compil_type = "autoconf"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["AUTOCONF", "LIBTOOL"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "AUTOCONF":
      dependency_object.depend_of = ["path"]
    if dependency_name == "LIBTOOL":
      dependency_object.depend_of = ["path"]
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gmp_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gmp_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author

import os
import string

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware


software_name = "GMSH"
gmsh_template = """
#---------- gmsh -------------------
export GMSHHOME="%install_dir"
export PATH=${GMSHHOME}/bin:${PATH}
export %ld_library_path=${GMSHHOME}/lib:${%ld_library_path}
"""
gmsh_template = misc.PercentTemplate(gmsh_template)

gmsh_configuration_template = """
#------ Gmsh ------
GMSH_DIR="$install_dir"
ADD_TO_PATH: %(GMSH_DIR)s/bin
ADD_TO_$ld_library_path: %(GMSH_DIR)s/lib
"""
gmsh_configuration_template = string.Template(gmsh_configuration_template)


class GMSH(SalomeSoftware):

    def get_debian_dev_package(self):
        return 'libgmsh-dev'

    def init_variables(self):
        self.software_source_type = "archive"
        self.archive_file_name = "gmsh-" + self.version + "-source.tar.gz"
        self.archive_type = "tar.gz"
        self.compil_type = "cmake"

        # Gmsh general settings

        self.config_options += "-DENABLE_ACIS=0 "
        # obligation to keep ENABLE_ANN by desactivating this line because
        # it seem to to lack a method in GFaceCompound if ANN not define in gmsh 2.8.2
        # self.config_options += "-DENABLE_ANN=0 "
        self.config_options += "-DENABLE_BUILD_SHARED=1 "
        self.config_options += "-DENABLE_FLTK=0 "
        self.config_options += "-DENABLE_MED=0 "
        # self.config_options += "-DENABLE_MPEG_ENCODE=0 " #seems to not be taken into account during configure
        self.config_options += "-DENABLE_ONELAB_METAMODEL=0 "
        self.config_options += "-DENABLE_PARSER=ON "
        # self.config_options += "-DENABLE_PLUGINS=0 " #seems to not be taken into account during configure
        # obligation to keep ENABLE_POST by desactivating this line because
        # it seem to to lack a "if define(HAVE_POST)" in a Mesh subroutine in gmsh 2.7.0
        # self.config_options += "-DENABLE_POST=0 "
        self.config_options += "-DENABLE_SALOME=0 "
        # Disable petsc because compilation fails if mpi is not installed
        self.config_options += "-DENABLE_PETSC=0 "

        self.post_install_commands.append('find $CURRENT_SOFTWARE_SRC_DIR -type f -name "*.h" '
                                          '-exec cp {} $CURRENT_SOFTWARE_INSTALL_DIR/include/ \; > copy_includes.log ')
        self.post_install_commands.append('mv $CURRENT_SOFTWARE_INSTALL_DIR/include/gmsh/* '
                                          '$CURRENT_SOFTWARE_INSTALL_DIR/include/ && '
                                          'rmdir $CURRENT_SOFTWARE_INSTALL_DIR/include/gmsh/ >> copy_includes.log')

    def init_dependency_list(self):
        self.software_dependency_dict['build'] = ["CMAKE"]
        self.software_dependency_dict['exec'] = ["OCC", "LAPACK", "TBB"]

    def get_dependency_object_for(self, dependency_name):
        dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
        if dependency_name == "OCC":
            dependency_object.depend_of = ["ld_lib_path"]
        return dependency_object

    def update_configuration_with_dependency(self, dependency_name, version_name):
        if dependency_name == "CMAKE":
            self.executor_software_name += "-cm" + misc.transform(version_name)
        elif dependency_name == "OCC":
            self.executor_software_name += "-occ" + misc.transform(version_name)
        elif dependency_name == "LAPACK":
            self.executor_software_name += "-la" + misc.transform(version_name)
        elif dependency_name == "TBB":
            self.config_options += "--with-tbb-incdir=$TBB_INSTALL_DIR/include "
            self.config_options += "--with-tbb-libdir=$TBB_INSTALL_DIR/lib "
            self.executor_software_name += "-tbb" + misc.transform(version_name)

    def get_type(self):
        return "prerequisite"

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return gmsh_template.substitute(install_dir=install_dir,
                                        ld_library_path=misc.get_ld_library_path())

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return gmsh_configuration_template.substitute(install_dir=install_dir,
                                                      ld_library_path=misc.get_ld_library_path())

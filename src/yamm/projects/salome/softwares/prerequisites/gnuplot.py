#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas Geimer (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GNUPLOT"
gnuplot_template = """
#------ gnuplot ------
export GNUPLOT_ROOT="%install_dir"
export PATH=${GNUPLOT_ROOT}/bin:$PATH
export %ld_library_path=${GNUPLOT_ROOT}/lib:${%ld_library_path}
"""
gnuplot_template = misc.PercentTemplate(gnuplot_template)

gnuplot_configuration_template = """
#------ gnuplot ------
GNUPLOT_ROOT="$install_dir"
ADD_TO_PATH: %(GNUPLOT_ROOT)s/bin
ADD_TO_$ld_library_path: %(GNUPLOT_ROOT)s/lib
"""
gnuplot_configuration_template = string.Template(gnuplot_configuration_template)

class GNUPLOT(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "gnuplot-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["QT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "QT":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "QT":
      self.executor_software_name += "-qt" + misc.transform(version_name)
      soft = self.project_softwares_dict['QT']
      qt_dir = soft.install_directory
      self.user_dependency_command = "export QT_CFLAGS='-DQT_SHARED -I{0}/include -I{0}/include/QtCore ".format(qt_dir)
      self.user_dependency_command += "-I{0}/include/QtGui -I{0}/include/QtNetwork -I{0}/include/QtSvg' ; ".format(qt_dir)
      self.user_dependency_command += " export UIC={0}/bin/uic ; ".format(qt_dir)
      self.user_dependency_command += " export QT_LIBS='-L{0}/lib -lQtNetwork -lQtSvg -lQtGui -lQtCore ' ; ".format(qt_dir)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gnuplot_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gnuplot_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

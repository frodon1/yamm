#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GPSBABEL"

gpsbabel_template = """
#------ gpsbabel ------
export GPSBABELHOME="%install_dir"
export PATH=${GPSBABELHOME}/bin:${PATH}
"""
gpsbabel_template = misc.PercentTemplate(gpsbabel_template)

gpsbabel_configuration_template = """
#------ gpsbabel ------
GPSBABELHOME="$install_dir"
ADD_TO_PATH: %(GPSBABELHOME)s/bin
"""
gpsbabel_configuration_template = string.Template(gpsbabel_configuration_template)

class GPSBABEL(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name    = "gpsbabel-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type          = "specific"
    
    patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
    
    self.specific_configure_command  = "cd $CURRENT_SOFTWARE_SRC_DIR; "
    self.specific_configure_command  += " patch -p1 < " + os.path.join(patch_directory, "gpsbabel.patch") + " ; "
    self.specific_configure_command  += " aclocal ; libtoolize --force --copy --automake ; autoconf ; "
    self.specific_configure_command  += " ./configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR ; "
    self.specific_build_command = "cd $CURRENT_SOFTWARE_SRC_DIR; make -j{0}; ".format(self.parallel_make)
    self.specific_install_command = "cd $CURRENT_SOFTWARE_SRC_DIR; make install;"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["QT"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gpsbabel_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gpsbabel_configuration_template.substitute(install_dir=install_dir)

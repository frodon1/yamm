#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GRACE"
grace_template = """
#------ grace ------
GRACEHOME="%install_dir"
export PATH=${GRACEHOME}/grace/bin:${PATH}
export %ld_library_path=${GRACEHOME}/grace/lib:${%ld_library_path}
"""
grace_template = misc.PercentTemplate(grace_template)

grace_configuration_template = """
#------ grace ------
GRACEHOME="$install_dir"
ADD_TO_PATH: %(GRACEHOME)s/grace/bin
ADD_TO_$ld_library_path: %(GRACEHOME)s/grace/lib
"""
grace_configuration_template = string.Template(grace_configuration_template)

class GRACE(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "grace-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory
    if(misc.get_calibre_version() == "7"):
      self.config_options += " --with-extra-incpath=$LESSTIF_INSTALL_DIR/include"
      self.config_options += " --with-extra-ldpath=$LESSTIF_INSTALL_DIR/lib"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["LESSTIF"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "LESSTIF":
      dependency_object.depend_of = ["install_path", "ld_lib_path"]
    return dependency_object


  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return grace_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return grace_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GRAPHVIZ"
graphviz_template = """
#------ graphviz ------
GRAPHVIZHOME="%install_dir"
export PATH=${GRAPHVIZHOME}/bin:${PATH}
export %ld_library_path=${GRAPHVIZHOME}/lib/graphviz:${GRAPHVIZHOME}/lib:${%ld_library_path}
"""
graphviz_template = misc.PercentTemplate(graphviz_template)

graphviz_configuration_template = """
#------ graphviz ------
GRAPHVIZHOME="$install_dir"
ADD_TO_PATH: %(GRAPHVIZHOME)s/bin
ADD_TO_$ld_library_path: %(GRAPHVIZHOME)s/lib/graphviz:%(GRAPHVIZHOME)s/lib
"""
graphviz_configuration_template = string.Template(graphviz_configuration_template)

class GRAPHVIZ(SalomeSoftware):

  def get_debian_dev_package(self):
    return ['graphviz-dev']

  def init_variables(self):
    self.archive_file_name = "graphviz-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    parallel_make = self.project_options.get_option(self.name, "parallel_make"),

    if misc.split_version(self.ordered_version) >= [2, 28, 0]:
      self.compil_type = "specific"
      self.specific_configure_command = "cd $CURRENT_SOFTWARE_SRC_DIR ; ./configure --enable-perl=no --enable-java=no --enable-tcl=no --enable-python=no --with-qt=no --with-pangocairo=yes --prefix=$CURRENT_SOFTWARE_INSTALL_DIR "
      self.specific_build_command = "cd $CURRENT_SOFTWARE_SRC_DIR ; make -j%s " % (parallel_make)
      self.specific_install_command = "cd $CURRENT_SOFTWARE_SRC_DIR ; make install"
    else:
      self.compil_type = "autoconf"
      self.config_options += " --enable-perl=no --enable-java=no --enable-tcl=no --enable-python=no --with-pangocairo=no "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["EXPAT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "EXPAT":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "EXPAT":
      self.config_options += " --with-expatincludedir=$EXPAT_INSTALL_DIR/include --with-expatlibdir=$EXPAT_INSTALL_DIR/lib "
      self.executor_software_name += "-ex" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return graphviz_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return graphviz_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

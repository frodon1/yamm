#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GRASS"

grass_template = """
#------ grass ------
export GRASSHOME="%install_dir"
export PATH=${GRASSHOME}/bin:${PATH}
export GRASSPREFIX=${GRASSHOME}/grass-"%version"
"""
grass_template = misc.PercentTemplate(grass_template)

grass_configuration_template = """
#------ grass ------
GRASSHOME="$install_dir"
ADD_TO_PATH: %(GRASSHOME)s/bin
GRASSPREFIX=%(GRASSHOME)s/grass-"$version"
"""
grass_configuration_template = string.Template(grass_configuration_template)

class GRASS(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "grass-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "specific"
    self.specific_configure_command = "cd $CURRENT_SOFTWARE_SRC_DIR; "
    self.specific_configure_command += ' CPPFLAGS="-I${NCURSESHOME}/include -I${NCURSESHOME}/include/ncurses" '
    self.specific_configure_command += ' CFLAGS="-fexceptions" LDFLAGS=-L${NCURSESHOME}/lib '
    self.specific_configure_command += ' ./configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR '
    self.specific_configure_command += " --enable-64bit --with-cxx --with-readline "
    self.specific_configure_command += " --with-python --with-sqlite --with-curses"
    self.specific_configure_command += " --with-gdal=${GDALHOME}/bin/gdal-config "
    self.specific_configure_command += " --with-geos=${GEOSHOME}/bin/geos-config "
    self.specific_configure_command += " --with-freetype-includes=${FREETYPE_ROOT}/include/freetype2 "
    self.specific_configure_command += " --with-freetype-libs=${FREETYPE_ROOT}/lib "
    self.specific_configure_command += " --with-proj-includes=${PROJHOME}/include "
    self.specific_configure_command += " --with-proj-libs=${PROJHOME}/lib "
    self.specific_configure_command += " --with-proj-share=${PROJHOME}/share/proj "
    self.specific_configure_command += " --with-tcltk-includes=${TCLDIR}/include "
    self.specific_configure_command += " --with-tcltk-libs=${TCLDIR}/lib "
    self.specific_configure_command += " --with-sqlite-includes=${SQLITE_ROOT}/include "
    self.specific_configure_command += " --with-sqlite-libs=${SQLITE_ROOT}/lib "

    parallel_make = self.project_options.get_option(self.name, "parallel_make")
    self.specific_build_command = "make -j{0} && " \
                                  "cd gui/wxpython ; make -j{0} && " \
                                  "cd ../.. ; make -j{0} ; ".format(parallel_make)
    self.specific_install_command = "cd $CURRENT_SOFTWARE_SRC_DIR; make install;"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GEOS", "PROJ", "GDAL", "TCL",
                                             "SQLITE", "FREETYPE", "NCURSES"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return grass_template.substitute(install_dir=install_dir, version=self.version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return grass_configuration_template.substitute(install_dir=install_dir, version=self.version)

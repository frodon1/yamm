#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "H5PY"
H5PY_template = """
#------- h5py ------
export PYTHONPATH=%install_dir/lib/python%python_version/site-packages:${PYTHONPATH}
"""
H5PY_template = misc.PercentTemplate(H5PY_template)

H5PY_configuration_template = """
#------- h5py ------
ADD_TO_PYTHONPATH: $install_dir/lib/python$python_version/site-packages
"""
H5PY_configuration_template = string.Template(H5PY_configuration_template)

class H5PY(SalomeSoftware):

  def get_debian_dev_package(self):
    if self._py3:
        return 'python3-h5py'
    return 'python-h5py'

  def init_variables(self):
    self.archive_file_name = "h5py-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"

    # Trick to force H5py to use compatible verions Pkgconfig and nose which is a 3rd order prerequisites (Pkgconfig)
    self.pre_configure_commands += ["sed -i \"s/^  setup_requires.*/  setup_requires = [\'pkgconfig>=1.1.0\', \'nose>=1.3.7\', \'six\'],/\" setup.py"]
    # Trick to force H5py dated setup_requires mechanism to download packages from FTP pleiade
    force_ftp_server = self.project_options.get_global_option("python_prereq_force_ftp_server")
    if force_ftp_server:
      prerequisites_server = self.project_options.get_global_option("python_prerequisites_ftp_server")
    else:
      prerequisites_server = self.project_options.get_global_option("python_prerequisites_server")
      prerequisites_server = os.path.join(prerequisites_server, 'simple/')
    if prerequisites_server:
      self.pre_configure_commands += ["echo \"[easy_install]\" >> setup.cfg"]
      self.pre_configure_commands += ["echo \"index_url = %s\n\" >> setup.cfg" % prerequisites_server]
      self.build_options = " develop --find-links=" + prerequisites_server

  def init_dependency_list(self):
    # ATTENTION : dependance supplementaire en mode MPI a MPI4PY qui est gere
    # dans le software OPENMPI ajoute uniquement avec la saveur mpi.
    self.software_dependency_dict['build'] = ["SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON", "NUMPY", "HDF5",
                                             "CYTHON","MPI4PY"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
    elif dependency_name == "NUMPY":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "HDF5":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "CYTHON":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "SETUPTOOLS":
      dependency_object.depend_of = ["python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "NUMPY":
      self.executor_software_name += "-num" + misc.transform(version_name)
    elif dependency_name == "HDF5":
      self.executor_software_name += "-hdf5" + misc.transform(version_name)
      self.config_options += " --hdf5=$HDF5_INSTALL_DIR"
    elif dependency_name == "CYTHON":
      self.executor_software_name += "-cyt" + misc.transform(version_name)
    elif dependency_name == "SETUPTOOLS":
      self.executor_software_name += "-set" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return H5PY_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return H5PY_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

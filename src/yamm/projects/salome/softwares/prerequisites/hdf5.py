#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "HDF5"
hdf5_template = """
#------ hdf5 ------
export HDF5HOME="%install_dir"
export PATH=${HDF5HOME}/bin:$PATH
export %ld_library_path=${HDF5HOME}/lib:${%ld_library_path}
export HDF5_DISABLE_VERSION_CHECK=1
"""
hdf5_template = misc.PercentTemplate(hdf5_template)

hdf5_configuration_template = """
#------ hdf5 ------
HDF5HOME="$install_dir"
ADD_TO_PATH: %(HDF5HOME)s/bin
ADD_TO_$ld_library_path: %(HDF5HOME)s/lib
HDF5_DISABLE_VERSION_CHECK=1
"""
hdf5_configuration_template = string.Template(hdf5_configuration_template)

class HDF5(SalomeSoftware):

  def get_debian_dev_package(self):
    if 'ENVHPCOPENMPI' in self.project_softwares_dict or 'OPENMPI' in self.project_softwares_dict:
      return ['libhdf5-openmpi-dev', 'hdf5-tools']
    else:
      if misc.get_calibre_version() == '7':
        return ['libhdf5-serial-dev', 'hdf5-tools']
      else:
        return ['libhdf5-dev', 'hdf5-tools']

  def init_variables(self):

    self.archive_file_name = "hdf5-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    if misc.minor(self.version) >= 10:
      self.compil_type="autoconf"
      self.config_options =  "--enable-hl "# Build Hih Level library set to ON by default in 1.10 (was OFF in 1.8.x versions)
      self.config_options += "" # Build Tools set to ON by default in 1.10 (was OFF in 1.8.x versions)
      self.config_options += "--enable-shared "
      self.config_options += "--with-zlib "
    else:
      self.compil_type = "cmake"
      self.config_options = "-DHDF5_BUILD_HL_LIB=ON "
      self.config_options += "-DHDF5_BUILD_TOOLS=ON "
      self.config_options += "-DBUILD_SHARED_LIBS=ON "
      self.config_options += "-DHDF5_ENABLE_Z_LIB_SUPPORT=ON "  # ZLib support is required at least by Cocagne

    patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
    patch_script = os.path.join(patch_directory, "patchHDF5Build.py")
    self.pre_build_commands.append("cd " + self.build_directory + " ; " + " python " + patch_script)

  def init_dependency_list(self):
    if misc.minor(self.version) < 10:
      self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = []

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    dependency_object.depend_of = ["path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if misc.minor(self.version) < 10:
      self.executor_software_name += "-cm" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return hdf5_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return hdf5_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

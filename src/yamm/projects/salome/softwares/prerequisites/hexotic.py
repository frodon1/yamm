#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "HEXOTIC"
hexotic_template = "# ----- Hexotic -------------"
if misc.is64():
  hexotic_template += """
export PATH="%install_dir/Linux_64":$PATH
"""
else:
  hexotic_template += """
export PATH="%install_dir/Linux":$PATH
"""
hexotic_template = misc.PercentTemplate(hexotic_template)

hexotic_configuration_template = "# ----- Hexotic -------------"
if misc.is64():
  hexotic_configuration_template += """
ADD_TO_PATH: "$install_dir/Linux_64"
"""
else:
  hexotic_configuration_template += """
ADD_TO_PATH: "$install_dir/Linux"
"""
hexotic_configuration_template = string.Template(hexotic_configuration_template)

class HEXOTIC(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "Hexotic-" + self.version + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "rsync"
    # EDF specifics
    if misc.is64():
      self.archive_address = os.path.join(self.archive_address, "..", "bin64")
    else:
      self.archive_address = os.path.join(self.archive_address, "..", "bin32")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["DISTENE"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    if misc.split_version(self.ordered_version)[:2] >= [1, 0]:
      install_dir = os.path.join(install_dir, "bin")
    return hexotic_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    if misc.split_version(self.ordered_version)[:2] >= [1, 0]:
      install_dir = os.path.join(install_dir, "bin")
    return hexotic_configuration_template.substitute(install_dir=install_dir)

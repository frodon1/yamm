#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "IPYTHON"
ipython_template = """
#------- ipython ------
IPYTHON_HOME="%install_dir"
export PATH=${IPYTHON_HOME}/bin:${PATH}
export PYTHONPATH=${IPYTHON_HOME}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
ipython_template = misc.PercentTemplate(ipython_template)

ipython_configuration_template = """
#------- ipython ------
IPYTHON_HOME="$install_dir"
ADD_TO_PATH: %(IPYTHON_HOME)s/bin
ADD_TO_PYTHONPATH: %(IPYTHON_HOME)s/lib/python$python_version/site-packages
"""
ipython_configuration_template = string.Template(ipython_configuration_template)

class IPYTHON(SalomeSoftware):

  def get_debian_dev_package(self):
      return []

  def get_debian_package(self):
    if self._py3:
      return ['ipython3']
    else:
      return ['ipython']

  def init_variables(self):
    self.archive_file_name = "ipython-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python_egg"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS", "SPHINX"]
    self.software_dependency_dict['exec'] = ["PYTHON", "PYGMENTS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
    elif dependency_name == "SETUPTOOLS":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "SPHINX":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "PYGMENTS":
      dependency_object.depend_of = ["python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "SETUPTOOLS":
      self.executor_software_name += "-set" + misc.transform(version_name)
    elif dependency_name == "SPHINX":
      self.executor_software_name += "-sph" + misc.transform(version_name)
    elif dependency_name == "PYGMENTS":
      self.executor_software_name += "-pyg" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ipython_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ipython_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "LAPACK"
lapack_template = """
#------ Lapack ------
export LAPACKDIR=%install_dir
export %ld_library_path="$LAPACKDIR/lib":${%ld_library_path}
"""
lapack_template = misc.PercentTemplate(lapack_template)
# warning : export needed by Edyos

lapack_configuration_template = """
#------ Lapack ------
LAPACKDIR=$install_dir
ADD_TO_$ld_library_path: "%(LAPACKDIR)s/lib"
"""
lapack_configuration_template = string.Template(lapack_configuration_template)

class LAPACK(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    if misc.split_version(self.ordered_version) < [3, 3, 0]:
      misc.print_error("Lapack version supported is >= 3.3.0")

  def get_debian_dev_package(self):
    return 'liblapack-dev'

  def init_variables(self):
    self.archive_file_name = "lapack-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"
    self.config_options = "-DBUILD_SHARED_LIBS:BOOL=ON"
    self.post_install_commands = ["cd lib ; ln -s libblas.so libblas.a", "cd lib ; ln -s liblapack.so liblapack.a"]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = []

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CMAKE":
        self.executor_software_name += "-cm" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return lapack_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return lapack_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

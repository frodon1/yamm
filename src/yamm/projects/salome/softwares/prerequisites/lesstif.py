#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "LESSTIF"
lesstif_template = """
#------ lesstif ------
LESSTIFHOME="%install_dir"
export PATH=${LESSTIFHOME}/bin:${PATH}
export %ld_library_path=${LESSTIFHOME}/lib:${%ld_library_path}
"""
lesstif_template = misc.PercentTemplate(lesstif_template)

lesstif_configuration_template = """
#------ lesstif ------
LESSTIFHOME="$install_dir"
ADD_TO_PATH: %(LESSTIFHOME)s/bin
ADD_TO_$ld_library_path: %(LESSTIFHOME)s/lib
"""
lesstif_configuration_template = string.Template(lesstif_configuration_template)

class LESSTIF(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "lesstif-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options = ""
    self.gen_commands = ['sed "s:LT_HAVE_FREETYPE:FINDXFT_HAVE_FREETYPE:g" acinclude.m4 > acinclude.m4.sed ; mv acinclude.m4.sed acinclude.m4', 'sed "s:LT_HAVE_XRENDER:FINDXFT_HAVE_XRENDER:g" acinclude.m4 > acinclude.m4.sed ; mv acinclude.m4.sed acinclude.m4']
    self.gen_commands.append('sed "s:\`aclocal --print-ac-dir\`:$CURRENT_SOFTWARE_INSTALL_DIR/share/aclocal:" configure.in > configure.in.sed ; mv configure.in.sed configure.in')
    self.gen_commands.append('aclocal -I. -Iscripts/autoconf')
    self.gen_commands.append('libtoolize --force --copy --automake')
    self.gen_commands.append('autoconf')
    self.gen_commands.append('automake --copy --gnu --add-missing')

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return lesstif_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return lesstif_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

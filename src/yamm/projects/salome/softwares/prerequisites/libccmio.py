#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc


software_name = "LIBCCMIO"
libccmio_template = """
#------ CCM Library ------
export LIBCCMIO_INSTALL_DIR="%install_dir"
export LD_LIBRARY_PATH=${LIBCCMIO_INSTALL_DIR}/arch/Linux_x86_64/lib:${LD_LIBRARY_PATH}
"""
libccmio_template = misc.PercentTemplate(libccmio_template)

libccmio_configuration_template = """
#------ CCM Library ------
LIBCCMIO_INSTALL_DIR="$install_dir"
ADD_TO_LD_LIBRARY_PATH: %(LIBCCMIO_INSTALL_DIR)s/arch/Linux_x86_64/lib
"""
libccmio_configuration_template = string.Template(libccmio_configuration_template)

class LIBCCMIO(SalomeSoftware):
  
  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "software"
    
  def init_variables(self):
    self.archive_file_name = "libccmio-" + self.version + ".tgz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "specific"
    self.specific_install_command = "rsync -r /data/projets/projets.001/saturne.005/opt/libccmio-{0}/ $CURRENT_SOFTWARE_INSTALL_DIR".format(self.version)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    version = self.version
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libccmio_template.substitute(install_dir=install_dir,version=version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    version = self.version
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libccmio_configuration_template.substitute(install_dir=install_dir,version=version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "LIBSPATIALITE"

libspatialite_template = """
#------ libspatialite ------
export LIBSPATIALITEHOME="%install_dir"
export %ld_library_path=${LIBSPATIALITEHOME}/lib:${%ld_library_path}
"""
libspatialite_template = misc.PercentTemplate(libspatialite_template)

libspatialite_configuration_template = """
#------ libspatialite ------
LIBSPATIALITEHOME="$install_dir"
ADD_TO_$ld_library_path: %(LIBSPATIALITEHOME)s/lib
"""
libspatialite_configuration_template = string.Template(libspatialite_configuration_template)

class LIBSPATIALITE(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name    = "libspatialite-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type          = "specific"
    
    patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
    
    self.specific_configure_command  = "cd $CURRENT_SOFTWARE_SRC_DIR; "
    self.specific_configure_command  += " patch -p1 < " + os.path.join(patch_directory, "libspatialite.patch") + " ; "
    self.specific_configure_command  += " /usr/bin/aclocal -I m4 ; /usr/bin/libtoolize --force --copy --automake ; "
    self.specific_configure_command  += " /usr/bin/autoconf ; /usr/bin/automake --add-missing --copy --gnu ;"
    self.specific_configure_command  += " cd $CURRENT_SOFTWARE_BUILD_DIR; "
    self.specific_configure_command  += ' CPPFLAGS="-I${PROJHOME}/include -I${SQLITE_ROOT}/include" '
    self.specific_configure_command  += ' LDFLAGS="-L${PROJHOME}/lib -L${SQLITE_ROOT}/lib" '
    self.specific_configure_command  += ' $CURRENT_SOFTWARE_SRC_DIR/configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR '
    self.specific_configure_command  += " --with-geosconfig=${GEOSHOME}/bin/geos-config "
    self.specific_configure_command  += " --disable-freexl "
    self.specific_build_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make -j{0} ; ".format(self.parallel_make)
    self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make install;"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["QT", "PROJ", "GEOS", "SQLITE"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libspatialite_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libspatialite_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

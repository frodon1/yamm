#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "LIBTOOL"

libtool_template = """
#------ libtool ------
export LIBTOOLHOME="%install_dir"
export PATH=${LIBTOOLHOME}/bin:${PATH}
export %ld_library_path=${LIBTOOLHOME}/lib:${%ld_library_path}
#export LIBTOOL=${LIBTOOLHOME}/bin/libtool
#export LIBTOOLACLOCAL=${LIBTOOLHOME}/share/aclocal
"""
libtool_template = misc.PercentTemplate(libtool_template)

libtool_configuration_template = """
#------ libtool ------
LIBTOOLHOME="$install_dir"
ADD_TO_PATH: %(LIBTOOLHOME)s/bin
ADD_TO_$ld_library_path: %(LIBTOOLHOME)s/lib
#LIBTOOL=%(LIBTOOLHOME)s/bin/libtool
LIBTOOLACLOCAL=%(LIBTOOLHOME)s/share/aclocal
"""
libtool_configuration_template = string.Template(libtool_configuration_template)

class LIBTOOL(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "libtool-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "specific"
    self.specific_configure_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; $CURRENT_SOFTWARE_SRC_DIR/configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR"
    self.specific_build_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make ;"
    self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make install ;"
    self.specific_install_command += "cp -p $CURRENT_SOFTWARE_INSTALL_DIR/share/aclocal/* $AUTOMAKEHOME/share/aclocal"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["M4", "AUTOCONF", "AUTOMAKE"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libtool_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libtool_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

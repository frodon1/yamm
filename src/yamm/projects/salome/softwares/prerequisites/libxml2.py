#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "LIBXML2"
libxml2_template = """
#------- libxml2 -------
export LIBXML_DIR="%install_dir"
export PATH=${LIBXML_DIR}/bin:${PATH}
export %ld_library_path=${LIBXML_DIR}/lib:${%ld_library_path}
export PYTHONPATH=${LIBXML_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
# Temporary added for New Geom, to be removed after
# new geom environment variables have been made
# conform with SALOME variables
export LIBXML2_ROOT_DIR=$LIBXML_DIR
"""
# WARNING, the export of LIBXML_DIR is used by kernel at configure time, do not remove
libxml2_template = misc.PercentTemplate(libxml2_template)

libxml2_configuration_template = """
#------- libxml2 -------
LIBXML_DIR="$install_dir"
ADD_TO_PATH: %(LIBXML_DIR)s/bin
ADD_TO_$ld_library_path: %(LIBXML_DIR)s/lib
ADD_TO_PYTHONPATH: %(LIBXML_DIR)s/lib/python$python_version/site-packages
"""
# WARNING, the export of LIBXML_DIR is used by kernel at configure time, do not remove
libxml2_configuration_template = string.Template(libxml2_configuration_template)

class LIBXML2(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "libxml2-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.config_options += " --with-python=$PYTHON_INSTALL_DIR "
      self.user_dependency_command += "export LDFLAGS=-L$PYTHON_INSTALL_DIR/lib ; "
      self.executor_software_name += "-py" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libxml2_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libxml2_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

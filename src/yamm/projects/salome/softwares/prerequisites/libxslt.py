#  Copyright (C) 2013 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "LIBXSLT"

libxslt_template = """
#------- libxslt -------
export LIBXSLT_ROOT_DIR="%install_dir"
export PATH=${LIBXSLT_ROOT_DIR}/bin:${PATH}
export %ld_library_path=${LIBXSLT_ROOT_DIR}/lib:${%ld_library_path}
"""
# WARNING, the export of LIBXSLT_DIR is used by kernel at configure time, do not remove
libxslt_template = misc.PercentTemplate(libxslt_template)

libxslt_configuration_template = """
#------- libxslt -------
LIBXSLT_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(LIBXSLT_ROOT_DIR)s/bin
ADD_TO_$ld_library_path: %(LIBXSLT_ROOT_DIR)s/lib
"""
# WARNING, the export of LIBXSLT_DIR is used by kernel at configure time, do not remove
libxslt_configuration_template = string.Template(libxslt_configuration_template)

class LIBXSLT(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'libxslt1-dev'

  def init_variables(self):
    self.archive_file_name = "libxslt-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options += " --without-python --without-crypto --without-debugger"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["LIBXML2"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "LIBXML2":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "LIBXML2":
      self.config_options += " --with-libxml-prefix=$LIBXML2_INSTALL_DIR "
      self.executor_software_name += "-xml" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libxslt_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libxslt_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2012, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Boris Daix (EDF R&D)

import os
import string
from yamm.core.base import misc
from yamm.core.base.misc import transform
from yamm.projects.salome.software import SalomeSoftware

software_name = "LOGSERVICE"

logservice_template = """
#------ LOGSERVICE ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
"""
logservice_template = misc.PercentTemplate(logservice_template)

logservice_configuration_template = """
#------ LOGSERVICE ------
ADD_TO_$ld_library_path="$install_dir/lib"
"""
logservice_configuration_template = string.Template(logservice_configuration_template)

class LOGSERVICE(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "LogService-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["OMNIORB", "DOCUTILS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    if dependency_name == "OMNIORB":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "DOCUTILS":
      dependency_object.depend_of = ["python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + transform(version_name)
    elif dependency_name == "OMNIORB":
      self.executor_software_name += "-orb" + transform(version_name)
      self.config_options += "-DOMNIORB4_DIR:PATH=$OMNIORB_INSTALL_DIR "
    elif dependency_name == "DOCUTILS":
      self.executor_software_name += "-docls" + transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return logservice_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return logservice_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "M4"

m4_template = """
#------ m4 ------
export M4HOME="%install_dir"
export PATH=${M4HOME}/bin:$PATH
"""
m4_template = misc.PercentTemplate(m4_template)

m4_configuration_template = """
#------ m4 ------
M4HOME="$install_dir"
ADD_TO_PATH: %(M4HOME)s/bin
"""
m4_configuration_template = string.Template(m4_configuration_template)

class M4(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name    = "m4-" + self.version + ".tar.bz2"
    self.archive_type         = "tar.bz2"
    self.compil_type          = "autoconf"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return m4_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return m4_configuration_template.substitute(install_dir=install_dir)

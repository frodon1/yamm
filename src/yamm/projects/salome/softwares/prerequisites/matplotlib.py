#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "MATPLOTLIB"
MATPLOTLIB_template = """
#------- Matplotlib ------
export MATPLOTLIB_HOME="%install_dir"
export PYTHONPATH=${MATPLOTLIB_HOME}/lib/python%python_version/site-packages:${PYTHONPATH}
export MPLCONFIGDIR="${HOME}/.config/salome/matplotlib-%mpl_version" # Useful to avoid changing the config files used by the system matplotlib
"""
MATPLOTLIB_template = misc.PercentTemplate(MATPLOTLIB_template)

MATPLOTLIB_configuration_template = """
#------- Matplotlib ------
MATPLOTLIB_HOME="$install_dir"
ADD_TO_PYTHONPATH: %(MATPLOTLIB_HOME)s/lib/python$python_version/site-packages
MPLCONFIGDIR="$${HOME}/.config/salome/matplotlib-$mpl_version" # Useful to avoid changing the config files used by the system matplotlib
"""
MATPLOTLIB_configuration_template = string.Template(MATPLOTLIB_configuration_template)

class MATPLOTLIB(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "matplotlib")

  def get_debian_dev_package(self):
    if self._py3:
        return 'python3-matplotlib'
    return 'python-matplotlib'

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if self.ordered_version == "1.1.0":
      self.patches += [os.path.join(self.patch_directory, "matplotlib_fix_paraview_gtk.patch")]
    elif self.ordered_version == "1.3.1":
      self.patches += [os.path.join(self.patch_directory, "matplotlib131_fix_paraview_gtk.patch")]
    elif self.ordered_version == "1.4.3":
      self.patches += [os.path.join(self.patch_directory, "matplotlib143_fix_paraview_gtk.patch")]

  def init_variables(self):
    self.archive_file_name = "matplotlib-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"
    force_ftp_server = self.project_options.get_global_option("python_prereq_force_ftp_server")
    if force_ftp_server:
      prerequisites_server = self.project_options.get_global_option("python_prerequisites_ftp_server")
      if prerequisites_server:
        sed = "'s#^DEFAULT_URL.*#DEFAULT_URL = \"{0}/\"#'".format(prerequisites_server)
    else:
      prerequisites_server = self.project_options.get_global_option("python_prerequisites_server")
      if prerequisites_server:
        sed = "'s#http://pypi.python.org#{0}#'".format(prerequisites_server)
    if prerequisites_server:
        # The following lines changes the pypi server for additional python
        # packages downloaded by matplotlib to the server define by a global option.
        self.pre_build_commands += ["sed -i {0} distribute_setup.py"
            .format(sed)]

    # Instead of patching the configuration file of matplotlib, a environment variable is used.
    try:
      qtversion = misc.major(self.version_object.get_software_version('QT'))
      self.user_dependency_command += " export MPLSETUPCFG=" + os.path.join(self.patch_directory,
                                                                            "setup_qt%s.cfg" % qtversion)
    except misc.LoggerException:
      # QT not defined in the version
      pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS", 'QT']
    self.software_dependency_dict['exec'] = ["PYTHON", "PYQT", "NUMPY"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
    if dependency_name == "PYQT":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "SETUPTOOLS":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "NUMPY":
      dependency_object.depend_of = ["python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    if dependency_name == "PYQT":
      self.executor_software_name += "-pyqt" + misc.transform(version_name)
    if dependency_name == "SETUPTOOLS":
      self.executor_software_name += "-set" + misc.transform(version_name)
    if dependency_name == "NUMPY":
      self.executor_software_name += "-num" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return MATPLOTLIB_template.substitute(install_dir=install_dir, python_version=self.python_version,
                                          mpl_version=self.version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return MATPLOTLIB_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version,
                                          mpl_version=self.version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "MESHGEMS"
MESHGEMS_template = """
# ------ MeshGems -------------
MESHGEMSHOME="%install_dir"
MESHGEMS_ROOT_DIR="%install_dir"
"""
if misc.is64():
  MESHGEMS_template += """
export PATH=${MESHGEMSHOME}/bin/Linux_64:$PATH
export %ld_library_path=${MESHGEMSHOME}/lib/Linux_64:${%ld_library_path}
"""
else:
  MESHGEMS_template += """
export PATH=${MESHGEMSHOME}/bin/Linux:$PATH
export %ld_library_path=${MESHGEMSHOME}/lib/Linux:${%ld_library_path}
"""
MESHGEMS_template = misc.PercentTemplate(MESHGEMS_template)

MESHGEMS_configuration_template = """
# ------ MeshGems -------------
MESHGEMSHOME="$install_dir"
MESHGEMS_ROOT_DIR="$install_dir"
"""
if misc.is64():
  MESHGEMS_configuration_template += """
ADD_TO_PATH: %(MESHGEMSHOME)s/bin/Linux_64
ADD_TO_$ld_library_path: %(MESHGEMSHOME)s/lib/Linux_64
"""
else:
  MESHGEMS_configuration_template += """
ADD_TO_PATH: %(MESHGEMSHOME)s/bin/Linux
ADD_TO_$ld_library_path: %(MESHGEMSHOME)s/lib/Linux
"""
MESHGEMS_configuration_template = string.Template(MESHGEMS_configuration_template)

class MESHGEMS(SalomeSoftware):

  def new_packaging(self):
    return misc.split_version(self.ordered_version) >= [1, 1]

  def init_variables(self):
    self.archive_file_name = "MeshGems-" + self.version + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "rsync"
    if self.new_packaging():
      if misc.split_version(self.ordered_version) >= [2, 2]:
        self.archive_file_name = "MeshGems_V{0}_linux.tar.gz".format(self.version)
      else:
         self.archive_file_name = "MeshGems_V{0}-Linux.tgz".format(self.version)
      # the content of the subdirectory MeshGems-VXXX/Products must be installed
      self.make_target = "MeshGems-{0}/Products/".format(self.ordered_version)

    bindir = "$CURRENT_SOFTWARE_INSTALL_DIR/bin/Linux"
    if misc.is64():
      bindir += "_64"
    self.post_install_commands.append("chmod +x " + bindir + "/*.exe")
    pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["DISTENE"]
    pass

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return MESHGEMS_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return MESHGEMS_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

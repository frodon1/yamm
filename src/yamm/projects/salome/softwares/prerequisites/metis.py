#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "METIS"
metis_template_old = """
#------ Metis ------
export METISDIR="%install_dir"
export %ld_library_path=${METISDIR}/Lib:${%ld_library_path}
"""
metis_template_new = """
#------ Metis ------
export METISDIR="%install_dir"    # export needed for compilation of medsplitter/partitinner
export PATH=${METISDIR}/bin:$PATH
export %ld_library_path=${METISDIR}/lib:${%ld_library_path}
"""
metis_template_old = misc.PercentTemplate(metis_template_old)
metis_template_new = misc.PercentTemplate(metis_template_new)

metis_configuration_template_old = """
#------ Metis ------
METISDIR="$install_dir"
ADD_TO_$ld_library_path: %(METISDIR)s/Lib
"""
metis_configuration_template_new = """
#------ Metis ------
METISDIR="$install_dir"
ADD_TO_PATH: %(METISDIR)s/bin
ADD_TO_$ld_library_path: %(METISDIR)s/lib
"""
metis_configuration_template_old = string.Template(metis_configuration_template_old)
metis_configuration_template_new = string.Template(metis_configuration_template_new)

class METIS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
    return 'libmetis-dev'

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if misc.split_version(self.ordered_version)[:2] == [4, 0]:
      self.patches.append(os.path.join(self.patch_directory, "metis.patch"))

  def init_variables(self):
    self.archive_file_name = "metis-" + self.version + "-src.tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    self.parallel_make = "1"

    if misc.split_version(self.ordered_version)[:2] == [4, 0]:
      self.specific_build_command = "make "
      self.specific_install_command = "cp -rf * $CURRENT_SOFTWARE_INSTALL_DIR ; mkdir $CURRENT_SOFTWARE_INSTALL_DIR/lib ; cd $CURRENT_SOFTWARE_INSTALL_DIR/lib ; ln -s ../libmetis.a"
      if misc.misc_platform == "BSD":
        self.specific_install_command += "rm $CURRENT_SOFTWARE_INSTALL_DIR/INSTALL ; "
        pass
      pass
    else:
      self.specific_configure_command = "make config prefix=$CURRENT_SOFTWARE_INSTALL_DIR "
      self.specific_build_command = "make "
      self.specific_install_command = "make install "
      pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = []

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    if misc.split_version(self.ordered_version)[:2] == [4, 0]:
      return metis_template_old.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())
    else :
      return metis_template_new.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    if misc.split_version(self.ordered_version)[:2] == [4, 0]:
      return metis_configuration_template_old.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())
    else :
      return metis_configuration_template_new.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware
import os
import string

software_name = "MUPARSER"

MUPARSER_template = """
#---------- MuParser -------------------
export %ld_library_path=%install_dir/lib:${%ld_library_path}
"""
MUPARSER_template = misc.PercentTemplate(MUPARSER_template)

MUPARSER_configuration_template = """
#---------- MuParser -------------------
ADD_TO_$ld_library_path: $install_dir/lib
"""
MUPARSER_configuration_template = string.Template(MUPARSER_configuration_template)

class MUPARSER(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'libmuparser-dev'

  def init_variables(self):
    self.archive_file_name = "muparser-" + self.version + ".tar.gz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "autoconf"
    self.parallel_make     = "1" # Parallel compilation not supported with muparser 1.32

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return MUPARSER_template.substitute(install_dir = install_dir,
                                        ld_library_path = misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return MUPARSER_configuration_template.substitute(install_dir = install_dir,
                                        ld_library_path = misc.get_ld_library_path())

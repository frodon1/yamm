#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "NCURSES"

ncurses_template = """
#------ ncurses ------
export NCURSESHOME="%install_dir"
export PATH=${NCURSESHOME}/bin:${PATH}
export %ld_library_path=${NCURSESHOME}/lib:${%ld_library_path}
"""
ncurses_template = misc.PercentTemplate(ncurses_template)

ncurses_configuration_template = """
#------ ncurses ------
NCURSESHOME="$install_dir"
ADD_TO_PATH: %(NCURSESHOME)s/bin
ADD_TO_$ld_library_path: %(NCURSESHOME)s/lib
"""
ncurses_configuration_template = string.Template(ncurses_configuration_template)

class NCURSES(SalomeSoftware):

  def init_variables(self):
    self.software_source_type = "archive"
    self.archive_file_name    = "ncurses-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type          = "autoconf"
    #if self.use_new_commands() :
    self.config_options  += "--with-shared "

#  def init_dependency_list(self):
#    self.software_dependency_list = []

#  def get_dependency_object_for(self, dependency_name):
#    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
#    return dependency_object

#  def update_configuration_with_dependency(self, dependency_name, version_name):

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ncurses_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ncurses_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())


#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Ulysse HUMBERT-GONZALEZ (Open Cascade)

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware
import os
import string

software_name = "NLOPT"

NLOPT_template = """
#---------- NLopt -------------------
export NLOPT_ROOT_DIR="%install_dir"
export %ld_library_path=%install_dir/lib:${%ld_library_path}
export PYTHONPATH=${NLOPT_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
NLOPT_template = misc.PercentTemplate(NLOPT_template)

NLOPT_configuration_template = """
#---------- NLopt -------------------
NLOPT_ROOT_DIR="$install_dir"
ADD_TO_$ld_library_path: $install_dir/lib
ADD_TO_PYTHONPATH: %(NLOPT_ROOT_DIR)s/lib/python$python_version/site-packages
"""
NLOPT_configuration_template = string.Template(NLOPT_configuration_template)

class NLOPT(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'libnlopt-dev'

  def init_variables(self):
    self.archive_file_name = "nlopt-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options += "--enable-shared"

  def get_type(self):
    return "prerequisite"

  def init_dependency_list(self):
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path", "python_version"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return NLOPT_template.substitute(install_dir = install_dir, ld_library_path = misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return NLOPT_configuration_template.substitute(install_dir = install_dir, ld_library_path = misc.get_ld_library_path(), python_version=self.python_version)

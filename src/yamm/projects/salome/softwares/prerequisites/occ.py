#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "OCC"
occ_template = """
#------ cas ------
export CASROOT="%install_dir"
export %ld_library_path=${CASROOT}/lib:${%ld_library_path}
# Variable for Foundation Classes :
export CSF_UnitsLexicon=${CASROOT}/src/UnitsAPI/Lexi_Expr.dat
export CSF_UnitsDefinition=${CASROOT}/src/UnitsAPI/Units.dat
# Variable for DataExchange :
export CSF_SHMessageStd=${CASROOT}/src/SHMessageStd
export CSF_SHMessage=${CASROOT}/src/SHMessage
export CSF_XSMessage=${CASROOT}/src/XSMessage
# Variable for Font :
export CSF_MDTVFontDirectory=${CASROOT}/src/FontMFT
export CSF_MDTVTexturesDirectory=${CASROOT}/src/Textures
# Needed for HYDRO module :
export CSF_StandardDefaults=${CASROOT}/src/StdResource
export CSF_PluginDefaults=${CASROOT}/src/StdResource
export CSF_StandardLiteDefaults=${CASROOT}/src/StdResource
export CSF_XmlOcafResource=${CASROOT}/src/XmlOcafResource
# Temporary added for New Geom, to be removed after
# new geom environment variables have been made
# conform with SALOME variables
export PATH=${CASROOT}:${PATH}
"""
occ_template = misc.PercentTemplate(occ_template)

occ_configuration_template = """
#------ cas ------
CASROOT="$install_dir"
ADD_TO_$ld_library_path: %(CASROOT)s/lib
# Variable for Foundation Classes :
CSF_UnitsLexicon=%(CASROOT)s/src/UnitsAPI/Lexi_Expr.dat
CSF_UnitsDefinition=%(CASROOT)s/src/UnitsAPI/Units.dat
# Variable for DataExchange :
CSF_SHMessageStd=%(CASROOT)s/src/SHMessageStd
CSF_SHMessage=%(CASROOT)s/src/SHMessage
CSF_XSMessage=%(CASROOT)s/src/XSMessage
# Variable for Font :
CSF_MDTVFontDirectory=%(CASROOT)s/src/FontMFT
CSF_MDTVTexturesDirectory=%(CASROOT)s/src/Textures
# Needed for HYDRO module :
CSF_StandardDefaults=%(CASROOT)s/src/StdResource
CSF_PluginDefaults=%(CASROOT)s/src/StdResource
CSF_StandardLiteDefaults=%(CASROOT)s/src/StdResource
CSF_XmlOcafResource=%(CASROOT)s/src/XmlOcafResource
"""
occ_configuration_template = string.Template(occ_configuration_template)

class OCC(SalomeSoftware):

  def get_debian_build_depends(self):
    return ['libgl1-mesa-dev', 'libglu1-mesa-dev', 'libxmu-dev']

  def init_variables(self):
    self.archive_file_name = "CAS-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    if misc.major(self.ordered_version) >= 7 :
      self.compil_type = "cmake"
      self.config_options += " -DINSTALL_DIR_RESOURCE=src "
    else :
      self.compil_type = "autoconf"

    if self.use_new_commands() :
      if misc.major(self.ordered_version) < 7 :  # for autoconf
        self.pre_configure_commands = ["./build_configure"]
        self.config_options += "--disable-draw --without-tcl --without-tk "
      # if self.version.startswith("dev"):          # Le temps d'exécution des tests devient prohibitif avec OCCT compilé en debug, compilé en enable-production par défaut
      #   self.config_options  += "--enable-debug "
      if misc.is64():
        self.user_dependency_command = "export CFLAGS=\"-fPIC -m64\" ; "
    else:
      # self.gen_commands = ["libtoolize --force --copy --automake", "aclocal", "autoheader", "autoconf", "automake --add-missing --copy"]
      self.config_options += "--disable-debug --enable-production --enable-wrappers=no --enable-wok=no --enable-draw=no --without-tcl --without-tk "

    if misc.split_version(self.ordered_version)[:2] == [6, 6]:
      self.post_install_commands = ["cp -r $CURRENT_SOFTWARE_SRC_DIR/src/UnitsAPI $CURRENT_SOFTWARE_INSTALL_DIR/src"]

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    patches = []
    if self.ordered_version[:5] == "6.9.1" and "dev-NewGEOM" not in self.version :
      patches = ["OCCT-6.9.1-0026870-Visualization-deactivated-selections-are-not.patch",  # p1: For OCC 6.9.1 implemented in frames of NEWGEOM project (DECISION 78.1)
                 "OCCT-6.9.1-0002-0027065-BRepOffsetAPI_MakePipe-misses-definition-of-.patch"
      ]
    if (misc.compareVersions(self.ordered_version, "7.0.0") == 0 and misc.get_calibre_version() == "7"):
      patches = ["OCCT-7.0.0_dlopen.patch"]

    if (misc.compareVersions(self.ordered_version, "7.1.0") == 0):
      patches = ["OCCT-7.1.0_transfrom_pers.patch"]

    if (misc.compareVersions(self.ordered_version, "7.2.0") == 0):
      patches = ["OCCT-7.2.0.patch"]

    # Apply patches
    patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
    for p in patches:
      self.patches.append(os.path.join(patch_directory, p))

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    if self.use_new_commands():
      self.software_dependency_dict['exec'] = ["TBB", "GL2PS", "FREETYPE"]
    if misc.split_version(self.ordered_version) >= [6, 7]:
      self.software_dependency_dict['exec'] += ["FREEIMAGE"]
    if misc.split_version(self.ordered_version) < [6, 6]:
      self.software_dependency_dict['exec'] += ["FTGL"]
    if misc.major(self.ordered_version) >= 7 :
      self.software_dependency_dict['build'] += ["CMAKE"]
      self.software_dependency_dict['exec'] += ["TCL", "TK"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if self.use_new_commands():
      if dependency_name == "TBB":
        dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
      if dependency_name == "FTGL":
        dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
      if dependency_name == "GL2PS":
        dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
      if dependency_name == "FREETYPE":
        dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
      if dependency_name == "FREEIMAGE":
        dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
      if dependency_name == "TCL":
        dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
      if dependency_name == "TK":
        dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if self.use_new_commands():
      if misc.major(self.ordered_version) >= 7 :
        if dependency_name == "TBB":
          self.config_options += "-DUSE_TBB=ON -D3RDPARTY_TBB_DIR=${TBB_INSTALL_DIR} -D3RDPARTY_TBB_LIBRARY_DIR=${TBB_INSTALL_DIR}/lib -D3RDPARTY_TBB_INCLUDE_DIR=${TBB_INSTALL_DIR}/include "
          self.executor_software_name += "-tbb" + misc.transform(version_name)
        if dependency_name == "GL2PS":
          self.config_options += "-DUSE_GL2PS=ON -D3RDPARTY_GL2PS_DIR=${GL2PS_INSTALL_DIR} "
          self.executor_software_name += "-gl2ps" + misc.transform(version_name)
        if dependency_name == "FREETYPE":
          self.config_options += "-D3RDPARTY_FREETYPE_DIR=${FREETYPE_INSTALL_DIR} "
          self.executor_software_name += "-freetp" + misc.transform(version_name)
        if dependency_name == "FREEIMAGE":
          self.config_options += "-DUSE_FREEIMAGE=ON -D3RDPARTY_FREEIMAGE_DIR=${FREEIMAGE_INSTALL_DIR} -D3RDPARTY_FREEIMAGE_LIBRARY_DIR:PATH=${FREEIMAGE_INSTALL_DIR}/lib -D3RDPARTY_FREEIMAGE_LIBRARY:FILEPATH=${FREEIMAGE_INSTALL_DIR}/lib/libfreeimageplus-${FreeImage_version}.so "
          self.executor_software_name += "-freeimg" + misc.transform(version_name)
        if dependency_name == "TCL":
          self.config_options += "-D3RDPARTY_TCL_DIR=${TCLDIR} -D3RDPARTY_TCL_LIBRARY_DIR=${TCLDIR}/lib -D3RDPARTY_TCL_LIBRARY:PATH=${TCL_LIBRARY} "
        if dependency_name == "TK":
          self.config_options += "-D3RDPARTY_TK_DIR=${TKDIR} -D3RDPARTY_TK_LIBRARY_DIR=${TKDIR}/lib -D3RDPARTY_TK_LIBRARY:PATH=${TK_LIBRARY} "

        # For Netgen
        patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        self.post_install_commands.append("cp " + patch_directory + "/config.h $CURRENT_SOFTWARE_INSTALL_DIR/include/opencascade")

      else :
        if dependency_name == "TBB":
          self.config_options += "--with-tbb-include=${TBB_INSTALL_DIR}/include --with-tbb-library=${TBB_INSTALL_DIR}/lib "
          self.executor_software_name += "-tbb" + misc.transform(version_name)
        if dependency_name == "FTGL":
          self.config_options += "--with-ftgl=${FTGL_INSTALL_DIR} "
          self.executor_software_name += "-ftgl" + misc.transform(version_name)
        if dependency_name == "GL2PS":
          self.config_options += "--with-gl2ps=${GL2PS_INSTALL_DIR} "
          self.executor_software_name += "-gl2ps" + misc.transform(version_name)
        if dependency_name == "FREETYPE":
          self.config_options += "--with-freetype=${FREETYPE_INSTALL_DIR} "
          self.executor_software_name += "-freetp" + misc.transform(version_name)
        if dependency_name == "FREEIMAGE":
          self.config_options += "--with-freeimage=${FREEIMAGE_INSTALL_DIR} "
          self.executor_software_name += "-freeimg" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return occ_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return occ_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def use_new_commands(self):
    use_new_commands = self.version.startswith("dev")
    if not use_new_commands:
      use_new_commands = misc.split_version(self.ordered_version)[:2] >= [6, 5]
      use_new_commands = use_new_commands or (misc.major(self.ordered_version) >= 7)
    return use_new_commands

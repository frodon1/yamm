#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "OMNIORB"
omniorb_template = """
#------ omniORB ------
export OMNIORB_DIR="%install_dir"
export PATH=${OMNIORB_DIR}/bin:$PATH
export PYTHONPATH=${OMNIORB_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
export %ld_library_path=$OMNIORB_DIR/lib:$%ld_library_path
"""
omniorb_template = misc.PercentTemplate(omniorb_template)

omniorb_configuration_template = """
#------ omniORB ------
OMNIORB_DIR="$install_dir"
ADD_TO_PATH: %(OMNIORB_DIR)s/bin
ADD_TO_PYTHONPATH: %(OMNIORB_DIR)s/lib/python$python_version/site-packages
ADD_TO_$ld_library_path: %(OMNIORB_DIR)s/lib
"""
omniorb_configuration_template = string.Template(omniorb_configuration_template)

class OMNIORB(SalomeSoftware):

  def get_debian_dev_package(self):
    return ['libomniorb4-dev', 'omniidl', 'libcos4-dev']

  def get_debian_package(self):
    # Dépendance au paquet omniorb-nameserver pour la commande 'omniNames'
    return ['omniorb-nameserver']

  def init_variables(self):
    self.archive_file_name = "omniORB-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options = "--disable-ipv6"

    # Replace Python install path in omniidl file by /usr/bin/env python
    # because build can fail if the full path is too long
    sedSep = '%'
    python_shebang = '#!/usr/bin/env python'
    file_with_long_path = 'src/tool/omniidl/python/scripts/omniidl'
    self.post_configure_commands.append("sed -i '1 s{0}^.*${0}{1}{0}g' {2}"
                                        .format(sedSep,
                                                python_shebang,
                                                file_with_long_path))

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return omniorb_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(),
                                       python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return omniorb_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(),
                                       python_version=self.python_version)

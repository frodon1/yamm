#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.engine.dependency import Dependency
import os
import string

software_name = "OMNIORBPY"
omniorbpy_template = """
#------ omniORBpy ------
export OMNIORBPY_DIR="%install_dir"
export PYTHONPATH=${OMNIORBPY_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
export PYTHONPATH=${OMNIORBPY_DIR}/lib/python%python_version/site-packages/omniidl_be:${PYTHONPATH}
"""
omniorbpy_template = misc.PercentTemplate(omniorbpy_template)

omniorbpy_configuration_template = """
#------ omniORBpy ------
OMNIORBPY_DIR="$install_dir"
ADD_TO_PYTHONPATH: %(OMNIORBPY_DIR)s/lib/python$python_version/site-packages
ADD_TO_PYTHONPATH: %(OMNIORBPY_DIR)s/lib/python$python_version/site-packages/omniidl_be
"""
omniorbpy_configuration_template = string.Template(omniorbpy_configuration_template)

class OMNIORBPY(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
    if self._py3:
        return ['python3-omniorb', 'omniidl-python3']
    return ['python-omniorb', 'omniidl-python']

  def init_variables(self):
    self.archive_file_name = "omniORBpy-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    if 'OMNIORB' not in self.project_softwares_dict:
      self.config_options += '--with-omniorb=/usr'

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if int(misc.major(self.ordered_version)) >= 4 and self._py3:
      self.patches.append(os.path.join(self.patch_directory, "omniORBpy-4.2.1-2-python3.patch"))

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "OMNIORB"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
        dependency_object = Dependency(name="PYTHON",
                                       depend_of=["path", "ld_lib_path"],
                                      )
    elif dependency_name == "OMNIORB":
        dependency_object = Dependency(name="OMNIORB",
                                       depend_of=["install_path"],
                                       specific_install_var_name="OMNIORB_INSTALL_DIR"
                                      )
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
        self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "OMNIORB":
        self.executor_software_name += "-orb" + misc.transform(version_name)
        self.config_options += "--with-omniorb=$OMNIORB_INSTALL_DIR"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return omniorbpy_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return omniorbpy_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "OPENCV"
opencv_template = """
#------ Opencv ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
"""
opencv_template = misc.PercentTemplate(opencv_template)

opencv_configuration_template = """
#------ Opencv ------
ADD_TO_$ld_library_path: "$install_dir/lib"
"""
opencv_configuration_template = string.Template(opencv_configuration_template)

class OPENCV(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'libopencv-dev'

  def init_variables(self):
    self.archive_file_name = "OpenCV-" + self.version + ".tar.bz2"
    self.archive_type = "tar.bz2"
    self.compil_type = "cmake"

    self.config_options += "-DCMAKE_BUILD_TYPE:STRING=Release "
    self.config_options += "-DWITH_FFMPEG=OFF "
    self.config_options += "-DWITH_GSTREAMER=OFF "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["PYTHON", "TBB"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    if dependency_name == "TBB":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      python_bin = "-DPYTHON_EXECUTABLE=${PYTHON_INSTALL_DIR}/bin/python${PYTHON_VERSION} "
      python_include = "-DPYTHON_INCLUDE_PATH=${PYTHON_INSTALL_DIR}/include/python${PYTHON_VERSION} "
      python_lib = "-DPYTHON_LIBRARY=${PYTHON_INSTALL_DIR}/lib/libpython${PYTHON_VERSION}.so "
      self.config_options += python_bin + python_include + python_lib
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "TBB":
      tbb_include = "-DTBB_INCLUDE_DIR=${TBB_INSTALL_DIR}/include/tbb "
      tbb_lib = "-DTBB_LIB_DIR=${TBB_INSTALL_DIR}/lib "
      self.config_options += tbb_include + tbb_lib
      self.executor_software_name += "-tbb" + misc.transform(version_name)


  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return opencv_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return opencv_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "OPENMPI"

openmpi_template = """
# ------- OPENMPI ---------
export OPENMPI_HOME="%install_dir"
export OPAL_PREFIX=${OPENMPI_HOME}
export PATH=${OPENMPI_HOME}/bin:${PATH}
export %ld_library_path=${OPENMPI_HOME}/lib:${%ld_library_path}
"""
openmpi_template = misc.PercentTemplate(openmpi_template)

openmpi_configuration_template = """
# ------- OPENMPI ---------
OPENMPI_HOME="$install_dir"
OPAL_PREFIX=%(OPENMPI_HOME)s
ADD_TO_PATH: %(OPENMPI_HOME)s/bin
ADD_TO_$ld_library_path: %(OPENMPI_HOME)s/lib
"""
openmpi_configuration_template = string.Template(openmpi_configuration_template)

class OPENMPI(SalomeSoftware):


  def init_variables(self):
    self.archive_file_name = "openmpi-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "autoconf"

    pkgconfigdir = "lib/pkgconfig/"
    
    self.replaceSelfInstallPath("bin/vtsetup")
    self.replaceSelfInstallPath("bin/vtrun")
    self.replaceSelfInstallPath("bin/orte_wrapper_script")

    self.replaceSelfInstallPath(pkgconfigdir + "ompi.pc")
    self.replaceSelfInstallPath(pkgconfigdir + "ompi-cxx.pc")
    self.replaceSelfInstallPath(pkgconfigdir + "ompi-c.pc")
    self.replaceSelfInstallPath(pkgconfigdir + "orte.pc")
    self.replaceSelfInstallPath(pkgconfigdir + "ompi-f77.pc")
    self.replaceSelfInstallPath(pkgconfigdir + "ompi-f90.pc")
  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["BINUTILS","GCC"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "GCC":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    sharedir = "share/openmpi/"
    linkerdir = "share/vampirtrace/"
    movableFiles = [ sharedir + "mpicxx-wrapper-data.txt",
                   sharedir + "mpiCC-wrapper-data.txt",
                   sharedir + "mpicc-wrapper-data.txt",
                   sharedir + "mpic++-wrapper-data.txt",
                   sharedir + "mpif77-wrapper-data.txt",
                   sharedir + "mpif90-wrapper-data.txt",
                   sharedir + "mpicxx-vt-wrapper-data.txt",
                   sharedir + "mpiCC-vt-wrapper-data.txt",
                   sharedir + "mpic++-vt-wrapper-data.txt",
                   sharedir + "mpif77-vt-wrapper-data.txt",
                   sharedir + "mpif90-vt-wrapper-data.txt",
                   sharedir + "ortecc-wrapper-data.txt",
                   sharedir + "ortec++-wrapper-data.txt",
                   ]
    if dependency_name == "GCC":
      self.executor_software_name += "-gcc" + misc.transform(version_name)
      for aFile in movableFiles:
        self.replaceSoftwareInstallPath("GCC", aFile)

      self.replaceSoftwareInstallPath("GCC", linkerdir + "libtool")
      self.replaceSoftwareInstallPath("GCC", linkerdir + "vtfort-wrapper-data.txt")
      self.replaceSoftwareInstallPath("GCC", linkerdir + "vtc++-wrapper-data.txt")
      self.replaceSoftwareInstallPath("GCC", linkerdir + "vtcc-wrapper-data.txt")
      self.replaceSoftwareInstallPath("GCC", "bin/orte_wrapper_script")

    if dependency_name == "BINUTILS":
      self.executor_software_name += "-bin" + misc.transform(version_name)

      self.replaceSoftwareInstallPath("BINUTILS", linkerdir + "libtool")
      self.replaceSoftwareInstallPath("BINUTILS", linkerdir + "vtsetup-data.xml")
      self.replaceSoftwareInstallPath("BINUTILS", "etc/vtsetup-config.xml")
      self.replaceSoftwareInstallPath("BINUTILS", "bin/vtrun")

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return openmpi_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return openmpi_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

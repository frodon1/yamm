#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "OSMESA"

osmesa_template = """
# ------- OSMESA ---------
export CATALYST_LD_ADD_PATH=%install_dir/lib:${CATALYST_LD_ADD_PATH}
"""
osmesa_template = misc.PercentTemplate(osmesa_template)

osmesa_configuration_template = """
# ------- OSMESA ---------
CATALYST_LD_ADD_PATH="$install_dir/lib"
"""
osmesa_configuration_template = string.Template(osmesa_configuration_template)

class OSMESA(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "mesa-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "autoconf"
    self.config_options += "--enable-opengl --disable-gles1 --disable-gles2 "
    self.config_options += "--disable-va --disable-xvmc --disable-vdpau "
    self.config_options += "--enable-shared-glapi "
    self.config_options += "--disable-texture-float "
    #self.config_options += "--enable-texture-float "
    self.config_options += "--enable-gallium-llvm --enable-llvm-shared-libs "
    self.config_options += "--with-gallium-drivers=swrast "
    self.config_options += "--disable-dri --with-dri-drivers= "
    self.config_options += "--disable-egl --with-egl-platforms= --disable-gbm "
    self.config_options += "--disable-glx "
    self.config_options += "--disable-osmesa --enable-gallium-osmesa "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["OTGUI", "DOCUMENTATION", "MED"] # Fake dependency to ensure OSMESA, PARAVIEW_OSMESA 
                                                        # and CFDSTUDY are built AFTER 
                                                        # PARAVIS and its gui machinery (Qt, Pyqt, GUI,...).

  def get_type(self):
    return "prerequisite"
  
  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return osmesa_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return osmesa_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

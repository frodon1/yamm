#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015, 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PARAVIEW_OSMESA"

paraview_osmesa_template = """
#------ paraview_osmesa ------
export CATALYST_ROOT_DIR="%install_dir"
"""

paraview_osmesa_configuration_template = """
#------ paraview_osmesa ------
CATALYST_ROOT_DIR="$install_dir"
"""

# NOTE: We use ";" instead of ":" in PV_PLUGIN_PATH because there is a bug in ParaView which doesn't load plugins otherwise; fixed in paraview 4.2

class PARAVIEW_OSMESA(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.paraview_version = "%s.%s" % (misc.major(self.ordered_version),
                                       misc.minor(self.ordered_version))

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    patches = []
    # Apply patches
    patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "paraview")

    for p in patches:
      self.patches.append(os.path.join(patch_directory, p))


  def init_variables(self):

    version_for_archive = self.get_version_for_archive()
    self.software_rendering = False
    if version_for_archive.endswith("-software_rendering"):
      version_for_archive = version_for_archive[:-19]
      self.software_rendering = True
      pass
    if not version_for_archive.startswith('v'):
      version_for_archive = 'v' + version_for_archive
    self.archive_file_name = "ParaView-" + version_for_archive + "-source.tar.gz"
    self.archive_type = "tar.gz"

    # Sets the root_repository address and the proxy
    self.remote_type = "git"
    self.tag = self.version
    internal_submodules = {'ThirdParty/IceT/vtkicet':
                           'https://git.forge.pleiade.edf.fr/git/prerequisites-salome.icet.git',
                           'ThirdParty/QtTesting/vtkqttesting':
                           'https://git.forge.pleiade.edf.fr/git/prerequisites-salome.QtTesting.git',
                           'ThirdParty/protobuf/vtkprotobuf':
                           'https://git.forge.pleiade.edf.fr/git/prerequisites-salome.protobuf.git',
                           'Utilities/VisItBridge':
                           'https://git.forge.pleiade.edf.fr/git/prerequisites-salome.visitbridge.git',
                           'VTK':
                           'https://git.forge.pleiade.edf.fr/git/prerequisites-salome.vtk.git',
                           }
    external_submodules = {'ThirdParty/IceT/vtkicet': 'https://gitlab.kitware.com/icet/icet.git',
                           'ThirdParty/QtTesting/vtkqttesting': 'https://github.com/Kitware/QtTesting.git',
                           'ThirdParty/protobuf/vtkprotobuf': 'https://gitlab.kitware.com/paraview/protobuf.git',
                           'Utilities/VisItBridge': 'https://gitlab.kitware.com/paraview/visitbridge.git',
                           'VTK': 'https://gitlab.kitware.com/vtk/vtk.git',
                           }
    self.configure_remote_software_pleiade_git("prerequisites-salome.paraview.git",
                                               internal_submodules=internal_submodules,
                                               external_server="https://gitlab.kitware.com",
                                               external_path="paraview",
                                               external_repo_name="paraview.git",
                                               external_submodules=external_submodules)

    self.compil_type = "cmake"
    self.can_delete_src = False  # Needed for GUI

    # Options identical with OCC team
    # Common settings
    self.config_options += "-DBUILD_SHARED_LIBS:BOOL=ON "
    if self.software_rendering:
      self.config_options += "-DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo "
    else:
      self.config_options += "-DCMAKE_BUILD_TYPE:STRING=Release "

    # Paraview general settings
    self.config_options += "-DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=ON "

    # VTK general settings
    self.config_options += "-DVTK_USE_64BIT_IDS:BOOL=OFF "
    if self.software_rendering:
      self.config_options += "-DVTK_USE_X:BOOL=OFF "
      self.config_options += "-DVTK_OPENGL_HAS_OSMESA:BOOL=ON "
      self.config_options += "-DVTK_USE_OFFSCREEN=ON "
      self.config_options += "-DVTK_USE_X=OFF "

    # Qt settings
    if self.software_rendering:
      self.config_options += "-DPARAVIEW_BUILD_QT_GUI:BOOL=OFF "
      self.config_options += "-DPARAVIEW_USE_MPI:BOOL=ON "
    else:
      self.config_options += "-DPARAVIEW_BUILD_QT_GUI:BOOL=ON "

    # Python settings
    self.config_options += "-DPARAVIEW_ENABLE_PYTHON:BOOL=ON "

    # Hdf5 settings
    # Do not use the paraview hdf5 version
    self.config_options += "-DVTK_USE_SYSTEM_HDF5:BOOL=ON "

    # MPI4RUN settings
    # Do not use the paraview mpi4run version
    if 'MPI4PY' in self.project_softwares_dict:
      self.config_options += "-DVTK_USE_SYSTEM_MPI4PY:BOOL=ON "

    # Pygments settings to avoid Sphinx issues
    self.config_options += "-DVTK_USE_SYSTEM_PYGMENTS:BOOL=ON "

    # Extra options
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_Moments:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_SierraPlotTools:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_SLACTools:BOOL=OFF "

    self.config_options += "-DPARAVIEW_INSTALL_DEVELOPMENT_FILES=ON -DVTK_NO_PYTHON_THREADS=OFF "
    self.config_options += "-DPARAVIEW_ENABLE_CATALYST:BOOL=ON "  # Enable co-processing
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_AnalyzeNIfTIIO:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_ArrowGlyph:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_CatalystScriptGeneratorPlugin:BOOL=ON "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_GMVReader:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_MobileRemoteControl:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_NonOrthogonalSource:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_PacMan:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_RGBZView:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_SciberQuestToolKit:BOOL=OFF "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_StreamingParticles:BOOL=ON "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_UncertaintyRendering:BOOL=ON "
    self.config_options += "-DPARAVIEW_ENABLE_MATPLOTLIB:BOOL=ON "


    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_EyeDomeLighting:BOOL=ON "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_H5PartReader:BOOL=ON "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_PointSprite:BOOL=ON "
    self.config_options += "-DPARAVIEW_BUILD_PLUGIN_SurfaceLIC:BOOL=ON "

    # End of options identical with OCC team
    self.config_options += "-DVTK_USE_SYSTEM_LIBXML2:BOOL=ON "
    self.config_options += "-DVTK_USE_OGGTHEORA_ENCODER:BOOL=ON "
    self.config_options += "-DBUILD_DOCUMENTATION:BOOL=ON "
    self.config_options += "-DBUILD_EXAMPLES:BOOL=OFF "
    self.config_options += "-DBUILD_TESTING:BOOL=OFF "


    self.config_options += "-DVTK_NO_PYTHON_THREADS=OFF "
    self.config_options += "-DVTK_PYTHON_FULL_THREADSAFE=ON "
    self.config_options += "-DVTK_BUILD_QT_DESIGNER_PLUGIN:BOOL=OFF "
    try:
      qtversion = misc.major(self.version_object.get_software_version('QT'))
      self.config_options += '-DPARAVIEW_QT_VERSION=%s ' % qtversion
    except misc.LoggerException:
      # Qt not defined in version
      pass

    if misc.compareVersions(self.ordered_version, "5.4") >= 0:
        self.config_options += "-DVTK_SMP_IMPLEMENTATION_TYPE=OpenMP "
        self.config_options += "-DPARAVIEW_AUTOLOAD_PLUGIN_StreamingParticles=ON "
        self.config_options += "-DPARAVIEW_AUTOLOAD_PLUGIN_UncertaintyRendering=ON "
        self.config_options += "-DPARAVIEW_AUTOLOAD_PLUGIN_EyeDomeLighting=ON "
        self.config_options += "-DPARAVIEW_AUTOLOAD_PLUGIN_H5PartReader=ON "
        self.config_options += "-DPARAVIEW_AUTOLOAD_PLUGIN_PointSprite=ON "
        self.config_options += "-DPARAVIEW_AUTOLOAD_PLUGIN_SurfaceLIC=ON "
        self.config_options += "-DPARAVIEW_AUTOLOAD_PLUGIN_CatalystScriptGeneratorPlugin=ON "

    self.user_dependency_command = "unset PYTHONHOME ; export PYTHONPATH=$CURRENT_SOFTWARE_INSTALL_DIR/lib/python$PYTHON_VERSION/site-packages:$PYTHONPATH ; "
    self.pre_install_commands = ["mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR"]

    if misc.compareVersions(self.ordered_version, "5.3") >= 0:
      # Paraview master Feb 10th 2017 unable to find corect GL2PS version
      self.config_options += "-DVTK_USE_SYSTEM_GL2PS:BOOL=OFF "
      # Bug on make install
      self.pre_install_commands.append('make -j%s DoxygenDoc' % self.parallel_make)
    # Manual copying of some missing files for version 3.98.1 and above
    self.post_install_commands.append("cp $CURRENT_SOFTWARE_SRC_DIR/VTK/CMake/TopologicalSort.cmake $CURRENT_SOFTWARE_INSTALL_DIR/lib/cmake/paraview-" + self.paraview_version)
    self.post_install_commands.append("cp $CURRENT_SOFTWARE_SRC_DIR/ParaViewCore/ServerImplementation/Core/vtkSIVectorPropertyTemplate.h $CURRENT_SOFTWARE_INSTALL_DIR/include/paraview-" + self.paraview_version)
    self.post_install_commands.append("cp $CURRENT_SOFTWARE_BUILD_DIR/VTK/VTKTargets.cmake $CURRENT_SOFTWARE_INSTALL_DIR/lib/cmake/paraview-" + self.paraview_version)
    self.post_install_commands.append('sed -i "s#$CURRENT_SOFTWARE_BUILD_DIR/lib#$CURRENT_SOFTWARE_INSTALL_DIR/lib/paraview-{0}#" $CURRENT_SOFTWARE_INSTALL_DIR/lib/cmake/paraview-{0}/VTKTargets.cmake '.format(self.paraview_version))
    self.post_install_commands.append('sed -i \'s#auto_load="0"#auto_load="1"#\' $CURRENT_SOFTWARE_INSTALL_DIR/lib/paraview-{0}/.plugins'.format(self.paraview_version))

    self.replaceSoftwareInstallPath("PARAVIEW", "lib/cmake/paraview-" + self.paraview_version + "/VTKConfig.cmake")
    self.replaceSoftwareInstallPath("PARAVIEW", "lib/cmake/paraview-" + self.paraview_version + "/VTKTargets.cmake")


  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "DOXYGEN", "GRAPHVIZ", "SPHINX"]
    self.software_dependency_dict['exec'] = ["PYTHON", "HDF5", "LIBXML2", "QT",
                                             "MPI4PY", "MATPLOTLIB", "GL2PS", "OSMESA"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path", "python_version"]
    elif dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    elif dependency_name == "HDF5":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "GRAPHVIZ":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "DOXYGEN":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "LIBXML2":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "QT":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    elif dependency_name == "GL2PS":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    elif dependency_name == "SPHINX":
      dependency_object.depend_of = ["path", "python_path"]
    elif dependency_name == "MATPLOTLIB":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "OSMESA":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      python_bin = "-DPYTHON_EXECUTABLE=${PYTHON_INSTALL_DIR}/bin/python${PYTHON_VERSION} "
      python_include = "-DPYTHON_INCLUDE_DIR=${PYTHON_INSTALL_DIR}/include/python${PYTHON_VERSION} "
      python_lib = "-DPYTHON_LIBRARY=${PYTHON_INSTALL_DIR}/lib/libpython${PYTHON_VERSION}.so "
      self.config_options += python_bin + python_include + python_lib
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "HDF5":
      # With version 3.98.1 HDF5 is badly detected and we have to specify more variables
      self.config_options += "-DHDF5_LIBRARIES=${HDF5_INSTALL_DIR}/lib "
      self.config_options += "-DHDF5_hdf5_hl_LIBRARY=${HDF5_INSTALL_DIR}/lib/libhdf5_hl.so "
      self.config_options += "-DHDF5_HL_LIBRARY=${HDF5_INSTALL_DIR}/lib/libhdf5_hl.so "
      self.executor_software_name += "-hd" + misc.transform(version_name)
    elif dependency_name == "GRAPHVIZ":
      self.config_options += "-DDOXYGEN_DOT_EXECUTABLE=${GRAPHVIZ_INSTALL_DIR}/bin/dot "
      self.executor_software_name += "-gr" + misc.transform(version_name)
    elif dependency_name == "DOXYGEN":
      self.config_options += "-DDOXYGEN_EXECUTABLE=${DOXYGEN_INSTALL_DIR}/bin/doxygen "
      self.executor_software_name += "-dox" + misc.transform(version_name)
    elif dependency_name == "LIBXML2":
      self.config_options += "-DLIBXML2_INCLUDE_DIR=${LIBXML2_INSTALL_DIR}/include/libxml2 "
      self.config_options += "-DLIBXML2_LIBRARIES=${LIBXML2_INSTALL_DIR}/lib/libxml2.so "
      self.executor_software_name += "-xml2" + misc.transform(version_name)
    elif dependency_name == "QT":
      self.config_options += '-DQT_HELP_GENERATOR=${QTDIR}/bin/qhelpgenerator '
      self.executor_software_name += "-qt" + misc.transform(version_name)
    elif dependency_name == "GL2PS":
      vtk_use_system_gl2ps = "-DVTK_USE_SYSTEM_GL2PS:BOOL=ON "
      gl2ps_include = "-DGL2PS_INCLUDE_DIR:STRING=${GL2PS_INSTALL_DIR}/include "
      gl2ps_lib = "-DGL2PS_LIBRARY:STRING=${GL2PS_INSTALL_DIR}/lib/libgl2ps.so "
      self.config_options += vtk_use_system_gl2ps + gl2ps_include + gl2ps_lib
      self.executor_software_name += "-gl2ps" + misc.transform(version_name)
    elif dependency_name == "OSMESA":
        self.config_options += "-DOPENGL_glu_LIBRARY=${OSMESA_INSTALL_DIR}/lib/libGLU.so " 
        self.config_options += "-DOSMESA_INCLUDE_DIR=${OSMESA_INSTALL_DIR}/include " 
        self.config_options += "-DOSMESA_LIBRARY=${OSMESA_INSTALL_DIR}/lib/libOSMesa.so " 
        self.user_dependency_command += " export LD_LIBRARY_PATH=${OSMESA_INSTALL_DIR}/lib:$LD_LIBRARY_PATH ; "

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    template = misc.PercentTemplate(paraview_osmesa_template)
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return template.substitute(install_dir=install_dir,)

  def get_configuration_str(self, specific_install_dir=""):
    template = string.Template(paraview_osmesa_configuration_template)

    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return template.substitute(install_dir=install_dir,)

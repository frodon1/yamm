#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PILLOW"
pillow_template = """
#------ Python Imaging Library ------
export PILLOWDIR="%install_dir"
export PATH=${PILLOWDIR}/bin:$PATH
export PYTHONPATH=${PILLOWDIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
# Remarque : export PILLOWDIR nécessaire (utilisé dans la procédure d'installation de PILLOW)

pillow_template = misc.PercentTemplate(pillow_template)

pillow_configuration_template = """
#------ Python Imaging Library ------
PILLOWDIR="$install_dir"
ADD_TO_PATH: %(PILLOWDIR)s/bin
ADD_TO_PYTHONPATH: %(PILLOWDIR)s/lib/python$python_version/site-packages
"""
# Remarque : export PILLOWDIR nécessaire (utilisé dans la procédure d'installation de PILLOW)

pillow_configuration_template = string.Template(pillow_configuration_template)

class PILLOW(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
      # From pillow
      if self._py3:
          return 'python3-pil'
      return 'python-pil'

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if "FREETYPE" in self.project_softwares_dict:
      self.patches.append(os.path.join(self.patch_directory, "pillow-py3.patch"))

  def init_variables(self):
    self.archive_file_name = "Pillow-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"

    #self.specific_build_command = "python setup.py build && cd Sane && python setup.py build"
    #self.specific_install_command = "python setup.py install --root $CURRENT_SOFTWARE_INSTALL_DIR"
    #self.specific_install_command += " && cd Sane && python setup.py install --root $CURRENT_SOFTWARE_INSTALL_DIR"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "FREETYPE"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
    if dependency_name == "FREETYPE":
      dependency_object.depend_of = ["ld_lib_path", "install_path"]
      dependency_object.specific_install_var_name = "FREETYPEDIR"
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "FREETYPE":
      self.executor_software_name += "-ft" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pillow_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pillow_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

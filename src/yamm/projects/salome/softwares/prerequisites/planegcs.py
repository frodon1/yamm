#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PLANEGCS"
planecgs_template = """
#------ planegcs ------
export PLANEGCS_ROOT_DIR="%install_dir"
export %ld_library_path=${PLANEGCS_ROOT_DIR}/lib:${%ld_library_path}
"""
planegcs_template = misc.PercentTemplate(planecgs_template)

planecgs_configuration_template = """
#------ planegcs ------
PLANEGCS_ROOT_DIR="$install_dir"
ADD_TO_$ld_library_path: %(PLANEGCS_ROOT_DIR)s/lib
"""
planegcs_configuration_template = string.Template(planecgs_configuration_template)


class PLANEGCS(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "planegcs-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

  def get_type(self):
    return "prerequisite"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["EIGEN", "BOOST"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "EIGEN":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BOOST":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "EIGEN":
      self.executor_software_name += "-ei" + misc.transform(version_name)
    elif dependency_name == "BOOST":
      self.executor_software_name += "-bst" + misc.transform(version_name)

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return planegcs_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return planegcs_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

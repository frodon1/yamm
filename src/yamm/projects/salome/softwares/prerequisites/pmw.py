#  Copyright (C) 2013, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

import os
import string
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "PMW"
PMW_template = """
#------- PYTHON MEGA-WIDGET ------
export PYTHONPATH=%install_dir/lib/python%python_version/site-packages:${PYTHONPATH}
"""
PMW_template = misc.PercentTemplate(PMW_template)

PMW_configuration_template = """
#------- PYTHON MEGA-WIDGET ------
ADD_TO_PYTHONPATH: $install_dir/lib/python$python_version/site-packages
"""
PMW_configuration_template = string.Template(PMW_configuration_template)

class PMW(SalomeSoftware):

  def init_variables(self):
    if self.version[0] < "2":
      self.archive_file_name = "Pmw_" + self.version.replace(".", "_") + ".tar.gz"
    else:
      self.archive_file_name = "Pmw-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    if dependency_name == "SETUPTOOLS":
      self.executor_software_name += "-set" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return PMW_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return PMW_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

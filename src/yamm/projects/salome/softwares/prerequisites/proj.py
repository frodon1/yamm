#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PROJ"

proj_template = """
#------ proj ------
export PROJHOME="%install_dir"
export PATH=${PROJHOME}/bin:${PATH}
export %ld_library_path=${PROJHOME}/lib:${%ld_library_path}
"""
proj_template = misc.PercentTemplate(proj_template)

proj_configuration_template = """
#------ proj ------
PROJHOME="$install_dir"
ADD_TO_PATH: %(PROJHOME)s/bin
ADD_TO_$ld_library_path: %(PROJHOME)s/lib
"""
proj_configuration_template = string.Template(proj_configuration_template)

class PROJ(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "proj-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "autoconf"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GCC"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return proj_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return proj_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

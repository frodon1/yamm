#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PSYCOPG2"

psycopg2_template = """
#------ psycopg2 ------
export PSYCOPG2HOME="%install_dir"
export PYTHONPATH="${PSYCOPG2HOME}/lib/python%python_version/site-packages":${PYTHONPATH}
"""
psycopg2_template = misc.PercentTemplate(psycopg2_template)

psycopg2_configuration_template = """
#------ psycopg2 ------
PSYCOPG2HOME="$install_dir"
ADD_TO_PYTHONPATH: %(PSYCOPG2HOME)s/lib/python$python_version/site-packages
"""
psycopg2_configuration_template = string.Template(psycopg2_configuration_template)

class PSYCOPG2(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "psycopg2-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "python"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return psycopg2_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return psycopg2_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

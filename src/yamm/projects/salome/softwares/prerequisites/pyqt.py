#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PYQT"
pyqt_template = """
#-------PyQt ----------
export PYQT_ROOT_DIR="%install_dir"
export PYQT_SIPS=${PYQT_ROOT_DIR}/share/sip
export PYUIC=${PYQT_ROOT_DIR}/bin/pyuic%major
export PYTHONPATH=${PYQT_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
export PATH=${PYQT_ROOT_DIR}/bin:${PATH}
"""
pyqt_template = misc.PercentTemplate(pyqt_template)
# WARNING, the export of PYQT_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

pyqt_configuration_template = """
#-------PyQt ----------
PYQT_ROOT_DIR="$install_dir"
PYQT_SIPS=%(PYQT_ROOT_DIR)s/share/sip
PYUIC=%(PYQT_ROOT_DIR)s/bin/pyuic$major
ADD_TO_PYTHONPATH: %(PYQT_ROOT_DIR)s/lib/python$python_version/site-packages
ADD_TO_PATH: %(PYQT_ROOT_DIR)s/bin
"""
pyqt_configuration_template = string.Template(pyqt_configuration_template)
# WARNING, the export of PYQT_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

class PYQT(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")
    self.major = misc.major(self.version)
    self.minor = misc.minor(self.version)
    self.sip_configure_option = ""

  def get_debian_dev_package(self):
    if self.version.startswith('5'):
      if self._py3:
          return ['python3-pyqt5', 'python3-pyqt5.qtopengl', 'pyqt5-dev', 'pyqt5-dev-tools']
      return ['python-pyqt5', 'python-pyqt5.qtopengl', 'pyqt5-dev', 'pyqt5-dev-tools']

    if self._py3:
        return ['python3-pyqt4', 'python3-pyqt4.qtopengl']
    return ['python-qt4-dev', 'pyqt4-dev-tools', 'python-qt4-gl']

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if self.ordered_version == "4.9.1":
      self.patches.append(os.path.join(self.patch_directory, "PyQt-4.9.1.patch"))

  def init_variables(self):
    self.archive_file_name = "PyQt-x11-gpl-" + self.version + ".tar.gz"
    if self.major == 5:
      self.archive_file_name = "PyQt-gpl-" + self.version + ".tar.gz"
      if self.minor >= 6:
        self.archive_file_name = "PyQt5_gpl-%s.%s.tar.gz" % (self.major, self.minor)
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    parallel_make = self.project_options.get_option(self.name, "parallel_make")

    pyqt_bin_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/bin"
    pyqt_lib_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/lib/python$PYTHON_VERSION/site-packages"
    pyqt_plugin_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/include/python$PYTHON_VERSION"
    pyqt_sip_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/share/sip"

    self.specific_configure_command = "sed -i \"s:videowidget:videowidget_fake:\" configure.py ; "
    self.specific_configure_command += "python configure.py --dbus /tmp --confirm-license -b " + pyqt_bin_dir + " -d " + pyqt_lib_dir + " -v " + pyqt_sip_dir
    if self.major == 4:
      self.specific_configure_command += " -p " + pyqt_plugin_dir
    if self.major == 5:
      self.specific_configure_command += " --designer-plugindir=" + pyqt_plugin_dir + " --qml-plugindir=" + pyqt_plugin_dir
    if misc.compareVersions(self.ordered_version, "5.5.1") == 0:
      self.specific_configure_command += " --no-qsci-api "
    self.specific_configure_command += self.sip_configure_option
    self.specific_build_command = "make -j%s" % parallel_make
    self.specific_install_command = "mkdir -p " + pyqt_bin_dir + " ; "
    self.specific_install_command += "mkdir -p " + pyqt_plugin_dir + " ; "
    self.specific_install_command += "mkdir -p " + pyqt_lib_dir + " ; "
    self.specific_install_command += "mkdir -p " + pyqt_sip_dir + " ; "
    self.specific_install_command += "make install"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "QT", "SIP"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", 'python_path', "python_version"]
    elif dependency_name == "QT":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    elif dependency_name == "SIP":
      dependency_object.depend_of = ["path", "python_path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "QT":
      self.executor_software_name += "-qt" + misc.transform(version_name)
      if misc.compareVersions(self.version, "5.5.1") < 0:
          afile = os.path.join("lib", "python" + self.python_version, "site-packages", "PyQt%d" % self.major, "pyqtconfig.py")
    elif dependency_name == "SIP":
      self.executor_software_name += "-sip" + misc.transform(version_name)
      if self.major == 5:
        self.sip_configure_option = " --sip-incdir=$SIP_INSTALL_DIR/include/python" + self.python_version

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pyqt_template.substitute(install_dir=install_dir,
                                    python_version=self.python_version,
                                    major=self.major)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pyqt_configuration_template.substitute(install_dir=install_dir,
                                                  python_version=self.python_version,
                                                  major=self.major)

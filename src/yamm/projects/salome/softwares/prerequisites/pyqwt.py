#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PYQWT"
pyqwt_template = """
#------ Pyqwt ------
# in Python install
"""
pyqwt_template = misc.PercentTemplate(pyqwt_template)

pyqwt_configuration_template = """
#------ Pyqwt ------
# in Python install
"""
pyqwt_configuration_template = string.Template(pyqwt_configuration_template)

class PYQWT(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "PyQwt-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    self.parallel_make = "1"

    self.specific_configure_command = "cd configure ; python configure.py "
    self.specific_configure_command += "-Q ../qwt-{0}.{1} ".format(misc.major(self.version),
                                                                   misc.minor(self.version))
    self.specific_build_command = "cd configure ; make -j1"
    self.specific_install_command = "cd configure ; make -j1 install "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "QT", "SIP", "PYQT",
                                             "QWT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    elif dependency_name == "QT":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    elif dependency_name == "SIP":
      dependency_object.depend_of = ["path", "python_path"]
    elif dependency_name == "PYQT":
      dependency_object.depend_of = ["path", "python_path"]
    elif dependency_name == "QWT":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "QT":
      self.executor_software_name += "-qt" + misc.transform(version_name)
    elif dependency_name == "SIP":
      self.executor_software_name += "-sip" + misc.transform(version_name)
    elif dependency_name == "PYQT":
      self.executor_software_name += "-pyqt" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pyqwt_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pyqwt_configuration_template.substitute(install_dir=install_dir)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PYTHON"
python_template = """
#------ python ------
export PYTHONHOME="%install_dir"
export PYTHONSTARTUP=${PYTHONHOME}/pythonrc.py
export PYTHON_INCLUDE=${PYTHONHOME}/include/python%python_version
export PATH=${PYTHONHOME}/bin:${PATH}
export %ld_library_path=${PYTHONHOME}/lib:${%ld_library_path}
export PYTHONPATH=${PYTHONHOME}/lib/python%python_version/site-packages:${PYTHONPATH}
# Temporary added for New Geom, to be removed after
# new geom environment variables have been made
# conform with SALOME variables
export PYTHON_ROOT_DIR=$PYTHONHOME
"""
python_template = misc.PercentTemplate(python_template)

python_configuration_template = """
#------ python ------
PYTHONHOME="$install_dir"
PYTHONSTARTUP=%(PYTHONHOME)s/pythonrc.py
PYTHON_INCLUDE=%(PYTHONHOME)s/include/python$python_version
ADD_TO_PATH: %(PYTHONHOME)s/bin
ADD_TO_$ld_library_path: %(PYTHONHOME)s/lib
ADD_TO_PYTHONPATH: %(PYTHONHOME)s/lib/python$python_version/site-packages
"""
python_configuration_template = string.Template(python_configuration_template)

pythonrc = """# Add auto-completion and a stored history file of commands to your Python
# interactive interpreter. Requires Python 2.0+, readline. Autocomplete is
# bound to the TAB key by default (you can change it - see readline docs).
#
# Store the history in ~/pyhistory,
#
try:
  import readline
  import atexit
  import os
  import rlcompleter
  readline.parse_and_bind('tab: complete')

  historyPath = os.path.expanduser('~/pyhistory')

  MAXLINES = 1000
  readline.set_history_length(MAXLINES)

  def save_history(historyPath=historyPath):
    import readline
    readline.write_history_file(historyPath)

  if os.path.exists(historyPath):
    readline.read_history_file(historyPath)

  atexit.register(save_history)
  del os, atexit, readline, rlcompleter, save_history, historyPath
except:
  pass"""

class PYTHON(SalomeSoftware):

  def get_debian_dev_package(self):
    if self.version.startswith('3'):
      return ['libpython3-dev']
    else:
      return ['libpython-dev']

  def get_debian_package(self):
    if self.version.startswith('3'):
      return ['python3']
    else:
      return ['python']

  def init_variables(self):
    self.archive_file_name = "Python-" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.compil_type = "autoconf"
    # without-pymalloc avoid to have the 'm' suffix to the lib name and the include directory
    self.config_options = "--enable-shared --without-pymalloc "
    self.post_install_commands = ["echo \"%s\" > pythonrc.py ;" % pythonrc]
    if self._py3:
      self.post_install_commands.append("cd lib/python{0} && "
                                        "ln -sf config-{0} config".format(self.python_version))
      self.post_install_commands.append("cd bin && ln -sf python3 python")
    else:
      self.config_options += "--enable-unicode=ucs4 "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["TCL", "TK"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "TCL":
      dependency_object.depend_of = ["ld_lib_path"]
    if dependency_name == "TK":
      dependency_object.depend_of = ["ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "TCL":
      self.user_dependency_command += "export LDFLAGS=\"${LDFLAGS} -L${TCLDIR}/lib\" ; "
      self.user_dependency_command += "export CPPFLAGS=\"${CPPFLAGS} -I${TCLDIR}/include\" ; "
      self.executor_software_name += "-tcl" + misc.transform(version_name)
      self.post_configure_commands += ["echo \" -L${{TCLDIR}}/lib -ltcl{0}.{1} \" >> Modules/Setup.local".format(misc.major(version_name), misc.minor(version_name))]
    elif dependency_name == "TK":
      self.user_dependency_command += "export LDFLAGS=\"${LDFLAGS} -L${TKDIR}/lib\" ; "
      self.user_dependency_command += "export CPPFLAGS=\"${CPPFLAGS} -I${TKDIR}/include\" ; "
      self.executor_software_name += "-tk" + misc.transform(version_name)
      self.post_configure_commands += ["echo \"_tkinter _tkinter.c tkappinit.c -DWITH_APPINIT \ \" >> Modules/Setup.local"]
      self.post_configure_commands += ["echo \" -L${{TKDIR}}/lib -ltk{0}.{1} \" >> Modules/Setup.local".format(misc.major(version_name), misc.minor(version_name))]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return python_template.substitute(install_dir=install_dir, python_version=self.python_version, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return python_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version, ld_library_path=misc.get_ld_library_path())

  def is_python(self):
    return True

  def get_python_version(self, py3=False):
    return self.ordered_version[:3]

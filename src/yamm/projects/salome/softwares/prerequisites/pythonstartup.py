#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os

from yamm.projects.salome.software import SalomeSoftware

software_name = "PYTHONSTARTUP"

pythonstartup_template = """
# ------- PYTHONSTARTUP ---------
export PYTHONSTARTUP="{install_dir}/pythonrc.py"
{path_str}"""

pythonstartup_configuration_template = """
# ------- PYTHONSTARTUP ---------
PYTHONSTARTUP="{install_dir}/pythonrc.py"
{path_cfg_str}"""

pythonrc = """# Add auto-completion and a stored history file of commands to your Python
# interactive interpreter. Requires Python 2.0+, readline. Autocomplete is
# bound to the TAB key by default (you can change it - see readline docs).
#
# Store the history in ~/pyhistory,
# History is limited to MAXLINES
#
try:
  import readline
  import atexit
  import os
  import rlcompleter
  readline.parse_and_bind('tab: complete')

  historyPath = os.path.expanduser('~/pyhistory')

  MAXLINES = 1000
  readline.set_history_length(MAXLINES)

  def save_history(historyPath=historyPath):
    import readline
    readline.write_history_file(historyPath)

  if os.path.exists(historyPath):
    readline.read_history_file(historyPath)

  atexit.register(save_history)
  del os, atexit, readline, rlcompleter, save_history, historyPath
except:
  pass"""

class PYTHONSTARTUP(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.tips = "It's a YAMM artifact to set the startup file of Python (which enables auto completion)"
    self.software_source_type = "software"
    self.path_str = ''
    self.path_cfg_str = ''
    self.bin_cmd_done = False

  def first(self):
    return True

  def create_bin_cmd(self):
    self.bin_cmd_done = True
    return '[ ! -d "bin" ] && mkdir bin'

  def init_variables(self):
    self.compil_type = "fake"
    if 'PYTHON' not in self.project_softwares_dict:
        self.compil_type = "specific"
        if not self.bin_cmd_done:
          self.post_install_commands.append(self.create_bin_cmd())
        self.specific_install_command = "echo \"%s\" > $CURRENT_SOFTWARE_INSTALL_DIR/pythonrc.py ;" % pythonrc
        if self._py3:
          self.post_install_commands.append('cd bin && ln -s $(which python3) python')
          self.path_str += 'export PATH=%s/bin:$PATH\n' % self.install_directory
          self.path_cfg_str += 'ADD_TO_PATH: %s/bin\n' % self.install_directory

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pythonstartup_template.format(**{'install_dir': install_dir,
                                            'path_str': self.path_str})

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return pythonstartup_configuration_template.format(**{'install_dir':install_dir,
                                                          'path_cfg_str': self.path_cfg_str})

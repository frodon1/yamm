#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "QCA"
qca_template = """
#------ qca ------
export QCAHOME="%install_dir"
export PATH=${QCAHOME}/bin:$PATH
export %ld_library_path=${QCAHOME}/lib:${%ld_library_path}
"""
qca_template = misc.PercentTemplate(qca_template)

qca_configuration_template = """
#------ qca ------
QCAHOME="$install_dir"
ADD_TO_PATH: %(QCAHOME)s/bin
ADD_TO_$ld_library_path: %(QCAHOME)s/lib
"""
qca_configuration_template = string.Template(qca_configuration_template)

class QCA(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name    = "qca-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type     = "cmake"
    self.config_options += " -DQT4_BUILD:BOOL=ON "
    self.config_options += " -DUSE_RELATIVE_PATHS:BOOL=ON "

#   self.config_options += " -DCORK_INCLUDE_DIR:PATH=${CORKHOME}/include "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["QT"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qca_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qca_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "QGIS"
qgis_template = """
#------ qgis ------
export QGISHOME="%install_dir"
export PATH=${QGISHOME}/bin:$PATH
export %ld_library_path=${QGISHOME}/lib:${%ld_library_path}
"""
qgis_template = misc.PercentTemplate(qgis_template)

qgis_configuration_template = """
#------ qgis ------
QGISHOME="$install_dir"
ADD_TO_PATH: %(QGISHOME)s/bin
ADD_TO_$ld_library_path: %(QGISHOME)s/lib
"""
qgis_configuration_template = string.Template(qgis_configuration_template)

class QGIS(SalomeSoftware):

  # def __init__(self, name, version, verbose, **kwargs):
    # SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    # self.patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

  # def init_patches(self):
    # SalomeSoftware.init_patches(self)
    # self.patches.append(os.path.join(self.patch_directory, "qgis-fix-sqlite-usage.patch"))

  def init_variables(self):
    self.archive_file_name = "qgis-" + self.version + ".tar.bz2"
    self.archive_type = "tar.bz2"

    self.compil_type = "cmake"

#    self.config_options += " -DCMAKE_CXX_COMPILER=`which gcc` " #${CXX} "
    self.config_options += " -DENABLE_TESTS=OFF "
    self.config_options += " -DENABLE_QT5=OFF "
    self.config_options += " -DPYTHON_LIBRARY=${PYTHONHOME}/lib/libpython%s.so " % self.python_version
#    self.config_options += " -DSQLITE3_INCLUDE_DIR=${SQLITE_ROOT}/include "
    self.config_options += " -DQSCINTILLA_INCLUDE_DIR=${QSCINTILLA_DIR}/include "
    self.config_options += " -DQSCINTILLA_LIBRARY=${QSCINTILLA_DIR}/lib/libqscintilla2.so "
    self.config_options += " -DGRASS_PREFIX=${GRASSPREFIX} "
    self.config_options += " -DPROJ_INCLUDE_DIR=${PROJHOME}/include "
    self.config_options += " -DPROJ_LIBRARY=${PROJHOME}/lib/libproj.so "
    self.config_options += " -DQWT_INCLUDE_DIR=${QWTHOME}/include "
    self.config_options += " -DQWT_LIBRARY=${QWTHOME}/lib/libqwt.so "
    self.config_options += " -DSPATIALINDEX_INCLUDE_DIR=${SPATIALINDEXHOME}/include/spatialindex "
    self.config_options += ' -DCMAKE_CXX_FLAGS="-I${SQLITE_ROOT}/include -I${SPATIALINDEXHOME}/include "'
    self.config_options += " -DSPATIALINDEX_LIBRARY=${SPATIALINDEXHOME}/lib/libspatialindex.so "
    self.config_options += " -DSPATIALITE_INCLUDE_DIR=${LIBSPATIALITEHOME}/include "
    self.config_options += " -DSPATIALITE_LIBRARY=${LIBSPATIALITEHOME}/lib/libspatialite.so  "
    self.config_options += " -DWITH_PY_COMPILE:BOOL=ON "
    self.config_options += " -DWITH_QSPATIALITE:BOOL=ON "
    self.config_options += " -DWITH_INTERNAL_QWTPOLAR:BOOL=OFF "
    self.config_options += " -DQWTPOLAR_INCLUDE_DIR=${QWTPOLARHOME}/include "
    self.config_options += " -DQWTPOLAR_LIBRARY=${QWTPOLARHOME}/lib/libqwtpolar.so "
    self.config_options += " -DCMAKE_PREFIX_PATH=${SQLITE_ROOT} "
    self.config_options += " -DQCA_LIBRARY=${QCAHOME}/lib/libqca.so "
    self.config_options += " -DQCA_INCLUDE_DIR=${QCAHOME}/include/QtCrypto "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["PYQT", "QSCINTILLA", "GRASS",
                                             "PROJ", "SPATIALINDEX",
                                             "LIBSPATIALITE", "PSYCOPG2",
                                             "QWTPOLAR", "GPSBABEL", "SQLITE",
                                             "SAGA", "SHAPELY", "FIONA", "QCA",
                                             "NUMPY", "MATPLOTLIB", "REQUESTS",
                                             "SCIPY", "NETWORKX", "PYOPENGL", "PIL"]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qgis_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qgis_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

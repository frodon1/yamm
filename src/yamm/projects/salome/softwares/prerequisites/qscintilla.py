#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "QSCINTILLA"
qscintilla_template = """
#------ qscintilla ------
QSCINTILLA_DIR="%install_dir"
export QSCINTILLA_INCLUDES=${QSCINTILLA_DIR}/include
export %ld_library_path=${QSCINTILLA_DIR}/lib:${%ld_library_path}
"""
qscintilla_template = misc.PercentTemplate(qscintilla_template)

qscintilla_configuration_template = """
#------ qscintilla ------
QSCINTILLA_DIR="$install_dir"
QSCINTILLA_INCLUDES=%(QSCINTILLA_DIR)s/include
ADD_TO_$ld_library_path: %(QSCINTILLA_DIR)s/lib
"""
qscintilla_configuration_template = string.Template(qscintilla_configuration_template)

class QSCINTILLA(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")
    self.qt5 = False
    # QtCore.PYQT_CONFIGURATION n'existe que pour pyqt >= 4.10
    # Pour les versions précédentes le script configure.py redirige
    # vers configure_old.py qui ne connait pas certaines options
    self.pyqt_has_pyqtconfiguration = False

  def get_debian_dev_package(self):
    if self.qt5:
      return ['libqt5qscintilla2-dev', 'pyqt5.qsci-dev']
    else:
      return ['libqscintilla2-dev', 'pyqt4.qsci-dev']

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if self.ordered_version == "2.6.1":
      self.patches.append(os.path.join(self.patch_directory, "QScintilla-2.6.1.patch"))

  def init_variables(self):
    if self.ordered_version >= "2.6.2":
      qt_dir = "Qt4Qt5"
    else:
      qt_dir = "Qt4"

    pyqt_version = "PyQt4"
    if self.qt5:
      pyqt_version = "PyQt5"

    postinst_cmd_options = ''
    if self.ordered_version >= "2.8.2":
      postinst_cmd_options += ' --pyqt={0}'.format(pyqt_version)
      postinst_cmd_options += ' --stubsdir=$PYQT_INSTALL_DIR/lib/python{0}/site-packages/{1}'.format(self.python_version, pyqt_version)

    if self.pyqt_has_pyqtconfiguration:
      postinst_cmd_options += ' --pyqt-sipdir=$PYQT_INSTALL_DIR/share/sip'
      postinst_cmd_options += ' --sip-incdir=$SIP_INSTALL_DIR/include/python{0}'.format(self.python_version)

    self.archive_file_name = "QScintilla-gpl-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "qmake"
    self.pro_file = os.path.join(qt_dir, 'qscintilla.pro')

    specific_dirs = {}
    specific_dirs['QT_INSTALL_LIBS'] = '$CURRENT_SOFTWARE_INSTALL_DIR/lib'
    specific_dirs['QT_INSTALL_HEADERS'] = '$CURRENT_SOFTWARE_INSTALL_DIR/include'
    specific_dirs['QT_INSTALL_TRANSLATIONS'] = '$CURRENT_SOFTWARE_INSTALL_DIR/translations'
    specific_dirs['QT_INSTALL_DATA'] = '$CURRENT_SOFTWARE_INSTALL_DIR'

    for d, p in list(specific_dirs.items()):
        self.specific_configure_command += 'export %s=%s; ' % (d, p)
    for d in specific_dirs:
        self.specific_configure_command += "sed -i 's/\[%s\]/(%s)/' %s ; " % (d, d, self.pro_file)

    self.specific_configure_command += "sed -i 's/LexBibTex/LexBibTeX/' " + self.pro_file + " ; "
    self.specific_configure_command += "sed -i 's/LexLaTex/LexLaTeX/' " + self.pro_file + " ; "

    self.post_install_commands = ['cp -r $CURRENT_SOFTWARE_SRC_DIR/{0}/* $CURRENT_SOFTWARE_BUILD_DIR'.format(qt_dir),
                                  'mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib',
                                  "cd $CURRENT_SOFTWARE_SRC_DIR/Python; "
                                  "python{0} configure.py --concatenate "
                                  "-o {1} -n {2} "
                                  "--destdir $PYQT_INSTALL_DIR/lib/python{0}/site-packages/{3} "
                                  "--apidir $CURRENT_SOFTWARE_INSTALL_DIR "
                                  "{4}"
                                  " && make && make install".format(self.python_version,
                                                                   specific_dirs['QT_INSTALL_LIBS'],
                                                                   specific_dirs['QT_INSTALL_HEADERS'],
                                                                   pyqt_version,
                                                                   postinst_cmd_options)]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["PYTHON", "PYQT", "SIP"]
    self.software_dependency_dict['exec'] = ["QT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", 'python_path']
    if dependency_name == "QT":
      dependency_object.depend_of = ["install_path", "path", "ld_lib_path"]
    if dependency_name == "PYQT":
      dependency_object.depend_of = ["install_path", "path", "ld_lib_path"]
    if dependency_name == "SIP":
      dependency_object.depend_of = ["install_path", "path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "QT":
      self.executor_software_name += "-qt" + misc.transform(version_name)
      self.qt5 = version_name.startswith('5')
    if dependency_name == "PYQT":
      self.pyqt_has_pyqtconfiguration = misc.compareVersions(version_name, '4.10') >= 0

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qscintilla_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qscintilla_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

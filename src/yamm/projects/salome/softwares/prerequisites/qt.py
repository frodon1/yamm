#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "QT"
qt_template = """
# ------ Qt -------------
export QTDIR="%install_dir"
export PATH=${QTDIR}/bin:$PATH
export QT_PLUGIN_PATH=${QTDIR}/plugins
export %ld_library_path=${QTDIR}/lib:${%ld_library_path}
export QTVERSION=%qt_version
export LRELEASE=${QTDIR}/bin/lrelease
export QT_QPA_PLATFORM_PLUGIN_PATH=${QTDIR}/plugins/platforms
"""
qt_template = misc.PercentTemplate(qt_template)
qtconf_template = """[Paths]
Prefix = %install_dir
Documentation = doc
Headers = include
Libraries = lib
Binaries = bin
Plugins = plugins
Data = .
Translations = translations
Settings = .
Examples = .
Demos = demos"""
qtconf_template = misc.PercentTemplate(qtconf_template)

qt_configuration_template = """
# ------ Qt -------------
QTDIR="$install_dir"
ADD_TO_PATH: %(QTDIR)s/bin
ADD_TO_QT_PLUGIN_PATH=%(QTDIR)s/plugins
ADD_TO_$ld_library_path: %(QTDIR)s/lib
QTVERSION=$qt_version
LRELEASE=%(QTDIR)s/bin/lrelease
QT_QPA_PLATFORM_PLUGIN_PATH=%(QTDIR)s/plugins/platforms
"""
qt_configuration_template = string.Template(qt_configuration_template)

class QT(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
    if misc.major(self.version) == 5:
      return ['qtbase5-dev', 'libqt5x11extras5-dev', 'libqt5opengl5-dev', 'qtwebengine5-dev']
    else:
      return ['libqt4-dev', 'libqt4-opengl-dev', 'libqtwebkit-dev', 'qt4-dev-tools']

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    patches = []
    if self.ordered_version[:3] == "4.8":
      patches = ["qt-gcc5.x.patch"]  # Bug on QT 4.8 and recent gcc
    if misc.compareVersions(self.version, "5.5.1") == 0:
      patches = ["qt_5.5.1-qwebengine.patch"]  # p1: Fix problem with compilation of QtWebEngine library on gcc 4.7 (some Linux platforms)
    #if misc.compareVersions(self.version, "5.6.1-1") == 0:
    #  patches = ["qt-5.6.1-null_ptr.patch","qt-5.6.1-webview_include.patch"]  # p1: Fix problem with compilation of QtWebEngine library on gcc 4.7 (some Linux platforms)
    # Apply patches
    patch_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
    for p in patches:
      self.patches.append(os.path.join(patch_directory, p))

  def init_variables(self):
    self.archive_file_name = "qt-everywhere-opensource-src-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options = "-opensource -confirm-license -no-rpath"
    if misc.compareVersions(self.version, "5.0.0") >= 0:  # QT 5 : Calibre 7 xcb not compatible
      self.config_options += " -qt-xcb"
    if misc.compareVersions(self.version, "5.5.1") == 0:
      self.pre_configure_commands += ["sed -i \"s%px_proxy_factory_get_proxies(factory, url.toEncoded())%px_proxy_factory_get_proxies(factory, url.toEncoded().data())%g\" qtbase/src/network/kernel/qnetworkproxy_libproxy.cpp ; "]

    self.make_movable_archive_commands.append("rm -Rf doc ;")
    self.post_install_commands.append("echo \"%s\" > bin/qt.conf ;" % (qtconf_template.substitute(install_dir=self.install_directory)))

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    prerequisite_str = qt_template.substitute(install_dir=install_dir,
                                              ld_library_path=misc.get_ld_library_path(),
                                              qt_version=misc.major(self.version))
    if misc.get_calibre_version() == '7':
      prerequisite_str += "export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb\n"
    return prerequisite_str

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    prerequisite_str = qt_configuration_template.substitute(install_dir=install_dir,
                                                            ld_library_path=misc.get_ld_library_path(),
                                                            qt_version=misc.major(self.version))
    if misc.get_calibre_version() == '7':
      prerequisite_str += "QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb\n"
    return prerequisite_str

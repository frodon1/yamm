#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "QWT"
qwt_template = """
#------ qwt ------
export QWTHOME="%install_dir" # pour PARAVIS
export QWT_INCLUDES=${QWTHOME}/include
export %ld_library_path=${QWTHOME}/lib:${%ld_library_path}
"""
qwt_template = misc.PercentTemplate(qwt_template)

qwt_configuration_template = """
#------ qwt ------
QWTHOME="$install_dir" # pour PARAVIS
QWT_INCLUDES=%(QWTHOME)s/include
ADD_TO_$ld_library_path: %(QWTHOME)s/lib
"""
qwt_configuration_template = string.Template(qwt_configuration_template)

class QWT(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
    if self.version.startswith('5'):
      return 'libqwt5-qt4-dev'
    else:
      return 'libqwt-dev'

  def get_debian_build_depends(self):
    if self.version.startswith('4'):
      return ['libqt4-opengl-dev']

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if ('QT' not in self.project_softwares_dict
        and misc.compareVersions(self.version, "6.1.2") == 0
        and misc.get_calibre_version() != "7"):
      # Ne detecte pas Qt5Svg / QtDesigner en natif C9
      self.patches.append(os.path.join(self.patch_directory, "qwt_remove_qtsvg_qtdesigner.patch"))

  def init_variables(self):
    self.archive_file_name = "qwt-" + self.version + ".tar.bz2"
    self.archive_type = "tar.bz2"
    self.compil_type = "qmake"
    self.pro_file = "qwt.pro"
    self.parallel_make = "1"
    if misc.split_version(self.ordered_version) < [6, 0, 0]:
      self.specific_configure_command = "sed \"s:/usr/local/qwt-" + self.version + ":$CURRENT_SOFTWARE_INSTALL_DIR:\" qwtconfig.pri > qwtconfig.pri_sed ; mv qwtconfig.pri_sed qwtconfig.pri ; "
    else:
      self.specific_configure_command = "sed \"s:/usr/local/qwt-\\$\\$QWT_VERSION:$CURRENT_SOFTWARE_INSTALL_DIR:\" qwtconfig.pri > qwtconfig.pri_sed ; mv qwtconfig.pri_sed qwtconfig.pri ; "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["QT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "QT":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "QT":
      self.executor_software_name += "-qt" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qwt_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return qwt_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

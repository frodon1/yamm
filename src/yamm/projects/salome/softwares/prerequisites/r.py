#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "R"
r_template = """
#------ R -----------
export R_HOME="%install_dir/%libdirname/R"
export %ld_library_path=${R_HOME}/lib:${%ld_library_path}
export PATH=${R_HOME}/bin:${PATH}
"""
r_template = misc.PercentTemplate(r_template)

r_configuration_template = """
#------ R -----------
R_HOME="$install_dir/$libdirname/R"
ADD_TO_$ld_library_path: %(R_HOME)s/lib
ADD_TO_PATH: %(R_HOME)s/bin
"""
r_configuration_template = string.Template(r_configuration_template)

class R(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'r-base-dev'

  def get_debian_build_depends(self):
    return 'libreadline-dev'

  def init_variables(self):
    self.archive_file_name    = "R-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "autoconf"
    self.config_options       = "--enable-R-shlib --without-tcltk "

    # MPI4RUN settings
    # Do not use the paraview mpi4run version
    if 'MPI4PY' in self.project_softwares_dict:
      self.parallel_make = "1"

    # On old 64 bits versions, R was installed in lib64 directory
    if misc.is64() and (misc.split_version(self.ordered_version) < [2, 15] or os.path.isdir("/usr/lib64")):
      self.libdirname = "lib64"
    else:
      self.libdirname = "lib"

    # Commands to create a movable binary archive
    # This sed script (multi-line replace) is inspired from
    # http://austinmatzko.com/2008/04/26/sed-multi-line-search-and-replace/
    r_sed = "sed -n -i -e '1h;1!H;${;g;" \
            "s#R_HOME_DIR=.*export R_DOC_DIR#" \
            "R_SHARE_DIR=${R_HOME}/share\\n" \
            "export R_SHARE_DIR\\n" \
            "R_INCLUDE_DIR=${R_HOME}/include\\n" \
            "export R_INCLUDE_DIR\\n" \
            "R_DOC_DIR=${R_HOME}/doc\\n" \
            "export R_DOC_DIR#g;p;}' %s/R/bin/R" % self.libdirname
    self.post_install_commands += ["cp -r %s/R/bin %s/R/bin.orig; " % (self.libdirname, self.libdirname),
                                          "find -name *.so -exec strip {} +",
                                          "rm -rf bin",
                                          r_sed,
                                          "ln -s %s/R/bin " % self.libdirname
                                          ]

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return r_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(),
                                 libdirname = self.libdirname)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return r_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(),
                                 libdirname = self.libdirname)

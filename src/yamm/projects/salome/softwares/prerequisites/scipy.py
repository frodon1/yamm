#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.engine.dependency import Dependency
from yamm.core.engine.tasks import compilation
import os
import string

software_name = "SCIPY"
SCIPY_template = """
# ------ Scipy ------
export PYTHONPATH="%install_dir/lib/python%python_version/site-packages":${PYTHONPATH}
"""
SCIPY_template = misc.PercentTemplate(SCIPY_template)

SCIPY_configuration_template = """
# ------ Scipy ------
ADD_TO_PYTHONPATH: "$install_dir/lib/python$python_version/site-packages"
"""
SCIPY_configuration_template = string.Template(SCIPY_configuration_template)

class SCIPY(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.tips = "We use LAPACK and BLAS environnement variables to choose LAPACK installation"
    data_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
    self.site_file = os.path.join(data_directory, "scipy_site.cfg")

  def get_debian_dev_package(self):
    if self._py3:
        return 'python3-scipy'
    return 'python-scipy'

  def init_variables(self):
    self.archive_file_name = "scipy-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "python"
    self.build_options = "--fcompiler=gnu95"

    site_cfg_file = compilation.AdditionalFile("site.cfg", self.site_file, "site.cfg")
    self.additional_src_files.append(site_cfg_file)

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON", "LAPACK", "NUMPY"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "LAPACK":
      dependency_object = []
      dependency_object.append(Dependency(name="LAPACK",
                                          depend_of=["install_path"],
                                          specific_install_var_name="LAPACK",
                                          specific_install_var_end="/lib/liblapack.so")
                              )
      dependency_object.append(Dependency(name="LAPACK",
                                          depend_of=["install_path"],
                                          specific_install_var_name="BLAS",
                                          specific_install_var_end="/lib/libblas.so")
                              )

    if dependency_name == "SWIG":
      dependency_object.depend_of = ["path"]
    if dependency_name == "NUMPY":
      dependency_object.depend_of = ["path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
      try:
        soft = self.project_softwares_dict['LAPACK']
        lapack_dir = soft.install_directory
        self.pre_build_commands.append("python -c 'file = open(\"site.cfg\", \"a\"); file.write(\"library_dirs = %s/lib\\n\"); file.close()'" % (lapack_dir))
      except KeyError:
        pass
      self.pre_build_commands.append("python -c 'file = open(\"site.cfg\", \"a\"); file.write(\"[blas_opt]\\n\"); file.close()'")
      self.pre_build_commands.append("python -c 'file = open(\"site.cfg\", \"a\"); file.write(\"libraries = f77blas, cblas\\n\"); file.close()'")
      self.pre_build_commands.append("python -c 'file = open(\"site.cfg\", \"a\"); file.write(\"[lapack_opt]\\n\"); file.close()'")
      self.pre_build_commands.append("python -c 'file = open(\"site.cfg\", \"a\"); file.write(\"libraries = lapack, f77blas, cblas\\n\"); file.close()'")
      self.user_dependency_command += " export ATLAS=\"\" ; "

    elif dependency_name == "LAPACK":
      self.executor_software_name += "-la" + misc.transform(version_name)
    elif dependency_name == "SWIG":
      self.executor_software_name += "-sw" + misc.transform(version_name)
    elif dependency_name == "NUMPY":
      self.executor_software_name += "-nu" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return SCIPY_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return SCIPY_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

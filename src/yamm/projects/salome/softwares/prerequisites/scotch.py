#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "SCOTCH"
scotch_template = """
#------ Scotch ------
export SCOTCHDIR="%install_dir"    # export needed for compilation of medsplitter/partitionner
export PATH=${SCOTCHDIR}/bin:${PATH}
"""
scotch_template = misc.PercentTemplate(scotch_template)

scotch_configuration_template = """
#------ Scotch ------
SCOTCHDIR="$install_dir"
ADD_TO_PATH: %(SCOTCHDIR)s/bin
"""
scotch_configuration_template = string.Template(scotch_configuration_template)

class SCOTCH(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
    return 'libscotch-dev'
 
  def init_patches(self):
    SalomeSoftware.init_patches(self)
    self.patches.append(os.path.join(self.patch_directory, "scotch-esmumps.patch"))
    
  def init_variables(self):
    self.archive_file_name = "scotch-" + self.version + ".tar.gz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "specific"
    self.parallel_make     = "1"

    self.specific_configure_command += "cd src/Make.inc ; patch -p1 < " + os.path.join(self.patch_directory, "scotch-" + self.version + ".patch") + " Makefile.inc.i686_pc_linux2 ; "
    self.specific_configure_command += "ln -s Make.inc/Makefile.inc.i686_pc_linux2 ../Makefile.inc"
    self.specific_build_command      = "cd src ; make"
    self.specific_install_command    = "cp -rf * $CURRENT_SOFTWARE_INSTALL_DIR"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return scotch_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return scotch_configuration_template.substitute(install_dir=install_dir)

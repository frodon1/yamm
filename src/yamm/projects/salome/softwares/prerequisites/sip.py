#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "SIP"
sip_template = """
#------ sip ------
export SIP_ROOT_DIR="%install_dir"
export PATH=${SIP_ROOT_DIR}/bin:${PATH}
export PYTHONPATH=${SIP_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
export %ld_library_path=${SIP_ROOT_DIR}/lib/python%python_version/site-packages:${%ld_library_path}
"""
sip_template = misc.PercentTemplate(sip_template)
# WARNING, the export of SIP_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

sip_configuration_template = """
#------ sip ------
SIP_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(SIP_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(SIP_ROOT_DIR)s/lib/python$python_version/site-packages
ADD_TO_$ld_library_path: %(SIP_ROOT_DIR)s/lib/python$python_version/site-packages
"""
sip_configuration_template = string.Template(sip_configuration_template)
# WARNING, the export of SIP_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

class SIP(SalomeSoftware):

  def get_debian_dev_package(self):
    if self._py3:
        return 'python3-sip-dev'
    return 'python-sip-dev'

  def init_variables(self):
    self.archive_file_name = "sip-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"

    sip_bin_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/bin"
    sip_include_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/include/python$PYTHON_VERSION"
    sip_lib_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/lib/python$PYTHON_VERSION/site-packages"
    sip_sip_dir = "$CURRENT_SOFTWARE_INSTALL_DIR/share/sip"

    self.specific_configure_command = "python configure.py -b " + sip_bin_dir + " -d " + sip_lib_dir + " -e " + sip_include_dir + " -v " + sip_sip_dir
    self.specific_build_command = "make"
    self.specific_install_command = "mkdir -p " + sip_bin_dir + " ; " + "mkdir -p " + sip_include_dir + " ; " + "mkdir -p " + sip_lib_dir + " ; " + "mkdir -p " + sip_sip_dir + " ; " + "make install"

  def init_dependency_list(self):
    # mpi flavour only: dependency to OPENMPI exists to set a MPI environment
    # at the beginning of the construction to cover every software needing MPI.

    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return sip_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return sip_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

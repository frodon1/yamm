# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud NEDELEC (OCC)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "SOLVESPACE"
SolveSpace_template = """
#------ solvespace ------
export SOLVESPACE_ROOT_DIR="%install_dir"
export %ld_library_path=${SOLVESPACE_ROOT_DIR}/lib:${%ld_library_path}
"""
solvespace_template = misc.PercentTemplate(SolveSpace_template)

SolveSpace_configuration_template = """
#------ solvespace ------
SOLVESPACE_ROOT_DIR="$install_dir"
ADD_TO_$ld_library_path: %(SOLVESPACE_ROOT_DIR)s/lib
"""
solvespace_configuration_template = string.Template(SolveSpace_configuration_template)

class SOLVESPACE(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name    = "solvespace_" + self.version + ".tgz"
    self.archive_type         = "tar.gz"
    self.compil_type          = "cmake"

    if misc.compareVersions(self.ordered_version, "2.1") >= 0:
      self.config_options += "-DBUILD_GUI=OFF"
    else:
      self.config_options += "-DONLY_LIB=ON"

  def get_type(self):
    return "prerequisite"
  
  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return solvespace_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return solvespace_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "SWIG"
swig_template = """
#------ swig ------
export SWIG_ROOT_DIR="%install_dir"
export SWIG_LIB=${SWIG_ROOT_DIR}/share/swig/%swig_version
export PATH=${SWIG_ROOT_DIR}/bin:$PATH
"""
swig_template = misc.PercentTemplate(swig_template)
# WARNING, the export of SWIG_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

swig_configuration_template = """
#------ swig ------
SWIG_ROOT_DIR="$install_dir"
SWIG_LIB=%(SWIG_ROOT_DIR)s/share/swig/$swig_version
ADD_TO_PATH: %(SWIG_ROOT_DIR)s/bin
"""
swig_configuration_template = string.Template(swig_configuration_template)
# WARNING, the export of SWIG_ROOT_DIR is used by kernel at configure time (Cmake mode), do not remove

class SWIG(SalomeSoftware):

  # We do not reimplement __init__ method here because we do not want to force
  # the name of the software and thus the __init__ method would have nothing
  # to do. We want to allow other names for this software in subclasses (in
  # SWIG_FOR_COCAGNE for instance).

  def get_debian_dev_package(self):	
      if self.version.startswith('3'):
          return 'swig3.0'
      else:
          return 'swig'

  def get_debian_package(self):    
    return ''

  def init_variables(self):
    self.archive_file_name = "swig-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"

    if misc.major(self.ordered_version) > 1:  # Versions 2.x.x
      self.config_options = "--without-pcre "
      if misc.split_version(self.ordered_version) >= [2, 0, 8] :  # Versions >= 2.0.8
        self.config_options += "--without-octave "
    if self._py3:
      self.config_options += '--program-suffix=3.0 '
      self.post_install_commands.append("cd bin && ln -sf swig3.0 swig")
      if 'PYTHON' not in self.project_softwares_dict:
        self.config_options += '--with-python=$(which python3) '

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
        dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
        self.executor_software_name += "-py" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return swig_template.substitute(install_dir=install_dir, swig_version=self.ordered_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return swig_configuration_template.substitute(install_dir=install_dir, swig_version=self.ordered_version)

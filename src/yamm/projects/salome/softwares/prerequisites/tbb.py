#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "TBB"
tbb_template = """
#------ TBB. ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
"""
tbb_template = misc.PercentTemplate(tbb_template)

tbb_configuration_template = """
#------ TBB. ------
ADD_TO_$ld_library_path: "$install_dir/lib"
"""
tbb_configuration_template = string.Template(tbb_configuration_template)

class TBB(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'libtbb-dev'

  def init_variables(self):
    if self.version[:1] >= "4" :
      self.archive_file_name = "tbb" + self.version + "_src.tgz"
    else:
      self.archive_file_name = "tbb-" + self.version + ".tar.gz"

    self.archive_type      = "tar.gz"
    self.compil_type       = "specific"
    self.parallel_make     = "1"

    self.specific_build_command      = "make"
    self.specific_install_command    = "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib ; cd build ; cp -rf ./linux*release/* $CURRENT_SOFTWARE_INSTALL_DIR/lib ; cd .. ; cp -rf ./include $CURRENT_SOFTWARE_INSTALL_DIR/include"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return tbb_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return tbb_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "TCL"
tcl_template = """
#------ tcl ------
export TCLDIR="%install_dir"   # export needed by Code_Aster
export PATH=${TCLDIR}/bin:$PATH
export %ld_library_path=${TCLDIR}/lib:$%ld_library_path
export TCL_LIBRARY=${TCLDIR}/lib/libtcl%version.so
"""
tcl_template = misc.PercentTemplate(tcl_template)

tcl_configuration_template = """
#------ tcl ------
TCLDIR="$install_dir"
ADD_TO_PATH: %(TCLDIR)s/bin
ADD_TO_$ld_library_path: %(TCLDIR)s/lib
TCL_LIBRARY=%(TCLDIR)s/lib/libtcl$version.so
"""
tcl_configuration_template = string.Template(tcl_configuration_template)

class TCL(SalomeSoftware):
  """
  ATTENTION: les logiciels TIX et TOGL dépendent de TCL et s'installent dans son répertoire.
  Si le répertoire d'install de TCL est supprimé, et résinstallé en mode update,
  il faut bien penser à supprimer aussi les répertoire d'installation de TIX et TOGL.
  """

  def get_debian_dev_package(self):
    return 'tcl%s.%s-dev' %(misc.major(self.version),
                            misc.minor(self.version))

  def get_debian_package(self):
    return 'tcl%s.%s' %(misc.major(self.version),
                        misc.minor(self.version))

  def init_variables(self):
    self.archive_file_name = "tcl" + self.version + "-src.tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    self.parallel_make = "1"
    self.can_delete_src = False

    self.specific_configure_command = "$CURRENT_SOFTWARE_SRC_DIR/unix/configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR"
    self.specific_configure_command += " --enable-threads"
    if misc.is64():
      self.specific_configure_command += " --enable-64bit"
    self.specific_install_command = "make install"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["GCC"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "GCC":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    short_version = "%s.%s" % (misc.major(self.version), misc.minor(self.version))
    return tcl_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), version=short_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    short_version = "%s.%s" % (misc.major(self.version), misc.minor(self.version))
    return tcl_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), version=short_version)

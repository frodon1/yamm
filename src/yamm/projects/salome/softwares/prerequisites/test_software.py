#!/usr/bin/env test_software
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "TEST_SOFTWARE"
test_software_template = """
"""
test_software_template = misc.PercentTemplate(test_software_template)

test_software_configuration_template = """
"""
test_software_configuration_template = string.Template(test_software_configuration_template)

class TEST_SOFTWARE(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name     = "test-" + self.version + ".tgz"
    self.archive_type          = "tar.gz"

    self.compil_type           = "specific"
    self.parallel_make         = "1"

    env_directory = self.project_options.get_global_option("version_directory")
    self.specific_build_command = "cp $CURRENT_SOFTWARE_SRC_DIR/test.py  $CURRENT_SOFTWARE_BUILD_DIR ; "
    self.specific_build_command += "echo print \\'My installation directory is : $CURRENT_SOFTWARE_INSTALL_DIR\\' >> $CURRENT_SOFTWARE_BUILD_DIR/test.py ; "
    self.specific_build_command += "echo print \\'The file prerequisites.sh is there : %s\\' >> $CURRENT_SOFTWARE_BUILD_DIR/test.py ; "%(os.path.join(env_directory,"salome_prerequisites.sh"))
    self.specific_build_command += "echo print \\'The file modules.sh is there : %s\\' >> $CURRENT_SOFTWARE_BUILD_DIR/test.py ; "%(os.path.join(env_directory,"salome_modules.sh"))
    self.specific_install_command = "cp -r $CURRENT_SOFTWARE_BUILD_DIR/*   $CURRENT_SOFTWARE_INSTALL_DIR ; "

    self.replaceEnvFilePath("salome_prerequisites.sh","test.py")
    self.replaceEnvFilePath("salome_modules.sh","test.py")

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    return "# TEST_SOFTWARE"

  def get_configuration_str(self, specific_install_dir=""):
    return "# TEST_SOFTWARE"

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os

software_name = "TIX"
tix_template = """
#------ tix ------
export %ld_library_path="${TCLDIR}/lib/Tix%version":$%ld_library_path
"""
tix_template = misc.PercentTemplate(tix_template)
tix_template_standalone = """
#------ tix ------
export %ld_library_path="%install_dir/lib/Tix%version":$%ld_library_path
"""
tix_template_standalone = misc.PercentTemplate(tix_template_standalone)


tix_configuration_template = """
#------ tix ------
ADD_TO_{ld_library_path}: "%(TCLDIR)s/lib/Tix{version}"
"""
# tix_configuration_template = string.Template(tix_configuration_template)
tix_configuration_template_standalone = """
#------ tix ------
ADD_TO_{ld_library_path}: "{install_dir}/lib/Tix{version}"
"""
# tix_configuration_template_standalone = string.Template(tix_configuration_template_standalone)

class TIX(SalomeSoftware):

  def get_debian_dev_package(self):
    return 'tix-dev'

  def init_variables(self):
    self.archive_file_name = "Tix" + self.version + "-src.tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options += " --enable-threads"
    if misc.is64():
      self.config_options += " --enable-64bit"

    if 'TCL' not in self.project_softwares_dict:
      try:
        # tclversion must be 8.5 or 8.6 for example
        tclversion = self.version_object.get_software_version('TCL')[:3]
        self.config_options += " --with-tcl=/usr/lib/tcl%s" % tclversion
        self.config_options += " --exec-prefix=${CURRENT_SOFTWARE_INSTALL_DIR}"
      except misc.LoggerException:
        # TCL software not defined in version
        pass
    if 'TK' not in self.project_softwares_dict:
      try:
        # tkversion must be 8.5 or 8.6 for example
        tkversion = self.version_object.get_software_version('TK')[:3]
        self.config_options += " --with-tk=/usr/lib/tk%s" % tkversion
      except misc.LoggerException:
        # TK software not defined in version
        pass

    self.pre_install_commands = ["mkdir -p %s " % self.install_directory]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["TCL", "TK"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "TCL":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    elif dependency_name == "TK":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "TCL":
      self.executor_software_name += "-tcl" + misc.transform(version_name)
      self.config_options += " --with-tcl=$TCLDIR/lib"
    elif dependency_name == "TK":
      self.executor_software_name += "-tk" + misc.transform(version_name)
      self.config_options += " --with-tk=$TKDIR/lib"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    if 'TCL' in self.project_softwares_dict:
      return tix_template.substitute(ld_library_path=misc.get_ld_library_path(),
                                     version=self.version)
    else:
      install_dir = self.install_directory
      if specific_install_dir != "":
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
      return tix_template_standalone.substitute(install_dir=install_dir,
                                                ld_library_path=misc.get_ld_library_path(),
                                                version=self.version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)

    d = {'ld_library_path': misc.get_ld_library_path(),
        'version': self.version,
        'install_dir': install_dir}

    if 'TCL' in self.project_softwares_dict:
      return tix_configuration_template.format(**d)
    else:
      return tix_configuration_template_standalone.format(**d)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "TK"
tk_template = """
#------ tk ------
export TKDIR="%install_dir"   # export needed by Code_Aster
export PATH=${TKDIR}/bin:$PATH
export %ld_library_path=${TKDIR}/lib:$%ld_library_path
export TK_LIBRARY=${TKDIR}/lib/libtk%version.so
"""
tk_template = misc.PercentTemplate(tk_template)

tk_configuration_template = """
#------ tk ------
TKDIR="$install_dir"
ADD_TO_PATH: %(TKDIR)s/bin
ADD_TO_$ld_library_path: %(TKDIR)s/lib
TK_LIBRARY=%(TKDIR)s/lib/libtk$version.so
"""
tk_configuration_template = string.Template(tk_configuration_template)

class TK(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.specific_configure_command_after = ""

  def get_debian_dev_package(self):
    return 'tk%s.%s-dev' %(misc.major(self.version),
                           misc.minor(self.version))

  def get_debian_package(self):
    return 'tk%s.%s' %(misc.major(self.version),
                       misc.minor(self.version))

  def init_variables(self):
    self.archive_file_name = "tk" + self.version + "-src.tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    self.parallel_make = "1"
    self.can_delete_src = False

    self.specific_configure_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; $CURRENT_SOFTWARE_SRC_DIR/unix/configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR"
    self.specific_configure_command += " --enable-threads"
    if misc.is64():
      self.specific_configure_command += " --enable-64bit"
    self.specific_build_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make"
    self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR; make install; "
    self.specific_install_command += "cp $CURRENT_SOFTWARE_INSTALL_DIR/include/*.h $TCLDIR/include"
    self.specific_configure_command += self.specific_configure_command_after

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["TCL"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "TCL":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "TCL":
      self.executor_software_name += "-tcl" + misc.transform(version_name)
      self.specific_configure_command_after += " --with-tcl=$TCLDIR/lib"
      self.specific_configure_command_after += " --with-tclinclude=$TCLDIR/include"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    short_version = "%s.%s" % (misc.major(self.version), misc.minor(self.version))
    return tk_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), version=short_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    short_version = "%s.%s" % (misc.major(self.version), misc.minor(self.version))
    return tk_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), version=short_version)

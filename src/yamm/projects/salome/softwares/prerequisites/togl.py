#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string

software_name = "TOGL"
togl_template = """
#------ togl ------
export %ld_library_path="${TCLDIR}/lib/Togl%version":$%ld_library_path
"""
togl_template = misc.PercentTemplate(togl_template)

togl_configuration_template = """
#------ togl ------
ADD_TO_$ld_library_path: "%(TCLDIR)s/lib/Togl$version"
"""
togl_configuration_template = string.Template(togl_configuration_template)

class TOGL(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.specific_configure_command_after = ""
    self.tips = "Togl must be compiled in its source directory"

  def get_debian_dev_package(self):
    return 'libtogl-dev'

  def init_variables(self):
    self.archive_file_name = "Togl-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    self.parallel_make = "1"

    self.specific_configure_command = "$CURRENT_SOFTWARE_SRC_DIR/configure --enable-threads --enable-shared"
    if misc.is64():
      self.specific_configure_command += " --enable-64bit"
    if 'TCL' not in self.project_softwares_dict:
      try:
        # tclversion must be 8.5 or 8.6 for example
        tclversion = self.version_object.get_software_version('TCL')[:3]
        self.specific_configure_command += " --with-tcl=/usr/lib/tcl%s" % tclversion
        self.specific_configure_command += " --prefix=${CURRENT_SOFTWARE_INSTALL_DIR}"
        self.specific_configure_command += " --exec-prefix=${CURRENT_SOFTWARE_INSTALL_DIR}"
        self.post_install_commands = ["cd $CURRENT_SOFTWARE_INSTALL_DIR/lib/Togl{0} "
                                      "&& ln -s libTogl{0}.so ./libTogl.so.1"
                                      "&& ln -s libTogl.so.1 ./libTogl.so"
                                      .format(self.version)]
      except misc.LoggerException:
        # TCL software not defined in version
        pass
    if 'TK' not in self.project_softwares_dict:
      try:
        # tkversion must be 8.5 or 8.6 for example
        tkversion = self.version_object.get_software_version('TK')[:3]
        self.specific_configure_command += " --with-tk=/usr/lib/tk%s" % tkversion
      except misc.LoggerException:
        # TK software not defined in version
        pass

    self.specific_build_command = "make"
    self.specific_install_command = "make install"
    # TODO symbolic link
    self.specific_configure_command += self.specific_configure_command_after

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["TCL", "TK"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "TCL":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    elif dependency_name == "TK":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "TCL":
      self.executor_software_name += "-tcl" + misc.transform(version_name)
      self.specific_configure_command_after += " --with-tcl=$TCLDIR/lib"
      self.specific_configure_command_after += " --with-tclinclude=$TCLDIR/include"
      self.post_install_commands = ["cd $TCLDIR/lib/Togl{0} && ln -s libTogl{0}.so ./libTogl.so.1"
                                    "&& ln -s libTogl.so.1 ./libTogl.so"
                                    .format(self.version)]
    elif dependency_name == "TK":
      self.executor_software_name += "-tk" + misc.transform(version_name)
      self.specific_configure_command_after += " --with-tk=$TKDIR/lib"
      self.specific_configure_command_after += " --with-tkinclude=$TKDIR/include"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    if 'TCL' in self.project_softwares_dict:
      return togl_template.substitute(version=self.version, ld_library_path=misc.get_ld_library_path())
    else :
      return ""

  def get_configuration_str(self, specific_install_dir=""):
    if 'TCL' in self.project_softwares_dict:
      return togl_configuration_template.substitute(version=self.version, ld_library_path=misc.get_ld_library_path())
    else :
      return ""

#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware

software_name = "TRALICS"

class TRALICS(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "tralics-src-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "specific"
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_build_command = "cd src && make -j%s" % parallel_make
    self.specific_install_command = \
        "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/bin && " \
        "cp src/tralics $CURRENT_SOFTWARE_INSTALL_DIR/bin && " \
        "cp -r confdir $CURRENT_SOFTWARE_INSTALL_DIR"

  def get_type(self):
    return "prerequisite"

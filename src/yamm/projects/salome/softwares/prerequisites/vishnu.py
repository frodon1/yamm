#  Copyright (C) 2012, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Boris Daix & Samuel KORTAS (EDF R&D)

import os
import string
from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware

software_name = "VISHNU"

vishnu_template_pre_3 = """
#------ VISHNU ------
VISHNU_HOME="%install_dir"
export PATH=$VISHNU_HOME/bin:$PATH
export MANPATH=$VISHNU_HOME/man:$MANPATH
export %ld_library_path=$VISHNU_HOME/lib:$%ld_library_path
export PYTHONPATH=$VISHNU_HOME/lib:$PYTHONPATH
export PYTHONPATH=$VISHNU_HOME/lib/swig_output:$PYTHONPATH
export VISHNU_CONFIG_FILE=$VISHNU_HOME/etc/VISHNU_EDF.cfg
"""
vishnu_template_pre_3 = misc.PercentTemplate(vishnu_template_pre_3)

vishnu_template = """
#------ VISHNU ------
VISHNU_HOME="%install_dir"
export PATH=$VISHNU_HOME/bin:$PATH
export MANPATH=$VISHNU_HOME/share/man:$MANPATH
export %ld_library_path=$VISHNU_HOME/lib:$%ld_library_path
export VISHNU_CONFIG_FILE=$VISHNU_HOME/etc/VISHNU_EDF.cfg
"""
vishnu_template = misc.PercentTemplate(vishnu_template)

vishnu_configuration_template_pre_3 = """
#------ VISHNU ------
VISHNU_HOME="$install_dir"
ADD_TO_PATH: %(VISHNU_HOME)s/bin
ADD_TO_MANPATH: %(VISHNU_HOME)s/man
ADD_TO_$ld_library_path: %(VISHNU_HOME)s/lib
ADD_TO_PYTHONPATH: %(VISHNU_HOME)s/lib
ADD_TO_PYTHONPATH: %(VISHNU_HOME)s/lib/swig_output
VISHNU_CONFIG_FILE=%(ISHNU_HOME)s/etc/VISHNU_EDF.cfg
"""
vishnu_configuration_template_pre_3 = string.Template(vishnu_configuration_template_pre_3)

vishnu_configuration_template = """
#------ VISHNU ------
VISHNU_HOME="$install_dir"
ADD_TO_PATH: %(VISHNU_HOME)s/bin
ADD_TO_MANPATH: %(VISHNU_HOME)s/share/man
ADD_TO_$ld_library_path: %(VISHNU_HOME)s/lib
VISHNU_CONFIG_FILE=%(VISHNU_HOME)s/etc/VISHNU_EDF.cfg
"""
vishnu_configuration_template = string.Template(vishnu_configuration_template)

class VISHNU(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "vishnu_" + self.version + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.config_options += "-DCOMPILE_UMS=ON "
    self.config_options += "-DCOMPILE_TMS=ON "
    self.config_options += "-DCOMPILE_FMS=ON "
    self.config_options += "-DCOMPILE_IMS=ON "

    if misc.major(self.ordered_version) < 2:
      self.config_options += "-DENABLE_SWIG:BOOL=ON "
    if misc.major(self.ordered_version) < 3:
      self.config_options += "-DENABLE_PYTHON:BOOL=ON "
      self.config_options += "-DCLIENT_ONLY=ON "
    else:
      self.config_options += "-DCOMPILE_CLIENT_CLI=ON "
      self.config_options += "-DENABLE_PYTHON:BOOL=OFF "
      self.config_options += "-DENABLE_SWIG:BOOL=OFF "
      self.config_options += "-DOPENSSL_INCLUDE_DIR=/usr/include/openssl "

    self.post_install_commands = \
        ["mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/etc", \
         "cp $CURRENT_SOFTWARE_SRC_DIR/VISHNU_EDF.cfg $CURRENT_SOFTWARE_INSTALL_DIR/etc"]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["BOOST"]
    if misc.major(self.ordered_version) < 2:
      self.software_dependency_dict['exec'].append("LIBCRYPT")
    if misc.major(self.ordered_version) < 3:
      self.software_dependency_dict['build'] += ["SWIG"]
      self.software_dependency_dict['exec'] += ["OMNIORB", "LOGSERVICE", "DIET", "PYTHON"]
    else:
      self.software_dependency_dict['exec'].append("ZMQ")

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    if dependency_name == "SWIG":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "BOOST":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "PYTHON":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "OMNIORB":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "LOGSERVICE":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "DIET":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "ZMQ":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "LIBCRYPT":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "SWIG":
      self.executor_software_name += "-sw" + misc.transform(version_name)
      self.config_options += "-DSWIG_DIR:PATH=$SWIG_INSTALL_DIR "
      self.config_options += "-DSWIG_EXECUTABLE:PATH=$SWIG_INSTALL_DIR/bin/swig "
    elif dependency_name == "BOOST":
      self.executor_software_name += "-bst" + misc.transform(version_name)
      self.config_options += "-DBOOST_ROOT:PATH=$BOOST_INSTALL_DIR "
    elif dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
      self.config_options += "-DPYTHON_DIR:PATH=$PYTHON_INSTALL_DIR "
      self.config_options += "-DPYTHON_INCLUDE_DIR:PATH=$PYTHON_INSTALL_DIR/include/python%s " % self.python_version
      self.config_options += "-DPYTHON_LIBRARY:PATH=$PYTHON_INSTALL_DIR/lib/libpython%s.so " % self.python_version
    elif dependency_name == "OMNIORB":
      self.executor_software_name += "-orb" + misc.transform(version_name)
      self.config_options += "-DOMNIORB4_DIR:PATH=$OMNIORB_INSTALL_DIR "
    elif dependency_name == "LOGSERVICE":
      self.executor_software_name += "-ls" + misc.transform(version_name)
      self.config_options += "-DLOGSERVICE_DIR:PATH=$LOGSERVICE_INSTALL_DIR "
    elif dependency_name == "DIET":
      self.executor_software_name += "-diet" + misc.transform(version_name)
      self.config_options += "-DDIET_DIR:PATH=$DIET_INSTALL_DIR "
    elif dependency_name == "LIBCRYPT":
      self.executor_software_name += "-crypt" + misc.transform(version_name)
      self.config_options += "-DLIBCRYPT_LIB:PATH=$LIBCRYPT_INSTALL_DIR/libcrypt.a "
    elif dependency_name == "ZMQ":
      self.executor_software_name += "-zmq" + misc.transform(version_name)
      self.config_options += "-DZMQ_DIR=$ZMQ_INSTALL_DIR "

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    if misc.major(self.ordered_version) < 3:
      return vishnu_template_pre_3.substitute(install_dir=install_dir,
                                              ld_library_path=misc.get_ld_library_path())
    else:
      return vishnu_template.substitute(install_dir=install_dir,
                                        ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    if misc.major(self.ordered_version) < 3:
      return vishnu_configuration_template_pre_3.substitute(install_dir=install_dir,
                                              ld_library_path=misc.get_ld_library_path())
    else:
      return vishnu_configuration_template.substitute(install_dir=install_dir,
                                        ld_library_path=misc.get_ld_library_path())

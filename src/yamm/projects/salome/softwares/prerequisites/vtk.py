#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "VTK"
VTK_template = """
#------ vtk ------
export VTKHOME="%install_dir"
export PATH=${VTKHOME}/bin:$PATH
export %ld_library_path=${VTKHOME}/lib:$%ld_library_path
export %ld_library_path=${VTKHOME}/lib/vtk%vtk_version:$%ld_library_path
export PYTHONPATH=${VTKHOME}/lib/python%python_version/site-packages:${PYTHONPATH}
export VTKSUFFIX=%vtk_version
"""
VTK_template = misc.PercentTemplate(VTK_template)

VTK_configuration_template = """
#------ vtk ------
VTKHOME="$install_dir"
ADD_TO_PATH: %(VTKHOME)s/bin
ADD_TO_$ld_library_path: %(VTKHOME)s/lib
ADD_TO_$ld_library_path: %(VTKHOME)s/lib/vtk$vtk_version
ADD_TO_PYTHONPATH: %(VTKHOME)s/lib/python$python_version/site-packages
VTKSUFFIX=$vtk_version
"""
VTK_configuration_template = string.Template(VTK_configuration_template)

class VTK(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "vtk-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"

    self.user_dependency_command = "unset PYTHONHOME ; export PYTHONPATH=$CURRENT_SOFTWARE_INSTALL_DIR/lib/python$PYTHON_VERSION/site-packages:$PYTHONPATH ; "
    self.pre_install_commands = ["mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib/python$PYTHON_VERSION/site-packages"]
    self.config_options += " -DBUILD_SHARED_LIBS:BOOL=ON -DCMAKE_BUILD_TYPE:STRING=Release -DVTK_USE_HYBRID:BOOL=ON "
    self.config_options += " -DVTK_USE_PARALLEL:BOOL=ON -DVTK_USE_PATENTED:BOOL=OFF -DVTK_USE_RENDERING:BOOL=ON "
    self.config_options += " -DVTK_USE_GL2PS:BOOL=ON -DVTK_WRAP_TCL:BOOL=OFF -DVTK_WRAP_TK:BOOL=OFF -DVTK_USE_TK:BOOL=OFF "
    flag64 = ""
    if misc.is64():
      flag64 = "-m64"
    self.config_options += "-DCMAKE_CXX_FLAGS:STRING=%s -DCMAKE_C_FLAGS:STRING=%s -DVTK_HAVE_GETSOCKNAME_WITH_SOCKLEN_T=1 " % (flag64, flag64)
    self.config_options += "-DVTK_WRAP_PYTHON:BOOL=ON "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path", "python_version"]
    elif dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      python_bin = " -DPYTHON_EXECUTABLE:STRING=${PYTHON_INSTALL_DIR}/bin/python${PYTHON_VERSION} "
      python_include = " -DPYTHON_INCLUDE_PATH:STRING=${PYTHON_INSTALL_DIR}/include/python${PYTHON_VERSION} "
      python_lib = " -DPYTHON_LIBRARY=${PYTHON_INSTALL_DIR}/lib/libpython${PYTHON_VERSION}.so "
      self.config_options += python_bin + python_include + python_lib
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return VTK_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version, vtk_version="-" + self.version[:3])

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return VTK_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version, vtk_version="-" + self.version[:3])

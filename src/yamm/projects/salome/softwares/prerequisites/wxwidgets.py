#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "WXWIDGETS"

wxwidgets_template = """
#------ wxwidgets ------
export WXWIDGETSHOME="%install_dir"
export PATH=${WXWIDGETSHOME}/bin:$PATH
export %ld_library_path=${WXWIDGETSHOME}/lib:${%ld_library_path}
"""
wxwidgets_template = misc.PercentTemplate(wxwidgets_template)

wxwidgets_configuration_template = """
#------ wxwidgets ------
WXWIDGETSHOME="$install_dir"
ADD_TO_PATH: %(WXWIDGETSHOME)s/bin
ADD_TO_$ld_library_path: %(WXWIDGETSHOME)s/lib
"""
wxwidgets_configuration_template = string.Template(wxwidgets_configuration_template)

class WXWIDGETS(SalomeSoftware):

  def init_variables(self):
    self.software_source_type = "archive"
    self.archive_file_name    = "wxWidgets-" + self.version + ".tar.bz2"
    self.archive_type         = "tar.bz2"

    self.compil_type          = "autoconf"
    self.config_options        = "--disable-gui --enable-monolithic --with-libjpeg=no --with-libpng=no --with-regex=builtin --with-libtiff=no --with-zlib=builtin --with-odbc=builtin --with-expat=builtin --with-libmspack=no --with-sdl=no "

  def init_dependency_list(self):
    self.software_dependency_list = ["PYTHON"]

  #def get_dependency_object_for(self, dependency_name):
    #dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    #if dependency_name == "AUTOCONF":
      #dependency_object.depend_of = ["path"]
    #if dependency_name == "LIBTOOL":
      #dependency_object.depend_of = ["path"]
    #return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return wxwidgets_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return wxwidgets_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

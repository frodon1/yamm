#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2012, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud NEDELEC (OCC)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "YAMS"

YAMS_template = """
# ------ Yams -------------
YAMSHOME="%install_dir/Tools/Yams-V%{version}"
export DISTENE_YAMS_DOC_PDF=${YAMSHOME}/Docs/YamsUserManual.pdf
export PATH=${YAMSHOME}/bin:$PATH
"""
YAMS_template = misc.PercentTemplate(YAMS_template)

YAMS_configuration_template = """
# ------ Yams -------------
YAMSHOME="$install_dir/Tools/Yams-V${version}"
DISTENE_YAMS_DOC_PDF=%(YAMSHOME)s/Docs/YamsUserManual.pdf
ADD_TO_PATH: %(YAMSHOME)s/bin
"""
YAMS_configuration_template = string.Template(YAMS_configuration_template)

class YAMS(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "Yams" + "-V" + self.version + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "rsync"
    # EDF specifics
    if misc.is64():
      self.archive_address = os.path.join(self.archive_address, "..", "bin64")
    else:
      self.archive_address = os.path.join(self.archive_address, "..", "bin32")
    pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["DISTENE"]
    pass

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return YAMS_template.substitute(install_dir=install_dir, version=self.version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return YAMS_configuration_template.substitute(install_dir=install_dir, version=self.version)

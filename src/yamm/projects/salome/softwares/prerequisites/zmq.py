#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Samuel KORTAS (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "ZMQ"
zmq_template = """
#------ zmq ------
export %ld_library_path=%install_dir/lib:${%ld_library_path}
"""
zmq_template = misc.PercentTemplate(zmq_template)

zmq_configuration_template = """
#------ zmq ------
ADD_TO_$ld_library_path: $install_dir/lib
"""
zmq_configuration_template = string.Template(zmq_configuration_template)

class ZMQ(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "zeromq-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options += "CPPFLAGS=-I$E2FSPROGS_INSTALL_DIR/include LDFLAGS=-L$E2FSPROGS_INSTALL_DIR/lib "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["E2FSPROGS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "E2FSPROGS":
        dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "E2FSPROGS":
      self.executor_software_name += "-e2fs" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return zmq_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return zmq_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

modules_list  = ["medfichier", "libbatch", "netgen", "yacsgen", "preopenturns_tool", "openturns_tool", "medcoupling", "paravisaddons"]
modules_list += ["homard_tool", "xdata", "eficasv1", "padder", "eficas_nouveau", "configuration"]
modules_list += ["cgns", "map"]
modules_list += ["openturns_subset", "openturns_doc"]
modules_list += ["openturns_adirsam", "openturns_distfunc", "openturns_svm", "openturns_lhs", "openturns_pmml"]
modules_list += ["openturns_lm", "openturns_robopt", "openturns_morris", "openturns_fftw", "openturns_mixmod", "openturns_wrapy"]
# SALOME_CFD
modules_list += ["eos", "saturne_dbg", "neptune_dbg", "neptune", "code_syrthes"]
#SALOME_CND
modules_list += ["scriptsMaillagesCndCarmel3d"]
# for couplage magneto-thermo and CARMEL3D CND
modules_list += ["interpol","carmel3d",]
#OTGUI
modules_list += ["otgui"]

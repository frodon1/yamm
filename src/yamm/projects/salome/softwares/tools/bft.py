#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "BFT"
bft_template = """
#------ bft ------
BFTHOME="%install_dir"
#export PATH=${BFTHOME}/bin:$PATH
#export %ld_library_path=${BFTHOME}/lib:${%ld_library_path}
export BFT_DISABLE_VERSION_CHECK=1
"""
bft_template = misc.PercentTemplate(bft_template)

bft_configuration_template = """
#------ bft ------
BFTHOME="$install_dir"
#ADD_TO_PATH=%(BFTHOME)s/bin
#ADD_TO_$ld_library_path: %(BFTHOME)s/lib
BFT_DISABLE_VERSION_CHECK=1
"""
bft_configuration_template = string.Template(bft_configuration_template)

class BFT(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "bft-" + self.version + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.install_dir = "keep"
    try:
      soft = self.project_softwares_dict['CGNS']
      self.install_directory = soft.install_directory
    except KeyError:
      pass

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return bft_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return bft_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

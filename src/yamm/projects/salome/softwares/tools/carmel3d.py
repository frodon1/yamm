#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Martine PAOLILLO (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CODE_CARMEL3D"
carmel3d_template = """
#------ carmel3d ------
export CARMEL3DROOT="%install_dir"
export Carmel3D_BinDirectory=${CARMEL3DROOT}/Bin
export CARMELCND_PYSCRIPTS_PBDIR=${CARMEL3DROOT}/PasBloque/PB
export PATH=${CARMELCND_PYSCRIPTS_PBDIR}:${Carmel3D_BinDirectory}:${PATH}
export PYTHONPATH=${CARMELCND_PYSCRIPTS_PBDIR}:${Carmel3D_BinDirectory}:${PYTHONPATH}
"""
carmel3d_template = misc.PercentTemplate(carmel3d_template)

carmel3d_configuration_template = """
#------ carmel3d ------
CARMEL3DROOT="$install_dir"
Carmel3D_BinDirectory=%(CARMEL3DROOT)s/Bin
CARMELCND_PYSCRIPTS_PBDIR=%(CARMEL3DROOT)s/PasBloque/PB
ADD_TO_PATH: %(Carmel3D_BinDirectory)s:%(CARMELCND_PYSCRIPTS_PBDIR)s
ADD_TO_PYTHONPATH: %(Carmel3D_BinDirectory)s:%(CARMELCND_PYSCRIPTS_PBDIR)s
"""
carmel3d_configuration_template = string.Template(carmel3d_configuration_template)

class CODE_CARMEL3D(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.software_source_type = "software"
    version_for_archive = self.version
    self.archive_file_name = "Code_carmel3d-" + version_for_archive + ".tgz"  # tgz non placé sur le serveur NEPAL. Pas les droits pour diffuser les sources
    self.archive_type = "tar.gz"

    self.tag = self.version
    self.root_repository = "/projets/projets.002/carmel3d.001/frequentiel/V_242_test"
    self.compil_type = "specific"
    self.specific_configure_command = ""
    self.specific_build_command = "rm -rf $CURRENT_SOFTWARE_BUILD_DIR "
#    self.specific_configure_command = "cp -rf /projets/projets.002/carmel3d.001/frequentiel/V_242_test/* $CURRENT_SOFTWARE_SRC_DIR ;"
# #09/06/2015 pb de libhdf5.so et execution des .exe dans un shell : on ne connait pas HDF5HOME
# #09/06/2015 pas besoin de recompiler Code_Carmel3D
# 09/06/2015    self.specific_configure_command += "cp -rf $CURRENT_SOFTWARE_SRC_DIR/* $CURRENT_SOFTWARE_BUILD_DIR ; "
# 09/06/2015    self.specific_configure_command += "cd $CURRENT_SOFTWARE_BUILD_DIR/Compil ; "
# 09/06/2015    self.specific_configure_command += "cp -f Make.inc Make.inc_ini ; "
# 09/06/2015    self.specific_configure_command += " sed 's/\$(DIRMED)/\$(MED3HOME)/' Make.inc > Make.inc_salome ; "
# 09/06/2015    self.specific_configure_command += "mv -f Make.inc_salome Make.inc ; "
# 09/06/2015    self.specific_configure_command += " sed 's/\$(DIRHDF5)/\$(HDF5HOME)/' Make.inc > Make.inc_salome;"
# 09/06/2015    self.specific_configure_command += "mv -f Make.inc_salome Make.inc ; "
# 09/06/2015    self.specific_configure_command += " sed 's/USE_MUMPS    = 1/USE_MUMPS    = 0/' Make.inc > Make.inc_salome;"
# 09/06/2015    self.specific_configure_command += "mv -f Make.inc_salome Make.inc ; "
# 09/06/2015    if misc.split_version(self.ordered_version) >= [7, 6]:
# 09/06/2015      self.specific_configure_command += " sed 's/libhdf5\.a/libhdf5\.so/' Make.inc > Make.inc_salome ; "
# 09/06/2015      self.specific_configure_command += "mv -f Make.inc_salome Make.inc ; "
# 09/06/2015    self.specific_build_command      = " cd $CURRENT_SOFTWARE_BUILD_DIR/Compil ; make ;"

# Generation de la documentation (mis en suspend)
# suppression de l'installation de la doc avec V2_4_0 - 20140217 car elle est supprimée dans le code - c'est provisoire

# MPdocsupp    self.specific_build_command     += "make cblockvector_class.f90; make cblockmatrix_class.f90;"
# MPdocsupp    self.specific_build_command     += "make cblockprecond_module.f90 ;make cblocksolver_module.f90;"
# MPdocsupp    self.specific_build_command     += "make doc;"

# 09/06/2015# Installation des binaires carmel
# 09/06/2015    self.specific_install_command    = "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/Bin ; cp -f $CURRENT_SOFTWARE_BUILD_DIR/Compil/*.exe $CURRENT_SOFTWARE_INSTALL_DIR/Bin ;"

# Installation du repertoire Wizard complet et remplacement dans cluster.cfg
    self.specific_install_command = "cp -rf /projets/projets.002/carmel3d.001/frequentiel/V_242_test/* $CURRENT_SOFTWARE_SRC_DIR ;"
    self.specific_install_command += "cp -rf $CURRENT_SOFTWARE_SRC_DIR/Wizard/PasBloque $CURRENT_SOFTWARE_INSTALL_DIR ;"
# Installation des binaires carmel
    self.specific_install_command += 'cd $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque ; cp -rf Bin ../Bin ;sed "0,/rootdir/s/rootdir/rootdirsalome/g" cluster.cfg > cluster.cfg.salome ; mv -f cluster.cfg.salome cluster.cfg ; sed "s|rootdirsalome|rootdir = $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque \\n#rootdir |g" cluster.cfg > cluster.cfg.salome ; mv -f cluster.cfg.salome cluster.cfg ;'

# Installation du repertoire Wizard complet et remplacement dans cluster.cfg si compilation du code
# 09/06/2015    self.specific_install_command    += 'mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque ; cp -f $CURRENT_SOFTWARE_SRC_DIR/Wizard/PasBloque/*.cfg  $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque ; cp -f $CURRENT_SOFTWARE_SRC_DIR/Wizard/PasBloque/*.sh  $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque ; cp -rf $CURRENT_SOFTWARE_SRC_DIR/Wizard/PasBloque/PB $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque ; cd $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque ; sed "0,/rootdir/s/rootdir/rootdirsalome/g" cluster.cfg > cluster.cfg.salome ; mv -f cluster.cfg.salome cluster.cfg ; sed "s|rootdirsalome|rootdir = $CURRENT_SOFTWARE_INSTALL_DIR/PasBloque \\n#rootdir |g" cluster.cfg > cluster.cfg.salome ; mv -f cluster.cfg.salome cluster.cfg ; ln -s ../Bin Bin'


# TODO FIN
# Installation des guides pdf de documentation
# MPdocsupp    self.specific_install_command   += "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/share/doc ; cp -f $CURRENT_SOFTWARE_BUILD_DIR/Doc/*.pdf $CURRENT_SOFTWARE_INSTALL_DIR/share/doc ;"
#    self.specific_install_command   += "cp -rf $CURRENT_SOFTWARE_BUILD_DIR/Doc/API/html $CURRENT_SOFTWARE_INSTALL_DIR/share/doc ;" # non généré car tous les sources sont visibles à partir des pages html
    self.post_install_commands = [""]

  def get_type(self):
    return "tool"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["HDF5", "MEDFICHIER", "METIS",
                                             "PYCRYPTO"]
    # self.software_dependency_dict['exec'] = ["HDF5","MEDFICHIER","METIS","MUMPS"] # à analyser

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "HDF5":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "METIS":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "PYCRYPTO":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return carmel3d_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return carmel3d_configuration_template.substitute(install_dir=install_dir)

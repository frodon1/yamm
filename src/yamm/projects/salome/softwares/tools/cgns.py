#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CGNS"
cgns_template = """
#------ Cgns ------
export CGNSDIR="%install_dir"
#export PATH=${CGNSDIR}/bin:$PATH
#export %ld_library_path=${CGNSDIR}/lib:$%ld_library_path
#export PYTHONPATH=${CGNSDIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
cgns_template = misc.PercentTemplate(cgns_template)

cgns_configuration_template = """
#------ Cgns ------
CGNSDIR="$install_dir"
#ADD_TO_PATH: %(CGNSDIR)s/bin
#ADD_TO_$ld_library_path: %(CGNSDIR)s/lib
#ADD_TO_PYTHONPATH: %(CGNSDIR)s/lib/python$python_version/site-packages
"""
cgns_configuration_template = string.Template(cgns_configuration_template)

class CGNS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.cfd_install_dir = ""
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "cgnslib_" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "configure_with_space"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory
    self.config_options = "--enable-lfs"
    if misc.is64():
      self.config_options += " --enable-64bit"
    self.pre_install_commands = ["mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/include $CURRENT_SOFTWARE_INSTALL_DIR/lib"]
    self.install_dir = "keep"
    if self.cfd_install_dir != "":
      self.install_directory = self.cfd_install_dir

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["CFD"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CFD":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CFD":
      try:
        soft = self.project_softwares_dict['CFD']
        self.cfd_install_dir = soft.install_directory
      except KeyError:
        pass

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cgns_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return cgns_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CODE_SYRTHES"
syrthes_template = """
#------ Code Syrthes ------
export SYRTHESROOT="%install_dir"
export NOM_ARCH=%nom_arch
export SYRTHES4_HOME=${SYRTHESROOT}/arch/${NOM_ARCH}
export PATH=${SYRTHES4_HOME}/share/syrthes:${PATH}
export PATH=${SYRTHES4_HOME}/bin:${PATH}
export PYTHONPATH=${SYRTHES4_HOME}/lib/syrthesGui:${PYTHONPATH}
"""
syrthes_template = misc.PercentTemplate(syrthes_template)

syrthes_configuration_template = """
#------ Code Syrthes ------
SYRTHESROOT="$install_dir"
NOM_ARCH=$nom_arch
SYRTHES4_HOME=%(SYRTHESROOT)s/arch/%(NOM_ARCH)s
ADD_TO_PATH: %(SYRTHES4_HOME)s/share/syrthes
ADD_TO_PATH: %(SYRTHES4_HOME)s/bin
ADD_TO_PYTHONPATH: %(SYRTHES4_HOME)s/lib/syrthesGui
"""
syrthes_configuration_template = string.Template(syrthes_configuration_template)

class CODE_SYRTHES(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = "syrthes" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("syrthes")

    self.compil_type = "specific"

    # Specific_install command necessarily ends with the below command
    self.specific_install_command += "cp -f setup.ini.salome setup.ini ; ./syrthes_install.py ; "
    
    self.specific_install_command = misc.insert_string(self.specific_install_command, "cd $CURRENT_SOFTWARE_INSTALL_DIR/src/syrthes-install ; ", 0)
    self.specific_install_command = misc.insert_string(self.specific_install_command, "cp -f Install.sh.salome  Install.sh ; ", 0)
    self.specific_install_command = misc.insert_string(self.specific_install_command, "cd $CURRENT_SOFTWARE_INSTALL_DIR/src/syrthes-gui/src ; ", 0)

# SATURNE_INSTALL_DIR is used into setup.ini.salome of SYRTHES code in case of coupling with CFD
    if "CFDSTUDY" not in self.version_object.softwares.keys():
        self.specific_install_command = misc.insert_string(self.specific_install_command, "sed -i 's|^#ple USE=no|ple USE=no|g' setup.ini.salome ; ", 0)
        self.specific_install_command = misc.insert_string(self.specific_install_command, "sed -i 's|^ple USE=yes|#ple USE=yes|g' setup.ini.salome ; ", 0)
        self.specific_install_command = misc.insert_string(self.specific_install_command, "sed -i 's/syrthescfd INSTALL=yes/syrthescfd INSTALL=no/'  setup.ini.salome ; ", 0)
        self.specific_install_command = misc.insert_string(self.specific_install_command, "cd $CURRENT_SOFTWARE_INSTALL_DIR/src/syrthes-install ; ", 0)
    # Workaround to compile with /usr/bin/mpi when OPENMPI is not present
    # in the composition of Salome such as Calibre version
    if "OPENMPI" not in self.version_object.softwares.keys():
      self.specific_install_command = misc.insert_string(self.specific_install_command, "sed -i 's|^mpi  USE=yes  PATH=${OPENMPI_HOME}|mpi  USE=yes  PATH=/usr|g' setup.ini.salome;", 0)
      self.specific_install_command = misc.insert_string(self.specific_install_command, "cd $CURRENT_SOFTWARE_INSTALL_DIR/src/syrthes-install ; ", 0)
    # Specific_install command necessarily starts with the below command
    self.specific_install_command = misc.insert_string(self.specific_install_command, "rsync -az $CURRENT_SOFTWARE_SRC_DIR/ $CURRENT_SOFTWARE_INSTALL_DIR ; ", 0)         

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["HDF5", "GRACE", "LESSTIF",
                                             "MEDFICHIER", "METIS", "SCOTCH",
                                             "PYQWT","CFDSTUDY","FUTURE",
                                             "OPENMPI" ]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    dependency_object.depend_of = ["install_path"]
    return dependency_object
    
  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "HDF5":
      self.executor_software_name += "-hdf" + misc.transform(version_name)
    elif dependency_name == "GRACE":
      self.executor_software_name += "-gra" + misc.transform(version_name)
    elif dependency_name == "LESSTIF":
      self.executor_software_name += "-les" + misc.transform(version_name)
    elif dependency_name == "MEDFICHIER":
      self.executor_software_name += "-med" + misc.transform(version_name)
    elif dependency_name == "METIS":
      self.executor_software_name += "-met" + misc.transform(version_name)
    elif dependency_name == "SCOTCH":
      self.executor_software_name += "-sco" + misc.transform(version_name)
    elif dependency_name == "PYQWT":
      self.executor_software_name += "-pyqw" + misc.transform(version_name)
    elif dependency_name == "CFDSTUDY":
      self.post_install_commands = ["cd $CURRENT_SOFTWARE_INSTALL_DIR/src/syrthes-gui/src ; sh Install.sh ",
      "mv $CURRENT_SOFTWARE_INSTALL_DIR/src/syrthes-gui/src/install $CURRENT_SOFTWARE_INSTALL_DIR/arch/" + self.get_arch() + "/lib/syrthesGui "]
    elif dependency_name == "OPENMPI":
      self.executor_software_name += "-mpi" + misc.transform(version_name)
           
  def get_arch(self):
    machine = os.uname()[-1];
    arch = "Linux" + "_" + machine
    if machine.endswith("86"):
      arch = "Linux"
    elif machine == "ia64":
      arch = "Linux_IA64"
    return arch

  def get_type(self):
    return "tool"
    
  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    nom_arch = self.get_arch()
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return syrthes_template.substitute(install_dir=install_dir, nom_arch=nom_arch)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    nom_arch = self.get_arch()
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return syrthes_configuration_template.substitute(install_dir=install_dir, nom_arch=nom_arch)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2013, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
import string

from yamm.core.base import misc

software_name = "CONFIGURATION"
CONFIGURATION_template = """
#---------- Configuration -------------------
export CONFIGURATION_ROOT_DIR="%install_dir"
export CONFIGURATION_CMAKE_DIR="%install_dir/cmake"
"""
CONFIGURATION_template = misc.PercentTemplate(CONFIGURATION_template)

CONFIGURATION_configuration_template = """
#---------- Configuration -------------------
CONFIGURATION_ROOT_DIR="$install_dir"
CONFIGURATION_CMAKE_DIR="$install_dir/cmake"
"""
CONFIGURATION_configuration_template = string.Template(CONFIGURATION_configuration_template)

class CONFIGURATION(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.root_repository_template = string.Template('https://$server/$pub')
    self.module_gui_sort = 0

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "rsync"

    self.remote_type     = "git"
    self.tag = self.version
    self.configure_occ_software(path="tools", repo_name="configuration.git")

  def get_type(self):
    return "tool"
  
  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return CONFIGURATION_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return CONFIGURATION_configuration_template.substitute(install_dir=install_dir)

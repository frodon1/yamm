#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from __future__ import absolute_import
from .eficasv1 import EFICASV1
from yamm.core.base import misc
import os
import string

software_name = "EFICAS_NOUVEAU"
EFICAS_NOUVEAU_template = """
#------ Eficas_Nouveau -----------
export EFICAS_NOUVEAU_ROOT="%install_dir"
export EFICAS_ROOT="%install_dir"
"""
EFICAS_NOUVEAU_template = misc.PercentTemplate(EFICAS_NOUVEAU_template)

EFICAS_NOUVEAU_configuration_template = """
#------ Eficas_Nouveau -----------
EFICAS_NOUVEAU_ROOT="$install_dir"
EFICAS_ROOT="$install_dir"
"""
EFICAS_NOUVEAU_configuration_template = string.Template(EFICAS_NOUVEAU_configuration_template)

class EFICAS_NOUVEAU(EFICASV1):

  def init_variables(self):
    EFICASV1.init_variables(self)
    self.config_options += " -DBUILD_DOC=ON "

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return EFICAS_NOUVEAU_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return EFICAS_NOUVEAU_configuration_template.substitute(install_dir=install_dir)


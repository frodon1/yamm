#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "EFICASV1"
EFICASV1_template = """
#------ EficasV1 -----------
export EFICAS_ROOT="%install_dir"
"""
EFICASV1_template = misc.PercentTemplate(EFICASV1_template)

EFICASV1_configuration_template = """
#------ EficasV1 -----------
EFICAS_ROOT="$install_dir"
"""
EFICASV1_configuration_template = string.Template(EFICASV1_configuration_template)

class EFICASV1(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)

  def init_variables(self):
    self.archive_file_name = "eficas-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"
    if (misc.compareVersions(self.ordered_version, "V8_2_0") <= 0):
        self.config_options += " -DWITH_OPENTURNS=ON -DIN_SALOME_CONTEXT=ON -DWITH_CARMEL=ON "
        # Pour Salome V7_5_0, le cata ZCraks doit être fourni en attendant qu'il soit intégré dans les sources de smesh.
        self.config_options += " -DWITH_ZCRACKS=ON "
    else:
        self.config_options += " -DWITH_TELEMAC_CATA=ON "

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    self.user_dependency_command = "unset PYTHONHOME ; "

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("eficas.git")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "SIP", "SPHINX",
                                              "SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON", "QT", "PYQT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    elif dependency_name == "QT":
      dependency_object.depend_of = ["path", "python_path"]
    elif dependency_name == "SIP":
      dependency_object.depend_of = ["path", "python_path"]
    elif dependency_name == "PYQT":
      dependency_object.depend_of = ["path", "python_path"]
    elif dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      python_bin = " -DPYTHON_EXECUTABLE=${PYTHON_INSTALL_DIR}/bin/python${PYTHON_VERSION} "
      python_include = " -DPYTHON_INCLUDE_PATH=${PYTHON_INSTALL_DIR}/include/python${PYTHON_VERSION} "
      python_lib = " -DPYTHON_LIBRARY=${PYTHON_INSTALL_DIR}/lib/libpython${PYTHON_VERSION}.so "
      self.config_options += python_bin + python_include + python_lib
      self.config_options += " -DPYVERSIONS_EXE=/fake_python_exe "
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "QT":
      self.executor_software_name += "-qt" + misc.transform(version_name)
    elif dependency_name == "SIP":
      self.executor_software_name += "-sip" + misc.transform(version_name)
    elif dependency_name == "PYQT":
      self.executor_software_name += "-pyqt" + misc.transform(version_name)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return EFICASV1_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return EFICASV1_configuration_template.substitute(install_dir=install_dir)

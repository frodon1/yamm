#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string
import os

software_name = "EOS"
eos_template = """
#------- Code eos ------
export EOS_INSTALL_DIR="%install_dir"
export NEPTUNE_EOS_DATA="${EOS_INSTALL_DIR}/data"
export %ld_library_path=${EOS_INSTALL_DIR}/lib:${%ld_library_path}
export PYTHONPATH=${EOS_INSTALL_DIR}/lib/python%python_version/site-packages/eos:${PYTHONPATH}
"""
eos_template = misc.PercentTemplate(eos_template)

eos_configuration_template = """
#------- Code eos ------
EOS_INSTALL_DIR="$install_dir"
NEPTUNE_EOS_DATA="%(EOS_INSTALL_DIR)s/data"
ADD_TO_$ld_library_path: %(EOS_INSTALL_DIR)s/lib
ADD_TO_PYTHONPATH: %(EOS_INSTALL_DIR)s/lib/python$python_version/site-packages/eos
"""
eos_configuration_template = string.Template(eos_configuration_template)

class EOS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.solver + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory

    self.archive_file_name = "EOS_" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "svn"
    self.tag = self.version
    self.configure_remote_software(internal_server="https://svn.isais.edf.fr",
                                   internal_path="mfee",
                                   internal_repo_name="neptune-eos")

    self.compil_type = "autoconf"
    libdir = "lib/python%s/site-packages/" % self.python_version

    self.config_options += " --install-with-cmake --without-python-api --without-gui --without-doc-doxygen --without-doc-latex --with-python=$PYTHON_INSTALL_DIR"

# TODO dependance chemin dans :
# TODO lib/python2.7/site-packages/eos/eosAva.py ::::: eoshome = "/local00/home/G98918/projets/applis/CFD_CODES/eos-1.3.0.install/data"
# TODO include/EOS/API/EOS_Config.hxx:#define EOS_DATA_DIR "/local00/home/G98918/projets/applis/CFD_CODES/eos-1.3.0.install/data"

# TODO doc/Doc/Makefile, doc/Doc/Interpolator/Makefile , doc/Doc/Components/Makefile , doc/Doc/UsersGuide/Makefile , doc/Doc/CTestTestfile.cmake, doc/Doc/GUIGuide , doc/Doc/ImplementersGuide/Makefile , doc/Doc/DeveloppersGuide/Makefile, doc/Doc/cmake_install.cmake, doc/Doc/Environnement/Makefile, doc/Doc/Environnement/CTestTestfile.cmake, doc/Doc/Environnement/cmake_install.cmake: generer la doc ou changer les chemins dans les fichiers qui seront dans le .run

#    self.pre_configure_commands = ["export LD_LIBRARY_PATH=$PYTHON_INSTALL_DIR/lib:$LD_LIBRARY_PATH ;"]
    # self.pre_configure_commands += ["cp -rf $CURRENT_SOFTWARE_SRC_DIR/* $CURRENT_SOFTWARE_BUILD_DIR ; cd $CURRENT_SOFTWARE_BUILD_DIR"]
    # Installation de la documentation
    # self.pre_install_commands = ["cd $CURRENT_SOFTWARE_BUILD_DIR ; make html; make doc ; "]
    # self.post_install_commands = ["cd $CURRENT_SOFTWARE_BUILD_DIR ; make install-doc ; "]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name in self.get_dependencies():
      dependency_object.depend_of = ["install_path"]
    return dependency_object

#  def update_configuration_with_dependency(self, dependency_name, version_name):

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "tools", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return eos_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(),
				   python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return eos_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(),
						 python_version=self.python_version)


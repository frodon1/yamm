#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "FVM"
fvm_template = """
#------- fvm -------
FVM_DIR="%install_dir"
#export PATH=${FVM_DIR}/bin:${PATH}
#export %ld_library_path=${FVM_DIR}/lib:${%ld_library_path}
"""
fvm_template = misc.PercentTemplate(fvm_template)

fvm_configuration_template = """
#------- fvm -------
FVM_DIR="$install_dir"
#ADD_TO_PATH: %(FVM_DIR)s/bin
#ADD_TO_$ld_library_path: %(FVM_DIR)s/lib
"""
fvm_configuration_template = string.Template(fvm_configuration_template)

class FVM(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "fvm-" + self.version + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.install_dir = "keep"
    try:
      soft = self.project_softwares_dict['CGNS']
      self.install_directory = soft.install_directory
    except KeyError:
      pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["HDF5", "MEDFICHIER", "CGNS", "BFT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "HDF5":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "CGNS":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "BFT":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "BFT":
      self.config_options += " --with-bft=$BFT_INSTALL_DIR "
    if dependency_name == "CGNS":
      self.config_options += " --with-cgns=$CGNS_INSTALL_DIR "
    if dependency_name == "MEDFICHIER":
      self.config_options += " --with-med=$MEDFICHIER_INSTALL_DIR "
    if dependency_name == "HDF5":
      self.config_options += " --with-hdf5=$HDF5_INSTALL_DIR "
      if os.path.exists("/usr/lib/openmpi"):
        self.config_options += " --with-mpi=/usr/lib/openmpi "
      elif os.path.exists("/usr/lib/mpich"):
        self.config_options += " --with-mpi=/usr/lib/mpich "
      self.config_options += " --disable-mpi-io "

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return fvm_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return fvm_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

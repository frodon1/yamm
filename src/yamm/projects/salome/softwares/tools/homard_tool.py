#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "HOMARD_TOOL"
HOMARD_TOOL_template = """
#---------- Homard -------------------
export HOMARD_REP_EXE="%install_dir"
export HOMARD_EXE=homard
"""
HOMARD_TOOL_template = misc.PercentTemplate(HOMARD_TOOL_template)

HOMARD_TOOL_configuration_template = """
#---------- Homard -------------------
HOMARD_REP_EXE="$install_dir"
HOMARD_EXE=homard
"""
HOMARD_TOOL_configuration_template = string.Template(HOMARD_TOOL_configuration_template)

class HOMARD_TOOL(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.archive_file_name = "Homard-" + self.version + ".tgz"
    self.archive_type      = "tar.gz"
    self.compil_type       = "rsync"
    # EDF specifics
    if misc.is64():
      self.archive_address = os.path.join(self.archive_address, "bin64")
    else:
      self.archive_address = os.path.join(self.archive_address, "bin32")

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return HOMARD_TOOL_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return HOMARD_TOOL_configuration_template.substitute(install_dir=install_dir)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Martine PAOLILLO (EDF R&D)


from __future__ import print_function
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "CODE_INTERPOL"
interpol_template = """
#------ interpol ------
export INTERPOLROOT="%install_dir"
export Interpol_BinDirectory=${INTERPOLROOT}/Bin
export PATH=${Interpol_BinDirectory}:${PATH}
"""
interpol_template = misc.PercentTemplate(interpol_template)

interpol_configuration_template = """
#------ interpol ------
INTERPOLROOT="$install_dir"
Interpol_BinDirectory=%(INTERPOLROOT)s/Bin
ADD_TO_PATH: %(Interpol_BinDirectory)s
"""
interpol_configuration_template = string.Template(interpol_configuration_template)

class CODE_INTERPOL(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = "interpol" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("carmel.interpol")

    self.compil_type = "specific"
    self.specific_configure_command = ""

    self.specific_build_command = "cp -rf $CURRENT_SOFTWARE_SRC_DIR/* $CURRENT_SOFTWARE_BUILD_DIR ;mkdir -p $CURRENT_SOFTWARE_BUILD_DIR/Bin; cd $CURRENT_SOFTWARE_BUILD_DIR/Compil ; "
    if misc.split_version(self.ordered_version) > [7, 5]:
      self.specific_build_command += " sed 's/libhdf5\.a/libhdf5\.so/' Make.inc > Make.inc_salome ; "
      self.specific_build_command += "mv -f Make.inc_salome Make.inc ; "
    self.specific_build_command += "make ;"
    self.specific_install_command = "cp -rf $CURRENT_SOFTWARE_BUILD_DIR/Bin $CURRENT_SOFTWARE_INSTALL_DIR ;"
    self.specific_install_command += "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/share/doc ; cp -f $CURRENT_SOFTWARE_BUILD_DIR/Doc/readme.pdf $CURRENT_SOFTWARE_INSTALL_DIR/share/doc ;"
    self.specific_install_command += "cp -f $CURRENT_SOFTWARE_BUILD_DIR/Doc/usage $CURRENT_SOFTWARE_INSTALL_DIR/share/doc ;"
    self.post_install_commands = [" "]
    if self.verbose == 2 :
      print(" interpol :: self.specific_configure_command = ", self.specific_configure_command)
      print(" interpol :: self.specific_build_command     = ", self.specific_build_command)

  def get_type(self):
    return "tool"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["HDF5", "MEDFICHIER", "METIS"]
    # self.software_dependency_dict['exec'] = ["HDF5","MEDFICHIER","METIS","MUMPS"] # à analyser

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name in self.get_dependencies():
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return interpol_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return interpol_configuration_template.substitute(install_dir=install_dir)

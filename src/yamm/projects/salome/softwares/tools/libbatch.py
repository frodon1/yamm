#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "LIBBATCH"
libbatch_template = """
#---------- libBatch -------------------
export LIBBATCH_ROOT_DIR="%install_dir"
export %ld_library_path=${LIBBATCH_ROOT_DIR}/lib:${%ld_library_path}
export PYTHONPATH=$LIBBATCH_ROOT_DIR/lib/python%python_version/site-packages:${PYTHONPATH}
"""
libbatch_template = misc.PercentTemplate(libbatch_template)

libbatch_configuration_template = """
#---------- libBatch -------------------
LIBBATCH_ROOT_DIR="$install_dir"
ADD_TO_$ld_library_path: %(LIBBATCH_ROOT_DIR)s/lib
ADD_TO_PYTHONPATH: %(LIBBATCH_ROOT_DIR)s/lib/python$python_version/site-packages
"""
libbatch_configuration_template = string.Template(libbatch_configuration_template)

class LIBBATCH(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.root_repository_template = string.Template('https://$server/$pub')

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = "libBatch-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    if self.version == "trunk":
      self.tag = ""
    self.configure_occ_software(path="tools", repo_name="libbatch.git")

    self.compil_type = "cmake"


  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "SWIG"]
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    elif dependency_name == "SWIG":
      dependency_object.depend_of = ["path", "install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
      if misc.get_calibre_version() == "9" :
        self.config_options += " -DPYTHON_INCLUDE_DIR=$PYTHON_INSTALL_DIR/include/python%s -DPYTHON_LIBRARY=$PYTHON_INSTALL_DIR/lib/libpython%s.so "%(self.python_version,self.python_version)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "SWIG":
      self.config_options += " -DSWIG_EXECUTABLE:PATH=$SWIG_INSTALL_DIR/bin/swig"  
      self.executor_software_name += "-sw" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libbatch_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return libbatch_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

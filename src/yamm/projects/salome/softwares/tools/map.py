#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "MAP"
map_template = """
#---------- MAP -------------------
MAP_DIRECTORY="%install_dir"
export %ld_library_path=${MAP_DIRECTORY}/lib:${%ld_library_path}
export PYTHONPATH=$MAP_DIRECTORY/lib/python%python_version/site-packages:${PYTHONPATH}
"""
map_template = misc.PercentTemplate(map_template)

map_configuration_template = """
#---------- MAP -------------------
MAP_DIRECTORY="$install_dir"
ADD_TO_$ld_library_path: %(MAP_DIRECTORY)s/lib
ADD_TO_PYTHONPATH: %(MAP_DIRECTORY)s/lib/python$python_version/site-packages
"""
map_configuration_template = string.Template(map_configuration_template)

class MAP(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "map-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"
    if self.version == 'DEV':
      _tarball_dir = 'tarballs'
    else :
      _tarball_dir = os.path.join('stableReleases', self.version)
    self.archive_address = os.path.join("ftp://claui2al.der.edf.fr",
                                        'projets/MAP', _tarball_dir)
    self.src_directory = self.install_directory
    self.build_directory = self.install_directory
    self.install_dir = "keep"
    self.build_dir = "keep_or_create"
    self._init_bad_context_variables()
    self.post_install_commands = ["ctest -V;", ]
    self.parallel_make = "1"

  def _init_bad_context_variables(self):
    """Particular context variables definitions necessary for build and install.

    This variables, in a clean readable build process should be eliminated.

    Building, installing and using MAP relies on a MAP_DIRECTORY environment
    variable that covers all three aspects. Although the build process is
    covered with cmake that maintains a tight control on global variables,
    there are several custom commands that call directly a shell script --
    often named build.sh -- that rely on this variable. Until these command are
    cleaned, MAP_DIRECTORY must be set to an acceptable INSTALL_DIR value.

    Furthermore, Petsc, included in the MAP code and used, for example by the
    fdvgrid components, has a complex build and install process. For the sake
    of its own build and for the fdvgrid components, PETSC_DIR and PETSC_ARCH
    must also be set in a user and build environment
    """
    # # config_options are not seen by custom shells executed at build time
    # # setting them is useless
    # self.config_options += "-DPETSC_DIR:PATH=$CURRENT_SOFTWARE_INSTALL_DIR/public/petsc-3.1-p4 "
    # self.config_options += "-DPETSC_ARCH:STRING=linux-gnu-c-debug "
    self.user_dependency_command += (
      " export MAP_DIRECTORY=$CURRENT_SOFTWARE_INSTALL_DIR; "
      " export PETSC_DIR=$CURRENT_SOFTWARE_INSTALL_DIR/public/petsc-3.1-p4; "
      " export PETSC_ARCH=linux-gnu-c-debug; ")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    self.software_dependency_dict['exec'] = ["VISU", "SMESH", "PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    if dependency_name == "OPENTURNS_TOOL":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "OPENTURNS_TOOL":
      self.executor_software_name += "-opt" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir,
                                 self.executor_software_name)
    return map_template.substitute(install_dir=install_dir,
                                   ld_library_path=misc.get_ld_library_path(),
                                   python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir,
                                 self.executor_software_name)
    return map_configuration_template.substitute(install_dir=install_dir,
                                   ld_library_path=misc.get_ld_library_path(),
                                   python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2015, 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from builtins import str
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.framework import test_suite
import os
import string

software_name = "MEDCOUPLING"
medcoupling_template = """
#------ MEDCoupling ------
export MEDCOUPLING_ROOT_DIR="%install_dir"
export PATH=${MEDCOUPLING_ROOT_DIR}/bin:${PATH}
export %ld_library_path=${MEDCOUPLING_ROOT_DIR}/lib:${%ld_library_path}
export PYTHONPATH=${MEDCOUPLING_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
medcoupling_template = misc.PercentTemplate(medcoupling_template)

medcoupling_configuration_template = """
#------ MEDCoupling ------
MEDCOUPLING_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(MEDCOUPLING_ROOT_DIR)s/bin
ADD_TO_$ld_library_path: %(MEDCOUPLING_ROOT_DIR)s/lib
ADD_TO_PYTHONPATH: %(MEDCOUPLING_ROOT_DIR)s/lib/python$python_version/site-packages
"""
medcoupling_configuration_template = string.Template(medcoupling_configuration_template)

class MEDCOUPLING(SalomeSoftware):

  @classmethod
  def get_module_load_name(cls, version):
    split_version = [str(s) for s in misc.split_version(version)]
    return 'medcoupling/' + '.'.join(split_version)

  def get_debian_dev_package(self):
    split_version = [str(s) for s in misc.split_version(self.ordered_version)]
    return 'medcoupling-%s' % ('.'.join(split_version))

  def init_variables(self):
    self.archive_file_name = "medcoupling-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="tools", repo_name="medcoupling.git")

    self.compil_type = "cmake"

    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    make_test = test_suite.MakeTestSuite(name="make test MEDCOUPLING",
                                         soft_name=self.name,
                                         soft_version=self.version)
    self.test_suite_list = [make_test]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SWIG", "DOXYGEN", "SPHINX",
                                              "SETUPTOOLS", "CMAKE",
                                              "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["PYTHON", "MEDFICHIER", "METIS",
                                             "SCOTCH", "NUMPY", "SCIPY",
                                             "BOOST", "GRAPHVIZ", "CPPUNIT",
                                             "LIBXML2"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    if dependency_name in ["METIS", "SCOTCH", "MEDFICHIER", "CPPUNIT"]:
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "METIS":
      self.config_options += " -DMETIS_ROOT_DIR=${METIS_INSTALL_DIR} "
    elif dependency_name == "SCOTCH":
      self.config_options += " -DSCOTCH_ROOT_DIR=${SCOTCH_INSTALL_DIR} "
    elif dependency_name == "MEDFICHIER":
      self.config_options += " -DMEDFILE_ROOT_DIR=${MEDFICHIER_INSTALL_DIR} "
    elif dependency_name == "CPPUNIT":
      self.config_options += " -DCPPUNIT_ROOT_DIR=${CPPUNIT_INSTALL_DIR} "

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return medcoupling_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return medcoupling_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_extra_test_path_list(self):
    """
    This method returns a list of paths to the software extra tests.
    Define each path relative to the software install directory.
    """
    return ["tests"]

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import str
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.framework import test_suite
import os
import string
from yamm.core.base.misc import is_on_debian, is_on_ubuntu, get_linux_version

software_name = "MEDFICHIER"
medfichier_template = """
#------ med fichier ------
export MED3HOME="%install_dir"
export MEDFICHIER_INSTALL_DIR="%install_dir"
export PATH=${MED3HOME}/bin:${PATH}
export %ld_library_path=${MED3HOME}/lib:${%ld_library_path}
export PYTHONPATH=${MED3HOME}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
medfichier_template = misc.PercentTemplate(medfichier_template)

medfichier_configuration_template = """
#------ med fichier ------
MED3HOME="$install_dir"
MEDFICHIER_INSTALL_DIR="$install_dir"
ADD_TO_PATH: %(MED3HOME)s/bin
ADD_TO_$ld_library_path: %(MED3HOME)s/lib
ADD_TO_PYTHONPATH: %(MED3HOME)s/lib/python$python_version/site-packages
"""
medfichier_configuration_template = string.Template(medfichier_configuration_template)

class MEDFICHIER(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    self.hdf5_config_options = ""

  @classmethod
  def get_module_load_name(cls, version, mpi=False):
    suffixe = 'openmpi' if mpi else 'serial'
    split_version = [str(s) for s in misc.split_version(version)]
    return 'med/%s' % ('.'.join(split_version[:2]) + suffixe)

  def get_debian_dev_package(self):
    # Attention ce paquet debian doit être utilisé avec la commande
    # module: module load med/3.2-serial par exemple.
    suffixe = 'serial'
    if 'ENVHPCOPENMPI' in self.project_softwares_dict or 'OPENMPI' in self.project_softwares_dict:
        suffixe = 'openmpi'
    split_version = [str(s) for s in misc.split_version(self.ordered_version)]
    return 'med-%s' % ('.'.join(split_version[:2]) + suffixe)

  def init_variables(self):
    self.archive_file_name = "med-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    self.compil_type = "autoconf"
    self.config_options += " --enable-mesgerr"
    # Si Python 3 ou gcc trop récent => désactive Python
    if self._py3 or misc.major(misc.get_gcc_version()) > 5:
      self.config_options += " -disable-python"

    # Correction de MED prevue 3.2.1
    if 'HDF5' not in self.project_softwares_dict \
            and misc.compareVersions(self.version, "3.2.1") < 0 \
            and (misc.get_calibre_version() != "7" or
                 (is_on_debian() and get_linux_version() >= '8') or
                 (is_on_ubuntu() and get_linux_version() >= '16')):
        hdf5_pkgconfig = 'hdf5-serial'
        if 'ENVHPCOPENMPI' in self.project_softwares_dict or 'OPENMPI' in self.project_softwares_dict:
            hdf5_pkgconfig = 'hdf5-openmpi'
        hdf5rep = '$(pkg-config --libs-only-L %s | cut -c 3-)' % hdf5_pkgconfig
        self.user_dependency_command += 'export HDF5_INSTALL_DIR="%s" ;' % hdf5rep
        self.hdf5_config_options = " --with-hdf5=$HDF5_INSTALL_DIR "

    self.config_options += self.hdf5_config_options

    make_check = test_suite.MakeCheckTestSuite(name="make check MEDFICHIER",
                                  soft_name=self.name,
                                  soft_version=self.version)
    make_installcheck = test_suite.MakeInstallcheckTestSuite(name="make install check MEDFICHIER",
                                  soft_name=self.name,
                                  soft_version=self.version)
    self.test_suite_list = [make_check, make_installcheck]

    self.make_movable_archive_commands.append("cd bin; "
                                              "rm -rf xmdump; "
                                              "rm -rf mdump; "
                                              "ln -s xmdump3 xmdump; "
                                              "ln -s mdump3 mdump;")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["HDF5", "PYTHON", "ENVHCOPENMPI", "OPENMPI"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    elif dependency_name == "HDF5":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "HDF5":
      self.hdf5_config_options = " --with-hdf5=$HDF5_INSTALL_DIR "
      self.executor_software_name += "-hdf5" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return medfichier_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return medfichier_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "NCS"
ncs_template = """
#------- ncs -------
export NCS_DIR="%install_dir"
#export PATH=${NCS_DIR}/bin:${PATH}
#export %ld_library_path=${NCS_DIR}/lib:${%ld_library_path}
#export PYTHONPATH=${NCS_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
ncs_template = misc.PercentTemplate(ncs_template)

ncs_configuration_template = """
#------- ncs -------
NCS_DIR="$install_dir"
#ADD_TO_PATH: %(NCS_DIR)s/bin
#ADD_TO_$ld_library_path: %(NCS_DIR)s/lib
#ADD_TO_PYTHONPATH: %(NCS_DIR)s/lib/python$python_version/site-packages
"""
ncs_configuration_template = string.Template(ncs_configuration_template)

class NCS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "ncs-" + self.version + ".tgz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.install_dir = "keep"
    try:
      soft = self.project_softwares_dict['CGNS']
      self.install_directory = soft.install_directory
    except KeyError:
      pass

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["BFT", "FVM", "MEI", "ECS",
                                             "LIBXML2"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "BFT":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "FVM":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MEI":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "ECS":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "LIBXML2":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "BFT":
      self.config_options += " --with-bft=$BFT_INSTALL_DIR "
    if dependency_name == "FVM":
      self.config_options += " --with-fvm=$FVM_INSTALL_DIR "
    if dependency_name == "MEI":
      self.config_options += " --with-mei=$MEI_INSTALL_DIR "
    if dependency_name == "ECS":
      self.config_options += " --with-prepro=$ECS_INSTALL_DIR "
    if dependency_name == "LIBXML2":
      self.config_options += " --with-libxml2=$LIBXML2_INSTALL_DIR "
      if os.path.exists("/usr/lib/openmpi"):
        self.config_options += " --with-mpi=/usr/lib/openmpi "
      elif os.path.exists("/usr/lib/mpich"):
        self.config_options += " --with-mpi=/usr/lib/mpich "

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ncs_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ncs_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

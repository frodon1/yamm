#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string
import os

software_name = "NEPTUNE"

NEPTUNE_template = """
#------- Neptune_CFD ------
export PYTHONPATH=${CS_ROOT_DIR}/lib/python%python_version/site-packages/neptune_cfd:${PYTHONPATH}
"""
NEPTUNE_template = misc.PercentTemplate(NEPTUNE_template)

NEPTUNE_configuration_template = """
#------- Neptune_CFD ------
ADD_TO_PYTHONPATH: %(CS_ROOT_DIR)s/lib/python$python_version/site-packages/neptune_cfd
"""
NEPTUNE_configuration_template = string.Template(NEPTUNE_configuration_template)

class NEPTUNE(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = self.module_gui_type.solver + 1

  def init_variables(self):
    SalomeSoftware.init_variables(self)
    self.install_dir = "keep"

    self.archive_file_name = "NEPTUNE_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "svn"
    self.tag = self.version
    self.configure_remote_software(internal_server="https://noeyy727.noe.edf.fr",
                                   internal_path="mfee",
                                   internal_repo_name="ncfd")

    self.compil_type = "autoconf"
    self.gen_file = "sbin/bootstrap"

    # Installation de la documentation neptune
    self.pre_install_commands = ["cd $CURRENT_SOFTWARE_BUILD_DIR ; make html; make doc ; "]
    self.post_install_commands = ["cd $CURRENT_SOFTWARE_BUILD_DIR ; make install-doc ; "]

    try:
      soft = self.project_softwares_dict['CFDSTUDY']
      self.make_movable_archive_commands += soft.make_movable_archive_commands
      self.install_directory = soft.install_directory
    except KeyError:
      self.logger.error("CFDSTUDY est absent")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["EOS","CFDSTUDY", ]

  def update_configuration_with_dependency(self, dependency_name, version_name):
    libdir = "lib/python%s/site-packages/" % self.python_version
    if dependency_name == "CFDSTUDY":
      self.config_options += " --with-code_saturne=$CS_ROOT_DIR/bin/code_saturne --enable-silent-rules "
      self.config_options += " --enable-relocatable "
    if dependency_name == "EOS":
      self.config_options += " --with-eos=$EOS_INSTALL_DIR "

  def get_type(self):
    return "tool"

  def get_substitute_files(self):
    """ Hack pour ne pas modifier les chemins à la création de l'archive binaire
    """
    return []

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.project_softwares_dict['CFDSTUDY'].executor_software_name)
    return NEPTUNE_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.project_softwares_dict['CFDSTUDY'].executor_software_name)
    return NEPTUNE_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

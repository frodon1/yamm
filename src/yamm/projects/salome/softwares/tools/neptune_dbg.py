#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.softwares.tools.neptune import NEPTUNE
from yamm.core.base import misc
import string
import os

software_name = "NEPTUNE_DBG"

NEPTUNE_DBG_template = """
#------- Neptune_CFD Debug ------
export PYTHONPATH=${SATURNE_DBG_ROOT_DIR}/lib/python%python_version/site-packages/neptune_cfd:${PYTHONPATH}
"""
NEPTUNE_DBG_template = misc.PercentTemplate(NEPTUNE_DBG_template)

NEPTUNE_DBG_configuration_template = """
#------- Neptune_CFD Debug ------
ADD_TO_PYTHONPATH: %(SATURNE_DBG_ROOT_DIR)s/lib/python$python_version/site-packages/neptune_cfd
"""
NEPTUNE_DBG_configuration_template = string.Template(NEPTUNE_DBG_configuration_template)

class NEPTUNE_DBG(NEPTUNE):

  def get_module_name_for_appli(self):
    return "NEPTUNE_DBG"

  def init_variables(self):
    NEPTUNE.init_variables(self)

    self.archive_file_name = "NEPTUNE_DBG_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    # Pas de doc
    self.pre_install_commands = []
    self.post_install_commands = []

    if 'SATURNE_DBG' in self.project_softwares_dict:
      soft = self.project_softwares_dict['SATURNE_DBG']
      self.install_directory = soft.install_directory
    
    if 'CFDSTUDY' in self.project_softwares_dict:
      soft = self.project_softwares_dict['CFDSTUDY']
      # Remplacement du path de NEPTUNE_DBG dans ${CFDSTUDY_INSTALL_DIR}/etc/neptune_cfd.cfg:
      # etc/neptune_cfd est créé en post_install de NEPTUNE_DBG soit après l'install de CFSTUDY.
      # Le remplacement de chemin doit etre fait AVANT la création de l'archive binaire de CFSTUDY
      # mais avec la connaissance de NEPTUNE_DBG. Ceci est fait avec la commande suivante
      # qui modifie la make_movable_archive_command de CFDSTUDY dans NEPTUNE_DBG.
      soft.replaceSoftwareInstallPath(software_name, "etc/neptune_cfd.cfg", "__SATURNE_DBG_INSTALL_PATH__" )
      # Création et Mise à jour du fichier neptune_cfd.cfg pour prendre en compte le mode debug dans l'interface CFD
      self.post_install_commands += ['cd ${CFDSTUDY_INSTALL_DIR}/etc ; APPEND=`echo "compute_versions = :$(echo ${CURRENT_SOFTWARE_INSTALL_DIR})"` ; sed "s|# compute_versions =|${APPEND}|" neptune_cfd.cfg.template > neptune_cfd.cfg ; ']

  def init_dependency_list(self):
    NEPTUNE.init_dependency_list(self)
    self.software_dependency_dict['exec'] += ["SATURNE_DBG", "CFDSTUDY", "NEPTUNE"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = NEPTUNE.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CFDSTUDY":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "SATURNE_DBG":
      self.config_options += " --with-code_saturne=$SATURNE_DBG_ROOT_DIR/bin/code_saturne --enable-silent-rules "
      self.config_options += " --enable-relocatable "

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.project_softwares_dict['SATURNE_DBG'].executor_software_name)
    return NEPTUNE_DBG_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.project_softwares_dict['SATURNE_DBG'].executor_software_name)
    return NEPTUNE_DBG_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)


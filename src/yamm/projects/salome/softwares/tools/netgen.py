#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "NETGEN"
netgen_template = """
#---------- netgen -------------------
export NETGENHOME="%install_dir"
export %ld_library_path=${NETGENHOME}/lib:${%ld_library_path}
"""
netgen_template = misc.PercentTemplate(netgen_template)

netgen_configuration_template = """
#---------- netgen -------------------
NETGENHOME="$install_dir"
ADD_TO_$ld_library_path: %(NETGENHOME)s/lib
"""
netgen_configuration_template = string.Template(netgen_configuration_template)

class NETGEN(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.cxxflags = []
    self.ldflags = []
    self.software_source_type = "archive"
    self.tips = "NETGEN is patched with a patch embedded in NETGENPLUGIN " \
                "(see PRENETGENPLUGIN)"
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if misc.split_version(self.ordered_version)[:2] == [4, 9]:
      try:
        occ = self.project_softwares_dict['OCC']
        if occ.version.startswith('7'):
          self.patches.append(os.path.join(self.patch_directory,
                                           "netgen-4.9.13-for-SALOME-OCCT7.0.0-Porting.patch"))
          self.patches.append(os.path.join(self.patch_directory,
                                           "netgen-4.9.13-OCCT7.0.0-IncludeDir-cpp11.patch"))
      except KeyError:
        pass

  def init_variables(self):
    self.software_source_type = "archive"
    self.archive_file_name = "netgen-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"

    if 'TOGL' not in self.project_softwares_dict:
      self.config_options += " --with-togl=/usr/lib"
      self.cxxflags.append("/usr/include")
      self.ldflags.append("/usr/lib")
      self.pre_configure_commands.append('sed -i "s#lTogl1\.7#lTogl#g" ng/Makefile.in')
    if 'TCL' not in self.project_softwares_dict:
      try:
        # tclversion must be 8.5 or 8.6 for example
        tclversion = self.version_object.get_software_version('TCL')[:3]
        self.config_options += " --with-tcl=/usr/lib/tcl%s" % tclversion
        self.cxxflags.append("/usr/include/tcl%s" % tclversion)
        self.ldflags.append("/usr/lib/tcl%s" % tclversion)
      except misc.LoggerException:
        # TCL software not defined in version
        pass
    if 'TK' not in self.project_softwares_dict:
      try:
        # tkversion must be 8.5 or 8.6 for example
        tkversion = self.version_object.get_software_version('TK')[:3]
        self.config_options += " --with-tk=/usr/lib/tk%s" % tkversion
        self.cxxflags.append("/usr/include/tk%s" % tkversion)
        self.ldflags.append("/usr/lib/tk%s" % tkversion)
      except misc.LoggerException:
        # TK software not defined in version
        pass

    self.compil_type = "autoconf"
    if misc.split_version(self.ordered_version)[:2] >= [5, 0]:
      self.gen_commands = ["libtoolize --force --copy --automake",
                           "aclocal -I m4", "autoheader", "autoconf",
                           "automake --add-missing --copy --gnu"]
      self.config_options += ' CXXFLAGS="${CXXFLAGS} -O2 -m64 -std=c++0x " '
      for directory in ('general', 'gprim', 'linalg', 'meshing'):
        self.post_install_commands.append('cp -f $CURRENT_SOFTWARE_SRC_DIR/libsrc/%s/*.h* $CURRENT_SOFTWARE_INSTALL_DIR/include' % directory)
      for include in ('mystdlib.h', 'mydefs.hpp'):
        self.post_install_commands.append('cp -f $CURRENT_SOFTWARE_SRC_DIR/libsrc/include/%s $CURRENT_SOFTWARE_INSTALL_DIR/include' % include)
      for include in ('occgeom.hpp', 'occmeshsurf.hpp'):
        self.post_install_commands.append('cp -f $CURRENT_SOFTWARE_SRC_DIR/libsrc/occ/%s $CURRENT_SOFTWARE_INSTALL_DIR/include' % include)

    # patcher NETGEN_SRC/libsrc/visualization/Makefile.am
    # pour ajouter $(TK_INCLUDES) ?
    version = "%s%s" % (misc.major(self.version), misc.minor(self.version))

    self.pre_configure_commands.append("patch -p1 < $PRENETGENPLUGIN_DIR/netgen%sForSalome.patch ; " % version)
    self.post_install_commands.append("cp $CURRENT_SOFTWARE_BUILD_DIR/config.h $CURRENT_SOFTWARE_SRC_DIR")
    self.post_install_commands.append("sh $PRENETGENPLUGIN_DIR/netgen_copy_include_for_salome $CURRENT_SOFTWARE_SRC_DIR $CURRENT_SOFTWARE_INSTALL_DIR")

    if self.ldflags != []:
      self.ldflags = list(set(self.ldflags))
      ldflags_cmd = " LDFLAGS=\""
      for lf in self.ldflags:
        ldflags_cmd += "-L" + lf + " "
      ldflags_cmd += "\" "
      self.user_dependency_command += ldflags_cmd
    if self.cxxflags != []:
      self.cxxflags = list(set(self.cxxflags))
      cxxflags_cmd = "CXXFLAGS=\""
      for cf in self.cxxflags:
        cxxflags_cmd += "-I" + cf + " "
      cxxflags_cmd += "\" "
      self.user_dependency_command += cxxflags_cmd

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["PRENETGENPLUGIN"]
    self.software_dependency_dict['exec'] = ["OCC", "TOGL", "TCL", "TK", "TIX"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "OCC":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "CASROOT"
    if dependency_name == "PRENETGENPLUGIN":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "PRENETGENPLUGIN_DIR"
    if dependency_name == "TCL":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "TK":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "TOGL":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "TIX":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "OCC":
      self.executor_software_name += "-occ" + misc.transform(version_name)
      self.config_options += " --with-occ=${CASROOT}"
    elif dependency_name == "TCL":
      self.executor_software_name += "-tcl" + misc.transform(version_name)
      self.config_options += " --with-tcl=${TCLDIR}/lib"
      self.cxxflags.append("${TCLDIR}/include")
      self.ldflags.append("${TCLDIR}/lib")
    elif dependency_name == "TK":
      self.executor_software_name += "-tk" + misc.transform(version_name)
      self.config_options += " --with-tk=${TKDIR}/lib"
      self.user_dependency_command += "TCL_INCLUDE=\"${TCLDIR}/include ${TKDIR}/include\" ; "
      self.cxxflags.append("${TKDIR}/include")
      self.ldflags.append("${TKDIR}/lib")
    elif dependency_name == "TOGL":
      self.executor_software_name += "-togl" + misc.transform(version_name)
      self.config_options += " --with-togl=${TCLDIR}/lib/Togl" + version_name
      self.ldflags.append("${TCLDIR}/lib/Togl" + version_name)
    elif dependency_name == "TIX":
      self.executor_software_name += "-tix" + misc.transform(version_name)
      self.ldflags.append("${TCLDIR}/lib/Tix-" + version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return netgen_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return netgen_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

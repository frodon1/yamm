# encoding: utf8
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from builtins import str
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "OPENTURNS_DOC"

class OPENTURNS_DOC(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def get_debian_dev_package(self):
    split_version = misc.split_version(self.ordered_version)
    return 'openturns-%s' % ('.'.join([str(s) for s in split_version]))

  def get_debian_depends(self):
    # Dépendance au paquet imagemagick pour la commande 'convert'
    return ['imagemagick']

  def init_variables(self):
    self.archive_file_name = "openturns-doc-" + self.version + "-src.tar.bz2"
    self.archive_type = "tar.bz2"
    self.compil_type = "cmake"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE"]
    # AJOUT DEPENDANCE: IMAGEMAGICK  (besoin de 'convert') ?
    self.software_dependency_dict['exec'] = ["TRALICS", "LIBXSLT", "OPENTURNS_TOOL"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "TRALICS":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "LIBXSLT":
      dependency_object.depend_of = ["path"]
    if dependency_name == "OPENTURNS_TOOL":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "TRALICS":
      self.executor_software_name += "-tr" + misc.transform(version_name)
      self.config_options += " -DCMAKE_PREFIX_PATH=${TRALICS_INSTALL_DIR} "
      self.config_options += " -DCMAKE_INCLUDE_PATH=${TRALICS_INSTALL_DIR}/confdir "
    elif dependency_name == "LIBXSLT":
      self.executor_software_name += "-xslt" + misc.transform(version_name)
    if dependency_name == "OPENTURNS_TOOL":
      self.config_options += " -DCMAKE_PREFIX_PATH=${OPENTURNS_TOOL_INSTALL_DIR} "

  def get_type(self):
    return "tool"

from __future__ import absolute_import
#  Copyright (C) 2013 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.projects.salome.softwares.tools.openturns_module import openturns_module

software_name = "OPENTURNS_FFTW"

class OPENTURNS_FFTW(openturns_module):

  def get_debian_build_depends(self):
    return 'libfftw3-dev'

  def get_module_name(self):
    return "FFTW"

  def get_archive_prefix(self):
    return "otfftw"

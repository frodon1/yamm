from __future__ import absolute_import
#  Copyright (C) 2013 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.projects.salome.softwares.tools.openturns_module import openturns_module

software_name = "OPENTURNS_LHS"

# Warning: To compile this module, you need to have the Debian package
# texlive-science (or an equivalent in other distributions) installed on your
# computer (latex packages are not yet installable by yamm).
class OPENTURNS_LHS(openturns_module):

  def get_debian_build_depends(self):
    return 'texlive-science'

  def get_module_name(self):
    return "LHS"

  def get_archive_prefix(self):
    return "otlhs"

#  Copyright (C) 2013, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from builtins import str
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.framework import test_suite
import os
import string

openturns_module_template = """
#------ OpenTURNS %module_name module ------
export PYTHONPATH="%install_dir/lib/python%python_version/site-packages":${PYTHONPATH}
export %ld_library_path=%install_dir/lib:${%ld_library_path}
"""
openturns_module_template = misc.PercentTemplate(openturns_module_template)

openturns_module_configuration_template = """
#------ OpenTURNS %module_name module ------
ADD_TO_PYTHONPATH: "$install_dir/lib/python$python_version/site-packages"
ADD_TO_$ld_library_path: $install_dir/lib
"""
openturns_module_configuration_template = string.Template(openturns_module_configuration_template)

class openturns_module(SalomeSoftware):

    def __init__(self, name, version, verbose, **kwargs):
        SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
        self.software_source_type = "archive"

    def get_debian_dev_package(self):
        split_version = [str(s) for s in misc.split_version(self.ordered_version)]
        return 'openturns-%s' % ('.'.join(split_version))

    def init_variables(self):
        self.archive_file_name = self.get_archive_prefix() + "-" + self.version + ".tar.bz2"
        self.archive_type = "tar.bz2"
        self.compil_type = "cmake"
        # To avoid RPath problems
        self.config_options += " -DCMAKE_SKIP_INSTALL_RPATH:BOOL=ON "
        self.config_options += " -DUSE_SPHINX=OFF " # Turn off doc build because of an issue with openturns_morris
                                                    # which download a package from github = proxy problem.
        # if self._py3 and 'SPHINX' not in self.project_softwares_dict:
        #     find_sphinx_build = '$(dpkg -L python3-sphinx |grep sphinx-build)'
        #     self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname %s)" % find_sphinx_build
        #     self.config_options += " -DSPHINX_EXECUTABLE:FILEPATH=%s" % find_sphinx_build

        parallel_flag = self.project_options.get_option(self.name, "parallel_make")
        make_check = test_suite.MakeCheckTestSuite(name="make check %s" % self.name,
                                                   soft_name=self.name,
                                                   soft_version=self.version,
                                                   parallel_flag=parallel_flag)
        make_installcheck = test_suite.MakeInstallcheckTestSuite(name="make install check %s" % self.name,
                                                                 soft_name=self.name,
                                                                 soft_version=self.version,
                                                                 parallel_flag=parallel_flag)

        self.test_suite_list = [make_check, make_installcheck]

    def init_dependency_list(self):
        self.software_dependency_dict['build'] = ["CMAKE", "SWIG", "SPHINX"]
        self.software_dependency_dict['exec'] = ["PYTHON", "OPENTURNS_TOOL"]

    def get_dependency_object_for(self, dependency_name):
        dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
        if dependency_name == "PYTHON":
            dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
        elif dependency_name == "SWIG":
            dependency_object.depend_of = ["path", "install_path"]
        elif dependency_name == "OPENTURNS_TOOL":
            dependency_object.depend_of = ["install_path"]
            dependency_object.specific_install_var_name = "OPENTURNS_HOME"
        elif dependency_name == "CMAKE":
            dependency_object.depend_of = ["path"]
        return dependency_object

    def update_configuration_with_dependency(self, dependency_name, version_name):
        if dependency_name == "PYTHON":
            self.executor_software_name += "-py" + misc.transform(version_name)
            self.config_options += " -DCMAKE_INCLUDE_PATH=${PYTHON_INSTALL_DIR}/include "
            self.config_options += " -DCMAKE_LIBRARY_PATH=${PYTHON_INSTALL_DIR}/lib "
            if misc.get_calibre_version() != "7":
                self.config_options += " -DPYTHON_INCLUDE_DIR=$PYTHON_INSTALL_DIR/include/python%s" % (self.python_version)
                self.config_options += " -DPYTHON_LIBRARY=$PYTHON_INSTALL_DIR/lib/libpython%s.so " % (self.python_version)
        elif dependency_name == "SWIG":
            self.executor_software_name += "-sw" + misc.transform(version_name)
            self.config_options += "-DCMAKE_FIND_ROOT_PATH=ON "
            self.config_options += "-DSWIG_EXECUTABLE=${SWIG_INSTALL_DIR}/bin/swig "
        elif dependency_name == "OPENTURNS_TOOL":
            self.config_options += " -DCMAKE_PREFIX_PATH=${OPENTURNS_HOME} "
            self.executor_software_name += "-ot" + misc.transform(version_name)
        elif dependency_name == "CMAKE":
            self.executor_software_name += "-cm" + misc.transform(version_name)

    def get_type(self):
        return "tool"

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return openturns_module_template.substitute(module_name=self.get_module_name(),
                                                    install_dir=install_dir,
                                                    ld_library_path=misc.get_ld_library_path(),
                                                    python_version=self.python_version)

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(specific_install_dir, self.executor_software_name)
        return openturns_module_configuration_template.substitute(module_name=self.get_module_name(),
                                                                  install_dir=install_dir,
                                                                  ld_library_path=misc.get_ld_library_path(),
                                                                  python_version=self.python_version)

    def get_module_name(self):
        return "Unknown"

    def get_archive_prefix(self):
        return "unknown"

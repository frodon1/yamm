# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from builtins import str
from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.framework import test_suite
import os
import string

software_name = "OPENTURNS_TOOL"

OPENTURNS_TOOL_template = """
#---------- OpenTURNS -------------------
export OPENTURNS_HOME="%install_dir"
export %ld_library_path=${OPENTURNS_HOME}/lib:${%ld_library_path}
export PYTHONPATH=${OPENTURNS_HOME}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
OPENTURNS_TOOL_template = misc.PercentTemplate(OPENTURNS_TOOL_template)

OPENTURNS_TOOL_configuration_template = """
#---------- OpenTURNS -------------------
OPENTURNS_HOME="$install_dir"
ADD_TO_$ld_library_path: %(OPENTURNS_HOME)s/lib
ADD_TO_PYTHONPATH: %(OPENTURNS_HOME)s/lib/python$python_version/site-packages
"""
OPENTURNS_TOOL_configuration_template = string.Template(OPENTURNS_TOOL_configuration_template)

class OPENTURNS_TOOL(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.cmake_include_path = ""
    self.cmake_library_path = ""
    self.cmake_prefix_path = ""
    self.software_source_type = "archive"
    self.patch_directory = os.path.join(os.path.dirname(
                              os.path.abspath(__file__)), "data")

  def get_debian_dev_package(self):
    split_version = [str(s) for s in misc.split_version(self.ordered_version)]
    return 'openturns-%s' % ('.'.join(split_version))

  def init_patches(self):
    SalomeSoftware.init_patches(self)
    if self.ordered_version == "1.5.1":
      # patch openturns tool 1.5.1 to support sphinx 1.1.3 of salome
      self.patches.append(os.path.join(self.patch_directory,
                                       "openturnstool151_fix_sphinx_version.patch"))
    if self.ordered_version == "1.8.0":
      # patch openturns tool 1.8 for OTGUI, fix CMake variable value
      self.patches.append(os.path.join(self.patch_directory,
                                       "openturnstool_otgui.patch"))

  def init_variables(self):
    self.archive_file_name = "openturns-" + self.version + ".tar.bz2"
    self.archive_type = "tar.bz2"
    self.compil_type = "cmake"
    if misc.split_version(self.ordered_version) < [1, 5]:
      self.config_options += " -DSYSTEM_INSTALL=ON "  # To avoid RPath problems
    if self.cmake_include_path != "":
      self.config_options += ' -DCMAKE_INCLUDE_PATH="%s" ' % self.cmake_include_path
    if self.cmake_library_path != "":
      self.config_options += ' -DCMAKE_LIBRARY_PATH="%s" ' % self.cmake_library_path
    if self.cmake_prefix_path != "":
      self.config_options += ' -DCMAKE_PREFIX_PATH="%s" ' % self.cmake_prefix_path
    if misc.split_version(self.ordered_version) > [1, 6]:
      self.config_options += " -DUSE_SPHINX=OFF "  # To avoid documentation problems


    # Fix bug #8017 (wrong paths in the sys.path)
    self.post_install_commands = ["mkdir -p lib/openturns/module"]

    parallel_flag = self.project_options.get_option(self.name, "parallel_make")
    make_check = test_suite.MakeCheckTestSuite(name="make check OPENTURNS_TOOL",
                                                  soft_name=self.name,
                                                  soft_version=self.version,
                                                  parallel_flag=parallel_flag)
    make_installcheck = test_suite.MakeInstallcheckTestSuite(name="make install check OPENTURNS_TOOL",
                                                                soft_name=self.name,
                                                                soft_version=self.version,
                                                                parallel_flag=parallel_flag)

    self.test_suite_list = [make_check, make_installcheck]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "SWIG", "GRAPHVIZ",
                                              "DOXYGEN"]
    self.software_dependency_dict['exec'] = ["PYTHON", "R", "ROTRPACKAGE",
                                             "LIBXML2", "LAPACK", "BISON",
                                             "BOOST", "TBB", "NUMPY", "SCIPY",
                                             "MATPLOTLIB", "MUPARSER", "NLOPT"]
    if misc.split_version(self.ordered_version) >= [1, 5]:
      self.software_dependency_dict['build'] += ["SPHINX"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "R":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "R_HOME"
    elif dependency_name == "ROTRPACKAGE":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "SWIG":
      dependency_object.depend_of = ["path", "install_path"]
    elif dependency_name == "LIBXML2":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "LAPACK":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "TBB":
      dependency_object.depend_of = ["ld_lib_path", "install_path"]
    elif dependency_name == "GRAPHVIZ":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "DOXYGEN":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "NUMPY":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "SCIPY":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "MATPLOTLIB":
      dependency_object.depend_of = ["python_path"]
    elif dependency_name == "CMAKE":
      dependency_object.depend_of = ["path"]
    elif dependency_name == "BISON":
      dependency_object.depend_of = ["path"]
    elif dependency_name == "MUPARSER":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
      self.cmake_include_path += "${PYTHON_INSTALL_DIR}/include;"
      self.cmake_library_path += "${PYTHON_INSTALL_DIR}/lib;"
      if misc.get_calibre_version() == "9":
        self.config_options += " -DPYTHON_INCLUDE_DIR=$PYTHON_INSTALL_DIR/include/python%s -DPYTHON_LIBRARY=$PYTHON_INSTALL_DIR/lib/libpython%s.so "%(self.python_version,self.python_version)
    elif dependency_name == "R":
      self.executor_software_name += "-r" + misc.transform(version_name)
    elif dependency_name == "ROTRPACKAGE":
      self.user_dependency_command += "export R_LIBS=$ROTRPACKAGE_INSTALL_DIR:$R_LIBS ; "
      self.executor_software_name += "-ro" + misc.transform(version_name)
    elif dependency_name == "SWIG":
      # Fix bug in swig detection
      self.executor_software_name += "-sw" + misc.transform(version_name)
      self.config_options += "-DCMAKE_FIND_ROOT_PATH=ON "
      self.config_options += "-DSWIG_EXECUTABLE=$(which swig) "
    elif dependency_name == "LIBXML2":
      self.executor_software_name += "-xm" + misc.transform(version_name)
      self.cmake_prefix_path += "${LIBXML2_INSTALL_DIR};"
      self.replaceSoftwareInstallPath(dependency_name, "lib/cmake/openturns/OpenTURNSConfig.cmake")
    elif dependency_name == "LAPACK":
      self.executor_software_name += "-la" + misc.transform(version_name)
      self.cmake_include_path += "${LAPACK_INSTALL_DIR}/include;"
      self.cmake_library_path += "${LAPACK_INSTALL_DIR}/lib;"
      self.config_options += " -DBLA_VENDOR=Generic "
      self.replaceSoftwareInstallPath(dependency_name, "lib/cmake/openturns/OpenTURNSConfig.cmake")
    elif dependency_name == "TBB":
      self.executor_software_name += "-tbb" + misc.transform(version_name)
      self.cmake_prefix_path += "${TBB_INSTALL_DIR};"
      self.replaceSoftwareInstallPath(dependency_name, "lib/cmake/openturns/OpenTURNSConfig.cmake")
      self.replaceSoftwareInstallPath(dependency_name, "bin/openturns-config")
    elif dependency_name == "GRAPHVIZ":
      self.executor_software_name += "-gr" + misc.transform(version_name)
    elif dependency_name == "DOXYGEN":
      self.executor_software_name += "-dox" + misc.transform(version_name)
    elif dependency_name == "NUMPY":
      self.executor_software_name += "-num" + misc.transform(version_name)
    elif dependency_name == "SCIPY":
      self.executor_software_name += "-sci" + misc.transform(version_name)
    elif dependency_name == "MATPLOTLIB":
      self.executor_software_name += "-mpl" + misc.transform(version_name)
    elif dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "BISON":
      self.executor_software_name += "-bis" + misc.transform(version_name)
    elif dependency_name == "MUPARSER":
      self.executor_software_name += "-mup" + misc.transform(version_name)
      self.cmake_prefix_path += "${MUPARSER_INSTALL_DIR};"
      self.replaceSoftwareInstallPath(dependency_name, "lib/cmake/openturns/OpenTURNSConfig.cmake")
      self.replaceSoftwareInstallPath(dependency_name, "bin/openturns-config")
    elif dependency_name == "BOOST":
      self.executor_software_name += "-bst" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return OPENTURNS_TOOL_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return OPENTURNS_TOOL_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

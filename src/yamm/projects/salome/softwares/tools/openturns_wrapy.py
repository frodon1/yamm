#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.engine.dependency import Dependency
from yamm.core.engine.tasks import compilation
import os
import string

software_name = "OPENTURNS_WRAPY"
otwrapy_template = """
#------ OpenTURNS Wrapy module------
export PYTHONPATH=%install_dir/lib/python%python_version/site-packages:${PYTHONPATH}
"""
otwrapy_template = misc.PercentTemplate(otwrapy_template)

otwrapy_configuration_template = """
#------ OpenTURNS Wrapy module ------
ADD_TO_PYTHONPATH: $install_dir/lib/python$python_version/site-packages
"""
otwrapy_configuration_template = string.Template(otwrapy_configuration_template)

class OPENTURNS_WRAPY(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def get_debian_dev_package(self):
    return 'python-otwrapy'

  def init_variables(self):
    self.archive_file_name = "otwrapy-" + self.version + ".tar.bz2"
    self.archive_type = "tar.gz"
    self.compil_type = "python"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["SETUPTOOLS_OTW"]
    self.software_dependency_dict['exec'] = ["PYTHON", "OPENTURNS_TOOL"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    if dependency_name == "SETUPTOOLS_OTW":
      dependency_object.depend_of = ["python_path"]
    if dependency_name == "OPENTURNS_TOOL":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "OPENTURNS_HOME"
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    elif dependency_name == "OPENTURNS_TOOL":
      self.executor_software_name += "-ot" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return otwrapy_template.substitute(install_dir=install_dir,
                                       python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return otwrapy_configuration_template.substitute(install_dir=install_dir,
                                                     python_version=self.python_version)


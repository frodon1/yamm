# -*- coding: utf-8 *-
#  Copyright (C) 2011-2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.framework import test_suite
import os
import string

software_name = "OTGUI"
class OTGUI(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.cmake_include_path = ""
    self.cmake_library_path = ""
    self.cmake_prefix_path = ""

  def get_debian_dependencies(self):
    # Dépendance au paquet dvipng
    return ['dvipng']

  def init_variables(self):
    self.archive_file_name = "otgui-" + self.version  + ".tar.bz2"
    self.archive_type = "tar.gz"
    self.compil_type = "cmake"
    try:
      qtversion = misc.major(self.version_object.get_software_version('QT'))
      self.config_options += " -DUSE_QT%s=ON " % qtversion
    except misc.LoggerException:
      # Qt not defined in version
      pass
    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname $(dpkg -L python3-sphinx |grep sphinx-build))"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("incertitudes.otgui-phimeca.git",
                                               external_repo_name="incertitudes.otgui.git",
                                               external_server="https://git.forge.pleiade.edf.fr",
                                               external_path="git",
                                               use_proxy=False)
    self.config_options += " -DCMAKE_PREFIX_PATH=\"%s\" " % (self.cmake_prefix_path)
    self.config_options += '-DOTGUI_PYTHON_VERSION=%s ' % self.python_version
    if self._py3 and 'SPHINX' not in self.project_softwares_dict:
      find_sphinx_build = '$(dpkg -L python3-sphinx |grep sphinx-build)'
      self.config_options += " -DSPHINX_ROOT_DIR:FILEPATH=$(dirname %s)" % find_sphinx_build
      self.config_options += " -DSPHINX_EXECUTABLE:FILEPATH=%s" % find_sphinx_build

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "SPHINX", "SETUPTOOLS", "SWIG",
                                              "NUMPYDOC"]
    self.software_dependency_dict['exec'] = ["PYTHON", "OPENTURNS_TOOL", "QWT",
                                             "KERNEL", "GUI", "YACS", "SWIG"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "OPENTURNS_TOOL":
      dependency_object.depend_of = ["path", "ld_lib_path", "install_path"]
    elif dependency_name == "SWIG":
      dependency_object.depend_of = ["path"]
    elif dependency_name == "QWT":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "PYTHON":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "KERNEL":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "GUI":
      dependency_object.depend_of = ["install_path"]
    elif dependency_name == "YACS":
      dependency_object.depend_of = ["install_path"]

    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "OPENTURNS_TOOL":
      self.cmake_prefix_path += "$OPENTURNS_HOME/lib/cmake/openturns;"
    elif dependency_name == "KERNEL":
      self.cmake_prefix_path += "$KERNEL_INSTALL_DIR/salome_adm/cmake_files;"
    elif dependency_name == "GUI":
      self.cmake_prefix_path += "$GUI_INSTALL_DIR/adm_local/cmake_files;"
    elif dependency_name == "YACS":
      self.cmake_prefix_path += "$YACS_INSTALL_DIR/adm/cmake;"
    elif dependency_name == "QWT":
      self.cmake_prefix_path += "$QWT_INSTALL_DIR;"
    elif dependency_name == "PYTHON":
      self.config_options += "-DPYTHON_INCLUDE_DIR=$PYTHON_INSTALL_DIR/include/python%s -DPYTHON_LIBRARY=$PYTHON_INSTALL_DIR/lib/libpython%s.so " % (self.python_version, self.python_version)
    elif dependency_name == "SWIG":
      # Fix bug in swig detection
      self.config_options += "-DCMAKE_FIND_ROOT_PATH=ON "
      self.config_options += "-DSWIG_EXECUTABLE=$(which swig) "

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", self.get_type() + "s", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)

    return_string = "\n"
    return_string += "#------- OTGUI -------\n"
    return_string += "OTGUI_DIR=" + install_dir + "\n"
    return_string += "export LD_LIBRARY_PATH=${OTGUI_DIR}/lib:${LD_LIBRARY_PATH}\n"
    return_string += "export PATH=${OTGUI_DIR}/bin:${PATH}\n"
    return_string += "export PYTHONPATH=${OTGUI_DIR}/lib/python%s/site-packages:${PYTHONPATH}\n"%self.python_version
    return return_string

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", self.get_type() + "s", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)

    return_string = "\n"
    return_string += "#------- OTGUI -------\n"
    return_string += "OTGUI_DIR=" + install_dir + "\n"
    return_string += "ADD_TO_PATH: %(OTGUI_DIR)s/bin\n"
    return_string += "ADD_TO_LD_LIBRARY_PATH: %(OTGUI_DIR)s/lib\n"
    return_string += "ADD_TO_PYTHONPATH: %%(OTGUI_DIR)s/lib/python%s/site-packages\n"%(self.python_version)
    return return_string

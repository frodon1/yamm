#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Guillaume Boulant (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "PADDER"
padder_template = """
#------ padder ------
export PADDERHOME="%install_dir"
"""
padder_template = misc.PercentTemplate(padder_template)

padder_configuration_template = """
#------ padder ------
PADDERHOME="$install_dir"
"""
padder_configuration_template = string.Template(padder_configuration_template)

class PADDER(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)

    if self.version == "r31" :
      self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "padder-" + self.version + "-source.tar.gz"
    self.archive_type      = "tar.gz"

    self.remote_type = "svn"
    self.tag = self.version
    self.configure_remote_software(internal_server="https://svn.forge.pleiade.edf.fr",
                                   internal_path="svn",
                                   internal_repo_name="padder.padderexe")

    self.compil_type       = "specific"
    self.specific_configure_command  = ""
    self.specific_build_command = "cd $CURRENT_SOFTWARE_SRC_DIR ; "
    if misc.get_calibre_version() != "7":
      cgalhome = '${CGALHOME}'
      if 'CGAL' not in self.project_softwares_dict:
          cgalhome = '/usr'
      self.specific_build_command += 'export CGAL_LFLAGS="-L%s/lib -lCGAL -lm -lgmp" ; ' % cgalhome
    if 'HDF5' not in self.project_softwares_dict:
      hdf5_pkgconfig = 'hdf5-serial'
      if 'ENVHPCOPENMPI' in self.project_softwares_dict or 'OPENMPI' in self.project_softwares_dict:
          hdf5_pkgconfig = 'hdf5-openmpi'
      self.specific_build_command += 'export HDF_CFLAGS="$(pkg-config --cflags %s)" ; ' % hdf5_pkgconfig
      self.specific_build_command += 'export HDF_LFLAGS="$(pkg-config --libs %s)" ; ' % hdf5_pkgconfig
    if 'BOOST' not in self.project_softwares_dict:
      self.specific_build_command += 'export BOOST_LFLAGS="-L/usr/lib/x86_64-linux-gnu -lboost_thread" ; '
    if 'MEDFICHIER' not in self.project_softwares_dict:
      medversion = self.version_object.get_software_package('MEDFICHIER')[0]
      suffixe = 'serial'
      if 'OPENMPI' in self.project_softwares_dict:
        suffixe = 'openmpi'
      medmodule = 'med/' + '.'.join(medversion[:-1]) + suffixe
      self.project_options.set_software_option('PADDER', 'module_load', medmodule)
    self.specific_build_command += "make -e"
    self.specific_install_command    = "cd $CURRENT_SOFTWARE_SRC_DIR; export PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR; make install -e"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["HDF5", "CGAL", "MEDFICHIER", "BOOST"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CGAL":
      dependency_object.depend_of = ["install_path", "path", "ld_lib_path"]
      dependency_object.specific_install_var_name = "CGALHOME"
    if dependency_name == "HDF5":
      dependency_object.depend_of = ["install_path", "path", "ld_lib_path"]
      dependency_object.specific_install_var_name = "HDF5HOME"
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = ["install_path", "path", "ld_lib_path"]
      dependency_object.specific_install_var_name = "MED2HOME"
    if dependency_name == "BOOST":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    # Substitution of CGAL install path in the file padder.env (located relative to the padder install path)
    if dependency_name == "CGAL":
      self.executor_software_name += "-cgal" + misc.transform(version_name)
    if dependency_name == "HDF5":
      self.executor_software_name += "-hdf5" + misc.transform(version_name)
    if dependency_name == "MEDFICHIER":
      self.executor_software_name += "-med" + misc.transform(version_name)
    if dependency_name == "BOOST":
      self.executor_software_name += "-boost" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return padder_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return padder_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.framework import test_suite
import os
import string

software_name = "PARAVISADDONS"
paravisaddons_template = """
#------ PARAVISADDONS ------
export PARAVISADDONS_ROOT_DIR="%install_dir"
export PV_PLUGIN_PATH=${PARAVISADDONS_ROOT_DIR}/lib:${PV_PLUGIN_PATH}
export %ld_library_path=${PARAVISADDONS_ROOT_DIR}/lib:${%ld_library_path}
"""
paravisaddons_template = misc.PercentTemplate(paravisaddons_template)

paravisaddons_configuration_template = """
#------ PARAVISADDONS ------
PARAVISADDONS_ROOT_DIR="$install_dir"
ADD_TO_PV_PLUGIN_PATH: %(PARAVISADDONS_ROOT_DIR)s/lib
ADD_TO_$ld_library_path: %(PARAVISADDONS_ROOT_DIR)s/lib
"""
paravisaddons_configuration_template = string.Template(paravisaddons_configuration_template)

class PARAVISADDONS(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "paravisaddons-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("paravisaddons.git")

    self.compil_type = "cmake"
    self.config_options    +=" -DCMAKE_MODULE_PATH=${CONFIGURATION_CMAKE_DIR} "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["KERNEL", "PARAVIEW", 'MEDCOUPLING']

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "dependency_name":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = "MEDCOUPLING_ROOT_DIR"
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "CMAKE":
      self.executor_software_name += "-cm" + misc.transform(version_name)
    elif dependency_name == "PARAVIEW":
      self.executor_software_name += "-pv" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return paravisaddons_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return paravisaddons_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware

software_name = "PREOPENTURNS_TOOL"
class PREOPENTURNS_TOOL(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.tips = "It's a YAMM artifact to copy rotRPackage into the archive directory"
    self.software_source_type = "archive"

  def get_debian_dev_package(self):
    return ['openturns-1.6.1', 'openturns-1.7.0']

  def init_variables(self):
    self.archive_file_name    = "openturns-" + self.version + ".tar.bz2"
    self.archive_type         = "tar.bz2"
    self.compil_type          = "specific"
    self.parallel_make        = "1"
    self.specific_install_command = "cp $CURRENT_SOFTWARE_SRC_DIR/utils/rot_* " + self.project_options.get_global_option("archives_directory")

  def get_type(self):
    return "tool"

  def isArtefact(self):
    return True

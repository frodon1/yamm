#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  Author : Martine PAOLILLO (EDF R&D)

from yamm.projects.salome.softwares.modules.cfdstudy import CFDSTUDY
from yamm.core.base import misc
import string
import os

software_name = "SATURNE_DBG"
saturne_dbg_template = """
#------- Code Saturne Debug ------
export SATURNE_DBG_ROOT_DIR=%install_dir
export PATH=${SATURNE_DBG_ROOT_DIR}/bin:${PATH}
export PATH=${SATURNE_DBG_ROOT_DIR}/libexec/code_saturne:${PATH}
export PYTHONPATH=${SATURNE_DBG_ROOT_DIR}/bin:${PYTHONPATH}
export PYTHONPATH=${SATURNE_DBG_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
export PYTHONPATH=${SATURNE_DBG_ROOT_DIR}/lib/python%python_version/site-packages/code_saturne:${PYTHONPATH}
"""
# SATURNE_INSTALL_DIR variable is needed by SYRTHES code for coupling between SATURNE or NEPTUNE
saturne_dbg_template = misc.PercentTemplate(saturne_dbg_template)

saturne_dbg_configuration_template = """
#------- Code Saturne Debug ------
SATURNE_DBG_ROOT_DIR=$install_dir
ADD_TO_PATH: %(SATURNE_DBG_ROOT_DIR)s/bin
ADD_TO_PATH: %(SATURNE_DBG_ROOT_DIR)s/libexec/code_saturne
ADD_TO_PYTHONPATH: %(SATURNE_DBG_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(SATURNE_DBG_ROOT_DIR)s/lib/python$python_version/site-packages
ADD_TO_PYTHONPATH: %(SATURNE_DBG_ROOT_DIR)s/lib/python$python_version/site-packages/code_saturne
"""
saturne_dbg_configuration_template = string.Template(saturne_dbg_configuration_template)

class SATURNE_DBG(CFDSTUDY):

  def get_module_name_for_appli(self):
    return self.get_saturnedbg_name()

  def init_variables(self):
    CFDSTUDY.init_variables(self)

    self.archive_file_name = "SATURNE_DBG_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.compil_type = "autoconf"

    if misc.compareVersions(self.ordered_version, "4.0.4") <= 0:
      self.pre_configure_commands += ['cd $CURRENT_SOFTWARE_SRC_DIR ; ']
      self.pre_configure_commands += ['sed -i "s#MODULE_NAME = cfdstudy#MODULE_NAME = cfdstudy_dbg#g" salome/cfd_study/resources/Makefile.am ; ']
      self.pre_configure_commands += ['sed -i "s#MODULE_NAME = cfdstudy#MODULE_NAME = cfdstudy_dbg#g" salome/cfd_study/src/CFDSTUDYGUI/Makefile.am ; ']
      self.pre_configure_commands += ['sed -i "s#MODULE_NAME = cfdstudy#MODULE_NAME = cfdstudy_dbg#g" salome/cfd_study/src/CFDSTUDY/Makefile.am ; ']
    else:
      self.pre_configure_commands += ['cd $CURRENT_SOFTWARE_SRC_DIR ; ']
      self.pre_configure_commands += ['sed -i "s#MODULE_NAME = cfdstudy#MODULE_NAME = cfdstudy_dbg#g" salome/cfd_study/resources/Makefile.in ; ']
      self.pre_configure_commands += ['sed -i "s#MODULE_NAME = cfdstudy#MODULE_NAME = cfdstudy_dbg#g" salome/cfd_study/src/CFDSTUDYGUI/Makefile.in ; ']
      self.pre_configure_commands += ['sed -i "s#MODULE_NAME = cfdstudy#MODULE_NAME = cfdstudy_dbg#g" salome/cfd_study/src/CFDSTUDY/Makefile.in ; ']

    self.config_options += " --enable-debug "

    if 'CFDSTUDY' in self.project_softwares_dict:
      soft = self.project_softwares_dict['CFDSTUDY']
      # Remplacement du path de SATURNE_DBG dans ${CFDSTUDY_INSTALL_DIR}/etc/code_saturne.cfg pour la variable compute_versions
      # etc/code_saturne.cfg est créé en post_install de SATURNE_DBG soit après l'install de CFSTUDY.
      # Le remplacement de chemin doit etre fait AVANT la création de l'archive binaire de CFSTUDY
      # mais avec la connaissance de SATURNE_DBG. Ceci est fait avec la commande suivante
      # qui modifie la make_movable_archive_command de CFDSTUDY dans SATURNE_DBG.
      soft.replaceSoftwareInstallPath(software_name, "etc/code_saturne.cfg" )
      # Création et Mise à jour du fichier code_saturne.cfg pour prendre en compte le mode debug dans l'interface CFD
      self.post_install_commands += ['cd ${CFDSTUDY_INSTALL_DIR}/etc ; APPEND=`echo "compute_versions = :$(echo ${CURRENT_SOFTWARE_INSTALL_DIR})"` ; sed "s|# compute_versions =|${APPEND}|" code_saturne.cfg.template > code_saturne.cfg ; ']

  def init_dependency_list(self):
    self.software_dependency_dict['exec'] += ["CFDSTUDY", "EOS"]

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "EOS":
      self.config_options += " --with-eos=$EOS_INSTALL_DIR "

  def get_make_doc_cmd(self):
    return ""

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
     install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return saturne_dbg_template.substitute(install_dir=install_dir,
                                             ld_library_path=misc.get_ld_library_path(),
                                             python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    # place dans env.d/envProducts.cfg
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return saturne_dbg_configuration_template.substitute(install_dir=install_dir,
                                                           ld_library_path=misc.get_ld_library_path(),
                                                           python_version=self.python_version)

  def get_type(self):
    return "tool"

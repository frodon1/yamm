#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string
software_name = "SCRIPTS_MAILLAGES_CND_CARMEL3D"
scriptsMaillagesCndCarmel3d_template = """
#------ scriptsMaillagesCndCarmel3d ------
export SCRIPTSMAILLAGES_CARMEL3DCND_ROOT="%install_dir"
export PATH=${SCRIPTSMAILLAGES_CARMEL3DCND_ROOT}:${PATH}
export PYTHONPATH=${SCRIPTSMAILLAGES_CARMEL3DCND_ROOT}:${PYTHONPATH}
"""
scriptsMaillagesCndCarmel3d_template = misc.PercentTemplate(scriptsMaillagesCndCarmel3d_template)

scriptsMaillagesCndCarmel3d_configuration_template = """
#------ scriptsMaillagesCndCarmel3d ------
SCRIPTSMAILLAGES_CARMEL3DCND_ROOT="$install_dir"
ADD_TO_PATH: %(SCRIPTSMAILLAGES_CARMEL3DCND_ROOT)s
ADD_TO_PYTHONPATH: %(SCRIPTSMAILLAGES_CARMEL3DCND_ROOT)s
"""
scriptsMaillagesCndCarmel3d_configuration_template = string.Template(scriptsMaillagesCndCarmel3d_configuration_template)

class SCRIPTS_MAILLAGES_CND_CARMEL3D(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)
    self.archive_file_name = self.name + "_SRC.tar.gz"
    self.archive_type = "tar.gz"

    # No proxy for SCRIPTS_MAILLAGES_CND_CARMEL3D
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "")

    self.remote_type = "git"
    self.tag = self.version
    self.configure_remote_software_pleiade_git("carmel.scripts-maillage-cnd-carmel3d")

    # Installation du repertoire scripts maillages
    self.compil_type = "rsync"

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return scriptsMaillagesCndCarmel3d_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return scriptsMaillagesCndCarmel3d_configuration_template.substitute(install_dir=install_dir)

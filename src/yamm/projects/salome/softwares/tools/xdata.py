#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "XDATA"
XDATA_template = """
#---------- Xdata -------------------
export XDATA_ROOT_DIR="%install_dir"
export PATH=$XDATA_ROOT_DIR/bin:$PATH
export PYTHONPATH=$XDATA_ROOT_DIR/lib/python%python_version/site-packages/xdata:${PYTHONPATH}
"""
XDATA_template = misc.PercentTemplate(XDATA_template)

XDATA_configuration_template = """
#---------- Xdata -------------------
XDATA_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(XDATA_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(XDATA_ROOT_DIR)s/lib/python$python_version/site-packages/xdata
"""
XDATA_configuration_template = string.Template(XDATA_configuration_template)

class XDATA(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"

  def init_variables(self):
    self.archive_file_name = "xdata-" + self.version + ".tar.gz"
    self.archive_type = "tar.gz"
    self.compil_type = "autoconf"
    self.config_options = "--with-salome-kernel=$KERNEL_ROOT_DIR"
    self.parallel_make = "1"

    self.gen_commands += ["libtoolize --force --copy --automake", "aclocal", "autoconf", "automake --force-missing --add-missing --copy"]
    self.gen_commands += ["./build_configure "]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "GUI", "PYQT"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "python_path"]
    if dependency_name == "PYQT":
      dependency_object.depend_of = ["ld_lib_path", "python_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)
    if dependency_name == "PYQT":
      self.executor_software_name += "-pyqt" + misc.transform(version_name)

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return XDATA_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return XDATA_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "YACSGEN"
yacsgen_template = """
# ------ YACSGEN ------------
export YACSGEN_ROOT_DIR="%install_dir"
export PYTHONPATH=$YACSGEN_ROOT_DIR:${PYTHONPATH}
"""
yacsgen_template = misc.PercentTemplate(yacsgen_template)

yacsgen_configuration_template = """
# ------ YACSGEN ------------
YACSGEN_ROOT_DIR="$install_dir"
ADD_TO_PYTHONPATH: %(YACSGEN_ROOT_DIR)s
"""
yacsgen_configuration_template = string.Template(yacsgen_configuration_template)

class YACSGEN(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="tools", repo_name="yacsgen.git")

    self.compil_type = "rsync"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return yacsgen_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return yacsgen_configuration_template.substitute(install_dir=install_dir)

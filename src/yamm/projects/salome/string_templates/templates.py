#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (Euriware)

from yamm.core.base import misc
import string

salome_prerequisite_file_header_template = """
export ROOT_SALOME="%{root_salome}"
export LC_NUMERIC=C

# Uncomment to disable automatic conversion from SIGFPE to exceptions
#export DISABLE_FPE=1

# Uncomment to turn on proper multi-threading support in the OCCT libraries.
# It provides a more stable behavior of OCCT-based applications in multi-threaded environment
#export MMGT_REENTRANT=1

# To be sure to display icons in Gnome, we use the Gnome Configuration tool
gconfTool=`which gconftool-2`
if test -n "$gconfTool"; then
  $gconfTool --type boolean --set /desktop/gnome/interface/buttons_have_icons true
  $gconfTool --type boolean --set /desktop/gnome/interface/menus_have_icons true
fi

"""
salome_prerequisite_file_header_template = misc.PercentTemplate(salome_prerequisite_file_header_template)

salome_prerequisite_file_header_configuration_template = """
ROOT_SALOME="${root_salome}"
LC_NUMERIC=C

# Uncomment to disable automatic conversion from SIGFPE to exceptions
#DISABLE_FPE=1

# Uncomment to turn on proper multi-threading support in the OCCT libraries.
# It provides a more stable behavior of OCCT-based applications in multi-threaded environment
#MMGT_REENTRANT=1

"""
salome_prerequisite_file_header_configuration_template = string.Template(salome_prerequisite_file_header_configuration_template)

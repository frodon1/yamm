# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from builtins import str
from builtins import object

import os
import tempfile
import time
import datetime

from yamm.core.framework.test_suite import TestSuite, ScriptListTestSuite

class SalomeTestSuite(ScriptListTestSuite):

  def __init__(self, name, soft_name="", soft_version="", version = None):
    ScriptListTestSuite.__init__(self, name, soft_name, soft_version, version)
    self.require_appli = True
    self.appli_directory = None
    self.test_groups_result = 0
    self.failed_groups = []
    self.succeeded_groups = []
    self.failed_tests = {}
    self.succeeded_tests = {}

  def nbTests(self):
    self.init_test_env()
    total = 0
    if self.test_list:
      for test_group in self.test_list:
        total += len(test_group.tests)
    return total

  def nbFailedTests(self):
    total = 0
    for tests in list(self.failed_tests.values()):
      total += len(tests)
    return total

  def nbSucceededTests(self):
    total = 0
    for tests in list(self.succeeded_tests.values()):
      total += len(tests)
    return total

  def execute_test(self, command_log, dependency_command, work_directory, logfile, space):
    self.log_files.append(logfile)
    self.test_log = self.create_test_log(command_log)
    self.test_log.start_test(self)
    errors = ""
    exe_tab = []
    # Test application
    if self.appli_directory is None:
      self.test_result = -1
      self.test_log.test_suite_end(exe_tab, errors)
      return "Error in tests, Salome application directory is missing"
    self.test_log.print_debug(" ".join(["Test application directory:", self.appli_directory]))
    if not os.path.isdir(self.appli_directory):
      self.test_result = -1
      self.test_log.test_suite_end(exe_tab, errors)
      return "Error in tests, Salome application directory %s does not exist" % self.appli_directory

    msg = self.init_test_env()
    if msg != "":
      self.test_result = -1
      self.test_log.test_suite_end(exe_tab, errors)
      return msg

    # Create the log directory
    test_suite_log_directory = os.path.join(work_directory, "logs", "test_" + self.name)
    if not os.path.isdir(test_suite_log_directory):
      os.makedirs(test_suite_log_directory)

    # Go to working directory
    current_directory = os.getcwd()
    self.test_log.print_debug("Test suite working directory: %s" % work_directory)
    os.chdir(work_directory)

    # Run the tests
    number_ok_test_group      = 0
    for ind_group_m1, test_group in enumerate(self.test_list):
      ind_group = ind_group_m1 + 1 # Can't use start parameter in enumerate for Python 2.5 compatibility
      t_group = int(time.time())
      group_result, exe_tab, errors = self.run_test_group(exe_tab, errors, test_group, ind_group, self.src_directory,
                                         self.appli_directory, test_suite_log_directory,
                                         self.test_log, space)
      t_group = int(time.time()) - t_group

      self.test_groups_result |= group_result
      if group_result != 0:
        self.failed_groups.append(ind_group)
      else:
        number_ok_test_group += 1
        self.succeeded_groups.append(ind_group)

    os.chdir(current_directory)

    if self.test_groups_result != 0:
      msg = ""
      for failed_group in self.failed_groups:
        msg += "\n\tError in tests in group %s:" % failed_group
        for failed_test in self.failed_tests[failed_group]:
          msg += "\n\t - %s" % failed_test
          pass
      self.test_log.test_suite_end(exe_tab, errors, self.test_log_files)
      return msg

    # Test suite correctly executed
    self.test_log.test_suite_end(exe_tab, errors, self.test_log_files)
    return ""

  def run_test_group(self, exe_tab, errors, test_group, group_index, test_src_path,
                     application_directory, test_suite_log_directory,
                     test_log, space):
    ns_port_log_dir = os.path.join(application_directory, "USERS")
    (fd, ns_port_file_name) = tempfile.mkstemp(prefix = "ns_port_", dir = ns_port_log_dir)
    os.close(fd)
    run_appli_log_file_name = os.path.join(test_suite_log_directory,
                                           "RunAppli-Group-%d.log" % group_index)
    self.log_files.append(run_appli_log_file_name)
    run_appli_command = os.path.join(application_directory, "salome")
    run_appli_command += " --ns-port-log=%s" % os.path.basename(ns_port_file_name)
    if not test_group.use_gui:
      run_appli_command += " -t"
    if test_group.module_list is not None:
      run_appli_command += " -m " + test_group.module_list

    log_group_file_name = os.path.join(test_suite_log_directory,
                                       "SalomeLog-Group-%d.log" % group_index)
    self.log_files.append(log_group_file_name)
    run_appli_command += " -f " + log_group_file_name
    run_appli_command += " > " + run_appli_log_file_name + " 2>&1"
    if test_group.use_gui:
      run_appli_command += " &"
    test_log.print_begin_work("Launch Salome for test group %d with application %s" %
                              (group_index, application_directory), space)
    test_log.print_debug("Running command: " + run_appli_command)
    self.test_result = os.system(run_appli_command)
    if self.test_result != 0:
      test_log.print_end_work_with_warning("Error while launching Salome, see log file %s" %
                                              run_appli_log_file_name)
      return self.test_result, exe_tab, errors

    if test_group.use_gui:
      time.sleep(20)

    # Get naming service port
    try:
      ns_port = ""
      with open(ns_port_file_name, "r") as ns_port_file:
        ns_port = ns_port_file.readline()
      test_log.print_debug("Naming service port is %s" % ns_port)
      if ns_port == "":
        raise Exception("Naming service port file %s is empty" % ns_port_file_name)
      os.remove(ns_port_file_name)
    except Exception as exc:
      self.test_result = -1
      test_log.print_end_work_with_warning("Error while launching Salome: %s" % exc)
      return self.test_result, exe_tab, errors

    test_log.print_end_work()

    test_log.print_debug("Known errors: " + str(self.known_errors))
    for numtest_m1, test in enumerate(test_group.tests):
      numtest = numtest_m1 + 1 # Can't use start parameter in enumerate for Python 2.5 compatibility
      t_test = int(time.time())
      test_log.print_begin_work("Run test %d-%d: %s" % (group_index, numtest, test.script_file), space)
      logfile = os.path.join(test_suite_log_directory,
                             "Test-%d-%d.log" % (group_index, numtest))
      self.test_log_files["%d-%d" % (group_index, numtest)] = logfile
      comment = "`Test %d-%d log file`_" % (group_index, numtest)
      self.log_files.append(logfile)
      test_command = os.path.join(application_directory, "salome")
      test_command += " shell -p %s" % ns_port
      if test.interpreter is not None:
        test_command += " " + test.interpreter
      test_command += " " + os.path.join(test_src_path, test.script_file)
      test_command += " > %s 2>&1" % logfile
      test_log.print_debug("Running command: " + test_command)
      test_result = os.system(test_command)
      t_test = int(time.time()) - t_test
      total_time = str(datetime.timedelta(seconds=t_test))
      state = "|OK|"
      if test_result == 0:
        if group_index in list(self.succeeded_tests.keys()):
          self.succeeded_tests[group_index].append(test.script_file)
        else:
          self.succeeded_tests[group_index] = [(test.script_file)]
        test_log.print_end_work()
      else:
        msg_error = "Result=%d, see log file %s for more details"% (test_result, logfile)
        test_log.print_end_work_with_warning("Test failed, " + msg_error)
        if group_index in list(self.failed_tests.keys()):
          self.failed_tests[group_index].append(test.script_file)
        else:
          self.failed_tests[group_index] = [(test.script_file)]
        if os.path.basename(test.script_file) not in self.known_errors:
          self.test_result |= test_result
          state = "|ERROR|"
        else:
          state = "|KNOWN_ERROR|"
      exe_tab.append((os.path.basename(test.script_file), state, str(total_time), comment))

    test_log.print_begin_work("Kill Salome", space)
    kill_command = os.path.join(application_directory, "salome")
    kill_command += " kill %s" % (ns_port)
    kill_command_log_filename = os.path.join(test_suite_log_directory,
                                             "Kill-Group-%d.log" % group_index)
    self.log_files.append(kill_command_log_filename)
    kill_command += " > " + kill_command_log_filename
    kill_command += " 2>&1"
    test_log.print_debug("Running command: %s" % kill_command)
    kill_result = os.system(kill_command)
    if kill_result != 0:
      test_log.print_end_work_with_warning("Error in Salome kill")
      self.test_result |= kill_result
    else:
      test_log.print_end_work()
    return self.test_result, exe_tab, errors


class DownloadOnlyTestSuite(TestSuite):

  def __init__(self, name, soft_name="", soft_version="", version = None):
    TestSuite.__init__(self, name, soft_name, soft_version, version)

  def execute_test(self, *args, **kwargs):
    os.environ["%s_ROOT_DIR" % self.name] = self.src_directory
    return ""


class SalomeTestGroup(object):

  def __init__(self, module_list = None, use_gui = False, tests = None):
    self.module_list = module_list
    self.use_gui = use_gui
    if tests is None:
      self.tests = []
    else:
      self.tests = tests

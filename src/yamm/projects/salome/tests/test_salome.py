# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 5
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

from builtins import str


if sys.version_info < (2, 7):
    import unittest2 as unittest  # @UnresolvedImport @UnusedImport
else:
    import unittest  # @Reimport


__author__ = 'Gilles DAVID - 2017'


class YammTests(unittest.TestCase):

    def setUp(self):
        import multiprocessing
        from yamm.projects.salome.project import Project
        from yamm.projects.salome.project import get_ftp_server
        from yamm.projects.salome.versions import LAST_STABLE

        self.version = LAST_STABLE
        self.salome_project = Project()
        self.salome_project.set_version(self.version)
        self.salome_project.set_global_option('write_soft_infos', False)
        self.salome_project.set_global_option('parallel_make', str(multiprocessing.cpu_count()))
        self.salome_project.set_global_option('use_system_version', True)
        for category in ('module', 'prerequisite', 'tool'):
            self.salome_project.set_category_option(category,
                                                    "binary_archive_url",
                                                    os.path.join(get_ftp_server(), 'binary-archives', category + 's', "%(name)s.tgz"))

    def test_00_download(self):
        import shutil
        import tempfile
        from yamm.core.base.misc import ping
        tmpdir = tempfile.mkdtemp()
        self.salome_project.set_global_option('top_directory', tmpdir)
        if ping('salome.der.edf.fr') == 0:
            self.salome_project.set_global_option('use_pleiade_mirrors', True)
        self.assertTrue(self.salome_project.download())
        shutil.rmtree(tmpdir)

    def test_00_download_from_archives(self):
        import shutil
        import tempfile
        tmpdir = tempfile.mkdtemp()
        self.salome_project.set_global_option('top_directory', tmpdir)
        for category in ('module', 'prerequisite', 'tool'):
            self.salome_project.set_category_option(category, 'source_type', 'archive')
        self.assertTrue(self.salome_project.download())
        shutil.rmtree(tmpdir)

    def test_01_start_from_archives(self):
        import shutil
        import tempfile
        tmpdir = tempfile.mkdtemp()
        self.salome_project.set_global_option('top_directory', tmpdir)
        for category in ('module', 'prerequisite', 'tool'):
            self.salome_project.set_category_option(category, 'source_type', 'archive')
        self.salome_project.set_global_option('software_minimal_list', ['KERNEL'])
        self.assertTrue(self.salome_project.start())
        shutil.rmtree(tmpdir)

    def test_02_create_installer(self):
        import shutil
        import tempfile
        tmpdir = tempfile.mkdtemp()
        self.salome_project.set_global_option('top_directory', tmpdir)
        self.salome_project.set_global_option('software_minimal_list', ['KERNEL'])
        self.salome_project.set_global_option('default_executor_mode', 'install_from_binary_archive')
        self.assertTrue(self.salome_project.start())
        self.assertTrue(self.salome_project.create_movable_installer())
        shutil.rmtree(tmpdir)

    def test_02_create_installer_with_topdir(self):
        import shutil
        import tempfile
        tmpdir = tempfile.mkdtemp()
        self.salome_project.set_global_option('top_directory', tmpdir)
        self.salome_project.set_global_option('software_minimal_list', ['KERNEL'])
        self.salome_project.set_global_option('default_executor_mode', 'install_from_binary_archive')
        self.assertTrue(self.salome_project.start())
        installer_dir = os.path.join(tmpdir, self.version, 'movable_installer')
        installer_top_dir = os.path.join(installer_dir, 'Salome-%s' % self.version)
        self.assertTrue(self.salome_project.create_movable_installer(installer_topdirectory=installer_top_dir))
        shutil.rmtree(tmpdir)


def main():
    try:
        import xmlrunner
        suite = unittest.TestLoader().loadTestsFromTestCase(YammTests)
        xmlrunner.XMLTestRunner().run(suite)
    except ImportError:
        unittest.main()

if __name__ == '__main__':
    main()

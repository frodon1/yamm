#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

from future import standard_library
standard_library.install_aliases()
import contextlib
import os
import urllib.request, urllib.error, urllib.parse
import urllib.parse

from yamm.core.framework.version import FrameworkVersion
from yamm.projects.salome import project as SalomeProject


class SalomeVersion(FrameworkVersion):
  """
    .. py:currentmodule:: yamm.projects.salome.SalomeVersion

    Base class for SALOME's versions. It adds new
    methods for specific capabilities of SALOME project.

    :param string name: name of the version.
    :param int verbose: verbose level of the version.

  """

  def __init__(self, name="NOT CONFIGURED", verbose=0, flavour = ""):
    self.dev_version_use_network = True
    FrameworkVersion.__init__(self, name, verbose, flavour)

#######
#
# Méthodes qui doivent être surchargées
#
#######

  def get_salome_tag(self):
    """
      This method is mandatory. It should returns the SALOME git tag targeted by
      the version.

      This value is used to get modules sources files and to some files names.
    """
    return None

  def get_salome_version(self):
    """
      This method is optional. It should returns the SALOME version targeted by
      the version. Most of the time, it is identical to :py:meth:`~yamm.projects.salome.version.version.get_salome_tag`.

      This value is used by the installer created by :py:meth:`~yamm.projects.salome.project.project.create_movable_installer`.
    """
    return self.get_salome_tag()

  def get_salome_dot_version(self):
    dotVersion = ""
    if self.get_salome_version_major() is not None:
      dotVersion = "%s"%self.get_salome_version_major()
    if self.get_salome_version_minor() is not None:
      dotVersion += ".%s"%self.get_salome_version_minor()
    if self.get_salome_version_maintenance() is not None:
      maintenance = "%s"%self.get_salome_version_maintenance()
      if maintenance.endswith("dev"):
        maintenance = maintenance[:-3]
      dotVersion += ".%s"%maintenance
    return dotVersion


  def get_salome_version_major(self):
    version = self.get_salome_version()
    if version is None:
      return None
    # Version of Salome is of type: VX_Y_Z or VX_main or VX_Y_BR
    version_split = version.split("_") # size max should be 3
    if version_split[0].isdigit():
      return int(version_split[0])
    elif version_split[0].startswith("V"):
      if version_split[0][1:].isdigit():
        return int(version_split[0][1:])
    else:
      return version_split[0]

  def get_salome_version_minor(self):
    version = self.get_salome_version()
    if version is None:
      return None
    # Version of Salome is of type: VX_Y_Z or VX_main or VX_Y_BR
    version_split = version.split("_") # size max should be 3
    if len(version_split) < 2:
      return None
    if version_split[1].isdigit():
      return int(version_split[1])
    else:
      return version_split[1]

  def get_salome_version_maintenance(self):
    version = self.get_salome_version()
    if version is None:
      return None
    # Version of Salome is of type: VX_Y_Z or VX_main or VX_Y_BR
    version_split = version.split("_") # size max should be 3
    if len(version_split) < 3:
      return None
    if version_split[2].isdigit():
      return int(version_split[2])
    else:
      return version_split[2]

  def add_unstable_software(self, name, ordered_version="", groups=None):
    if groups is None:
      groups = []
    version = ""
    if self.dev_version_use_network:
      try:
        version = "dev_"+self.get_dev_tag(name)
      except urllib.error.URLError as e:
        self.logger.warning("urllib error: {0}".format(e))
        self.dev_version_use_network = False
        version = self.get_dev_version_from_local_file(name)
    else:
      version = self.get_dev_version_from_local_file(name)

    if not version:
      self.logger.error("Impossible to find the dev version for %s"%name)

    self.add_software(name, version, ordered_version, groups)

  def get_dev_version_from_local_file(self, name):
    version = ""
    dev_tag_file_name = self.get_dev_tag_file_name(name)
    if os.path.exists(dev_tag_file_name):
      version = open(dev_tag_file_name).read()
    return version

  def is_universal(self):
    """
      Have to returns True if this version is a correct SALOME universal version.

      :return bool: True if universal.
    """
    return False

  def is_public(self):
    """
      Have to returns True if this version is a public SALOME version.

      :return bool: True if public.
    """
    return False

  def get_dev_tag(self,name):
    server = os.path.join(SalomeProject.get_ftp_server(), 'sources/prerequis/')
    filename = "last_%s_dev_snapshot.txt" % name
    url = urllib.parse.urljoin(server, filename)
    with contextlib.closing(urllib.request.urlopen(url)) as remote_file:
      tag = remote_file.readline().split()[0]
    open(self.get_dev_tag_file_name(name),"w").write(tag)
    return tag

  def get_dev_tag_file_name(self, name):
    filename = "last_"+name+"_dev_snapshot.txt"
    return os.path.join(os.path.dirname(os.path.abspath(__file__)),"versions",filename)

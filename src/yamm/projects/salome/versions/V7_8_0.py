#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
from yamm.projects.salome.version import SalomeVersion
from yamm.projects.salome.test_suite import SalomeTestSuite, DownloadOnlyTestSuite

version_name = "V7_8_0"
class V7_8_0(SalomeVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    SalomeVersion.__init__(self, name, verbose, flavour)
    self.accepted_flavours = ["distrib_edf",
                            "public",
                            "occ-dev",
                            "shaper",
                            "multiphysique",
                            "magnetothermic",
                            "yacs_only_nogui",
                            "oscard_yacs_only_nogui",
                            "only_geom",
                            "only_geom_nogui",
                            "only_smesh",
                            "only_smesh_nogui",
                            "salome_cnd",
                            "salome_cfd",
                            "salome_cfd_neptune",
                            "mpi",
                            "using_software_rendering",
                            ]

  def get_salome_tag(self):
    return "V7_8_0"

  def get_salome_version(self):
    return "V7_8_0"

  def configure_softwares(self):
    # prerequisites
    self.add_software("HDF5", "1.8.14", groups=["base"], min_version='1.8.13')
    self.add_software("PYTHON", "2.7.10", groups=["base"], min_version="2.7.9")
    self.add_software("BOOST", "1.52.0", groups=["base"])
    self.add_software("SWIG", "2.0.8", groups=["base"])
    self.add_software("OMNIORB", "4.1.6", groups=["base"])
    self.add_software("OMNIORBPY", "3.6", groups=["base"])
    self.add_software("CMAKE", "3.0.2", groups=["base"])
    self.add_software("LAPACK", "3.5.0", groups=["base"])
    self.add_software("NUMPY", "1.9.2", groups=["base"], min_version='1.8.2')
    self.add_software("SCIPY", "0.15.1", groups=["base"], min_version='0.14')
    self.add_software("GRAPHVIZ", "2.38.0", groups=["base"])
    self.add_software("DOXYGEN", "1.8.3.1", groups=["base"])
    self.add_software("LIBXML2", "2.9.0", groups=["base"])
    self.add_software("SETUPTOOLS", "0.6c11", groups=["base"])
    self.add_software("PYGMENTS", "2.0.2", groups=["base"], min_version='2.0.1')
    self.add_software("JINJA", "2.7.3", groups=["base"])
    self.add_software("DOCUTILS", "0.12", groups=["base"])
    self.add_software("SPHINX", "1.2.3", groups=["base"])
    self.add_software("TBB", "3.0", groups=["base"])
    self.add_software("GL2PS", "1.3.8", groups=["base"])
    self.add_software("FREETYPE", "2.4.11", groups=["base"])
    # p1 : patch pour un include <string.h> manquant et
    #      pour pouvoir préciser le chemin d'installation
    self.add_software("FREEIMAGE", "3160p1", "3.16.0_occ6", groups=["base"],
                      min_version='3.15.4')
    self.add_software("OCC", "6.9.1", groups=["base"])
    self.add_software("QT", "4.8.4", groups=["base"], min_version='4.8')
    self.add_software("SIP", "4.14.2", groups=["base"])
    self.add_software("PYQT", "4.9.6", groups=["base"])
    # QWT 6.1 minimum car header qwt_legend_label.h nécessaire
    self.add_software("QWT", "6.1.0", groups=["base"], min_version='6.1')

    software_rendering = ""
    if "using_software_rendering" in self.flavour:
      software_rendering = "-software_rendering"
      pass
    self.add_software("PARAVIEW", "git_tag/v5.0.1_EDF%s" % software_rendering, "5.0.1", sha1_version="dbdd7f96", groups=["base"])

    self.add_software("METIS", "5.1.0", groups=["base"])
    self.add_software("SCOTCH", "5.1.11", groups=["base"])
    self.add_software("TOGL", "1.7", groups=["base"])
    self.add_software("TCL", "8.5.13", groups=["base"])
    self.add_software("TK", "8.5.13", groups=["base"])
    self.add_software("TIX", "8.4.3", groups=["base"])
    self.add_software("CGNSLIB", "3.1.3-4", "3.1.3", groups=["base"])
    self.add_software("OPENCV", "2.3.1", groups=["base"])
    self.add_software("CPPUNIT", "1.12.1", groups=["base"])
    self.add_software("PKGCONFIG", "1.1.0", groups=["base"])
    self.add_software("CYTHON", "0.23.2", groups=["base"], min_version='0.21.1')
    self.add_software("H5PY", "2.5.0", groups=["base"], min_version='2.2.1')
    # Meshgems is also distributed in the public version
    # to allow usage of the plugins for those who have a license
    self.add_software("MESHGEMS", "2.1-11", "2.1", groups=["base"])
    self.add_software("GMSH", "2.11.0", "2.11.0", groups=["base"])

    # salome tools
    self.add_software("MEDFICHIER", "3.2.0", groups=["base"])
    self.add_software("LIBBATCH", "git_tag/V2_3_0", groups=["base"])
    self.add_software("NETGEN", "4.9.13", groups=["base"])
    self.add_software("YACSGEN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("HOMARD_TOOL", "11.7", groups=["base"])
    self.add_software("MEDCOUPLING", "git_tag/V7_8_0", groups=["base"])

    # salome modules
    self.add_software("KERNEL", "git_tag/V7_8_0", groups=["base"])
    self.add_software("GUI", "git_tag/V7_8_0", groups=["base"])
    self.add_software("YACS", "git_tag/V7_8_0", groups=["base"])
    self.add_software("GEOM", "git_tag/V7_8_0", groups=["base"])
    self.add_software("MED", "git_tag/V7_8_0", groups=["base"])
    self.add_software("SMESH", "fbt/salome_V7_8_0_public", groups=["base"])
    self.add_software("PARAVIS", "agr/edf_V780rc2_disable_unstable_tests", "V7_8_0", groups=["base"])
    self.add_software("JOBMANAGER", "git_tag/V7_8_0", groups=["base"])
    self.add_software("PRENETGENPLUGIN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("NETGENPLUGIN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("HEXABLOCK", "git_tag/V7_8_0", groups=["base"])
    self.add_software("HEXABLOCKPLUGIN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("BLSURFPLUGIN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("GHS3DPLUGIN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("GHS3DPRLPLUGIN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("HEXOTICPLUGIN", "git_tag/V7_8_0", groups=["base"])
    self.add_software("DSCCODE", "git_tag/V7_8_0", groups=["base"])
    self.add_software("HOMARD", "git_tag/V7_8_0", groups=["base"])
    self.add_software("HYBRIDPLUGIN", "git_tag/V7_8_0")
    self.add_software("GMSHPLUGIN", "git_tag/V7_8_0")

    # example modules
    self.add_software("CALCULATOR", "git_tag/V7_8_0", groups=["example"])
    self.add_software("PYHELLO1", "git_tag/V7_8_0", groups=["example"])
    self.add_software("HELLO1", "git_tag/V7_8_0", groups=["example"])
    self.add_software("LIGHT", "git_tag/V7_8_0", groups=["example"])
    self.add_software("PYLIGHT", "git_tag/V7_8_0", groups=["example"])
    self.add_software("SAMPLES", "git_tag/V7_8_0", groups=["example"])
    self.add_software("ATOMIC", "git_tag/V7_8_0", groups=["example"])
    self.add_software("ATOMGEN", "git_tag/V7_8_0", groups=["example"])
    self.add_software("ATOMSOLV", "git_tag/V7_8_0", groups=["example"])

    # Documentation
    self.add_software("DOCUMENTATION", "git_tag/V7_8_0", groups=["doc"])

    # EDF

    # prerequisites

    # Matplotlib
    self.add_software("MATPLOTLIB", "1.4.3", min_version='1.4.2')

    # Openturns
    self.add_software("R", "2.15.1", groups=["edf_only"], max_version='2.15.2')
    self.add_software("ROTRPACKAGE", "1.4.6", groups=["edf_only"])
    self.add_software("TRALICS", "2.15.0", groups=["edf_only"], min_version='2.14.4')
    self.add_software("LIBXSLT", "1.1.28", groups=["edf_only"])
    self.add_software("MUPARSER", "1.32", groups=["edf_only"])

    # Padder
    self.add_software("CGAL", "4.0", groups=["edf_only"])
    self.add_software("PADDER", "r31", "31", groups=["edf_only"])
    # autres
    self.add_software("PIL", "1.1.7", groups=["edf_only"], max_version='2.7')
    self.add_software("DISTENE", "", groups=["edf_only"])
    self.add_software("GNUPLOTPY", "1.8", groups=["edf_only"])

    # salome tools
    self.add_software("PREOPENTURNS_TOOL", "1.6.1", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_TOOL", "1.6.1", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_SUBSET", "1.2h", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_ADIRSAM", "1.0h", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_DISTFUNC", "0.4h", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_SVM", "0.1h", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_LHS", "1.2", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_PMML", "1.2", groups=["edf_only"], min_version='1.6.1')
    self.add_software("OPENTURNS_DOC", "2015.02.1", groups=["edf_only"], min_version='1.6.1')
    # Les deux EFICAS sont livrés pour Salome V7_8_0
    # L'ancien EFICAS est activé par défaut
    self.add_software("EFICAS_NOUVEAU", "git_tag/V7_8_0_EfiN",)
    self.add_software("EFICASV1", "git_tag/V7_8_0",)
    self.add_software("PARAVISADDONS", "git_tag/V7_8_0_visuHPC",)
    # # Test OTGUI
    self.add_software("OTGUI", "git_tag/V1_0_1", groups=["edf_only"])

    # salome modules
    self.add_software("EFICAS", "git_tag/V7_8_0",)
    self.add_software("OPENTURNS", "git_tag/V7_8_0", groups=["edf_only"])  # git
    # Old openturns module
    self.add_software("OT", "git_tag/V7_8_0", groups=["edf_only"])  # git
    self.add_software("GENERICSOLVER", "git_tag/V7_8_0",)  # git
    self.add_software("ADAO", "git_tag/V7_8_0",)  # git
    self.add_software("PARAMETRIC", "git_tag/V7_8_0")  # git

    self.add_software("EDFDOC", "git_tag/V7_8_0", groups=["edf_only"])  # git

    # Other
    self.add_software("XDATA", "0.9.11")

  def update_configuration_with_flavour(self):
    SalomeVersion.update_configuration_with_flavour(self)

    ######
    # Variantes utiles pour la distribution de la plateforme SALOME de base
    ######

    minimal_soft_list = []

    if "_nogui" in self.flavour:
      self.remove_software("GUI", remove_deps=True)

    if "only_geom" in self.flavour:
      minimal_soft_list.append("GEOM")

    if "only_smesh" in self.flavour:
      minimal_soft_list.append("SMESH")

    # Profil pour la distribution de SALOME à  EDF :
    # on ne distribue pas les modules d'exemple
    if self.flavour == "distrib_edf":
      self.remove_software_groups(["example"], remove_deps=True)
      # Presque tous les modules sont dans les dépendances du
      # module documentation on enlève donc pas ses dépendances
      self.remove_software("DOCUMENTATION", remove_deps=False)

    # Profil pour la diffusion publique :
    # on enlève tous les modules et prérequis spécifiques à  EDF
    if "public" in self.flavour:
      self.remove_software_groups(["edf_only"], remove_deps=True)
      self.remove_software_groups(["example"], remove_deps=True)
      # Matplotlib doit être présent dans la version publique
      # bien que les modules EDF ne soient pas présents
      self.add_software("MATPLOTLIB", "1.4.3",)
      # Presque tous les modules sont dans les dépendances du
      # module documentation on enlève donc pas ses dépendances
      self.remove_software("DOCUMENTATION", remove_deps=False)

    # Profil pour la compilation avec la tête de branche d'OCCT
    # utile dans les phases de test avant les sorties de version
    if "occ-dev" in self.flavour:
      self.add_unstable_software("OCC")

    # Profil pour compiler Salome avec MPI
    if self.flavour == "mpi":
      self.add_software("OPENMPI", "")

# Profil pour tester le nouveau GEOM
    if "shaper" in self.flavour:
      self.add_software("OCC", "dev-NewGEOM_2.5.0", "6.9.1", groups=["base"])
      self.add_software("SOLVESPACE", "2.1")
      self.add_software("EIGEN", "3.2.7")
      self.add_software("PLANEGCS", "0.16")
      self.add_software("SHAPER", "git_tag/V_2.2.0")
      minimal_soft_list.append("SHAPER")
      minimal_soft_list.extend(["BLSURFPLUGIN", "GHS3DPLUGIN"])
      minimal_soft_list.extend(["HEXOTICPLUGIN", "NETGENPLUGIN"])

    ######
    # Variantes pour les différentes communautés scientifiques
    ######

    # # SALOME multiphysique

    if self.flavour == "multiphysique":
      # On ne distribue pas les modules d'exemple
      self.remove_software_groups(["example"], remove_deps=True)

      # prerequisites
      self.add_software("PYQWT", "5.2.0")
      self.add_software("GRACE", "5.1.21")
      self.add_software("LESSTIF", "0.95.2")
      # Le module MED est maintenant compatible
      # avec Metis 4 et 5, on garde Metis 4 dans la version
      # de base pour Salome-meca et on prend Metis 5
      # pour Syrthes et Saturne
      self.add_software("METIS", "5.0.2")
      self.add_software("CODE_SYRTHES", "git_tag/4.3.1")

      # modules
      self.add_software("SYRTHES", "git_tag/V7_6_0_SYR4_3_1")
      self.add_software("CFDSTUDY", "tags/V4_0_2")

    # # SALOME_CFD
    if "salome_cfd" in self.flavour:
      # On ne distribue pas les modules d'exemple
      self.remove_software_groups(["example"], remove_deps=True)

      # prerequisites
      self.add_software("PYQWT", "5.2.0")  # pour syrthes
      self.add_software("GRACE", "5.1.21")  # pour syrthes
      self.add_software("LESSTIF", "0.95.2")  # pour syrthes
      # Le module MED est maintenant compatible
      # avec Metis 4 et 5, on garde Metis 4 dans la version
      # de base pour Salome-meca et on prend Metis 5
      # pour Syrthes et Saturne
      self.add_software("METIS", "5.0.2")
      self.add_software("CODE_SYRTHES", "master")
      self.add_software("LIBCCMIO", "2.6.19")
      # modules
      self.add_software("SYRTHES", "master")
      self.add_software("CFDSTUDY", "trunk")
      self.add_software("SATURNE_DBG", "trunk")
      # Tools
      minimal_soft_list.append("ADAO")
      minimal_soft_list.append("BLSURFPLUGIN")
      minimal_soft_list.append("GENERICSOLVER")
      minimal_soft_list.append("GHS3DPLUGIN")
      minimal_soft_list.append("GRACE")
      minimal_soft_list.append("HEXABLOCKPLUGIN")
      minimal_soft_list.append("HEXOTICPLUGIN")
      minimal_soft_list.append("HOMARD")
      minimal_soft_list.append("JOBMANAGER")
      minimal_soft_list.append("LESSTIF")
      minimal_soft_list.append("NETGENPLUGIN")
      minimal_soft_list.append("OPENTURNS")
      minimal_soft_list.append("OPENTURNS_ADIRSAM")
      minimal_soft_list.append("OPENTURNS_DISTFUNC")
      minimal_soft_list.append("OPENTURNS_SUBSET")
      minimal_soft_list.append("OPENTURNS_SVM")
      minimal_soft_list.append("PARAMETRIC")
      minimal_soft_list.append("PARAVISADDONS")
      minimal_soft_list.append("CODE_SYRTHES")
      minimal_soft_list.append("SYRTHES")
      minimal_soft_list.append("CFDSTUDY")
      minimal_soft_list.append("SATURNE_DBG")
      

    if "neptune" in self.flavour:
      self.add_software("NEPTUNE_DBG", "trunk")
      self.add_software("NEPTUNE", "trunk")
      # Tools
      self.add_software("EOS", "tags/eos-1.4.0_salome", "1.4.0_salome")
      minimal_soft_list.append("NEPTUNE_DBG")
      minimal_soft_list.append("NEPTUNE")

    # # OSCARD

    if "yacs_only" in self.flavour:
      minimal_soft_list.append("YACS")
      if "oscard" in self.flavour:
        self.add_software("OSCARD", "4.4")
        minimal_soft_list.append("OSCARD")

      self.remove_softwares(["QSCINTILLA", "GUI"], remove_deps=True)
      boost_version = self.get_software_version("BOOST")
      boost_ordered_version = self.get_software_ordered_version("BOOST")
      if not boost_version.endswith("_salome"):
        self.set_software_version("BOOST", boost_version + "_salome", boost_ordered_version)

    # # Salome magnetothermic

    if "magnetothermic" in self.flavour :
      # tools
      self.add_software("CODE_SYRTHES", "git_tag/4.3.1")
      self.add_software("CODE_CARMEL3D", "V_242_test")
      self.add_software("CODE_INTERPOL", "master")
      # modules
      self.add_software("SYRTHES", "git_tag/V7_6_0_SYR4_3_1")
      self.add_software("CARMEL3D", "master")
      self.add_software("INTERPOL", "master")
      self.add_software("MAGNETOTHERMIC", "master")
      minimal_soft_list.append("CARMEL3D")
      minimal_soft_list.append("CODE_CARMEL3D")
      minimal_soft_list.append("SYRTHES")
      minimal_soft_list.append("CODE_SYRTHES")
      minimal_soft_list.append("INTERPOL")
      minimal_soft_list.append("CODE_INTERPOL")
      minimal_soft_list.append("MAGNETOTHERMIC")
      minimal_soft_list.append("YACS")
      minimal_soft_list.append("EFICAS")
      minimal_soft_list.append("BLSURFPLUGIN")
      minimal_soft_list.append("GHS3DPLUGIN")
      minimal_soft_list.append("HEXABLOCKPLUGIN")
      minimal_soft_list.append("HEXOTICPLUGIN")
      minimal_soft_list.append("HOMARD")
      minimal_soft_list.append("JOBMANAGER")
      minimal_soft_list.append("NETGENPLUGIN")
      minimal_soft_list.append("OPENTURNS")
      minimal_soft_list.append("OPENTURNS_ADIRSAM")
      minimal_soft_list.append("OPENTURNS_DISTFUNC")
      minimal_soft_list.append("OPENTURNS_SUBSET")
      minimal_soft_list.append("OPENTURNS_SVM")
      minimal_soft_list.append("PARAVIS")

    # # Salome cnd

    if "salome_cnd" in self.flavour :

      # prerequisites for CND
      self.add_software("PYCRYPTO", "2.6.1")
      self.add_software("ECDSA", "0.11")
      self.add_software("PARAMIKO", "1.13.0")
      # modules
      self.add_software("CARMELCND"                     , "IHM_SMX_LP")
      # tools
      self.add_software("SCRIPTS_MAILLAGES_CND_CARMEL3D", "IHM_SMX_LP")
      self.add_software("CODE_CARMEL3D", "V_242_test")
      minimal_soft_list.append("EFICAS_NOUVEAU")
      minimal_soft_list.append("CARMELCND")
      minimal_soft_list.append("CODE_CARMEL3D")
      minimal_soft_list.append("SCRIPTS_MAILLAGES_CND_CARMEL3D")
      minimal_soft_list.append("EFICAS")
      minimal_soft_list.append("BLSURFPLUGIN")
      minimal_soft_list.append("GHS3DPLUGIN")
      minimal_soft_list.append("HEXABLOCKPLUGIN")
      minimal_soft_list.append("HEXOTICPLUGIN")
      minimal_soft_list.append("HOMARD")
      minimal_soft_list.append("JOBMANAGER")
      minimal_soft_list.append("NETGENPLUGIN")
      minimal_soft_list.append("OPENTURNS")
      minimal_soft_list.append("OPENTURNS_ADIRSAM")
      minimal_soft_list.append("OPENTURNS_DISTFUNC")
      minimal_soft_list.append("OPENTURNS_SUBSET")
      minimal_soft_list.append("OPENTURNS_SVM")
      minimal_soft_list.append("PARAVIS")
      minimal_soft_list.append("YACS")

    if minimal_soft_list:
      self.set_software_minimal_list(minimal_soft_list)

  def configure_tests(self, project_options):
    autotests = DownloadOnlyTestSuite(name="SALOME_AUTOTESTS",
                                  soft_name=self.name,
                                  version="")
    autotests.src_directory = os.path.join(project_options.get_global_option("version_directory"), "test_src", self.name + "_" + autotests.name)
    if autotests.version != "":
      autotests.src_directory += "_" + autotests.version
    autotests.work_directory = autotests.src_directory
    autotests.post_install = True
    autotests.post_move = True
    autotests.remote_system_type = "cvs"
    autotests.repository_name = "autotests-v2"
    autotests.root_repository = ":pserver:anonymous@nepal:2401/nepal/cvs/autotests"

    overhead = SalomeTestSuite(name="salome-autotests-overhead",
                                  soft_name=self.name,
                                  version="master")
    overhead.insource = False
    overhead.src_directory = os.path.join(project_options.get_global_option("version_directory"), "test_src", self.name + "_" + overhead.name + "_" + overhead.version)
    overhead.work_directory = os.path.join(project_options.get_global_option("version_directory"), "test_workdir", self.name + "_" + overhead.name + "_" + overhead.version)
    overhead.post_install = True
    overhead.post_move = True
    overhead.remote_system_type = "git"
    overhead.repository_name = "salome-autotests-overhead.git"
    overhead.root_repository = "git://cli70rw.der.edf.fr/salome-autotests"
    overhead.test_list_file = os.path.join(overhead.src_directory, "test_list.py")
    overhead.require_appli = True
    overhead.known_errors = ["test_non_regression_6_6_0.py"]
    self.test_suite_list = [autotests, overhead]

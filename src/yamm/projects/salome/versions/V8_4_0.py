#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015, 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)

import os
from yamm.projects.salome.version import SalomeVersion
from yamm.projects.salome.test_suite import SalomeTestSuite, DownloadOnlyTestSuite

version_name = "V8_4_0"
class V8_4_0(SalomeVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    SalomeVersion.__init__(self, name, verbose, flavour)
    self.accepted_flavours = ["distrib_edf",
                            "public",
                            "specific_gcc",
                            "occ-dev",
                            "shaper",
                            "yacs_only_nogui",
                            "oscard_yacs_only_nogui",
                            "only_geom",
                            "only_geom_nogui",
                            "only_smesh",
                            "only_smesh_nogui",
                            "salome_cfd",
                            "salome_cfd_neptune",
                            "salome_cfd_catalyst",
                            "mpi",
                            "using_software_rendering",
                            "specific_gcc_salome_cfd",
                            "specific_gcc_salome_cfd_neptune",
                            "non_regression_tests",
                            "non_regression_tests_confidential"
                            ]

  def get_salome_tag(self):
    return "V8_4_0"

  def get_salome_version(self):
    return "V8_4_0"

  def configure_softwares(self):
    # prerequisites
    self.add_software("HDF5", "1.8.14", groups=["base"], min_version='1.8.13')
    self.add_software("PYTHON", "2.7.10", groups=["base"], min_version="2.7.9")
    self.add_software("BOOST", "1.52.0", groups=["base"])
    self.add_software("SWIG", "2.0.8", groups=["base"])
    self.add_software("OMNIORB", "4.1.6", groups=["base"])
    self.add_software("OMNIORBPY", "3.6", groups=["base"])
    self.add_software("CMAKE", "3.6.2", groups=["base"], min_version='3.3')
    self.add_software("LAPACK", "3.5.0", groups=["base"])
    self.add_software("NUMPY", "1.9.2", groups=["base"], min_version='1.8.2')
    self.add_software("NUMPYDOC", "0.6.0", groups=["base"], min_version='0.6.0')
    self.add_software("SCIPY", "0.15.1", groups=["base"], min_version='0.14')
    self.add_software("GRAPHVIZ", "2.38.0", groups=["base"])
    self.add_software("DOXYGEN", "1.8.3.1", groups=["base"])
    self.add_software("LIBXML2", "2.9.0", groups=["base"])
    self.add_software("LIBXSLT", "1.1.28", groups=["base"])
    self.add_software("LXML", "3.4.0", groups=["base"])
    self.add_software("SETUPTOOLS", "0.6c11", groups=["base"])
    self.add_software("SETUPTOOLS_OTW", "5.5.1", groups=["base"])
    self.add_software("PYGMENTS", "2.0.2", groups=["base"], min_version='2.0.1')
    self.add_software("JINJA", "2.7.3", groups=["base"])
    self.add_software("DOCUTILS", "0.12", groups=["base"])
    self.add_software("SPHINX", "1.2.3", groups=["base"])
    self.add_software("TBB", "4.2.4", groups=["base"], min_version='4.2')
    self.add_software("GL2PS", "1.3.9-svn-20160620", "1.3.9", groups=["base"], min_version='1.3.10')
    self.add_software("FREETYPE", "2.4.11", groups=["base"])
    # p1 : patch pour un include <string.h> manquant et
    #      pour pouvoir préciser le chemin d'installation
    self.add_software("FREEIMAGE", "3160p1", "3.16.0", groups=["base"],
                      min_version='3.15.4')
    self.add_software("OCC", "7.2.0", groups=["base"])
    self.add_software("QT", "5.9.1", groups=["base"])
    self.add_software("SIP", "4.19.3", groups=["base"])
    self.add_software("PYQT", "5.9.0", groups=["base"])
    self.add_software("QWT", "6.1.2", groups=["base"])

    software_rendering = ""
    if "using_software_rendering" in self.flavour:
      software_rendering = "-software_rendering"
      pass
    self.add_software("PARAVIEW", "git_tag/5.4.0_EDF%s" % software_rendering, "5.4", sha1_version='463c0633', groups=["base"])
    self.add_software("METIS", "5.1.0", groups=["base"])
    self.add_software("SCOTCH", "5.1.11", groups=["base"])
    self.add_software("TOGL", "1.7", groups=["base"])
    self.add_software("TCL", "8.5.13", groups=["base"])
    self.add_software("TK", "8.5.13", groups=["base"])
    self.add_software("TIX", "8.4.3", groups=["base"])
    self.add_software("CGNSLIB", "3.3.1", groups=["base"])
    self.add_software("OPENCV", "2.3.1", groups=["base"])
    self.add_software("CPPUNIT", "1.13.2", groups=["base"])
    self.add_software("PKGCONFIG", "1.1.0", groups=["base"])
    self.add_software("CYTHON", "0.23.2", groups=["base"], min_version='0.21.1')
    self.add_software("H5PY", "2.5.0", groups=["base"], min_version='2.2.1')
    # Meshgems is also distributed in the public version
    # to allow usage of the plugins for those who have a license
    self.add_software("MESHGEMS", "2.5-7", "2.5", groups=["base"])
    self.add_software("ZCRACKS", "2.8.7", "2.8.7", groups=["base"])
    self.add_software("GMSH", "3.0.5", groups=["base"], min_version='3.0.5')

    # salome tools
    self.add_software("MEDFICHIER", "3.3.1", groups=["base"], min_version='3.3')
    self.add_software("LIBBATCH", "git_tag/V2_3_2", groups=["base"])
    self.add_software("NETGEN", "5.3.1", groups=["base"])
    self.add_software("YACSGEN", "git_tag/925bebee", groups=["base"])
    self.add_software("HOMARD_TOOL", "1110r330b4", "11.10", groups=["base"])
    self.add_software("MEDCOUPLING", "git_tag/V8_4_0", '8.4.0', groups=["base"])

    # salome modules
    self.add_software("KERNEL", "git_tag/V8_4_0", groups=["base"])
    self.add_software("GUI", "git_tag/V8_4_0", groups=["base"])
    self.add_software("YACS", "git_tag/V8_4_0", groups=["base"])
    self.add_software("GEOM", "git_tag/V8_4_0", groups=["base"])
    self.add_software("MED", "git_tag/V8_4_0", groups=["base"])
    self.add_software("SMESH", "git_tag/V8_4_0", groups=["base"])
    self.add_software("PARAVIS", "git_tag/V8_4_0", groups=["base"])
    self.add_software("JOBMANAGER", "git_tag/V8_4_0", groups=["base"])
    self.add_software("PRENETGENPLUGIN", "git_tag/V8_4_0", groups=["base"])
    self.add_software("NETGENPLUGIN", "git_tag/V8_4_0", groups=["base"])
    self.add_software("HEXABLOCK", "git_tag/V8_4_0", groups=["base"])
    self.add_software("HEXABLOCKPLUGIN", "git_tag/V8_4_0", groups=["base"])
    self.add_software("BLSURFPLUGIN", "git_tag/V8_4_0", groups=["base"])
    self.add_software("GHS3DPLUGIN", "git_tag/V8_4_0", groups=["base"])
    # MAJ du plugin a faire (CEA) :
    # self.add_software("GHS3DPRLPLUGIN", "git_tag/V8_4_0", groups=["base"])
    self.add_software("HEXOTICPLUGIN", "git_tag/V8_4_0", groups=["base"])
    self.add_software("DSCCODE", "git_tag/V8_4_0", groups=["base"])
    self.add_software("HOMARD", "git_tag/V8_4_0", groups=["base"])
    self.add_software("SAMPLES", "git_tag/V8_4_0", groups=["base"])
    self.add_software("CONFIGURATION", "git_tag/V8_4_0", groups=["base"])
    self.add_software("HYBRIDPLUGIN", "git_tag/V8_4_0", groups=["base"])
    self.add_software("GMSHPLUGIN", "git_tag/V8_4_0", groups=["base"])

    # example modules
    self.add_software("CALCULATOR", "git_tag/V8_4_0", groups=["example"])
    self.add_software("PYHELLO1", "git_tag/V8_4_0", groups=["example"])
    self.add_software("HELLO1", "git_tag/V8_4_0", groups=["example"])
    self.add_software("LIGHT", "git_tag/V8_4_0", groups=["example"])
    self.add_software("PYLIGHT", "git_tag/V8_4_0", groups=["example"])
    self.add_software("ATOMIC", "git_tag/V8_4_0", groups=["example"])
    self.add_software("ATOMGEN", "git_tag/V8_4_0", groups=["example"])
    self.add_software("ATOMSOLV", "git_tag/V8_4_0", groups=["example"])

    # Documentation
    self.add_software("DOCUMENTATION", "git_tag/V8_4_0", groups=["doc"])

    # EDF

    # prerequisites

    # Matplotlib
    self.add_software("MATPLOTLIB", "1.4.3", min_version='1.4.2')

    # Openturns
    self.add_software("R", "2.15.1", groups=["edf_only"], max_version='2.15.2')
    self.add_software("RPY", "1.0.3", groups=["edf_only"])
    self.add_software("ROTRPACKAGE", "1.4.6", groups=["edf_only"])
    self.add_software("TRALICS", "2.15.0", groups=["edf_only"], min_version='2.14.4')
    self.add_software("MUPARSER", "1.32", groups=["edf_only"])

    # Padder
    self.add_software("CGAL", "4.0", groups=["edf_only"])
    self.add_software("PADDER", "tags/20170530", groups=["edf_only"])
    # autres
    self.add_software("PIL", "1.1.7", groups=["edf_only"], max_version='2.6.1-2')
    self.add_software("DISTENE", "", groups=["edf_only"])
    self.add_software("GNUPLOTPY", "1.8", groups=["edf_only"])


    # salome tools
    self.add_software("OPENTURNS_TOOL", "1.9.1", groups=["edf_only"], min_version='1.9')
    self.add_software("NLOPT", "2.4.2", groups=["edf_only"], min_version='2.4.2')
    self.add_software("OPENTURNS_SVM", "0.3", groups=["edf_only"], min_version='0.3')
    self.add_software("OPENTURNS_PMML", "1.4", groups=["edf_only"], min_version='1.4')
    self.add_software("OPENTURNS_LM", "0.4", groups=["edf_only"], min_version='0.4')
    self.add_software("OPENTURNS_ROBOPT", "0.2", groups=["edf_only"], min_version='0.2')
    self.add_software("OPENTURNS_FFTW", "0.4", groups=["edf_only"], min_version='0.4')
    self.add_software("OPENTURNS_MORRIS", "0.2", groups=["edf_only"], min_version='0.2')
    self.add_software("OPENTURNS_MIXMOD", "0.5", groups=["edf_only"], min_version='0.5')
    self.add_software("OPENTURNS_WRAPY", "0.7_EDF", "0.7", groups=["edf_only"], min_version='0.6')
    self.add_software("OPENTURNS_DOC", "2015.02.1", '1.8', groups=["edf_only"], min_version='1.8')
    self.add_software("EFICAS_NOUVEAU", "git_tag/V8_4_0-rc2",)
    self.add_software("PARAVISADDONS", "git_tag/9b1bb7f6",)
        # # Test OTGUI
    self.add_software("OTGUI", "git_tag/v3.0", groups=["edf_only"])

    # salome modules
    self.add_software("EFICAS", "git_tag/17b2334",)
    self.add_software("OPENTURNS", "git_tag/d3a959ac", groups=["edf_only"]) # git
    self.add_software("GENERICSOLVER", "git_tag/1c8802c5",) # git
    self.add_software("ADAO", "git_tag/31c4a558",) # git

    # en attendant le portage Qt5 d'Eficas
    self.add_software("EDFDOC", "git_tag/d7630ba9", groups=["edf_only"]) # git

    # Other
    # en attendant le portage Qt5 d'Eficas
    ###self.add_software("XDATA", "0.9.11")

    # Tests de non-regression
    if "non_regression_tests" in self.flavour:
        self.add_software("NON_REGRESSION_TESTS", "git_tag/V8_4_0rc4", groups=["edf_only"])
    if "non_regression_tests_confidential" in self.flavour:
        self.add_software("NON_REGRESSION_TESTS_CONFIDENTIAL", "git_tag/V8_4_0rc4", groups=["edf_only"])

  def update_configuration_with_flavour(self):
    SalomeVersion.update_configuration_with_flavour(self)

    ######
    # Variantes utiles pour la distribution de la plateforme SALOME de base
    ######

    minimal_soft_list = []

    if "_nogui" in self.flavour:
      self.remove_software("GUI", remove_deps=True)

    if "only_geom" in self.flavour:
      minimal_soft_list.append("GEOM")

    if "only_smesh" in self.flavour:
      minimal_soft_list.append("SMESH")

    if "specific_gcc" in self.flavour:
      self.add_software("M4",               "1.4.17",           groups=["base"])
      self.add_software("AUTOCONF",         "2.69",             groups=["base"])
      self.add_software("AUTOMAKE",         "1.15",             groups=["base"])
      self.add_software("LIBTOOL",          "2.4.5",            groups=["base"])
      self.add_software("GMP",              "5.1.3",            groups=["base"])
      self.add_software("MPFR",             "3.1.2",            groups=["base"])
      self.add_software("MPC",              "1.0.2",            groups=["base"])
      self.add_software("CLOOG",            "0.18.1",           groups=["base"])
      self.add_software("BINUTILS",         "2.21",             groups=["base"])
      self.add_software("GCC",              "4.9.2",            groups=["base"])

    # Profil pour la distribution de SALOME à  EDF :
    # on ne distribue pas les modules d'exemple
    if "distrib_edf" in self.flavour:
      self.remove_software_groups(["example"], remove_deps=True)
      # Presque tous les modules sont dans les dépendances du
      # module documentation on enlève donc pas ses dépendances
      self.remove_software("DOCUMENTATION", remove_deps=False)

    # Profil pour la diffusion publique :
    # on enlève tous les modules et prérequis spécifiques à  EDF
    if "public" in self.flavour:
      self.remove_software_groups(["edf_only"], remove_deps=True)
      self.remove_software_groups(["example"], remove_deps=True)
      # Matplotlib doit être présent dans la version publique
      # bien que les modules EDF ne soient pas présents
      self.add_software("MATPLOTLIB", "1.4.3",)
      # Presque tous les modules sont dans les dépendances du
      # module documentation on enlève donc pas ses dépendances
      self.remove_software("DOCUMENTATION", remove_deps=False)

    # Profil pour la compilation avec la tête de branche d'OCCT
    # utile dans les phases de test avant les sorties de version
    if "occ-dev" in self.flavour:
      self.add_unstable_software("OCC")

    # Profil pour compiler Salome avec MPI
    if "mpi" in self.flavour:
      self.add_software("ENVHPCOPENMPI", "")
      self.add_software("MPI4PY", "1.3.1")

# Profil pour tester le nouveau GEOM
    if "shaper" in self.flavour:
      self.add_software("OCC", "dev-NewGEOM_2.0.0", "6.9.1", groups=["base"])
      self.add_software("SOLVESPACE", "2.1")
      self.add_software("EIGEN", "3.2.7")
      self.add_software("PLANEGCS", "0.16")
      self.add_software("GUI", "git_tag/SHAPER_2.2.0", groups=["base"])
      self.add_software("SHAPER", "git_tag/V_2.2.0")
      minimal_soft_list.append("SHAPER")
      minimal_soft_list.extend(["BLSURFPLUGIN","GHS3DPLUGIN"])
      minimal_soft_list.extend(["HEXOTICPLUGIN","NETGENPLUGIN"])

    ######
    # Variantes pour les différentes communautés scientifiques
    ######

    # # SALOME_CFD
    if "salome_cfd" in self.flavour:
      # On ne distribue pas les modules d'exemple
      self.remove_software_groups(["example"], remove_deps=True)

      # prerequisites
      self.add_software("OPENMPI", "1.6.5")
      self.add_software("GRACE", "5.1.21")  # pour syrthes
      self.add_software("LESSTIF", "0.95.2")  # pour syrthes
      self.add_software("FUTURE","0.16.0") #  pour syrthes QT5 et python2 - 3
      # Le module MED est maintenant compatible
      # avec Metis 4 et 5, on garde Metis 4 dans la version
      # de base pour Salome-meca et on prend Metis 5
      # pour Syrthes et Saturne
      self.add_software("METIS", "5.0.2")
      self.add_software("CODE_SYRTHES", "master")
      self.add_software("LIBCCMIO", "2.6.19")
      self.add_software("COOLPROP", "6.1.0")

      # modules
      self.add_software("SYRTHES", "master")
      self.add_software("CFDSTUDY", "branches/Version5_1")
      self.add_software("SATURNE_DBG", "branches/Version5_1")

    if "neptune" in self.flavour:
      self.add_software("NEPTUNE_DBG", "trunk")
      self.add_software("NEPTUNE", "trunk")
      # Tools
      self.add_software("EOS", "tags/eos-1.4.0_salome", "1.4.0_salome")

    if "catalyst" in self.flavour:
      self.add_software("OSMESA", "13.0.3")
      software_rendering = "-software_rendering"
      self.add_software("PARAVIEW_OSMESA", "git_tag/v5.4.0_dev_edf%s" % software_rendering, "5.4", sha1_version="9b8e8a02")

    # # OSCARD

    if "yacs_only" in self.flavour:
      minimal_soft_list.append("YACS")
      if "oscard" in self.flavour:
        self.add_software("OSCARD", "4.4")
        minimal_soft_list.append("OSCARD")

      self.remove_softwares(["QSCINTILLA", "GUI"], remove_deps=True)
      boost_version = self.get_software_version("BOOST")
      boost_ordered_version = self.get_software_ordered_version("BOOST")
      if not boost_version.endswith("_salome"):
        self.set_software_version("BOOST", boost_version + "_salome", boost_ordered_version)

    if minimal_soft_list:
      self.set_software_minimal_list(minimal_soft_list)

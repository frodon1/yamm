#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015, 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.core.base import misc
from yamm.projects.salome.versions.DEV import DEV

version_name = "V9_DEV"


class V9_DEV(DEV):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        DEV.__init__(self, name, verbose, flavour)

    def get_salome_tag(self):
        return "V9_DEV"

    def get_salome_version(self):
        return "V9_DEV"

    def configure_softwares(self):
        DEV.configure_softwares(self)

        self.add_debian_depends(['net-tools', 'texlive-fonts-recommended',
                                 'texlive-lang-french', 'texlive-latex-extra'])

        self.add_software("PYTHONSTARTUP", '')
        self.set_software_version("PYTHON", "3.5.2", min_version="3.4")
        self.set_software_version("SWIG", "3.0.12", min_version='3.0.10')
        self.set_software_version("OMNIORB", "4.2.1-2")
        self.set_software_version("OMNIORBPY", "4.2.1-2")
        self.set_software_version("SETUPTOOLS", "21.0.0", min_version='5.5')
        self.set_software_version("GNUPLOTPY", "1.8-3k")

        self.set_software_version("CGNSLIB", "3.3.1")
        self.set_software_version("HDF5", "1.10.1", min_version='1.10')
        self.set_software_version("MEDFICHIER", "4.0.0alpha3", "4.0")

        self.add_software("QT", "5.6.1-1", "5.6.1", groups=["base"])
        self.add_software("SIP", "4.18", groups=["base"])
        self.add_software("PYQT", "5.6.0", groups=["base"])

        # salome tools
        self.set_software_version("LIBBATCH", "V9_dev")
        self.set_software_version("YACSGEN", "V9_dev")
        self.set_software_version("MEDCOUPLING", 'master', "9.2")

        # salome modules
        self.set_software_version("KERNEL", "V9_dev")
        self.set_software_version("GUI", "V9_dev")
        self.set_software_version("YACS", "V9_dev")
        self.set_software_version("GEOM", "V9_dev")
        self.set_software_version("MED", "V9_dev")
        self.set_software_version("SMESH", "V9_dev")
        self.set_software_version("PARAVIS", "V9_dev")
        self.set_software_version("PARAVISADDONS", "git_tag/de9d566")
        self.set_software_version("PRENETGENPLUGIN", "uhz/V9_dev")
        self.set_software_version("NETGENPLUGIN", "uhz/V9_dev")
        self.set_software_version("HEXABLOCK", "V9_dev")
        self.set_software_version("HEXABLOCKPLUGIN", "V9_dev")
        self.set_software_version("BLSURFPLUGIN", "V9_dev")
        self.set_software_version("GHS3DPLUGIN", "V9_dev")
        # MAJ du plugin a faire (CEA) :
        # self.set_software_version("GHS3DPRLPLUGIN", "V9_dev")
        self.set_software_version("HEXOTICPLUGIN", "V9_dev")
        self.set_software_version("DSCCODE", "V9_dev")
        self.set_software_version("HOMARD", "V9_dev")
        self.set_software_version("SAMPLES", "V9_dev")
        self.set_software_version("CONFIGURATION", "V9_dev")
        self.set_software_version("JOBMANAGER", "V9_dev")
        self.set_software_version("HYBRIDPLUGIN", "V9_dev")
        # portage a faire
        # self.add_software("GMSHPLUGIN", "master", groups=["base"])

        # example modules
        self.set_software_version("CALCULATOR", "V9_dev")
        self.set_software_version("PYHELLO1", "V9_dev")
        self.set_software_version("HELLO1", "V9_dev")
        self.set_software_version("PYLIGHT", "V9_dev")
        self.set_software_version("LIGHT", "V9_dev")
        self.set_software_version("ATOMIC", "V9_dev")
        self.set_software_version("ATOMGEN", "V9_dev")
        self.set_software_version("ATOMSOLV", "V9_dev")

        # salome modules
        self.set_software_version("GENERICSOLVER", "V9_dev")
        self.set_software_version("DOCUMENTATION", "V9_dev")
        self.set_software_version("EFICAS", "V9_dev")

        self.set_software_version("EDFDOC", "gdd/python3_dev")

        # Python 3 issues
        self.remove_software('GMSHPLUGIN')
        self.remove_software_groups(["openturns"], True)
        self.remove_software('OTGUI')
        self.remove_software('PIL')
        self.remove_software('H5PY')
        self.remove_software('RPY', remove_deps=True)
        self.add_software("PILLOW", "3.4.2", min_version='2.6.1-2')
        if int(misc.get_calibre_version()) < 10:
            self.add_software("GDB", "7.12")
            self.add_software("SIX", "1.10.0", min_version='1.8.0')

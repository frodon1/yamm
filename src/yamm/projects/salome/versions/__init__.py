# Development
versions_list = ["DEV", "DEV_universal"]
versions_list += ["DEV_OscardYacsOnlyNoGui"]

# V7.x.0
versions_list += ["V7_7_1", "V7_8_0"]
versions_list += ["V7_dev"]

# V7_dev
versions_list += ["V7_dev"]

# V8.x.x
versions_list += ["V8_0_BR", "V8_0_0"]
versions_list += ["V8_1_0"]
versions_list += ["V8_2_0"]
versions_list += ["V8_3_0"]
versions_list += ["V8_4_0"]

# V9.x.x
versions_list += ["V9_DEV"]

# test
versions_list += ["test_version"]

LAST_STABLE = 'V8_4_0'


# =============================================================================
# Obsoletes versions
# Files were removed from directory.
# They can be retrieved in the git history.

# V5.1.5
old_versions_list = ["V5_1_5", "V5_1_5_CFD"]

# V6.3
old_versions_list += ["V6_3_0", "V6_3_0_Calibre7"]
old_versions_list += ["V6_3_1", "V6_3_1_universal", "V6_3_1_Calibre7", "V6_3_1_formation"]

# V6.4
old_versions_list += ["V6_4_0", "V6_4_0_universal_edf", "V6_4_0_universal_public", "V6_4_0_Calibre7"]
old_versions_list += ["V6_4_0_vishnu_universal_edf", "V6_4_0_vishnu_Calibre7", "V6_4_0_CFD"]
old_versions_list += ["V6_4_0_MULTIPHYSIQUE"]
old_versions_list += ["V6_4_0_formation", "V6_4_0_formation_Calibre7"]

# V6.5
old_versions_list += ["V6_5_0", "V6_5_0_universal_edf", "V6_5_0_universal_public", "V6_5_0_Calibre7"]
old_versions_list += ["V6_5_0_prl", "V6_5_0_prl_Calibre7"]
old_versions_list += ["V6_5_0_MULTIPHYSIQUE", "V6_5_0_Calibre7_MULTIPHYSIQUE"]
old_versions_list += ["V6_5_0p1", "V6_5_0p1_universal_edf", "V6_5_0p1_universal_public"]
old_versions_list += ["V6_5_0p1_Calibre7", "V6_5_0p1_openturns_universal_public"]
old_versions_list += ["V6_5_0_formation", "V6_5_0_formation_Calibre7"]

# V6_6
old_versions_list += ["V6_6_0"]
old_versions_list += ["V6_6_0_MULTIPHYSIQUE"]
old_versions_list += ["V6_6_0_YacsOnlyNoGui_Calibre7", "V6_6_0_OscardYacsOnlyNoGui_Calibre7"]

# V7.x.0
old_versions_list += ["V7_1_0"]
old_versions_list += ["V7_2_0"]
old_versions_list += ["V7_3_0", "V7_3_1"]
old_versions_list += ["V7_4_0", "V7_4_1"]
old_versions_list += ["V7_5_0", "V7_5_1"]
old_versions_list += ["V7_6_BR", "V7_6_0"]
old_versions_list += ["V7_7_BR", "V7_7_0"]

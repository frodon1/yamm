#  Copyright (C) 2012, 2014 - EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from builtins import object
import os

from yamm.core.engine.tasks.generic_task import copy

class CustomizeAppliTask(object):
  """
  This task is called to customize SALOME application for Salome-Coeur (splash screen, etc.)
  """
  def __init__(self, appli_directory):
    self.appli_directory = appli_directory

  def prepare_work(self, software_config, space, command_log):
    self.command_log = command_log
    self.space = space

  def execute(self, dependency_command, executor, current_software):
    self.command_log.print_begin_work("Customize application", space = self.space)

    salome_coeur_res_dir = os.path.join(self.appli_directory, "share", "salome", "resources",
                                        "module_cocagne", "salome-coeur")

    # Copy SalomeApp file
    copy(os.path.join(salome_coeur_res_dir, "SalomeAppGlobal.xml"),
         os.path.join(self.appli_directory, "SalomeApp.xml"))

    # Copy CatalogResources file
    copy(os.path.join(salome_coeur_res_dir, "CatalogResources.xml"), self.appli_directory)

    self.command_log.print_end_work()

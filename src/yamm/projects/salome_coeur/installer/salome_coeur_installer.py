#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Thomas LAUFFENBURGER (EDF R&D)
from __future__ import with_statement

import os

from yamm.core.framework.installer import FrameworkInstaller
from yamm.projects.salome_coeur.installer.strings_template import SalomeCoeurInstallerStringsTemplate
from yamm.projects.salome.installer.salome_installer import SalomeInstaller


class SalomeCoeurInstaller(SalomeInstaller):
    """
    Installer class for salome coeur
    NB: Universal installer doesn't work for now because of CBLAS that is taken from the system by Cocagne
    """
    def __init__(self, *args, **kwargs):

        framework_dict = {}
        framework_keys = ['delete', 'runnable', 'nosubdir']
        for k in framework_keys:
            framework_dict[k] = kwargs[k]
            del kwargs[k]
        if args:
            name, logger, topdirectory, softwares, executor = args
            FrameworkInstaller.__init__(self, name, logger, topdirectory, softwares,
                                        executor, **framework_dict)
        else:
            framework_keys = ['name', 'logger', 'topdirectory', 'softwares', 'executor']
            for k in framework_keys:
                framework_dict[k] = kwargs[k]
                del kwargs[k]
            FrameworkInstaller.__init__(self, **framework_dict)

        if not kwargs.get("default_icon_path"):
            for software in kwargs["softwares"]:
                if software.name == "MODULE_COCAGNE":
                    software_install_name = software.name + "_" + misc.transform(software.version)
                    installdir = os.path.join(kwargs["topdirectory"], "modules", software_install_name)
                    kwargs["default_icon_path"] = os.path.join(installdir, "share", "salome", "resources", "module_cocagne", "salome-coeur", "salome-Coeur3.png")

        # Update default values
        kwargs.update(self.get_defaults())

        # To create the installer, we want to use the archives that were generated just before, not those on the remote server
        options = self.executor.options
        options.set_category_option("module", "binary_archive_url",
                                    os.path.join(options.get_category_option("module", "binary_archive_topdir"),
                                                 "%(name)s.tgz"))
        options.set_category_option("prerequisite", "binary_archive_url",
                                    os.path.join(options.get_category_option("prerequisite", "binary_archive_topdir"),
                                                 "%(name)s.tgz"))
        options.set_category_option("tool", "binary_archive_url",
                                    os.path.join(options.get_category_option("tool", "binary_archive_topdir"),
                                                 "%(name)s.tgz"))

        self.init_installer(**kwargs)

    def init_installer(self, **kwargs):
        SalomeInstaller.init_installer(self, **kwargs)
        self.post_install_file_name = os.path.join(self.topdirectory, "coeur_post_install.py")
        self.prerequisites_file = os.path.join(self.topdirectory, "salome_coeur.sh")
        self.context_file = os.path.join(self.topdirectory, "salome_coeur.cfg")
        self.modules_file = self.prerequisites_file

    def get_defaults(self):
        defaults = {}
        defaults["default_name"] = "SalomeCoeur"
        defaults["default_contact"] = "https://forge.pleiade.edf.fr/projects/danae"
        defaults["default_exec"] = "coeur_launcher"
        defaults["default_install_dir"] = "$HOME/salome_coeur"
        defaults["default_appli_dir"] = '$HOME/salome_coeur/COEUR'
        defaults["stringsTemplate"] = SalomeCoeurInstallerStringsTemplate()
        defaults["salome_version"] = self.executor.version_object.name
        cmd = '(cd $APPLIDIR && ln -s bin/salome/appliskel/salome coeur_launcher)\n'
        if not self.nosubdir:
            cmd += '(. salome_coeur.sh &&\n'
            cmd += 'ln -s $INTERFACE_MOTEUR_ROOT_DIR &&\n'
            cmd += 'ln -s $FORMULAIRE_ROOT_DIR &&\n'
            cmd += 'ln -s $HOME_T2C &&\n'
            cmd += 'ln -s $COCAGNE_ROOT_DIR &&\n'
            cmd += 'ln -s $HOME_C3THER_POUR_EMC2 &&\n'
            cmd += 'ln -s $HOME_THYC'
            cmd += ')'
        defaults["final_custom_command"] = cmd
        return defaults

    def get_decompress_language(self):
        return 'LANGUAGE=""\n'

    def get_catalog_cmd(self):
        return ''

    def get_hpc_visu_servers_cmd(self):
        return ''

    def get_create_appli_language(self):
        return ''

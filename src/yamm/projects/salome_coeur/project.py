#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Thomas LAUFFENBURGER (EDF R&D)
from __future__ import with_statement

from __future__ import print_function
import os
import sys
import platform
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base import misc
from yamm.core.framework.project import yamm_command

from yamm.core.base.misc import VerboseLevels
from yamm.projects.emc2.project   import Project       as EMC2Project
from yamm.projects.salome_coeur.software  import SalomeCoeurSoftware
from yamm.projects.salome_coeur.installer.salome_coeur_installer import SalomeCoeurInstaller

from yamm.projects.salome_coeur.softwares  import tools         as default_tools
from yamm.projects.salome_coeur.softwares  import modules       as default_modules
from yamm.projects.salome_coeur.softwares  import prerequisites as default_prerequisites
from yamm.projects.salome_coeur            import versions      as default_versions

from yamm.projects.salome.appli_tasks import CreateAppliTask


def get_ftp_server():
  return os.path.join(misc.get_ftp_server(), 'F3C', 'EEM')


class Project(EMC2Project):

  def __init__(self, log_name="SALOME_COEUR", verbose_level=VerboseLevels.INFO):
    EMC2Project.__init__(self, log_name, verbose_level)
    self.init_salome_coeur_vars()
    self.define_salome_coeur_options()
    pass

  def init_catalog(self):
    EMC2Project.init_catalog(self)

    # Suppression des versions non définies dans SALOME_COEUR
    for version_name in self.catalog.versions.keys():
      if version_name not in default_versions.versions_list:
        del self.catalog.versions[version_name]

    self.catalog.name = "SALOME_COEUR"
    # Import des tools SALOME COEUR
    for name in default_tools.modules_list:
      self.add_software_in_catalog(name, "yamm.projects.salome_coeur.softwares.tools")
    # Import des modules SALOME COEUR
    for name in default_modules.modules_list:
      self.add_software_in_catalog(name, "yamm.projects.salome_coeur.softwares.modules")
    # Import des prerequis SALOME COEUR
    for module_name in default_prerequisites.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.salome_coeur.softwares.prerequisites")
    # Import des versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.salome_coeur.versions")
      pass
    self.logger.debug("End init catalog")
    pass

  def init_salome_coeur_vars(self):
    EMC2Project.init_emc2_vars(self)
    pass

  def define_salome_coeur_options(self):
    EMC2Project.define_emc2_options(self)
    pass

#######
#
# Méthodes d'initialisation externes
#
#######

  def set_default_options_values(self, current_command_options):
    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "salome_coeur"))
      pass

    if not current_command_options.get_global_option("version_directory"):
      current_command_options.set_global_option("version_directory", os.path.join(current_command_options.get_global_option("top_directory"), self.version))

    if not current_command_options.get_global_option("salome_prerequisites_env_file"):
      current_command_options.set_global_option("salome_prerequisites_env_file", "%s/salome_coeur.sh" % (current_command_options.get_global_option("version_directory")))
    if not current_command_options.get_global_option("salome_modules_env_file"):
      current_command_options.set_global_option("salome_modules_env_file", "%s/salome_coeur.sh" % (current_command_options.get_global_option("version_directory")))
    if not current_command_options.get_global_option("salome_context_file"):
      current_command_options.set_global_option("salome_context_file", "%s/salome_coeur.cfg" % (current_command_options.get_global_option("version_directory")))

    # Salome project default values
    EMC2Project.set_default_options_values(self, current_command_options)
    pass

#######
#
# Méthodes pour tester la configuration
#
#######

  def check_specific_configuration(self, current_command_options):
    EMC2Project.check_specific_configuration(self, current_command_options)
    pass

#######
#
# Print methods
#
#######

  def print_general_configuration(self, current_command_options):
    EMC2Project.print_general_configuration(self, current_command_options)
    self.logger.info("SALOME COEUR Version          : %s" % self.version)
    pass

#######
#
# Méthodes internes pour les commandes
#
#######


  def create_env_files(self, delete=True, installer=False, nosubdir=False):
    self.create_context_file(delete, installer, nosubdir)
    self.create_prerequisites_file(delete, installer, nosubdir)
    self.create_modules_file(False)

  def update_configuration(self):
    #VERIFY
    # Change archive remote address for Salome Coeur private softwares
    # Those softwares inherit from SalomeCoeurSoftware
    for soft in self.catalog.softwares:
      software_class = self.catalog.get_software_class(soft)
      if issubclass(software_class, SalomeCoeurSoftware) and \
          self.current_command_options.get_option(soft, "archive_remote_address") == "":
        self.current_command_options.set_software_option(soft, "archive_remote_address", get_ftp_server())
    EMC2Project.update_configuration(self)


#######
#
# Lanceur
#
########

  def _create_appli(self, appli_topdirectory="", appli_name="COEUR",
                    appli_catalogresources="", generates_env_files=True):
      ret = EMC2Project._create_appli(self, appli_topdirectory, appli_name,
                                        appli_catalogresources, generates_env_files)
      if not ret:
          return ret

      coeur_launcher = os.path.join(self.appli_directory, "coeur_launcher")
      if not os.path.exists(coeur_launcher):
          os.symlink(os.path.join(self.appli_directory, "bin", "salome",
                                  "appliskel", "salome"),
                     coeur_launcher)
      return ret

  @yamm_command("Creation of an Salome Coeur application")
  def create_appli(self, appli_topdirectory="", appli_name="COEUR",
                   appli_catalogresources="", generates_env_files=True):
    ret = self._create_appli(appli_topdirectory, appli_name,
                             appli_catalogresources, generates_env_files)
    #if ret:
      # Final step: collect sha1s of softwares
      #ret = self._make(executor_mode="collect_sha1")
    return ret

  def _get_appli_launcher(self):
      return 'coeur_launcher'

  @yamm_command("Launch of Salome Coeur application")
  def run_appli(self, appli_topdirectory="", appli_name="COEUR", appli_options=""):
    return EMC2Project._run_appli(self, appli_topdirectory, appli_name, appli_options)

  def get_version_custom_tasks(self): # Pour eviter la creation de 2 applis avec 2 noms differents
    tasks = EMC2Project.get_version_custom_tasks(self)
    if tasks:
        appli_topdirectory = self.current_command_options.get_global_option("version_directory")
        self.appli_directory = os.path.join(appli_topdirectory, "COEUR")
        tasks[0].appli_directory = self.appli_directory
        for test_suite in self.current_command_version_object.test_suite_list:
            test_suite.appli_directory = self.appli_directory
    return tasks

#######
#
# Installeur
#
#######

  def get_installer_class(self):
    return SalomeCoeurInstaller

  def get_installer_name(self):
    formatted_version = self.version.replace("V", "v").replace("_", ".")
    return "SalomeCoeur-{0}".format(formatted_version)


if __name__ == "__main__":

  print( "Testing salome_coeur project class")
  pro = Project(VerboseLevels.DEBUG)
  pro.set_version("DEV")
  pro.print_configuration()
  pro.nothing()
#  pro.download()
#  pro.start()

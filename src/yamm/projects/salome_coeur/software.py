#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

from yamm.core.base import misc
from yamm.projects.emc2.software import EMC2Software


class SalomeCoeurSoftware(EMC2Software):
    '''
    This class defines the generic interface of a Salome Coeur software
    '''

    def __init__(self, name="NOT CONFIGURED", version="NOT CONFIGURED",
                 verbose=misc.VerboseLevels.INFO, **kwargs):
        '''
        Constructor
        '''
        SalomeSoftware.__init__(self, name, version, verbose, **kwargs)

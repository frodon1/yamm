#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware

software_name = "MODULE_COCAGNE"
class MODULE_COCAGNE(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 101

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    # No proxy for MODULE_COCAGNE
    if self.project_options.get_option(self.name, "proxy_server"):
      self.project_options.set_software_option(self.name, "proxy_server", "")

    self.remote_type = "git"
    self.repository_name = "salome-coeur"
    self.root_repository = "https://git.forge.pleiade.edf.fr/git"
    self.tag = self.version

    self.compil_type = "specific"
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),
    self.specific_build_command = \
        "cd $CURRENT_SOFTWARE_BUILD_DIR && " \
        "python $CURRENT_SOFTWARE_SRC_DIR/yacsgen.py && " \
        "cp -Rp MODULE_COCAGNE_SRC/* $CURRENT_SOFTWARE_BUILD_DIR && "
    if misc.split_version(self.ordered_version) >= [1, 2]:
      self.specific_build_command += \
        "cmake -DCMAKE_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR && "
    else:
      self.specific_build_command += \
        "./autogen.sh && " \
        "./configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR && "
    self.specific_build_command += "make -j%s" % parallel_make

    self.specific_install_command = \
        "cd $CURRENT_SOFTWARE_BUILD_DIR && " \
        "make -j%s install" % parallel_make

  def init_dependency_list(self):
    # We must compile MODULE_COCAGNE after PARAVIS because the macros are installed
    # in PARAVIS install directory
    self.software_dependency_dict['build'] = ["YACSGEN","CONFIGURATION","SPHINX"]
    self.software_dependency_dict['exec'] = ["PARAVIS"]

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

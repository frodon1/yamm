#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.emc2.versions.DEV import DEV as EMC2_DEV

version_name = "DEV"
version_value = "master"


class DEV(EMC2_DEV):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        EMC2_DEV.__init__(self, name, verbose, flavour)
        self.accepted_flavours = ["SALOME_COEUR_PORTHOS"]
        
        self.minimal_soft_list =  ["YACS", "H5PY", "SIC", "THYCOX", "COUPLEUR_T2C", "INTERFACE_MOTEUR"]
        self.minimal_soft_list += ["PARAVIS", "ADAO", "JOBMANAGER", "OPENTURNS"]
        self.minimal_soft_list += ["MODULE_COCAGNE", "SALOMON"]


    def configure_softwares(self):
        EMC2_DEV.configure_softwares(self)

        self.add_software("MODULE_COCAGNE", "V1.4_DEV")
        self.add_software("SALOMON", "V1.5_DEV")
        
        self.set_software_minimal_list(self.minimal_soft_list)

        self.remove_software("SMESH", remove_deps=True) # SMESH has been added as a dependency of PARAVIS, so we need to remove it explicitly
        pass

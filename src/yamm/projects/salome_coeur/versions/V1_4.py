#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.projects.emc2.versions.V1_0_2 import V1_0_2 as EMC2_V1_0_2

version_name = "V1_4"
version_value = "1.4"


class V1_4(EMC2_V1_0_2):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        EMC2_V1_0_2.__init__(self, name, verbose, flavour)
        self.accepted_flavours = ["SCOEUR","SCOEUR_PORTHOS"]
        
        self.minimal_soft_list =  ["YACS", "H5PY", "INTERFACE_MOTEUR"]
        self.minimal_soft_list += ["PARAVIS", "ADAO", "JOBMANAGER", "OPENTURNS"]
        self.minimal_soft_list += ["MODULE_COCAGNE", "SALOMON"]


    def configure_softwares(self):
        EMC2_V1_0_2.configure_softwares(self)

        self.add_software("MODULE_COCAGNE", "V1.4_DEV")
        self.add_software("SALOMON", "V1.5_DEV")
        
        self.set_software_minimal_list(self.minimal_soft_list)

        self.remove_software("SMESH", remove_deps=True) # SMESH has been added as a dependency of PARAVIS, so we need to remove it explicitly
        self.remove_software("THYC", remove_deps=True)
        pass

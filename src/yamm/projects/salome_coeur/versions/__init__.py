# Stable versions
versions_list = []

# Development version
versions_list += ["DEV", "V1_4"]

LAST_STABLE = 'DEV'


# Obsoletes versions
# Files were removed from directory.
# They can be retrieved in the git history.

old_versions_list = ["V1_0", "V1_1", "V1_2"]

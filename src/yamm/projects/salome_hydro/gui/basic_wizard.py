#  Copyright (C) 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

import os

from yamm.core.framework.gui.basic_wizard import FrameworkBasicWizard
from yamm.core.framework.gui.basic_wizard import FrameworkBasicIntro
from yamm.core.framework.gui.basic_wizard import FrameworkBasicOptions

from yamm.projects.salome_hydro.project import Project

class BasicWizard(FrameworkBasicWizard):

  def __init__(self, parent, title="Create a SALOME_HYDRO project", project = Project()):
    FrameworkBasicWizard.__init__(self, parent, title, project)
    self.setWindowTitle("Create a SALOME_HYDRO project")

  def get_yamm_project(self):
    return Project()

  def add_pages(self):
    self.addPage(FrameworkBasicIntro(self, self.get_yamm_project()))
    self.addPage(BasicOptions(self, self.get_yamm_project()))

  def end(self, result):
    self.result = result
    if result:
      self.record_intro_page_fields()
      self.record_options_page_fields()

class BasicOptions(FrameworkBasicOptions):
  def __init__(self, parent, project):
    FrameworkBasicOptions.__init__(self, parent, project)

  def setDefaultValues(self):
    self.current_topDir = os.path.join(self.home_dir, "salome_hydro")
    FrameworkBasicOptions.setDefaultValues(self)
    idx = self.version_comboBox.findText("DEV")
    if idx != -1:
      self.version_comboBox.setCurrentIndex(idx)
    self.opt_par_spinBox.setValue(4)

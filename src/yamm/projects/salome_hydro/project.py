#!/usr/bin/env python
#  Copyright (C) 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from __future__ import print_function
import os
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels
from yamm.projects.salome                 import project       as salome_project
from yamm.projects.salome.project         import Project       as SalomeProject
from yamm.projects.salome.installer.salome_installer import SalomeInstaller

from yamm.projects.salome_hydro.softwares  import tools         as default_tools
from yamm.projects.salome_hydro.softwares  import modules       as default_modules
from yamm.projects.salome_hydro            import versions      as default_versions

class Project(SalomeProject):

  def __init__(self, log_name="SALOME_HYDRO", verbose_level=VerboseLevels.INFO):
    SalomeProject.__init__(self, log_name, verbose_level)

  def init_catalog(self):
    SalomeProject.init_catalog(self)
    self.catalog.name = "SALOME_HYDRO"
    # Import modules
    for module_name in default_modules.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.salome_hydro.softwares.modules")
    # Import tools
    for module_name in default_tools.modules_list:
      self.add_software_in_catalog(module_name, "yamm.projects.salome_hydro.softwares.tools")

    # Remove versions that have been added by Salome project
    self.catalog.versions = {}
    # Import Salome-Hydro versions
    for version_name in default_versions.versions_list:
      self.add_version_in_catalog(version_name, "yamm.projects.salome_hydro.versions")
    self.logger.debug("End init catalog")

  def set_default_options_values(self, current_command_options):
    version_class = self.catalog.get_version_class(self.version)
    salome_version = version_class().get_salome_vanilla_version()

    # Set Specific top directory
    if not current_command_options.get_global_option("top_directory"):
      current_command_options.set_global_option("top_directory", os.path.join(os.environ["HOME"], "salome_hydro"))

    # Setting this option to False saves some time when building project
    if not current_command_options.get_global_option("write_soft_infos"):
      current_command_options.set_global_option("write_soft_infos", False)

    # Default compilation options: install all from binary archives except Salome-Hydro specific softwares
    if not current_command_options.get_global_option("default_executor_mode"):
      current_command_options.set_global_option("default_executor_mode", "install_from_binary_archive")

    baseurl = os.path.join(salome_project.get_ftp_server(), 
                           'binary-archives','SALOME7', 'Calibre-7-gcc484')
    if not current_command_options.is_category_option("prerequisite", "binary_archive_url"):
      current_command_options.set_category_option("prerequisite", "binary_archive_url",
                                                  os.path.join(baseurl, 'prerequisites', '%(name)s.tgz'))
    if not current_command_options.is_category_option("tool", "binary_archive_url"):
      current_command_options.set_category_option("tool", "binary_archive_url",
                                                  os.path.join(baseurl, salome_version, 'tools', '%(name)s.tgz'))
    if not current_command_options.is_category_option("module", "binary_archive_url"):
      current_command_options.set_category_option("module", "binary_archive_url",
                                                  os.path.join(baseurl, salome_version, 'modules', '%(name)s.tgz'))

    # Softwares specific to Salome-Hydro (that are not in basic Salome or that we use in non-standard versions)
    specific_software_list = ["MASCARET", "TELEMAC", "HYDROSOLVER", "HYDRO", "GUI", "GEOM", "SMESH", "ADAO"]
    for soft in specific_software_list:
      if not current_command_options.is_software_option(soft, "default_executor_mode"):
        current_command_options.set_software_option(soft, "default_executor_mode", "keep_if_no_diff")

    # Disable GEOM doc generation that fails for now (09/07/2015, sha1 : 44f924fa670420fc1bd7deefe2c6c3d595bc552c)
    if current_command_options.is_software_option("GEOM", "software_additional_config_options"):
      additional_config_options = current_command_options.get_option("GEOM", "software_additional_config_options")
    else:
      additional_config_options = ""
    #current_command_options.set_software_option("GEOM", "software_additional_config_options", " -DSALOME_BUILD_DOC=OFF " + additional_config_options)

    # Create appli by default
    if not current_command_options.get_global_option("create_appli"):
      current_command_options.set_global_option("create_appli", True)

    # Salome project default values
    SalomeProject.set_default_options_values(self, current_command_options)

  def get_installer_class(self):
    return SalomeHydroInstaller

  def get_default_installer_name(self, distrib = None, universal = False):
    return "Salome-Hydro-{0}-Calibre-7".format(self.version)


class SalomeHydroInstaller(SalomeInstaller):
  """
  Installer class for Salome-Hydro
  NB: Universal installer doesn't work for now because we need a specific
  version of the compiler to compile user fortran files.
  """
  def __init__(self, *args, **kwargs):
    if not kwargs.get("default_name"):
      kwargs["default_name"] = "Salome-Hydro"
    if not kwargs.get("default_install_dir"):
      kwargs["default_install_dir"] = "$HOME/salome-hydro"

    # To create the installer, we want to use the archives that were generated just before, not those on the remote server
    options = kwargs["executor"].options
    options.set_category_option("module", "binary_archive_url", os.path.join(options.get_category_option("module", "binary_archive_topdir"), "%(name)s.tgz"))
    options.set_category_option("prerequisite", "binary_archive_url", os.path.join(options.get_category_option("prerequisite", "binary_archive_topdir"), "%(name)s.tgz"))
    options.set_category_option("tool", "binary_archive_url", os.path.join(options.get_category_option("tool", "binary_archive_topdir"), "%(name)s.tgz"))

    SalomeInstaller.__init__(self, *args, **kwargs)


if __name__ == "__main__":

  print("Testing salome_hydro project class")
  pro = Project()
  pro.set_version("DEV")
  pro.print_configuration()
  pro.options.set_global_option("write_soft_infos", False)
  pro.nothing()
#  pro.download()
#  pro.start()

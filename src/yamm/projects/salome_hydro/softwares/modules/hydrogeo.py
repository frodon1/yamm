# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  Author : André RIBES (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware

software_name = "HYDRO"
class HYDRO(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 2

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "git"
    self.tag = self.version
    self.configure_occ_software(path="modules", repo_name="hydro.git")

    self.compil_type = "cmake"

  def is_classic_branch(self, version):
    return version.startswith("V")

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "SETUPTOOLS", "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["GUI", "GEOM", "OCC"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "OCC":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "OCC":
      self.config_options += " -DCAS_ROOT_DIR=$OCC_INSTALL_DIR"

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

  def module_support_cmake_compilation(self):
    return True

  def has_dev_docs(self):
    return True

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return_string = "\n"
    return_string += "#------- Hydro ------\n"
    return_string += "export HYDRO_DIR=" + install_dir + "\n"
    return_string += "export HYDRORes=${HYDRO_DIR}/share/salome/resources/hydro\n"

    return return_string

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return_string = "\n"
    return_string += "#------- Hydro ------\n"
    return_string += "HYDRO_DIR=" + install_dir + "\n"
    return_string += "HYDRORes=%(HYDRO_DIR)s/share/salome/resources/hydro\n"

    return return_string

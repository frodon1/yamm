#  Copyright (C) 2012 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

import os
import string
from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware

software_name = "HYDROSOLVER"
class HYDROSOLVER(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.root_repository_template = string.Template('https://$server/$pub')
    self.module_gui_sort = 30

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    #self.remote_type     = "svn"
    #self.repository_name = "salome-hydro.module-hydrosolver"
    #self.root_repository = "https://svn.forge.pleiade.edf.fr/svn"
    #self.tag             = self.version
    self.remote_type       = "git"
    self.tag               = self.version
    self.configure_occ_software(path="modules", repo_name="hydrosolver.git")

    if "alpha" not in self.version and misc.split_version(self.ordered_version) >= [0, 2]:
      self.compil_type = "cmake"
    else:
      self.compil_type = "autoconf"
      self.gen_commands += ["./clean_configure", "./build_configure "]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE", "SETUPTOOLS", "CONFIGURATION"]
    self.software_dependency_dict['exec'] = ["KERNEL", "GUI", "MASCARET",
                                             "TELEMAC", "NUMPY", "MEDFICHIER", "HYDRO"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "MASCARET":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "TELEMAC":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "MEDFICHIER":
      self.config_options += " -DMEDFILE_ROOT_DIR=${MEDFICHIER_INSTALL_DIR}"

  def get_type(self):
    return "module"

  def has_salome_module_gui(self):
    return True

#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "MASCARET"
mascaret_template = """
#------ Mascaret ------
export %ld_library_path="%install_dir/lib":$%ld_library_path
"""
mascaret_template = misc.PercentTemplate(mascaret_template)

mascaret_configuration_template = """
#------ Mascaret ------
ADD_TO_$ld_library_path: "$install_dir/lib"
"""
mascaret_configuration_template = string.Template(mascaret_configuration_template)

class MASCARET(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.software_source_type = "archive"
    
  def init_variables(self):
    self.archive_file_name    = "mascaret-" + self.version + ".tar.gz"
    self.archive_type         = "tar.gz"

    self.compil_type = "specific"
    parallel_make = self.project_options.get_option(self.name, "parallel_make"),

    # Mascaret needs gcc >= 4.6 to compile. If the gcc installed in the system is < 4.6,
    # it is necessary to install a newer gcc somewhere and to set environment variable
    # MASCARET_GCC_ROOT_DIR to the root of the new gcc installation.
    self.specific_build_command = \
        'if test -n "$MASCARET_GCC_ROOT_DIR"; then' \
        "  export PATH=$MASCARET_GCC_ROOT_DIR/bin:$PATH;" \
        "  export LD_LIBRARY_PATH=$MASCARET_GCC_ROOT_DIR/lib64:$LD_LIBRARY_PATH;" \
        "fi && " \
        "rm -f MakefileSO Fox/FoX-config &&" \
        "chmod u+x Fox/configure &&" \
        "export FC=gfortran &&" \
        "export FCFLAGS=-fPIC &&" \
        'cat Makefile - > MakefileSO <<"EOF"\n' \
        "FFLAGS+= -fPIC\n" \
        "CFLAGS+= -fPIC\n" \
        "libmascaret.so: FOX_LIB $(OBJ)\n" \
        "\t$(LD) $(OBJ) -shared -Wl,-soname,$@ -o $@ -L./Fox/objs/lib -lFoX_sax -lFoX_common -lFoX_utils -lFoX_fsys \n" \
        "EOF\n" \
        "make -f MakefileSO -j %s libmascaret.so" % parallel_make
    self.specific_install_command = \
        "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib $CURRENT_SOFTWARE_INSTALL_DIR/include && " \
        "cp libmascaret.so $CURRENT_SOFTWARE_INSTALL_DIR/lib && " \
        "cp API/cpp/apimascaret.h $CURRENT_SOFTWARE_INSTALL_DIR/include && " \
        'if test -n "$MASCARET_GCC_ROOT_DIR"; then' \
        "  cp $MASCARET_GCC_ROOT_DIR/lib64/libquadmath.so.* $MASCARET_GCC_ROOT_DIR/lib64/libgfortran.so.* $CURRENT_SOFTWARE_INSTALL_DIR/lib;" \
        "fi"

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mascaret_template.substitute(install_dir = install_dir,
                                        ld_library_path = misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mascaret_configuration_template.substitute(install_dir = install_dir,
                                        ld_library_path = misc.get_ld_library_path())

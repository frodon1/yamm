# -*- coding: utf-8 -*-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud Barate (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "TELEMAC"
telemac_template = """
#------ Telemac ------
export TELEMAC_DIR="%install_dir"
export HOMETEL="%install_dir"
export SYSTELCFG=${TELEMAC_DIR}/configs/%systelcfg
export USETELCFG=%usetelcfg
export GFORTRAN_CONVERT_UNIT='big_endian'
export %ld_library_path=${TELEMAC_DIR}/builds/salome/wrap_api/lib:${%ld_library_path}
export PYTHONPATH=${TELEMAC_DIR}/builds/salome/wrap_api/lib:${PYTHONPATH}
export PYTHONPATH=${TELEMAC_DIR}/scripts/python27:${PYTHONPATH}
export PATH="${TELEMAC_DIR}/scripts/python27":$PATH
"""
telemac_template = misc.PercentTemplate(telemac_template)

telemac_configuration_template = """
#------ Telemac ------
TELEMAC_DIR="$install_dir"
HOMETEL="$install_dir"
SYSTELCFG=%(TELEMAC_DIR)s/configs/$systelcfg
USETELCFG=$usetelcfg
GFORTRAN_CONVERT_UNIT='big_endian'
ADD_TO_$ld_library_path: %(TELEMAC_DIR)s/builds/salome/wrap_api/lib
ADD_TO_PYTHONPATH: %(TELEMAC_DIR)s/builds/salome/wrap_api/lib
ADD_TO_PYTHONPATH: %(TELEMAC_DIR)s/scripts/python27
ADD_TO_PATH: "%(TELEMAC_DIR)s/scripts/python27"
"""
telemac_configuration_template = string.Template(telemac_configuration_template)

class TELEMAC(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "telemac-" + self.get_version_for_archive() + ".tar.gz"
    self.archive_type = "tar.gz"

    self.remote_type     = "svn"

    self.configure_remote_software_pleiade_svn("prerequisites-salome.opentelemac",
                                               external_server="http://svn.opentelemac.org",
                                               external_path="svn",
                                               external_repo_name="opentelemac",
                                               )

    self.tag             = self.version

    data_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

    self.compil_type = "specific"
    self.make_repeat = 0
    self.systelcfg = "systel.salome.cfg"
    self.usetelcfg = "salome"

    self.specific_build_command = \
        "$CURRENT_SOFTWARE_SRC_DIR/scripts/python27/compileTELEMAC.py --clean" \
        " -c %s -f $CURRENT_SOFTWARE_SRC_DIR/configs/%s && " \
        "$CURRENT_SOFTWARE_SRC_DIR/scripts/python27/compileAPI.py" \
        " -c %s -f $CURRENT_SOFTWARE_SRC_DIR/configs/%s -r $CURRENT_SOFTWARE_SRC_DIR" \
        % (self.usetelcfg,self.systelcfg,self.usetelcfg,self.systelcfg)
    self.specific_install_command = \
        "cp -r $CURRENT_SOFTWARE_SRC_DIR/* $CURRENT_SOFTWARE_INSTALL_DIR"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "METIS", "HDF5",
                                             "MEDFICHIER"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version"]
      dependency_object.specific_install_var_name = "PYTHONDIR"
    if dependency_name == "METIS":
      dependency_object.depend_of = ["path", "ld_lib_path"]
      dependency_object.specific_install_var_name = "METISDIR"
    if dependency_name == "HDF5":
      dependency_object.depend_of = ["path", "ld_lib_path"]
      dependency_object.specific_install_var_name = "HDF5DIR"
    if dependency_name == "MEDFICHIER":
      dependency_object.depend_of = ["path", "ld_lib_path"]
      dependency_object.specific_install_var_name = "MEDFICHIERDIR"
    return dependency_object

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return telemac_template.substitute(install_dir=install_dir,
                                       systelcfg=self.systelcfg,
                                       usetelcfg=self.usetelcfg,
                                       ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return telemac_configuration_template.substitute(install_dir=install_dir,
                                       TELEMAC_DIR=install_dir,
                                       systelcfg=self.systelcfg,
                                       usetelcfg=self.usetelcfg,
                                       ld_library_path=misc.get_ld_library_path())

# -*- coding: utf-8 -*-
#  Copyright (C) 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.projects.salome.versions.V7_8_0 import V7_8_0

version_name = "V780H"
class V780H(V7_8_0):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    V7_8_0.__init__(self, name, verbose, flavour)
    self.accepted_flavours=["system_gcc"]

  def get_salome_version(self):
    return self.name

  def get_salome_vanilla_version(self):
    return "V7_8_0"

  def configure_softwares(self):
    V7_8_0.configure_softwares(self)

    # prerequisites
    self.add_software("M4",               "1.4.17",           groups=["base"])
    self.add_software("AUTOCONF",         "2.69",             groups=["base"])
    self.add_software("AUTOMAKE",         "1.15",             groups=["base"])
    self.add_software("LIBTOOL",          "2.4.5",            groups=["base"])
    self.add_software("GMP",              "5.1.3",            groups=["base"])
    self.add_software("MPFR",             "3.1.2",            groups=["base"])
    self.add_software("MPC",              "1.0.2",            groups=["base"])
    self.add_software("CLOOG",            "0.18.1",           groups=["base"])
    self.add_software("BINUTILS",         "2.21",             groups=["base"])
    self.add_software("GCC",              "4.8.4",            groups=["base"])

    self.add_software("PROJ",             "4.9.1",            groups=["base"])
    self.add_software("GEOS",             "3.4.2",            groups=["base"])
    self.add_software("GDAL",             "1.11.2",           groups=["base"])
    self.add_software("SQLITE",           "3.8.11.1",         groups=["base"])
    self.add_software("GRASS",            "6.4.4",            groups=["base"])
    self.add_software("GSL",              "1.16",             groups=["base"])
    self.add_software("GPSBABEL",         "1.5.2",            groups=["base"])
    self.add_software("NCURSES",          "5.9",              groups=["base"])
    self.add_software("LIBSPATIALITE",    "4.3.0",            groups=["base"])
    self.add_software("SPATIALINDEX",     "1.8.5",            groups=["base"])
    self.add_software("QWTPOLAR",         "1.1.1",            groups=["base"])
    self.add_software("PSYCOPG2",         "2.6.1",            groups=["base"])
    self.add_software("QSCINTILLA",       "2.7.1",            groups=["base"])
    self.add_software("WXWIDGETS",        "3.0.2",            groups=["base"])
    self.add_software("JASPER",           "1.900.1",          groups=["base"])
    self.add_software("SAGA",             "2.2.0",            groups=["base"])
    self.add_software("SHAPELY",          "1.5.9",            groups=["base"])
    self.add_software("FIONA",            "1.6.4",            groups=["base"])
    self.add_software("QGIS",             "2.10.1",           groups=["base"])

    self.add_software("CLOUDCOMPARE",     "v2.6.3.1",         groups=["base"])

    #self.add_software("NUMPY",            "1.8.1a",           groups=["base"])
    #self.add_software("SPHINX",           "1.3.1",            groups=["base"])

    # Tools
    self.add_software("MASCARET", "V7P1P8", "V7_1_8")
    self.add_software("TELEMAC", "trunk", "v7p1r1")

    # Modules
    #self.add_software("KERNEL", "hydro/V780")     # specifique HYDRO
    #self.add_software("GUI", "hydro/V780")        # specifique HYDRO
    self.add_software("GEOM", "hydro/V780")        # specifique HYDRO
    #self.add_software("SMESH", "hydro/V780")      # specifique HYDRO
    self.add_software("HYDROSOLVER", "trunk")
    self.add_software("HYDRO", "V7_dev")

  def update_configuration_with_flavour(self):
    V7_8_0.update_configuration_with_flavour(self)

    if "system_gcc" in self.flavour:
      self.remove_softwares(["GCC", "GMP"], remove_deps=True)

# -*- coding: utf-8 -*-
#  Copyright (C) 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.projects.salome.versions.V8_2_0 import V8_2_0
from yamm.core.base import misc

version_name = "V820H"
class V820H(V8_2_0):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    V8_2_0.__init__(self, name, verbose, flavour)
    self.accepted_flavours=["specific_gcc"]

  def get_salome_version(self):
    return self.name

  def get_salome_vanilla_version(self):
    return "V8_2_0"

  def configure_softwares(self):
    V8_2_0.configure_softwares(self)

    # prerequisites
    self.add_software("MPI4PY",           "1.3.1",            groups=["base"])

    if misc.get_calibre_version() != '7':
      self.add_software("SQLITE",           "3.8.11.1",         groups=["base"])
      self.add_software("GEOS",             "3.4.2",            groups=["base"])
      self.add_software("GDAL",             "1.11.2",           groups=["base"])
      self.add_software("CLOUDCOMPARE",     "v2.8.1",           groups=["base"])


    # Tools
    self.add_software("MASCARET", "V7P1P8", "V7_1_8")
    self.add_software("TELEMAC", "trunk", "v7p2r0")
    self.add_software("EFICAS_NOUVEAU", "nouvelEficas_V8",)

    # Modules
    self.add_software("GEOM", "hydro/imps_2017")        # specifique HYDRO, merge dans master apres V8_2, avant V8_3
    self.add_software("HYDROSOLVER", "pre/V8_2_BR")
    self.add_software("HYDRO", "pre/V8_2_BR")
    self.add_software("EFICAS", "V8_main",)

  def update_configuration_with_flavour(self):
    V8_2_0.update_configuration_with_flavour(self)

    if "specific_gcc" in self.flavour:
      self.add_software("OPENMPI", "1.6.5", groups=["base"])


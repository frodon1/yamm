# -*- coding: utf-8 -*-
#  Copyright (C) 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Renaud BARATE (EDF R&D)

from yamm.projects.salome.versions.V7_8_0 import V7_8_0
from yamm.core.base import misc

version_name = "V820H_ADDONS"
class V820H_ADDONS(V7_8_0):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    V7_8_0.__init__(self, name, verbose, flavour)
    self.accepted_flavours=[]

  def get_salome_version(self):
    return self.name

  def get_salome_vanilla_version(self):
    return "V8_2_0"

  def configure_softwares(self):
    V7_8_0.configure_softwares(self)

    # prerequisites
    self.add_software("PROJ",             "4.9.1",            groups=["base"])
    self.add_software("GEOS",             "3.4.2",            groups=["base"])
    self.add_software("GDAL",             "1.11.2",           groups=["base"])
    self.add_software("SQLITE",           "3.8.11.1",         groups=["base"])
    self.add_software("GRASS",            "6.4.4",            groups=["base"])
    self.add_software("GSL",              "1.16",             groups=["base"])
    self.add_software("GPSBABEL",         "1.5.2",            groups=["base"])
    self.add_software("NCURSES",          "5.9",              groups=["base"])
    self.add_software("LIBSPATIALITE",    "4.3.0",            groups=["base"])
    self.add_software("SPATIALINDEX",     "1.8.5",            groups=["base"])
    self.add_software("QWTPOLAR",         "1.1.1",            groups=["base"])
    self.add_software("PSYCOPG2",         "2.6.1",            groups=["base"])
    self.add_software("QSCINTILLA",       "2.7.1",            groups=["base"])
    self.add_software("WXWIDGETS",        "3.0.2",            groups=["base"])
    self.add_software("JASPER",           "1.900.1",          groups=["base"])
    self.add_software("SAGA",             "2.2.0",            groups=["base"])
    self.add_software("SHAPELY",          "1.5.9",            groups=["base"])
    self.add_software("FIONA",            "1.6.4",            groups=["base"])
    self.add_software("QCA",              "2.1.0",            groups=["base"])
    self.add_software("REQUESTS",         "2.13.0",           groups=["base"])
    self.add_software("NETWORKX",         "1.11",             groups=["base"])
    self.add_software("PYOPENGL",         "3.1.0",            groups=["base"])
    self.add_software("QGIS",             "2.18.3",           groups=["base"])
    minimal_list = ["QGIS", "KERNEL"]

    if misc.get_calibre_version() == '7':
      self.add_software("M4",               "1.4.17",           groups=["base"])
      self.add_software("AUTOCONF",         "2.69",             groups=["base"])
      self.add_software("AUTOMAKE",         "1.15",             groups=["base"])
      self.add_software("LIBTOOL",          "2.4.5",            groups=["base"])
      self.add_software("GMP",              "5.1.3",            groups=["base"])
      self.add_software("MPFR",             "3.1.2",            groups=["base"])
      self.add_software("MPC",              "1.0.2",            groups=["base"])
      self.add_software("CLOOG",            "0.18.1",           groups=["base"])
      self.add_software("BINUTILS",         "2.21",             groups=["base"])
      self.add_software("GCC",              "4.8.4",            groups=["base"])
      self.add_software("CLOUDCOMPARE",     "v2.6.3.1",         groups=["base"])
      minimal_list.append("CLOUDCOMPARE")

    self.set_software_minimal_list(minimal_list)

  def update_configuration_with_flavour(self):
    V7_8_0.update_configuration_with_flavour(self)


# -*- coding: utf-8 -*-
# Stable version
versions_list = []

# Development version
versions_list += ["V771H", "V780H", "V780H2016", "V820H", "V820H_ADDONS", "DEV"]

LAST_STABLE = 'V820H'

# Obsoletes versions
# Files were removed from directory.
# They can be retrieved in the git history.

old_versions_list = ["V0_1", "V7_4_1_DEV", "V7_5_1_DEV", "V7_6_BR_DEV"]

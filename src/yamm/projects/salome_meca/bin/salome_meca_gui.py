#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import sys
import os

yamm_path = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,)*4
sys.path.append(os.path.join(*yamm_path))

from yamm.core.framework.bin.yamm_gui import FrameworkGuiLauncher
from yamm.projects.salome_meca import project_gui

class SalomeMecaGuiLauncher(FrameworkGuiLauncher):

  def getSpecificTranslationFile(self):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        os.pardir, 'gui', 'yamm')

  def getProjectGui(self):
    return project_gui.ProjectGui

if __name__ == "__main__":
  launcher = SalomeMecaGuiLauncher(sys.argv)
  launcher.launchGui()

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Gilles DAVID (EDF R&D)

import os, sys

yamm_default_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../.."
sys.path.append(yamm_default_directory)

from yamm.projects.salome.gui.basic_wizard import BasicWizard as SalomeBasicWizard
from yamm.projects.salome.gui.basic_wizard import BasicIntro as SalomeBasicIntro
from yamm.projects.salome.gui.basic_wizard import BasicOptions as SalomeBasicOptions
from yamm.projects.salome.gui.basic_wizard import BasicRemotes as SalomeBasicRemotes
from yamm.core.framework.gui.select_software_list import ManageSoftwareList

from yamm.projects.salome_meca.project import Project

class BasicWizard(SalomeBasicWizard):

  def __init__(self, parent, title="Create an SALOME_MECA project", project = Project()):
    SalomeBasicWizard.__init__(self, parent, title, project)
    self.setWindowTitle("Create an SALOME_MECA project")

  def get_yamm_project(self):
    return Project()

  def add_pages(self):
    self.addPage(BasicIntro(self, self.get_yamm_project()))
    self.addPage(BasicOptions(self, self.get_yamm_project()))
    self.addPage(BasicRemotes(self, self.get_yamm_project()))
    self.addPage(ManageSoftwareList(self, self.get_yamm_project()))

class BasicIntro(SalomeBasicIntro):
  def __init__(self, parent, project):
    SalomeBasicIntro.__init__(self, parent, project)

class BasicOptions(SalomeBasicOptions):
  def __init__(self, parent, project):
    SalomeBasicOptions.__init__(self, parent, project)

  def setDefaultValues(self):
    SalomeBasicOptions.setDefaultValues(self, "salome_meca")

class BasicRemotes(SalomeBasicRemotes):
  def __init__(self, parent, project):
    SalomeBasicRemotes.__init__(self, parent, project)


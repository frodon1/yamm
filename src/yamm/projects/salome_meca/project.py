#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Nicolas GEIMER (EDF R&D)

from __future__ import print_function

import os
import os.path as osp
import sys
yamm_directory = os.path.dirname(os.path.abspath(__file__)) + "/../../../../src"
sys.path.append(yamm_directory)

from yamm.core.base.misc import VerboseLevels
from yamm.core.framework.project import yamm_command
from yamm.projects.salome.project import Project as SalomeProject

from yamm.projects.salome_meca.softwares import tools as default_tools
from yamm.projects.salome_meca.softwares import prerequisites as default_prerequisites
from yamm.projects.salome_meca.softwares import modules as default_modules
from yamm.projects.salome_meca import versions as default_versions


class Project(SalomeProject):

    """
      .. py:currentmodule:: yamm.projects.salome_meca.Project

      This class provides a project to build the SALOME-MECA platform.

      :param log_name: the project name
      :type log_name:  string
      :param verbose:  Verbose level of the project.
      :type verbose:   int
    """

    def __init__(self, log_name="SALOME_MECA", verbose_level=VerboseLevels.INFO):
        """Initialisation"""
        SalomeProject.__init__(self, log_name, verbose_level)
        self.appli_catalogresources = None
        self.appli_directory = ""
        self.define_salome_meca_options()
        self.only_prereq = False

    def set_version(self, version, flavour="", package_name=None):
        """Define the version used by the project, the flavour of this version
        and the name used for the built package.

        :param string package_name: name of the package (used as appli name).
        """
        super(Project, self).set_version(version, flavour)
        version_class = self.catalog.get_version_class(version)
        version_class.set_package_name(package_name or version)

    def init_catalog(self):
        SalomeProject.init_catalog(self)
        self.catalog.name = "SALOME_MECA"
        # Import des modules
        for module_name in default_modules.modules_list:
            self.add_software_in_catalog(
                module_name, "yamm.projects.salome_meca.softwares.modules")
        # Import des tools
        for module_name in default_tools.modules_list:
            self.add_software_in_catalog(
                module_name, "yamm.projects.salome_meca.softwares.tools")
        # Import des prerequis
        for module_name in default_prerequisites.modules_list:
            self.add_software_in_catalog(
                module_name, "yamm.projects.salome_meca.softwares.prerequisites")
        # Import des versions
        for version_name in default_versions.versions_list:
            self.add_version_in_catalog(
                version_name, "yamm.projects.salome_meca.versions")
            pass
        self.logger.debug("End init catalog")

    def define_salome_meca_options(self):
        """This method defines the options for Salome-Meca projects."""
        self.options.add_option("smeca_installer_directory", "", ["global"])
        self.options.add_option("smeca_public_build", False, ["global"])
        self.options.add_option("smeca_universal", False, ["global"])
        self.options.add_option("smeca_distrib", "", ["global"])

#
# Méthodes d'initialisation externes
#
    def set_default_options_values(self, opts):
        """Assign default values for options"""
        # Set Specific top directory
        if not opts.get_global_option("top_directory"):
            opts.set_global_option(
                "top_directory", os.path.join(os.environ["HOME"], "salome_meca"))

        # Framework project default values
        SalomeProject.set_default_options_values(self, opts)

#
# Méthodes pour tester la configuration
#
    def check_specific_configuration(self, opts):
        """Check a command configuration. It checks specific project options."""
        SalomeProject.check_specific_configuration(self, opts)

        if "codeaster_prerequisites" in self.version_flavour:
            self.only_prereq = True
        env = opts.get_global_option("software_additional_env_file")
        if env:
            for soft in ("MUMPS", "MUMPS4" ,"PETSC_MPI", "MISS3D", "CODE_EDYOS", "MFRONT", "MFRONT_STABLE"
                         "CODE_ASTER_OLDSTABLE", "CODE_ASTER_STABLE", "CODE_ASTER_TESTING", 
                         "PARMETIS_ASTER", "SCOTCH_ASTER", "MOMA" ):
                self.options.set_software_option(
                    soft, "software_additional_env_file", env)
        # Use remote mode instead of archive mode
        for soft in ("GMSH_BIN", "MUMPS", "MUMPS4", "PETSC_MPI", "INTEL_RUNTIME", "CODE_ASTER_DATA_TESTING", "CODE_ASTER_DATA",
                     "MERCURIAL", "MFRONT", "MFRONT_STABLE", "SCOTCH_ASTER" , "METIS_ASTER" , "PARMETIS_ASTER" , "MOMA"):
            self.options.set_software_option(soft, "source_type", 'remote')

#
# Méthodes d'introspection/affichage
#
    def print_directories(self, opts):
        """
          This method prints directories values of the current command.

          Do not use it by yourself. Instead use :py:meth:`~yamm.core.framework.project.FrameworkProject.print_configuration` command.
        """

        SalomeProject.print_directories(self, opts)
        self.logger.info("Installer directory      : %s"
                         % opts.get_global_option("smeca_installer_directory"))

    def print_general_configuration(self, opts):
        """
          This method prints general configuration values of the current command.

          Do not use it by yourself. Instead use :py:meth:`~yamm.core.framework.project.FrameworkProject.print_configuration` command.
        """
        SalomeProject.print_general_configuration(self, opts)
        # because SalomeProject prints 'SALOME Version'
        self.logger.info("SALOME-MECA Version    : %s" % self.version)
        self.logger.info("Version flavour        : %s" % self.version_flavour)
        self.logger.info("Create a public version: %s"
                         % opts.get_global_option("smeca_public_build"))
        self.logger.info("Create a universal arch: %s"
                         % opts.get_global_option("smeca_universal"))
        self.logger.info("Embedded distribution  : %s"
                         % opts.get_global_option("smeca_distrib"))

    def print_compilation_configuration(self, opts):
        """
          Print project's general compilation configuration.

          :param misc.options opts: Options object of current command.

        """
        SalomeProject.print_compilation_configuration(self, opts)
        # not general but only for some softwares
        if opts.get_global_option("software_additional_env_file"):
            self.logger.info("Environment file               : %s"
                             % opts.get_global_option("software_additional_env_file"))

#
# Méthodes spécifiques au projet salome_meca
#


if __name__ == "__main__":

    print("Testing salome_meca project class")
    pro = Project(0)
    pro.set_version("V2014_1")
    pro.print_configuration()
    pro.options.set_global_option("write_soft_infos", False)
    pro.nothing()
#  pro.download()
#  pro.start()

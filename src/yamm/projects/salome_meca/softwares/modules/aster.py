#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "ASTER"
aster_template = """
#------ ASTER module ------
export ASTER_ROOT_DIR="%install_dir"
# asrun
export PYTHONPATH=${ASTER_ROOT}/lib/python%python_version/site-packages:${PYTHONPATH}
# Services Python du module
export PYTHONPATH=${ASTER_ROOT_DIR}/lib/python%python_version/site-packages/salome:${PYTHONPATH}
# required for ASTER module (to update mesh)
export PYTHONPATH=${PYTHONPATH}:${EFICAS_ROOT}
export PYTHONPATH=${PYTHONPATH}:${EFICAS_ROOT}/UiQT5
export PYTHONPATH=${PYTHONPATH}:${EFICAS_ROOT}/Editeur
# ------- end of ASTER module ------
"""
aster_template = misc.PercentTemplate(aster_template)

aster_configuration_template = """
#------ ASTER module ------
# required for ASTER module (to update mesh)
ADD_TO_PYTHONPATH: %(EFICAS_ROOT)s
ADD_TO_PYTHONPATH: %(EFICAS_ROOT)s/UiQT5
ADD_TO_PYTHONPATH: %(EFICAS_ROOT)s/Editeur
# ------- end of ASTER module ------
"""
aster_configuration_template = string.Template(aster_configuration_template)

class ASTER(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 10

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    # self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "hg"
    self.repository_name = "salome-codeaster"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'
    self.tag = self.version

    self.compil_type = "python"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "KERNEL", "EFICAS",
                                             "CODE_ASTER_FRONTEND",
                                             "SALOMEMECA_PYUTILS"]

  def get_type(self):
    return "module"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)


  def module_support_cmake_compilation(self):
    return False

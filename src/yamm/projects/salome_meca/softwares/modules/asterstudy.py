#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GN
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "ASTERSTUDY"
aster_template = """
#------ ASTERSTUDY module ------
export ASTERSTUDY_ROOT_DIR="%install_dir"
# asrun
export PYTHONPATH=${ASTER_ROOT}/lib/python%python_version/site-packages:${PYTHONPATH}
# Services Python du module
export PYTHONPATH=${ASTERSTUDY_ROOT_DIR}/lib/python%python_version/site-packages/salome:${PYTHONPATH}
export PYTHONPATH=${ASTERSTUDY_ROOT_DIR}/lib/python%python_version/site-packages/asterstudy:${PYTHONPATH}

# ------- end of ASTERSTUDY module ------
"""
aster_template = misc.PercentTemplate(aster_template)

aster_configuration_template = """
#------ ASTERSTUDY module ------
ASTERSTUDY_ROOT_DIR="$install_dir"
# asrun
ADD_TO_PYTHONPATH: %(ASTER_ROOT)s/lib/python$python_version/site-packages
# Services Python du module
ADD_TO_PYTHONPATH: %(ASTERSTUDY_ROOT_DIR)s/lib/python$python_version/site-packages/salome
ADD_TO_PYTHONPATH: %(ASTERSTUDY_ROOT_DIR)s/lib/python$python_version/site-packages/asterstudy
# ------- end of ASTERSTUDY module ------
"""
aster_configuration_template = string.Template(aster_configuration_template)

class ASTERSTUDY(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.module_gui_sort = 10

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    # self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "hg"
    self.repository_name = "salome-codeaster-study"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'
    self.tag = self.version

    self.compil_type = "cmake"
    self.config_options = " -DENABLE_SALOME=ON -DFORCE_SALOMEMECA=ON -DWCKEY=P10WB:ASTER -DVERSION_LABEL={0} ".format(self.tag)
    self.pre_configure_commands = [
                        'cd ${CURRENT_SOFTWARE_SRC_DIR}/docs && echo -e ".. _devguide-salome:\n\n***************\nSALOME wrapping\n***************" > salome.rst',
                        ]
    self.post_install_commands = ['cd ${CURRENT_SOFTWARE_SRC_DIR} && hg update --clean', 
                                  'cd ${CURRENT_SOFTWARE_INSTALL_DIR}/lib && ln -s .. salome']
    #Lien symbolique vers lib/salome en attendant la mise en confirmité avec Salome
    

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CONFIGURATION" , "SETUPTOOLS" , "PYLINT", "SPHINX", "CMAKE"]
    self.software_dependency_dict['exec'] = ["KERNEL", "GUI","MEDCOUPLING","CODE_ASTER_FRONTEND"]

  def get_type(self):
    return "module"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_template.substitute(install_dir=install_dir,
                                             ld_library_path=misc.get_ld_library_path(),
                                             python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    # place dans env.d/envProducts.cfg
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_configuration_template.substitute(install_dir=install_dir,
                                                           ld_library_path=misc.get_ld_library_path(),
                                                           python_version=self.python_version)


  def module_support_cmake_compilation(self):
    return False

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import sys, string

software_name = "EDYOS"

edyos_template = """
#-------- MODULE EDYOS ------
"""
edyos_template = misc.PercentTemplate(edyos_template)

edyos_configuration_template = """
#-------- MODULE EDYOS -------
"""
edyos_configuration_template = string.Template(edyos_configuration_template)


class EDYOS(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "hg"
    self.repository_name = "salome-edyos"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'
    self.tag = self.version

    if "integr" in self.version:

        self.repository_name = ""
        self.root_repository = 'http://localhost:10002'
        self.tag=self.version.split('integr_')[1]


    self.compil_type = "cmake"
    self.config_options = " -DCMAKE_BUILD_TYPE=Debug "

    self.post_install_commands = [
                         'mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/bin/salome', ]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CONFIGURATION","CMAKE"]
    self.software_dependency_dict['exec'] = ["PYTHON", "CODE_EDYOS", "GUI"]

  def get_type(self):
    return "module"

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CODE_EDYOS":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return edyos_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return edyos_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def module_support_cmake_compilation(self):
    return False

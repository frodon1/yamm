#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D)
#
import string
import os

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc


software_name = "EPXDATA"

epxdata_configuration_template = """
#------ EPXDATA module ------
EPXDATA_DIR="$install_dir/EPX_INSTALL"
EPX_TTX_DOC=%(EPXDATA_DIR)s/manual_EPX
EPXDATA_ROOT_DIR=%(EPXDATA_DIR)s
ADD_TO_PATH: %(EPXDATA_DIR)s/TOOLS
# ------- end of EPXDATA module ------
"""
epxdata_configuration_template = string.Template(epxdata_configuration_template)


class EPXDATA(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.get_version_for_archive() + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "hg"
    self.repository_name = "epxdata"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'
    self.tag = self.version

    self.compil_type = "specific"

    self.specific_build_command = \
        "cd $CURRENT_SOFTWARE_SRC_DIR ;" \
        "export EPX_TTX_DOC=$CURRENT_SOFTWARE_SRC_DIR/manual_EPX ;" \
        "./EPX_SRC/build_configure ;" \
        "./EPX_SRC/configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR/EPX_INSTALL;"\
        "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/EPX_INSTALL ; cp -rp  $CURRENT_SOFTWARE_SRC_DIR/TOOLS $CURRENT_SOFTWARE_INSTALL_DIR/EPX_INSTALL/. ;"
    self.specific_install_command = \
        "export EPX_TTX_DOC=$CURRENT_SOFTWARE_SRC_DIR/manual_EPX ;" \
        "export EPXDATA_ROOT_DIR=$CURRENT_SOFTWARE_INSTALL_DIR ;" \
        "cp -rp  $CURRENT_SOFTWARE_SRC_DIR/TOOLS $CURRENT_SOFTWARE_INSTALL_DIR/EPX_INSTALL/. ;" \
        "make install"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["XDATA"]

  def get_type(self):
    return "module"

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return epxdata_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def module_support_cmake_compilation(self):
    return False


#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre Lefebvre (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import sys
import string
#
# ATTENTION : le module est nommé GVB en cohérence avec les scripts de construction
#
software_name = "GVB"

gevibus_configuration_template = """
#------ GVB module ------
GVB_DIR="$install_dir"
PREFS_CATA_GVB=%(GVB_DIR)s/share/salome/resources/gvb/eficas/GVB
ADD_TO_PYTHONPATH: %(PREFS_CATA_GVB)s
"""
gevibus_configuration_template = string.Template(gevibus_configuration_template)

class GVB(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "hg"

    self.repository_name = "salome-gevibus"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'

    self.tag = self.version

    if "integr" in self.version:

        self.repository_name = ""
        self.root_repository = 'http://localhost:10004'
        self.tag=self.version.split('integr_')[1]


    self.compil_type = "autoconf"
    self.gen_commands += ["./build_configure > build_configure.log 2>&1 ; "]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CONFIGURATION", "SPHINX", "SETUPTOOLS"]
    self.software_dependency_dict['exec'] = ["PYTHON", "KERNEL", "GUI", "GEOM" , "SMESH", "MED", "NETGENPLUGIN"]

  def get_type(self):
    return "module"

  """
  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "KERNEL":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "CONFIGURATION":
      dependency_object.depend_of = ["install_path"]
      dependency_object.specific_install_var_name = ["CONFIGURATION_CMAKE_DIR"]
    return dependency_object
  """
  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gevibus_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def module_support_cmake_compilation(self):
    return False

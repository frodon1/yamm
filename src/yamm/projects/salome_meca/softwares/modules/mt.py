#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "MT"

mt_configuration_template = """
#------ MT module ------
MT_DIR="$install_dir"
PREFS_CATA_MT=%(MT_DIR)s/share/salome/resources/mt/eficas/MT
ADD_TO_PYTHONPATH: %(PREFS_CATA_MT)s
PREFS_CATA_SPECA=%(MT_DIR)s/share/salome/resources/mt/eficas/SPECA
ADD_TO_PYTHONPATH: %(PREFS_CATA_SPECA)s
ADD_TO_PYTHONPATH: %(EFICAS_ROOT)s/InterfaceQT4
"""
mt_configuration_template = string.Template(mt_configuration_template)

class MT(SalomeSoftware):

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "hg"

    self.repository_name = "salome-rotor"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'

    self.tag = self.version
    if self.version == "trunk":
      self.tag = ""

    if "integr" in self.version:

        self.repository_name = ""
        self.root_repository = 'http://localhost:10001'
        self.tag=self.version.split('integr_')[1]


    self.compil_type = "autoconf"
    self.gen_commands += ["./build_configure"]

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON", "NETGENPLUGIN",
                                             "EFICASV1", "EFICAS", "KERNEL"]

  def get_type(self):
    return "module"

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "%(PREREQUISITES_ROOT_DIR)s":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mt_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def module_support_cmake_compilation(self):
    return False

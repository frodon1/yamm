# for Salome-Meca
modules_list = [
    "mumps", "petsc_mpi", "mfront", "mfront_stable",
    "gmsh_bin" , "mumps4",
    "intel_runtime", "mercurial",
    "scotch_aster", "metis_aster", "parmetis_aster",
    "sphinx_aster", "cmake_aster", "moma",
]

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2017 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)
#

from yamm.projects.salome.softwares.prerequisites.cmake import CMAKE

software_name = "CMAKE_ASTER"

class CMAKE_ASTER(CMAKE):

    def init_variables(self):
        """Define specific values for this distribution of cmake"""
        super(CMAKE_ASTER, self).init_variables()

        # self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

        self.tag = self.version
        self.build_dir = "keep_or_create"
        self.shell = "/bin/bash"


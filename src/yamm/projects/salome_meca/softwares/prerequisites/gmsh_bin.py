#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GMSH_BIN"
gmsh_bin_template = """
#------ gmsh_bin ------
export GMSH_BIN_DIR=%install_dir/bin
export PATH=${GMSH_BIN_DIR}:$PATH
"""
gmsh_bin_template = misc.PercentTemplate(gmsh_bin_template)

gmsh_bin_configuration_template = """
#------ gmsh_bin ------
GMSH_BIN_DIR=$install_dir/bin
ADD_TO_PATH: %(GMSH_BIN_DIR)s
"""
gmsh_bin_configuration_template = string.Template(gmsh_bin_configuration_template)

class GMSH_BIN(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.repository_name = "aster-prerequisites/gmsh"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/'
    self.tag = self.version

    self.compil_type = "rsync"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gmsh_bin_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gmsh_bin_configuration_template.substitute(install_dir=install_dir)

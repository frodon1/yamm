#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string
import os
import sys

software_name = "INTEL_RUNTIME"
intel_runtime_template = """
#------ INTEL_RUNTIME ------
INTEL_RUNTIME_INSTALL_DIR=%install_dir
export %ld_library_path=${INTEL_RUNTIME_INSTALL_DIR}/compiler/lib/intel64:${INTEL_RUNTIME_INSTALL_DIR}/mkl/lib/intel64:${%ld_library_path}
"""
intel_runtime_template = misc.PercentTemplate(intel_runtime_template)

intel_runtime_configuration_template = """
#------ INTEL_RUNTIME ------
INTEL_RUNTIME_INSTALL_DIR=$install_dir
ADD_TO_$ld_library_path: %(INTEL_RUNTIME_INSTALL_DIR)s/compiler/lib/intel64:%(INTEL_RUNTIME_INSTALL_DIR)s/mkl/lib/intel64
"""
intel_runtime_configuration_template = string.Template(intel_runtime_configuration_template)

class INTEL_RUNTIME(SalomeSoftware):

  def init_variables(self):

    # self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    # self.parallel_make     = "1"

    self.remote_type = "hg"
    self.repository_name = "runtime-intel"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca-prerequisites/'
    self.tag = self.version

    self.compil_type = "rsync"

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return intel_runtime_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return intel_runtime_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

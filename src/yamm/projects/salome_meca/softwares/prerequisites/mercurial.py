#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "MERCURIAL"
mercurial_template = """
#------- mercurial ------
export MERCURIAL_ROOT_DIR="%install_dir"
export PATH=${MERCURIAL_ROOT_DIR}/bin:${PATH}
export PYTHONPATH=${MERCURIAL_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
"""
mercurial_template = misc.PercentTemplate(mercurial_template)

mercurial_configuration_template = """
#------- mercurial ------
MERCURIAL_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(MERCURIAL_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(MERCURIAL_ROOT_DIR)s/lib/python$python_version/site-packages
"""
mercurial_configuration_template = string.Template(mercurial_configuration_template)

class MERCURIAL(SalomeSoftware):

  def init_variables(self):
    self.archive_file_name = "mercurial-" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    self.remote_type = "hg"
    self.repository_name = "mercurial"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca/'
    self.compil_type = "python"

    self.tag             = self.version
    
    self.replacePythonHeaderInstallPath("bin/hg")


  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path"]
    return dependency_object

  def update_configuration_with_dependency(self, dependency_name, version_name):
    if dependency_name == "PYTHON":
      self.executor_software_name += "-py" + misc.transform(version_name)

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mercurial_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mercurial_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

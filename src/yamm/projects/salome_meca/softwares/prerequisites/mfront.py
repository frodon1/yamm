#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)

import os
import os.path as osp
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "MFRONT"
mfront_template = """
#------ MFRONT ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
export PATH="%install_dir/bin":"$PATH"
export PYTHONPATH="%install_dir/lib/python%python_version/site-packages":$PYTHONPATH
export TFELHOME="%install_dir"
"""
mfront_template = misc.PercentTemplate(mfront_template)

mfront_configuration_template = """
#------ MFRONT ------
ADD_TO_$ld_library_path: "$install_dir/lib"
ADD_TO_PATH: "$install_dir/bin"
ADD_TO_PYTHONPATH: "$install_dir/lib/python$python_version/site-packages"
TFELHOME="$install_dir"
"""
mfront_configuration_template = string.Template(mfront_configuration_template)


class MFRONT(SalomeSoftware):

    def __init__(self, name, version, verbose, **kwargs):
        SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
        self.root_repository_template = string.Template('http://$server')

    def init_variables(self):

        if not self.project_options.is_software_option(self.name, "occ_server"):
            self.project_options.set_software_option(
                software_name, "occ_server",
                "aster-repo.der.edf.fr/scm/git/aster-prerequisites/")

        self.archive_type = "tar.gz"
        self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
        self.remote_type = "git"
        self.tag = self.version
        self.repository_name = "tfel"
        self.root_repository = self.root_repository_template.substitute(
            server=self.project_options.get_option(self.name, "occ_server"))

        self.compil_type = "cmake"
        self.build_dir = "keep_or_create"
        self.shell = "/bin/bash"
        # Configure options
        self.config_options = "-DTFEL_SVN_REVISION={0} -DCMAKE_BUILD_TYPE=Release -Dlocal-castem-header=ON  -Denable-fortran=ON -DPython_ADDITIONAL_VERSIONS=2.7 -Denable-python=ON -Denable-python-bindings=ON -Denable-cyrano=ON -Denable-aster=ON -DCMAKE_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR -DCMAKE_Fortran_COMPILER=/usr/bin/gfortran -Ddisable-reference-doc=ON -Ddisable-website=ON -Denable-portable-build=ON".format(
            self.tag)

        parallel_make = self.project_options.get_global_option("parallel_make")

        paths = self._get_python_paths()
        if paths:
            self.config_options += " -DPYTHON_LIBRARY={0.lib} -DPYTHON_INCLUDE_DIR={0.include}".format(paths)

        self.specific_build_command = "cd $CURRENT_SOFTWARE_BUILD_DIR && make -j%s" % parallel_make
        self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR && make install"

    def _get_python_paths(self):
        from collections import namedtuple
        paths = None
        for soft in self.project_softwares:
            if soft.name == "PYTHON":
                install_dir = soft.install_directory
                version = soft.get_python_version()
                lib = osp.join(install_dir, "lib",
                               "libpython{0}.so".format(version))
                inc = osp.join(install_dir, "include",
                               "python{0}".format(version))
                paths = namedtuple('paths', ['lib', 'include'])(lib, inc)
        return paths

    def init_dependency_list(self):
        self.software_dependency_dict['build'] = ["CMAKE"]
        self.software_dependency_dict['exec'] = ["PYTHON", "MERCURIAL"]

    def get_dependency_object_for(self, dependency_name):
        dependency_object = SalomeSoftware.get_dependency_object_for(
            self, dependency_name)
        if dependency_name == "PYTHON":
            dependency_object.depend_of = [
                "path", "ld_lib_path", "python_version", "install_path"]
        if dependency_name == "CMAKE":
            dependency_object.depend_of = ["path", "install_path"]
        return dependency_object

    def get_type(self):
        return "prerequisite"

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = osp.join(
                specific_install_dir, self.executor_software_name)
        return mfront_template.substitute(
            install_dir=install_dir,
            ld_library_path=misc.get_ld_library_path(),
            python_version=self.python_version)

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = osp.join(
                specific_install_dir, self.executor_software_name)
        return mfront_configuration_template.substitute(
            install_dir=install_dir,
            ld_library_path=misc.get_ld_library_path(),
            python_version=self.python_version)

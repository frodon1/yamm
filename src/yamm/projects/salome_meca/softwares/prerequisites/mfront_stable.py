#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)

import os
import os.path as osp
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.projects.salome_meca.softwares.prerequisites.mfront import MFRONT
from yamm.core.base import misc

software_name = "MFRONT_STABLE"
mfront_template = """
#------ MFRONT_STABLE ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
export PATH="%install_dir/bin":"$PATH"
"""
mfront_template = misc.PercentTemplate(mfront_template)

mfront_configuration_template = """
#------ MFRONT_STABLE ------
ADD_TO_$ld_library_path: "$install_dir/lib"
ADD_TO_PATH: "$install_dir/bin"
"""
mfront_configuration_template = string.Template(mfront_configuration_template)

class MFRONT_STABLE(MFRONT):

    def init_variables(self):
        """Define specific values for this distribution of MFRONT"""
        super(MFRONT_STABLE, self).init_variables()

        self.remote_type = "hg"
        self.repository_name = "mfront"
        self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/aster-prerequisites'
        self.tag = self.version
        svntag = self.tag

        self.compil_type = "specific"
        self.build_dir = "keep_or_create"
        self.shell = "/bin/bash"
        self.config_options =""
    # Use a private Python
        self.specific_configure_command = 'if [ ! -z $PYTHON_INCLUDE ]; then export INCLUDES="$INCLUDES $PYTHON_INCLUDE" ; fi ; '
        self.specific_configure_command += 'if [ ! -z $PYTHONHOME ]; then export LIBPATH="$LIBPATH $PYTHONHOME/lib" ; fi ; '

        parallel_make = self.project_options.get_global_option("parallel_make")

        self.specific_configure_command += """cd $CURRENT_SOFTWARE_BUILD_DIR && cmake $CURRENT_SOFTWARE_SRC_DIR -DTFEL_SVN_REVISION=%s -DCMAKE_BUILD_TYPE=Release -Dlocal-castem-header=ON  -Denable-fortran=ON \
    -Denable-python=ON  -Denable-aster=ON -DCMAKE_INSTALL_PREFIX=$CURRENT_SOFTWARE_INSTALL_DIR  \
    -DCMAKE_Fortran_COMPILER=/usr/bin/gfortran \
    -Ddisable-reference-doc=ON -Ddisable-website=ON  \
    -Denable-portable-build=ON
""" % svntag
        self.specific_build_command = "cd $CURRENT_SOFTWARE_BUILD_DIR && make -j%s" % parallel_make
        self.specific_install_command = "cd $CURRENT_SOFTWARE_BUILD_DIR && make install"  
    def init_dependency_list(self):
        self.software_dependency_dict['build'] = ["CMAKE_ASTER"]
        self.software_dependency_dict['exec'] = ["PYTHON", "MERCURIAL", "MFRONT"]

    def get_dependency_object_for(self, dependency_name):
        dependency_object = MFRONT.get_dependency_object_for(
            self, dependency_name)
        if dependency_name == "PYTHON":
            dependency_object.depend_of = [
                "path", "ld_lib_path", "python_version", "install_path"]
        if dependency_name == "CMAKE_ASTER":
            dependency_object.depend_of = ["path", "install_path"]
        return dependency_object

    def get_prerequisite_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = osp.join(
                specific_install_dir, self.executor_software_name)
        return mfront_template.substitute(
            install_dir=install_dir,
            ld_library_path=misc.get_ld_library_path(),
            python_version=self.python_version)

    def get_configuration_str(self, specific_install_dir=""):
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = osp.join(
                specific_install_dir, self.executor_software_name)
        return mfront_configuration_template.substitute(
            install_dir=install_dir,
            ld_library_path=misc.get_ld_library_path(),
            python_version=self.python_version)

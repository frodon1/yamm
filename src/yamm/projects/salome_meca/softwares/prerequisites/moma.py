#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "MOMA"
moma_template = """
#------ moma ------
MOMA_ROOT_DIR="%install_dir"
MAP_DIRECTORY="%install_dir"
export PYTHONPATH=${MOMA_ROOT_DIR}/lib/python%python_version/site-packages:${PYTHONPATH}
export PATH=${MAP_INSTALL_DIR}/bin:$PATH
export LD_LIBRARY_PATH=${MAP_DIRECTORY}/lib:${LD_LIBRARY_PATH}
"""
moma_template = misc.PercentTemplate(moma_template)

moma_configuration_template = """
#------ moma ------
MOMA_ROOT_DIR="$install_dir"
MAP_DIRECTORY="$install_dir"
ADD_TO_PATH: %(MOMA_ROOT_DIR)s/bin
ADD_TO_PYTHONPATH: %(MOMA_ROOT_DIR)s/lib/python$python_version/site-packages
ADD_TO_LD_LIBRARY_PATH=%(MAP_DIRECTORY)s/lib
"""
moma_configuration_template = string.Template(moma_configuration_template)

class MOMA(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.repository_name = "moma"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'
    self.tag = self.version	

    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.shell = "/bin/bash"

    parallel_make = self.project_options.get_global_option("parallel_make")

# build madnex prerequisite

    self.specific_configure_command  = """cd $CURRENT_SOFTWARE_SRC_DIR ; PYTHON_XY="python`/usr/bin/env python --version 2>&1 | cut -d" " -f2 | cut -d"." -f1-2`";
                                          wget http://ftp.pleiade.edf.fr/projets/MAP/MAPshare/deps/madnex/madnex-1.4.tar.gz ; 
                                          tar xf madnex-1.4.tar.gz ;
                                          MADNEX_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/build/madnex-1.4 && mkdir -p ${MADNEX_BUILD_DIR} ;"""
    self.specific_build_command =      """MADNEX_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/build/madnex-1.4 ;
                                          MADNEX_INSTALL_DIR=${CURRENT_SOFTWARE_INSTALL_DIR} ;
                                          cd  $CURRENT_SOFTWARE_SRC_DIR/madnex-1.4 ; python setup.py build --build-base=${MADNEX_BUILD_DIR} install --prefix=${MADNEX_INSTALL_DIR} ;
""" 
# build MAP libraries
    self.specific_configure_command += """MFRONT_COMMON_BUILD_DIR=$CURRENT_SOFTWARE_SRC_DIR/mfront_common; 
                                          mkdir -p ${MFRONT_COMMON_BUILD_DIR}; """
    self.specific_build_command +=     """MFRONT_COMMON_BUILD_DIR=$CURRENT_SOFTWARE_SRC_DIR/mfront_common; 
                                          MFRONT_COMMON_SOURCE_DIR=$CURRENT_SOFTWARE_SRC_DIR/src/MAP_library/mapy/mapnumeric/mfront_common;
                                          cd ${MFRONT_COMMON_BUILD_DIR};
                                          cmake -DCMAKE_INSTALL_PREFIX=${CURRENT_SOFTWARE_INSTALL_DIR} ${MFRONT_COMMON_SOURCE_DIR};""" 
                                       
    self.specific_install_command =    """MFRONT_COMMON_BUILD_DIR=$CURRENT_SOFTWARE_SRC_DIR/mfront_common
                                          cd $MFRONT_COMMON_BUILD_DIR  && make; make install;"""
## mapcore
    self.specific_configure_command += """MAPCORE_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/mapcore;
                                          mkdir -p ${MAPCORE_BUILD_DIR}; """
    self.specific_build_command +=     """MAPCORE_SOURCE_DIR=${CURRENT_SOFTWARE_SRC_DIR}/src/MAP_library/mapy/mapcore;
                                          MAPCORE_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/mapcore;
                                          cd ${MAPCORE_BUILD_DIR};
                                          cmake -DCMAKE_INSTALL_PREFIX=${CURRENT_SOFTWARE_INSTALL_DIR} ${MAPCORE_SOURCE_DIR};"""
    self.specific_install_command +=   """MAPCORE_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/mapcore;
                                          cd ${MAPCORE_BUILD_DIR} && make; make install;"""
## mapsh
    self.specific_configure_command += """MAPSH_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/mapsh;
                                          mkdir -p ${MAPSH_BUILD_DIR};  """
    self.specific_build_command +=     """MAPSH_SOURCE_DIR=${CURRENT_SOFTWARE_SRC_DIR}/src/MAP_library/mapsh;
                                          MAPSH_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/mapsh;
                                          cd ${MAPSH_BUILD_DIR};
                                          cmake -DCMAKE_INSTALL_PREFIX=${CURRENT_SOFTWARE_INSTALL_DIR} ${MAPSH_SOURCE_DIR} ;"""
    self.specific_install_command +=   """MAPSH_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/mapsh;
                                          cd ${MAPSH_BUILD_DIR} && make; make install;"""

## add missing __init__.py files
    self.specific_configure_command += """mkdir -p ${CURRENT_SOFTWARE_INSTALL_DIR}/lib/${PYTHON_XY}/site-packages/mapy ;"""
    self.specific_configure_command += """touch ${CURRENT_SOFTWARE_INSTALL_DIR}/lib/${PYTHON_XY}/site-packages/mapy/__init__.py ;"""

# build MAP component
    self.specific_configure_command += """CSCL0D_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/src/MAP_components/solver/c_solver_constitutive_law_0d; 
                                          mkdir -p ${CSCL0D_BUILD_DIR}; """
    self.specific_build_command +=      """CSCL0D_SOURCE_DIR=${CURRENT_SOFTWARE_SRC_DIR}/src/MAP_components/solver/c_solver_constitutive_law_0d;
                                           CSCL0D_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/src/MAP_components/solver/c_solver_constitutive_law_0d;
                                           cd ${CSCL0D_BUILD_DIR};
                                           cmake -DCMAKE_INSTALL_PREFIX=${CURRENT_SOFTWARE_INSTALL_DIR} ${CSCL0D_SOURCE_DIR};"""
    self.specific_install_command +=    """CSCL0D_BUILD_DIR=${CURRENT_SOFTWARE_SRC_DIR}/src/MAP_components/solver/c_solver_constitutive_law_0d;
                                           cd ${CSCL0D_BUILD_DIR} && make; make install;"""

# add missing __init__.py files
    self.specific_configure_command += """mkdir -p ${CURRENT_SOFTWARE_INSTALL_DIR}/lib/${PYTHON_XY}/site-packages/mapy/components ; """
    self.specific_configure_command += """touch ${CURRENT_SOFTWARE_INSTALL_DIR}/lib/${PYTHON_XY}/site-packages/mapy/components/__init__.py ; """

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return moma_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return moma_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)


  def init_dependency_list(self):
    self.software_dependency_dict['build'] = ["CMAKE_ASTER"]
    self.software_dependency_dict['exec'] = ["MFRONT","EFICAS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
#    if dependency_name == "MFRONT":
#      dependency_object.depend_of = ["TFELHOME", "install_path"]
    if dependency_name == "CMAKE":
      dependency_object.depend_of = ["path", "install_path"]
    return dependency_object


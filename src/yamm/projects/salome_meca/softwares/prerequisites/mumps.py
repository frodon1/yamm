

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "MUMPS"
mumps_template = """
#------ MUMPS ------
export %ld_library_path="%install_dir/%REP/lib":${%ld_library_path}
"""
mumps_template = misc.PercentTemplate(mumps_template)

mumps_configuration_template = """
#------ MUMPS ------
ADD_TO_$ld_library_path: "$install_dir/$REP/lib"
"""
mumps_configuration_template = string.Template(mumps_configuration_template)

class MUMPS(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
     version_split = version.split()
     version_split_percent = version_split[0].split("%")
     short_version = version_split_percent[0]
     self.version_suffix = "seq"
     self.version = short_version
     if len(version_split_percent) >= 2:
        self.version_suffix = version_split_percent[1]
     if len(version_split) > 1:
        version = "{0} {1}".format(short_version, version_split[1])
     SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
     self.root_repository_template = string.Template('http://$server')
     self.tag = short_version

  def init_variables(self):

    if not self.project_options.is_software_option(self.name, "occ_server"):
      self.project_options.set_software_option(software_name, "occ_server",
                                               "aster-repo.der.edf.fr/scm/hg/aster-prerequisites/")
    short_version = self.version.split()[0].split("%")[0]
    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.remote_type = "hg"
    self.repository_name = "mumps"
    self.root_repository = self.root_repository_template.substitute(
    server=self.project_options.get_option(self.name, "occ_server"))
#    self.repository_name = ""
#    self.root_repository = 'http://athosdev2.hpc.edf.fr:10006'
#    self.tag = "119"

    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory
    self.shell = "/bin/bash"
    # Configure options

    # Use a private Python
    self.specific_configure_command = 'if [ ! -z $PYTHON_INCLUDE ]; then export INCLUDES="$INCLUDES $PYTHON_INCLUDE" ; fi ; '
    self.specific_configure_command += 'if [ ! -z $PYTHONHOME ]; then export LIBPATH="$LIBPATH $PYTHONHOME/lib" ; fi ; '

    # Waf arguments
    self.specific_configure_command += ' args=" " ; '

    # Intel Compiler
    self.specific_configure_command += "isintel=`ifort --help 2>/dev/null` ; "
    self.specific_configure_command += 'if [ "x$isintel" != "x" ]; then '
    #
    if self.version_suffix == 'mpi':
      self.specific_configure_command += 'export CC="mpiicc" ; '
      self.specific_configure_command += 'export FC="mpiifort" ; '
      self.specific_configure_command += "vers_ifort=`ifort --version | head -1 | awk '{print $3}'| awk -F. '{print $1}'`;"
      self.specific_configure_command += "if [ $vers_ifort -lt 13 ]; then "
      self.specific_configure_command += "   export OPTLIB_FLAGS='-Wl,-Bstatic -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -Wl,--end-group' ;"
      self.specific_configure_command += "fi ; "
    else :
      self.specific_configure_command += 'export CC="icc" ; '
      self.specific_configure_command += 'export FC="ifort" ; '

    self.specific_configure_command += "fi ; "
    # Fin Intel Compiler

    # Default waf parameters
    if self.version_suffix == 'mpi':
      self.specific_configure_command += "export LIBPATH=\"$METIS_ASTER_INSTALL_DIR/lib $PARMETIS_ASTER_INSTALL_DIR/lib $SCOTCH_ASTER_INSTALL_DIR/MPI/lib\" ; "
      self.specific_configure_command += "export INCLUDES=\"$METIS_ASTER_INSTALL_DIR/include $PARMETIS_ASTER_INSTALL_DIR/include $SCOTCH_ASTER_INSTALL_DIR/MPI/include\" ; "
    else :
      self.specific_configure_command += "export LIBPATH=\"$METIS_ASTER_INSTALL_DIR/lib $SCOTCH_ASTER_INSTALL_DIR/SEQ/lib\" ; "
      self.specific_configure_command += "export INCLUDES=\"$METIS_ASTER_INSTALL_DIR/include $SCOTCH_ASTER_INSTALL_DIR/SEQ/include\" ; "
      
    # GNU blas/lapack
    if self.version_suffix == 'mpi':
       self.specific_configure_command += "args=\"$args --enable-metis --enable-parmetis --enable-scotch \" ; "
    else :
       self.specific_configure_command += "args=\"$args --enable-metis --enable-scotch \" ; "
       

    if self.version_suffix == 'mpi':
      self.specific_configure_command += "./waf configure --enable-mpi --prefix=$CURRENT_SOFTWARE_INSTALL_DIR/MPI  $args "
    else :
      self.specific_configure_command += "./waf configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR/SEQ  $args "

    # Build
    self.specific_build_command = "./waf build -j1;"

    # Install
    self.specific_install_command = "./waf install --install-tests ${args}; "

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    if self.version_suffix == "seq" :
       self.software_dependency_dict['exec'] = ["PYTHON", "METIS_ASTER", "SCOTCH_ASTER", "MERCURIAL", "NUMPY"]
    else : 
       self.software_dependency_dict['exec'] = ["PYTHON", "METIS_ASTER", "PARMETIS_ASTER", "SCOTCH_ASTER", "MERCURIAL", "NUMPY"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    if dependency_name == "METIS_ASTER":
      dependency_object.depend_of = ["install_path"]
    if self.version_suffix == "mpi" :
      if dependency_name == "PARMETIS_ASTER":
        dependency_object.depend_of = ["install_path"]
    if dependency_name == "SCOTCH_ASTER":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):

    REP = "MPI"
    if self.version_suffix == "seq" :
      REP = "SEQ"
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mumps_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), REP=REP)

  def get_configuration_str(self, specific_install_dir=""):
    REP = "MPI"
    if self.version_suffix == "seq" :
      REP = "SEQ"
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return mumps_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), REP=REP)

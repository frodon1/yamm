#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)

from yamm.projects.salome_meca.softwares.prerequisites.mumps import MUMPS

software_name = "MUMPS4"

class MUMPS4(MUMPS):

    def init_variables(self):
        """Define specific values for this distribution of Mumps"""
        super(MUMPS4, self).init_variables()

        self.remote_type = "hg"
        self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/aster-prerequisites'
        self.archive_type = "tar.gz"
        self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
        self.repository_name = "mumps"

        # Configure options

        # Use a private Python
        self.specific_configure_command = 'if [ ! -z $PYTHON_INCLUDE ]; then export INCLUDES="$INCLUDES $PYTHON_INCLUDE" ; fi ; '
        self.specific_configure_command += 'if [ ! -z $PYTHONHOME ]; then export LIBPATH="$LIBPATH $PYTHONHOME/lib" ; fi ; '

        # Waf arguments
        self.specific_configure_command += ' args="" ; '

        # Intel Compiler
        self.specific_configure_command += "isintel=`ifort --help 2>/dev/null` ; "
        self.specific_configure_command += 'if [ "x$isintel" != "x" ]; then '
        #
        if self.version_suffix == 'mpi':
           self.specific_configure_command += 'export CC="mpiicc" ; '
           self.specific_configure_command += 'export FC="mpiifort" ; '
        else :
           self.specific_configure_command += 'export CC="icc" ; '
           self.specific_configure_command += 'export FC="ifort" ; '

        self.specific_configure_command += "fi ; "
        # Fin Intel Compiler

        # Default waf parameters
        self.specific_configure_command += "export LIBPATH=\"$METIS_INSTALL_DIR $SCOTCH_INSTALL_DIR/lib\" ; "
        self.specific_configure_command += "export INCLUDES=\"$METIS_INSTALL_DIR/Lib $SCOTCH_INSTALL_DIR/include\" ; "

        # GNU blas/lapack
        self.specific_configure_command += "args=\"$args --enable-metis --enable-scotch \" ; "

        if self.version_suffix == 'mpi':
           self.specific_configure_command += "./waf configure --enable-mpi --prefix=$CURRENT_SOFTWARE_INSTALL_DIR/MPI  $args "
        else :
          self.specific_configure_command += "./waf configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR/SEQ  $args "

    def init_dependency_list(self):
        self.software_dependency_dict['build'] = []
        self.software_dependency_dict['exec'] = ["PYTHON", "METIS", "SCOTCH", "MERCURIAL", "NUMPY"]

    def get_dependency_object_for(self, dependency_name):
       dependency_object = MUMPS.get_dependency_object_for(self, dependency_name)
       if dependency_name == "PYTHON":
          dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
       if dependency_name == "METIS":
          dependency_object.depend_of = ["install_path"]
       if dependency_name == "SCOTCH":
          dependency_object.depend_of = ["install_path"]
       return dependency_object



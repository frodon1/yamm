#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)

from yamm.projects.salome_meca.softwares.prerequisites.metis_aster import METIS_ASTER

software_name = "PARMETIS_ASTER"

class PARMETIS_ASTER(METIS_ASTER):

    def init_variables(self):
        """Define specific values for this distribution of metis"""
        super(PARMETIS_ASTER, self).init_variables()

        # self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

        self.remote_type = "hg"
        self.repository_name = "parmetis"
        self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/aster-prerequisites'
        self.tag = self.version
        self.build_dir = "keep_or_create"
        self.shell = "/bin/bash"
        self.build_directory = self.src_directory

# on teste la présence du compilateur Intel
	self.specific_configure_command  = "isintel=`ifort --help 2>/dev/null` ; "
        self.specific_configure_command += 'if [ "x$isintel" != "x" ]; then '
#
        self.specific_configure_command += 'cc="mpiicc" ; '
        self.specific_configure_command += 'cxx="mpiicc" ; '
        self.specific_configure_command += 'else  '
        self.specific_configure_command += 'cc="mpicc" ; '
        self.specific_configure_command += 'cxx="mpicxx" ; '
        self.specific_configure_command += "fi ; "
# Fin compilateur Intel

        self.specific_configure_command += "make config CFLAGS=-fPIC cc=$cc cxx=$cxx prefix=$CURRENT_SOFTWARE_INSTALL_DIR ;"
        self.specific_build_command = "make"
        self.specific_install_command = "make install"

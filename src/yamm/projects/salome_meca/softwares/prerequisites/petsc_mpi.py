#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc

software_name = "PETSC_MPI"
petsc_mpi_template = """
#------ PETSC_MPI ------
export %ld_library_path="%install_dir/lib":${%ld_library_path}
"""
petsc_mpi_template = misc.PercentTemplate(petsc_mpi_template)

petsc_mpi_configuration_template = """
#------ PETSC_MPI ------
ADD_TO_$ld_library_path: "$install_dir/lib"
"""
petsc_mpi_configuration_template = string.Template(petsc_mpi_configuration_template)

class PETSC_MPI(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.root_repository_template = string.Template('http://$server')
    self.version = version.split()[0]
    self.tag = version.split()[1]   
  def init_variables(self):

    if not self.project_options.is_software_option(self.name, "occ_server"):
      self.project_options.set_software_option(software_name, "occ_server",
                                               "aster-repo.der.edf.fr/scm/hg/aster-prerequisites/")
    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.tag = self.version
    self.repository_name = "petsc"
    self.root_repository = self.root_repository_template.substitute(
                             server=self.project_options.get_option(self.name, "occ_server"))

    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory
    self.shell = "/bin/bash"

#    parallel_make          = self.project_options.get_global_option("parallel_make")

    # Use a private Python
    self.specific_configure_command = 'if [ ! -z $PYTHON_INCLUDE ]; then export INCLUDES="$INCLUDES $PYTHON_INCLUDE" ; fi ; '
    self.specific_configure_command += 'if [ ! -z $PYTHONHOME ]; then export LIBPATH="$LIBPATH $PYTHONHOME/lib" ; fi ; '

    # si Intel et MKLROOT non defini (Ivanoe)
    self.specific_configure_command = "isintel=`ifort --help 2>/dev/null` ; "
    self.specific_configure_command += 'if [ "x$isintel" != "x" ]; then '  # warning : need "" inside if statement
    self.specific_configure_command += ' MPIICPC="mpiicpc" ; MPIICC="mpiicc" ; MPIIFORT="mpiifort" ;' 
    self.specific_configure_command += "if [ -z $MKLROOT ]; then "
    self.specific_configure_command += "MKL=`echo $LD_LIBRARY_PATH | awk -F':' '{for (i = 1; i < (NF); i++) {print $i} }' | grep 'mkl' | tail -1 | awk -F'mkl/' '{print $1}'` ; "
    self.specific_configure_command += 'export MKLROOT="${MKL}/mkl" ; '
    self.specific_configure_command += "if [ ! -d $MKLROOT ] ; then exit 1 ; fi ; "
    self.specific_configure_command += "fi ; "
    self.specific_configure_command += 'LIBSCALA="-L$MKLROOT/lib/intel64 -Wl,--start-group -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lmkl_lapack95_lp64 -Wl,--end-group " ;'
    self.specific_configure_command += 'LIBBLAS=$LIBSCALA;'
    self.specific_configure_command += 'DIRSCALA="dirname `find $MKLROOT -name "libmkl_scalapack_lp64.so" | head -1` ;"'
    self.specific_configure_command += 'pour_hypre_ml="--with-blas-lapack-lib=${LIBBLAS} --with-scalapack-dir=${DIRSCALA}" ;'
    self.specific_configure_command += "else "
    self.specific_configure_command += ' MPIICPC="mpicxx" ; MPIICC="mpicc" ; MPIIFORT="mpif90" ;'
    self.specific_configure_command += 'LIBSCALA="-L/usr/lib -lscalapack-openmpi -lblacs-openmpi -lblacsF77init-openmpi -lblacsCinit-openmpi " ;'
    self.specific_configure_command += 'DIRSCALA="/usr/lib" ;'
    self.specific_configure_command += 'LIBBLAS="-L/usr/lib -llapack -lopenblas " ;'
    self.specific_configure_command += 'pour_hypre_ml=" " ;'
    self.specific_configure_command += "fi ; "
#   La variable LANG ne doit pas etre definie pour le configure
    self.specific_configure_command += "unset LANG; hg up " + self.tag + "; cd petsc-src; THRD=`pwd`/../3rd; "
    
    if self.tag == "3.7.3_aster" : 
       self.specific_configure_command += "export LANG=C; export LC_ALL=C ; "
       self.specific_configure_command += 'export LIBPATH="$MUMPS_INSTALL_DIR/MPI/lib $METIS_ASTER_INSTALL_DIR/lib $SCOTCH_ASTER_INSTALL_DIR/MPI/lib" ; '
       self.specific_configure_command += 'export INCLUDES="$MUMPS_INSTALL_DIR/MPI/include $METIS_ASTER_INSTALL_DIR/include $SCOTCH_ASTER_INSTALL_DIR/MPI/include" ; '
       self.specific_configure_command += "export PETSC_INSTALLDIR=$CURRENT_SOFTWARE_INSTALL_DIR;"
       self.specific_configure_command += "export PETSC_DIR=$CURRENT_SOFTWARE_SRC_DIR/petsc-src;"
       self.specific_configure_command += """./configure --with-debugging=0 \
       --with-mpi=1 \
       --with-ssl=0 \
       --with-x=0 \
       --with-cxx=${MPIICPC} \
       --with-cc=${MPIICC} \
       --with-fc=${MPIIFORT} \
       --with-mumps-lib="-L$MUMPS_INSTALL_DIR/MPI/lib -lzmumps -ldmumps -lmumps_common -lpord -L$SCOTCH_ASTER_INSTALL_DIR/MPI/lib -lesmumps -lptscotch -lptscotcherr -lptscotcherrexit -lscotch -lscotcherr -lscotcherrexit -L$PARMETIS_ASTER_INSTALL_DIR/lib -lparmetis -L$METIS_ASTER_INSTALL_DIR/lib -lmetis " \
       --with-mumps-include=$MUMPS_INSTALL_DIR/MPI/include \
       --with-blas-lapack-lib="${LIBBLAS}" \
       --with-scalapack-lib="${LIBSCALA}" \
       --PETSC_ARCH=arch-linux2-c-opt-mpi-ml-hypre  \
       --download-ml=${THRD}/petsc-pkg-ml-e5040d11aa07.tar.gz \
       --download-hypre=${THRD}/hypre-2.11.1.tar.gz \
       --with-openmp=0 \
         LIBS=-lgomp \
       --prefix=$CURRENT_SOFTWARE_INSTALL_DIR ; """
    elif self.tag == "petsc_aster-3.6.3" : 
       self.specific_configure_command += """python ./config/configure.py --with-debugging=0 \
       --with-shared-libraries=0 \
       --with-mpi=1 \
       --with-ssl=0 \
       --with-x=0 \
       --with-cxx=${MPIICPC} \
       --with-cc=${MPIICC} \
       --with-fc=${MPIIFORT} \
       --with-blas-lapack-lib="${LIBBLAS}" \
       --with-scalapack-lib="${LIBSCALA}" \
       --PETSC_ARCH=arch-linux2-c-opt-mpi-ml-hypre \
       --download-ml=${THRD}/ml-6.2-p2.tar.gz \
       --download-hypre=${THRD}/hypre-2.10.0b-p1.tar.gz \
       --prefix=$CURRENT_SOFTWARE_INSTALL_DIR \
       --with-git=0 ; """
    elif self.tag == "3.4.4_aster" :

       self.specific_configure_command += """python ./config/configure.py --with-debugging=0 \
       --with-shared-libraries=0 \
       --with-mpi=1 \
       --with-x=0 \
       --with-cxx=${MPIICPC} \
       --with-cc=${MPIICC} \
       --with-fc=${MPIIFORT} \
       "${pour_hypre_ml}" \
       --PETSC_ARCH=arch-linux2-c-opt-mpi-ml-hypre \
       --download-ml=${THRD}/ml-6.2-win.tar.gz \
       --download-hypre=${THRD}/hypre-2.7.0b.tar.gz \
       --prefix=$CURRENT_SOFTWARE_INSTALL_DIR \
       --with-git=0 ; """
    else :
       self.specific_configure_command += """ 'echo error !!!'; """ 
#       --with-blas-lapack-lib="${LIBBLAS}" \
#       --with-scalapack-dir=${DIRSCALA} \

    self.specific_build_command = "cd petsc-src; make PETSC_DIR=$CURRENT_SOFTWARE_SRC_DIR/petsc-src PETSC_ARCH=arch-linux2-c-opt-mpi-ml-hypre all   ;"

    self.specific_install_command = "cd petsc-src; make PETSC_DIR=$CURRENT_SOFTWARE_SRC_DIR/petsc-src  PETSC_ARCH=arch-linux2-c-opt-mpi-ml-hypre install ;"

  def init_dependency_list(self):
    self.software_dependency_dict['exec'] = ["PYTHON", "MERCURIAL", "METIS_ASTER", "PARMETIS_ASTER", "SCOTCH_ASTER", "MUMPS"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    if dependency_name == "METIS_ASTER":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "PARMETIS_ASTER":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "SCOTCH_ASTER":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "MUMPS":
      dependency_object.depend_of = ["install_path"]
    return dependency_object

  def get_type(self):
    return "prerequisite"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return petsc_mpi_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return petsc_mpi_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path())

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D AMA)

import os
import string

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.projects.salome.softwares.prerequisites.scotch import SCOTCH

software_name = "SCOTCH_ASTER"

scotch_template = ""
scotch_template = misc.PercentTemplate(scotch_template)

scotch_configuration_template = ""
scotch_configuration_template = string.Template(scotch_configuration_template)

class SCOTCH_ASTER(SCOTCH):

    def __init__(self, name, version, verbose, **kwargs):
 
        version_split = version.split()
        version_split_percent = version_split[0].split("%")
        short_version = version_split_percent[0]
        self.version_suffix = "seq"
        if len(version_split_percent) >= 2:
           self.version_suffix = version_split_percent[1]
        if len(version_split) > 1:
           version = "{0} {1}".format(short_version, version_split[1])
        SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
        self.root_repository_template = string.Template('http://$server')
        self.version = short_version
        self.patch_directory=" "

    def init_variables(self):
        """Define specific values for this distribution of Scotch"""
        super(SCOTCH_ASTER, self).init_variables()

        if not self.project_options.is_software_option(self.name, "occ_server"):
          self.project_options.set_software_option(software_name, "occ_server",
                                               "aster-repo.der.edf.fr/scm/hg/aster-prerequisites/")
        short_version = self.version.split()[0].split("%")[0]
        self.remote_type     = "hg"
        self.repository_name = "scotch"
        self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/aster-prerequisites'
        self.tag             = short_version
        self.build_dir       = "keep_or_create"
        self.shell           = "/bin/bash"
        self.build_directory = self.src_directory
           
        self.specific_configure_command = "cd src ; "
        if self.version_suffix == 'mpi':
    # Test Intel MPI Compiler
           self.specific_configure_command += "isintel=`ifort --help 2>/dev/null` ; "
           self.specific_configure_command += 'if [ "x$isintel" != "x" ]; then '
           self.specific_configure_command += "  sed -i -e 's/= gcc/= mpiicc/g' Makefile.inc ; "
           self.specific_configure_command += "  sed -i -e 's/= mpicc/= mpiicc/g' Makefile.inc ; "
           self.specific_configure_command += "else "
           self.specific_configure_command += "  sed -i -e 's/= gcc/= mpicc/g' Makefile.inc ; "
           self.specific_configure_command += "fi ; "
           self.specific_configure_command += "cd .. ; "
    #
    # Fin Intel MPI Compiler
        self.specific_build_command = "cd src ; make &&  make esmumps "
        if self.version_suffix == 'mpi':
           self.specific_build_command += " && make ptscotch && mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/MPI  "
           self.specific_install_command = "cd src ;  make install prefix=$CURRENT_SOFTWARE_INSTALL_DIR/MPI ;"
        else :
           self.specific_build_command += " && mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/SEQ " 

           self.specific_install_command = "cd src ;  make install prefix=$CURRENT_SOFTWARE_INSTALL_DIR/SEQ ;"
        self.specific_install_command += "cd src ;  hg revert Makefile.inc "
        
    def get_prerequisite_str(self, specific_install_dir=""):
      install_dir = self.install_directory
      if specific_install_dir != "":
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
      return scotch_template.substitute(install_dir=install_dir)

    def get_configuration_str(self, specific_install_dir=""):
      install_dir = self.install_directory
      if specific_install_dir != "":
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
      return scotch_configuration_template.substitute(install_dir=install_dir)

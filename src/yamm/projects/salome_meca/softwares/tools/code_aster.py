#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : J-P LEFEBVRE (EDF R&D)

import os
import string
import platform
import tempfile

from yamm.core.base import misc
from yamm.projects.salome.software import SalomeSoftware
from yamm.projects.salome_meca.templates import code_aster


# software_name = "CODE_ASTER_..."


class AbstractCodeAster(SalomeSoftware):
    """Generic Code_Aster software.
    Must be derivated by defining `TAGNAME = "stable"` for example

    `ordered_version` is used to pass the branch name.
    """

    TAGNAME = None

    def __init__(self, name, version, verbose, **kwargs):
        """Initialisation"""
        SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
        self.root_repository_template = string.Template('http://$server/hg/')
        self.waf_libpath = ""
        self.waf_includes = ""
        self.waf_options = ""
        self.branch = None

    def init_dependency_list(self):
        # called at __init__
        # warning: this order is used later to fill LIBPATH, INCLUDES...
        self.software_dependency_dict['build'] = []
        if self.TAGNAME == "oldstable" :
          self.software_dependency_dict['exec'] = [
            "CODE_ASTER_FRONTEND", "CODE_ASTER_DATA_OLDSTABLE", "PYTHON",
            "MERCURIAL", "NUMPY", "LAPACK", "MEDFICHIER", "HDF5",
            "METIS", "SCOTCH", "MUMPS4", "MFRONT_STABLE", "HOMARD_ASTER",
            "ECREVISSE_OLD","GMSH_BIN", "MISS3D", "GIBI"]
        elif self.TAGNAME == "stable"  :
          self.software_dependency_dict['exec'] = [
            "CODE_ASTER_FRONTEND", "CODE_ASTER_DATA", "PYTHON",
            "MERCURIAL", "NUMPY", "LAPACK", "MEDFICHIER", "HDF5",
            "METIS_ASTER", "SCOTCH_ASTER", "MUMPS", "MFRONT",
            "HOMARD_ASTER", "ECREVISSE","GMSH_BIN", "MISS3D", "GIBI"]
        else :
          self.software_dependency_dict['exec'] = [
            "CODE_ASTER_FRONTEND", "CODE_ASTER_DATA_TESTING", "PYTHON",
            "MERCURIAL", "NUMPY", "LAPACK", "MEDFICHIER", "HDF5",
            "METIS_ASTER", "SCOTCH_ASTER", "MUMPS", "MFRONT",
            "HOMARD_ASTER", "ECREVISSE","GMSH_BIN", "MISS3D", "GIBI"]

    def init_variables(self):
        """Define Code_Aster variables"""
        # called at step 6
        SalomeSoftware.init_variables(self)

        if not self.project_options.is_software_option(self.name, "occ_server"):
            self.project_options.set_software_option(
                self.name, "occ_server", "aster-repo.der.edf.fr/scm")

        self.archive_type = "tar.gz"
        self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
        self.tag = self.version
        branch = self.branch = self.ordered_version
        if not branch or (branch != "default" and not branch.startswith("v")):
            raise ValueError("Code_Aster software: expecting branch name "
                "(default or vNN) as 'ordered_version', not %r" % branch)

        self.remote_type = "hg"
        self.repository_name = "codeaster/src"
        self.root_repository = self.root_repository_template.substitute(
            server=self.project_options.get_option(self.name, "occ_server"))

        self.compil_type = "specific"
        self.build_dir = "keep_or_create"
        self.build_directory = self.src_directory
        self.shell = "/bin/bash"

        # Configure options
        self.specific_configure_command += self._substitute(code_aster.configure_command,
            waf_libpath=self.waf_libpath,
            waf_includes=self.waf_includes,
            waf_options=self.waf_options)

        # Build
        self.specific_build_command += self._substitute(code_aster.build_command)

        # Install
        self.specific_install_command = self._substitute(code_aster.install_command)


        post_command = self._substitute(code_aster.post_command)
        self.specific_install_command += post_command

        # Stocke le fichier pour postinstall
        temp_filename = tempfile.NamedTemporaryFile().name
        open(temp_filename, "wb").write("#!%s\n" % self.shell + post_command)
        os.chmod(temp_filename, 0o755)

        self.specific_install_command += self._substitute(code_aster.post_inst_command,
                                                          post_filename=temp_filename)


    def get_type(self):
        """Return the type of software"""
        return "tool"

    def get_dependency_object_for(self, dependency_name):
        # called at step 2
        dependency_object = SalomeSoftware.get_dependency_object_for(
            self, dependency_name)
        if dependency_name == "CODE_ASTER_FRONTEND":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "CODE_ASTER_DATA":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "CODE_ASTER_DATA_TESTING":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "CODE_ASTER_DATA_OLDSTABLE":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "PYTHON":
            dependency_object.depend_of = [
                "path", "ld_lib_path", "python_version", "install_path"]
        if dependency_name == "NUMPY":
            dependency_object.depend_of = ["python_path"]
        if dependency_name == "LAPACK":
            dependency_object.depend_of = ["ld_lib_path"]
        if dependency_name == "HDF5":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "MEDFICHIER":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "METIS_ASTER":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "MUMPS":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "SCOTCH_ASTER":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "METIS":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "MUMPS4":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "SCOTCH":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "HOMARD":
            dependency_object.depend_of = ["install_path"]
        if dependency_name == "MFRONT":
            dependency_object.depend_of = ["install_path", "path", "ld_lib_path"]
        if dependency_name == "MFRONT_STABLE":
            dependency_object.depend_of = ["install_path", "path", "ld_lib_path"]
        return dependency_object

    def update_configuration_with_dependency(self, dependency_name, version_name):
        """Called for each dependency with its ordered_version"""
        # called at step 2
        if dependency_name == "LAPACK":
            self._insert_wafopts(dependency_name,
                "",
                "$LAPACKDIR/lib",
                "")
        if dependency_name == "PYTHON":
            self._insert_wafopts(dependency_name,
                "",
                "$PYTHONHOME/lib",
                "$PYTHON_INCLUDE")
        if dependency_name == "HDF5":
            self._insert_wafopts(dependency_name,
                "--enable-hdf5 --hdf5-libs=hdf5",
                "$HDF5_INSTALL_DIR/lib",
                "$HDF5_INSTALL_DIR/include")
        if dependency_name == "MEDFICHIER":
            self._insert_wafopts(dependency_name,
                '--enable-med --embed-med --med-libs="med gfortran stdc++"',
                "$MEDFICHIER_INSTALL_DIR/lib",
                "$MEDFICHIER_INSTALL_DIR/include")
        if dependency_name == "MUMPS":
            self._insert_wafopts(dependency_name,
                "--enable-mumps --embed-mumps",
                "$MUMPS_INSTALL_DIR/SEQ/lib $MUMPS_INSTALL_DIR/SEQ/libseq",
                "$MUMPS_INSTALL_DIR/SEQ/include $MUMPS_INSTALL_DIR/SEQ/include_seq")
        if dependency_name == "MUMPS4":
            self._insert_wafopts(dependency_name,
                "--enable-mumps --embed-mumps --mumps-version=%s" % version_name,
                "$MUMPS4_INSTALL_DIR/SEQ/lib $MUMPS4_INSTALL_DIR/SEQ/libseq", "")
        if dependency_name == "METIS_ASTER":
            self._insert_wafopts(dependency_name,
                "--enable-metis --embed-metis",
                "$METIS_ASTER_INSTALL_DIR/lib",
                "$METIS_ASTER_INSTALL_DIR/include")
        if dependency_name == "METIS":
            self._insert_wafopts(dependency_name,
                "--enable-metis --embed-metis",
                "$METIS_INSTALL_DIR",
                "$METIS_INSTALL_DIR/Lib")
        if dependency_name == "SCOTCH_ASTER":
            self._insert_wafopts(dependency_name, "--enable-scotch --embed-scotch",
            "$SCOTCH_ASTER_INSTALL_DIR/SEQ/lib",
            "$SCOTCH_ASTER_INSTALL_DIR/SEQ/include")
        if dependency_name == "SCOTCH":
            self._insert_wafopts(dependency_name, "--enable-scotch --embed-scotch",
            "$SCOTCH_INSTALL_DIR/lib",
            "$SCOTCH_INSTALL_DIR/include")
        if dependency_name == "PETSC_MPI":
            # this will override --disable-petsc by default
            self._insert_wafopts(dependency_name, "--enable-petsc",
            "",
            "")
        if dependency_name == "MFRONT":
            self._insert_wafopts(dependency_name, "--enable-mfront",
            "$MFRONT_INSTALL_DIR/lib",
            "$MFRONT_INSTALL_DIR/include")
        if dependency_name == "MFRONT_STABLE":
            self._insert_wafopts(dependency_name, "--enable-mfront",
            "$MFRONT_STABLE_INSTALL_DIR/lib",
            "$MFRONT_STABLE_INSTALL_DIR/include")

    def get_prerequisite_str(self, specific_install_dir=""):
        """Add environment to salome_prerequisites.sh"""
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(
                specific_install_dir, self.executor_software_name)
        return self._substitute(code_aster.environment_shell,
                                install_dir=install_dir)

    def get_configuration_str(self, specific_install_dir=""):
        """Add environment to salome_context.cfg"""
        install_dir = self.install_directory
        if specific_install_dir != "":
            install_dir = os.path.join(
                specific_install_dir, self.executor_software_name)
        return self._substitute(code_aster.environment_cfg,
                                install_dir=install_dir)

    def _substitute(self, template, **kwargs):
        """Performs the template substitution"""
        # this will force to always use the same variables in templates
        values = {
            'version': self.version,
            # keep only the first two digits for the version name
            'shortTag': ".".join(self.version.split(".")[:2]),
            'tagname': self.TAGNAME.lower(),
            'TAGNAME': self.TAGNAME.upper(),
            'branch': self.ordered_version,
        }
        values.update(kwargs)
        return template.substitute(**values)

    def _insert_wafopts(self, software, options, libpath, includes):
        """Insert waf options in `specific_configure_command`"""
        self.waf_libpath += " " + libpath
        self.waf_includes += " " + includes
        self.waf_options += " " + options

    def get_extra_test_path_list(self):
       """
       This method returns a list of paths to the software extra tests.
       Define each path relative to the software install directory.
       """
       return ["share/aster"]

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string
import platform

software_name = "CODE_ASTER_DATA"
aster_data_template = ''
aster_data_template = misc.PercentTemplate(aster_data_template)

aster_data_configuration_template = ''
aster_data_configuration_template = string.Template(aster_data_configuration_template)

class CODE_ASTER_DATA(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.root_repository_template = string.Template('http://$server/hg/')

  def init_variables(self):
    SalomeSoftware.init_variables(self)

    if not self.project_options.is_software_option(self.name, "occ_server"):
      self.project_options.set_software_option(software_name, "occ_server", "aster-repo.der.edf.fr/scm")

    self.archive_type    = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type     = "hg"
    self.repository_name = "codeaster/data"
    self.root_repository   = self.root_repository_template.substitute(
                             server=self.project_options.get_option(self.name, "occ_server"))
    self.tag             = self.version

    self.compil_type       = "specific"
    self.build_dir         = "keep_or_create"
    self.build_directory   = self.src_directory

    self.specific_install_command = " cd ${CURRENT_SOFTWARE_INSTALL_DIR}/ ; cp -r  ${CURRENT_SOFTWARE_SRC_DIR}/datg datg ; cp -r  ${CURRENT_SOFTWARE_SRC_DIR}/materiau materiau ; cd ${CURRENT_SOFTWARE_INSTALL_DIR} ; "

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_data_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_data_configuration_template.substitute(install_dir=install_dir)

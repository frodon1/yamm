#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011, 2014 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Authors : Guillaume DROUET & Jean-Pierre LEFEBVRE (EDF R&D AMA)

from yamm.projects.salome_meca.softwares.tools.code_aster_data import CODE_ASTER_DATA

software_name = "CODE_ASTER_DATA_TESTING"

class CODE_ASTER_DATA_TESTING(CODE_ASTER_DATA):
   
    def init_variables(self):
        """Define specific values for this distribution of CODE_ASTER_DATA_TESTING"""
        super(CODE_ASTER_DATA_TESTING, self).init_variables()

    	self.remote_type = "hg"
    	self.repository_name = "codeaster/data"
    	self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/'
    	self.tag = self.version


#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string
import os
import tempfile

software_name = "CODE_ASTER_FRONTEND"
aster_template = """
#------ codeaster-frontend ------
export ASTER_ROOT="%install_dir"
export PATH=${ASTER_ROOT}/bin:${PATH}
export LD_LIBRARY_PATH=${ASTER_ROOT}/lib:$LD_LIBRARY_PATH
export PYTHONPATH=${ASTER_ROOT}/lib/python%python_version/site-packages:$PYTHONPATH
"""
aster_template = misc.PercentTemplate(aster_template)

aster_configuration_template = """
#------ codeaster-frontend ------
ASTER_ROOT="$install_dir"
ADD_TO_PATH: %(ASTER_ROOT)s/bin
ADD_TO_LD_LIBRARY_PATH: %(ASTER_ROOT)s/lib
ADD_TO_PYTHONPATH: %(ASTER_ROOT)s/lib/python$python_version/site-packages
"""
aster_configuration_template = string.Template(aster_configuration_template)

class CODE_ASTER_FRONTEND(SalomeSoftware):

  def __init__(self, name, version, verbose, **kwargs):
    SalomeSoftware.__init__(self, name, version, verbose, **kwargs)
    self.root_repository_template = string.Template('http://$server/hg/')

  def init_variables(self):
    SalomeSoftware.init_variables(self)
    if not self.project_options.is_software_option(self.name, "occ_server"):
      self.project_options.set_software_option(software_name, "occ_server", "aster-repo.der.edf.fr/scm")

    self.archive_type = "tar.gz"

    self.remote_type = "hg"
    self.repository_name = "aster/codeaster-frontend"
    self.root_repository = self.root_repository_template.substitute(
                             server=self.project_options.get_option(self.name, "occ_server"))
    self.tag = self.version
#    if self.version == "trunk":
#      self.tag = ""

    self.parallel_make = "1"

    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory

    self.specific_install_command = "python setup.py install --prefix=$CURRENT_SOFTWARE_INSTALL_DIR ;"
# File etc/codeaster/profile.sh

    post = """ """

    self.specific_install_command += post

    # Stocke le fichier postinstall
    (fd, temp_filename) = tempfile.mkstemp()
    fw = open(temp_filename, "w")
    fw.write("#!/bin/bash")
    fw.write("\n" + post)
    fw.close()
    os.chmod(temp_filename, 0o755)
    self.specific_install_command += "cp %s ${CURRENT_SOFTWARE_BUILD_DIR}/postinstall.sh ;" % temp_filename
    self.specific_install_command += "rm -f %s ;" % temp_filename


  def get_type(self):
    return "tool"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON",]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]


    return dependency_object

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return aster_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

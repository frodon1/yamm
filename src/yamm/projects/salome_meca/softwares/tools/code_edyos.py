#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.engine.dependency import Dependency
import os
import string

software_name = "CODE_EDYOS"
code_edyos_template = """
#------ edyos ------
export EDYOS_ROOT=%install_dir     # export is necessary for generation with yacsgen
export PATH=${EDYOS_ROOT}/bin:$PATH
export LD_LIBRARY_PATH=${EDYOS_ROOT}/lib:$LD_LIBRARY_PATH
"""
code_edyos_template = misc.PercentTemplate(code_edyos_template)

code_edyos_configuration_template = """
#------ code_edyos ------
EDYOS_ROOT=$install_dir
ADD_TO_PATH: %(EDYOS_ROOT)s/bin
ADD_TO_LD_LIBRARY_PATH: %(EDYOS_ROOT)s/lib
"""
code_edyos_configuration_template = string.Template(code_edyos_configuration_template)

class CODE_EDYOS(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"

    self.repository_name = "edyos"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca-prerequisites'
    self.tag = self.version

    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory
    self.shell = "/bin/bash"

    parallel_make = '1'  # EDYOS ne supporte pas la compilation parallele


    # Ligne d'arguments du configure
    # ------------------------------
    self.specific_configure_command += ""


    # Ligne d'arguments du build
    # --------------------------
    self.specific_build_command = " "
    self.specific_build_command += "( "
    self.specific_build_command += "make clean ; "

    # Intel Compiler
    self.specific_build_command += "isintel=`ifort --help 2>/dev/null` ; "
#    self.specific_build_command     += "isintel=`` ; "
    self.specific_build_command += 'if [ "x$isintel" != "x" ]; then '  # si l'environnement Intel est source...
    self.specific_build_command += "IFORT=`which ifort` ; "
    #   Mkl
    self.specific_build_command += 'if [ "x${MKL_TARGET_ARCH}" != "x" ]; then '
    self.specific_build_command += "MKLDIR=${MKLROOT}/lib/${MKL_TARGET_ARCH} ; "
    self.specific_build_command += 'else '
    self.specific_build_command += "MKLDIR=`echo $LD_LIBRARY_PATH | awk -F':' '{for (i = 1; i < (NF); i++) {print $i} }' | grep 'mkl' | head -1` ; "
    self.specific_build_command += "fi ;"
#    self.specific_build_command     += "make FC=${IFORT} FFLAGS='-fPIC -O3' KERNEL_ROOT_DIR=${KERNEL_ROOT_DIR} LIB_LAP=${MKLDIR} BIB_LAPACK='-Wl,--start-group $(LIB_LAP)/libmkl_intel_lp64.a $(LIB_LAP)/libmkl_blas95_lp64.a $(LIB_LAP)/libmkl_lapack95_lp64.a $(LIB_LAP)/libmkl_sequential.a $(LIB_LAP)/libmkl_core.a -Wl,--end-group -lpthread -lm' -j " + parallel_make + " all ; "
    self.specific_build_command += "make FC=${IFORT} FFLAGS='-fPIC -O3' KERNEL_ROOT_DIR=${KERNEL_ROOT_DIR} LIB_LAP=${MKLDIR} BIB_LAPACK='-Wl,--start-group -L $(LIB_LAP) -lmkl_intel_lp64 -lmkl_blas95_lp64 -lmkl_lapack95_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread -lm' -j " + parallel_make + " all ; "

    # Blas/lapack systeme
    self.specific_build_command += 'elif [ "x$BLAS_LIBS$LAPACK_LIBS" != "x" ] ; then '  # si BLAS_LIBS et LAPACK_LIBS sont definis
    self.specific_build_command += "make GFORTRAN=1 KERNEL_ROOT_DIR=${KERNEL_ROOT_DIR} BIB_LAPACK='-Wl,--start-group ${BLAS_LIBS} ${LAPACK_LIBS} -Wl,--end-group -lpthread -lm' -j " + parallel_make + " all ; "

    # Blas/lapack Salome
    self.specific_build_command += "else "  # si BLAS_LIBS et LAPACK_LIBS ne sont pas definis
    self.specific_build_command += "make GFORTRAN=1 KERNEL_ROOT_DIR=${KERNEL_ROOT_DIR} -j " + parallel_make + " all ; "

    self.specific_build_command += "fi "
    self.specific_build_command += ") "

    # Ligne d'arguments du install
    # ----------------------------
    self.specific_install_command = "( "
    self.specific_install_command += "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/lib ; "
    self.specific_install_command += "mkdir -p $CURRENT_SOFTWARE_INSTALL_DIR/bin ; "
    self.specific_install_command += "cp -ax $CURRENT_SOFTWARE_BUILD_DIR/bin/* $CURRENT_SOFTWARE_INSTALL_DIR/bin/. ; "
    self.specific_install_command += "cp -ax $CURRENT_SOFTWARE_BUILD_DIR/lib/* $CURRENT_SOFTWARE_INSTALL_DIR/lib/. ; "

    hg_command = "hg"
    if misc.get_calibre_version() == "7":
      hg_command = "compat-calibre9 hg"

    self.specific_install_command += ") "
    self.specific_install_command += ") "
    self.specific_install_command += " ; "
    self.specific_install_command += "( "
    self.specific_install_command += "cd {0} ; ".format(self.src_directory)
    self.specific_install_command += "%s update ; " %hg_command  # necessaire car le make modifie des sources !
    self.specific_install_command += "%s up -C %s ; " % (hg_command, self.tag)


  def get_type(self):
    return "tool"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["INTEL_RUNTIME", "BLAS", "LAPACK", "KERNEL", ]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "LAPACK":
      # We link against static libraries, which is not very clean nor portable, but at
      # least it works and it loads the user shared libraries at runtime. When trying
      # to link against shared libraries, it links with system libraries instead of
      # user libraries and thus it loads system libraries at runtime (when they exist)
      # or crash if they don't (so this doesn't work with the universal installer).
      dependency_object = []
      dependency_object.append(Dependency(name="LAPACK",
                                          depend_of=["install_path"],
                                          specific_install_var_name="LAPACK_LIBS",
                                          specific_install_var_end="/lib/liblapack.a")
                              )
      dependency_object.append(Dependency(name="LAPACK",
                                          depend_of=["install_path"],
                                          specific_install_var_name="BLAS_LIBS",
                                          specific_install_var_end="/lib/libblas.a")
                              )

    return dependency_object

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return code_edyos_template.substitute(install_dir=install_dir, python_version=self.python_version)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return code_edyos_configuration_template.substitute(install_dir=install_dir, python_version=self.python_version)

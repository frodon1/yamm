#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Jean-Pierre LEFEBVRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string
import platform
import tempfile

software_name = "ECREVISSE"
ECREVISSE_template = """
#------ ECREVISSE  ------
export ECREVISSE_ROOT_DIR="%install_dir"
"""
ECREVISSE_template = misc.PercentTemplate(ECREVISSE_template)

ECREVISSE_configuration_template = """
#------ ECREVISSE  ------
ECREVISSE_ROOT_DIR="$install_dir"
"""
ECREVISSE_configuration_template = string.Template(ECREVISSE_configuration_template)

class ECREVISSE(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.repository_name = "aster-prerequisites/ecrevisse"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg'

    self.tag = self.version
    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory


    self.specific_configure_command = ""
    self.specific_build_command = "cd source ; ln -s Makefile_linux_c7 Makefile; make optim"
    self.specific_install_command = "cd source ; cp -p ecrevisse_linux_* $CURRENT_SOFTWARE_INSTALL_DIR/ecrevisse"    

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ECREVISSE_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return ECREVISSE_configuration_template.substitute(install_dir=install_dir)

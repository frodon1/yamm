#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "EFICAS_ASTER"
eficas_aster_template = """
#------ eficas_aster ------
export PREFS_CATA_ASTER="%install_dir/Aster"
export PYTHONPATH=${PREFS_CATA_ASTER}:${PYTHONPATH}
export PYTHONPATH="%install_dir":${PYTHONPATH}
"""
eficas_aster_template = misc.PercentTemplate(eficas_aster_template)

eficas_aster_configuration_template = """
#------ eficas_aster ------
PREFS_CATA_ASTER="$install_dir/Aster"
ADD_TO_PYTHONPATH=%(PREFS_CATA_ASTER)s
"""
eficas_aster_configuration_template = string.Template(eficas_aster_configuration_template)

class EFICAS_ASTER(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.repository_name = "aster-prerequisites/eficas"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg'
    self.tag = self.version

    self.compil_type = "rsync"


  def get_type(self):
    return "tool"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    # dependance pour que EFICAS_ROOT soit defini apres celui de SALOME
    self.software_dependency_dict['exec'] = ["EFICASV1"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    return dependency_object


  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return eficas_aster_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return eficas_aster_configuration_template.substitute(install_dir=install_dir)

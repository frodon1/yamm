#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "GIBI"
gibi_template = """
#------ gibi ------
export GIBI_DIR=%install_dir     # export necessaire au lien Code_Aster/outils/gibi
export PATH=${GIBI_DIR}:$PATH
"""
gibi_template = misc.PercentTemplate(gibi_template)

gibi_configuration_template = """
#------ gibi ------
GIBI_DIR=$install_dir
ADD_TO_PATH: %(GIBI_DIR)s
"""
gibi_configuration_template = string.Template(gibi_configuration_template)

class GIBI(SalomeSoftware):

  def init_variables(self):

    self.archive_type    = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type     = "hg"
    self.repository_name = "aster-prerequisites/gibi"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg'
    self.tag               = self.version

    self.compil_type       = "specific"
    self.build_dir         = "keep_or_create"


    # Ligne d'arguments du configure
    # ------------------------------
    self.specific_configure_command  = " "

    # Ligne d'arguments du build
    # --------------------------
    self.specific_build_command      = " "

    # Ligne d'arguments du install
    # ----------------------------
    self.specific_install_command    = """
cp -ax * $CURRENT_SOFTWARE_INSTALL_DIR/ ;
chmod 666 $CURRENT_SOFTWARE_INSTALL_DIR/USRDAT ;

# Gibi 2000 sur C7
sed -i "3 a\export TERM=xterm-vi" $CURRENT_SOFTWARE_INSTALL_DIR/gibi2000

"""

  def get_type(self):
    return "tool"

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gibi_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return gibi_configuration_template.substitute(install_dir=install_dir)

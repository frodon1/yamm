#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string
import platform
import tempfile

software_name = "HOMARD_ASTER"
homard_aster_template = """
#------ HOMARD_ASTER  ------
export HOMARD_ASTER_ROOT_DIR="%install_dir"
export PATH=${HOMARD_ASTER_ROOT_DIR}:$PATH
"""
homard_aster_template = misc.PercentTemplate(homard_aster_template)

homard_aster_configuration_template = """
#------ HOMARD_ASTER  ------
HOMARD_ASTER_ROOT_DIR="$install_dir"
ADD_TO_PATH: %(HOMARD_ASTER_ROOT_DIR)s
"""
homard_aster_configuration_template = string.Template(homard_aster_configuration_template)

class HOMARD_ASTER(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.repository_name = "aster-prerequisites/homard"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg'

    self.tag = self.version
    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory

    self.specific_install_command = "python setup_homard.py --prefix=$CURRENT_SOFTWARE_INSTALL_DIR"

  def get_type(self):
    return "tool"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["PYTHON"]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "PYTHON":
      dependency_object.depend_of = ["path", "ld_lib_path", "python_version", "install_path"]
    return dependency_object

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return homard_aster_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return homard_aster_configuration_template.substitute(install_dir=install_dir)

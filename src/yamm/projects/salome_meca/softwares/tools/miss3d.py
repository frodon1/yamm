#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)

from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
from yamm.core.engine.dependency import Dependency
import os
import string

software_name = "MISS3D"
miss3d_template = """
#------ miss3d ------
export MISS3D_DIR=%install_dir     # export necessaire a Code_Aster/outils/run_miss3d
export PATH=${MISS3D_DIR}:$PATH
"""
miss3d_template = misc.PercentTemplate(miss3d_template)

miss3d_configuration_template = """
#------ miss3d ------
MISS3D_DIR=$install_dir
ADD_TO_PATH: %(MISS3D_DIR)s
"""
miss3d_configuration_template = string.Template(miss3d_configuration_template)

class MISS3D(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.repository_name = "miss3d"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/aster-prerequisites'
    self.tag = self.version

    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.shell = "/bin/bash"
    self.build_directory = self.src_directory


    # Ligne d'arguments du configure
    # ------------------------------
    self.specific_configure_command = " cp -ax src/Makefile.inc src/Makefile.inc.orig ; "  # needed for avoiding hg differences

    # Intel Compiler
    self.specific_configure_command += "isintel=`ifort --help 2>/dev/null` ; "
    self.specific_configure_command += 'if [ "x$isintel" != "x" ]; then '  # warning : need "" inside if statement
    self.specific_configure_command += "  v=`ifort -v  2>&1| sed -e 's/ifort//' | awk '{print $2}'| awk -F'.' '{print $1}'` ;"
    self.specific_configure_command += "  if [ $v -lt 14 ]; then "
    self.specific_configure_command += "    cp -ax src/Makefile.inc.intel64.SSE4.2 src/Makefile.inc ;"
    self.specific_configure_command += "  else"
    self.specific_configure_command += "    cp -ax  src/Makefile.inc.intel64.AVX   src/Makefile.inc ;"
    self.specific_configure_command += "  fi ;"
    # Si Blas/Lapack
    self.specific_configure_command += "else "
    self.specific_configure_command += "  VEROS=`file /usr/bin/file  | awk -F'-' '{print $1}' | awk '{print $3}'`;"
    self.specific_configure_command += "  VERGCC=`gcc --version | head -1 | awk '{print $4}'` ; "
    self.specific_configure_command += "  suff='' ;"
    self.specific_configure_command += '  if [ "x$VERGCC" ==  "x4.9.2" ]; then '
    self.specific_configure_command += '     suff=".gcc-4.9.2" ;'
    self.specific_configure_command += "  fi ;"
    self.specific_configure_command += "  cp -ax src/Makefile.inc.gnu$VEROS$suff src/Makefile.inc ; "
    self.specific_configure_command += '  if [ ! -z $LAPACKDIR ]; then sed -i "s#^LDFLAGS =.*#LDFLAGS = -L$LAPACKDIR/lib -llapack -lblas -lpthread#g" src/Makefile.inc ; fi ; '
    self.specific_configure_command += "fi ; "


    # Ligne d'arguments du build
    # --------------------------
    # si Intel et MKLROOT non defini (Ivanoe)
    self.specific_build_command = "isintel=`ifort --help 2>/dev/null` ; "
    self.specific_build_command += 'if [ "x$isintel" != "x" ]; then '  # warning : need "" inside if statement
    self.specific_build_command += "if [ -z $MKLROOT ]; then "
    self.specific_build_command += "MKL=`echo $LD_LIBRARY_PATH | awk -F':' '{for (i = 1; i < (NF); i++) {print $i} }' | grep 'mkl' | head -1 | awk -F'mkl/' '{print $1}'` ; "
    self.specific_build_command += 'export MKLROOT="${MKL}/mkl" ; '
    self.specific_build_command += "if [ ! -d $MKLROOT ] ; then exit 1 ; fi ; "
    self.specific_build_command += "fi ; "
    self.specific_build_command += "fi ; "
    # Make
    self.specific_build_command += "make ; "


    # Ligne d'arguments du install
    # ----------------------------
    self.specific_install_command = "make -j" + self.parallel_make + " install prefix=$CURRENT_SOFTWARE_INSTALL_DIR ; "
    self.specific_install_command += "cp -ax src/Makefile.inc.orig src/Makefile.inc ; "    # restore original (for hg diff)

    # Ajout du lien dans le repertoire outil de Code_Aster
    self.specific_install_command += "if [ ! -z $CODE_ASTER_FRONTEND_INSTALL_DIR ]; then ln -s $CURRENT_SOFTWARE_INSTALL_DIR/miss3d.csh $CODE_ASTER_FRONTEND_INSTALL_DIR/outils/miss3d ; fi ; "

  def get_type(self):
    return "tool"

  def init_dependency_list(self):
    self.software_dependency_dict['build'] = []
    self.software_dependency_dict['exec'] = ["MERCURIAL", "LAPACK", ]

  def get_dependency_object_for(self, dependency_name):
    dependency_object = SalomeSoftware.get_dependency_object_for(self, dependency_name)
    if dependency_name == "CODE_ASTER_FRONTEND":
      dependency_object.depend_of = ["install_path"]
    if dependency_name == "LAPACK":
      # We link against static libraries, which is not very clean nor portable, but at
      # least it works and it loads the user shared libraries at runtime. When trying
      # to link against shared libraries, it links with system libraries instead of
      # user libraries and thus it loads system libraries at runtime (when they exist)
      # or crash if they don't (so this doesn't work with the universal installer).
      dependency_object = []
      dependency_object.append(Dependency(name="LAPACK",
                                          depend_of=["install_path"],
                                          specific_install_var_name="LAPACK_LIBS",
                                          specific_install_var_end="/lib/liblapack.a")
                              )
      dependency_object.append(Dependency(name="LAPACK",
                                          depend_of=["install_path"],
                                          specific_install_var_name="BLAS_LIBS",
                                          specific_install_var_end="/lib/libblas.a")
                              )
    return dependency_object

  def get_prerequisite_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return miss3d_template.substitute(install_dir=install_dir)

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return miss3d_configuration_template.substitute(install_dir=install_dir)

#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Aimery ASSIRE (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import os
import string

software_name = "SALOMEMECA_EXTRA"

salomemeca_extra_configuration_template = """
#------ SALOMEMECA_EXTRA ----
SALOMEMECA_EXTRA_ROOT_DIR=$install_dir
OPENTURNS_MODULE_CONF_FILE=%(SALOMEMECA_EXTRA_ROOT_DIR)s/custom/config_file_template.py
OMNIORB_USERS_PATH=/tmp
"""

salomemeca_extra_configuration_template = string.Template(salomemeca_extra_configuration_template)


class SALOMEMECA_EXTRA(SalomeSoftware):

  def init_variables(self):

    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"
    self.archive_type = "tar.gz"

    #self.parallel_make     = "1"

    self.remote_type = "hg"
    self.repository_name = "salome-meca-extra"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca-prerequisites'
    self.tag = self.version

    self.compil_type = "specific"
    self.build_dir = "keep_or_create"
    self.build_directory = self.src_directory

    self.specific_configure_command = ""
    self.specific_build_command = ""
    self.specific_install_command = "cp -ax $CURRENT_SOFTWARE_BUILD_DIR/custom $CURRENT_SOFTWARE_INSTALL_DIR/ ; "
#    self.specific_install_command   += "if [ -d $CURRENT_SOFTWARE_BUILD_DIR/validation ]; then cp -ax $CURRENT_SOFTWARE_BUILD_DIR/validation $CURRENT_SOFTWARE_INSTALL_DIR/ ; fi ; "

  def get_type(self):
    return "tool"

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return salomemeca_extra_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version, salomemeca_version=self.version)

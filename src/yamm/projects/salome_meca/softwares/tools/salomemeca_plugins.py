#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : André RIBES (EDF R&D)


from yamm.projects.salome.software import SalomeSoftware
from yamm.core.base import misc
import string
import os

software_name = "SALOMEMECA_PLUGINS"


salomemeca_plugins_configuration_template = """
#------ SALOMEMECA_PLUGINS -------
SALOMEMECA_PLUGINS_ROOT_DIR=$install_dir
ADD_TO_PYTHONPATH: %(SALOMEMECA_PLUGINS_ROOT_DIR)s/lib/python$python_version/site-packages/salome
ADD_TO_SALOME_PLUGINS_PATH=%(SALOMEMECA_PLUGINS_ROOT_DIR)s/lib/python$python_version/site-packages/salome
PREFS_CATA_CF=%(SALOMEMECA_PLUGINS_ROOT_DIR)s/lib/python$python_version/site-packages/salome/CF
ADD_TO_PYTHONPATH: %(PREFS_CATA_CF)s
"""

salomemeca_plugins_configuration_template = string.Template(salomemeca_plugins_configuration_template)


class SALOMEMECA_PLUGINS(SalomeSoftware):

  def init_variables(self):

    self.archive_type = "tar.gz"
    self.archive_file_name = self.name + "_SRC_" + self.version + ".tgz"

    self.remote_type = "hg"
    self.repository_name = "salome-meca-plugins"
    self.root_repository = 'http://aster-repo.der.edf.fr/scm/hg/smeca'

    self.tag = self.version

    if "integr" in self.version:

        self.repository_name = ""
        self.root_repository = 'http://localhost:10008'
        self.tag=self.version.split('integr_')[1]


    self.compil_type = "python"

  def get_type(self):
    return "tool"

  def get_configuration_str(self, specific_install_dir=""):
    install_dir = self.install_directory
    if specific_install_dir != "":
      if specific_install_dir == "${PREREQUISITES_ROOT_DIR}":
        install_dir = os.path.join(specific_install_dir, "..", "modules", self.executor_software_name)
      else:
        install_dir = os.path.join(specific_install_dir, self.executor_software_name)
    return salomemeca_plugins_configuration_template.substitute(install_dir=install_dir, ld_library_path=misc.get_ld_library_path(), python_version=self.python_version)

  def get_extra_test_path_list(self):
    """
    This method returns a list of paths to the software extra tests.
    Define each path relative to the software install directory.
    """
    return ["bin/salome/test"]

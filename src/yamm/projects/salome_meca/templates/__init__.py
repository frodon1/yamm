#!/usr/bin/env python
# coding: utf-8
#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from yamm.core.base.misc import PercentTemplate, PoundTemplate
import string

empty = string.Template("")

# run during the installer construction to customize the appli
custom_appli = PercentTemplate("""
# =============================================================================
# added for Salome-Meca. Template custom_appli

# write salomemeca_custom_appli.py near create_appli.sh
cat << EOF_SMECA > %{installer_directory}/salomemeca_custom_appli.py
%{custom_appli_py}
EOF_SMECA
chmod 755 %{installer_directory}/salomemeca_custom_appli.py

# create_appli.sh will execute salomemeca_custom_appli.py
sed -i -e 's:^#APPLI_CUSTOM_COMMANDS.*:python "\${SALOMEDIR}"/salomemeca_custom_appli.py "\${APPLIDIR}":g' \
    %{installer_directory}/create_appli.sh

# =============================================================================
""")

# will be called by create_appli.sh
custom_appli_py = PoundTemplate("""
#!/usr/bin/env python

# Postinstall script for appli

import os
import sys

# Exclusions de modules
lexcludes = [
  'HEXABLOCK', 'HEXABLOCKPLUGIN',
  'HELLO', 'PYHELLO',
  'LIGHT', 'PYLIGHT',
  'ATOMIC', 'ATOMGEN', 'ATOMSOLV',
  'GENERICSOLVER',
]

if len(sys.argv) < 2:
    print "exactly one argument is expected (APPLIDIR)"
    sys.exit()

appli_directory = sys.argv[1]
if not os.path.isdir(appli_directory):
    print "Directory %s doesn't exists" % appli_directory
    sys.exit()

# -------------------------------
# Customisation du SalomeApp.xml
# -------------------------------
SalomeApp=os.path.join(appli_directory, "SalomeApp.xml")

# TODO : utiliser une bibliotheque de manipulation de fichiers XML

# Splash-screen, barre de titre
try:
    f=open(SalomeApp, "r")
    txt=f.read()

    if "SALOMEMECA_EXTRA_ROOT_DIR" not in txt:
        txt = txt.replace("<document>", '''<document>
  <section name="resources">
    <parameter name="LightApp"  value="%SALOMEMECA_EXTRA_ROOT_DIR%/custom"/>
  </section>
  <section name="splash" >
    <!-- Splash screen settings -->
    <parameter name="image" value="\${SALOMEMECA_EXTRA_ROOT_DIR}/custom/salomemeca.png" />
  </section>''')

    lines = []
    for line in txt.split(os.linesep):
        # Si on trouve la ligne des modules et on supprime les modules exclus dans Salome-Meca
        if '<parameter name="modules"' in line:
            lmodules = line.split("=")[2].split('"')[1].split(',')
            for m in lexcludes:
                if m in lmodules:
                    lmodules.remove(m)
            line = '    <parameter name="modules" value="%s"/>' % ','.join(lmodules)
        lines.append(line)
    txt = os.linesep.join(lines)

    print "Adjusting the file : %s" % SalomeApp
    open(SalomeApp, "wb").write(txt)
except Exception, exc:
    print exc
""")

# run by the installer after the installation for a package of prerequisites
post_install_prerequisites = PoundTemplate("""
# =============================================================================
# added for Salome-Meca. Template post_install_prerequisites

. ${TARGETDIR}/£{package}/salome_prerequisites.sh
${TARGETDIR}/£{package}/salome_post_install.py


# changement des droits sur le fichier USRDAT pour gibi
#
find ${TARGETDIR}/£{package}/tools/Gibi* -name "USRDAT" -exec chmod 666 {} \;

# Repertoire d'installation de Salome
SALOME_HOME=${TARGETDIR}/£{base_installer_directory}

# ASTER_ROOT
ASTER_ROOT=`find ${SALOME_HOME}/tools/ -maxdepth 1 -mindepth 1 -type d -name "Code_aster_frontend*" |tail -1`
REPOUT="${ASTER_ROOT}/outils"

WISHEXECUTABLE=`which wish8.5`
find ${ASTER_ROOT} -type f -name "profile.sh" -exec sed -i "s#^export WISHEXECUTABLE=.*#export WISHEXECUTABLE=$WISHEXECUTABLE#g" {} \;
find ${ASTER_ROOT} -type f -name "profile.sh" -exec echo "Adjusting wishexecutable {}" \;

EDITOR=`which gedit`
if [ $? -gt 0 ];  then
   EDITOR="/usr/bin/vi"
fi
TERMINAL=`which xterm`

plt=`uname -m`
if [ $plt == "x86_64" ] ; then
   IFDEF="Linux64"
else
   IFDEF="Linux32"
fi

find ${ASTER_ROOT}/etc/codeaster -type f -name "asrun" -exec sed -i -e "s#?EDITOR?#$EDITOR#" -e "s#?TERMINAL?#$TERMINAL -e @E#" -e "s#?IFDEF?#$IFDEF#" {} \;
find ${ASTER_ROOT}/etc/codeaster/astkrc -type f -name "config_serveurs" -exec sed -i -e "s#?EDITOR?#$EDITOR#g" -e "s#?TERMINAL?#$TERMINAL#g" {} \;
find ${ASTER_ROOT}/etc/codeaster/astkrc -type f -name "prefs" -exec sed -i -e "s#?EDITOR?#$EDITOR#" -e "s#?TERMINAL?#$TERMINAL#" {} \;

echo vers : DEV:\$HOME/dev/codeaster/install/std/share/aster >> ${ASTER_ROOT}/etc/codeaster/astkrc/prefs
echo vers : DEV_MPI:\$HOME/dev/codeaster/install/mpi/share/aster >> ${ASTER_ROOT}/etc/codeaster/astkrc/prefs

# Patch pour Calibre 9 : rep_trav : /tmp --> /local00/tmp

if [ `egrep "Calibre 9" /etc/issue | wc -l` -eq 1 ] ; then
    rm -rf ${ASTER_ROOT}/etc/codeaster/profile_local.sh
    echo export ASTER_TMPDIR=/local00/tmp >> ${ASTER_ROOT}/etc/codeaster/profile_local.sh
    echo "Adjusting ASTER_TMPDIR for Calibre work station"
fi

# ---------------------------------------

# =============================================================================
""")

# run by the installer after the installation when an appli is created
post_install_appli = PoundTemplate("""
#!/bin/bash
# added for Salome-Meca. Template post_install_appli

# Repertoire d'installation de Salome
SALOME_HOME=${TARGETDIR}/£{base_installer_directory}

. ${TARGETDIR}/£{package}/salome_prerequisites.sh
. ${TARGETDIR}/£{package}/salome_modules.sh

# check required variables/directories
if [ ! -d "${SALOME_HOME}" ]; then
    echo "no such directory SALOME_HOME: ${SALOME_HOME}"
    exit 1
fi
if [ ! -d "${APPLIDIR}" ]; then
    echo "no such directory APPLIDIR: ${APPLIDIR}"
    exit 1
fi


# ASTER_ROOT
ASTER_ROOT=`find ${SALOME_HOME}/tools/ -maxdepth 1 -mindepth 1 -type d -name "Code_aster_frontend*" |tail -1`

# Initialize env.d/envLocal.cfg and its main section
# (only really used for Calibre for the moment)
if [ -d "${APPLIDIR}/env.d" ]; then
    envLocal=${APPLIDIR}/env.d/envLocal.cfg
    echo "[SALOME Configuration]" > ${envLocal}
    # to avoid warning about empty section
    echo "__place_holder__ = 0" >> ${envLocal}
fi

# Patch pour C7 : rep_trav : /tmp --> /local00/tmp
if grep -q "Calibre" "/etc/issue" ; then
    tmpdir=/local00/tmp
    if [ -d "${tmpdir}" ] && [ -f "${envLocal}" ]; then
        perm=$(stat -c %a "${tmpdir}")
        if [ "${perm}" == "777" ] || [ "${perm}" == "1777" ]; then
            cat << EOF >> ${envLocal}

# used by asrun
SALOME_TMP_DIR="${tmpdir}"
TMP_DIR="${tmpdir}"
ASTER_TMPDIR="${tmpdir}"
EOF

        fi
    fi
fi


WISHEXECUTABLE=`find ${SALOME_HOME} -name "wish8.5"`
find ${ASTER_ROOT} -type f -name "profile.sh" -exec sed -i "s#^export WISHEXECUTABLE=.*#export WISHEXECUTABLE=$WISHEXECUTABLE#g" {} \;
find ${ASTER_ROOT} -type f -name "profile.sh" -exec echo "Adjusting wishexecutable {}" \;

EDITOR=`which gedit`
if [ $? -gt 0 ];  then
   EDITOR="/usr/bin/vi"
fi
TERMINAL=`which xterm`

plt=`uname -m`
if [ $plt == "x86_64" ] ; then
   IFDEF="Linux64"
else
   IFDEF="Linux32"
fi

find ${ASTER_ROOT}/etc/codeaster -type f -name "asrun" -exec sed -i -e "s#?EDITOR?#$EDITOR#" -e "s#?TERMINAL?#$TERMINAL -e @E#" -e "s#?IFDEF?#$IFDEF#" {} \;
find ${ASTER_ROOT}/etc/codeaster/astkrc -type f -name "config_serveurs" -exec sed -i -e "s#?EDITOR?#$EDITOR#g" -e "s#?TERMINAL?#$TERMINAL#g" {} \;
find ${ASTER_ROOT}/etc/codeaster/astkrc -type f -name "prefs" -exec sed -i -e "s#?EDITOR?#$EDITOR#" -e "s#?TERMINAL?#$TERMINAL#" {} \;
echo "Initialize terminal and editor for Code_aster"


# Liens dans  vers salome
if [ ! -z "${APPLIDIR}" ]; then
    if [ -f "${APPLIDIR}/salome" ] && [ ! -z "${ASTER_STABLE}" ]; then
        cd ${ASTER_STABLE}/share/aster
        python -c "import json; d = json.load(open('external_programs.js')); d['salome']='${APPLIDIR}/salome'; json.dump(d, open('external_programs.js', 'wb'))"
        echo "Creating symlink in ${ASTER_STABLE} : salome -> ${APPLIDIR}/salome"
    fi
    if [ -f "${APPLIDIR}/salome" ] && [ ! -z "${ASTER_OLDSTABLE}" ]; then
        cd ${ASTER_OLDSTABLE}/share/aster
        python -c "import json; d = json.load(open('external_programs.js')); d['salome']='${APPLIDIR}/salome'; json.dump(d, open('external_programs.js', 'wb'))"
        echo "Creating symlink in ${ASTER_OLDSTABLE} : salome -> ${APPLIDIR}/salome"
    fi
    if [ -f "${APPLIDIR}/salome" ] && [ ! -z "${ASTER_TESTING}" ]; then
        cd ${ASTER_TESTING}/share/aster
        python -c "import json; d = json.load(open('external_programs.js')); d['salome']='${APPLIDIR}/salome'; json.dump(d, open('external_programs.js', 'wb'))"
        echo "Creating symlink in ${ASTER_TESTING} : salome -> ${APPLIDIR}/salome"
    fi
fi

# Suppression du lien runAppli et remplacement par salome

if [ -f "${APPLIDIR}/runAppli" ]; then
   rm -f ${APPLIDIR}/runAppli
   echo "replace ${APPLIDIR}/runAppli with ${APPLIDIR}/salome"
   ln -s ${APPLIDIR}/salome ${APPLIDIR}/runAppli
fi

# Ajout CatalogResources.xml avec aster5

if  [ -f "${APPLIDIR}/CatalogResources.xml.template" ]; then 
    echo "replace ${APPLIDIR}//CatalogResources.xml"
    cp -f ${APPLIDIR}/CatalogResources.xml.template ${APPLIDIR}/CatalogResources.xml
fi

# Desactivation ASTER Module GUI

if [ -f "${APPLIDIR}/SalomeApp.xml" ]; then
    sed -re 's/,ASTER,/,/' ${APPLIDIR}/SalomeApp.xml> tmp
    mv tmp ${APPLIDIR}/SalomeApp.xml
fi

# Set default vtk window background in GUI

if [ -f "${GUI_ROOT_DIR}/share/salome/resources/gui/LightApp.xml" ]; then
    sed -re 's/parameter name="background"                       value="0, 0, 0"/parameter name="background"                       value="bt=2;c1=#00557F;c2=#FFFFFF;gt=1"/' ${GUI_ROOT_DIR}/share/salome/resources/gui/LightApp.xml > tmp
    mv tmp ${GUI_ROOT_DIR}/share/salome/resources/gui/LightApp.xml
fi

""")

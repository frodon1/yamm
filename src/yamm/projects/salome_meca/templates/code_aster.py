#!/usr/bin/env python
# coding: utf-8
#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#

from yamm.core.base.misc import PercentTemplate, PoundTemplate


configure_command = PoundTemplate("""
# Template configure_command

# For intranet datafiles
if [ £TAGNAME == "STABLE" ]; then
  if [ ! -z $CODE_ASTER_DATA_INSTALL_DIR ]; then
    DATA=$CODE_ASTER_DATA_INSTALL_DIR
  fi
elif [ £TAGNAME == "TESTING" ] ; then
  if [ ! -z $CODE_ASTER_DATA_TESTING_INSTALL_DIR ]; then
    DATA=$CODE_ASTER_DATA_TESTING_INSTALL_DIR
  fi
elif [ £TAGNAME == "OLDSTABLE" ] ; then
  if [ ! -z $CODE_ASTER_DATA_OLDSTABLE_INSTALL_DIR ]; then
    DATA=$CODE_ASTER_DATA_OLDSTABLE_INSTALL_DIR
  fi
  SAVE_ECREVISSE=$ECREVISSE_ROOT_DIR
  export ECREVISSE_ROOT_DIR=$ECREVISSE_OLD_ROOT_DIR
fi

# Intel Compiler
intel_options=
use_intel=`ifort --help 2>/dev/null`
if [ ! -z "$use_intel" ]; then
    intel_options="--use-config=intel"
    # for med built by g++ & gfortran
    export LIB="$LIB gfortran stdc++"
fi

# create pkginfo.py
version=`echo £version | awk -F'.' '{print $1", "$2", "$3}'`
rev=`hg parent --template='{node|short}'`
date=`hg parent --template='{date|shortdate}' | awk -F'-' '{print $3"/"$2"/"$1}'`
# si version ne contient pas un point, on ne fait pas pginfo
echo £version |grep '\.'
nopoint=$?
branch=£branch
if [ "£version" = "v13.4_smeca" ]; then
    version="(13, 4, 0)"
    branch="v13"
fi
if [ $nopoint == 0 ]; then
    echo "pkginfo = [($version), '$rev', '$branch', '$date', '£branch', 0, []]" > ./bibpyt/Accas/pkginfo.py
fi

export LIBPATH="£waf_libpath"
export INCLUDES="£waf_includes"

# increase default memory required at startup
export ADDMEM=512
# avoid automatic environement sourcing
export DEVTOOLS_COMPUTER_ID=" "
# limit number of jobs
if [ `nproc` -gt 8 ]; then
    opts="--jobs=8"
fi
# execute
# disable-petsc by default to avoid linking against a PETSc system library
#
./waf configure --prefix=$CURRENT_SOFTWARE_INSTALL_DIR --disable-petsc --embed-aster $intel_options £waf_options --with-data=$DATA $opts
""")

build_command = PercentTemplate("""
# Template build_command
if [ `nproc` -gt 8 ]; then
    opts="--jobs=8"
fi
./waf build --out=${CURRENT_SOFTWARE_BUILD_DIR} $opts
""")

install_command = PercentTemplate("""
# Template install_command
if [ `nproc` -gt 8 ]; then
    opts="--jobs=8"
fi
./waf install --install-tests $opts
""")

post_command = PercentTemplate("""
# Template post_command

# Get relative path between Frontend and Code_Aster binary installation
LINKPATH=`python -c "import os ; print os.path.relpath( os.environ['CURRENT_SOFTWARE_INSTALL_DIR'], os.environ['CODE_ASTER_FRONTEND_INSTALL_DIR'] )"`

# Check if version is already in etc/codeaster/aster file and add it if not
shortTag=%shortTag
if [ "%shortTag" = "v13.4_smeca" ]; then
    shortTag="13.4"
fi
if [ -z "`grep 'vers : $shortTag:' $CODE_ASTER_FRONTEND_INSTALL_DIR/etc/codeaster/aster`" ]; then
    # Add "vers : tag:linkpath/share/aster" to conf file
    echo "vers : %tagname:$LINKPATH/share/aster" >> $CODE_ASTER_FRONTEND_INSTALL_DIR/etc/codeaster/aster
    echo "vers : $shortTag:$LINKPATH/share/aster" >> $CODE_ASTER_FRONTEND_INSTALL_DIR/etc/codeaster/aster
fi

cd $CURRENT_SOFTWARE_INSTALL_DIR
rm -f *.log

""")

post_inst_command = PercentTemplate("""
# Template post_inst_command

cp %post_filename ${CURRENT_SOFTWARE_BUILD_DIR}/postinstall.sh
rm -f %post_filename

if [ £TAGNAME == "OLDSTABLE" ] ; then
  ECREVISSE_ROOT_DIR=$SAVE_ECREVISSE
fi

""")

environment_shell = PercentTemplate("""
#------ code_aster - Template environment_shell ------
export ASTER_%TAGNAME="%install_dir"
if [ -d /usr/lib/atlas-base/atlas ]; then
    if [ -z "$LD_LIBRARY_PATH" ]; then
        export LD_LIBRARY_PATH=/usr/lib/atlas-base/atlas
    else
        export LD_LIBRARY_PATH=/usr/lib/atlas-base/atlas:$LD_LIBRARY_PATH
    fi
fi

""")

environment_cfg = PoundTemplate("""
#------ code_aster - Template environment_cfg ------
ASTER_£TAGNAME="£install_dir"
ADD_TO_LD_LIBRARY_PATH: /usr/lib/atlas-base/atlas
ADD_TO_LD_LIBRARY_PATH: %(ASTER_£TAGNAME)s/lib/aster
""")

#!/usr/bin/env python
# coding: utf-8
#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Mathieu Courtois (EDF R&D)

from builtins import map
from yamm.projects.salome.version import SalomeVersion


class SalomeMecaVersion(SalomeVersion):
    """
    .. py:currentmodule:: yamm.projects.salome_meca.SalomeMecaVersion

    Base class for Salome-Meca's versions.
    It adds a class attribute that must be set by the project built on this version.
    """
    package_name = None

    @classmethod
    def set_package_name(cls, package_name):
        """A Salome-Meca Project must define the package name"""
        cls.package_name = package_name

    def get_salome_version(self):
        """Return the version name used to create the appli : the package name"""
        return self.package_name

    def check_flavour(self, flavour):
        """Raise an error for an unknown flavour
        See :py:meth:`~version.update_flavour`.
        """
        if self.flavour and not self.has_flavour(self.flavour):
            msg  = "Version %s will ignore the flavour ( %s )" %(self.name, self.flavour)
            msg += '\nIt is not in the accepted flavours: [%s]'%(",".join(map(str, self.accepted_flavours)))
            self.logger.error(msg)

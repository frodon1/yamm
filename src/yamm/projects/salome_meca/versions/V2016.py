#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Isabelle FOURNIER & Jean-Pierre LEFEBVRE - 2014

from yamm.projects.salome.versions.V7_8_0 import V7_8_0
from yamm.projects.salome_meca.version import SalomeMecaVersion

version_name = "V2016"

"""
Fichier de construction d'une version de Salome-Meca 2016.
"""


class V2016(SalomeMecaVersion, V7_8_0 ):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        """Initialisation"""
        super(V2016, self).__init__(name, verbose, flavour)
        self.accepted_flavours = ["calibre_7",
                                  "calibre_9",
                                  "public",
                                  ]

    def configure_softwares(self):
        """
        This method is used to add softwares to the version.
        It is called during the initialisation of the object.
        """
        super(V2016, self).configure_softwares()
        # General dependencies
        self.add_software(
            "MERCURIAL",     "hg_tag/2.6.1", groups=["code_aster"])
        
        # On force version METIS = 4.0
        self.add_software(
            "METIS",     "4.0", groups=["base"])

        # On force version PARAVIS patché pour issue13193
        self.add_software(
            "PARAVIS",   "git_tag/V7_8_0_SMECA",groups=["base"])

        if "calibre_7" in self.flavour :
            self.add_software(
                "INTEL_RUNTIME", "hg_tag/2011.3.174", groups=["code_aster"])

        if "public" in self.flavour :
            self.add_software(
                "INTEL_RUNTIME", "hg_tag/2011.3.174", groups=["code_aster"])

        # Code_Aster dependencies
        self.add_software(
            "GIBI",          "hg_tag/2000-8", groups=["code_aster"])
        self.add_software(
            "GMSH_BIN",      "hg_tag/2.12.0Linux64", groups=["code_aster"])
        self.add_software(
            "HOMARD_ASTER",  "hg_tag/11.7_aster", groups=["code_aster"])
        self.add_software(
            "MFRONT",        "hg_tag/TFEL-2.0.3", groups=["code_aster"]) 
        self.add_software(
            "MFRONT_STABLE", "hg_tag/TFEL-2.0.2", groups=["code_aster"])
        self.add_software(
            "MISS3D",        "hg_tag/6.6_aster3", groups=["code_aster"])
        self.add_software(
            "MUMPS",         "hg_tag/5.0.1_consortium_aster5%seq", "5.0.1", groups=["code_aster"])
        self.add_software(
            "METIS_ASTER",   "2016.0" ,  groups=["code_aster"])
        self.add_software(
            "SCOTCH_ASTER",  "hg_tag/6.0.4_aster1" ,  groups=["code_aster"])
        self.add_software(
            "MUMPS4",      "4.10.0_aster3%seq", "4.10.0", groups=["code_aster"])

        # Salome-Meca modules
        self.add_software(
            "ASTER",            "2016.0", groups=["salome_meca"])
        self.add_software(
            "EUROPLEXUS",       "2016.0", groups=["salome_meca"])
        self.add_software(
            "OMA",              "2016.0", groups=["salome_meca_edf"])
        self.add_software(
            "MT",               "2016.0p1", groups=["salome_meca_edf"])
        self.add_software(
            "EDYOS",            "2016.0", groups=["salome_meca_edf"])
        self.add_software(
            "YACS_EDYOS",       "2016.0", groups=["salome_meca_edf"])
        self.add_software(
            "GVB",              "2016.0", groups=["salome_meca_edf"])

        # Salome-Meca dependencies (embedded softwares)
        self.add_software(
            "CODE_ASTER_STABLE",    "12.6.0", "v12", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_TESTING",   "13.2.0", "default", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_DATA",      "12.6.0", groups=["code_aster", "salome_meca_edf"])
        self.add_software(
            "CODE_ASTER_DATA_TESTING", "13.2.0", groups=["code_aster", "salome_meca_edf"])
        self.add_software(
            "CODE_EDYOS",       "2016.0", groups=["salome_meca_edf"])
        self.add_software(
            "CODE_GROOVE",       "2016.0", groups=["salome_meca_edf"])

        # Salome-Meca tools
        self.add_software(
            "CODE_ASTER_FRONTEND",  "2016.0", groups=["code_aster"])
        self.add_software(
            "SALOMEMECA_EXTRA", "2016.0", groups=["salome_meca"])
        self.add_software(
            "EFICAS_ASTER",     "2016.0", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_PYUTILS", "2016.0", groups=["salome_meca"])

        # Salome-Meca plugins
        self.add_software(
            "SALOMEMECA_AC",       "2016.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_ORT",       "2016.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_DHRC",      "2016.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_PLUGINS", "2016.0", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_MOFEM", "2016.0", groups=["salome_meca_edf"])


    def update_configuration_with_flavour(self):
        """
        This method is used to update the configuration with
        a given flavour.
        It is called by :py:meth:`~version.update_flavour`.
        """
        super(V2016, self).update_configuration_with_flavour()


        if "public" in self.flavour:
            # public release : remove EDF specific softwares
            # and change branch for few others
            self.add_software("SALOMEMECA_EXTRA",   "2016.0_public")
            self.add_software("SALOMEMECA_PLUGINS", "2016.0_public")
            self.remove_software_groups(["salome_meca_edf"])
            # change Mumps branch
            self.add_software(
                "MUMPS",     "hg_tag/5.0.1_aster1%seq", groups=["code_aster"])

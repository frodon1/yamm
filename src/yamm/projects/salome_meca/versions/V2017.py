#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011-2016 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Guillaume DROUET & Jean-Pierre LEFEBVRE - 2016

#
#  créé à partir de V2016 et de V8_INTEGR
#

from yamm.projects.salome.versions.V8_3_0 import V8_3_0
from yamm.projects.salome_meca.version import SalomeMecaVersion

version_name = "V2017"

"""
Fichier de construction de Salome-Meca 2017.
"""


class V2017(SalomeMecaVersion, V8_3_0 ):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        """Initialisation"""
        super(V2017, self).__init__(name, verbose, flavour)
        self.accepted_flavours = ["calibre_9",
                                  "public",
                                  ]

    def configure_softwares(self):
        """
        This method is used to add softwares to the version.
        It is called during the initialisation of the object.
        """
        super(V2017, self).configure_softwares()
        # General dependencies
        self.add_software(
            "GUI",     "V8_3_asterstudy", groups=["base"])

        self.add_software(
            "MERCURIAL",     "hg_tag/2.6.1", groups=["code_aster"])
        
        # On force version METIS = 4.0
        self.add_software(
            "METIS",     "4.0", groups=["base"])

        # Code_Aster dependencies
        self.add_software(
            "GIBI",          "hg_tag/2000-8", groups=["code_aster"])
        self.add_software(
            "GMSH_BIN",      "hg_tag/2.12.0Linux64", groups=["code_aster"])
        self.add_software(
            "HOMARD_ASTER",  "hg_tag/11.7_aster", groups=["code_aster"])
        self.add_software(
            "ECREVISSE",     "hg_tag/3.2.2", groups=["code_aster"])
        self.add_software(
            "ECREVISSE_OLD", "hg_tag/3.2.1", groups=["code_aster"])
        self.add_software(
            "MFRONT",        "git_tag/TFEL-3.0.0", groups=["code_aster"]) 
        self.add_software(
            "MFRONT_STABLE", "hg_tag/TFEL-2.0.2", groups=["code_aster"])
        self.add_software(
            "MISS3D",        "hg_tag/6.6_aster4", groups=["code_aster"])
        self.add_software(
            "MUMPS",         "hg_tag/5.1.1_consortium_aster%seq", "5.1.1", groups=["code_aster"])
        self.add_software(
            "METIS_ASTER",   "hg_tag/5.1.0_aster1" ,  groups=["code_aster"])
        self.add_software(
            "SCOTCH_ASTER",  "hg_tag/6.0.4_aster6%seq" ,  groups=["code_aster"])
        self.add_software(
            "MUMPS4",      "4.10.0_aster3%seq", "4.10.0", groups=["code_aster"])

        # Salome-Meca modules
        self.add_software(
            "ASTER",            "hg_tag/2017.0", groups=["salome_meca"])
        self.add_software(
            "EUROPLEXUS",       "hg_tag/2017.0", groups=["salome_meca"])
        self.add_software(
            "OMA",              "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "MT",               "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "EDYOS",            "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "YACS_EDYOS",       "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "ASTERSTUDY",  "2017.0", groups=["salome_meca"])

        self.add_software("CMAKE_ASTER", "3.0.2", groups=["salome_meca"])

# Salome-Meca dependencies (embedded softwares)
        self.add_software(
           "CODE_ASTER_OLDSTABLE", "12.8.0", "v12", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_STABLE",    "v13.4_smeca", "v13.4_smeca", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_DATA_OLDSTABLE", "12.8.0", groups=["code_aster", "salome_meca_edf"])
        self.add_software(
            "CODE_ASTER_DATA",     "13.4.0", groups=["code_aster", "salome_meca_edf"])
        self.add_software(
            "CODE_EDYOS",       "4.2.2", groups=["salome_meca_edf"])
        self.add_software(
            "CODE_GROOVE",       "1.0.0", groups=["salome_meca_edf"])
        self.add_software(
            "MOMA",              "hg_tag/2017.1-PSM", groups=["salome_meca_edf"])

        # Salome-Meca tools
        self.add_software(
            "CODE_ASTER_FRONTEND",  "hg_tag/2017.0", groups=["code_aster"])
        self.add_software(
            "SALOMEMECA_EXTRA", "hg_tag/2017.0", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_PYUTILS", "hg_tag/2017.0", groups=["salome_meca"])

        # Salome-Meca plugins
        self.add_software(
            "SALOMEMECA_AC",       "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_ORT",       "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_DHRC",      "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_PLUGINS", "hg_tag/2017.0", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_MOFEM", "hg_tag/2017.0", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_RSTE",  "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_ARCADE", "hg_tag/2017.0", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_TRANSLATOR", "hg_tag/2017.0", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_MAP", "hg_tag/2017.0", groups=["salome_meca_edf"])


    def update_configuration_with_flavour(self):
        """
        This method is used to update the configuration with
        a given flavour.
        It is called by :py:meth:`~version.update_flavour`.
        """
        super(V2017, self).update_configuration_with_flavour()

        if "public" in self.flavour:
            # public release : remove EDF specific softwares
            # and change branch for few others
            self.add_software("SALOMEMECA_EXTRA",   "2017.0_public")
            self.add_software("SALOMEMECA_PLUGINS", "2017.0_public")
            self.remove_software_groups(["salome_meca_edf"])
            # change Mumps branch
            self.add_software(
                "MUMPS",     "hg_tag/5.1.1_aster2", groups=["code_aster"])

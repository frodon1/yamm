#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Isabelle FOURNIER & Jean-Pierre LEFEBVRE - 2014

from yamm.projects.salome.versions.V7_dev import V7_dev
from yamm.projects.salome_meca.version import SalomeMecaVersion

version_name = "V7_DEV"

"""Fichier de construction d'une version de Salome-Meca de développement.
Sert d'exemple et pour les tests.
On pointe toujours vers les versions de développement de chaque produit.
Quand on commencer à figer une version, on crée le fichier, par exemple, V2016.py.
"""


class V7_DEV(SalomeMecaVersion, V7_dev ):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        """Initialisation"""
        super(V7_DEV, self).__init__(name, verbose, flavour)
        self.accepted_flavours = ["calibre_7",
                                  "calibre_9",
                                  "public",
                                  ]

    def configure_softwares(self):
        """
        This method is used to add softwares to the version.
        It is called during the initialisation of the object.
        """
        super(V7_DEV, self).configure_softwares()
        # General dependencies
        self.add_software(
            "MERCURIAL",     "hg_tag/2.6.1", groups=["code_aster"])

        #self.add_software("HDF5", "1.8.14", groups=["base"]) 
        #self.add_software("MEDFICHIER", "3.2.0", groups=["base"])

        if "calibre_7" in self.flavour :

            self.add_software(
                "INTEL_RUNTIME", "hg_tag/2011.3.174", groups=["code_aster"])

        # Code_Aster dependencies
        self.add_software(
            "GIBI",          "hg_tag/2000-8", groups=["code_aster"])
        self.add_software(
            "GMSH_BIN",      "hg_tag/2.12.0Linux64", groups=["code_aster"])
        self.add_software(
            "HOMARD_ASTER",  "hg_tag/11.7_aster", groups=["code_aster"])
        self.add_software(
            "MFRONT",        "hg_tag/TFEL-2.0.3", groups=["code_aster"]) 
        self.add_software(
            "MFRONT_STABLE", "hg_tag/TFEL-2.0.2", groups=["code_aster"])
        self.add_software(
            "MISS3D",        "hg_tag/6.6_aster3", groups=["code_aster"])
        self.add_software(
            "MUMPS",         "hg_tag/5.0.1_consortium_aster5%seq", "5.0.1", groups=["code_aster"])
        self.add_software(
            "METIS_ASTER",  "hg_tag/5.1.0_aster" ,  groups=["code_aster"])
        self.add_software(
            "SCOTCH_ASTER",  "hg_tag/6.0.4_aster1" ,  groups=["code_aster"])
        self.add_software(
            "MUMPS4",      "4.10.0_aster3%seq", "4.10.0", groups=["code_aster"])

        # Salome-Meca modules
        self.add_software(
            "ASTER",            "default", groups=["salome_meca"])
        self.add_software(
            "EUROPLEXUS",       "default", groups=["salome_meca"])
        self.add_software(
            "OMA",              "trunk", groups=["salome_meca_edf"])
        self.add_software(
            "MT",               "default", groups=["salome_meca_edf"])
        self.add_software(
            "EDYOS",            "default", groups=["salome_meca_edf"])
        self.add_software(
            "YACS_EDYOS",       "default", groups=["salome_meca_edf"])
        self.add_software(
            "GVB",              "default", groups=["salome_meca_edf"])

        # Salome-Meca dependencies (embedded softwares)
        #self.add_software(
        #   "CODE_ASTER_OLDSTABLE", "v11", "v11", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_STABLE",    "v12", "v12", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_TESTING",   "default", "default", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_DATA",      "v12", groups=["code_aster", "salome_meca_edf"])
        self.add_software(
            "CODE_ASTER_DATA_TESTING", "default", groups=["code_aster", "salome_meca_edf"])
        self.add_software(
            "CODE_EDYOS",       "2015.2_decole", groups=["salome_meca_edf"])
        self.add_software(
            "CODE_GROOVE",       "default", groups=["salome_meca_edf"])

        # Salome-Meca tools
        self.add_software(
            "CODE_ASTER_FRONTEND",  "salome-meca", groups=["code_aster"])
        self.add_software(
            "SALOMEMECA_EXTRA", "default", groups=["salome_meca"])
        self.add_software(
            "EFICAS_ASTER",     "eficas-aster", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_PYUTILS", "default", groups=["salome_meca"])

        # Salome-Meca plugins
        self.add_software(
            "SALOMEMECA_AC",       "default", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_ORT",       "default", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_DHRC",      "default", groups=["salome_meca_edf"])
        self.add_software(
            "SALOMEMECA_PLUGINS", "default", groups=["salome_meca"])
        self.add_software(
            "SALOMEMECA_MOFEM", "default", groups=["salome_meca_edf"])


    def update_configuration_with_flavour(self):
        """
        This method is used to update the configuration with
        a given flavour.
        It is called by :py:meth:`~version.update_flavour`.
        """
        super(V7_DEV, self).update_configuration_with_flavour()

        if "public" in self.flavour:
            # public release : remove EDF specific softwares
            # and change branch for few others
            self.add_software("SALOMEMECA_EXTRA",   "lgpl")
            self.add_software("SALOMEMECA_PLUGINS", "lgpl")
            self.remove_software_groups(["salome_meca_edf"])
            # change Mumps branch
            self.add_software(
                "MUMPS",     "hg_tag/5.0.1_aster1%seq", groups=["code_aster"])

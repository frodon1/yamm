#!/usr/bin/env python
# -*- coding: utf-8 *-
#  Copyright (C) 2011-2015 EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
#
#  Author : Isabelle FOURNIER & Jean-Pierre LEFEBVRE - 2014

from yamm.projects.salome.versions.V8_3_0 import V8_3_0
from yamm.projects.salome_meca.version import SalomeMecaVersion

version_name = "V8_PRE_default"

"""Fichier de construction d'une version de Salome-Meca de développement.
Sert d'exemple et pour les tests.
On pointe toujours vers les versions de développement de chaque produit.
Quand on commencer à figer une version, on crée le fichier, par exemple, V2016.py.
"""


class V8_PRE_default(SalomeMecaVersion, V8_3_0 ):

    def __init__(self, name=version_name, verbose=0, flavour=""):
        """Initialisation"""
        super(V8_PRE_default, self).__init__(name, verbose, flavour)
        self.accepted_flavours = ["calibre_7",
                                  "codeaster_prerequisites_calibre_7",
                                  "codeaster_prerequisites_calibre_7_mpi",
                                  "codeaster_prerequisites_calibre_9",
                                  "codeaster_prerequisites_calibre_9_mpi",
                                  "codeaster_prerequisites_calibre_7_servers",
                                  "codeaster_prerequisites_calibre_7_servers_mpi",
                                  "codeaster_prerequisites_centos_32",
                                  ]

    def configure_softwares(self):
        """
        This method is used to add softwares to the version.
        It is called during the initialisation of the object.
        """
        super(V8_PRE_default, self).configure_softwares()
        # General dependencies

        self.add_software("HDF5", "1.8.14", groups=["base"])
        self.add_software("MEDFICHIER", "3.2.1", groups=["base"])

        self.add_software(
            "INTEL_RUNTIME", "2011.3.174", groups=["code_aster"])
        self.add_software(
            "MERCURIAL",     "2.6.1", groups=["code_aster"])

        # Code_Aster dependencies
        self.add_software(
            "GIBI",          "hg_tag/2000-8", groups=["code_aster"])
        self.add_software(
            "GMSH_BIN",      "hg_tag/2.12.0Linux64", groups=["code_aster"])
        self.add_software(
            "HOMARD_ASTER",  "hg_tag/11.7_aster", groups=["code_aster"])
        self.add_software(
            "ECREVISSE",     "hg_tag/3.2.2", groups=["code_aster"])
        self.add_software(
            "MFRONT",        "git_tag/TFEL-3.0.0", groups=["code_aster"])
        self.add_software(
            "MISS3D",        "hg_tag/6.6_aster4", groups=["code_aster"])
        self.add_software(
            "MUMPS",         "hg_tag/5.1.1_consortium_aster%seq", "5.1.1", groups=["code_aster"])
        self.add_software(
            "METIS_ASTER",   "hg_tag/5.1.0_aster1" ,  groups=["code_aster"])
        self.add_software(
            "SCOTCH_ASTER",  "hg_tag/6.0.4_aster6%seq" ,  groups=["code_aster"])


        # Salome-Meca dependencies (embedded softwares)
        #self.add_software(
        #   "CODE_ASTER_OLDSTABLE", "v11", "v11", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_STABLE",    "v12", "v12", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_TESTING",   "default", "default", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_DATA",   "v12", "v12", groups=["code_aster"])
        self.add_software(
            "CODE_ASTER_DATA_TESTING",   "default", "default", groups=["code_aster"])

        # Salome-Meca tools
        self.add_software(
            "CODE_ASTER_FRONTEND",  "salome-meca", groups=["code_aster"])

    def update_configuration_with_flavour(self):
        """
        This method is used to update the configuration with
        a given flavour.
        It is called by :py:meth:`~version.update_flavour`.
        """
        super(V8_PRE_default, self).update_configuration_with_flavour()

        if "centos_32" in self.flavour:
            # only currently used for prerequisites but may be used by
            # Salome-Meca when it will embed a mpi build of Code_Aster
            self.add_software(
            "GMSH_BIN",      "2.12.0Linux32", groups=["code_aster"])

        if "mpi" in self.flavour:
            # only currently used for prerequisites but may be used by
            # Salome-Meca when it will embed a mpi build of Code_Aster
            self.add_software(
                "PETSC_MPI", "3.7.3_aster", "petsc_aster" , groups=["code_aster"])
            self.add_software(
                "MUMPS",     "5.1.1_consortium_aster%mpi", "5.1.1", groups=["code_aster"])
            self.add_software(
                "PARMETIS_ASTER",  "4.0.3_aster" ,  groups=["code_aster"])
            self.add_software(
                "SCOTCH_ASTER",  "6.0.4_aster6%mpi" ,  groups=["code_aster"])

        if "codeaster_prerequisites" in self.flavour:
            # liste des prerequis de Code_Aster stand-alone
            prerequisites = [
                "MEDFICHIER",
                "HDF5",
#                "METIS",
                "METIS_ASTER",
                "GIBI",
                "GMSH_BIN",
                "HOMARD_ASTER",
                "ECREVISSE",
                "MFRONT",
                "MISS3D",
                "MUMPS",
                "SCOTCH_ASTER",
#                "CODE_ASTER_DATA_TESTING",
#                "CODE_ASTER_DATA",
#                "CODE_ASTER_STABLE",
#                "CODE_ASTER_TESTING",
            ]
            if "servers" not in self.flavour and not "centos_32" in self.flavour :
		 prerequisites.extend([
                    "CODE_ASTER_FRONTEND",
                ])
            if "calibre_9" in self.flavour:
                prerequisites.extend([
                    "CMAKE",
#                    "LAPACK",
                ])
            elif "calibre_7" in self.flavour or "centos_32" in self.flavour :
                prerequisites.extend([
                    "CMAKE",
                    "LAPACK",
                    "MERCURIAL",
                    "NUMPY",
                    "PYTHON",
                    "SCIPY",
                    "TCL",
                    "TIX",
                    "TK",
                ])
            if "mpi" in self.flavour:
                 prerequisites.extend([
                    "PETSC_MPI",
                    "PARMETIS_ASTER",
                ])

            self.set_list_of_softwares(prerequisites)

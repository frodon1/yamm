# Development
versions_list = [
    "V7_DEV",  "V7_INTEGR", "DEV",
    "V8_PRE_default", "V8_PRE_v12",
    "V2017", "V8_INTEGR",
]

LAST_STABLE = 'V2017'

# Obsoletes versions
# Files were removed from directory.
# They can be retrieved in the git history.

old_versions_list = ["V2014_1", "V2014_2", "V2015_1", "V2015_2","V2016"]

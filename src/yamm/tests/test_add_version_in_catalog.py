# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 3
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

__author__ = "André RIBES - 2011"

from yamm.projects.salome.project import Project

salome_project = Project()

print()
print("Print default versions in the salome project:")
print()

salome_project.catalog.print_versions()

# Adding a new version
salome_project.add_version_in_catalog("new_salome_version", "tests_files")

print()
print("Print versions in the salome project:")
print()

salome_project.catalog.print_versions()

# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys

py_version = sys.version[:3]
yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 3
yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
sys.path.append(yamm_src_dir)
sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))

__author__ = "André RIBES - 2011"

from yamm.projects.salome.project import Project

salome_project = Project()
salome_project.set_version("V7_7_1")

print()
print("Test of use of software_reset_config_options with True")
print()

salome_project.options.set_software_option("PARAVIEW", "software_reset_config_options", True)
salome_project.begin_command()
software_mode = "nothing"
for software in salome_project.current_command_softwares:
  if software.name == "PARAVIEW":
    software.set_command_env(salome_project.current_command_softwares, salome_project.current_command_options, software_mode, salome_project.current_command_version_object, salome_project.current_command_python_version)
    software.print_infos()
    break
salome_project.end_command()

print()
print("Test of use of software_reset_config_options with False")
print()

salome_project.options.set_software_option("PARAVIEW", "software_reset_config_options", False)
salome_project.begin_command()
software_mode = "nothing"
for software in salome_project.current_command_softwares:
  if software.name == "PARAVIEW":
    software.set_command_env(salome_project.current_command_softwares, salome_project.current_command_options, software_mode, salome_project.current_command_version_object, salome_project.current_command_python_version)
    software.print_infos()
    break
salome_project.end_command()

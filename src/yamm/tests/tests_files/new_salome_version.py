#!/usr/bin/env python
# -*- coding: utf-8 -*-

from yamm.core.framework.version import FrameworkVersion

version_name = "TEST_ADD_VERSION"

class TEST_ADD_VERSION(FrameworkVersion):

  def __init__(self, name=version_name, verbose=0, flavour=""):
    FrameworkVersion.__init__(self, name, verbose, flavour)

  def configure_softwares(self):
    self.add_software("KERNEL", "V8_main")



# -*- coding: utf-8 -*-
import unittest


__author__ = 'André RIBES - 2011'


class YammTests(unittest.TestCase):

    def setUp(self):
        import os
        import sys

        py_version = sys.version[:3]
        yamm_default_dir = (os.path.dirname(os.path.abspath(__file__)),) + (os.pardir,) * 3
        yamm_src_dir = os.path.join(*(yamm_default_dir + ('src',)))
        yamm_lib_dir = os.path.join(*(yamm_default_dir + ('lib',)))
        sys.path.append(yamm_src_dir)
        sys.path.append(os.path.join(yamm_lib_dir, 'python%s' % py_version, 'site-packages'))
        from yamm.projects.salome.project import Project
        self.salome_project = Project()

    def testAddSoftwareInCatalog(self):
        self.assertFalse(self.salome_project.catalog.has_software('TEST_ADD_SOFTWARE'))
        # Adding a new software
        self.salome_project.add_software_in_catalog('new_salome_software',
                                                    'tests_files')
        self.assertTrue(self.salome_project.catalog.has_software('TEST_ADD_SOFTWARE'))

    def testAddVersionInCatalog(self):
        self.assertFalse(self.salome_project.catalog.has_version('TEST_ADD_VERSION'))
        # Adding a new version
        self.salome_project.add_version_in_catalog('new_salome_version',
                                                   'tests_files')
        self.assertTrue(self.salome_project.catalog.has_version('TEST_ADD_VERSION'))

    def testResetOptionWithTrue(self):
        self.salome_project.set_version('DEV')
        self.salome_project.set_software_option('PARAVIEW',
                                                'software_reset_config_options',
                                                True)
        self.salome_project.begin_command()
        has_config_option = True
        for software in self.salome_project.current_command_softwares:
            if software.name == 'PARAVIEW':
                software.set_command_env(self.salome_project.current_command_softwares,
                                         self.salome_project.current_command_options,
                                         'nothing',
                                         self.salome_project.current_command_version_object,
                                         self.salome_project.current_command_python_version)
                has_config_option = 'config_options' in list(software.get_infos().keys())
                break
        self.salome_project.end_command()
        self.assertFalse(has_config_option)

    def testResetOptionWithFalse(self):
        self.salome_project.set_version('DEV')
        self.salome_project.set_software_option('PARAVIEW',
                                                'software_reset_config_options',
                                                False)
        self.salome_project.begin_command()
        has_config_option = False
        for software in self.salome_project.current_command_softwares:
            if software.name == 'PARAVIEW':
                software.set_command_env(self.salome_project.current_command_softwares,
                                         self.salome_project.current_command_options,
                                         'nothing',
                                         self.salome_project.current_command_version_object,
                                         self.salome_project.current_command_python_version)
                has_config_option = 'config_options' in list(software.get_infos().keys())
                break
        self.salome_project.end_command()
        self.assertTrue(has_config_option)

    def testCheckTagSha1(self):
        import shutil
        import tempfile
        tmpdir = tempfile.mkdtemp()
        self.salome_project.set_version('DEV')
        self.salome_project.set_global_option('top_directory', tmpdir)
        self.salome_project.set_global_option('use_pleiade_mirrors', True)
        self.salome_project.set_global_option('software_only_list', ['LIBBATCH'])
        self.salome_project.set_global_option('softwares_user_version', {'LIBBATCH': 'git_tag/V2_3_1,92e7402a'})
        self.assertTrue(self.salome_project.download())
        shutil.rmtree(tmpdir)
        self.salome_project.set_global_option('softwares_user_version', {'LIBBATCH': 'git_tag/V2_3_1,123456'})
        self.assertFalse(self.salome_project.download())
        shutil.rmtree(tmpdir)


def main():
    try:
        import xmlrunner
        suite = unittest.TestLoader().loadTestsFromTestCase(YammTests)
        xmlrunner.XMLTestRunner().run(suite)
    except ImportError:
        unittest.main()

if __name__ == '__main__':
    main()
